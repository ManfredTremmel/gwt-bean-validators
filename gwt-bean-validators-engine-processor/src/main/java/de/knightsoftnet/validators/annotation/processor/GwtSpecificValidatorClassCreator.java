package de.knightsoftnet.validators.annotation.processor;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.RegExUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.hibernate.validator.internal.metadata.core.ConstraintOrigin;
import org.hibernate.validator.internal.metadata.location.ConstraintLocation.ConstraintLocationKind;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.MissingResourceException;
import java.util.Objects;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.processing.FilerException;
import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.AnnotationValue;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.ArrayType;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.ExecutableType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.tools.Diagnostic;
import javax.tools.JavaFileObject;

import jakarta.validation.Constraint;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.GroupSequence;
import jakarta.validation.UnexpectedTypeException;
import jakarta.validation.groups.Default;

public class GwtSpecificValidatorClassCreator {

  private enum Stage {
    OBJECT, PROPERTY, VALUE
  }

  private static final String DEFAULT_VIOLATION_VAR = "violations";

  private final Set<TypeMirror> additionalInterfacesToCreate = Sets.newConcurrentHashSet();

  private final Set<Element> fieldsToWrap = Sets.newHashSet();

  private final Set<ExecutableElement> gettersToWrap = Sets.newHashSet();

  private final Map<TypeElementConstraintDescriptor<?>, Boolean> validTypeElementConstraintsMap =
      Maps.newHashMap();

  private final Set<String> validGroupsTypeMirror;

  private final ProcessingEnvironment processingEnv;

  private final boolean forceUsingGetter;

  private final boolean generateReflectionGetter;


  /**
   * constructor.
   *
   * @param processingEnv processing environment
   * @param validGroups set of validation groups
   */
  public GwtSpecificValidatorClassCreator(final ProcessingEnvironment processingEnv,
      final Set<TypeMirror> validGroups, final boolean forceUsingGetter,
      final boolean generateReflectionGetter, final String[] languages) {
    super();
    this.processingEnv = processingEnv;
    this.forceUsingGetter = forceUsingGetter;
    this.generateReflectionGetter = generateReflectionGetter;
    validGroupsTypeMirror =
        validGroups.stream().map(TypeMirror::toString).collect(Collectors.toSet());
    readValidationMessages(List.of(languages));
  }

  private void readValidationMessages(final List<String> languages) {
    if (ValidationMessagesMap.isEmpty()) {
      languages.stream().map(Locale::forLanguageTag).forEach(this::readValidationMessages);
    }
  }

  private void readValidationMessages(final Locale locale) {
    ResourceBundle bundle = null;
    try {
      bundle = ResourceBundle.getBundle("ValidationMessages", locale, new Utf8Control());
    } catch (final MissingResourceException e) {
      try {
        bundle = ResourceBundle.getBundle("org.hibernate.validator.ValidationMessages", locale,
            new Utf8Control());
      } catch (final MissingResourceException e2) {
        return;
      }
    }
    if (bundle == null || !bundle.getKeys().hasMoreElements()) {
      return;
    }
    final Enumeration<String> keys = bundle.getKeys();
    while (keys.hasMoreElements()) {
      final String key = keys.nextElement();
      final String value = bundle.getString(key);
      ValidationMessagesMap.addMessage(key, locale, value);
    }
  }

  /**
   * write validation class for a given type mirror.
   *
   * @param typeMirror type mirror of class to validate
   * @return additional interfaces to create
   */
  public Set<TypeMirror> writeClass(final TypeMirror typeMirror) {
    fieldsToWrap.clear();
    gettersToWrap.clear();
    final String packageToGenerate = processingEnv.getElementUtils()
        .getPackageOf(processingEnv.getTypeUtils().asElement(typeMirror)).getQualifiedName()
        .toString();
    final String classToGenerate = TypeUtils.getValidationClassForType(typeMirror,
        processingEnv.getTypeUtils(), processingEnv.getElementUtils());
    try {
      final JavaFileObject builderFile =
          processingEnv.getFiler().createSourceFile(packageToGenerate + "." + classToGenerate);
      try (PrintWriter out = new PrintWriter(builderFile.openWriter())) {
        writeClassHeader(out, typeMirror);
        out.println();
        writeClassBody(out, typeMirror);
        out.println();
        writeClassFooter(out);
      }
    } catch (final FilerException e) {
      // happens when trying to recreate an existing interface
      processingEnv.getMessager().printMessage(Diagnostic.Kind.NOTE, e.getMessage());
    } catch (final IOException | UnexpectedTypeException e) {
      processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR, e.getMessage());
      e.printStackTrace();
    }
    return additionalInterfacesToCreate;
  }

  /**
   * Returns the literal value of an object that is suitable for inclusion in Java Source code.
   *
   * <p>
   * Supports all types that {@link Annotation} value can have.
   * </p>
   *
   * @param value the object to handle
   * @return string of the literal
   * @throws IllegalArgumentException if the type of the object does not have a java literal form.
   */
  @SuppressWarnings("deprecation")
  private static String asLiteral(final Object value, final TypeMirror type,
      final ProcessingEnvironment processingEnv) throws IllegalArgumentException {
    if (value == null) {
      return "null";
    }

    final Class<?> clazz = value.getClass();

    if (clazz.isArray()) {
      final StringBuilder sb = new StringBuilder();
      final Object[] array = (Object[]) value;

      sb.append("new " + clazz.getComponentType().getCanonicalName() + "[] {");
      boolean first = true;
      for (final Object object : array) {
        if (first) {
          first = false;
        } else {
          sb.append(',');
        }
        sb.append(asLiteral(object, type, processingEnv));
      }
      sb.append('}');
      return sb.toString();
    }

    if (value instanceof Boolean) {
      return Objects.toString(value);
    } else if (value instanceof Byte) {
      return Objects.toString(value) + "Y";
    } else if (value instanceof Short) {
      return Objects.toString(value) + "S";
    } else if (value instanceof Character) {
      return '\'' + StringEscapeUtils.escapeJava(Objects.toString(value)) + '\'';
    } else if (value instanceof final Class<?> valueClass) {
      return valueClass.getCanonicalName() + ".class";
    } else if (value instanceof Double) {
      return Objects.toString(value) + "D";
    } else if (value instanceof final Enum<?> valueEnum) {
      return value.getClass().getCanonicalName() + "." + valueEnum.name();
    } else if (value instanceof Float) {
      return Objects.toString(value) + "F";
    } else if (value instanceof Integer) {
      return Objects.toString(value);
    } else if (value instanceof Long) {
      return Objects.toString(value) + "L";
    } else if (value instanceof final String valueString) {
      return '"' + StringEscapeUtils.escapeJava(valueString) + '"';
    } else if (value instanceof final TypeMirror typeMirror) {
      return switch (typeMirror.getKind()) {
        case ARRAY -> asLiteral(((ArrayType) value).getComponentType(), type, processingEnv);
        case EXECUTABLE -> asLiteral(((ExecutableType) value).getReturnType(), type, processingEnv);
        default -> typeMirrorAsLiteral(type, typeMirror, processingEnv);
      };
    } else if (value instanceof final Collection<?> list) {
      final StringBuilder sb = new StringBuilder();

      sb.append("new ");
      final String classString;
      if (type == null) {
        classString = "java.lang.Class";
      } else {
        classString = RegExUtils.replacePattern(
            StringUtils.removeEnd(asLiteral(type, null, processingEnv), ".class"),
            "^java\\.lang\\.Class<.*>$", "java.lang.Class");
      }
      sb.append(classString);
      sb.append("[] {");
      boolean first = true;
      for (final Object object : list) {
        if (first) {
          first = false;
        } else {
          sb.append(',');
        }
        sb.append("\n              ");
        sb.append(asLiteral(object, type, processingEnv));
      }
      sb.append('}');
      return sb.toString();
    } else if (value instanceof final AnnotationMirror annotation) {
      final StringBuilder sb = new StringBuilder();
      sb.append("new ");
      sb.append(annotation.getAnnotationType().toString());
      sb.append("() {\n");

      sb.append("              @Override\n");
      sb.append("              public Class<? extends Annotation> annotationType() {\n");
      sb.append("                return null;\n");
      sb.append("              }\n\n");

      for (final Entry<? extends ExecutableElement, //
          ? extends AnnotationValue> elementeValue : annotation.getElementValues().entrySet()) {
        sb.append("              @Override\n");
        sb.append("              public ");
        sb.append(TypeUtils.getClassNameWithProperties(elementeValue.getKey().getReturnType()));
        sb.append(" ");
        sb.append(elementeValue.getKey().toString());
        sb.append(" {\n");
        sb.append("                return ");
        sb.append(asLiteral(elementeValue.getValue().getValue(),
            elementeValue.getKey().getReturnType(), processingEnv));
        sb.append(";\n");
        sb.append("              }\n\n");

      }

      sb.append("              }");
      return sb.toString();
    } else if (type != null) {
      return switch (type.getKind()) {
        case EXECUTABLE -> asLiteral(value, ((ExecutableType) type).getReturnType(), processingEnv);
        case DECLARED -> TypeUtils.getClassName(type) + "." + value.toString();
        default -> value.toString();
      };
    } else {
      // TODO(nchalko) handle Annotation types
      throw new IllegalArgumentException(
          value.getClass() + " can not be represented as a Java Literal.");
    }
  }

  private static String typeMirrorAsLiteral(final TypeMirror typeParent, final TypeMirror typeField,
      final ProcessingEnvironment processingEnv) {
    if (typeParent != null && typeParent.getKind() == TypeKind.DECLARED) {
      return TypeUtils.getClassName(processingEnv.getTypeUtils()
          .asMemberOf((DeclaredType) typeParent, processingEnv.getTypeUtils().asElement(typeField)))
          + ".class";
    }
    return TypeUtils.getClassName(typeField) + ".class";
  }

  private void writeClassHeader(final PrintWriter out, final TypeMirror typeMirror) {

    // package xxx.yyy.zzz;
    out.print("package ");
    out.print(processingEnv.getElementUtils()
        .getPackageOf(processingEnv.getTypeUtils().asElement(typeMirror)).getQualifiedName()
        .toString());
    out.println(";");
    out.println();

    // imports
    out.println("import de.knightsoftnet.validators.client.impl.AbstractGwtSpecificValidator;");
    out.println("import de.knightsoftnet.validators.client.impl.ConstraintDescriptorImpl;");
    out.println("import de.knightsoftnet.validators.client.impl.Group;");
    out.println("import de.knightsoftnet.validators.client.impl.GroupChain;");
    out.println("import de.knightsoftnet.validators.client.impl.GroupChainGenerator;");
    out.println("import de.knightsoftnet.validators.client.impl.GwtBeanDescriptor;");
    out.println("import de.knightsoftnet.validators.client.impl.GwtBeanDescriptorImpl;");
    out.println("import de.knightsoftnet.validators.client.impl.GwtValidationContext;");
    out.println("import de.knightsoftnet.validators.client.impl.metadata.BeanMetadata;");
    out.println(
        "import de.knightsoftnet.validators.client.impl.metadata.ValidationGroupsMetadata;");
    out.println("import de.knightsoftnet.validators.client.impl.PropertyDescriptorImpl;");
    out.println();

    out.println("import org.hibernate.validator.internal.engine.path.PathImpl;");
    out.println();

    out.println("import java.lang.annotation.Annotation;");
    out.println("import java.lang.IllegalArgumentException;");
    out.println("import java.util.ArrayList;");
    out.println("import java.util.Collection;");
    out.println("import java.util.HashSet;");
    out.println("import java.util.Iterator;");
    out.println("import java.util.List;");
    out.println("import java.util.Set;");
    out.println("import java.util.stream.Collectors;");
    out.println("import java.util.stream.Stream;");
    out.println();

    out.println("import jakarta.validation.ConstraintViolation;");
    out.println("import jakarta.validation.Path.Node;");
    out.println("import jakarta.validation.ValidationException;");
    out.println();

    // public class _XxxxxxxValidatorImpl extends AbstractGwtSpecificValidator<been.to.validate>
    // implements
    // _XxxxxxxValidator {
    out.print("public class ");
    out.println(TypeUtils.getValidationClassForType(typeMirror, processingEnv.getTypeUtils(),
        processingEnv.getElementUtils()));
    out.print("    extends AbstractGwtSpecificValidator<");
    out.print(TypeUtils.getClassNameWithProperties(typeMirror));
    out.println(">");
    out.print("    implements ");
    out.print(TypeUtils.getValidationInterfaceForType(typeMirror, processingEnv.getTypeUtils()));
    out.print(" {");
  }

  protected void writeClassBody(final PrintWriter out, final TypeMirror typeMirror)
      throws UnexpectedTypeException {
    writeFields(out, typeMirror);
    out.println();
    writeValidateClassGroups(out, typeMirror);
    out.println();
    writeExpandDefaultAndValidateClassGroups(out, typeMirror);
    out.println();
    writeExpandDefaultAndValidatePropertyGroups(out, typeMirror);
    out.println();
    writeExpandDefaultAndValidateValueGroups(out, typeMirror);
    out.println();
    writeValidatePropertyGroups(out, typeMirror);
    out.println();
    writeValidateValueGroups(out, typeMirror);
    out.println();
    writeGetBeanMetadata(out);
    out.println();
    writeGetDescriptor(out, typeMirror);
    out.println();
    writePropertyValidators(out, typeMirror);
    out.println();
    writeValidateAllNonInheritedProperties(out, typeMirror);
    out.println();

    // Write the wrappers after we know which are needed
    if (!forceUsingGetter) {
      writeWrappers(out, typeMirror);
      out.println();
    }

    if (TypeUtils.isBeanConstrained(typeMirror, processingEnv.getTypeUtils(),
        processingEnv.getElementUtils()) && generateReflectionGetter) {
      final List<String> annotationParameters = detectAnnotationStringParameters(typeMirror);
      if (CollectionUtils.isNotEmpty(annotationParameters)) {
        writeReflectionGetterReplacement(out, typeMirror, annotationParameters);
      }
    }
  }

  private void writeClassFooter(final PrintWriter out) {
    out.println("}");
  }

  private void writeFields(final PrintWriter out, final TypeMirror typeMirror)
      throws UnexpectedTypeException {

    // private static final java.util.List<String> ALL_PROPERTY_NAMES =
    out.println("  private static final java.util.List<String> ALL_PROPERTY_NAMES = ");

    // Collections.<String>unmodifiableList(
    out.println("      java.util.Collections.<String>unmodifiableList(");

    // java.util.Arrays.<String>asList(
    out.print("          java.util.Arrays.<String>asList(");

    // "foo","bar" );
    out.print(TypeUtils.getGetterNames(typeMirror).stream()
        .map(getter -> "\"" + TypeUtils.valueFromGetter(getter) + "\"")
        .collect(Collectors.joining(",")));
    out.println("));");

    // Write the metadata for the bean
    writeBeanMetadata(out, typeMirror);
    out.println();

    // Create a variable for each constraint of each property
    for (final TypeElementPropertyDescriptor p : TypeUtils.getTypeElementProperties(typeMirror,
        processingEnv)) {
      int count = 0;
      for (final TypeElementConstraintDescriptor<?> constraint : p.getConstraintDescriptors()) {
        if (areConstraintDescriptorGroupsValid(constraint)) {
          writeConstraintDescriptor(out, constraint,
              constraint.getConstraintLocationKind().getElementType(), constraint.getDefinedOn(),
              constraintDescriptorVar(p.getPropertyName(), count++), p);
        }
      }
      writePropertyDescriptor(out, p);
    }

    // Create a variable for each constraint of this class.
    int count = 0;
    for (final TypeElementConstraintDescriptor<?> constraint : TypeUtils
        .getTypeElementConstraintDescriptors(typeMirror, processingEnv.getElementUtils(),
            processingEnv.getTypeUtils())) {
      if (areConstraintDescriptorGroupsValid(constraint)) {
        writeConstraintDescriptor(out, constraint, ElementType.TYPE, constraint.getDefinedOn(),
            constraintDescriptorVar("this", count++),
            new TypeElementPropertyDescriptorImpl(
                processingEnv.getTypeUtils().asElement(typeMirror), (DeclaredType) typeMirror,
                processingEnv, ConstraintLocationKind.TYPE));
      }
    }

    // Now write the BeanDescriptor after we already have the
    // PropertyDescriptors and class constraints
    writeBeanDescriptor(out, typeMirror);
    out.println();
  }

  /**
   * check if elementClass is iterable.
   *
   * @param elementClass class to check
   * @return true if iterable, otherwise false
   */
  private boolean isIterableOrMap(final TypeMirror elementClass) {
    if (elementClass.getKind() == TypeKind.ARRAY) {
      return true;
    }
    return typeMirrorIsOrHasInterface(elementClass, "java.lang.Iterable")
        || typeMirrorIsOrHasInterface(elementClass, "java.util.Map");
  }

  /**
   * check if elementClass is map.
   *
   * @param elementClass class to check
   * @return true if map, otherwise false
   */
  private boolean isMap(final TypeMirror elementClass) {
    return typeMirrorIsOrHasInterface(elementClass, "java.util.Map");
  }

  /**
   * check if elementClass is List.
   *
   * @param elementClass class to check
   * @return true if list, otherwise false
   */
  private boolean isList(final TypeMirror elementClass) {
    return typeMirrorIsOrHasInterface(elementClass, "java.util.List");
  }

  /**
   * check if elementClass is List.
   *
   * @param elementClass class to check
   * @return true if list, otherwise false
   */
  private boolean typeMirrorIsOrHasInterface(final TypeMirror elementClass,
      final String className) {
    final List<String> interfaceNames =
        TypeUtils.getInterfaces(elementClass).stream().map(TypeUtils::getClassName).toList();

    return interfaceNames.contains(className)
        || className.equals(TypeUtils.getClassName(elementClass));
  }

  private void writeExpandDefaultAndValidateClassGroups(final PrintWriter out,
      final TypeMirror typeMirror) throws UnexpectedTypeException {
    // public <T> void expandDefaultAndValidateClassGroups(
    out.println("  public <T> void expandDefaultAndValidateClassGroups(");

    // GwtValidationContext<T> context, BeanType object,
    // Set<ConstraintViolation<T>> violations, Group... groups) {
    out.println("      final GwtValidationContext<T> context,");
    out.print("      final ");
    out.println(TypeUtils.getClassNameWithProperties(typeMirror) + " object,");
    out.println("      final Set<ConstraintViolation<T>> violations,");
    out.println("      final Group... groups) {");

    writeExpandDefaultAndValidate(out, typeMirror, Stage.OBJECT); // NOPMD

    // }
    out.println("  }");
  }

  private void writeExpandDefaultAndValidatePropertyGroups(final PrintWriter out,
      final TypeMirror typeMirror) throws UnexpectedTypeException {
    // public <T> void expandDefaultAndValidatePropertyGroups(
    out.println("  public <T> void expandDefaultAndValidatePropertyGroups(");

    // GwtValidationContext<T> context, BeanType object, String propertyName,
    // Set<ConstraintViolation<T>> violations, Group... groups) {
    out.println("      final GwtValidationContext<T> context,");
    out.print("      final ");
    out.println(TypeUtils.getClassNameWithProperties(typeMirror) + " object,");
    out.println("      final String propertyName,");
    out.println("      final Set<ConstraintViolation<T>> violations,");
    out.println("      final Group... groups) {");

    writeExpandDefaultAndValidate(out, typeMirror, Stage.PROPERTY); // NOPMD

    // }
    out.println("  }");
  }

  private void writeExpandDefaultAndValidateValueGroups(final PrintWriter out,
      final TypeMirror typeMirror) throws UnexpectedTypeException {
    // public <T> void expandDefaultAndValidateValueGroups(
    out.println("  public <T> void expandDefaultAndValidateValueGroups(");

    // GwtValidationContext<T> context, Class<Author> beanType, String propertyName,
    // Object value, Set<ConstraintViolation<T>> violations, Group... groups) {
    out.println("      final GwtValidationContext<T> context,");
    out.println(
        "      final Class<" + TypeUtils.getClassNameWithProperties(typeMirror) + "> beanType,");
    out.println("      final String propertyName,");
    out.println("      final Object value,");
    out.println("      final Set<ConstraintViolation<T>> violations,");
    out.println("      final Group... groups) {");

    writeExpandDefaultAndValidate(out, typeMirror, Stage.VALUE); // NOPMD

    // }
    out.println("  }");
  }

  private void writeValidatePropertyGroups(final PrintWriter out, final TypeMirror typeMirror)
      throws UnexpectedTypeException {
    // public <T> void validatePropertyGroups(
    out.println("  public <T> void validatePropertyGroups(");

    // GwtValidationContext<T> context, BeanType object, String propertyName,
    // Set<ConstraintViolation<T>> violations, Class<?>... groups) throws ValidationException {
    out.println("      final GwtValidationContext<T> context,");
    out.print("      final ");
    out.println(TypeUtils.getClassNameWithProperties(typeMirror) + " object,");
    out.println("      final String propertyName,");
    out.println("      final Set<ConstraintViolation<T>> violations,");
    out.println("      final Class<?>... groups) throws ValidationException {");

    out.println("    switch (propertyName) {");

    for (final TypeElementPropertyDescriptor property : TypeUtils
        .getTypeElementProperties(typeMirror, processingEnv)) {
      // case "myPropety":
      out.print("      case \"");
      out.print(property.getPropertyName());
      out.println("\":");

      writeValidatePropertyCall(out, typeMirror, property, false, false, "        ");

      // validate all super classes and interfaces
      writeValidateInheritance(out, typeMirror, Stage.PROPERTY, // NOPMD
          property);

      // break;
      out.println("        break;");
    }

    writeDefaultNameNotFound(out, typeMirror);

    out.println("    }");

    // }
    out.println("  }");
  }

  private void writeValidateValueGroups(final PrintWriter out, final TypeMirror typeMirror)
      throws UnexpectedTypeException {
    // public <T> void validateValueGroups(
    out.println("  public <T> void validateValueGroups(");

    // GwtValidationContext<T> context, Class<Author> beanType, String propertyName,
    // Object value, Set<ConstraintViolation<T>> violations, Class<?>... groups) {
    out.println("      final GwtValidationContext<T> context,");
    out.println(
        "      final Class<" + TypeUtils.getClassNameWithProperties(typeMirror) + "> beanType,");
    out.println("      final String propertyName,");
    out.println("      final Object value,");
    out.println("      final Set<ConstraintViolation<T>> violations,");
    out.println("      final Class<?>... groups) {");

    out.println("    switch (propertyName) {");

    for (final TypeElementPropertyDescriptor property : TypeUtils
        .getTypeElementProperties(typeMirror, processingEnv)) {
      // case "myPropety":
      out.print("      case \"");
      out.print(property.getPropertyName());
      out.println("\":");

      if (!isIterableOrMap(property.getElementType())) {
        writeValidatePropertyCall(out, typeMirror, property, true, false, "        ");
      }

      // validate all super classes and interfaces
      writeValidateInheritance(out, typeMirror, Stage.VALUE, property);

      out.println("        break;");
    }

    writeDefaultNameNotFound(out, typeMirror);

    out.println("    }");

    out.println("  }");
  }

  private void writeGetBeanMetadata(final PrintWriter out) {
    // public BeanMetadata getBeanMetadata() {
    out.println("  public BeanMetadata getBeanMetadata() {");

    // return beanMetadata;
    out.println("    return beanMetadata;");

    // }
    out.println("  }");
  }

  private void writeGetDescriptor(final PrintWriter out, final TypeMirror typeMirror) {
    // public GwtBeanDescriptor<beanType>
    // getConstraints(ValidationGroupsMetadata validationGroupsMetadata) {
    out.print("  public ");
    out.print("GwtBeanDescriptor<" + TypeUtils.getClassNameWithProperties(typeMirror) + "> ");
    out.println("getConstraints(");
    out.println("      final ValidationGroupsMetadata validationGroupsMetadata) {");

    // beanDescriptor.setValidationGroupsMetadata(validationGroupsMetadata);
    out.println("    beanDescriptor.setValidationGroupsMetadata(validationGroupsMetadata);");

    // return beanDescriptor;
    out.println("    return beanDescriptor;");

    out.println("  }");
  }

  private void writePropertyValidators(final PrintWriter out, final TypeMirror typeMirror)
      throws UnexpectedTypeException {
    for (final TypeElementPropertyDescriptor propertyDescriptor : TypeUtils
        .getTypeElementProperties(typeMirror, processingEnv)) {
      if (TypeUtils.hasField(typeMirror, propertyDescriptor.getPropertyName())) {
        final Optional<Element> optionalGenericFieldType =
            TypeUtils.fieldElementByName(typeMirror, propertyDescriptor.getPropertyName());
        if (optionalGenericFieldType.isPresent()) {
          writeValidatePropertyMethod(out, typeMirror, propertyDescriptor,
              optionalGenericFieldType.get().asType(), true);
        }
        out.println();
      }
      if (TypeUtils.hasGetter(typeMirror, propertyDescriptor.getPropertyName())) {
        final Optional<ExecutableElement> optionalGetter =
            TypeUtils.getGetter(typeMirror, propertyDescriptor.getPropertyName());
        if (optionalGetter.isPresent()) {
          writeValidatePropertyMethod(out, typeMirror, propertyDescriptor,
              optionalGetter.get().getReturnType(), false);
        }
        out.println();
      }
    }
  }

  private void writeValidateAllNonInheritedProperties(final PrintWriter out,
      final TypeMirror typeMirror) {
    // private <T> void validateAllNonInheritedProperties(
    out.println("  private <T> void validateAllNonInheritedProperties(");

    // GwtValidationContext<T> context, BeanType object,
    // Set<ConstraintViolation<T>> violations, Class<?>... groups) {
    out.println("      final GwtValidationContext<T> context,");
    out.print("      final ");
    out.print(TypeUtils.getClassNameWithProperties(typeMirror));
    out.println(" object,");
    out.println("      final Set<ConstraintViolation<T>> violations,");
    out.println("      final Class<?>... groups) {");

    if (TypeUtils.isBeanConstrained(typeMirror, processingEnv.getTypeUtils(),
        processingEnv.getElementUtils()) && generateReflectionGetter
        && CollectionUtils.isNotEmpty(detectAnnotationStringParameters(typeMirror))) {
      out.print("    ");
      out.println("de.knightsoftnet.validators.client.GwtReflectorMap.put(object.getClass(),");
      out.println("        generateReflectionGetterReplacementMap(object));");
    }

    for (final TypeElementPropertyDescriptor p : TypeUtils.getTypeElementProperties(typeMirror,
        processingEnv)) {
      writeValidatePropertyCall(out, typeMirror, p, false, true, "    ");
    }

    out.println("  }");
  }

  private void writeValidatePropertyMethod(final PrintWriter out, final TypeMirror typeMirror,
      final TypeElementPropertyDescriptor ppropertyDescription, final TypeMirror pelementType,
      final boolean useField) throws UnexpectedTypeException {
    if (!isPropertyConstrained(ppropertyDescription, typeMirror, useField)
        && !TypeUtils.isGetter(ppropertyDescription.getPropertyName())) {
      return;
    }
    final TypeMirror elementClassType = ppropertyDescription.getElementType();

    // private final <T> void validateProperty_{get}<p>(
    out.print("  private final <T> void ");
    if (useField) {
      out.print(validateMethodFieldName(ppropertyDescription));
    } else {
      out.print(validateMethodGetterName(ppropertyDescription));
    }
    out.println("(");

    // final GwtValidationContext<T> context,
    out.println("      final GwtValidationContext<T> context,");

    // final Set<ConstraintViolation<T>> violations,
    out.println("      final Set<ConstraintViolation<T>> violations,");

    // BeanType object,
    out.print("      final ");
    out.println(TypeUtils.getClassNameWithProperties(typeMirror) + " object,");

    // final <Type> value,
    out.print("      final ");
    out.print(TypeUtils.getClassNameWithProperties(TypeUtils
        .fieldType(typeMirror, ppropertyDescription.getPropertyName(), processingEnv).get()));
    out.println(" value,");

    // boolean honorValid,
    out.println("      final boolean honorValid,");

    // Class<?>... groups) {
    out.println("      final Class<?>... groups) {");

    // context = context.append("myProperty");
    out.print("    final GwtValidationContext<T> myContext = context.append(\"");
    out.print(TypeUtils.getElementName(ppropertyDescription));
    out.println("\");");

    // only check this property if the TraversableResolver says we can

    // Node leafNode = myContext.getPath().getLeafNode();
    out.println("    final Node leafNode = myContext.getPath().getLeafNode();");
    // PathImpl path = PathImpl.createCopyWithoutLeafNode(myContext.getPath());
    out.println(
        "    final PathImpl path = PathImpl.createCopyWithoutLeafNode(myContext.getPath());");
    // boolean isReachable;
    out.println("    boolean isReachable;");
    // try {
    out.println("    try {");
    // isReachable = myContext.getTraversableResolver().isReachable(object, leafNode,
    // myContext.getRootBeanClass(), path, ElementType);
    out.println("      isReachable = myContext.getTraversableResolver().isReachable(object, "
        + "leafNode,");
    out.println("          myContext.getRootBeanClass(), path, "
        + (useField ? asLiteral(ElementType.FIELD, null, processingEnv)
            : asLiteral(ElementType.METHOD, null, processingEnv))
        + ");");
    // } catch (Exception e) {
    out.println("    } catch (Exception e) {");
    // throw new ValidationException("TraversableResolver isReachable caused an exception", e);
    out.println("      throw new ValidationException(\"TraversableResolver isReachable caused an "
        + "exception\", e);");
    // }
    out.println("    }");
    // if (isReachable) {
    out.println("    if (isReachable) {");

    // TODO(nchalko) move this out of here to the Validate method
    if (ppropertyDescription.isCascaded() && hasValid(ppropertyDescription, typeMirror, useField)) {

      // if (honorValid && value != null) {
      out.println("      if (honorValid && value != null) {");
      // boolean isCascadable;
      out.println("        boolean isCascadable;");
      // try {
      out.println("        try {");
      // isCascadable = myContext.getTraversableResolver().isCascadable(object, leafNode,
      // myContext.getRootBeanClass(), path, ElementType)
      out.println("          isCascadable = myContext.getTraversableResolver().isCascadable("
          + "object, leafNode,");
      out.println("              myContext.getRootBeanClass(), path, "
          + (useField ? asLiteral(ElementType.FIELD, null, processingEnv)
              : asLiteral(ElementType.METHOD, null, processingEnv))
          + ");");
      // } catch (Exception e) {
      out.println("        } catch (Exception e) {");
      // throw new ValidationException("TraversableResolver isReachable caused an exception", e);
      out.println(
          "          throw new ValidationException(\"TraversableResolver isCascadable caused an "
              + "exception\", e);");
      // }
      out.println("        }");
      // if (isCascadable) {
      out.println("        if (isCascadable) {");

      if (isIterableOrMap(elementClassType)) {
        // final JClassType associationType =
        // beanHelper.getAssociationType(ppropertyDescription, useField);
        // final BeanHelper beanHelper = createBeanHelper(associationType);
        if (isMap(elementClassType)) {
          writeValidateMap(out, ppropertyDescription, elementClassType);
        } else {
          writeValidateIterable(out, ppropertyDescription, elementClassType);
        }
      } else {
        final Optional<TypeMirror> elementType =
            TypeUtils.fieldType(typeMirror, ppropertyDescription.getPropertyName(), processingEnv);
        if (elementType.isPresent()) {
          additionalInterfacesToCreate.add(elementType.get());
        }
        // additionalInterfacesToCreate.add(elementClass);

        // if (!context.alreadyValidated(value)) {
        out.println("          if (!context.alreadyValidated(value)) {");

        // violations.addAll(myContext.getValidator().validate(context, value,
        // groups));
        out.print("            violations.addAll(");
        out.print(TypeUtils.getValidationInstanceForType(elementClassType,
            processingEnv.getTypeUtils(), processingEnv.getElementUtils()));
        out.println(".validate(");
        out.print("                myContext, (");
        out.print(TypeUtils.getClassNameWithProperties(elementClassType));
        out.println(") value, groups));");

        // }
        out.println("          }");
      }

      // }
      out.println("        }");
      // }
      out.println("      }");
    }

    // It is possible for an annotation with the exact same values to be set on
    // both the field and the getter.
    // Keep track of the ones we have used to make sure we don't duplicate.
    final Set<Object> includedAnnotations = Sets.newHashSet();
    int count = 0;
    for (final TypeElementConstraintDescriptor<?> constraint : ppropertyDescription
        .getConstraintDescriptors()) {
      if (areConstraintDescriptorGroupsValid(constraint)) {
        final Object annotation = constraint.getAnnotation();
        if (hasMatchingAnnotation(ppropertyDescription, typeMirror, useField, constraint)) {
          final String constraintDescriptorVar =
              constraintDescriptorVar(ppropertyDescription.getPropertyName(), count);
          if (includedAnnotations.contains(annotation)) {
            // The annotation has been looked at once already during this validate property call
            // so we know the field and the getter are both annotated with the same constraint.
            if (!useField) {
              writeValidateConstraint(out, ppropertyDescription, elementClassType, pelementType,
                  constraint, constraintDescriptorVar);
            }
          } else {
            if (useField) {
              writeValidateConstraint(out, ppropertyDescription, elementClassType, pelementType,
                  constraint, constraintDescriptorVar);
            } else {
              // The annotation hasn't been looked at twice (yet) and we are validating a getter
              // Write the call if only the getter has this constraint applied to it
              final boolean hasField =
                  TypeUtils.hasField(typeMirror, ppropertyDescription.getPropertyName());
              if (!hasField
                  || !hasMatchingAnnotation(ppropertyDescription, typeMirror, true, constraint)) {
                writeValidateConstraint(out, ppropertyDescription, elementClassType, pelementType,
                    constraint, constraintDescriptorVar);
              }
            }
          }
          includedAnnotations.add(annotation);
        }
        count++;
      }
    }
    // }
    out.println("    }");
    out.println("  }");
  }

  private void writeValidateIterable(final PrintWriter out,
      final TypeElementPropertyDescriptor propertyDescription, final TypeMirror elementClassType) {
    final Optional<? extends TypeMirror> optionalTypeParameter =
        ((DeclaredType) elementClassType).getTypeArguments().stream().findFirst();
    final String typeParameter;
    if (optionalTypeParameter.isPresent()) {
      typeParameter = TypeUtils.getClassName(optionalTypeParameter.get());
      additionalInterfacesToCreate.add(optionalTypeParameter.get());
    } else {
      typeParameter = "Object";
    }

    // int i = 0;
    out.println("          int i = 0;");

    // for (Object instance : value) {
    out.println("          for(final " + typeParameter + " instance : value) {");

    // if(instance != null && !context.alreadyValidated(instance)) {
    out.println("            if (instance != null  && !context.alreadyValidated(instance)) {");

    if (optionalTypeParameter.isPresent()) {
      // violations.addAll(
      out.println("              violations.addAll(");

      // context.getValidator().validate(
      out.print("                  ");
      out.print(TypeUtils.getValidationInstanceForType(optionalTypeParameter.get(),
          processingEnv.getTypeUtils(), processingEnv.getElementUtils()));
      out.println(".validate(");

      // final Class<?> elementClass = ppropertyDescription.getElementClass();
      if (elementClassType.getKind() == TypeKind.ARRAY || isList(elementClassType)) {
        // context.appendIndex("myProperty",i++),
        out.print("                      ");
        out.print("context.appendIndex(\"");
        out.print(propertyDescription.getPropertyName());
        out.println("\",i),");
      } else {
        // context.appendIterable("myProperty"),
        out.print("                      ");
        out.print("context.appendIterable(\"");
        out.print(propertyDescription.getPropertyName());
        out.println("\"),");
      }

      // instance, groups));
      out.print("                      ");
      out.println(" (" + typeParameter + ") instance, groups));");

      // }
      out.print("            ");
      out.println("}");
    }
    // i++;
    out.print("            ");
    out.println("i++;");

    // }
    out.print("          ");
    out.println("}");
  }

  private void writeValidateMap(final PrintWriter out,
      final TypeElementPropertyDescriptor propertyDescription, final TypeMirror typeMirror) {
    // for (Entry<?, Type> entry : value.entrySet()) {
    out.print("          for(");
    out.print(Entry.class.getCanonicalName());
    out.println("<?, ?> entry : value.entrySet()) {");

    // if(entry.getValue() != null &&
    // !context.alreadyValidated(entry.getValue())) {
    out.print("            ");
    out.println("if (entry.getValue() != null && !context.alreadyValidated(entry.getValue())) {");

    // violations.addAll(
    out.println("              violations.addAll(");

    // context.getValidator().validate(
    out.print("              ");
    out.print(TypeUtils.getValidationInstanceForType(typeMirror, processingEnv.getTypeUtils(),
        processingEnv.getElementUtils()));
    out.println(".validate(");

    // context.appendKey("myProperty",entry.getKey()),
    out.print("              ");
    out.print("context.appendKey(\"");
    out.print(propertyDescription.getPropertyName());
    out.println("\",entry.getKey()),");

    // entry.getValue(), groups));
    out.print("                  ");
    out.print("(");
    out.print(TypeUtils.getClassNameWithProperties(typeMirror));
    out.println(") entry.getValue(), groups));");

    // }
    out.println("            }");

    // }
    out.println("          }");
  }

  private void writeValidateConstraint(final PrintWriter out,
      final TypeElementPropertyDescriptor propertyDescription, final TypeMirror elementClassType,
      final TypeMirror elementType, final TypeElementConstraintDescriptor<?> constraint,
      final String constraintDescriptorVar) throws UnexpectedTypeException {
    writeValidateConstraint(out, propertyDescription, elementClassType, elementType, constraint,
        constraintDescriptorVar, DEFAULT_VIOLATION_VAR, "      ", true);
  }

  /**
   * Writes the call to actually validate a constraint, including its composite constraints.
   * <p>
   * If the constraint is annotated as {@link jakarta.validation.ReportAsSingleViolation
   * ReportAsSingleViolation}, then is called recursively and the {@code violationsVar} is changed
   * to match the {@code constraintDescriptorVar}.
   * </p>
   *
   * @param out the Print Writer
   * @param propertyDescription the property
   * @param elementClass The class of the Element
   * @param elementType the type of the element
   * @param constraint the constraint to validate.
   * @param constraintDescriptorVar the name of the constraintDescriptor variable.
   * @param violationsVar the name of the variable to hold violations
   * @throws UnexpectedTypeException when validation can not be completed
   */
  private void writeValidateConstraint(final PrintWriter out,
      final TypeElementPropertyDescriptor propertyDescription, final TypeMirror elementClass,
      final TypeMirror elementType, final TypeElementConstraintDescriptor<?> constraint,
      final String constraintDescriptorVar, final String violationsVar, final String indentPrefix,
      final boolean indentFirst) throws UnexpectedTypeException {
    final boolean isComposite = !constraint.getComposingConstraints().isEmpty();
    final boolean firstReportAsSingleViolation = constraint.isReportAsSingleViolation()
        && violationsVar.equals(DEFAULT_VIOLATION_VAR) && isComposite;
    final boolean reportAsSingleViolation =
        firstReportAsSingleViolation || !violationsVar.equals(DEFAULT_VIOLATION_VAR);
    final boolean hasValidator = !constraint.getConstraintValidatorClasses().isEmpty();
    final String compositeViolationsVar = constraintDescriptorVar + "_violations";
    String workIndentPrefix = indentPrefix;

    // Only do this the first time in a constraint composition.
    if (firstReportAsSingleViolation) {
      // Report myConstraint as Single Violation
      out.print(workIndentPrefix);
      out.print("// Report ");
      out.print(
          TypeUtils.getClassNameWithProperties(constraint.getAnnotation().getAnnotationType()));
      out.println(" as Single Violation");
      writeNewViolations(out, compositeViolationsVar);
    }

    if (hasValidator) {
      final TypeMirror validatorClass = getValidatorForType(constraint, elementType);

      if (firstReportAsSingleViolation) {
        // if (!
        out.print(workIndentPrefix);
        out.print("if (!");
        workIndentPrefix = indentPrefix + "  ";
      }

      // validate(myContext, violations object, value, new MyValidator(),
      // constraintDescriptor, groups));
      if (!firstReportAsSingleViolation && indentFirst) {
        out.print(workIndentPrefix);
      }
      out.print("validate(myContext, ");
      out.print(violationsVar);
      out.println(", object, value,");
      out.print(workIndentPrefix);
      out.print("    new ");
      out.print(TypeUtils.getClassNameWithProperties(validatorClass));
      out.println("(),");
      out.print(workIndentPrefix);
      out.print("    ");
      out.print(constraintDescriptorVar);
      out.print(", groups)");
      if (firstReportAsSingleViolation) {
        // ) {
        out.println(") {");

      } else if (reportAsSingleViolation) {
        if (isComposite) {
          // ||
          out.println(" ||");
        }
      } else {
        // ;
        out.println(";");
      }
    } else if (!isComposite) {
      // TODO(nchalko) What does the spec say to do here.
      processingEnv.getMessager().printMessage(Diagnostic.Kind.WARNING,
          "No ConstraintValidator of " + constraint + " for "
              + propertyDescription.getPropertyName() + " of type " + elementClass);
    }

    if (firstReportAsSingleViolation) {
      // if (
      out.print(workIndentPrefix);
      out.print("if (");
    }
    int count = 0;

    for (final TypeElementConstraintDescriptor<?> compositeConstraint : constraint
        .getComposingConstraints()) {
      out.print(workIndentPrefix);
      final String compositeVar = constraintDescriptorVar + "_" + count++;
      writeValidateConstraint(out, propertyDescription, elementClass, elementType,
          compositeConstraint, compositeVar,
          firstReportAsSingleViolation ? compositeViolationsVar : violationsVar, workIndentPrefix,
          count > 1);
      if (reportAsSingleViolation) {
        // ||
        out.println(" ||");
      } else {
        // ;
        out.println(";");
      }
    }
    if (isComposite && reportAsSingleViolation) {
      // false
      out.print(workIndentPrefix);
      out.print("    false");
    }
    if (firstReportAsSingleViolation) {
      // ) {
      out.println(") {");

      // addSingleViolation(myContext, violations, object, value,
      // constraintDescriptor);
      out.print(workIndentPrefix);
      out.print("  ");
      out.print("addSingleViolation(myContext, violations, object, value, ");
      out.print(constraintDescriptorVar);
      out.println(");");

      // }
      out.print(workIndentPrefix);
      out.println("}");

      if (hasValidator) {
        // }
        out.print(indentPrefix);
        out.println("}");
      }
    }
  }

  private void writeNewViolations(final PrintWriter out, final String violationName) {
    // Set<ConstraintViolation<T>> violations =
    out.print("      Set<ConstraintViolation<T>> ");
    out.print(violationName);
    out.println(" = ");

    // new HashSet<>();
    out.println("          new HashSet<>();");
  }

  private boolean hasValid(final TypeElementPropertyDescriptor propertyDescription,
      final TypeMirror typeMirror, final boolean useField) {
    return getAnnotation(propertyDescription, typeMirror, useField, (DeclaredType) processingEnv
        .getElementUtils().getTypeElement("jakarta.validation.Valid").asType()).isPresent();
  }

  private Optional<? extends AnnotationMirror> getAnnotation(
      final TypeElementPropertyDescriptor propertyDescription, final TypeMirror typeMirror,
      final boolean useField, final DeclaredType expectedAnnotationClass) {
    Optional<? extends AnnotationMirror> annotation = Optional.empty();
    if (useField) {
      final Optional<Element> element =
          TypeUtils.fieldElementByName(typeMirror, propertyDescription.getPropertyName());
      if (element.isPresent()) {
        annotation = element.get().getAnnotationMirrors().stream()
            .filter(anno -> TypeUtils.getClassName(anno.getAnnotationType())
                .equals(TypeUtils.getClassName(expectedAnnotationClass)))
            .findFirst();
      }
    } else {
      final Optional<ExecutableElement> element =
          TypeUtils.getGetter(typeMirror, propertyDescription.getPropertyName());
      if (element.isPresent()) {
        annotation = element.get().getAnnotationMirrors().stream()
            .filter(anno -> TypeUtils.getClassName(anno.getAnnotationType())
                .equals(TypeUtils.getClassName(expectedAnnotationClass)))
            .findFirst();
      }
    }
    return annotation;
  }

  private List<? extends AnnotationMirror> getAnnotations(
      final TypeElementPropertyDescriptor propertyDescription, final TypeMirror typeMirror,
      final boolean useField) {
    if (useField) {
      final Optional<Element> optionalField =
          TypeUtils.fieldElementByName(typeMirror, propertyDescription.getPropertyName());
      if (optionalField.isPresent()) {
        return optionalField.get().getAnnotationMirrors();
      }
    } else {
      final Optional<ExecutableElement> optionalMethod =
          TypeUtils.getGetter(typeMirror, propertyDescription.getPropertyName());
      if (optionalMethod.isPresent()) {
        return optionalMethod.get().getAnnotationMirrors();
      }
    }
    return Collections.emptyList();
  }

  private boolean isPropertyConstrained(final TypeElementPropertyDescriptor propertyDescription,
      final TypeMirror typeMirror, final boolean useField) {
    // cascaded counts as constrained
    // we must know if the @Valid annotation is on a field or a getter
    if (hasValid(propertyDescription, typeMirror, useField)) {
      return true;
    }
    // for non-cascaded properties
    for (final TypeElementConstraintDescriptor<?> constraint : propertyDescription
        .getConstraintDescriptors()) {
      if (constraint.getConstraintLocationKind()
          .getElementType() == (useField ? ElementType.FIELD : ElementType.METHOD)) {
        return true;
      }
    }
    return false;
  }

  private void writeDefaultNameNotFound(final PrintWriter out, final TypeMirror typeMirror) {
    out.println("      default:");

    // if (!ALL_PROPERTY_NAMES.contains(propertyName)) {
    out.println("        if (!ALL_PROPERTY_NAMES.contains(propertyName)) {");

    // throw new IllegalArgumentException(propertyName
    // +"is not a valid property of myClass");
    out.print("          throw new IllegalArgumentException");
    out.println("(propertyName + \" is not a valid property of \"");
    out.print("              + \"");
    out.print(TypeUtils.getClassNameWithProperties(typeMirror));
    out.println("\");");

    out.println("        }");

    out.println("        break;");
  }

  private void writeValidatePropertyCall(final PrintWriter out, final TypeMirror typeMirror,
      final TypeElementPropertyDescriptor property, final boolean useValue,
      final boolean honorValid, final String indentPrefix) {

    if (TypeUtils.hasGetter(typeMirror, property.getPropertyName())
        && isPropertyConstrained(property, typeMirror, false)
        || TypeUtils.isGetter(property.getPropertyName())) {
      if (useValue) {
        // if (value == null || value instanceof propertyType) {
        out.print(indentPrefix);
        out.print("if (value == null || value instanceof ");
        out.print(getQualifiedSourceNonPrimitiveType(
            TypeUtils.getterReturnType(typeMirror, property.getPropertyName())));
        out.println(") {");

        // validate_getMyProperty
        writeValidateGetterCall(out, typeMirror, property, useValue, honorValid,
            "  " + indentPrefix);

        // }
        out.print(indentPrefix);
        out.print("}");
      } else {
        // validate_getMyProperty
        writeValidateGetterCall(out, typeMirror, property, useValue, honorValid, indentPrefix);
      }
    }

    if (TypeUtils.hasField(typeMirror, property.getPropertyName())
        && isPropertyConstrained(property, typeMirror, true)
        && !TypeUtils.isGetter(property.getPropertyName())) {
      if (useValue) {
        // if (value == null || value instanceof propertyType) {
        out.print(indentPrefix);
        out.print("if (value == null || value instanceof ");
        out.print(getQualifiedSourceNonPrimitiveType(
            TypeUtils.fieldType(typeMirror, property.getPropertyName(), processingEnv)));
        out.println(") {");

        // validate_myProperty
        writeValidateFieldCall(out, typeMirror, property, useValue, honorValid,
            "  " + indentPrefix);

        // } else
        out.print(indentPrefix);
        out.print("}");
      } else {
        // validate_myProperty
        writeValidateFieldCall(out, typeMirror, property, useValue, honorValid, indentPrefix);
      }
    }

    if (useValue) {
      if (TypeUtils.isGetter(property.getPropertyName())
          || TypeUtils.hasGetter(typeMirror, property.getPropertyName())
          || TypeUtils.hasField(typeMirror, property.getPropertyName())) {
        out.println(" else {");

        // throw new ValidationException(value.getClass +
        // " is not a valid type for " + propertyName);
        out.print(indentPrefix);
        out.print("  throw new ValidationException");
        out.println("(value.getClass() + \" is not a valid type for \" + propertyName);");

        // }
        out.print(indentPrefix);
        out.println("}");
      } else {
        out.println();
      }
    }
  }

  private void writeValidateFieldCall(final PrintWriter out, final TypeMirror typeMirror,
      final TypeElementPropertyDescriptor propertyDescription, final boolean useValue,
      final boolean honorValid, final String indentPrefix) {
    if (!isPropertyConstrained(propertyDescription, typeMirror, true)) {
      return;
    }
    final String propertyName = propertyDescription.getPropertyName();

    // validateProperty_<<field>>(context,
    out.print(indentPrefix);
    out.print(validateMethodFieldName(propertyDescription));
    out.print("(context, ");
    out.print("violations, ");

    // null, (MyType) value,
    // or
    // object, object.getLastName(),
    if (useValue) {
      out.println("null,");
      out.print(indentPrefix);
      out.print("    ");
      out.print("(");
      out.print(getQualifiedSourceNonPrimitiveType(
          TypeUtils.fieldType(typeMirror, propertyName, processingEnv)));
      out.print(") value");
    } else {
      out.println("object,");
      out.print(indentPrefix);
      out.print("    ");
      final Optional<Element> field = TypeUtils.fieldElementByName(typeMirror, propertyName);
      if (!field.isPresent()) {
        out.print("null");
      } else if (field.get().getModifiers().contains(Modifier.PUBLIC)) {
        out.print("object.");
        out.print(propertyName);
      } else if (forceUsingGetter) {
        final Optional<ExecutableElement> getter = TypeUtils.getGetter(typeMirror, propertyName);
        if (getter.isPresent()) {
          out.print("object.");
          out.print(getter.get().getSimpleName());
          out.print("()");
        } else {
          out.print("null");
        }
      } else {
        fieldsToWrap.add(field.get());
        out.print(toWrapperName(field.get()) + "(object)");
      }
    }
    out.print(", ");

    // honorValid, groups);
    out.print(Boolean.toString(honorValid));
    out.println(", groups);");
  }

  private void writeValidateGetterCall(final PrintWriter out, final TypeMirror typeMirror,
      final TypeElementPropertyDescriptor propertyDescription, final boolean useValue,
      final boolean honorValid, final String indentPrefix) {
    if (!isPropertyConstrained(propertyDescription, typeMirror, false)
        && !TypeUtils.isGetter(propertyDescription.getPropertyName())) {
      return;
    }
    // validateProperty_get<<field>>(context, violations,
    out.print(indentPrefix);
    out.print(validateMethodGetterName(propertyDescription));
    out.print("(context, ");
    out.print("violations, ");

    // object, object.getMyProp(),
    // or
    // null, (MyType) value,
    if (useValue) {
      out.println("null,");
      out.print(indentPrefix);
      out.print("    (");
      out.print(TypeUtils.getClassNameWithProperties(
          TypeUtils.getterReturnType(typeMirror, propertyDescription.getPropertyName()).get()));
      out.print(") value");
    } else {
      out.println("object,");
      out.print(indentPrefix);
      out.print("    ");
      final Optional<ExecutableElement> method =
          TypeUtils.getGetter(typeMirror, propertyDescription.getPropertyName());
      if (!method.isPresent()) {
        out.print("null");
      } else if (method.get().getModifiers().contains(Modifier.PUBLIC)) {
        out.print("object.");
        out.print(method.get().getSimpleName());
        out.print("()");
      } else if (forceUsingGetter) {
        out.print("null");
      } else {
        gettersToWrap.add(method.get());
        out.print(toWrapperName(method.get()) + "(object)");
      }
    }
    out.print(", ");

    // honorValid, groups);
    out.print(Boolean.toString(honorValid));
    out.println(", groups);");
  }

  private String toWrapperName(final ExecutableElement field) {
    return "_" + field.getSimpleName();
  }

  private String toWrapperName(final Element method) {
    return "_" + method.getSimpleName();
  }

  private String validateMethodFieldName(final TypeElementPropertyDescriptor propertyDescription) {
    return "validateProperty_" + propertyDescription.getPropertyName();
  }

  private String validateMethodGetterName(final TypeElementPropertyDescriptor propertyDescription) {
    if (TypeUtils.isGetter(propertyDescription.getPropertyName())) {
      return "validateProperty_" + propertyDescription.getPropertyName();
    }
    return "validateProperty_get" + propertyDescription.getPropertyName();
  }

  private String getQualifiedSourceNonPrimitiveType(final Optional<TypeMirror> elementType) {
    if (elementType.isPresent()) {
      return switch (elementType.get().getKind()) {
        case BOOLEAN -> "Boolean";
        case BYTE -> "Byte";
        case SHORT -> "Short";
        case INT -> "Integer";
        case LONG -> "Long";
        case CHAR -> "Char";
        case FLOAT -> "Float";
        case DOUBLE -> "Double";
        default -> TypeUtils.getClassName(elementType.get());
      };
    }
    return null;
  }

  private void writeExpandDefaultAndValidate(final PrintWriter out, final TypeMirror typeMirror,
      final Stage stage) throws UnexpectedTypeException {

    // ArrayList<Class<?>> justGroups = new ArrayList<>();
    out.println("    final ArrayList<Class<?>> justGroups = new ArrayList<>();");

    // for (Group g : groups) {
    out.println("    for (Group g : groups) {");
    // if (!g.isDefaultGroup() || !getBeanMetadata().defaultGroupSequenceIsRedefined()) {
    out.println(
        "      if (!g.isDefaultGroup() || !getBeanMetadata().defaultGroupSequenceIsRedefined()) {");
    // justGroups.add(g.getGroup());
    out.println("        justGroups.add(g.getGroup());");
    // }
    out.println("      }");
    // }
    out.println("    }");

    // Class<?>[] justGroupsArray = justGroups.toArray(new Class<?>[justGroups.size()]);
    out.println("    final Class<?>[] justGroupsArray = "
        + "justGroups.toArray(new Class<?>[justGroups.size()]);");

    switch (stage) {
      case OBJECT:
        // validateAllNonInheritedProperties(context, object, violations, justGroupsArray);
        out.println("    validateAllNonInheritedProperties(context, object, violations, "
            + "justGroupsArray);");
        writeClassLevelConstraintsValidation(out, typeMirror, "justGroupsArray", "    ");
        break;
      case PROPERTY:
        // validatePropertyGroups(context, object, propertyName, violations, justGroupsArray);
        out.println("    validatePropertyGroups(context, object, propertyName, violations, "
            + "justGroupsArray);");
        break;
      case VALUE:
        // validateValueGroups(context, beanType, propertyName, value, violations,
        // justGroupsArray);
        out.println("    validateValueGroups(context, beanType, propertyName, value, violations, "
            + "justGroupsArray);");
        break;
      default:
        throw new IllegalStateException();
    }

    // if (getBeanMetadata().defaultGroupSequenceIsRedefined()) {
    out.println("    if (getBeanMetadata().defaultGroupSequenceIsRedefined()) {");
    // for (Class<?> g : beanMetadata.getDefaultGroupSequence()) {
    out.println("      for (Class<?> g : beanMetadata.getDefaultGroupSequence()) {");
    // int numberOfViolations = violations.size();
    out.println("        int numberOfViolations = violations.size();");

    switch (stage) {
      case OBJECT:
        // validateAllNonInheritedProperties(context, object, violations, g);
        out.println("        validateAllNonInheritedProperties(context, object, violations, g);");
        writeClassLevelConstraintsValidation(out, typeMirror, "g", "        ");
        // validate super classes and super interfaces
        writeValidateInheritance(out, typeMirror, Stage.OBJECT, null, false, "g"); // NOPMD
        break;
      case PROPERTY:
        // validatePropertyGroups(context, object, propertyName, violations, g);
        out.println(
            "        validatePropertyGroups(context, object, propertyName, violations, g);");
        break;
      case VALUE:
        // validateValueGroups(context, beanType, propertyName, value, violations, g);
        out.println(
            "        validateValueGroups(context, beanType, propertyName, value, violations, g);");
        break;
      default:
        throw new IllegalStateException();
    }

    // if (violations.size() > numberOfViolations) {
    out.println("        if (violations.size() > numberOfViolations) {");
    // break;
    out.println("          break;");
    // }
    out.println("        }");
    // }
    out.println("      }");
    if (stage == Stage.OBJECT && typeMirror != null) {
      final TypeElement typeElement =
          processingEnv.getElementUtils().getTypeElement(TypeUtils.getClassName(typeMirror));
      if (typeElement != null && CollectionUtils.isNotEmpty(typeElement.getInterfaces())) {
        // else {
        out.println("    } else {");

        // validate super classes and super interfaces
        writeValidateInheritance(out, typeMirror, Stage.OBJECT, null, true, "groups"); // NOPMD
      }
    }
    // }
    out.println("    }");
  }

  private void writeBeanMetadata(final PrintWriter out, final TypeMirror typeMirror) {
    // private final BeanMetadata beanMetadata =
    out.println("  private final BeanMetadata beanMetadata =");

    // new BeanMetadata(
    out.println("      new BeanMetadata(");

    // <<bean class>>, <<default group seq class 1>>, <<default group seq class 2>>, ...
    out.print("          ");
    out.print(TypeUtils.getClassName(typeMirror));
    out.print(".class");
    final GroupSequence groupSeqAnnotation = typeMirror.getAnnotation(GroupSequence.class);
    final List<String> groupSequence = new ArrayList<>();
    if (groupSeqAnnotation == null) {
      groupSequence.add(TypeUtils.getClassName(typeMirror));
    } else {
      groupSequence.addAll(Stream.of(groupSeqAnnotation.value()).map(Class::getName).toList());
    }
    boolean groupSequenceContainsDefault = false;
    for (final String group : groupSequence) {
      out.println(",");
      if (group.equals(TypeUtils.getClassName(typeMirror))) {
        out.print("          ");
        out.print(asLiteral(Default.class, typeMirror, processingEnv));
        groupSequenceContainsDefault = true;
      } else if (group.equals(Default.class.getName())) {
        processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR,
            "'Default.class' cannot appear in default group sequence list.");
      } else {
        out.print("          ");
        out.print(asLiteral(group, typeMirror, processingEnv));
      }
    }
    if (!groupSequenceContainsDefault) {
      processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR,
          TypeUtils.getClassNameWithProperties(typeMirror)
              + " must be part of the redefined default group sequence.");
    }

    out.println(");");
  }

  @SuppressWarnings("deprecation")
  private void writeConstraintDescriptor(final PrintWriter out,
      final TypeElementConstraintDescriptor<? extends AnnotationMirror> constraint,
      final ElementType elementType, final ConstraintOrigin origin,
      final String constraintDescripotorVar,
      final TypeElementPropertyDescriptor propertyDescription) throws UnexpectedTypeException {
    final DeclaredType annotationType = constraint.getAnnotation().getAnnotationType();

    // First list all composing constraints
    int count = 0;
    for (final TypeElementConstraintDescriptor<?> composingConstraint : constraint
        .getComposingConstraints()) {
      writeConstraintDescriptor(out, composingConstraint, elementType, origin,
          constraintDescripotorVar + "_" + count++, propertyDescription);
    }

    // private final ConstraintDescriptorImpl<MyAnnotation> constraintDescriptor = ;
    out.print("  private final ConstraintDescriptorImpl<");
    out.print(TypeUtils.getClassNameWithProperties(annotationType));
    out.print("> ");
    out.println(constraintDescripotorVar + "  = ");

    // ConstraintDescriptorImpl.<MyConstraint> builder()
    out.print("      ConstraintDescriptorImpl.<");
    out.print(TypeUtils.getClassNameWithProperties(annotationType));
    out.println("> builder()");

    // .setAnnotation(new MyAnnotation )
    out.println("          .setAnnotation(");
    writeNewAnnotation(out, constraint);
    out.println("              )");

    // .setAttributes(builder()
    out.println("          .setAttributes(attributeBuilder()");

    for (final Entry<String, Entry<? extends ExecutableElement, //
        ? extends AnnotationValue>> entry : constraint.getAttributes().entrySet()) {
      // .put(key, value)
      out.print("            .put(");
      final String key = entry.getKey();
      out.print(asLiteral(key, null, processingEnv));
      out.print(", ");
      Object value = entry.getValue().getValue().getValue();
      // Add the Default group if it is not already present
      if ("groups".equals(key) && value instanceof final List<?> list && list.isEmpty()) {
        value = new Class[] {Default.class};
      }
      out.print(asLiteral(value, entry.getValue().getKey().getReturnType(), processingEnv));
      out.println(")");
      if (StringUtils.startsWith(key, "message")) {
        out.print("            .put(\"");
        out.print(key);
        out.print("Localized");
        out.println("\",");
        out.println("                Stream.of(new String[][] {");
        final Map<String, String> messages =
            ValidationMessagesMap
                .getMessages(
                    StringUtils.removeEnd(
                        StringUtils.removeStart(asLiteral(value,
                            entry.getValue().getKey().getReturnType(), processingEnv), "\"{"),
                        "}\""));
        if (messages != null) {
          messages.entrySet().forEach(messageEntry -> {
            out.print("                  {\"");
            out.print(StringEscapeUtils.escapeJava(messageEntry.getKey()));
            out.print("\", \"");
            out.print(StringEscapeUtils.escapeJava(messageEntry.getValue()));
            out.println("\"},");
          });
        }
        out.println(
            "                }).collect(Collectors.toMap(data -> data[0], data -> data[1]))");
        out.println("            )");
      }
    }

    // .build())
    out.println("            .build())");

    // .setConstraintValidatorClasses(classes )
    out.print("          .setConstraintValidatorClasses(");
    try {
      out.print(
          asLiteral(List.of(getValidatorForType(constraint, propertyDescription.getElementType())),
              null, processingEnv));
    } catch (final UnexpectedTypeException e) {
      out.print(asLiteral(constraint.getConstraintValidatorClasses(), null, processingEnv));
    }
    out.println(")");

    final int ccCount = constraint.getComposingConstraints().size();
    for (int i = 0; i < ccCount; i++) {
      // .addComposingConstraint(cX_X)
      out.print("          .addComposingConstraint(");
      out.print(constraintDescripotorVar + "_" + i);
      out.println(")");
    }

    // .getGroups(groups)
    out.print("          .setGroups(");
    final Set<TypeMirror> groups = constraint.getGroups();
    out.print(asLiteral(groups, null, processingEnv));
    out.println(")");

    // .setPayload(payload)
    out.print("          .setPayload(");
    final Set<TypeElement> payload = constraint.getPayload();
    out.print(asLiteral(payload, null, processingEnv));
    out.println(")");

    // .setReportAsSingleViolation(boolean )
    out.print("          .setReportAsSingleViolation(");
    out.print(Boolean.toString(constraint.isReportAsSingleViolation()));
    out.println(")");

    // .setElementType(elementType)
    out.print("          .setElementType(");
    out.print(asLiteral(elementType, null, processingEnv));
    out.println(")");

    // .setDefinedOn(origin)
    out.print("          .setDefinedOn(");
    out.print(asLiteral(origin, null, processingEnv));
    out.println(")");

    // .build();
    out.println("          .build();");
    out.println();
  }

  private void writeNewAnnotation(final PrintWriter out,
      final TypeElementConstraintDescriptor<? extends AnnotationMirror> constraint)
      throws UnexpectedTypeException {
    final AnnotationMirror annotation = constraint.getAnnotation();
    final DeclaredType annotationType = annotation.getAnnotationType();

    // new MyAnnotation () {
    out.print("              new ");
    out.print(TypeUtils.getClassNameWithProperties(annotationType));
    out.println("() {");

    // public Class<? extends Annotation> annotationType() { return
    // MyAnnotation.class; }
    out.println("                  public Class<? extends Annotation> annotationType() {");
    out.print("                    return ");
    out.print(TypeUtils.getClassName(annotationType));
    out.println(".class;");
    out.println("                  }");

    // for (final Entry<? extends ExecutableElement, ? extends AnnotationValue> element :
    // processingEnv
    // .getElementUtils().getElementValuesWithDefaults(annotation).entrySet()) {
    for (final Entry<? extends ExecutableElement, ? extends AnnotationValue> element : constraint
        .getAttributes().values()) {
      final ExecutableElement method = element.getKey();
      if (method.getModifiers().contains(Modifier.ABSTRACT)) {
        out.print("                  public ");
        out.print(TypeUtils.getClassNameWithProperties(method.getReturnType()));
        out.print(" ");
        out.print(method.getSimpleName().toString());
        out.println("() {");
        out.print("                    return ");

        try {
          final Object value;
          if ("groups".equals(method.getSimpleName().toString())) {
            value = constraint.getGroups().stream().filter(
                group -> !"jakarta.validation.groups.Default".equals(TypeUtils.getClassName(group)))
                .collect(Collectors.toSet());
          } else {
            value = element.getValue().getValue();
          }
          out.print(asLiteral(value, method.asType(), processingEnv));
        } catch (final IllegalArgumentException e) {
          processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR, e.getMessage());
        }
        out.println(";");
        out.println("                  }");
      }
    }

    out.println("              }");
  }

  private void writePropertyDescriptor(final PrintWriter out,
      final TypeElementPropertyDescriptor propertyDescription) {
    // private final PropertyDescriptor myProperty_pd =
    out.print("  private final PropertyDescriptorImpl ");
    out.print(propertyDescription.getPropertyName());
    out.println("_pd =");

    // new PropertyDescriptorImpl(
    out.println("      new PropertyDescriptorImpl(");

    // "myProperty",
    out.println("          \"" + TypeUtils.getElementName(propertyDescription) + "\",");

    // MyType.class,
    out.print("          ");
    out.println(TypeUtils.getClassName(propertyDescription.getElementType()) + ".class,");

    // isCascaded,
    out.print("          ");
    out.print(Boolean.toString(propertyDescription.isCascaded()) + ",");

    // beanMetadata,
    out.print("beanMetadata");

    // myProperty_c0,
    // myProperty_c1 );
    int count = 0;
    for (final TypeElementConstraintDescriptor<?> constraint : propertyDescription
        .getConstraintDescriptors()) {
      if (areConstraintDescriptorGroupsValid(constraint)) {
        out.println(","); // Print the , for the previous line
        out.print("          ");
        out.print(constraintDescriptorVar(propertyDescription.getPropertyName(), count));
        count++;
      }
    }
    out.println(");");
  }

  private void writeBeanDescriptor(final PrintWriter out, final TypeMirror typeMirror) {

    // private final GwtBeanDescriptor <MyBean> beanDescriptor =
    out.print("  private final GwtBeanDescriptor<"
        + TypeUtils.getClassNameWithProperties(typeMirror) + ">");
    out.println(" beanDescriptor = ");

    // GwtBeanDescriptorImpl.builder(Order.class)
    out.print("      (GwtBeanDescriptorImpl) GwtBeanDescriptorImpl.builder(");
    out.print(TypeUtils.getClassName(typeMirror));
    out.println(".class)");

    // .setConstrained(true)
    out.println("          .setConstrained(" + TypeUtils.isBeanConstrained(typeMirror,
        processingEnv.getTypeUtils(), processingEnv.getElementUtils()) + ")");

    int count = 0;
    for (final TypeElementConstraintDescriptor<?> constraint : TypeUtils
        .getTypeElementConstraintDescriptors(typeMirror, processingEnv.getElementUtils(),
            processingEnv.getTypeUtils())) {
      if (areConstraintDescriptorGroupsValid(constraint)) {
        // .add(c0)
        out.println("          .add(" + constraintDescriptorVar("this", count) + ")");
        count++;
      }
    }

    // .put("myProperty", myProperty_pd)
    for (final TypeElementPropertyDescriptor p : TypeUtils.getTypeElementProperties(typeMirror,
        processingEnv)) {
      out.print("          .put(\"");
      out.print(p.getPropertyName());
      out.print("\", ");
      out.print(p.getPropertyName());
      out.println("_pd)");
    }

    // .setBeanMetadata(beanMetadata)
    out.println("          .setBeanMetadata(beanMetadata)");

    // .build();
    out.println("          .build();");
  }

  private void writeValidateClassGroups(final PrintWriter out, final TypeMirror typeMirror)
      throws UnexpectedTypeException {
    // public <T> void validateClassGroups(
    out.println("  public <T> void validateClassGroups(");

    // GwtValidationContext<T> context, BeanType object,
    // Set<ConstraintViolation<T>> violations, Group... groups) {
    out.println("      final GwtValidationContext<T> context,");
    out.print("      final ");
    out.println(TypeUtils.getClassNameWithProperties(typeMirror) + " object,");
    out.println("      final Set<ConstraintViolation<T>> violations,");
    out.println("      final Class<?>... groups) {");

    // /// For each group

    // TODO(nchalko) handle the sequence in the AbstractValidator

    // See JSR 303 section 3.5
    // all reachable fields
    // all reachable getters (both) at once
    // including all reachable and cascadable associations

    out.println("    validateAllNonInheritedProperties(context, object, violations, groups);");

    // validate super classes and super interfaces
    writeValidateInheritance(out, typeMirror, Stage.OBJECT, null, false, "groups"); // NOPMD

    writeClassLevelConstraintsValidation(out, typeMirror, "groups", "    ");

    // }
    out.println("  }");
  }

  private void writeValidateInheritance(final PrintWriter out, final TypeMirror typeMirror,
      final Stage stage, final TypeElementPropertyDescriptor property)
      throws UnexpectedTypeException {
    writeValidateInheritance(out, typeMirror, stage, property, false, "groups");
  }

  private void writeValidateInheritance(final PrintWriter out, final TypeMirror typeMirror,
      final Stage stage, final TypeElementPropertyDescriptor property,
      final boolean expandDefaultGroupSequence, final String groupsVarName)
      throws UnexpectedTypeException {
    writeValidateInterfaces(out, typeMirror, stage, property, expandDefaultGroupSequence,
        groupsVarName);
  }

  private void writeValidateInterfaces(final PrintWriter out, final TypeMirror typeMirror,
      final Stage stage, final TypeElementPropertyDescriptor propertyDescription,
      final boolean expandDefaultGroupSequence, final String groupsVarName)
      throws UnexpectedTypeException {
    if (typeMirror != null) {
      final TypeElement typeElement =
          processingEnv.getElementUtils().getTypeElement(TypeUtils.getClassName(typeMirror));
      if (typeElement != null) {
        for (final TypeMirror type : typeElement.getInterfaces()) {
          writeValidateInterfaces(out, type, stage, propertyDescription, expandDefaultGroupSequence,
              groupsVarName);
        }
      }
    }
  }

  private void writeClassLevelConstraintsValidation(final PrintWriter out,
      final TypeMirror typeMirror, final String groupsVarName, final String offset)
      throws UnexpectedTypeException {
    // all class level constraints
    int count = 0;
    for (final TypeElementConstraintDescriptor<?> constraint : TypeUtils
        .getTypeElementConstraintDescriptors(typeMirror, processingEnv.getElementUtils(),
            processingEnv.getTypeUtils())) {
      if (areConstraintDescriptorGroupsValid(constraint)) {
        if (hasMatchingAnnotation(typeMirror, constraint)) {

          if (!constraint.getConstraintValidatorClasses().isEmpty()) { // NOPMD
            final TypeMirror validatorClass = getValidatorForType(constraint, typeMirror);

            // validate(context, violations, null, object,
            out.print(offset);
            out.println("validate(context, violations, null, object, ");

            // new MyValidtor(),
            out.print(offset);
            out.print("    new ");
            out.print(TypeUtils.getClassNameWithProperties(validatorClass));
            out.print("(), "); // TODO(nchalko) use ConstraintValidatorFactory

            // aConstraintDescriptor, groups);
            out.print(constraintDescriptorVar("this", count));
            out.print(", ");
            out.print(groupsVarName);
            out.println(");");
          } else if (constraint.getComposingConstraints().isEmpty()) {
            // TODO(nchalko) What does the spec say to do here.
            processingEnv.getMessager().printMessage(Diagnostic.Kind.WARNING,
                "No ConstraintValidator of " + constraint + " for type "
                    + TypeUtils.getClassNameWithProperties(typeMirror));
          }
          // TODO(nchalko) handle constraint.isReportAsSingleViolation() and
          // hasComposingConstraints
        }
        count++;
      }
    }
  }

  private Optional<TypeMirror> typeOfValdator(final TypeMirror type,
      final TypeMirror validationClass, final List<TypeMirror> constraintValidatorClasses) {
    final Optional<ExecutableElement> optionalIsValidMethod =
        TypeUtils.getMethod(validationClass, "isValid");
    if (optionalIsValidMethod.isPresent()) {
      final VariableElement validationVariableType =
          optionalIsValidMethod.get().getParameters().get(0);
      if (processingEnv.getTypeUtils().isAssignable(type, validationVariableType.asType())) {
        return Optional.of(validationVariableType.asType());
      }
    }
    final TypeMirror superClass =
        ((TypeElement) ((DeclaredType) validationClass).asElement()).getSuperclass();
    if (superClass != null && !"java.lang.Object".equals(TypeUtils.getClassName(superClass))) {
      if (CollectionUtils.isNotEmpty(((DeclaredType) superClass).getTypeArguments())
          && processingEnv.getTypeUtils().isAssignable(type,
              ((DeclaredType) superClass).getTypeArguments().get(0))) {
        return Optional.of(((DeclaredType) superClass).getTypeArguments().get(0));
      } else {
        return typeOfValdator(type, superClass, constraintValidatorClasses);
      }
    }
    return Optional.empty();
  }

  private Optional<TypeMirror> getValidatorForTypeElement(final TypeMirror type,
      final List<TypeMirror> constraintValidatorClasses) {
    // final TypeMirror elementType = processingEnv.getTypeUtils().asElement(ptype).asType();
    final Map<TypeMirror, TypeMirror> map = constraintValidatorClasses.stream()
        .map(typeMirror -> Pair.of(typeOfValdator(type, typeMirror, constraintValidatorClasses),
            typeMirror))
        .filter(pair -> pair.getKey().isPresent())
        .collect(Collectors.toMap(pair -> pair.getKey().get(), pair -> pair.getValue()));

    final Optional<TypeMirror> bestMatchingKey = findBestMatches(type, map.keySet());
    if (bestMatchingKey.isPresent()) {
      return Optional.ofNullable(map.get(bestMatchingKey.get()));
    }
    return Optional.empty();
  }

  private Optional<TypeMirror> findBestMatches(final TypeMirror type,
      final Set<TypeMirror> keySet) {
    if (CollectionUtils.isEmpty(keySet)) {
      return Optional.empty();
    }
    if (keySet.size() == 1) {
      return keySet.stream().findFirst();
    }
    final Optional<TypeMirror> sameTypeValidator = keySet.stream()
        .filter(typeMirror -> processingEnv.getTypeUtils().isSameType(typeMirror, type)).findAny();
    if (sameTypeValidator.isPresent()) {
      return sameTypeValidator;
    }
    return keySet.stream().findFirst();
  }

  /**
   * Gets the best {@link ConstraintValidator}.
   *
   * <p>
   * The ConstraintValidator chosen to validate a declared type {@code targetType} is the one where
   * the type supported by the ConstraintValidator is a supertype of {@code targetType} and where
   * there is no other ConstraintValidator whose supported type is a supertype of {@code type} and
   * not a supertype of the chosen ConstraintValidator supported type.
   * </p>
   *
   * @param constraint the constraint to find ConstraintValidators for.
   * @param targetType The type to find a ConstraintValidator for.
   * @return ConstraintValidator
   *
   * @throws UnexpectedTypeException if there is not exactly one maximally specific constraint
   *         validator for targetType.
   */
  protected TypeMirror getValidatorForType(final TypeElementConstraintDescriptor<?> constraint,
      final TypeMirror targetType) throws UnexpectedTypeException {
    final List<TypeMirror> constraintValidatorClasses = constraint.getConstraintValidatorClasses();
    if (constraintValidatorClasses.isEmpty()) {
      throw new UnexpectedTypeException(
          "No ConstraintValidator found for  " + constraint.getAnnotation());
    }
    final Optional<TypeMirror> best =
        getValidatorForTypeElement(targetType, constraintValidatorClasses);
    if (!best.isPresent()) {
      throw new UnexpectedTypeException(
          "No " + constraint.getAnnotation() + " ConstraintValidator for type " + targetType);
    }
    return best.get();
  }

  private boolean hasMatchingAnnotation(final AnnotationMirror expectedAnnotation,
      final List<? extends AnnotationMirror> annotations) throws UnexpectedTypeException {
    // See spec section 2.2. Applying multiple constraints of the same type
    for (final AnnotationMirror annotation : annotations) {
      // annotations not annotated by @Constraint
      if (annotation.getAnnotationType().getAnnotation(Constraint.class) == null) {
        try {
          // value element has a return type of an array of constraint
          // annotations
          final Optional<ExecutableElement> valueMethod = processingEnv.getTypeUtils()
              .asElement(annotation.getAnnotationType()).getEnclosedElements().stream()
              .filter(entry -> entry.getKind() == ElementKind.METHOD
                  && "value".equals(entry.getSimpleName().toString()))
              .map(ExecutableElement.class::cast).findFirst();
          if (valueMethod.isPresent()) {
            final TypeMirror valueType = valueMethod.get().getReturnType();
            if (valueType.getKind() == TypeKind.ARRAY && processingEnv.getTypeUtils()
                .isAssignable(expectedAnnotation.getAnnotationType(), valueType)) {
              // TODO maybe remove this
              // if (valueMethod.getModifiers().contains(Modifier.ABSTRACT)) {
              // handle edge case where interface is marked "abstract"
              // valueMethod.setAccessible(true);
              // }
              final List<? extends AnnotationMirror> valueAnnotions =
                  valueMethod.get().getAnnotationMirrors();
              for (final AnnotationMirror annotation2 : valueAnnotions) {
                if (expectedAnnotation.equals(annotation2)) {
                  return true;
                }
              }
            }
          }
        } catch (final Exception e) {
          processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR, e.getMessage());
        }
      }
    }
    return false;
  }

  private boolean hasMatchingAnnotation(final TypeMirror typeMirror,
      final TypeElementConstraintDescriptor<?> constraint) throws UnexpectedTypeException {
    final AnnotationMirror expectedAnnotation = constraint.getAnnotation();
    final List<AnnotationMirror> annotations = TypeUtils.getAnnotationsForType(typeMirror,
        processingEnv.getTypeUtils(), processingEnv.getElementUtils());
    if (annotations.contains(expectedAnnotation)) {
      return true;
    }

    // See spec section 2.2. Applying multiple constraints of the same type
    return hasMatchingAnnotation(expectedAnnotation, annotations);
  }

  private boolean hasMatchingAnnotation(final TypeElementPropertyDescriptor propertyDescription,
      final TypeMirror typeMirror, final boolean useField,
      final TypeElementConstraintDescriptor<?> constraint) throws UnexpectedTypeException {
    final AnnotationMirror expectedAnnotation = constraint.getAnnotation();
    final DeclaredType expectedAnnotationClass = expectedAnnotation.getAnnotationType();
    if (getAnnotation(propertyDescription, typeMirror, useField, expectedAnnotationClass)
        .isPresent()) {
      return true;
    }
    return hasMatchingAnnotation(expectedAnnotation,
        getAnnotations(propertyDescription, typeMirror, useField));
  }

  private boolean areConstraintDescriptorGroupsValid(
      final TypeElementConstraintDescriptor<?> constraintDescriptor) {
    if (validTypeElementConstraintsMap.containsKey(constraintDescriptor)) {
      return validTypeElementConstraintsMap.get(constraintDescriptor);
    } else {
      final boolean areValid = checkGroupsTypeMirror(constraintDescriptor.getGroups());
      // cache result
      validTypeElementConstraintsMap.put(constraintDescriptor, areValid);
      return areValid;
    }
  }

  private boolean checkGroupsTypeMirror(final Set<TypeMirror> groups) {
    return groups.stream().map(TypeMirror::toString).anyMatch(validGroupsTypeMirror::contains);
  }

  private List<String> detectAnnotationStringParameters(final TypeMirror typeMirror) {

    final List<AnnotationMirror> annotations = TypeUtils.getAnnotationsForType(typeMirror,
        processingEnv.getTypeUtils(), processingEnv.getElementUtils());

    return annotations.stream().flatMap(mirror -> streamOfAnnotationValues(mirror))
        .flatMap(entry -> {
          if (entry instanceof final AnnotationMirror annotationMirror) {
            return streamOfAnnotationValues(annotationMirror);
          } else if (entry instanceof final List<?> list) {
            return list.stream().flatMap(mirror -> {
              if (mirror instanceof final AnnotationMirror annotationMirror) {
                return streamOfAnnotationValues(annotationMirror);
              } else {
                return Stream.of(mirror);
              }
            });
          } else {
            return Stream.of(entry);
          }
        })
        .filter(entry -> entry instanceof final String entryString
            && !entryString.matches("^\\{.*\\}$")) //
        .map(String.class::cast) //
        .toList();
  }

  private Stream<Object> streamOfAnnotationValues(final AnnotationMirror mirror) {
    return processingEnv.getElementUtils().getElementValuesWithDefaults(mirror).values().stream()
        .map(AnnotationValue::getValue);
  }

  private String constraintDescriptorVar(final String name, final int count) {
    return name + "_c" + count;
  }

  private void writeWrappers(final PrintWriter out, final TypeMirror typeMirror) {
    out.println("  // Write the wrappers after we know which are needed");
    for (final Element field : fieldsToWrap) {
      writeFieldWrapperMethod(out, field, typeMirror);
      out.println();
    }

    for (final ExecutableElement method : gettersToWrap) {
      writeGetterWrapperMethod(out, method, typeMirror);
      out.println();
    }
  }

  private void writeFieldWrapperMethod(final PrintWriter out, final Element field,
      final TypeMirror typeMirror) {
    if (typeMirror instanceof final DeclaredType declaredType) {
      writeUnsafeNativeLongIfNeeded(out,
          processingEnv.getTypeUtils().asMemberOf(declaredType, field));
    } else {
      writeUnsafeNativeLongIfNeeded(out, field.asType());
    }

    // private native fieldType _fieldName(com.example.Bean object) /*-{
    out.print("  private native ");

    if (typeMirror instanceof final DeclaredType declaredType) {
      out.print(TypeUtils.getClassNameWithProperties(
          processingEnv.getTypeUtils().asMemberOf(declaredType, field)));
    } else {
      out.print(TypeUtils.getClassNameWithProperties(field.asType()));
    }
    out.print(" ");
    out.print(toWrapperName(field));
    out.println("(");
    out.print("      final ");
    out.print(TypeUtils.getClassNameWithProperties(typeMirror));
    out.println(" object) /*-{");

    // return object.@com.examples.Bean::myMethod();
    out.print("    return object.@");
    out.print(TypeUtils.getClassNameWithPropertiesOfElementNoType(typeMirror, field));
    out.print("::");
    out.print(field.getSimpleName());
    out.println(";");

    // }-*/;
    out.println("  }-*/;");
  }

  private void writeGetterWrapperMethod(final PrintWriter out, final ExecutableElement method,
      final TypeMirror typeMirror) {
    writeUnsafeNativeLongIfNeeded(out, method.getReturnType());

    // private native fieldType _getter(Bean object) /*={
    out.print("  private native ");
    out.print(TypeUtils.getClassNameWithProperties(method.getReturnType()));
    out.print(" ");
    out.print(toWrapperName(method));
    out.println("(");
    out.print("      final ");
    out.print(TypeUtils.getClassNameWithProperties(typeMirror));
    out.println(" object) /*-{");

    // return object.@com.examples.Bean::myMethod()();
    out.print("    return object.");
    out.print(method.getSimpleName());
    out.println("();");

    // }-*/;
    out.println("  }-*/;");
  }

  protected void writeUnsafeNativeLongIfNeeded(final PrintWriter out, final TypeMirror typeMirror) {
    if (typeMirror.getKind() == TypeKind.LONG) {
      // @com.google.gwt.core.client.UnsafeNativeLong
      out.println("  @com.google.gwt.core.client.UnsafeNativeLong");
    }
  }

  private void writeReflectionGetterReplacement(final PrintWriter out, final TypeMirror typeMirror,
      final List<String> parameters) throws UnexpectedTypeException {
    // private Map<String, Object> generateReflectionGetterReplacementMap(
    out.println("  private java.util.Map<String, Object> generateReflectionGetterReplacementMap(");

    // BeanType object) {
    out.print("      ");
    out.println(TypeUtils.getClassNameWithProperties(typeMirror) + " object) {");

    out.println(
        "    final java.util.Map<String, Object> reflectionMap = new java.util.HashMap<>();");

    writeRecursiveGetterReplacmentEntries(out, "    ", typeMirror, "object", Optional.empty(), true,
        0, parameters);

    out.println("    return reflectionMap;");

    // }
    out.println("  }");
  }

  private void writeRecursiveGetterReplacmentEntries(final PrintWriter out, final String offset,
      final TypeMirror typeMirror, final String objectName, final Optional<String> prefix,
      final boolean hasValue, final int count, final List<String> parameters) {

    // max recursion count reached, then stop
    if (count >= 2) {
      return;
    }

    TypeUtils.getGetter(typeMirror).forEach(getter -> {

      final String pureGetterName = TypeUtils.getterNameFromElement(getter);
      final String pureField = TypeUtils.valueFromGetter(pureGetterName);
      if (!"class".equals(pureField)) {
        final String field =
            (prefix.isPresent() ? prefix.get() + "." : StringUtils.EMPTY) + pureField;
        final String getterName = objectName + "." + pureGetterName + "()";

        if (parameters.contains(field)) {
          out.print(offset);
          out.print("reflectionMap.put(\"");
          out.print(field);
          out.print("\", ");
          if (hasValue) {
            out.print(objectName);
            out.print(" == null ? null : ");
            out.print(getterName);
          } else {
            out.print("null");
          }
          out.println(");");
        }

        if (getter.getReturnType().getKind() == TypeKind.DECLARED
            && processingEnv.getTypeUtils().asElement(getter.getReturnType())
                .getKind() != ElementKind.ENUM
            && !StringUtils.startsWith(TypeUtils.getClassName(getter.getReturnType()), "java")
            && parameters.stream().anyMatch(entry -> StringUtils.startsWith(entry, field))) {
          if (hasValue) {
            out.print(offset);
            out.print("if (");
            out.print(objectName);
            out.print(" == null || ");
            out.print(getterName);
            out.println(" == null) {");

            writeRecursiveGetterReplacmentEntries(out, offset + "  ", getter.getReturnType(),
                getterName, Optional.of(field), false, count + 1, parameters);

            out.print(offset);
            out.println("} else {");

            writeRecursiveGetterReplacmentEntries(out, offset + "  ", getter.getReturnType(),
                getterName, Optional.of(field), hasValue, count + 1, parameters);

            out.print(offset);
            out.println("}");
          } else {
            writeRecursiveGetterReplacmentEntries(out, offset, getter.getReturnType(), getterName,
                Optional.of(field), false, count + 1, parameters);
          }
        }
      }
    });
  }
}

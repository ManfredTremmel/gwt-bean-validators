package de.knightsoftnet.validators.annotation.processor;

import org.hibernate.validator.internal.metadata.core.ConstraintOrigin;
import org.hibernate.validator.internal.metadata.location.ConstraintLocation.ConstraintLocationKind;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.AnnotationValue;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;
import jakarta.validation.ReportAsSingleViolation;
import jakarta.validation.metadata.ConstraintDescriptor;

public interface TypeElementConstraintDescriptor<T extends AnnotationMirror> {

  /**
   * Returns the annotation describing the constraint declaration. If a composing constraint,
   * attribute values are reflecting the overridden attributes of the composing constraint
   *
   * @return the annotation for this constraint
   */
  T getAnnotation();

  /**
   * Returns a map containing the annotation attribute names as keys and the annotation attribute
   * values as value.
   * <p>
   * If this constraint is used as part of a composed constraint, attribute values are reflecting
   * the overridden attribute of the composing constraint.
   * </p>
   *
   * @return a map containing the annotation attribute names as keys and the annotation attribute
   *         values as value
   */
  Map<String, Entry<? extends ExecutableElement, ? extends AnnotationValue>> getAttributes();

  /**
   * Return a set of composing {@link ConstraintDescriptor}s where each descriptor describes a
   * composing constraint. {@code ConstraintDescriptor} instances of composing constraints reflect
   * overridden attribute values in {@link #getAttributes()} and {@link #getAnnotation()}.
   *
   * @return a set of {@code TypeElementConstraintDescriptor} objects or an empty set in case there
   *         are no composing constraints
   */
  List<TypeElementConstraintDescriptor<?>> getComposingConstraints();

  /**
   * List of the constraint validation implementation classes.
   *
   * @return list of the constraint validation implementation classes
   */
  List<TypeMirror> getConstraintValidatorClasses();

  /**
   * The set of groups the constraint is applied on. If the constraint declares no group, a set with
   * only the {@link jakarta.validation.groups.Default} group is returned.
   *
   * @return the groups the constraint is applied on
   */
  Set<TypeMirror> getGroups();

  /**
   * The set of payload the constraint hosts.
   *
   * @return payload classes hosted on the constraint or an empty set if none
   */
  Set<TypeElement> getPayload();

  /**
   * check if this is reported as single violation.
   *
   * @return {@code true} if the constraint is annotated with {@link ReportAsSingleViolation}
   */
  boolean isReportAsSingleViolation();

  /**
   * get defined on.
   *
   * @return constraint origin
   */
  ConstraintOrigin getDefinedOn();

  /**
   * get constraint location kind.
   *
   * @return constraint location kind
   */
  ConstraintLocationKind getConstraintLocationKind();
}

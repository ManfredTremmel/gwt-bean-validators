package de.knightsoftnet.validators.annotation.processor;

import de.knightsoftnet.validators.client.GwtValidation;

import com.google.auto.service.AutoService;
import com.google.common.collect.Sets;

import java.util.List;
import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Processor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;
import javax.tools.Diagnostic;

@SupportedAnnotationTypes("de.knightsoftnet.validators.client.GwtValidation")
@SupportedSourceVersion(SourceVersion.RELEASE_17)
@AutoService(Processor.class)
public class GwtSpecificValidatorCreatorProcessor extends AbstractProcessor {

  private static final GwtSpecificValidatorInterfaceCreator INTERFACE_CREATOR =
      new GwtSpecificValidatorInterfaceCreator();

  private final Set<TypeMirror> validGroups = Sets.newConcurrentHashSet();
  private final Set<String> processedInterfaces = Sets.newConcurrentHashSet();

  @Override
  public boolean process(final Set<? extends TypeElement> annotations,
      final RoundEnvironment roundEnv) {
    if (annotations == null || annotations.size() != 1 || roundEnv == null
        || roundEnv.processingOver()) {
      return false;
    }
    final Set<? extends Element> annotatedElements =
        roundEnv.getElementsAnnotatedWith(annotations.iterator().next());
    if (annotatedElements == null || annotatedElements.size() != 1) {
      processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR,
          "@GwtValidation must be applied to exactly one class");
      return false;
    }

    final Element annotatedElement = annotatedElements.iterator().next();
    final String packageName = processingEnv.getElementUtils().getPackageOf(annotatedElement)
        .getQualifiedName().toString();
    final GwtValidation gwtValidation = annotatedElement.getAnnotation(GwtValidation.class);
    validGroups.addAll(ApUtils.getTypeMirrorFromAnnotationValue(gwtValidation::groups));
    final boolean generateReflectionGetter = gwtValidation.generateReflectionGetter();
    final List<? extends TypeMirror> generateReflectionsFor =
        ApUtils.getTypeMirrorFromAnnotationValue(gwtValidation::reflect);
    final String[] languages = gwtValidation.languages();

    ApUtils.getTypeMirrorFromAnnotationValue(gwtValidation::value)
        .forEach(typeMirror -> writeClassForTypeMirror(gwtValidation, generateReflectionGetter,
            generateReflectionsFor, languages, typeMirror));

    final ValidatorCreator validatorCreator = new ValidatorCreator();
    final String interfaceName = annotatedElement.getSimpleName().toString();
    final String factoryClass = annotatedElement.getEnclosingElement().getSimpleName().toString();
    validatorCreator.writeClass(packageName, interfaceName, factoryClass, gwtValidation,
        processingEnv);

    if (gwtValidation.generateValidatorFactoryInterface()) {
      final ValidatorInterfaceCreator validatorInterfaceCreator = new ValidatorInterfaceCreator();
      validatorInterfaceCreator.writeInterface(packageName, factoryClass, processingEnv);
    }

    return true;
  }

  protected void writeClassForTypeMirror(final GwtValidation gwtValidation,
      final boolean generateReflectionGetter,
      final List<? extends TypeMirror> generateReflectionsFor, final String[] languages,
      final TypeMirror typeMirror) {
    if (!processedInterfaces.contains(TypeUtils.getClassName(typeMirror))) {
      processedInterfaces.add(TypeUtils.getClassName(typeMirror));
      INTERFACE_CREATOR.writeInterface(typeMirror, processingEnv);
      final boolean generateReflection =
          (generateReflectionsFor.isEmpty() || generateReflectionsFor.contains(typeMirror))
              && generateReflectionGetter;
      final GwtSpecificValidatorClassCreator classCreator =
          new GwtSpecificValidatorClassCreator(processingEnv, validGroups,
              gwtValidation.forceUsingGetter(), generateReflection, languages);
      classCreator.writeClass(typeMirror).forEach(mirror -> writeClassForTypeMirror(gwtValidation,
          generateReflectionGetter, generateReflectionsFor, languages, mirror));
    }
  }
}

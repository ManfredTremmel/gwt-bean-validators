package de.knightsoftnet.validators.annotation.processor;

import de.knightsoftnet.validators.client.GwtValidation;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeMirror;
import javax.tools.Diagnostic;
import javax.tools.JavaFileObject;

import jakarta.validation.GroupSequence;

public class ValidatorCreator {

  /**
   * write validator factory class.
   *
   * @param packageName package name
   * @param interfaceName interface name
   * @param factoryClass factory class name
   * @param gwtValidation gwt validation annotation with data
   * @param processingEnv processing environment.
   */
  public void writeClass(final String packageName, final String interfaceName,
      final String factoryClass, final GwtValidation gwtValidation,
      final ProcessingEnvironment processingEnv) {
    try {
      final JavaFileObject builderFile =
          processingEnv.getFiler().createSourceFile(packageName + "." + interfaceName + "Impl");
      try (PrintWriter out = new PrintWriter(builderFile.openWriter())) {
        writeClassHeader(out, packageName, interfaceName, factoryClass);
        out.println();
        writeConstructor(out, interfaceName, gwtValidation, processingEnv);
        out.println();
        writeCreateValidationGroupsMetadata(out, gwtValidation);
        out.println();
        writeCreateValidatorForInstanceClass(out, gwtValidation, processingEnv);
        writeClassFooter(out);
      }
    } catch (final IOException e) {
      processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR, e.getMessage());
      e.printStackTrace();
    }
  }

  private void writeClassHeader(final PrintWriter out, final String packageName,
      final String interfaceName, final String factoryClass) {

    // package de.knightsoftnet.validators.client.factories;
    out.print("package ");
    out.print(packageName);
    out.println(";");
    out.println();

    // imports
    out.println("import de.knightsoftnet.validators.client.impl.AbstractGwtSpecificValidator;");
    out.println("import de.knightsoftnet.validators.client.impl.AbstractGwtValidator;");
    out.println("import de.knightsoftnet.validators.client.impl.GwtBeanDescriptor;");
    out.println("import de.knightsoftnet.validators.client.impl.GwtSpecificValidator;");
    out.println("import de.knightsoftnet.validators.client.impl.GwtValidationContext;");
    out.println(
        "import de.knightsoftnet.validators.client.impl.metadata.ValidationGroupsMetadata;");
    out.println();

    out.println("import java.util.HashMap;");
    out.println("import java.util.HashSet;");
    out.println("import java.util.Map;");
    out.println("import java.util.Set;");
    out.println("import java.util.stream.Collectors;");
    out.println("import java.util.stream.Stream;");
    out.println();

    out.println("import jakarta.validation.groups.Default;");
    out.println("import jakarta.validation.ConstraintViolation;");
    out.println("import jakarta.validation.metadata.BeanDescriptor;");
    out.println();

    // public class ValidatorFactory_GwtValidatorImpl extends AbstractGwtValidator implements
    // ValidatorFactory.GwtValidator {
    out.print("public class ");
    out.print(interfaceName);
    out.print("Impl extends AbstractGwtValidator implements ");
    out.print(factoryClass);
    out.print(".");
    out.print(interfaceName);
    out.println(" {");
  }

  private void writeConstructor(final PrintWriter out, final String interfaceName,
      final GwtValidation gwtValidation, final ProcessingEnvironment processingEnv) {
    // public ValidatorFactory_GwtValidatorImpl() {
    out.print("  public ");
    out.print(interfaceName);
    out.println("Impl() {");
    // super(createValidationGroupsMetadata());
    out.println("    super(Stream.of(new Object[][] {");

    ApUtils.getTypeMirrorFromAnnotationValue(gwtValidation::value).forEach(typeMirror -> {
      out.println("        {" + typeMirror.toString() + ".class,");
      out.println("            " + TypeUtils.getValidationInstanceForType(typeMirror,
          processingEnv.getTypeUtils(), processingEnv.getElementUtils()) + "},");
    });

    out.println("    }).collect(Collectors.toMap(data -> (Class<?>) data[0],");
    out.println("        data -> (AbstractGwtSpecificValidator<?>) data[1])), "
        + "createValidationGroupsMetadata());");

    // }
    out.println("  }");
  }

  private void writeCreateValidationGroupsMetadata(final PrintWriter out,
      final GwtValidation gwtValidation) {
    // private static ValidationGroupsMetadata createValidationGroupsMetadata() {
    out.println("  private static ValidationGroupsMetadata createValidationGroupsMetadata() {");

    // return ValidationGroupsMetadata.builder()
    out.println("    return ValidationGroupsMetadata.builder()");
    for (final TypeMirror group : ApUtils.getTypeMirrorFromAnnotationValue(gwtValidation::groups)) {
      final GroupSequence sequenceAnnotation = group.getAnnotation(GroupSequence.class);
      List<String> groups;
      if (sequenceAnnotation == null) {
        // .addGroup(<<group>>
        out.print("        .addGroup(");
        out.print(group.toString() + ".class");
        final List<? extends TypeMirror> interfaces =
            ((TypeElement) ((DeclaredType) group).asElement()).getInterfaces();
        groups = interfaces.stream().map(inter -> inter.toString() + ".class").toList();
      } else {
        // .addSequence(<<sequence>>
        out.print("        .addSequence(");
        out.print(group.getClass().getCanonicalName() + ".class");
        groups = Arrays.stream(sequenceAnnotation.value())
            .map(clazz -> clazz.getCanonicalName() + ".class").toList();
      }
      for (final String clazz : groups) {
        // , <<group class>>
        out.print(", ");
        out.print(clazz);
      }
      // )
      out.println(")");
    }

    // .build();
    out.println("        .build();");

    // }
    out.println("  }");
  }

  private void writeCreateValidatorForInstanceClass(final PrintWriter out,
      final GwtValidation gwtValidation, final ProcessingEnvironment processingEnv) {
    out.println("  @Override");
    // private static ValidationGroupsMetadata createValidationGroupsMetadata() {
    out.println("  protected <T> AbstractGwtSpecificValidator<T> getValidatorForInstanceClass(");
    out.println("      final Object object) {");

    ApUtils.getTypeMirrorFromAnnotationValue(gwtValidation::value).forEach(typeMirror -> {
      out.println("    if (object instanceof " + typeMirror.toString() + ") {");
      out.println("      return (AbstractGwtSpecificValidator<T>) "
          + TypeUtils.getValidationInstanceForType(typeMirror, processingEnv.getTypeUtils(),
              processingEnv.getElementUtils())
          + ";");
      out.println("    }");
    });

    out.println("    return null;");
    out.println("  }");
  }

  private void writeClassFooter(final PrintWriter out) {
    out.println("}");
  }
}

package de.knightsoftnet.validators.annotation.processor;

import java.util.List;

import javax.lang.model.type.MirroredTypeException;
import javax.lang.model.type.MirroredTypesException;
import javax.lang.model.type.TypeMirror;

public class ApUtils {

  @FunctionalInterface
  public interface GetClassValue {
    void execute() throws MirroredTypeException, MirroredTypesException;
  }

  /**
   * get type mirror from annotation value.
   *
   * @param classValue class value
   * @return list of type mirrors
   */
  public static List<? extends TypeMirror> getTypeMirrorFromAnnotationValue(
      final GetClassValue classValue) {
    try {
      classValue.execute();
    } catch (final MirroredTypesException ex) {
      return ex.getTypeMirrors();
    }
    return null;
  }
}

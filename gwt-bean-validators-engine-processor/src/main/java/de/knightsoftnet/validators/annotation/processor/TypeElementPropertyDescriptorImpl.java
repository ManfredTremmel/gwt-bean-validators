package de.knightsoftnet.validators.annotation.processor;

import org.hibernate.validator.internal.metadata.location.ConstraintLocation.ConstraintLocationKind;

import java.util.Collections;
import java.util.List;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeMirror;

public class TypeElementPropertyDescriptorImpl implements TypeElementPropertyDescriptor {

  private final String propertyName;
  private final Element typeElement;
  private final TypeMirror elementType;
  private final List<TypeElementConstraintDescriptor<?>> constraintDescriptors;
  private final ConstraintLocationKind constraintLocationKind;

  /**
   * constructor.
   *
   * @param typeElement the element to get data from.
   * @param beanType declared type of the bean to get data from
   * @param processingEnv to get utility for elements and types
   * @param constraintLocationKind where annotation is located
   */
  public TypeElementPropertyDescriptorImpl(final Element typeElement, final DeclaredType beanType,
      final ProcessingEnvironment processingEnv,
      final ConstraintLocationKind constraintLocationKind) {
    super();
    this.typeElement = typeElement;
    elementType = switch (typeElement.getKind()) {
      case METHOD -> ((ExecutableElement) typeElement).getReturnType();
      case FIELD -> processingEnv.getTypeUtils().asMemberOf(beanType, typeElement);
      default -> typeElement.asType();
    };
    this.constraintLocationKind = constraintLocationKind;
    propertyName = constraintLocationKind == ConstraintLocationKind.TYPE ? "this"
        : typeElement.getSimpleName().toString();
    constraintDescriptors =
        TypeElementConstraintDescriptorImpl.createComposingConstraintsForElement(typeElement,
            processingEnv.getElementUtils(), constraintLocationKind, Collections.emptyMap());
  }

  @Override
  public String getPropertyName() {
    return propertyName;
  }

  @Override
  public TypeMirror getElementType() {
    return elementType;
  }

  @Override
  public List<TypeElementConstraintDescriptor<?>> getConstraintDescriptors() {
    return constraintLocationKind == ConstraintLocationKind.TYPE ? Collections.emptyList()
        : constraintDescriptors;
  }

  @Override
  public boolean isCascaded() {
    return typeElement.getAnnotationMirrors().stream()
        .filter(annotationMirror -> "jakarta.validation.Valid"
            .contentEquals(annotationMirror.getAnnotationType().toString()))
        .count() != 0L;
  }

  @Override
  public List<TypeElementConstraintDescriptor<?>> getConstrainedContainerElementTypes() {
    return constraintLocationKind == ConstraintLocationKind.TYPE ? constraintDescriptors
        : Collections.emptyList();
  }
}

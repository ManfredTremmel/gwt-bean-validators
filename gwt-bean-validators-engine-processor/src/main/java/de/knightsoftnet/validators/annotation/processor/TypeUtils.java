package de.knightsoftnet.validators.annotation.processor;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.internal.metadata.location.ConstraintLocation.ConstraintLocationKind;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.AnnotationValue;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.ArrayType;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.ExecutableType;
import javax.lang.model.type.NoType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;

import jakarta.validation.Valid;
import jakarta.validation.metadata.PropertyDescriptor;

public class TypeUtils {

  /**
   * get class name for type element.
   *
   * @param element to get name for
   * @return class name
   */
  public static String getClassName(final TypeElement element) {
    Element currElement = element;
    String result = element.getSimpleName().toString();
    while (currElement.getEnclosingElement() != null) {
      currElement = currElement.getEnclosingElement();
      if (currElement instanceof TypeElement) {
        result = currElement.getSimpleName() + "$" + result;
      } else if (currElement instanceof PackageElement
          && StringUtils.isNotEmpty(currElement.getSimpleName())) {
        result = ((PackageElement) currElement).getQualifiedName() + "." + result;
      }
    }
    return result;
  }

  /**
   * get class name for type element.
   *
   * @param typeMirror to get name for
   * @return class name
   */
  public static String getClassName(final TypeMirror typeMirror) {
    return switch (typeMirror.getKind()) {
      case ARRAY -> getClassName(((ArrayType) typeMirror).getComponentType()) + "[]";
      case EXECUTABLE -> getClassName(((ExecutableType) typeMirror).getReturnType());
      case DECLARED -> ((DeclaredType) typeMirror).asElement().toString();
      case BOOLEAN -> "boolean";
      case BYTE -> "byte";
      case SHORT -> "short";
      case INT -> "int";
      case LONG -> "long";
      case CHAR -> "char";
      case FLOAT -> "float";
      case DOUBLE -> "double";
      default -> typeMirror.toString();
    };
  }

  /**
   * get class name for type element without path.
   *
   * @param typeMirror to get name for
   * @return class name
   */
  public static String getClassNameWithoutPath(final TypeMirror typeMirror) {
    String className = getClassName(typeMirror);
    if (StringUtils.contains(className, '.')) {
      className = StringUtils.substring(className, StringUtils.lastIndexOf(className, '.') + 1);
    }
    return className;
  }

  /**
   * get class name for type mirror with properties.
   *
   * @param typeMirror to get name for
   * @return class name
   */
  public static String getClassNameWithProperties(final TypeMirror typeMirror) {
    return switch (typeMirror.getKind()) {
      case ARRAY -> getClassName(((ArrayType) typeMirror).getComponentType()) + "[]";
      case EXECUTABLE -> getClassName(((ExecutableType) typeMirror).getReturnType());
      case DECLARED -> getClassNameWithPropertiesDeclared((DeclaredType) typeMirror);
      case BOOLEAN -> "boolean";
      case BYTE -> "byte";
      case SHORT -> "short";
      case INT -> "int";
      case LONG -> "long";
      case CHAR -> "char";
      case FLOAT -> "float";
      case DOUBLE -> "double";
      default -> typeMirror.toString();
    };
  }

  private static String getClassNameWithPropertiesDeclared(final DeclaredType typeMirror) {
    final String type;
    if (CollectionUtils.isEmpty(typeMirror.getTypeArguments())) {
      type = StringUtils.EMPTY;
    } else {
      type = typeMirror.getTypeArguments().stream().map(TypeUtils::getClassNameWithProperties)
          .collect(Collectors.joining(", ", "<", ">"));
    }
    return typeMirror.asElement().toString() + type;
  }

  /**
   * get class name for type mirror with properties which contains given field, if it's defined in
   * super class, the name of superclass is returned.
   *
   * @param typeMirror (or it's superclass) to get name for
   * @param field element to check if typeMirrorContains it.
   * @return class name
   */
  public static String getClassNameWithPropertiesOfElement(final TypeMirror typeMirror,
      final Element field) {
    final TypeElement typeElement = (TypeElement) ((DeclaredType) typeMirror).asElement();
    if (typeElement.getEnclosedElements().contains(field)) {
      return getClassNameWithProperties(typeMirror);
    }
    if (typeElement.getSuperclass() instanceof NoType) {
      return null;
    }
    return getClassNameWithPropertiesOfElement(typeElement.getSuperclass(), field);
  }

  /**
   * get class name for type mirror with properties which contains given field, if it's defined in
   * super class, the name of superclass is returned.
   *
   * @param typeMirror (or it's superclass) to get name for
   * @param field element to check if typeMirrorContains it.
   * @return class name
   */
  public static String getClassNameWithPropertiesOfElementNoType(final TypeMirror typeMirror,
      final Element field) {
    final TypeElement typeElement = (TypeElement) ((DeclaredType) typeMirror).asElement();
    if (typeElement.getEnclosedElements().contains(field)) {
      return getClassName(typeMirror);
    }
    if (typeElement.getSuperclass() instanceof NoType) {
      return null;
    }
    return getClassNameWithPropertiesOfElementNoType(typeElement.getSuperclass(), field);
  }

  /**
   * Returns {@code true} if the bean involves validation.
   * <ul>
   * <li>a constraint is hosted on the bean itself</li>
   * <li>a constraint is hosted on one of the bean properties</li>
   * <li>or a bean property is marked for cascaded validation ({@link Valid})</li>
   * </ul>
   * <p>
   * Constrained methods and constructors are ignored.
   * </p>
   *
   * @param typeMirror to get getter from
   * @return {@code true} if the bean involves validation, {@code false} otherwise
   */
  public static boolean isBeanConstrained(final TypeMirror typeMirror, final Types typeUtils,
      final Elements elementUtils) {
    return !getTypeElementConstraintDescriptors(typeMirror, elementUtils, typeUtils).isEmpty()
        || getFields(typeMirror).stream().filter(TypeUtils::hasValidAnnotation).findFirst()
            .isPresent()
        || getGetter(typeMirror).stream().filter(TypeUtils::hasValidAnnotation).findFirst()
            .isPresent();

  }

  /**
   * Returns all constraint descriptors for this element in the class hierarchy or an empty
   * {@code Set} if none are present.
   *
   * @param typeMirror to get getter from
   * @return {@code Set} of type element constraint descriptors for this element
   */
  public static List<TypeElementConstraintDescriptor<?>> getTypeElementConstraintDescriptors(
      final TypeMirror typeMirror, final Elements elementUtils, final Types typeUtils) {
    return getTypeElementConstraintDescriptorsRecursive(((DeclaredType) typeMirror).asElement(),
        elementUtils, typeUtils, new ArrayList<>());
  }

  private static List<TypeElementConstraintDescriptor<?>> //
      getTypeElementConstraintDescriptorsRecursive(final Element element,
          final Elements elementUtils, final Types typeUtils,
          final List<TypeElementConstraintDescriptor<?>> constraintDescriptors) {
    constraintDescriptors
        .addAll(TypeElementConstraintDescriptorImpl.createComposingConstraintsForElement(element,
            elementUtils, ConstraintLocationKind.TYPE, Collections.emptyMap()));
    if (!(element instanceof TypeElement)
        || ((TypeElement) element).getSuperclass() instanceof NoType) {
      return constraintDescriptors;
    }
    return getTypeElementConstraintDescriptorsRecursive(
        typeUtils.asElement(((TypeElement) element).getSuperclass()), elementUtils, typeUtils,
        constraintDescriptors);
  }

  /**
   * Returns a set of type element property descriptors having at least one constraint defined or
   * marked as cascaded ({@link Valid}).
   * <p>
   * If no property matches, an empty set is returned. Properties of super types are considered.
   * </p>
   *
   * @param typeMirror to get getter from
   * @return the set of {@link PropertyDescriptor}s for the constraint properties; if there are no
   *         constraint properties, the empty set is returned
   */
  public static List<TypeElementPropertyDescriptor> getTypeElementProperties(
      final TypeMirror typeMirror, final ProcessingEnvironment processingEnv) {
    return getRecursiveEnclosedElements(typeMirror).stream()
        .filter(subElement -> (subElement.getKind() == ElementKind.FIELD || isGetter(subElement))
            && hasValidationAnnotation(subElement))
        .map(subElement -> new TypeElementPropertyDescriptorImpl(subElement,
            (DeclaredType) typeMirror, processingEnv, ConstraintLocationKind.FIELD))
        .collect(Collectors.toList());
  }

  private static boolean hasValidationAnnotation(final Element element) {
    return element.getAnnotationMirrors().stream().filter(TypeUtils::isValidationAnnotation)
        .count() != 0L;
  }

  private static boolean isValidationAnnotation(final AnnotationMirror annotationMirror) {
    return "jakarta.validation.Valid".contentEquals(annotationMirror.getAnnotationType().toString())
        || annotationMirror.getAnnotationType().asElement().getAnnotationMirrors().stream()
            .filter(entry -> {
              return "jakarta.validation.Constraint"
                  .equals(entry.getAnnotationType().asElement().toString());
            }).count() != 0L;
  }

  private static boolean hasValidAnnotation(final Element element) {
    return element.getAnnotationMirrors().stream()
        .filter(annotationMirror -> "jakarta.validation.Valid"
            .contentEquals(annotationMirror.getAnnotationType().toString()))
        .findFirst().isPresent();
  }

  /**
   * get list of getters for a type mirror.
   *
   * @param typeMirror to get getter from
   * @return list of getters
   */
  public static List<Element> getRecursiveEnclosedElements(final TypeMirror typeMirror) {
    final List<Element> getterList = new ArrayList<>();
    return getRecursiveEnclosedElements(typeMirror, getterList);
  }

  /**
   * get list of getter for a type mirror.
   *
   * @param typeMirror to get getter from
   * @param enclosingTypeList list of enclosing types to extend
   * @return list of getters
   */
  private static List<Element> getRecursiveEnclosedElements(final TypeMirror typeMirror,
      final List<Element> enclosingTypeList) {
    final TypeElement typeElement = (TypeElement) ((DeclaredType) typeMirror).asElement();
    enclosingTypeList.addAll(typeElement.getEnclosedElements());

    if (typeElement.getSuperclass() instanceof NoType) {
      return enclosingTypeList;
    }
    return getRecursiveEnclosedElements(typeElement.getSuperclass(), enclosingTypeList);
  }

  /**
   * get list of getter names for a type mirror.
   *
   * @param typeMirror to get getter from
   * @return list of getters
   */
  public static List<String> getGetterNames(final TypeMirror typeMirror) {
    return getGetter(typeMirror).stream().map(TypeUtils::getterNameFromElement).toList();
  }

  /**
   * get list of getters for a type mirror.
   *
   * @param typeMirror to get getter from
   * @return list of getters
   */
  public static List<ExecutableElement> getGetter(final TypeMirror typeMirror) {
    return getRecursiveEnclosedElements(typeMirror).stream().filter(TypeUtils::isGetter)
        .map(ExecutableElement.class::cast).toList();
  }

  /**
   * get getter for a value name.
   *
   * @param typeMirror type mirror of the class
   * @param valueName value element name
   * @return true if getter exists.
   */
  public static Optional<ExecutableElement> getGetter(final TypeMirror typeMirror,
      final String valueName) {
    if (isGetter(valueName)) {
      return getGetter(typeMirror).stream()
          .filter(element -> StringUtils.equals(getterNameFromElement(element), valueName))
          .findFirst();
    }
    return getGetter(typeMirror).stream().filter(
        element -> StringUtils.equals(valueFromGetter(getterNameFromElement(element)), valueName))
        .findFirst();
  }

  /**
   * check if element is a getter method.
   *
   * @param element to check
   * @return true if it is a getter
   */
  public static boolean isGetter(final Element element) {
    if (element.getKind() == ElementKind.METHOD) {
      final ExecutableElement method = (ExecutableElement) element;
      if (!method.getModifiers().contains(Modifier.PRIVATE) //
          && method.getParameters().isEmpty() //
          && method.getReturnType().getKind() != TypeKind.VOID //
          && isGetter(method.getSimpleName().toString())) {
        return true;
      }
    }
    return false;
  }

  /**
   * check if element is a getter method.
   *
   * @param elementName to check
   * @return true if it is a getter
   */
  public static boolean isGetter(final String elementName) {
    return StringUtils.startsWith(elementName, "get") && StringUtils.length(elementName) > 3
        && Character.isUpperCase(elementName.charAt(3))
        || StringUtils.startsWith(elementName, "is") && StringUtils.length(elementName) > 2
            && Character.isUpperCase(elementName.charAt(2));
  }

  /**
   * get list of interfaces of a type mirror.
   *
   * @param typeMirror to get getter from
   * @return list of getters
   */
  public static List<TypeMirror> getInterfaces(final TypeMirror typeMirror) {
    final List<TypeMirror> interfacesList = new ArrayList<>();
    return getInterfaces(typeMirror, interfacesList);
  }

  /**
   * get list of interfaces of a type mirror.
   *
   * @param typeMirror to get getter from
   * @param interfacesList list of interfaces to extend
   * @return list of interfaces
   */
  private static List<TypeMirror> getInterfaces(final TypeMirror typeMirror,
      final List<TypeMirror> interfacesList) {
    if (typeMirror.getKind() == TypeKind.DECLARED) {
      final TypeElement typeElement = (TypeElement) ((DeclaredType) typeMirror).asElement();
      for (final TypeMirror interfaceElement : typeElement.getInterfaces()) {
        getInterfaces(interfaceElement, interfacesList);
        interfacesList.add(interfaceElement);
      }
    }
    return interfacesList;
  }

  /**
   * get list of methods for a type mirror.
   *
   * @param typeMirror to get methods from
   * @return list of methods
   */
  public static List<ExecutableElement> getMethods(final TypeMirror typeMirror) {
    return getRecursiveEnclosedElements(typeMirror).stream()
        .filter(element -> element.getKind() == ElementKind.METHOD)
        .map(ExecutableElement.class::cast).toList();
  }

  /**
   * get list of fields for a type mirror.
   *
   * @param typeMirror to get fields from
   * @return list of field elements
   */
  public static List<Element> getFields(final TypeMirror typeMirror) {
    return getRecursiveEnclosedElements(typeMirror).stream()
        .filter(element -> element.getKind() == ElementKind.FIELD).toList();
  }

  /**
   * get annotations for type.
   *
   * @param typeMirror to get annotations from
   * @param typeUtils type utils
   * @param elementUtils element utils
   * @return list of annotation mirrors
   */
  public static List<AnnotationMirror> getAnnotationsForType(final TypeMirror typeMirror,
      final Types typeUtils, final Elements elementUtils) {
    return getAnnotationsForElement(typeUtils.asElement(typeMirror), typeUtils, elementUtils,
        new ArrayList<>());
  }

  /**
   * get annotations for Element.
   *
   * @param element to get annotations from
   * @param typeUtils type utils
   * @param elementUtils element utils
   * @return list of annotation mirrors
   */
  public static List<AnnotationMirror> getAnnotationsForElement(final Element element,
      final Types typeUtils, final Elements elementUtils) {
    return getAnnotationsForElement(element, typeUtils, elementUtils, new ArrayList<>());
  }

  /**
   * get annotations for Element.
   *
   * @param element to get annotations from
   * @param typeUtils type utils
   * @param elementUtils element utils
   * @return list of annotation mirrors
   */
  private static List<AnnotationMirror> getAnnotationsForElement(final Element element,
      final Types typeUtils, final Elements elementUtils, final List<AnnotationMirror> result) {
    for (final AnnotationMirror any : elementUtils.getAllAnnotationMirrors(element)) {
      boolean isList = false;
      for (final Entry<? extends ExecutableElement, ? extends AnnotationValue> entry : any
          .getElementValues().entrySet()) {
        final String methodName = entry.getKey().getSimpleName().toString();
        if ("value".equals(methodName) && entry.getValue().getValue() instanceof List) {
          for (final Object annotationMirror : (List<?>) entry.getValue().getValue()) {
            if (annotationMirror instanceof AnnotationMirror) {
              isList = true;
              result.add((AnnotationMirror) annotationMirror);
            }
          }
        }
      }
      if (!isList) {
        result.add(any);
      }
    }
    if (!(element instanceof TypeElement)
        || ((TypeElement) element).getSuperclass() instanceof NoType) {
      return result;
    }
    return getAnnotationsForElement(typeUtils.asElement(((TypeElement) element).getSuperclass()),
        typeUtils, elementUtils, result);
  }

  /**
   * get the getter name from element.
   *
   * @param element to get getter name from
   * @return getter name
   */
  public static String getterNameFromElement(final Element element) {
    return ((ExecutableElement) element).getSimpleName().toString();
  }

  /**
   * get element from getter.
   *
   * @param getterName name of getter method
   * @return element name
   */
  public static String valueFromGetter(final String getterName) {
    return StringUtils
        .uncapitalize(StringUtils.removeStart(StringUtils.removeStart(getterName, "get"), "is"));
  }

  /**
   * test if a element name has a corresponding getter.
   *
   * @param typeMirror type mirror of the class
   * @param valueName value element name
   * @return true if getter exists.
   */
  public static boolean hasGetter(final TypeMirror typeMirror, final String valueName) {
    if (isGetter(valueName)) {
      return getGetterNames(typeMirror).stream()
          .filter(getterName -> StringUtils.equals(getterName, valueName)).count() != 0;
    }
    return getGetterNames(typeMirror).stream()
        .filter(getterName -> StringUtils.equals(valueFromGetter(getterName), valueName))
        .count() != 0;
  }

  /**
   * test if a element name has exists.
   *
   * @param typeMirror type mirror of the class
   * @param valueName value element name
   * @return true if element exists.
   */
  public static boolean hasField(final TypeMirror typeMirror, final String valueName) {
    if (isGetter(valueName)) {
      return getGetterNames(typeMirror).stream()
          .filter(getterName -> StringUtils.equals(getterName, valueName)).count() != 0;
    }
    return getFields(typeMirror).stream().map(Element::getSimpleName)
        .filter(elementName -> StringUtils.equals(elementName, valueName)).count() != 0;
  }

  /**
   * getter return type.
   *
   * @param typeMirror type mirror of the class
   * @param valueName value element name
   * @return type mirror of getters return type
   */
  public static Optional<TypeMirror> getterReturnType(final TypeMirror typeMirror,
      final String valueName) {
    if (isGetter(valueName)) {
      return getGetter(typeMirror).stream()
          .filter(element -> StringUtils.equals(getterNameFromElement(element), valueName))
          .map(ExecutableElement::getReturnType).findFirst();
    }
    return getGetter(typeMirror).stream().filter(
        element -> StringUtils.equals(valueFromGetter(getterNameFromElement(element)), valueName))
        .map(ExecutableElement::getReturnType).findFirst();
  }

  /**
   * getter method.
   *
   * @param typeMirror type mirror of the class
   * @param methodName name of the method
   * @return type mirror of getters return type
   */
  public static Optional<ExecutableElement> getMethod(final TypeMirror typeMirror,
      final String methodName) {
    return getMethods(typeMirror).stream()
        .filter(element -> StringUtils.equals(element.getSimpleName().toString(), methodName))
        .findFirst();
  }

  /**
   * element type.
   *
   * @param typeMirror type mirror of the class
   * @param valueName value element name
   * @return type mirror of getters return type
   */
  public static Optional<TypeMirror> fieldType(final TypeMirror typeMirror, final String valueName,
      final ProcessingEnvironment processingEnv) {
    if (isGetter(valueName)) {
      return getGetter(typeMirror).stream()
          .filter(element -> StringUtils.equals(getterNameFromElement(element), valueName))
          .map(ExecutableElement::getReturnType).findFirst();
    }
    return getFields(typeMirror).stream()
        .filter(element -> StringUtils.equals(element.getSimpleName(), valueName))
        .map(element -> processingEnv.getTypeUtils().asMemberOf((DeclaredType) typeMirror, element))
        .findFirst();
  }

  /**
   * element type.
   *
   * @param typeMirror type mirror of the class
   * @param valueName value element name
   * @return type mirror of getters return type
   */
  public static Optional<Element> fieldElementByName(final TypeMirror typeMirror,
      final String valueName) {
    return getFields(typeMirror).stream()
        .filter(element -> StringUtils.equals(element.getSimpleName(), valueName)).findFirst();
  }

  /**
   * get validation interface for type mirror.
   *
   * @param typeMirror to get validation interface from
   * @param typeUtils type utils
   * @return interface name as string
   */
  public static String getValidationInterfaceForType(final TypeMirror typeMirror,
      final Types typeUtils) {
    return "_" + typeUtils.asElement(typeMirror).getSimpleName().toString() + "Validator";
  }

  /**
   * get validation interface for type mirror.
   *
   * @param typeMirror to get validation interface from
   * @param typeUtils type utils
   * @return interface name as string
   */
  public static String getValidationInstanceForType(final TypeMirror typeMirror,
      final Types typeUtils, final Elements elementUtils) {
    final String packageToGenerate =
        elementUtils.getPackageOf(typeUtils.asElement(typeMirror)).getQualifiedName().toString();
    if (StringUtils.isEmpty(packageToGenerate)) {

      return getValidationInterfaceForType(typeMirror, typeUtils) + ".INSTANCE";
    }
    return packageToGenerate + "." + getValidationInterfaceForType(typeMirror, typeUtils)
        + ".INSTANCE";
  }

  /**
   * get validation class for type mirror.
   *
   * @param typeMirror to get validation class from
   * @param typeUtils type utils
   * @return validation class as string
   */
  public static String getValidationClassForType(final TypeMirror typeMirror, final Types typeUtils,
      final Elements elementUtils) {
    return getValidationInterfaceForType(typeMirror, typeUtils) + "Impl";
  }

  /**
   * get type for type mirror.
   *
   * @param typeMirror to get validation class from
   * @return type
   */
  public static Type getType(final TypeMirror typeMirror) {
    return new Type() {
      @Override
      public String getTypeName() {
        return typeMirror.toString();
      }
    };
  }

  /**
   * get element name, even if descriptor is getter, return element name.
   *
   * @param propertyDescription descriptor to test
   * @return name of element
   */
  public static String getElementName(final TypeElementPropertyDescriptor propertyDescription) {
    if (isGetter(propertyDescription.getPropertyName())) {
      return valueFromGetter(propertyDescription.getPropertyName());
    }
    return propertyDescription.getPropertyName();
  }
}

package de.knightsoftnet.validators.annotation.processor;

import java.util.List;

import javax.lang.model.type.TypeMirror;
import jakarta.validation.Valid;

/**
 * Describes a TypeElement property hosting validation constraints.
 *
 * <p>
 * Constraints placed on the attribute and the getter of a given property are all referenced.
 * </p>
 *
 * @author Manfred Tremmel
 */
public interface TypeElementPropertyDescriptor {

  /**
   * Name of the property according to the Java Bean specification.
   *
   * @return property name
   */
  String getPropertyName();

  /**
   * get element type.
   *
   * @return the statically defined returned type
   */
  TypeMirror getElementType();


  /**
   * Returns all constraint descriptors for this element in the class hierarchy or an empty
   * {@code Set} if none are present.
   *
   * @return {@code Set} of constraint descriptors for this element
   */
  List<TypeElementConstraintDescriptor<?>> getConstraintDescriptors();

  /**
   * Whether this element is marked for cascaded validation or not.
   *
   * @return {@code true}, if this element is marked for cascaded validation, {@code false}
   *         otherwise
   */
  boolean isCascaded();

  /**
   * If this element is of a container type, e.g. {@code List} or {@code Map}, a set of descriptors
   * of those container element types is returned, which are constrained or marked with
   * {@link Valid}. A container element type is constrained, if it hosts at least one constraint.
   * <p>
   * In the context of properties and method return values, container element types of super-types
   * are considered.
   * </p>
   *
   * @return the set of descriptors representing the container element types that are constrained or
   *         are marked with {@code Valid}. An empty set will be returned if this element is not of
   *         a container type or is of a container type but there are no container element types
   *         hosting constraints or marked with {@code Valid}.
   */
  List<TypeElementConstraintDescriptor<?>> getConstrainedContainerElementTypes();
}

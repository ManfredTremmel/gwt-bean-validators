package de.knightsoftnet.validators.annotation.processor;

import java.io.IOException;
import java.io.PrintWriter;

import javax.annotation.processing.FilerException;
import javax.annotation.processing.ProcessingEnvironment;
import javax.tools.Diagnostic;
import javax.tools.JavaFileObject;

public class ValidatorInterfaceCreator {

  /**
   * write interface for validator factory.
   *
   * @param packageOfFacatory package of the factory
   * @param classOfFactory class of the factory
   * @param processingEnv processing environment
   */
  public void writeInterface(final String packageOfFacatory, final String classOfFactory,
      final ProcessingEnvironment processingEnv) {
    try {
      final JavaFileObject builderFile =
          processingEnv.getFiler().createSourceFile("de.knightsoftnet.validators.replace."
              + "de.knightsoftnet.validators.client.spi.ValidatorFactoryInterface");
      try (PrintWriter out = new PrintWriter(builderFile.openWriter())) {
        out.println("package de.knightsoftnet.validators.client.spi;");
        out.println();

        out.println("import " + packageOfFacatory + "." + classOfFactory + ";");
        out.println("import de.knightsoftnet.validators.client.AbstractGwtValidatorFactory;");
        out.println();

        out.println("public interface ValidatorFactoryInterface {");

        out.print("  static AbstractGwtValidatorFactory INSTANCE = new ");
        out.print(classOfFactory);
        out.println("();");

        out.println("}");
      }
    } catch (final FilerException e) {
      // happens when trying to recreate an existing interface
      processingEnv.getMessager().printMessage(Diagnostic.Kind.NOTE, e.getMessage());
    } catch (final IOException e) {
      processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR, e.getMessage());
      e.printStackTrace();
    }
  }
}

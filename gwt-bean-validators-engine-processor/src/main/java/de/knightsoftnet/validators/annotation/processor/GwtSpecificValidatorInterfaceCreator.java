package de.knightsoftnet.validators.annotation.processor;

import java.io.IOException;
import java.io.PrintWriter;

import javax.annotation.processing.FilerException;
import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.type.TypeMirror;
import javax.tools.Diagnostic;
import javax.tools.JavaFileObject;

public class GwtSpecificValidatorInterfaceCreator {

  /**
   * write interface for given type mirror.
   *
   * @param typeMirror type mirror of the class to validate
   * @param processingEnv processing environment
   */
  public void writeInterface(final TypeMirror typeMirror,
      final ProcessingEnvironment processingEnv) {
    final String packageToGenerate = processingEnv.getElementUtils()
        .getPackageOf(processingEnv.getTypeUtils().asElement(typeMirror)).getQualifiedName()
        .toString();
    final String interfaceToGenerate =
        TypeUtils.getValidationInterfaceForType(typeMirror, processingEnv.getTypeUtils());

    writeInterface(packageToGenerate, interfaceToGenerate,
        TypeUtils.getClassNameWithProperties(typeMirror), processingEnv);
  }

  private void writeInterface(final String packageToGenerate, final String interfaceToGenerate,
      final String classToValidate, final ProcessingEnvironment processingEnv) {
    try {
      final JavaFileObject builderFile =
          processingEnv.getFiler().createSourceFile(packageToGenerate + "." + interfaceToGenerate);
      try (PrintWriter out = new PrintWriter(builderFile.openWriter())) {
        out.print("package ");
        out.print(packageToGenerate);
        out.println(";");
        out.println();

        out.println("import de.knightsoftnet.validators.client.impl.GwtSpecificValidator;");
        out.println();

        out.print("public interface ");
        out.print(interfaceToGenerate);
        out.print(" extends GwtSpecificValidator<");
        out.print(classToValidate);
        out.println("> {");

        out.print("  static ");
        out.print(interfaceToGenerate);
        out.print(" INSTANCE = new ");
        out.print(interfaceToGenerate);
        out.println("Impl();");

        out.println("}");
      }
    } catch (final FilerException e) {
      // happens when trying to recreate an existing interface
      processingEnv.getMessager().printMessage(Diagnostic.Kind.NOTE, e.getMessage());
    } catch (final IOException e) {
      processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR, e.getMessage());
      e.printStackTrace();
    }
  }
}

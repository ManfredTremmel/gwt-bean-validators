package de.knightsoftnet.validators.annotation.processor;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class ValidationMessagesMap {

  private static final String DEFAULT_LANG = "en";
  private static final Map<String, Map<String, String>> MESSAGES_MAP = new HashMap<>();

  /**
   * add message to message map.
   *
   * @param code code of the message
   * @param localizedMap localized map
   */
  public static void addMessages(final String code, final Map<String, String> localizedMap) {
    MESSAGES_MAP.put(clearCode(code), localizedMap);
  }

  /**
   * add message to message map.
   *
   * @param code code of the message
   * @param locale localization of the message
   * @param localizedMessage localized message
   */
  public static void addMessage(final String code, final Locale locale,
      final String localizedMessage) {
    addMessage(code, locale.toString(), localizedMessage);
  }

  /**
   * add message to message map.
   *
   * @param code code of the message
   * @param lang localization of the message
   * @param localizedMessage localized message
   */
  public static void addMessage(final String code, final String lang,
      final String localizedMessage) {
    Map<String, String> localizedMap = getMessages(code);
    if (localizedMap == null) {
      localizedMap = new HashMap<>();
      MESSAGES_MAP.put(clearCode(code), localizedMap);
    }
    localizedMap.put(lang == null ? "" : lang, localizedMessage);
  }

  /**
   * get localized messages for code.
   *
   * @param code code of the message
   * @return localized message in all available languages
   */
  public static Map<String, String> getMessages(final String code) {
    return MESSAGES_MAP.get(clearCode(code));
  }

  /**
   * get localized message for code.
   *
   * @param code code of the message
   * @param locale localization of the message
   * @return localized message or null if not exists
   */
  public static String getMessage(final String code, final Locale locale) {
    return getMessage(code, locale.toString());
  }

  /**
   * get localized message for code.
   *
   * @param code code of the message
   * @param lang localization of the message
   * @return localized message or null if not exists
   */
  public static String getMessage(final String code, final String lang) {
    final Map<String, String> localizedMap = getMessages(code);
    if (localizedMap == null || localizedMap.isEmpty()) {
      return null;
    }
    if (localizedMap.containsKey(lang)) {
      return localizedMap.get(lang);
    }
    String reducedLang = lang == null ? DEFAULT_LANG : lang;
    while (reducedLang.contains("_")) {
      reducedLang = reducedLang.substring(0, reducedLang.length() - 3);
      if (localizedMap.containsKey(reducedLang)) {
        return localizedMap.get(reducedLang);
      }
    }
    return localizedMap.get(DEFAULT_LANG);
  }

  private static String clearCode(final String code) {
    final int length = code.length();
    if (length < 2) {
      return code;
    }
    return code.substring(code.charAt(0) == '{' ? 1 : 0,
        code.charAt(length - 1) == '}' ? length - 1 : length);
  }

  /**
   * test if message map is empty.
   */
  public static boolean isEmpty() {
    return MESSAGES_MAP.isEmpty();
  }
}

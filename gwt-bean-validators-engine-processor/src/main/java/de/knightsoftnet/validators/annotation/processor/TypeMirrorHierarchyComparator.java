package de.knightsoftnet.validators.annotation.processor;

import java.util.Comparator;

import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Types;

public class TypeMirrorHierarchyComparator implements Comparator<TypeMirror> {

  private final Types typeUtils;

  public TypeMirrorHierarchyComparator(final Types typeUtils) {
    this.typeUtils = typeUtils;
  }

  @Override
  public int compare(final TypeMirror o1, final TypeMirror o2) {
    if (typeUtils.isAssignable(o1, o2)) {
      return -1;
    }
    if (typeUtils.isAssignable(o2, o1)) {
      return 1;
    }
    return 0;
  }
}

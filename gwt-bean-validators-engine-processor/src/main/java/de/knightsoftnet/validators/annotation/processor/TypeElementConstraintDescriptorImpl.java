package de.knightsoftnet.validators.annotation.processor;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.internal.metadata.core.ConstraintOrigin;
import org.hibernate.validator.internal.metadata.location.ConstraintLocation.ConstraintLocationKind;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.lang.model.AnnotatedConstruct;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.AnnotationValue;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Elements;

public class TypeElementConstraintDescriptorImpl<T extends AnnotationMirror>
    implements TypeElementConstraintDescriptor<T> {

  private static final String CONSTRAINT = "jakarta.validation.Constraint";
  private static final String REPORT_AS_SINGLETON = "jakarta.validation.ReportAsSingleViolation";
  private static final String DEFAULT_GROUP = "jakarta.validation.groups.Default";
  private static final Map<String, List<String>> FALLBACK;

  static {
    final ClassLoader loader = TypeElementConstraintDescriptorImpl.class.getClassLoader();
    try (BufferedReader reader = new BufferedReader(new InputStreamReader(
        loader.getResourceAsStream("validatorResolve.properties"), StandardCharsets.UTF_8))) {
      FALLBACK =
          reader.lines().filter(StringUtils::isNotBlank).map(line -> StringUtils.split(line, '='))
              .filter(array -> array.length == 2).collect(Collectors.toMap(array -> array[0],
                  array -> List.of(StringUtils.split(array[1], ','))));
    } catch (final IOException e) {
      throw new RuntimeException(e);
    }
  }

  private final AnnotationMirror annotation;
  private final Map<String, Entry<? extends ExecutableElement, //
      ? extends AnnotationValue>> attributes;
  private final List<TypeMirror> constraintValidatorClasses;
  private final Set<TypeMirror> groups;
  private final List<TypeElementConstraintDescriptor<?>> composingConstraints;
  private final boolean reportAsSingleViolation;
  private final ConstraintLocationKind constraintLocationKind;

  /**
   * default constructor initializing stuff.
   *
   * @param annotation annotation mirror to fill data from
   * @param elementUtils utility for elements
   */
  public TypeElementConstraintDescriptorImpl(final AnnotationMirror annotation,
      final Elements elementUtils, final ConstraintLocationKind constraintLocationKind) {
    this(annotation, elementUtils, constraintLocationKind, null);
  }

  /**
   * default constructor initializing stuff.
   *
   * @param annotation annotation mirror to fill data from
   * @param elementUtils utility for elements
   */
  public TypeElementConstraintDescriptorImpl(final AnnotationMirror annotation,
      final Elements elementUtils, final ConstraintLocationKind constraintLocationKind,
      final Map<String, Entry<? extends ExecutableElement, //
          ? extends AnnotationValue>> overrideFromParent) {
    super();
    this.annotation = annotation;
    final Map<String, Map<String, Entry<? extends ExecutableElement, //
        ? extends AnnotationValue>>> overridesMap = new HashMap<>();
    reportAsSingleViolation = filterConstraint(annotation, REPORT_AS_SINGLETON).isPresent();
    constraintValidatorClasses = createConctraintValidatorClasses(annotation, elementUtils);
    attributes = createAttributes(annotation, elementUtils, overrideFromParent, overridesMap);
    groups = createGroups(attributes, elementUtils);
    composingConstraints =
        constraintLocationKind == ConstraintLocationKind.TYPE ? Collections.emptyList()
            : createComposingConstraintsForElement(annotation.getAnnotationType().asElement(),
                elementUtils, constraintLocationKind, overridesMap);
    this.constraintLocationKind = constraintLocationKind;
  }

  private List<TypeMirror> createConctraintValidatorClasses(final AnnotationMirror annotation,
      final Elements elementUtils) {
    final List<TypeMirror> constraintValidatorClasses = new ArrayList<>();
    final Optional<? extends AnnotationMirror> optionalConstraint =
        filterConstraint(annotation, CONSTRAINT);
    if (optionalConstraint.isPresent()) {
      constraintValidatorClasses.addAll(((List<?>) optionalConstraint.get().getElementValues()
          .values().stream().toList().get(0).getValue()).stream()
              .map(object -> createTypeMirror(object.toString(), elementUtils)).toList());

      // workaround for missing validation classes
      if (constraintValidatorClasses.isEmpty()
          && FALLBACK.containsKey(annotation.getAnnotationType().toString())) {
        constraintValidatorClasses
            .addAll(FALLBACK.get(annotation.getAnnotationType().toString()).stream()
                .map(entry -> elementUtils.getTypeElement(entry) == null ? null
                    : elementUtils.getTypeElement(entry).asType())
                .filter(Objects::nonNull).toList());
      }
    }
    return constraintValidatorClasses;
  }

  private Map<String, Entry<? extends ExecutableElement, //
      ? extends AnnotationValue>> createAttributes(final AnnotationMirror annotation,
          final Elements elementUtils, final Map<String, Entry<? extends ExecutableElement, //
              ? extends AnnotationValue>> overrideFromParent,
          final Map<String, Map<String, Entry<? extends ExecutableElement, //
              ? extends AnnotationValue>>> overridesMap) {
    final Map<String, Entry<? extends ExecutableElement, //
        ? extends AnnotationValue>> attributes = new HashMap<>();
    for (final Entry<? extends ExecutableElement, ? extends AnnotationValue> element : elementUtils
        .getElementValuesWithDefaults(annotation).entrySet()) {

      final String elementName = element.getKey().getSimpleName().toString();
      final Optional<? extends AnnotationMirror> override = TypeUtils
          .getMethod(annotation.getAnnotationType(), elementName).get().getAnnotationMirrors()
          .stream()
          .filter(mirror -> "jakarta.validation.OverridesAttribute".equals(
              ((TypeElement) mirror.getAnnotationType().asElement()).getQualifiedName().toString()))
          .findFirst();
      if (override.isPresent()) {
        String constraint = null;
        String name = null;
        for (final Entry<? extends ExecutableElement, //
            ? extends AnnotationValue> overrideElement : elementUtils
                .getElementValuesWithDefaults(override.get()).entrySet()) {
          final String overrideElementName = overrideElement.getKey().getSimpleName().toString();
          if ("constraint".equals(overrideElementName)) {
            constraint = overrideElement.getValue().getValue().toString();
          } else if ("name".equals(overrideElementName)) {
            name = overrideElement.getValue().getValue().toString();
          }
        }
        final Map<String, Entry<? extends ExecutableElement, //
            ? extends AnnotationValue>> overrideEntry =
                ObjectUtils.defaultIfNull(overridesMap.get(constraint), new HashMap<>());
        overrideEntry.put(name, element);
        overridesMap.put(constraint, overrideEntry);
      }
      if (overrideFromParent == null || overrideFromParent.get(elementName) == null) {
        attributes.put(elementName, element);
      } else {
        attributes.put(elementName, new AbstractMap.SimpleEntry<>(element.getKey(),
            overrideFromParent.get(elementName).getValue()));
      }
    }
    return attributes;
  }

  private Set<TypeMirror> createGroups(final Map<String, Entry<? extends ExecutableElement, //
      ? extends AnnotationValue>> attributes, final Elements elementUtils) {
    final Set<TypeMirror> groups;
    if (CollectionUtils.isEmpty((List<?>) attributes.get("groups").getValue().getValue())) {
      groups = new HashSet<>();
      groups.add(createTypeMirror(DEFAULT_GROUP, elementUtils));
    } else {
      groups = ((List<?>) attributes.get("groups").getValue().getValue()).stream()
          .map(group -> createTypeMirror(group.toString(), elementUtils))
          .collect(Collectors.toSet());
    }
    return groups;
  }

  /**
   * create composing constraints for element.
   *
   * @param element the element to create for
   * @param elementUtils element utils to work with
   * @param overridesMap override maps
   * @return list of TypeElementConstraintDescriptors
   */
  public static List<TypeElementConstraintDescriptor<?>> createComposingConstraintsForElement(
      final Element element, final Elements elementUtils,
      final ConstraintLocationKind constraintLocationKind,
      final Map<String, Map<String, Entry<? extends ExecutableElement, //
          ? extends AnnotationValue>>> overridesMap) {
    final List<TypeElementConstraintDescriptor<?>> composingConstraints = new ArrayList<>();
    for (final AnnotationMirror any : elementUtils.getAllAnnotationMirrors(element)) {
      for (final Entry<? extends ExecutableElement, ? extends AnnotationValue> entry : any
          .getElementValues().entrySet()) {
        final String methodName = entry.getKey().getSimpleName().toString();
        if ("value".equals(methodName) && entry.getValue().getValue() instanceof List) {
          for (final Object annotationMirror : (List<?>) entry.getValue().getValue()) {
            if (annotationMirror instanceof AnnotationMirror
                && filterConstraint((AnnotationMirror) annotationMirror, CONSTRAINT).isPresent()) {
              composingConstraints.add(new TypeElementConstraintDescriptorImpl<>(
                  (AnnotationMirror) annotationMirror, elementUtils, constraintLocationKind,
                  buildOverrides((AnnotationMirror) annotationMirror, overridesMap)));
            }
          }
        }
      }
    }
    composingConstraints
        .addAll(element.getAnnotationMirrors().stream()
            .filter(subannotation -> filterConstraint(subannotation, CONSTRAINT).isPresent())
            .map(subannotation -> new TypeElementConstraintDescriptorImpl<>(subannotation,
                elementUtils, constraintLocationKind, buildOverrides(subannotation, overridesMap)))
            .toList());
    return composingConstraints;
  }

  private static Map<String, Entry<? extends ExecutableElement, //
      ? extends AnnotationValue>> buildOverrides(final AnnotationMirror annotation,
          final Map<String, Map<String, Entry<? extends ExecutableElement, //
              ? extends AnnotationValue>>> overridesMap) {
    return overridesMap.get(annotation.getAnnotationType().toString());
  }

  private static Optional<? extends AnnotationMirror> filterConstraint(
      final AnnotationMirror annotation, final String className) {
    return ((AnnotatedConstruct) annotation.getAnnotationType().asElement()).getAnnotationMirrors()
        .stream()
        .filter(mirror -> className.equals(
            ((TypeElement) mirror.getAnnotationType().asElement()).getQualifiedName().toString()))
        .findAny();
  }

  private TypeMirror createTypeMirror(final String name, final Elements elementUtils) {
    final TypeElement defaultGroup =
        elementUtils.getTypeElement(StringUtils.removeEnd(name, ".class"));
    if (defaultGroup == null) {
      return null;
    }
    return defaultGroup.asType();
  }

  @SuppressWarnings("unchecked")
  @Override
  public T getAnnotation() {
    return (T) annotation;
  }

  @Override
  public Map<String, Entry<? extends ExecutableElement, //
      ? extends AnnotationValue>> getAttributes() {
    return attributes;
  }

  @Override
  public List<TypeElementConstraintDescriptor<?>> getComposingConstraints() {
    return composingConstraints;
  }

  @Override
  public List<TypeMirror> getConstraintValidatorClasses() {
    return constraintValidatorClasses;
  }

  @Override
  public Set<TypeMirror> getGroups() {
    return groups;
  }

  @Override
  public Set<TypeElement> getPayload() {
    // TODO Auto-generated method stub
    return Collections.emptySet();
  }

  @Override
  public boolean isReportAsSingleViolation() {
    return reportAsSingleViolation;
  }

  @Override
  public ConstraintOrigin getDefinedOn() {
    // TODO Auto-generated method stub
    return ConstraintOrigin.DEFINED_LOCALLY;
  }

  @Override
  public ConstraintLocationKind getConstraintLocationKind() {
    return constraintLocationKind;
  }
}

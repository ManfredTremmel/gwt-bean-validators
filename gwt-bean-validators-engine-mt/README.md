gwt-bean-validators-engine-mt
=============================

gwt-bean-validators-engine-mt is a drop-in replacement for [gwt-bean-validators-engine](../gwt-bean-validators-engine) and [gwt-bean-validators-engine-jsr310](../gwt-bean-validators-engine-jsr310). Additional to [gwt-time](https://github.com/foal/gwt-time) integration, it also adds support for all the validators of [mt-bean-validators](../mt-bean-validators).

Supported Validation Annotations
--------------------------------
The list of additional validators are listed in the [mt-bean-validators](../mt-bean-validators) package.


Maven integration
----------------

Add the dependencies itself for GWT-Projects:

```xml
    <dependency>
      <groupId>de.knightsoft-net</groupId>
      <artifactId>gwt-bean-validators-engine-mt</artifactId>
      <version>2.4.1</version>
    </dependency>
```

And exclude **validation-api** from your **gwt-user** dependency (otherwise old 1.0.0.GA will be included):

```xml
    <dependency>
      <groupId>org.gwtproject</groupId>
      <artifactId>gwt-user</artifactId>
      <version>2.12.1</version>
      <exclusions>
        <exclusion>
          <groupId>javax.validation</groupId>
          <artifactId>validation-api</artifactId>
        </exclusion>
      </exclusions>
    </dependency>
```

GWT Integration
---------------

Add this inherits entry to your projects gwt.xml configuration file:

```xml
<inherits name="de.knightsoftnet.validators.GwtBeanValidatorsEngineMT" />
```

The rest is equal to [gwt-bean-validators-engine](../gwt-bean-validators-engine), so read stuff there.

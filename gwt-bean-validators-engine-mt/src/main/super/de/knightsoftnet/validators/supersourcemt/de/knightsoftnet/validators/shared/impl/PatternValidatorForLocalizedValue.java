/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.impl;

import org.apache.commons.lang3.StringUtils;
import org.gwtproject.regexp.shared.MatchResult;
import org.gwtproject.regexp.shared.RegExp;
import org.hibernate.validator.constraintvalidation.HibernateConstraintValidatorContext;
import org.hibernate.validator.internal.engine.messageinterpolation.util.InterpolationHelper;
import org.hibernate.validator.internal.util.logging.Log;
import org.hibernate.validator.internal.util.logging.LoggerFactory;

import java.lang.invoke.MethodHandles;
import java.util.Objects;

import jakarta.validation.ConstraintValidatorContext;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Pattern.Flag;

/**
 * pattern validator.
 *
 * @author Manfred Tremmel - GWT port
 */
public class PatternValidatorForLocalizedValue extends AbstractValidatorForLocalizedValue<Pattern> {

  private static final Log LOG = LoggerFactory.make(MethodHandles.lookup()); // NOPMD

  private RegExp pattern = null;
  private String escapedRegexp;

  @Override
  public void initialize(final Pattern parameters) {
    message = parameters.message();
    final Pattern.Flag[] flags = parameters.flags();
    final StringBuilder flagString = new StringBuilder();
    for (final Pattern.Flag flag : flags) {
      flagString.append(this.toString(flag));
    }
    try {
      pattern = RegExp.compile(parameters.regexp(), flagString.toString());
    } catch (final RuntimeException e) {
      throw LOG.getInvalidRegularExpressionException(e);
    }
    escapedRegexp = InterpolationHelper.escapeMessageParameter(parameters.regexp());
  }

  @Override
  protected void extendValidationContext(
      final ConstraintValidatorContext constraintValidatorContext) {
    if (constraintValidatorContext instanceof HibernateConstraintValidatorContext) {
      constraintValidatorContext.unwrap(HibernateConstraintValidatorContext.class)
          .addMessageParameter("regexp", escapedRegexp);
    }
  }

  @Override
  protected boolean checkEntryValue(final Object value) {
    final MatchResult match = pattern.exec(Objects.toString(value, null));
    return match != null
        && match.getGroup(0).length() == StringUtils.length(Objects.toString(value, null));
  }

  private String toString(final Flag flag) {
    String value;
    switch (flag) {
      case CASE_INSENSITIVE:
      case UNICODE_CASE:
        value = "i";
        break;
      case MULTILINE:
        value = "m";
        break;
      default:
        throw LOG
            .getIllegalArgumentException(flag + " is not a suppoted gwt Pattern (RegExp) flag");
    }
    return value;
  }
}

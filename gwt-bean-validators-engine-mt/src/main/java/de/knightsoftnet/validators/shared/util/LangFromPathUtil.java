/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.util;

import org.gwtproject.regexp.shared.MatchResult;
import org.gwtproject.regexp.shared.RegExp;

/**
 * Language from path extraction util.
 *
 * @author Manfred Tremmel
 *
 */
public class LangFromPathUtil {
  private static final RegExp REG_EXP =
      RegExp.compile(".*\\.localizedText\\[([a-zA-Z_-]*)\\]\\.<map value>");

  /**
   * extract language from path of a localized text.
   *
   * @param path the path to parse
   * @return language or null if none is found
   */
  public static String extract(final String path) {
    final MatchResult result = REG_EXP.exec(path);
    if (result != null && result.getGroupCount() == 2) {
      return result.getGroup(1);
    }
    return null;
  }
}

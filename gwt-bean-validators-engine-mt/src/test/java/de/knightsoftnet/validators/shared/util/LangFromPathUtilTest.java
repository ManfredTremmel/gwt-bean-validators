/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.util;

import org.junit.Assert;
import org.junit.Test;

public class LangFromPathUtilTest {

  @Test
  public void testExtractLanguageOfCorrectPath() {
    // Given
    final String path = "name.localizedText[en].<map value>";

    // When
    final String lang = LangFromPathUtil.extract(path);

    // Then
    Assert.assertEquals("en", lang);
  }

  @Test
  public void testExtractLanguageOfWrongPath() {
    // Given
    final String path = "name";

    // When
    final String lang = LangFromPathUtil.extract(path);

    // Then
    Assert.assertNull(lang);
  }
}

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.client;

import de.knightsoftnet.validators.shared.beans.NotEmptyIfOthersHaveValueTestBean;
import de.knightsoftnet.validators.shared.testcases.NotEmptyIfOthersHaveValueTestCases;

import org.junit.Test;

/**
 * test for not empty if others have value validator.
 *
 * @author Manfred Tremmel
 *
 */
public class GwtTstNotEmptyIfOthersHaveValue
    extends AbstractValidationMtTst<NotEmptyIfOthersHaveValueTestBean> {

  /**
   * tests which should have no validation errors.
   */
  public final void testValueIsSetFieldsToAllowed() {
    for (final NotEmptyIfOthersHaveValueTestBean testBean : NotEmptyIfOthersHaveValueTestCases
        .getCorrectTestBeans()) {
      validationTest(testBean, true, null);
    }
  }

  /**
   * tests which should return validation errors.
   */
  public final void testValueIsSetFieldsNotWrong() {
    for (final NotEmptyIfOthersHaveValueTestBean testBean : NotEmptyIfOthersHaveValueTestCases
        .getWrongEmptyTestBeans()) {
      validationTest(testBean, false,
          "de.knightsoftnet.validators.shared.impl.NotEmptyIfOthersHaveValueValidator");
    }
  }
}

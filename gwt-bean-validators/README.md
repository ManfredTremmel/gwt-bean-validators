gwt-bean-validators
===================

A collection of JSR-303/JSR-349/JSR-380 bean validators that can also be used on the client side of gwt. It also includes missing functionality in gwt like getter reflection, editor component which validates entries on typing, on change or on submit and decorators to display errors inside the form.

This package is a metapackage which includes the functionality of (read documentation there):
* [gwt-bean-validators-engine-mt](../gwt-bean-validators-engine-mt), which includes [gwt-bean-validators-engine-jsr310](./gwt-bean-validators-engine-jsr310) and [gwt-bean-validators-engine](./gwt-bean-validators-engine)
* [gwt-bean-validators-editor](../gwt-bean-validators-editor)
* [gwt-bean-validators-decorators](../gwt-bean-validators-decorators)


Maven integration
----------------

Add the dependencies itself for GWT-Projects:

```xml
    <dependency>
      <groupId>de.knightsoft-net</groupId>
      <artifactId>gwt-bean-validators</artifactId>
      <version>2.4.1</version>
    </dependency>
```

And exclude **validation-api** from your **gwt-user** dependency (otherwise old 1.0.0.GA will be included):

```xml
    <dependency>
      <groupId>org.gwtproject</groupId>
      <artifactId>gwt-user</artifactId>
      <version>2.12.1</version>
      <exclusions>
        <exclusion>
          <groupId>javax.validation</groupId>
          <artifactId>validation-api</artifactId>
        </exclusion>
      </exclusions>
    </dependency>
```
For non GWT-Projects you can use [mt-bean-validators](../mt-bean-validators) instead, which contains only the validators and has no dependencies to gwt.

GWT Integration
---------------

```xml
<inherits name="de.knightsoftnet.validators.GwtBeanValidators" />
```

package de.knightsoftnet.validators.client.editor;

import de.knightsoftnet.validators.client.editor.annotation.IsValidationDriver;
import de.knightsoftnet.validators.shared.beans.SimpleNameTestBean;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class IsValidationDriverGenerationTest {

  @IsValidationDriver(checkTime = CheckTimeEnum.ON_SUBMIT, forceUsingGetter = true)
  interface Driver extends BeanValidationEditorDriver<SimpleNameTestBean, SimpleNameEditor> {
  }

  private Driver driver;
  private SimpleNameEditor editor;
  private SimpleNameTestBean bean;

  @Before
  public void setUp() {
    driver = new IsValidationDriverGenerationTest_Driver_Impl();
    editor = new SimpleNameEditor();
    driver.initialize(editor);
    bean = new SimpleNameTestBean();
  }

  @Test
  public void testEditingWithValidData() {
    // Given
    bean.setFirstName("Firstname");
    bean.setLastName("Lastname");

    // When
    driver.edit(bean);

    // Then
    Assert.assertNotNull(editor);
    Assert.assertEquals(bean.getFirstName(), editor.getValue().getFirstName());
    Assert.assertEquals(bean.getLastName(), editor.getValue().getLastName());
    Assert.assertTrue(driver.validate());
  }

  @Test
  public void testEditingWithInValidData() {
    // Given
    bean.setFirstName("Firstname");
    bean.setLastName(null);

    // When
    driver.edit(bean);

    // Then
    Assert.assertNotNull(editor);
    Assert.assertEquals(bean.getFirstName(), editor.getValue().getFirstName());
    Assert.assertEquals(bean.getLastName(), editor.getValue().getLastName());
    Assert.assertFalse(driver.validate());
    Assert.assertEquals(1, driver.getErrors().size());
  }
}

package de.knightsoftnet.validators.client.editor;

import de.knightsoftnet.validators.shared.beans.SimpleNameTestBean;

import org.gwtproject.editor.client.TakesValue;

public class SimpleNameEditor extends TakesValueEditor<SimpleNameTestBean> {

  public SimpleNameEditor() {
    this(new SimpleNameTakesValue());
  }

  public SimpleNameEditor(final TakesValue<SimpleNameTestBean> peer) {
    super(peer);
  }
}

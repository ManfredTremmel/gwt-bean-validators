package de.knightsoftnet.validators.client.editor;

import de.knightsoftnet.validators.shared.beans.RegisterAccountData;

import org.gwtproject.editor.client.TakesValue;

public class RegisterAccountDataEditor extends TakesValueEditor<RegisterAccountData> {

  public RegisterAccountDataEditor() {
    this(new RegisterAccountDataTakesValue());
  }

  public RegisterAccountDataEditor(final TakesValue<RegisterAccountData> peer) {
    super(peer);
  }
}

package de.knightsoftnet.validators.client.editor;

import de.knightsoftnet.validators.client.editor.annotation.IsValidationDriver;
import de.knightsoftnet.validators.shared.beans.RegisterAccountData;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class IsValidationDriverGenerationRegisterAccountDataTest {

  @IsValidationDriver(checkTime = CheckTimeEnum.ON_SUBMIT, forceUsingGetter = true)
  interface Driver
      extends BeanValidationEditorDriver<RegisterAccountData, RegisterAccountDataEditor> {
  }

  private Driver driver;
  private RegisterAccountDataEditor editor;
  private RegisterAccountData bean;

  @Before
  public void setUp() {
    driver = new IsValidationDriverGenerationRegisterAccountDataTest_Driver_Impl();
    editor = new RegisterAccountDataEditor();
    driver.initialize(editor);
    bean = new RegisterAccountData();
  }

  @Test
  public void testEditingWithValidData() {
    // Given
    bean.setCaptchaResponse("Firstname");

    // When
    driver.edit(bean);

    // Then
    Assert.assertNotNull(editor);
    Assert.assertEquals(bean.getCaptchaResponse(), editor.getValue().getCaptchaResponse());
    Assert.assertFalse(driver.validate());
  }
}

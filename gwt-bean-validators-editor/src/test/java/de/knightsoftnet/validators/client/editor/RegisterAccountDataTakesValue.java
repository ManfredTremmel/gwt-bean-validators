package de.knightsoftnet.validators.client.editor;

import de.knightsoftnet.validators.shared.beans.RegisterAccountData;

import org.gwtproject.editor.client.TakesValue;

public class RegisterAccountDataTakesValue implements TakesValue<RegisterAccountData> {

  private RegisterAccountData value;

  @Override
  public void setValue(final RegisterAccountData value) {
    this.value = value;
  }

  @Override
  public RegisterAccountData getValue() {
    return value;
  }
}

package de.knightsoftnet.validators.client.editor;

import de.knightsoftnet.validators.shared.beans.SimpleNameTestBean;

import org.gwtproject.editor.client.TakesValue;

public class SimpleNameTakesValue implements TakesValue<SimpleNameTestBean> {

  private SimpleNameTestBean value;

  @Override
  public void setValue(final SimpleNameTestBean value) {
    this.value = value;
  }

  @Override
  public SimpleNameTestBean getValue() {
    return value;
  }
}

package de.knightsoftnet.validators.shared.beans;

import de.knightsoftnet.validators.shared.NotEmptyIfOtherHasValue;
import de.knightsoftnet.validators.shared.NotEmptyIfOtherIsEmpty;
import de.knightsoftnet.validators.shared.PhoneNumberValue;
import de.knightsoftnet.validators.shared.data.CountryEnum;

import java.util.Date;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Past;

@PhoneNumberValue(fieldCountryCode = "countryCode", fieldPhoneNumber = "phoneNumber",
    allowDin5008 = false, allowE123 = false, allowUri = false, allowMs = true, allowCommon = false)
@NotEmptyIfOtherHasValue.List({
    @NotEmptyIfOtherHasValue(fieldCompare = "baywaCustomer", valueCompare = "true",
        field = "baywaCustomerNumber", message = "{ValidationMessages.MANDATORY_FIELD_MESSAGE}"),
    @NotEmptyIfOtherHasValue(fieldCompare = "companyType", valueCompare = "Eingetragener_Kfm",
        field = "companyOwner", message = "{ValidationMessages.MANDATORY_FIELD_MESSAGE}"),
    @NotEmptyIfOtherHasValue(fieldCompare = "companyType", valueCompare = "Einzelunternehmer",
        field = "companyOwner", message = "{ValidationMessages.MANDATORY_FIELD_MESSAGE}")})
@NotEmptyIfOtherIsEmpty.List({
    @NotEmptyIfOtherIsEmpty(field = "vatID", fieldCompare = "taxNumber",
        message = "{ValidationMessages.TAX_NUMBER_OR_VAT_ID_FORMAT_MESSAGE}"),
    @NotEmptyIfOtherIsEmpty(field = "taxNumber", fieldCompare = "vatID",
        message = "{ValidationMessages.TAX_NUMBER_OR_VAT_ID_FORMAT_MESSAGE}")})
public class CompanyData {

  @NotEmpty(message = "{ValidationMessages.MANDATORY_FIELD_MESSAGE}")
  private String companyName;

  @NotNull(message = "{ValidationMessages.MANDATORY_FIELD_MESSAGE}")
  private CompanyType companyType;

  private String companyOwner;

  @Past(message = "{ValidationMessages.DATE_PAST_FIELD_MESSAGE}")
  private Date dateOfBirth;

  @NotEmpty(message = "{ValidationMessages.MANDATORY_FIELD_MESSAGE}")
  private String phoneNumber;

  private String website;

  private String taxNumber;

  private String vatID;

  private Boolean baywaCustomer;

  private String baywaCustomerNumber;

  private CountryEnum countryCode = CountryEnum.DE;

  public String getCompanyName() {
    return companyName;
  }

  public void setCompanyName(final String companyName) {
    this.companyName = companyName;
  }

  public CompanyType getCompanyType() {
    return companyType;
  }

  public void setCompanyType(final CompanyType companyType) {
    this.companyType = companyType;
  }

  public String getCompanyOwner() {
    return companyOwner;
  }

  public void setCompanyOwner(final String companyOwner) {
    this.companyOwner = companyOwner;
  }

  public Date getDateOfBirth() {
    return dateOfBirth == null ? null : new Date(dateOfBirth.getTime());
  }

  public void setDateOfBirth(final Date dateOfBirth) {
    this.dateOfBirth = dateOfBirth == null ? null : new Date(dateOfBirth.getTime());
  }

  public String getPhoneNumber() {
    return phoneNumber;
  }

  public void setPhoneNumber(final String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  public String getWebsite() {
    return website;
  }

  public void setWebsite(final String website) {
    this.website = website;
  }

  public String getTaxNumber() {
    return taxNumber;
  }

  public void setTaxNumber(final String taxNumber) {
    this.taxNumber = taxNumber;
  }

  public String getVatID() {
    return vatID;
  }

  public void setVatID(final String vatID) {
    this.vatID = vatID;
  }

  public Boolean getBaywaCustomer() {
    return baywaCustomer;
  }

  public void setBaywaCustomer(final Boolean baywaCustomer) {
    this.baywaCustomer = baywaCustomer;
  }

  public String getBaywaCustomerNumber() {
    return baywaCustomerNumber;
  }

  public void setBaywaCustomerNumber(final String baywaCustomerNumber) {
    this.baywaCustomerNumber = baywaCustomerNumber;
  }

  public CountryEnum getCountryCode() {
    return countryCode;
  }

  public void setCountryCode(final CountryEnum countryCode) {
    this.countryCode = countryCode;
  }
}

package de.knightsoftnet.validators.shared.beans;

import jakarta.validation.Valid;
import jakarta.validation.constraints.AssertTrue;

public class RegisterAccountData {

  @Valid
  private CompanyData companyData;
  @Valid
  private PersonalData personalData;
  @Valid
  private Address invoiceAddress;
  @Valid
  private Address shippingAddress;

  private String captchaResponse;

  private String priceOfferUUID;

  @AssertTrue(message = "{ValidationMessages.MANDATORY_CHECKBOX_MESSAGE}")
  private boolean privacyPolicyAgreement;

  @AssertTrue(message = "{ValidationMessages.MANDATORY_CHECKBOX_MESSAGE}")
  private boolean companyConfirmation;

  private boolean agreeToObtainInformation;

  public boolean isPrivacyPolicyAgreement() {
    return privacyPolicyAgreement;
  }

  public void setPrivacyPolicyAgreement(final boolean privacyPolicyAgreement) {
    this.privacyPolicyAgreement = privacyPolicyAgreement;
  }

  public boolean isCompanyConfirmation() {
    return companyConfirmation;
  }

  public void setCompanyConfirmation(final boolean companyConfirmation) {
    this.companyConfirmation = companyConfirmation;
  }

  public boolean isAgreeToObtainInformation() {
    return agreeToObtainInformation;
  }

  public void setAgreeToObtainInformation(final boolean agreeToObtainInformation) {
    this.agreeToObtainInformation = agreeToObtainInformation;
  }

  public PersonalData getPersonalData() {
    return personalData;
  }

  public void setPersonalData(final PersonalData personalData) {
    this.personalData = personalData;
  }

  public CompanyData getCompanyData() {
    return companyData;
  }

  public void setCompanyData(final CompanyData companyData) {
    this.companyData = companyData;
  }

  public String getCaptchaResponse() {
    return captchaResponse;
  }

  public void setCaptchaResponse(final String captchaResponse) {
    this.captchaResponse = captchaResponse;
  }

  public Address getInvoiceAddress() {
    return invoiceAddress;
  }

  public void setInvoiceAddress(final Address invoiceAddress) {
    this.invoiceAddress = invoiceAddress;
  }

  public Address getShippingAddress() {
    return shippingAddress;
  }

  public void setShippingAddress(final Address shippingAddress) {
    this.shippingAddress = shippingAddress;
  }

  public String getPriceOfferUUID() {
    return priceOfferUUID;
  }

  public void setPriceOfferUUID(final String priceOfferUUID) {
    this.priceOfferUUID = priceOfferUUID;
  }
}

package de.knightsoftnet.validators.shared.beans;

import de.knightsoftnet.validators.shared.LimitCharset;
import de.knightsoftnet.validators.shared.NotEmptyIfOtherHasValue;
import de.knightsoftnet.validators.shared.PostalCode;
import de.knightsoftnet.validators.shared.data.CountryEnum;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

@PostalCode(fieldCountryCode = "countryCode", fieldPostalCode = "postalCode")

@NotEmptyIfOtherHasValue.List({
    @NotEmptyIfOtherHasValue(fieldCompare = "validateOptionalFields", valueCompare = "true",
        field = "firstname", message = "{ValidationMessages.MANDATORY_FIELD_MESSAGE}"),
    @NotEmptyIfOtherHasValue(fieldCompare = "validateOptionalFields", valueCompare = "true",
        field = "lastname", message = "{ValidationMessages.MANDATORY_FIELD_MESSAGE}"),
    @NotEmptyIfOtherHasValue(fieldCompare = "validateOptionalFields", valueCompare = "true",
        field = "company", message = "{ValidationMessages.MANDATORY_FIELD_MESSAGE}"),
    @NotEmptyIfOtherHasValue(fieldCompare = "validateOptionalFields", valueCompare = "true",
        field = "salutation", message = "{ValidationMessages.MANDATORY_FIELD_MESSAGE}")})
public class Address {

  private Long addressID;

  @Size(min = 2, max = 60)
  @LimitCharset(charset = "ISO-8859-1")
  @NotEmpty(message = "{ValidationMessages.MANDATORY_FIELD_MESSAGE}")
  private String street;
  @Size(min = 2, max = 40)
  @LimitCharset(charset = "ISO-8859-1")
  @NotEmpty(message = "{ValidationMessages.MANDATORY_FIELD_MESSAGE}")
  private String city;
  @NotEmpty(message = "{ValidationMessages.MANDATORY_FIELD_MESSAGE}")
  private String postalCode;
  @NotNull(message = "{ValidationMessages.MANDATORY_FIELD_MESSAGE}")
  private CountryEnum countryCode;
  @Size(min = 2, max = 60)
  private String company;
  private String firstname;
  private String lastname;
  private SalutationEnum salutation;
  private boolean validateOptionalFields;

  /**
   * Makes copy of given address.
   *
   * @param addressSource the source address.
   * @return the new address
   */
  public static Address ofAddress(final Address addressSource) {
    final Address address = new Address();
    if (addressSource != null) {
      address.setAddressID(addressSource.getAddressID());
      address.setCity(addressSource.getCity());
      address.setCountryCode(addressSource.getCountryCode());
      address.setPostalCode(addressSource.getPostalCode());
      address.setStreet(addressSource.getStreet());
      address.setFirstname(addressSource.getFirstname());
      address.setLastname(addressSource.getLastname());
      address.setCompany(addressSource.getCompany());
      address.setSalutation(addressSource.getSalutation());
      address.setValidateOptionalFields(addressSource.isValidateOptionalFields());
    }
    return address;
  }

  public Long getAddressID() {
    return addressID;
  }

  public void setAddressID(final Long addressID) {
    this.addressID = addressID;
  }

  public String getStreet() {
    return street;
  }

  public void setStreet(final String street) {
    this.street = street;
  }

  public String getCity() {
    return city;
  }

  public void setCity(final String city) {
    this.city = city;
  }

  public String getPostalCode() {
    return postalCode;
  }

  public void setPostalCode(final String postalCode) {
    this.postalCode = postalCode;
  }

  public CountryEnum getCountryCode() {
    return countryCode;
  }

  public void setCountryCode(final CountryEnum countryCode) {
    this.countryCode = countryCode;
  }

  public String getCompany() {
    return company;
  }

  public void setCompany(final String company) {
    this.company = company;
  }

  public String getFirstname() {
    return firstname;
  }

  public void setFirstname(final String firstname) {
    this.firstname = firstname;
  }

  public String getLastname() {
    return lastname;
  }

  public void setLastname(final String lastname) {
    this.lastname = lastname;
  }

  public SalutationEnum getSalutation() {
    return salutation;
  }

  public void setSalutation(final SalutationEnum salutation) {
    this.salutation = salutation;
  }

  public boolean isValidateOptionalFields() {
    return validateOptionalFields;
  }

  public void setValidateOptionalFields(final boolean validateOptionalFields) {
    this.validateOptionalFields = validateOptionalFields;
  }
}

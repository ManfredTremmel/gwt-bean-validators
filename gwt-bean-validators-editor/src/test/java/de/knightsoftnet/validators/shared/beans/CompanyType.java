package de.knightsoftnet.validators.shared.beans;

public enum CompanyType {
  AG, GmbH, S_E, GmbH_AND_Co_KG, KGAA, KG, OHG, UG, eG_E_Genossenschaft, eV_Eingetr_Verein, Eingetragener_Kfm, OEG_O_Erwerbsgesell, GbR_Ges_bürg_Rechts, Gewerbebetrieb, Einzelunternehmer, Stiftung, Staatl_Einrichtung, UG_AND_Co_KG;
}

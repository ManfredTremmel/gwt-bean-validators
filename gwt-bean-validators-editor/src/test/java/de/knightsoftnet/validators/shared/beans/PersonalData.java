package de.knightsoftnet.validators.shared.beans;

import de.knightsoftnet.validators.shared.Email;
import de.knightsoftnet.validators.shared.MustBeEqual;
import de.knightsoftnet.validators.shared.Password;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

@MustBeEqual(field1 = "password", field2 = "repeatPassword",
    message = "{ValidationMessages.PASSWORDS_MUST_BE_EQAL_FIELD_MESSAGE}")
public class PersonalData {

  @NotNull(message = "{ValidationMessages.MANDATORY_FIELD_MESSAGE}")
  private SalutationEnum salutation;
  @NotEmpty(message = "{ValidationMessages.MANDATORY_FIELD_MESSAGE}")
  private String firstname;
  @NotEmpty(message = "{ValidationMessages.MANDATORY_FIELD_MESSAGE}")
  private String lastname;
  @Email
  @NotEmpty(message = "{ValidationMessages.MANDATORY_FIELD_MESSAGE}")
  private String email;
  @Password(minRules = 3)
  @NotEmpty(message = "{ValidationMessages.MANDATORY_FIELD_MESSAGE}")
  @Size(min = 8, max = 60)
  private String password;
  @Size(min = 8, max = 60)
  @NotEmpty(message = "{ValidationMessages.MANDATORY_FIELD_MESSAGE}")
  private String repeatPassword;

  private Long customerID;
  private Integer sapCustomerID;
  private boolean registered;

  public SalutationEnum getSalutation() {
    return salutation;
  }

  public void setSalutation(final SalutationEnum salutation) {
    this.salutation = salutation;
  }

  public String getFirstname() {
    return firstname;
  }

  public void setFirstname(final String firstname) {
    this.firstname = firstname;
  }

  public String getLastname() {
    return lastname;
  }

  public void setLastname(final String lastname) {
    this.lastname = lastname;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(final String email) {
    this.email = email;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(final String password) {
    this.password = password;
  }

  public String getRepeatPassword() {
    return repeatPassword;
  }

  public void setRepeatPassword(final String repeatPassword) {
    this.repeatPassword = repeatPassword;
  }

  public Integer getSapCustomerID() {
    return sapCustomerID;
  }

  public void setSapCustomerID(final Integer sapCustomerID) {
    this.sapCustomerID = sapCustomerID;
  }

  public Long getCustomerID() {
    return customerID;
  }

  public void setCustomerID(final Long customerID) {
    this.customerID = customerID;
  }

  public boolean isRegistered() {
    return registered;
  }

  public void setRegistered(final boolean registered) {
    this.registered = registered;
  }
}

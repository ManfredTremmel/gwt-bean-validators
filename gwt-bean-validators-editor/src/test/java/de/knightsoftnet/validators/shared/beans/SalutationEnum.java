package de.knightsoftnet.validators.shared.beans;

public enum SalutationEnum {
  MR, MRS;
}

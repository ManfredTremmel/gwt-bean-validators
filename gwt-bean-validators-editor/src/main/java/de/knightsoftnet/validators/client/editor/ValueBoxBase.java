package de.knightsoftnet.validators.client.editor;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.HasChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.i18n.client.AutoDirectionHandler;
import com.google.gwt.i18n.client.BidiPolicy;
import com.google.gwt.i18n.client.BidiUtils;
import com.google.gwt.i18n.shared.DirectionEstimator;
import com.google.gwt.i18n.shared.HasDirectionEstimator;
import com.google.gwt.text.shared.Parser;
import com.google.gwt.text.shared.Renderer;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.ChangeListener;
import com.google.gwt.user.client.ui.FocusWidget;
import com.google.gwt.user.client.ui.HasName;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.ListenerWrapper;
import com.google.gwt.user.client.ui.impl.TextBoxImpl;

import org.gwtproject.editor.client.IsEditor;
import org.gwtproject.editor.client.TakesValue;

import java.text.ParseException;

/**
 * Abstract base class for all text entry widgets.
 *
 * <b>Use in UiBinder Templates</b>
 *
 * @param <T> the value type
 */
public class ValueBoxBase<T> extends FocusWidget
    implements HasChangeHandlers, HasName, HasDirectionEstimator, TakesValue<T>, HasValue<T>,
    HasText, AutoDirectionHandler.Target, IsEditor<ValueBoxEditor<T>> {

  /**
   * Alignment values for {@link ValueBoxBase#setAlignment}.
   */
  public enum TextAlignment {
    /**
     * centered text.
     */
    CENTER {
      @Override
      String getTextAlignString() {
        return "center";
      }
    },
    /**
     * justified text.
     */
    JUSTIFY {
      @Override
      String getTextAlignString() {
        return "justify";
      }
    },
    /**
     * left text.
     */
    LEFT {
      @Override
      String getTextAlignString() {
        return "left";
      }
    },
    /**
     * right text.
     */
    RIGHT {
      @Override
      String getTextAlignString() {
        return "right";
      }

    };

    abstract String getTextAlignString();
  }

  private static TextBoxImpl impl = GWT.create(TextBoxImpl.class);
  private final AutoDirectionHandler autoDirHandler;

  private final Parser<T> parser;
  private final Renderer<T> renderer;
  private final HasValue<T> oldBox;
  private ValueBoxEditor<T> editor;
  private Event currentEvent;

  private boolean valueChangeHandlerInitialized;

  /**
   * Creates a value box that wraps the given browser element handle. This is only used by
   * subclasses.
   *
   * @param elem the browser element to wrap
   * @param renderer the renderer of the value to text
   * @param parser the parser of the value from text
   */
  protected ValueBoxBase(final Element elem, final Renderer<T> renderer, final Parser<T> parser) {
    super(elem);
    autoDirHandler = AutoDirectionHandler.addTo(this, BidiPolicy.isBidiEnabled());
    this.renderer = renderer;
    this.parser = parser;
    oldBox = null;
  }

  /**
   * Creates a value box that wraps the given browser element handle. This is only used by
   * subclasses.
   *
   * @param elem the browser element to wrap
   * @param oldBox old gwt element to wrap
   */
  protected ValueBoxBase(final Element elem, final HasValue<T> oldBox) {
    super(elem);
    autoDirHandler = AutoDirectionHandler.addTo(this, BidiPolicy.isBidiEnabled());
    renderer = null;
    parser = null;
    this.oldBox = oldBox;
  }

  public static <T> ValueBoxBase<T> oldToNew(final Element elem, final HasValue<T> oldBox) {
    return new ValueBoxBase<>(elem, oldBox);
  }

  public static <T> ValueBoxBase<T> oldToNew(
      final com.google.gwt.user.client.ui.ValueBoxBase<T> oldBox) {
    return new ValueBoxBase<>(oldBox.getElement(), oldBox);
  }

  @Override
  public HandlerRegistration addChangeHandler(final ChangeHandler handler) {
    return addDomHandler(handler, ChangeEvent.getType());
  }

  @Override
  public HandlerRegistration addValueChangeHandler(final ValueChangeHandler<T> handler) {
    // Initialization code
    if (!valueChangeHandlerInitialized) {
      valueChangeHandlerInitialized = true;
      addChangeHandler(event -> ValueChangeEvent.fire(this, getValue()));
    }
    return addHandler(handler, ValueChangeEvent.getType());
  }

  /**
   * Returns an Editor that is backed by the ValueBoxBase. The default implementation returns
   * {@link ValueBoxEditor#of(ValueBoxBase)}. Subclasses may override this method to provide custom
   * error-handling when using the Editor framework.
   */
  @Override
  public ValueBoxEditor<T> asEditor() {
    if (editor == null) {
      editor = ValueBoxEditor.of(this);
    }
    return editor;
  }

  /**
   * If a keyboard event is currently being handled on this text box, calling this method will
   * suppress it. This allows listeners to easily filter keyboard input.
   */
  public void cancelKey() {
    if (currentEvent != null) {
      currentEvent.preventDefault();
    }
  }

  /**
   * Gets the current position of the cursor (this also serves as the beginning of the text
   * selection).
   *
   * @return the cursor's position
   */
  public int getCursorPos() {
    return impl.getCursorPos(getElement());
  }

  @Override
  public Direction getDirection() {
    return BidiUtils.getDirectionOnElement(getElement());
  }

  /**
   * Gets the direction estimation model of the auto-dir handler.
   */
  @Override
  public DirectionEstimator getDirectionEstimator() {
    return autoDirHandler.getDirectionEstimator();
  }

  @Override
  public String getName() {
    return getElement().getPropertyString("name");
  }

  /**
   * Gets the text currently selected within this text box.
   *
   * @return the selected text, or an empty string if none is selected
   */
  public String getSelectedText() {
    final int start = getCursorPos();
    if (start < 0) {
      return "";
    }
    final int length = getSelectionLength();
    return getText().substring(start, start + length);
  }

  /**
   * Gets the length of the current text selection.
   *
   * @return the text selection length
   */
  public int getSelectionLength() {
    return impl.getSelectionLength(getElement());
  }

  @Override
  public String getText() {
    return getElement().getPropertyString("value");
  }

  /**
   * Return the parsed value, or null if the field is empty or parsing fails.
   *
   * @return value of the base box
   */
  @Override
  public T getValue() {
    try {
      return getValueOrThrow();
    } catch (final ParseException e) {
      return null;
    }
  }

  /**
   * Return the parsed value, or null if the field is empty.
   *
   * @return value of the base box
   * @throws ParseException if the value cannot be parsed
   */
  public T getValueOrThrow() throws ParseException {
    if (oldBox != null) {
      if (oldBox instanceof final com.google.gwt.user.client.ui.ValueBoxBase<T> oldValueBox) {
        return oldValueBox.getValueOrThrow();
      }
      return oldBox.getValue();
    }
    final String text = getText();

    final T parseResult = parser.parse(text);

    if ("".equals(text)) {
      return null;
    }

    return parseResult;
  }

  /**
   * Determines whether or not the widget is read-only.
   *
   * @return <code>true</code> if the widget is currently read-only, <code>false</code> if the
   *         widget is currently editable
   */
  public boolean isReadOnly() {
    return getElement().getPropertyBoolean("readOnly");
  }

  @Override
  public void onBrowserEvent(final Event event) {
    final int type = DOM.eventGetType(event);
    if ((type & Event.KEYEVENTS) != 0) {
      // Fire the keyboard event. Hang on to the current event object so that
      // cancelKey() and setKey() can be implemented.
      currentEvent = event;
      // Call the superclass' onBrowserEvent as that includes the key event
      // handlers.
      super.onBrowserEvent(event);
      currentEvent = null;
    } else {
      // Handles Focus and Click events.
      super.onBrowserEvent(event);
    }
  }

  /**
   * remove change listener.
   *
   * @param listener the change listener to remove
   * @deprecated Use the {@link HandlerRegistration#removeHandler} method on the object returned by
   *             {@link #addChangeHandler} instead
   */
  @Deprecated
  public void removeChangeListener(final ChangeListener listener) {
    ListenerWrapper.WrappedChangeListener.remove(this, listener);
  }

  /**
   * Selects all of the text in the box.
   *
   * <p>
   * This will only work when the widget is attached to the document and not hidden.
   * </p>
   */
  public void selectAll() {
    final int length = getText().length();
    if (length > 0) {
      setSelectionRange(0, length);
    }
  }

  /**
   * Set text alignment for input widget.
   *
   * @param align the text alignment enum
   */
  public void setAlignment(final TextAlignment align) {
    getElement().getStyle().setProperty("textAlign", align.getTextAlignString());
  }

  /**
   * Sets the cursor position.
   *
   * <p>
   * This will only work when the widget is attached to the document and not hidden.
   * </p>
   *
   * @param pos the new cursor position
   */
  public void setCursorPos(final int pos) {
    setSelectionRange(pos, 0);
  }

  @Override
  public void setDirection(final Direction direction) {
    BidiUtils.setDirectionOnElement(getElement(), direction);
  }

  /**
   * Toggles on / off direction estimation.
   */
  @Override
  public void setDirectionEstimator(final boolean enabled) {
    autoDirHandler.setDirectionEstimator(enabled);
  }

  /**
   * Sets the direction estimation model of the auto-dir handler.
   */
  @Override
  public void setDirectionEstimator(final DirectionEstimator directionEstimator) {
    autoDirHandler.setDirectionEstimator(directionEstimator);
  }

  /**
   * If a keyboard event is currently being handled by the text box, this method replaces the
   * unicode character or key code associated with it. This allows listeners to easily filter
   * keyboard input.
   *
   * @param key the new key value
   * @deprecated this method only works in IE and should not have been added to the API
   */
  @Deprecated
  public void setKey(final char key) {
    if (currentEvent != null) {
      DOM.eventSetKeyCode(currentEvent, key);
    }
  }

  @Override
  public void setName(final String name) {
    getElement().setPropertyString("name", name);
  }

  /**
   * Turns read-only mode on or off.
   *
   * @param readOnly if <code>true</code>, the widget becomes read-only; if <code>false</code> the
   *        widget becomes editable
   */
  public void setReadOnly(final boolean readOnly) {
    getElement().setPropertyBoolean("readOnly", readOnly);
    final String readOnlyStyle = "readonly";
    if (readOnly) {
      addStyleDependentName(readOnlyStyle);
    } else {
      removeStyleDependentName(readOnlyStyle);
    }
  }

  /**
   * Sets the range of text to be selected.
   *
   * <p>
   * This will only work when the widget is attached to the document and not hidden.
   * </p>
   *
   * @param pos the position of the first character to be selected
   * @param length the number of characters to be selected
   */
  public void setSelectionRange(final int pos, final int length) {
    // Setting the selection range will not work for unattached elements.
    if (!isAttached()) {
      return;
    }

    if (length < 0) {
      throw new IndexOutOfBoundsException("Length must be a positive integer. Length: " + length);
    }
    if (pos < 0 || length + pos > getText().length()) {
      throw new IndexOutOfBoundsException("From Index: " + pos + "  To Index: " + (pos + length)
          + "  Text Length: " + getText().length());
    }
    impl.setSelectionRange(getElement(), pos, length);
  }

  /**
   * Sets this object's text. Note that some browsers will manipulate the text before adding it to
   * the widget. For example, most browsers will strip all <code>\r</code> from the text, except IE
   * which will add a <code>\r</code> before each <code>\n</code>. Use {@link #getText()} to get the
   * text directly from the widget.
   *
   * @param text the object's new text
   */
  @Override
  public void setText(final String text) {
    getElement().setPropertyString("value", text != null ? text : "");
    autoDirHandler.refreshDirection();
  }

  @Override
  public void setValue(final T value) {
    setValue(value, false);
  }

  @Override
  public void setValue(final T value, final boolean fireEvents) {
    if (oldBox != null) {
      oldBox.setValue(value, fireEvents);
    }
    if (renderer != null) {
      final T oldValue = fireEvents ? getValue() : null;
      setText(renderer.render(value));
      if (fireEvents) {
        final T newValue = getValue();
        ValueChangeEvent.fireIfNotEqual(this, oldValue, newValue);
      }
    }
  }

  /**
   * Get text box implementation.
   *
   * @return TextBoxImpl
   */
  protected TextBoxImpl getImpl() {
    return impl;
  }

  @Override
  protected void onLoad() {
    super.onLoad();
    autoDirHandler.refreshDirection();
  }

  /**
   * Gheck if value box base has a parser.
   *
   * @return true if parser exists
   */
  public boolean hasParser() {
    return parser != null;
  }
}

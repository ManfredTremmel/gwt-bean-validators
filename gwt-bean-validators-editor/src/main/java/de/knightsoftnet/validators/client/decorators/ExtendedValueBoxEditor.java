/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.client.decorators;

import de.knightsoftnet.validators.client.editor.ValueBoxBase;
import de.knightsoftnet.validators.client.editor.ValueBoxEditor;

import org.gwtproject.editor.client.EditorDelegate;
import org.gwtproject.editor.client.TakesValue;

import java.text.ParseException;

/**
 * Extended version of the value box editor with delegation support to another element.
 *
 * @author Manfred Tremmel
 *
 * @param <T> type of the value to handle
 */
public class ExtendedValueBoxEditor<T> extends ValueBoxEditor<T> {

  private final TakesValue<T> takesValues;
  private EditorDelegate<T> delegate;
  private final IsDecorator<T> decorator;

  /**
   * constructor uses widget as base.
   *
   * @param takesValues widget which is able to set and get value from/to
   * @param decorator corresponding decorator
   */
  public ExtendedValueBoxEditor(final TakesValue<T> takesValues, final IsDecorator<T> decorator) {
    this(takesValues, decorator,
        takesValues instanceof final ValueBoxBase<T> valueBox ? valueBox.asEditor().getDelegate()
            : null);
  }

  /**
   * constructor uses widget as base.
   *
   * @param takesValues widget which is able to set and get value from/to
   * @param decorator corresponding decorator
   * @param delegate delegate
   */
  public ExtendedValueBoxEditor(final TakesValue<T> takesValues, final IsDecorator<T> decorator,
      final EditorDelegate<T> delegate) {
    super(null);
    this.takesValues = takesValues;
    this.decorator = decorator;
    this.delegate = delegate;
  }

  @Override
  public EditorDelegate<T> getDelegate() {
    return delegate;
  }

  @Override
  public T getValue() {
    T value = null;
    if (takesValues instanceof final ValueBoxBase<T> valueBoxBase && valueBoxBase.hasParser()) {
      try {
        value = valueBoxBase.getValueOrThrow();
      } catch (final ParseException e) {
        if (delegate != null) {
          final String entryAsText = valueBoxBase.getText();
          delegate.recordError("Bad value (" + entryAsText + ")", entryAsText, e);
        }
      }
    } else {
      value = takesValues.getValue();
    }
    return value;
  }

  @Override
  public void setDelegate(final EditorDelegate<T> delegate) {
    this.delegate = delegate;
  }

  @Override
  public void setValue(final T value) {
    takesValues.setValue(value);
    if (decorator != null) {
      decorator.clearErrors();
    }
  }

  /**
   * getter for the decorator.
   *
   * @return the decorator
   */
  public final IsDecorator<T> getDecorator() {
    return decorator;
  }

  /**
   * getter for the widget which takes the values.
   *
   * @return the takesValues
   */
  public final TakesValue<T> getTakesValues() {
    return takesValues;
  }
}

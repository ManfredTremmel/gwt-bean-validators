/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.client.decorators;

import de.knightsoftnet.validators.client.editor.ValueBoxEditor;

import com.google.gwt.event.dom.client.HasKeyPressHandlers;
import com.google.gwt.event.dom.client.HasKeyUpHandlers;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Focusable;
import com.google.gwt.user.client.ui.HasEnabled;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.Widget;

import org.gwtproject.editor.client.HasEditorErrors;
import org.gwtproject.editor.client.IsEditor;

/**
 * Interface for classes for decorators.
 *
 * @param <T> the type of data being edited
 * @author Manfred Tremmel
 *
 */
public interface IsDecorator<T>
    extends HasEditorErrors<T>, IsEditor<ValueBoxEditor<T>>, HasValue<T>, HasValueChangeHandlers<T>,
    HasKeyUpHandlers, HasKeyPressHandlers, Focusable, HasEnabled {

  /**
   * get the flow panel.
   *
   * @return flow panel
   */
  FlowPanel getLayout();

  /**
   * Set the widget that the EditorPanel will display. This method will automatically call
   * setEditor.
   *
   * @param widget a {@link IsEditor} widget
   */
  void setChildWidget(final Widget widget);

  /**
   * set focus on error flag, if it's true, the widget get's the focus if validation finds an error.
   *
   * @param focusOnError set focus on error when true, otherwise don't
   */
  void setFocusOnError(final boolean focusOnError);

  /**
   * is focus on error.
   *
   * @return true when focus is set
   */
  boolean isFocusOnError();

  /**
   * clear errors.
   */
  void clearErrors();
}

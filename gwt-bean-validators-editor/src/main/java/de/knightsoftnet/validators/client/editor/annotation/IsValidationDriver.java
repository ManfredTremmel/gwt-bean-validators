package de.knightsoftnet.validators.client.editor.annotation;

import de.knightsoftnet.validators.client.editor.CheckTimeEnum;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

import jakarta.validation.groups.Default;

/**
 * Marks a editor validation driver interface as being usable and causes a generated implementation
 * of it to be created, with required supporting classes.
 */
@Documented
@Target(ElementType.TYPE)
public @interface IsValidationDriver {

  /**
   * The list of Groups which can be processed by the annotated {@code Validator}. The default value
   * is {@link Default}. An empty array is illegal.
   *
   * @return array of classes
   */
  Class<?>[] groups() default {Default.class};

  /**
   * time of validation check.
   *
   * @return CheckTimeEnum
   */
  CheckTimeEnum checkTime() default CheckTimeEnum.ON_KEY_UP;

  /**
   * submit unchanged forms.
   *
   * @return boolean, default false
   */
  boolean submitUnchanged() default false;

  /**
   * submit form on return press.
   *
   * @return boolean, default true
   */
  boolean submitOnReturn() default true;

  /**
   * submit form on value change.
   *
   * @return boolean, default false
   */
  boolean submitOnValueChange() default false;

  /**
   * force using getter for accessing field values.
   *
   * @return boolean, true when only using getter
   */
  boolean forceUsingGetter() default false;

  /**
   * generate reflection getter for class based validation.
   *
   * @return boolean, true when generating reflection getter
   */
  boolean generateReflectionGetter() default true;

  /**
   * The list of languages to generate messages for.
   *
   * @return array of strings
   */
  String[] languages() default {"de", "fr", "en"};
}

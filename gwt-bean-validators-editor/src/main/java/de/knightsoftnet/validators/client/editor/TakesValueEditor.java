package de.knightsoftnet.validators.client.editor;

import org.gwtproject.editor.client.LeafValueEditor;
import org.gwtproject.editor.client.TakesValue;

/**
 * Adapts the {@link TakesValue} interface to the Editor framework.
 *
 * @param <T> the type of value to be edited
 */
public class TakesValueEditor<T> implements LeafValueEditor<T> {
  /**
   * Returns a new ValueEditor that modifies the given {@link TakesValue} peer instance.
   *
   * @param peer a {@link TakesValue} instance
   * @param <T> the type of value to be edited
   * @return a TakesValueEditor instance of the same type as its peer
   */
  public static <T> TakesValueEditor<T> of(final TakesValue<T> peer) {
    return new TakesValueEditor<>(peer);
  }

  private final TakesValue<T> peer;

  /**
   * Returns a new ValueEditor that modifies the given {@link TakesValue} peer instance.
   *
   * @param peer a {@link TakesValue} instance
   */
  protected TakesValueEditor(final TakesValue<T> peer) {
    this.peer = peer;
  }

  @Override
  public T getValue() {
    return peer.getValue();
  }

  @Override
  public void setValue(final T value) {
    peer.setValue(value);
  }
}

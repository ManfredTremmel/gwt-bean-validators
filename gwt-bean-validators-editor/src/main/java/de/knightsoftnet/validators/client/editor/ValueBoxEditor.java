package de.knightsoftnet.validators.client.editor;

import org.gwtproject.editor.client.EditorDelegate;
import org.gwtproject.editor.client.HasEditorDelegate;

import java.text.ParseException;

/**
 * Adapts the {@link ValueBoxBase} interface to the Editor framework. This adapter uses
 * {@link ValueBoxBase#getValueOrThrow()} to report parse errors to the Editor framework.
 *
 * @param <T> the type of value to be edited
 */
public class ValueBoxEditor<T> extends TakesValueEditor<T> implements HasEditorDelegate<T> {

  /**
   * Returns a new TakesValueEditor that adapts a {@link ValueBoxBase} instance.
   *
   * @param valueBox a {@link ValueBoxBase} instance to adapt
   * @return a ValueBoxEditor instance of the same type as the adapted {@link ValueBoxBase} instance
   */
  public static <T> ValueBoxEditor<T> of(final ValueBoxBase<T> valueBox) {
    return new ValueBoxEditor<>(valueBox);
  }

  private EditorDelegate<T> delegate;
  private final ValueBoxBase<T> peer;
  private T value;

  /**
   * Constructs a new ValueBoxEditor that adapts a {@link ValueBoxBase} peer instance.
   *
   * @param peer a {@link ValueBoxBase} instance of type T
   */
  protected ValueBoxEditor(final ValueBoxBase<T> peer) {
    super(peer);
    this.peer = peer;
  }

  /**
   * Returns the {@link EditorDelegate} for this instance.
   *
   * @return an {@link EditorDelegate}, or {@code null}
   * @see #setDelegate(EditorDelegate)
   */
  public EditorDelegate<T> getDelegate() {
    return delegate;
  }

  /**
   * Calls {@link ValueBoxBase#getValueOrThrow()}. If a {@link ParseException} is thrown, it will be
   * available through {@link com.google.gwt.editor.client.EditorError#getUserData()
   * EditorError.getUserData()}.
   *
   * @return a value of type T
   * @see #setValue(Object)
   */
  @Override
  public T getValue() {
    try {
      value = peer.getValueOrThrow();
    } catch (final ParseException e) {
      // TODO i18n
      getDelegate().recordError("Bad value (" + peer.getText() + ")", peer.getText(), e);
    }
    return value;
  }

  /**
   * Sets the {@link EditorDelegate} for this instance. This method is only called by the driver.
   *
   * @param delegate an {@link EditorDelegate}, or {@code null}
   * @see #getDelegate()
   */
  @Override
  public void setDelegate(final EditorDelegate<T> delegate) {
    this.delegate = delegate;
  }

  @Override
  public void setValue(final T value) {
    peer.setValue(this.value = value);
  }
}

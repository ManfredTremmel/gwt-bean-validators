package de.knightsoftnet.validators.client.editor.impl;

import org.gwtproject.editor.client.impl.SimpleViolation;

import java.util.Iterator;

import jakarta.validation.ConstraintViolation;

/**
 * Abstraction of a ConstraintViolation or a RequestFactory Violation object. Also contains a
 * factory method to create SimpleViolation instances from {@link ConstraintViolation} objects.
 * Extended for jakarta validation implementation.
 */
public abstract class AbstractJakartaSimpleViolation extends SimpleViolation {
  /**
   * Provides a source of SimpleViolation objects based on ConstraintViolations. This is re-used by
   * the RequestFactoryEditorDriver implementation, which does not share a type hierarchy with the
   * SimpleBeanEditorDriver.
   */
  static class JakartaConstraintViolationIterable implements Iterable<SimpleViolation> {

    private final Iterable<ConstraintViolation<?>> violations;

    public JakartaConstraintViolationIterable(final Iterable<ConstraintViolation<?>> violations) {
      this.violations = violations;
    }

    @Override
    public Iterator<SimpleViolation> iterator() {
      // Use a fresh source iterator each time
      final Iterator<ConstraintViolation<?>> source = violations.iterator();
      return new Iterator<>() {
        @Override
        public boolean hasNext() {
          return source.hasNext();
        }

        @Override
        public SimpleViolation next() {
          return new SimpleViolationAdapter(source.next());
        }

        @Override
        public void remove() {
          source.remove();
        }
      };
    }
  }

  /** Adapts the ConstraintViolation interface to the SimpleViolation interface. */
  static class SimpleViolationAdapter extends SimpleViolation {
    private final ConstraintViolation<?> violation;

    public SimpleViolationAdapter(final ConstraintViolation<?> violation) {
      this.violation = violation;
    }

    @Override
    public Object getKey() {
      return violation.getRootBean();
    }

    @Override
    public String getMessage() {
      return violation.getMessage();
    }

    @Override
    public String getPath() {
      return violation.getPropertyPath().toString();
    }

    @Override
    public Object getUserDataObject() {
      return violation;
    }
  }

  public static Iterable<SimpleViolation> iterableFromJakartaConstrantViolations(
      final Iterable<ConstraintViolation<?>> violations) {
    return new JakartaConstraintViolationIterable(violations);
  }
}

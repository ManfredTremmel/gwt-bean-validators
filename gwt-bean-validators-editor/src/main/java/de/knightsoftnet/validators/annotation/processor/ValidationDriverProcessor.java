package de.knightsoftnet.validators.annotation.processor;


import de.knightsoftnet.validators.client.editor.BeanValidationEditorDriver;
import de.knightsoftnet.validators.client.editor.annotation.IsValidationDriver;

import com.google.auto.service.AutoService;

import org.gwtproject.editor.processor.model.EditorModel;
import org.gwtproject.editor.processor.model.EditorTypes;
import org.gwtproject.editor.shaded.com.squareup.javapoet.ClassName;

import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Processor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;

/**
 * Validation driver processor.
 *
 * @author Manfred Tremmel
 */
@SupportedAnnotationTypes("de.knightsoftnet.validators.client.editor.annotation.IsValidationDriver")
@SupportedSourceVersion(SourceVersion.RELEASE_17)
@AutoService(Processor.class)
public class ValidationDriverProcessor extends AbstractProcessor {

  @Override
  public boolean process(final Set<? extends TypeElement> annotations,
      final RoundEnvironment roundEnv) {
    if (annotations == null || annotations.isEmpty() || roundEnv == null || processingEnv == null) {
      return false;
    }
    if (!roundEnv.processingOver()) {
      for (final TypeElement annotation : annotations) {
        if (IsValidationDriver.class.getCanonicalName()
            .equals(TypeUtils.getClassName(annotation))) {
          for (final Element element : roundEnv
              .getElementsAnnotatedWith(IsValidationDriver.class)) {
            final EditorModel rootEditorModel = new EditorModel(processingEnv.getMessager(),
                new EditorTypes(processingEnv.getTypeUtils(), processingEnv.getElementUtils()),
                element.asType(),
                processingEnv.getTypeUtils()
                    .erasure(processingEnv.getElementUtils()
                        .getTypeElement(ClassName.get(BeanValidationEditorDriver.class).toString())
                        .asType()));
            final ValidationDriverClassCreator classCreator =
                new ValidationDriverClassCreator(processingEnv);
            classCreator.generate((TypeElement) element, rootEditorModel);
          }
        }
      }
    }
    return true;
  }
}

package de.knightsoftnet.validators.annotation.processor;

import de.knightsoftnet.validators.client.editor.BeanValidationEditorDriver;
import de.knightsoftnet.validators.client.editor.annotation.IsValidationDriver;
import de.knightsoftnet.validators.client.editor.impl.AbstractBeanValidationEditorDelegate;
import de.knightsoftnet.validators.client.editor.impl.AbstractBeanValidationEditorDriver;
import de.knightsoftnet.validators.client.impl.AbstractGwtValidator;

import org.gwtproject.editor.client.Editor;
import org.gwtproject.editor.client.EditorVisitor;
import org.gwtproject.editor.client.impl.AbstractEditorContext;
import org.gwtproject.editor.client.impl.RootEditorContext;
import org.gwtproject.editor.processor.NameFactory;
import org.gwtproject.editor.processor.model.EditorModel;
import org.gwtproject.editor.processor.model.EditorProperty;
import org.gwtproject.editor.shaded.com.google.auto.common.MoreTypes;
import org.gwtproject.editor.shaded.com.squareup.javapoet.ClassName;
import org.gwtproject.editor.shaded.com.squareup.javapoet.FieldSpec;
import org.gwtproject.editor.shaded.com.squareup.javapoet.JavaFile;
import org.gwtproject.editor.shaded.com.squareup.javapoet.MethodSpec;
import org.gwtproject.editor.shaded.com.squareup.javapoet.ParameterizedTypeName;
import org.gwtproject.editor.shaded.com.squareup.javapoet.TypeName;
import org.gwtproject.editor.shaded.com.squareup.javapoet.TypeSpec;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringJoiner;
import java.util.stream.Collectors;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;
import javax.tools.Diagnostic;
import jakarta.validation.ConstraintViolation;

/**
 * Validation Driver Class Creator.
 *
 * @author Manfred Tremmel
 */
public class ValidationDriverClassCreator {

  private static final GwtSpecificValidatorInterfaceCreator INTERFACE_CREATOR =
      new GwtSpecificValidatorInterfaceCreator();

  private final ProcessingEnvironment processingEnv;
  private final List<String> generatedDelegates;
  private final Set<TypeMirror> validGroups;

  /**
   * constructor.
   *
   * @param processingEnv processing environment
   */
  public ValidationDriverClassCreator(final ProcessingEnvironment processingEnv) {
    super();
    this.processingEnv = processingEnv;
    generatedDelegates = new ArrayList<>();
    validGroups = new HashSet<>();
  }

  /**
   * Getter for driver interface type.
   *
   * @return ClassName of the driver interface
   */
  protected ClassName getDriverInterfaceType() {
    return ClassName.get(BeanValidationEditorDriver.class);
  }

  /**
   * Getter for driver superclass type.
   *
   * @return ClassName of the driver superclass
   */
  protected ClassName getDriverSuperclassType() {
    return ClassName.get(AbstractBeanValidationEditorDriver.class);
  }

  /**
   * Getter for editor delegate type.
   *
   * @return ClassName of the editor delegate
   */
  protected ClassName getEditorDelegateType() {
    return ClassName.get(AbstractBeanValidationEditorDelegate.class);
  }

  private void createMessage(final Diagnostic.Kind kind, final String message) {
    final StringWriter sw = new StringWriter();
    final PrintWriter pw = new PrintWriter(sw);
    pw.println(message);
    pw.close();
    processingEnv.getMessager().printMessage(kind, sw.toString());
  }

  /**
   * generate driver class.
   *
   * @param interfaceToImplement interface to implement
   * @param rootEditorModel root editor model
   */
  public void generate(final TypeElement interfaceToImplement, final EditorModel rootEditorModel) {
    // start driver
    final String pkgName = processingEnv.getElementUtils().getPackageOf(interfaceToImplement)
        .getQualifiedName().toString();
    final String typeName = createNameFromEnclosedTypes(interfaceToImplement, "_Impl");

    final TypeName classToEditName = TypeName.get(rootEditorModel.getProxyType());

    final IsValidationDriver isValidationDriver =
        interfaceToImplement.getAnnotation(IsValidationDriver.class);
    validGroups.clear();
    validGroups.addAll(ApUtils.getTypeMirrorFromAnnotationValue(() -> isValidationDriver.groups()));

    // generate validation interfaces and classes
    createValidationInterfaceAndClass(rootEditorModel.getProxyType(),
        isValidationDriver.forceUsingGetter(), isValidationDriver.generateReflectionGetter(),
        isValidationDriver.languages());

    // default constructor
    final MethodSpec constructor = MethodSpec.constructorBuilder().addModifiers(Modifier.PUBLIC)
        .addStatement("super()")
        .addStatement("setCheckTime(de.knightsoftnet.validators.client.editor.CheckTimeEnum."
            + isValidationDriver.checkTime() + ")")
        .addStatement("setSubmitUnchanged(" + isValidationDriver.submitUnchanged() + ")")
        .addStatement("setSubmitOnReturn(" + isValidationDriver.submitOnReturn() + ")")
        .addStatement("setSubmitOnValueChange(" + isValidationDriver.submitOnValueChange() + ")")
        .addStatement("setValidationGroups("
            + validGroups.stream().map(typeMirror -> TypeUtils.getClassName(typeMirror) + ".class")
                .collect(Collectors.joining(",%n"))
            + ")")
        .build();

    // impl accept(visitor) method
    final ParameterizedTypeName rootEdContextType =
        ParameterizedTypeName.get(ClassName.get(RootEditorContext.class), classToEditName);
    final MethodSpec accept =
        MethodSpec.methodBuilder("accept").addModifiers(Modifier.PUBLIC).returns(void.class)
            .addAnnotation(Override.class).addParameter(EditorVisitor.class, "visitor")
            // ugly cast to shut up java warnings at compile time - however, this might be overkill,
            // could just use raw types
            .addStatement("$T ctx = new $T(getDelegate(), (Class<$T>)(Class)$L.class, getObject())",
                rootEdContextType, rootEdContextType, classToEditName,
                MoreTypes.asElement(rootEditorModel.getProxyType()))
            .addStatement("ctx.traverse(visitor, getDelegate())").build();

    // impl createDelegate() method
    // - lazily building the delegate type if require (this is recursive), see
    // com.google.gwt.editor.rebind.AbstractEditorDriverGenerator.getEditorDelegate()
    // - build context
    // - break out various impl methods to allow custom EditorDriver subtypes like RFED
    final ParameterizedTypeName delegateType = ParameterizedTypeName.get(getEditorDelegateType(),
        classToEditName, TypeName.get(rootEditorModel.getEditorType()));
    final MethodSpec createDelegate =
        MethodSpec.methodBuilder("createDelegate").addModifiers(Modifier.PROTECTED)
            .returns(delegateType).addAnnotation(Override.class).addStatement("return new $T()",
                getEditorDelegate(rootEditorModel, rootEditorModel.getRootData()))
            .build();


    // impl validateContent(object, validator) method
    final TypeName returnType = ParameterizedTypeName.get(ClassName.get(Set.class),
        ParameterizedTypeName.get(ClassName.get(ConstraintViolation.class), classToEditName));
    final MethodSpec validateContent = MethodSpec.methodBuilder("validateContent")
        .addModifiers(Modifier.PROTECTED).returns(returnType).addAnnotation(Override.class)
        .addParameter(classToEditName, "object", Modifier.FINAL)
        .addParameter(ClassName.get(AbstractGwtValidator.class), "validator", Modifier.FINAL)
        .addStatement(String.format(
            "de.knightsoftnet.validators.client.impl.GwtValidationContext<%1$s> context%n"
                + " = new de.knightsoftnet.validators.client.impl.GwtValidationContext<%1$s>(%n"
                + "(Class<%1$s>) %1$s.class,%nobject,%n%2$s.getConstraints("
                + "validator.getValidationGroupsMetadata()),%nvalidator.getMessageInterpolator(),%n"
                + "validator.getTraversableResolver(),%nvalidator)",
            classToEditName.toString(),
            TypeUtils.getValidationInstanceForType(rootEditorModel.getProxyType(),
                processingEnv.getTypeUtils(), processingEnv.getElementUtils())))
        .addStatement(
            String.format("return %2$s.validate(context,%n(%1$s) object, this.validationGroups)",
                classToEditName.toString(),
                TypeUtils.getValidationInstanceForType(rootEditorModel.getProxyType(),
                    processingEnv.getTypeUtils(), processingEnv.getElementUtils())))
        .build();

    // implement interface, extend BaseEditorDriver or what not
    final TypeSpec driverType = TypeSpec.classBuilder(typeName)
        .addOriginatingElement(interfaceToImplement).addModifiers(Modifier.PUBLIC)
        // Once GWT supports the new package of the Generated class
        // we can uncomment this code
        // .addAnnotation(
        // AnnotationSpec.builder(getGeneratedClassName())
        // .addMember("value", "\"$L\"",
        // DriverProcessor.class.getCanonicalName())
        // .build())
        .addSuperinterface(TypeName.get(interfaceToImplement.asType()))
        .superclass(ParameterizedTypeName.get(getDriverSuperclassType(), classToEditName,
            TypeName.get(rootEditorModel.getEditorType())))
        .addMethod(constructor) //
        .addMethod(accept) //
        .addMethod(createDelegate) //
        .addMethod(validateContent) //
        .build();

    final JavaFile driverFile = JavaFile.builder(pkgName, driverType).build();

    try {
      driverFile.writeTo(processingEnv.getFiler());
    } catch (final IOException e) {
      createMessage(Diagnostic.Kind.WARNING,
          "Exception: type >>" + rootEditorModel.getEditorType().toString()
              + " << - trying to write: >>" + driverFile.packageName + "." + driverType
              + "<< -> message >>" + e.getMessage() + "<< multiple times");
    }
  }

  private void createValidationInterfaceAndClass(final TypeMirror validationClassMirror,
      final boolean forceUsingGetter, final boolean generateReflectionGetter,
      final String[] languages) {
    INTERFACE_CREATOR.writeInterface(validationClassMirror, processingEnv);
    final GwtSpecificValidatorClassCreator classCreator = new GwtSpecificValidatorClassCreator(
        processingEnv, validGroups, forceUsingGetter, generateReflectionGetter, languages);
    classCreator.writeClass(validationClassMirror)
        .forEach(mirror -> createValidationInterfaceAndClass(mirror, forceUsingGetter,
            generateReflectionGetter, languages));
  }

  /**
   * Joins the name of the type with any enclosing types, with "_" as the delimiter, and appends an
   * optional suffix.
   */
  private String createNameFromEnclosedTypes(final TypeElement interfaceToImplement,
      final String suffix) {
    final StringJoiner joiner = new StringJoiner("_", "", suffix == null ? "" : suffix);
    ClassName.get(interfaceToImplement).simpleNames().forEach(joiner::add);
    return joiner.toString();
  }

  private ClassName getEditorDelegate(final EditorModel editorModel, final EditorProperty data) {
    final String delegateSimpleName = escapedMaybeParameterizedBinaryName(data.getEditorType())
        + "_" + getEditorDelegateType().simpleName();
    final String packageName = processingEnv.getElementUtils()
        .getPackageOf(processingEnv.getTypeUtils().asElement(data.getEditorType()))
        .getQualifiedName().toString();

    final String delegateClassName = packageName + "." + delegateSimpleName;
    // check, if delegate is already generated ...
    if (generatedDelegates.contains(delegateClassName)) {
      // already generated ... nothing to do
      return ClassName.get(packageName, delegateSimpleName);
    }
    generatedDelegates.add(delegateClassName);

    final TypeName rawEditorType =
        TypeName.get(processingEnv.getTypeUtils().erasure(data.getEditorType()));

    final TypeSpec.Builder delegateTypeBuilder = TypeSpec.classBuilder(delegateSimpleName)
        .addOriginatingElement(processingEnv.getTypeUtils().asElement(data.getEditedType()))
        .addOriginatingElement(processingEnv.getTypeUtils().asElement(data.getEditorType()))
        .addModifiers(Modifier.PUBLIC)
        // Once GWT supports the new package of the Generated class
        // we can uncomment this code
        // .addAnnotation(
        // AnnotationSpec.builder(getGeneratedClassName())
        // .addMember("value", "\"$L\"",
        // DriverProcessor.class.getCanonicalName())
        // .build())
        .superclass(getEditorDelegateType()); // raw type here, for the same reason as above

    final NameFactory names = new NameFactory();
    final Map<EditorProperty, String> delegateFields = new IdentityHashMap<>();

    delegateTypeBuilder
        .addField(FieldSpec.builder(rawEditorType, "editor", Modifier.PRIVATE).build());
    names.addName("editor");
    delegateTypeBuilder.addField(
        FieldSpec.builder(TypeName.get(data.getEditedType()), "object", Modifier.PRIVATE).build());
    names.addName("object");

    // Fields for the sub-delegates that must be managed
    for (final EditorProperty d : editorModel.getEditorData(data.getEditorType())) {
      if (d.isDelegateRequired()) {
        final String fieldName = names.createName(d.getPropertyName() + "Delegate");
        delegateFields.put(d, fieldName);
        delegateTypeBuilder.addField(getEditorDelegateType(), fieldName, Modifier.PRIVATE);
        // TODO parameterize
      }
    }

    delegateTypeBuilder.addMethod(MethodSpec.methodBuilder("getEditor")
        .addModifiers(Modifier.PROTECTED).returns(rawEditorType).addAnnotation(Override.class)
        .addStatement("return editor").build());

    delegateTypeBuilder
        .addMethod(MethodSpec.methodBuilder("setEditor").addModifiers(Modifier.PROTECTED)
            .returns(void.class).addAnnotation(Override.class).addParameter(Editor.class, "editor")
            .addStatement("this.editor = ($T) editor", rawEditorType).build());

    delegateTypeBuilder.addMethod(MethodSpec.methodBuilder("getObject")
        .addModifiers(Modifier.PUBLIC).returns(TypeName.get(data.getEditedType()))
        .addAnnotation(Override.class).addStatement("return object").build());

    delegateTypeBuilder.addMethod(
        MethodSpec.methodBuilder("setObject").addModifiers(Modifier.PROTECTED).returns(void.class)
            .addAnnotation(Override.class).addParameter(ClassName.get(Object.class), "object")
            .addStatement("this.object = ($T) object", TypeName.get(data.getEditedType())).build());

    final MethodSpec.Builder initializeSubDelegatesBuilder =
        MethodSpec.methodBuilder("initializeSubDelegates").addModifiers(Modifier.PROTECTED)
            .returns(void.class).addAnnotation(Override.class);
    if (data.isCompositeEditor()) {
      initializeSubDelegatesBuilder.addStatement("createChain($L.class)",
          MoreTypes.asElement(data.getComposedData().getEditedType()));
    }
    for (final EditorProperty d : editorModel.getEditorData(data.getEditorType())) {
      final ClassName subDelegateType = getEditorDelegate(editorModel, d);
      if (d.isDelegateRequired()) {
        initializeSubDelegatesBuilder
            .beginControlFlow("if (editor.$L != null)", d.getSimpleExpression())
            .addStatement("$L = new $T()", delegateFields.get(d), subDelegateType)
            .addStatement("addSubDelegate($L, appendPath(\"$L\"), editor.$L)",
                delegateFields.get(d), d.getDeclaredPath(), d.getSimpleExpression())
            .endControlFlow();
      }
    }
    delegateTypeBuilder.addMethod(initializeSubDelegatesBuilder.build());

    final MethodSpec.Builder acceptBuilder =
        MethodSpec.methodBuilder("accept").addModifiers(Modifier.PUBLIC).returns(void.class)
            .addAnnotation(Override.class).addParameter(EditorVisitor.class, "visitor");
    if (data.isCompositeEditor()) {
      acceptBuilder.addStatement("getEditorChain().accept(visitor)");
    }
    for (final EditorProperty d : editorModel.getEditorData(data.getEditorType())) {
      if (d.isDelegateRequired()) {
        acceptBuilder.beginControlFlow("if ($L != null)", delegateFields.get(d));
      } else {
        acceptBuilder.beginControlFlow("");
      }
      final ClassName editorContextName = getEditorContext(data, d);
      acceptBuilder.addStatement("$T ctx = new $T(getObject(), editor.$L, appendPath(\"$L\"))",
          editorContextName, editorContextName, d.getSimpleExpression(), d.getDeclaredPath());
      if (d.isDelegateRequired()) {
        acceptBuilder.addStatement("ctx.setEditorDelegate($L)", delegateFields.get(d));
        acceptBuilder.addStatement("ctx.traverse(visitor, $L)", delegateFields.get(d));
      } else {
        acceptBuilder.addStatement("ctx.traverse(visitor, null)");
      }
      acceptBuilder.endControlFlow();
    }

    delegateTypeBuilder.addMethod(acceptBuilder.build());

    if (data.isCompositeEditor()) {
      final ClassName compositeEditorDelegateType =
          getEditorDelegate(editorModel, data.getComposedData());
      delegateTypeBuilder.addMethod(
          MethodSpec.methodBuilder("createComposedDelegate").addModifiers(Modifier.PROTECTED)
              .returns(compositeEditorDelegateType).addAnnotation(Override.class)
              .addStatement("return new $T()", compositeEditorDelegateType).build());
    }

    final JavaFile delegateFile =
        JavaFile.builder(packageName, delegateTypeBuilder.build()).build();
    try {
      delegateFile.writeTo(processingEnv.getFiler());
    } catch (final IOException e) {
      createMessage(Diagnostic.Kind.WARNING,
          "Exception: type >>" + editorModel.getEditorType().toString()
              + " << - trying to write: >>" + delegateFile.packageName + "." + delegateSimpleName
              + "<< -> message >>" + e.getMessage() + "<< multiple times");
    }

    return ClassName.get(packageName, delegateSimpleName);
  }

  private String escapedMaybeParameterizedBinaryName(final TypeMirror editor) {
    /*
     * The parameterization of the editor type is included to ensure that a correct specialization
     * of a CompositeEditor will be generated. For example, a ListEditor<Person, APersonEditor>
     * would need a different delegate from a ListEditor<Person, AnotherPersonEditor>.
     */
    final StringBuilder maybeParameterizedName =
        new StringBuilder(createNameFromEnclosedTypes(MoreTypes.asTypeElement(editor), null));

    // recursive departure from gwt, in case we have ListEditor<Generic<Foo>, GenericEditor<Foo>>,
    // etc
    for (final TypeMirror typeParameterElement : MoreTypes.asDeclared(editor).getTypeArguments()) {
      maybeParameterizedName.append("$")
          .append(escapedMaybeParameterizedBinaryName(typeParameterElement));
    }
    return escapedBinaryName(maybeParameterizedName.toString());
  }

  private String escapedBinaryName(final String binaryName) {
    return binaryName.replace("_", "_1").replace('$', '_').replace('.', '_');
  }

  private ClassName getEditorContext(final EditorProperty parent, final EditorProperty data) {
    final String contextSimpleName = escapedMaybeParameterizedBinaryName(parent.getEditorType())
        + "_" + data.getDeclaredPath().replace("_", "_1").replace(".", "_") + "_Context";
    final String packageName = processingEnv.getElementUtils()
        .getPackageOf(processingEnv.getTypeUtils().asElement(parent.getEditorType()))
        .getQualifiedName().toString();

    // try {
    final TypeSpec.Builder contextTypeBuilder = TypeSpec.classBuilder(contextSimpleName)
        .addOriginatingElement(processingEnv.getTypeUtils().asElement(parent.getEditorType()))
        .addOriginatingElement(processingEnv.getTypeUtils().asElement(parent.getEditedType()))
        .addOriginatingElement(processingEnv.getTypeUtils().asElement(data.getEditedType()))
        .addModifiers(Modifier.PUBLIC)
        // Once GWT supports the new package of the Generated class
        // we can uncomment this code
        // .addAnnotation(
        // AnnotationSpec.builder(getGeneratedClassName())
        // .addMember("value", "\"$L\"",
        // DriverProcessor.class.getCanonicalName())
        // .build())
        .superclass(ParameterizedTypeName.get(ClassName.get(AbstractEditorContext.class),
            TypeName.get(data.getEditedType())));

    contextTypeBuilder.addField(TypeName.get(parent.getEditedType()), "parent", Modifier.PRIVATE,
        Modifier.FINAL);

    contextTypeBuilder.addMethod(MethodSpec.constructorBuilder().addModifiers(Modifier.PUBLIC)
        .addParameter(TypeName.get(parent.getEditedType()), "parent")
        .addParameter(ParameterizedTypeName.get(ClassName.get(Editor.class),
            TypeName.get(data.getEditedType())), "editor")
        .addParameter(String.class, "path").addStatement("super(editor, path)")
        .addStatement("this.parent = parent").build());

    contextTypeBuilder.addMethod(MethodSpec.methodBuilder("canSetInModel")
        .addModifiers(Modifier.PUBLIC).addAnnotation(Override.class).returns(boolean.class)
        .addStatement("return parent != null && $L && $L",
            data.getSetterName() == null ? "false" : "true", data.getBeanOwnerGuard("parent"))
        .build());

    contextTypeBuilder.addMethod(MethodSpec.methodBuilder("checkAssignment")
        .addModifiers(Modifier.PUBLIC).addAnnotation(Override.class)
        .returns(TypeName.get(data.getEditedType())).addParameter(Object.class, "value")
        .addStatement("return ($T) value", TypeName.get(data.getEditedType())).build());

    contextTypeBuilder.addMethod(MethodSpec.methodBuilder("getEditedType")
        .addModifiers(Modifier.PUBLIC).addAnnotation(Override.class).returns(Class.class)
        .addStatement("return $L.class", MoreTypes.asElement(data.getEditedType())).build());

    contextTypeBuilder
        .addMethod(MethodSpec.methodBuilder("getFromModel").addModifiers(Modifier.PUBLIC)
            .addAnnotation(Override.class).returns(TypeName.get(data.getEditedType()))
            .addStatement("return (parent != null && $L) ? parent$L$L : null",
                data.getBeanOwnerGuard("parent"), data.getBeanOwnerExpression(),
                data.getGetterExpression())
            .build());

    final MethodSpec.Builder setInModelMethodBuilder = MethodSpec.methodBuilder("setInModel")
        .addModifiers(Modifier.PUBLIC).addAnnotation(Override.class).returns(void.class)
        .addParameter(TypeName.get(data.getEditedType()), "data");
    if (data.getSetterName() == null) {
      setInModelMethodBuilder.addStatement("throw new UnsupportedOperationException()");
    } else {
      setInModelMethodBuilder.addStatement("parent$L.$L(data)", data.getBeanOwnerExpression(),
          data.getSetterName());
    }
    contextTypeBuilder.addMethod(setInModelMethodBuilder.build());

    final JavaFile contextFile = JavaFile.builder(packageName, contextTypeBuilder.build()).build();
    try {
      contextFile.writeTo(processingEnv.getFiler());
    } catch (final IOException e) {
      createMessage(Diagnostic.Kind.WARNING,
          "Exception: type >>" + parent.getEditorType().toString() + " << - trying to write: >>"
              + contextFile.packageName + "." + contextSimpleName + "<< -> message >>"
              + e.getMessage() + "<< multiple times");
    }
    // } catch (Exception ignored) {
    // System.out.println("Exception");
    // already exists, ignore
    // }

    return ClassName.get(packageName, contextSimpleName);
  }
}

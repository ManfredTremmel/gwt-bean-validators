gwt-bean-validators-editor
==========================

The driver is based on the SimpleEditorDriver from [gwtproject/gwt-editor](https://github.com/gwtproject/gwt-editor) project, it provides not only the binding of a java bean to a input form, it also includes the capability to validate the input and set validation state to the input widgets.

With version 1.5.0, I've switched from the gwt integrated code generator to java annotation processing code generation, to work with upcomming gwt 3. This causes some changes on upgrade to this version, described below.


Maven integration
----------------

Add the dependencies itself for GWT-Projects:

```xml
    <dependency>
      <groupId>de.knightsoft-net</groupId>
      <artifactId>gwt-bean-validators-editor</artifactId>
      <version>2.4.1</version>
    </dependency>
```

You also need a validation engine to do the work, see:
* [gwt-bean-validators-engine](../gwt-bean-validators-engine)
* [gwt-bean-validators-engine-jsr310](../gwt-bean-validators-engine-jsr310)
* [gwt-bean-validators-engine-mt](../gwt-bean-validators-engine-mt)


GWT Integration
---------------

Add this inherits entry to your projects gwt.xml configuration file:

```xml
<inherits name="de.knightsoftnet.validators.GwtBeanValidatorsEditor" />
```

In the View you use it, define a new Driver which extends `BeanValidationEditorDriver` and has two parameters, the bean and the view to bind:

```java
public class ExampleView

  @IsValidationDriver
  interface Driver extends BeanValidationEditorDriver<ExampleData, ExampleView> {}
...
```

The driver generation can be configured by some parameters in the `@IsValidationDriver`:
* groups: array of validation groups to use on validation, default value is `javax.validation.groups.Default.class`
* checkTime: enum which defines, what time the validation is triggered, default value is `ON_KEY_UP`, possible options are:
  - ON_KEY_UP: each time a key up event is triggered, the validation is started, this results in a on the fly validation on typing
  - ON_CHANGE: each time a input widget sends a change event the validation is done, this happens when widget looses focus and value has changed. This happens when you leave a widget with tab, or klick outside the widget
  - ON_SUBMIT: validation is only triggered when the form is sent, to make this work correctly, you have to tell the editor which button triggers submit
* submitUnchanged: boolen which defines if a unchanged form can be submitted, default is `false`. In case of false a form also can't be submitted two times with a doubleclick on submit button, second time the form entries match the bean values, so no submit is triggered
* submitOnReturn: boolean, trigger submit on return, default is `true`. "Normal" html forms can be submitted by pressing return in an input widget, in JavaScript based forms this isn't the case, this option brings the known feature back
* submitOnValueChange: boolean, trigger submit on value change event, default is `false`
* forceUsingGetter: boolean, hint for validation code generator to use getters to access values and do not generate JSNI workaround to access private fields, default is `false`
* generateReflectionGetter: boolean, hint for validation code generator to generate reflection helpers using getters to access values by name, default is `true`. The reflection helper stuff is generated for all constrained beans (class based valdiation or `@Valid` annotation), if you are sure, you don't need it, you can disable it with this option
* languages: array of strings with the languages, the message for this languages are integrated into the generated code, default is `{"de", "fr", "en"}`

The code-generator now creates the needed code in a class which is named as the View concatenated with the extension `_Driver_Impl`, in this case this results in a class named `ExampleView_Driver_Impl`. This driver now can simply instantiated with new (no GWT.create required) or injected if you use the GIN-injection framework.


```java
  @UiField
  Button submitButton;

  private Driver driver;
...
  public ExampleView() {
...
    driver = new ExampleView_Driver_Impl();
    driver.initialize(this);
    driver.setSubmitButton(submitButton);
    driver.addFormSubmitHandler(this::onFormSubmit);
...
  }

  public void onFormSubmit(final FormSubmitEvent<LoginData> event) {
    // handle submitted form data, this is only triggered, when validation was successful!
  }
```

When you set the submit button to the driver, it handles the en- and disables for you, when ever the submit is possible (disable on validation error, if you don't use check ON_SUBMIT and disabled on unchanged forms, if not activated).

The form submit handler is triggered when ever the form can be successful submitted, in this can be sure, the bean has passed all validations the `FormSubmitEvent` itself contains the bean, but it's also synced by editor, so you can work with the bean you've you can also work with it.

Additional to the driver itself, the code to validate `ExampleData` is generated the driver calls this directly, so you don't need to define a validation factory in your project. Without this centralized validation instance, the validation code is also splittable, so the whole validation code no longer lands in initial- or leftover-fragment of the application.

New in version 1.5.0 or later
-----------------------------
Up to 1.4.4 the `BeanValidationEditorDriver` was generated by gwt code generator, which is incompatible with the GWT 3.0, so it was time to replace it by annotation driven generator.
In case of of using the `BeanValidationEditorDriver`, you have to extend the Driver definition by a `@IsValidationDriver` annotation and instead of creating a instance by `GWT.create(Driver.class)`, you can instantiate the generated Driver by new. All you need to now is the name of the generated class and this is simply the name of the class in which you've defined the driver, combined with the ending `_Driver_Impl`. So e.g. if you defined the driver in `LoginView` the generated driver is `LoginView_Driver_Impl`. The generated driver is based on [gwtproject/gwt-editor](https://github.com/gwtproject/gwt-editor), so make sure you don't combine it with the gwt-user editors.

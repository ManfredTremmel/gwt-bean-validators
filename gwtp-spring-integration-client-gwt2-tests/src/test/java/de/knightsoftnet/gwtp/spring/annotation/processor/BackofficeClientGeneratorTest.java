package de.knightsoftnet.gwtp.spring.annotation.processor;

import static com.google.testing.compile.Compiler.javac;

import de.knightsoftnet.validators.annotation.processor.ValidationDriverProcessor;
import de.knightsoftnet.validators.shared.data.CountryEnum;

import com.google.testing.compile.Compilation;
import com.google.testing.compile.CompilationSubject;
import com.google.testing.compile.JavaFileObjects;

import org.gwtproject.editor.processor.DriverProcessor;
import org.junit.jupiter.api.Test;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.tools.JavaFileObject;
import javax.tools.StandardLocation;

/**
 * compile test with code generation.
 */
class BackofficeClientGeneratorTest {
  final JavaFileObject source = JavaFileObjects
      .forSourceString("de.knightsoftnet.gwtp.spring.annotation.processor.GeneratingBackoffice", """
          package de.knightsoftnet.gwtp.spring.annotation.processor;

          import static de.knightsoftnet.gwtp.spring.client.annotation.BackofficeClientGenerator.*;

          import de.knightsoftnet.gwtp.spring.client.annotation.BackofficeClientGenerator;
          import de.knightsoftnet.gwtp.spring.client.annotation.SearchField;

          public class GeneratingBackoffice {
            @BackofficeClientGenerator(
                value = "/api/rest/person",
                baseToken = "/person/",
                languages = {"en", "de"},
                searchResults = {
                    @SearchField(field = "id", gridColumns = 1),
                    @SearchField(field = "firstName", gridColumns = 3),
                    @SearchField(field = "lastName", gridColumns = 3),
                    @SearchField(field = "email", gridColumns = 3),
                    @SearchField(field = "birthday", gridColumns = 2)
                },
                uiBinderHeader = {GSSS_UIB_RESOURCE_FILE,
                    "<ui:style src=\\\"../../basepage/Global.gss\\\"/>"},
                uiBinderStyleCentralPanel = "{style.centerPanel}",
                uiBinderStyleRow = GSSS_UIB_ROW + " {style.row}",
                uiBinderStyleHeader = GSSS_UIB_COLUMN + " " + GSSS_UIB_COLUMN_12
                    + " {style.header}",
                uiBinderErrorStyle = "{style.errorPanel}"
                )
            private Person person;
          }""");

  final String restService = """
      package de.knightsoftnet.gwtp.spring.annotation.processor;

      import de.knightsoftnet.gwtp.spring.annotation.processor.Person;
      import de.knightsoftnet.mtwidgets.client.services.AdminService;

      import javax.ws.rs.Path;

      @Path("/api/rest/person")
      public interface PersonRestService extends AdminService<Person> {
      }""";

  final String costCenterEntityWidgetRestService = """
      package de.knightsoftnet.gwtp.spring.annotation.processor;

      import de.knightsoftnet.gwtp.spring.annotation.processor.CostCenterEntity;
      import de.knightsoftnet.mtwidgets.client.services.ManyToOneSuggestionRestService;

      import java.util.List;
      import javax.ws.rs.GET;
      import javax.ws.rs.Path;
      import javax.ws.rs.PathParam;

      @Path("/api/rest/costcenterentitywidget")
      public interface CostCenterEntityWidgetRestService
          extends ManyToOneSuggestionRestService<CostCenterEntity> {

        @Override
        @GET
        @Path("/{number}")
        CostCenterEntity getReferenceByKey(@PathParam("number") final String key);

        @Override
        @GET
        @Path("/suggestions/{number}")
        List<CostCenterEntity> findByKeyContaining(@PathParam("number") final String key);
      }""";

  final String costCenterEntityWidget =
      """
          package de.knightsoftnet.gwtp.spring.annotation.processor;

          import de.knightsoftnet.gwtp.spring.annotation.processor.CostCenterEntity;
          import de.knightsoftnet.mtwidgets.client.ui.widget.AbstractManyToOneEntityWidget;

          import com.gwtplatform.dispatch.rest.delegates.client.ResourceDelegate;

          import jakarta.inject.Inject;

          public class CostCenterEntityWidget
              extends AbstractManyToOneEntityWidget<CostCenterEntity, CostCenterEntityWidgetSuggestion> {

            @Inject
            public CostCenterEntityWidget(final Binder uiBinder,
                final ResourceDelegate<CostCenterEntityWidgetRestService> restDelegate) {
              super(uiBinder, restDelegate, new CostCenterEntityWidgetSuggestion(null, null));
            }
          }""";

  final String costCenterEntityWidgetSuggestion =
      """
          package de.knightsoftnet.gwtp.spring.annotation.processor;

          import de.knightsoftnet.gwtp.spring.annotation.processor.CostCenterEntity;
          import de.knightsoftnet.mtwidgets.client.ui.widget.oracle.AbstractManyToOneSuggestion;

          import com.google.gwt.user.client.ui.SuggestOracle.Request;

          public class CostCenterEntityWidgetSuggestion extends AbstractManyToOneSuggestion<CostCenterEntity> {

            public CostCenterEntityWidgetSuggestion(CostCenterEntity entity, Request request) {
              super(entity, request);
            }

            @Override
            public AbstractManyToOneSuggestion<CostCenterEntity> of(CostCenterEntity entity,
                Request request) {
              return new CostCenterEntityWidgetSuggestion(entity, request);
            }

            @Override
            public String getReplacementString() {
              return entity.getNumber();
            }

            @Override
            public String getNameString() {
              return entity.getName();
            }
          }""";

  final String messages =
      """
          package de.knightsoftnet.gwtp.spring.annotation.processor;


          import com.google.gwt.i18n.client.Messages;

          public interface PersonMessages extends Messages {
            String header();
            String id();
            String salutation();
            String salutation(@Select de.knightsoftnet.gwtp.spring.annotation.processor.SalutationEnum salutation);
            String firstName();
            String lastName();
            String email();
            String birthday();
            String phoneNumber();
            String costCenter();
            String postalAddress();
            @Key("postalAddress.street")
            String postalAddressStreet();
            @Key("postalAddress.streetNumber")
            String postalAddressStreetNumber();
            @Key("postalAddress.postalCode")
            String postalAddressPostalCode();
            @Key("postalAddress.locality")
            String postalAddressLocality();
            @Key("postalAddress.countryCode")
            String postalAddressCountryCode();
            @Key("postalAddress.region")
            String postalAddressRegion();
            String emails();
            @Key("emails.email")
            String emailsEmail();
            @Key("emails.type")
            String emailsType();
            @Key("emails.type")
            String emailsType(@Select de.knightsoftnet.gwtp.spring.annotation.processor.EmailTypeEnum type);
          }""";

  final String localizationProperties = """
      header=Person
      id=id
      salutation=salutation
      salutation[MR]=MR
      salutation[MRS]=MRS
      salutation[INTER]=INTER
      firstName=firstName
      lastName=lastName
      email=email
      birthday=birthday
      phoneNumber=phoneNumber
      costCenter=costCenter
      postalAddress=postalAddress
      postalAddress.street=postalAddress.street
      postalAddress.streetNumber=postalAddress.streetNumber
      postalAddress.postalCode=postalAddress.postalCode
      postalAddress.locality=postalAddress.locality
      postalAddress.countryCode=postalAddress.countryCode
      postalAddress.region=postalAddress.region
      emails=emails
      emails.email=emails.email
      emails.type=emails.type
      emails.type[HOME]=HOME
      emails.type[WORK]=WORK
      """;

  final String presenter =
      """
          package de.knightsoftnet.gwtp.spring.annotation.processor;

          import de.knightsoftnet.gwtp.spring.annotation.processor.Person;
          import de.knightsoftnet.gwtp.spring.annotation.processor.PersonPresenter.MyProxy;
          import de.knightsoftnet.gwtp.spring.annotation.processor.PersonPresenter.MyView;
          import de.knightsoftnet.gwtp.spring.client.resources.ResourcesFile;
          import de.knightsoftnet.gwtp.spring.client.session.Session;
          import de.knightsoftnet.gwtp.spring.shared.search.SearchFieldDefinition;
          import de.knightsoftnet.gwtp.spring.shared.search.TableFieldDefinition;
          import de.knightsoftnet.mtwidgets.client.ui.page.admin.AbstractAdminPresenter;
          import de.knightsoftnet.mtwidgets.client.ui.request.DeleteRequestPresenter;
          import de.knightsoftnet.mtwidgets.client.ui.widget.helper.CountryMessages;
          import de.knightsoftnet.validators.shared.data.FieldTypeEnum;

          import com.google.web.bindery.event.shared.EventBus;
          import com.gwtplatform.dispatch.rest.delegates.client.ResourceDelegate;
          import com.gwtplatform.mvp.client.annotations.NameToken;
          import com.gwtplatform.mvp.client.annotations.ProxyCodeSplit;
          import com.gwtplatform.mvp.client.proxy.ProxyPlace;

          import org.apache.commons.lang3.StringUtils;
          import org.apache.commons.lang3.tuple.Pair;

          import java.util.List;
          import java.util.Objects;

          import jakarta.inject.Inject;

          public class PersonPresenter extends AbstractAdminPresenter<Person, MyProxy, MyView> {

            public interface MyView extends AbstractAdminPresenter.MyViewDef<Person, MyProxy, MyView> {
            }

            @ProxyCodeSplit
            @NameToken({"/person/", "/person/{id}"})
            public interface MyProxy extends ProxyPlace<PersonPresenter> {
            }

            @Inject
            public PersonPresenter(final EventBus eventBus, final MyView view, final MyProxy proxy,
                final ResourceDelegate<PersonRestService> service, final Session session,
                final DeleteRequestPresenter deleteRequestPresenter, final PersonMessages messages, final CountryMessages countryMessages,
                final ResourcesFile resources) {
              super(eventBus, view, proxy, service, session, deleteRequestPresenter,
                  List.of(
                      new SearchFieldDefinition("id", messages.id(), FieldTypeEnum.NUMERIC),
                      new SearchFieldDefinition("salutation", messages.salutation(), FieldTypeEnum.ENUM_FIXED,
                          List.of(
                              Pair.of("MR", messages.salutation(de.knightsoftnet.gwtp.spring.annotation.processor.SalutationEnum.MR)),
                              Pair.of("MRS", messages.salutation(de.knightsoftnet.gwtp.spring.annotation.processor.SalutationEnum.MRS)),
                              Pair.of("INTER", messages.salutation(de.knightsoftnet.gwtp.spring.annotation.processor.SalutationEnum.INTER))
                          )),
                      new SearchFieldDefinition("firstName", messages.firstName(), FieldTypeEnum.STRING),
                      new SearchFieldDefinition("lastName", messages.lastName(), FieldTypeEnum.STRING),
                      new SearchFieldDefinition("email", messages.email(), FieldTypeEnum.STRING),
                      new SearchFieldDefinition("birthday", messages.birthday(), FieldTypeEnum.DATE),
                      new SearchFieldDefinition("phoneNumber", messages.phoneNumber(), FieldTypeEnum.STRING),
                      new SearchFieldDefinition("costCenter", messages.costCenter(), FieldTypeEnum.MANY_TO_ONE),
                      new SearchFieldDefinition("postalAddress.street", messages.postalAddressStreet(), FieldTypeEnum.STRING),
                      new SearchFieldDefinition("postalAddress.streetNumber", messages.postalAddressStreetNumber(), FieldTypeEnum.STRING),
                      new SearchFieldDefinition("postalAddress.postalCode", messages.postalAddressPostalCode(), FieldTypeEnum.STRING),
                      new SearchFieldDefinition("postalAddress.locality", messages.postalAddressLocality(), FieldTypeEnum.STRING),
                      new SearchFieldDefinition("postalAddress.countryCode", messages.postalAddressCountryCode(), FieldTypeEnum.ENUM_FIXED,
                          List.of("""
          + "\n"
          + Stream.of(CountryEnum.values())
              .map(entry -> "                    Pair.of(\"" + entry.toString()
                  + "\", countryMessages.name(de.knightsoftnet.validators.shared.data.CountryEnum."
                  + entry.toString() + "))")
              .collect(Collectors.joining(",\n"))
          + "\n"
          + """
                              )),
                          new SearchFieldDefinition("postalAddress.region", messages.postalAddressRegion(), FieldTypeEnum.STRING),
                          new SearchFieldDefinition("emails.email", messages.emailsEmail(), FieldTypeEnum.STRING),
                          new SearchFieldDefinition("emails.type", messages.emailsType(), FieldTypeEnum.ENUM_FIXED,
                              List.of(
                                  Pair.of("HOME", messages.emailsType(de.knightsoftnet.gwtp.spring.annotation.processor.EmailTypeEnum.HOME)),
                                  Pair.of("WORK", messages.emailsType(de.knightsoftnet.gwtp.spring.annotation.processor.EmailTypeEnum.WORK))
                              ))),
                      List.of(
                          new TableFieldDefinition<Person>("id", messages.id(),
                              source -> Objects.toString(source.getId(), StringUtils.EMPTY),
                              resources.grid().col(), resources.grid().col_1(), resources.grid().col_t_2(), resources.grid().col_m_12()),
                          new TableFieldDefinition<Person>("firstName", messages.firstName(),
                              source -> source.getFirstName(),
                              resources.grid().col(), resources.grid().col_3(), resources.grid().col_t_6(), resources.grid().col_m_12()),
                          new TableFieldDefinition<Person>("lastName", messages.lastName(),
                              source -> source.getLastName(),
                              resources.grid().col(), resources.grid().col_3(), resources.grid().col_t_6(), resources.grid().col_m_12()),
                          new TableFieldDefinition<Person>("email", messages.email(),
                              source -> source.getEmail(),
                              resources.grid().col(), resources.grid().col_3(), resources.grid().col_t_6(), resources.grid().col_m_12()),
                          new TableFieldDefinition<Person>("birthday", messages.birthday(),
                              source -> source.getBirthday().format(java.time.format.DateTimeFormatter.ofLocalizedDate(java.time.format.FormatStyle.MEDIUM)),
                              resources.grid().col(), resources.grid().col_2(), resources.grid().col_t_3(), resources.grid().col_m_12()))
                      );
                }

                @Override
                protected Person createNewEntry() {
                  return new Person();
                }
              }""";

  final String view =
      """
          package de.knightsoftnet.gwtp.spring.annotation.processor;

          import de.knightsoftnet.gwtp.spring.annotation.processor.EmailData;
          import de.knightsoftnet.gwtp.spring.annotation.processor.Person;
          import de.knightsoftnet.gwtp.spring.annotation.processor.PersonMessages;
          import de.knightsoftnet.gwtp.spring.annotation.processor.PersonPresenter.MyProxy;
          import de.knightsoftnet.gwtp.spring.annotation.processor.PersonPresenter.MyView;
          import de.knightsoftnet.gwtp.spring.annotation.processor.SalutationEnum;
          import de.knightsoftnet.mtwidgets.client.ui.page.admin.AbstractAdminView;
          import de.knightsoftnet.mtwidgets.client.ui.widget.AdminNavigationWidget;
          import de.knightsoftnet.mtwidgets.client.ui.widget.DateBoxLocalDate;
          import de.knightsoftnet.mtwidgets.client.ui.widget.EmailTextBox;
          import de.knightsoftnet.mtwidgets.client.ui.widget.LongBox;
          import de.knightsoftnet.mtwidgets.client.ui.widget.PhoneNumberMsSuggestBox;
          import de.knightsoftnet.mtwidgets.client.ui.widget.SortableIdAndNameRadioButton;
          import de.knightsoftnet.mtwidgets.client.ui.widget.TextBox;
          import de.knightsoftnet.mtwidgets.client.ui.widget.helper.IdAndNameBean;
          import de.knightsoftnet.validators.client.editor.BeanValidationEditorDriver;
          import de.knightsoftnet.validators.client.editor.annotation.IsValidationDriver;

          import com.google.gwt.event.dom.client.ClickEvent;
          import com.google.gwt.uibinder.client.UiBinder;
          import com.google.gwt.uibinder.client.UiFactory;
          import com.google.gwt.uibinder.client.UiField;
          import com.google.gwt.uibinder.client.UiHandler;
          import com.google.gwt.user.client.ui.Widget;

          import java.time.LocalDate;
          import java.util.stream.Collectors;
          import java.util.stream.Stream;

          import jakarta.inject.Inject;
          import jakarta.inject.Provider;

          public class PersonView extends AbstractAdminView<Person, MyProxy, MyView>
              implements MyView {

            public static final String TOKEN = "/person/";
            public static final String TOKEN_WITH_ID = TOKEN + "{id}";

            interface Binder extends UiBinder<Widget, PersonView> {
            }

            @IsValidationDriver
            interface Driver extends BeanValidationEditorDriver<Person, PersonView> {
            }

            @UiField
            LongBox id;
            @UiField(provided = true)
            SortableIdAndNameRadioButton<SalutationEnum> salutation;
            @UiField
            TextBox firstName;
            @UiField
            TextBox lastName;
            @UiField
            EmailTextBox email;
            @UiField
            DateBoxLocalDate birthday;
            @UiField
            PhoneNumberMsSuggestBox phoneNumber;
            @UiField
            CostCenterEntityWidget costCenter;
            @UiField
            PersonPostalAddressView postalAddress;
            @UiField
            PersonEmailDataEditor emails;

            private final Provider<CostCenterEntityWidget> costCenterEntityWidgetProvider;
            private final Provider<PersonPostalAddressView> personPostalAddressViewProvider;
            private final Provider<PersonEmailDataEditor> personEmailDataEditorProvider;

            @Inject
            public PersonView(final Driver driver, final Binder uiBinder,
                final Provider<AdminNavigationWidget<Person>> adminNavigationProvider,
                final Provider<CostCenterEntityWidget> costCenterEntityWidgetProvider,
                final Provider<PersonPostalAddressView> personPostalAddressViewProvider,
                final Provider<PersonEmailDataEditor> personEmailDataEditorProvider,
                final PersonMessages messages) {
              super(driver, adminNavigationProvider);
              salutation = new SortableIdAndNameRadioButton<SalutationEnum>("salutation",
                  Stream.of(SalutationEnum.values())
                      .map(entry -> new IdAndNameBean<SalutationEnum>(entry, messages.salutation(entry)))
                      .collect(Collectors.toList()));
              this.costCenterEntityWidgetProvider = costCenterEntityWidgetProvider;
              this.personPostalAddressViewProvider = personPostalAddressViewProvider;
              this.personEmailDataEditorProvider = personEmailDataEditorProvider;
              initWidget(uiBinder.createAndBindUi(this));
              emails.setParentDriver(this.driver);
              driver.initialize(this);
              driver.setSubmitButton(adminNavigation.getSaveEntry());
              driver.addFormSubmitHandler(this);
              birthday.setMax(LocalDate.now());
              phoneNumber.setCountryCodeReference(postalAddress.countryCode);
            }

            @UiHandler("addEmails")
            public void addEmails(final ClickEvent event) {
              emails.addNewEntry();
            }

            @Ignore
            @UiFactory
            public CostCenterEntityWidget buildCostCenterEntityWidget() {
              return costCenterEntityWidgetProvider.get();
            }

            @Ignore
            @UiFactory
            public PersonPostalAddressView buildPersonPostalAddressView() {
              return personPostalAddressViewProvider.get();
            }

            @Ignore
            @UiFactory
            public PersonEmailDataEditor buildPersonEmailDataEditor() {
              return personEmailDataEditorProvider.get();
            }
          }""";

  final String viewAddress =
      """
          package de.knightsoftnet.gwtp.spring.annotation.processor;

          import de.knightsoftnet.gwtp.spring.annotation.processor.PostalAddress;
          import de.knightsoftnet.mtwidgets.client.ui.widget.CountryListBox;
          import de.knightsoftnet.mtwidgets.client.ui.widget.PostalCodeTextBox;
          import de.knightsoftnet.mtwidgets.client.ui.widget.TextBox;

          import com.google.gwt.uibinder.client.UiBinder;
          import com.google.gwt.uibinder.client.UiField;
          import com.google.gwt.user.client.ui.Composite;
          import com.google.gwt.user.client.ui.Widget;

          import org.gwtproject.editor.client.EditorDelegate;
          import org.gwtproject.editor.client.HasEditorDelegate;

          import jakarta.inject.Inject;

          public class PersonPostalAddressView extends Composite implements HasEditorDelegate<PostalAddress> {

            interface Binder extends UiBinder<Widget, PersonPostalAddressView> {
            }

            @UiField
            TextBox street;
            @UiField
            TextBox streetNumber;
            @UiField
            PostalCodeTextBox postalCode;
            @UiField
            TextBox locality;
            @UiField
            CountryListBox countryCode;
            @UiField
            TextBox region;

            @Inject
            public PersonPostalAddressView(final Binder uiBinder) {
              super();
              initWidget(uiBinder.createAndBindUi(this));
              postalCode.setCountryCodeReference(countryCode);
            }

            @Override
            public void setDelegate(final EditorDelegate<PostalAddress> delegate) {
              delegate.subscribe();
            }
          }""";

  final String viewPersonEmailDataEditorEntry = """
      package de.knightsoftnet.gwtp.spring.annotation.processor;

      import de.knightsoftnet.gwtp.spring.annotation.processor.EmailData;
      import de.knightsoftnet.gwtp.spring.annotation.processor.EmailTypeEnum;
      import de.knightsoftnet.mtwidgets.client.ui.widget.EmailTextBox;
      import de.knightsoftnet.mtwidgets.client.ui.widget.IdAndNameListBox;
      import de.knightsoftnet.mtwidgets.client.ui.widget.LongBox;
      import de.knightsoftnet.mtwidgets.client.ui.widget.helper.AbstractListItemView;
      import de.knightsoftnet.mtwidgets.client.ui.widget.helper.IdAndNameBean;

      import com.google.gwt.event.dom.client.ClickEvent;
      import com.google.gwt.uibinder.client.UiBinder;
      import com.google.gwt.uibinder.client.UiField;
      import com.google.gwt.uibinder.client.UiHandler;
      import com.google.gwt.user.client.ui.Widget;

      import java.util.stream.Collectors;
      import java.util.stream.Stream;

      import jakarta.inject.Inject;

      public class PersonEmailDataEditorEntry extends AbstractListItemView<EmailData> {

        interface Binder extends UiBinder<Widget, PersonEmailDataEditorEntry> {
        }

        @UiField
        LongBox id;
        @UiField
        EmailTextBox email;
        @UiField(provided = true)
        IdAndNameListBox<EmailTypeEnum> type;

        @Inject
        public PersonEmailDataEditorEntry(final Binder uiBinder, final PersonMessages messages) {
          super();
          type = new IdAndNameListBox<EmailTypeEnum>(Stream.of(EmailTypeEnum.values())
                  .map(entry -> new IdAndNameBean<EmailTypeEnum>(entry, messages.emailsType(entry)))
                  .collect(Collectors.toList()));
          initWidget(uiBinder.createAndBindUi(this));
        }

        @UiHandler("deleteRow")
        public void onDeleteRow(final ClickEvent event) {
          removeThisEntry();
        }
      }""";

  final String viewPersonEmailDataEditor =
      """
          package de.knightsoftnet.gwtp.spring.annotation.processor;

          import de.knightsoftnet.gwtp.spring.annotation.processor.EmailData;
          import de.knightsoftnet.mtwidgets.client.ui.widget.helper.AbstractListEditorWithProvider;
          import de.knightsoftnet.mtwidgets.client.ui.widget.helper.ItemEditorSourceWithProvider;
          import de.knightsoftnet.validators.client.editor.impl.ListValidationEditor;

          import com.google.gwt.event.dom.client.ClickEvent;
          import com.google.gwt.uibinder.client.UiHandler;

          import org.gwtproject.editor.client.SimpleBeanEditorDriver;
          import org.gwtproject.editor.client.annotation.IsDriver;

          import java.util.List;

          import jakarta.inject.Inject;
          import jakarta.inject.Provider;

          public class PersonEmailDataEditor extends AbstractListEditorWithProvider<EmailData, PersonEmailDataEditorEntry> {

            @IsDriver
            interface Driver extends SimpleBeanEditorDriver<List<EmailData>,
                ListValidationEditor<EmailData, PersonEmailDataEditorEntry>> {
            }

            private final ListValidationEditor<EmailData, PersonEmailDataEditorEntry> editor;

            @Inject
            public PersonEmailDataEditor(final Driver driver, final Provider<EmailData> dataProvider,
                final Provider<PersonEmailDataEditorEntry> viewProvider) {
              super(dataProvider);
              editor = ListValidationEditor.of(new ItemEditorSourceWithProvider<>(this, viewProvider));
              driver.initialize(editor);
            }

            @Override
            public ListValidationEditor<EmailData, PersonEmailDataEditorEntry> asEditor() {
              return editor;
            }
          }""";

  final String uiBinderXml =
      """
          <!DOCTYPE ui:UiBinder SYSTEM "http://dl.google.com/gwt/DTD/xhtml.ent">
          <ui:UiBinder xmlns:ui="urn:ui:com.google.gwt.uibinder"
            xmlns:e="urn:import:de.knightsoftnet.gwtp.spring.annotation.processor"
            xmlns:g="urn:import:com.google.gwt.user.client.ui"
            xmlns:w="urn:import:de.knightsoftnet.mtwidgets.client.ui.widget">

            <ui:with field="messages" type="de.knightsoftnet.gwtp.spring.annotation.processor.PersonMessages" />
            <ui:with field="resources" type="de.knightsoftnet.gwtp.spring.client.resources.ResourcesFile" />
            <ui:style src="../../basepage/Global.gss"/>

            <ui:import field="de.knightsoftnet.gwtp.spring.annotation.processor.PersonView.TOKEN"/>
            <ui:import field="de.knightsoftnet.gwtp.spring.annotation.processor.PersonView.TOKEN_WITH_ID"/>

            <g:HTMLPanel styleName="{style.centerPanel}">
              <div class="{resources.grid.row} {style.row}">
                <div class="{resources.grid.col} {resources.grid.col_12} {style.header}">
                  <h1>
                    <ui:text from="{messages.header}" />
                  </h1>
                </div>
              </div>
              <div class="{resources.grid.row} {style.row}">
                <w:AdminNavigationWidget ui:field="adminNavigation"
                  link="{TOKEN}"
                  linkWithParameter="{TOKEN_WITH_ID}"
                  allowNew="true" />
              </div>
              <div class="{resources.grid.row} {style.row}">
                <div class="{resources.grid.col} {resources.grid.col_2} {resources.grid.col_t_3} {resources.grid.col_m_12}">
                  <label for="id"><ui:text from="{messages.id}" /></label>
                </div>
                <div class="{resources.grid.col} {resources.grid.col_4} {resources.grid.col_t_9} {resources.grid.col_m_12}">
                  <w:LongBox debugId="id" ui:field="id" enabled="false" validationMessageElement="{idValidation}" />
                  <g:HTMLPanel tag="span" ui:field="idValidation" styleName="{style.errorPanel}" />
                </div>
                <div class="{resources.grid.col} {resources.grid.col_2} {resources.grid.col_t_3} {resources.grid.col_m_12}">
                  <label for="salutation"><ui:text from="{messages.salutation}" /></label>
                </div>
                <div class="{resources.grid.col} {resources.grid.col_4} {resources.grid.col_t_9} {resources.grid.col_m_12}">
                  <w:SortableIdAndNameRadioButton debugId="salutation" ui:field="salutation" autofocus="true" validationMessageElement="{salutationValidation}" />
                  <g:HTMLPanel tag="span" ui:field="salutationValidation" styleName="{style.errorPanel}" />
                </div>
              </div>
              <div class="{resources.grid.row} {style.row}">
                <div class="{resources.grid.col} {resources.grid.col_2} {resources.grid.col_t_3} {resources.grid.col_m_12}">
                  <label for="firstName"><ui:text from="{messages.firstName}" /></label>
                </div>
                <div class="{resources.grid.col} {resources.grid.col_4} {resources.grid.col_t_9} {resources.grid.col_m_12}">
                  <w:TextBox debugId="firstName" ui:field="firstName" required="true" maxLength="50" validationMessageElement="{firstNameValidation}" />
                  <g:HTMLPanel tag="span" ui:field="firstNameValidation" styleName="{style.errorPanel}" />
                </div>
                <div class="{resources.grid.col} {resources.grid.col_2} {resources.grid.col_t_3} {resources.grid.col_m_12}">
                  <label for="lastName"><ui:text from="{messages.lastName}" /></label>
                </div>
                <div class="{resources.grid.col} {resources.grid.col_4} {resources.grid.col_t_9} {resources.grid.col_m_12}">
                  <w:TextBox debugId="lastName" ui:field="lastName" required="true" maxLength="50" validationMessageElement="{lastNameValidation}" />
                  <g:HTMLPanel tag="span" ui:field="lastNameValidation" styleName="{style.errorPanel}" />
                </div>
              </div>
              <div class="{resources.grid.row} {style.row}">
                <div class="{resources.grid.col} {resources.grid.col_2} {resources.grid.col_t_3} {resources.grid.col_m_12}">
                  <label for="email"><ui:text from="{messages.email}" /></label>
                </div>
                <div class="{resources.grid.col} {resources.grid.col_4} {resources.grid.col_t_9} {resources.grid.col_m_12}">
                  <w:EmailTextBox debugId="email" ui:field="email" validationMessageElement="{emailValidation}" />
                  <g:HTMLPanel tag="span" ui:field="emailValidation" styleName="{style.errorPanel}" />
                </div>
                <div class="{resources.grid.col} {resources.grid.col_2} {resources.grid.col_t_3} {resources.grid.col_m_12}">
                  <label for="birthday"><ui:text from="{messages.birthday}" /></label>
                </div>
                <div class="{resources.grid.col} {resources.grid.col_4} {resources.grid.col_t_9} {resources.grid.col_m_12}">
                  <w:DateBoxLocalDate debugId="birthday" ui:field="birthday" validationMessageElement="{birthdayValidation}" />
                  <g:HTMLPanel tag="span" ui:field="birthdayValidation" styleName="{style.errorPanel}" />
                </div>
              </div>
              <div class="{resources.grid.row} {style.row}">
                <div class="{resources.grid.col} {resources.grid.col_2} {resources.grid.col_t_3} {resources.grid.col_m_12}">
                  <label for="phoneNumber"><ui:text from="{messages.phoneNumber}" /></label>
                </div>
                <div class="{resources.grid.col} {resources.grid.col_4} {resources.grid.col_t_9} {resources.grid.col_m_12}">
                  <w:PhoneNumberMsSuggestBox debugId="phoneNumber" ui:field="phoneNumber" validationMessageElement="{phoneNumberValidation}" />
                  <g:HTMLPanel tag="span" ui:field="phoneNumberValidation" styleName="{style.errorPanel}" />
                </div>
                <div class="{resources.grid.col} {resources.grid.col_2} {resources.grid.col_t_3} {resources.grid.col_m_12}">
                  <label for="costCenter"><ui:text from="{messages.costCenter}" /></label>
                </div>
                <div class="{resources.grid.col} {resources.grid.col_4} {resources.grid.col_t_9} {resources.grid.col_m_12}">
                  <e:CostCenterEntityWidget debugId="costCenter" ui:field="costCenter" validationMessageElement="{costCenterValidation}" />
                  <g:HTMLPanel tag="span" ui:field="costCenterValidation" styleName="{style.errorPanel}" />
                </div>
              </div>
              <e:PersonPostalAddressView ui:field="postalAddress" />
              <div class="{resources.grid.row} {style.row}">
                <div class="{resources.grid.col} {resources.grid.col_6} {resources.grid.col_t_12}">
                  <ui:text from="{messages.emailsEmail}" />
                </div>
                <div class="{resources.grid.col} {resources.grid.col_5} {resources.grid.col_t_12}">
                  <ui:text from="{messages.emailsType}" />
                </div>
                <div class="{resources.grid.col} {resources.grid.col_1} {resources.grid.col_t_2} {resources.grid.col_m_12}">
                  &nbsp;
                </div>
              </div>
              <e:PersonEmailDataEditor ui:field="emails" validationMessageElement="{emailsValidation}" />
              <div class="{resources.grid.row} {style.row}">
                <div class="{resources.grid.col} {resources.grid.col_12}">
                  <g:HTMLPanel tag="span" ui:field="emailsValidation" styleName="{style.errorPanel}" />
                </div>
              </div>
              <div class="{resources.grid.row} {style.row}">
                <div class="{resources.grid.col} {resources.grid.col_12}">
                  <g:Button ui:field="addEmails">
                    ➕ <ui:text from="{messages.emails}" />
                  </g:Button>
                </div>
              </div>
            </g:HTMLPanel>
          </ui:UiBinder>""";

  final String uiBinderAddressXml =
      """
          <!DOCTYPE ui:UiBinder SYSTEM "http://dl.google.com/gwt/DTD/xhtml.ent">
          <ui:UiBinder xmlns:ui="urn:ui:com.google.gwt.uibinder"
            xmlns:g="urn:import:com.google.gwt.user.client.ui"
            xmlns:w="urn:import:de.knightsoftnet.mtwidgets.client.ui.widget">

            <ui:with field="messages" type="de.knightsoftnet.gwtp.spring.annotation.processor.PersonMessages" />
            <ui:with field="resources" type="de.knightsoftnet.gwtp.spring.client.resources.ResourcesFile" />
            <ui:style src="../../basepage/Global.gss"/>

            <g:HTMLPanel>
              <div class="{resources.grid.row} {style.row}">
                <div class="{resources.grid.col} {resources.grid.col_2} {resources.grid.col_t_3} {resources.grid.col_m_12}">
                  <label for="street"><ui:text from="{messages.postalAddressStreet}" /></label>
                </div>
                <div class="{resources.grid.col} {resources.grid.col_4} {resources.grid.col_t_9} {resources.grid.col_m_12}">
                  <w:TextBox debugId="street" ui:field="street" required="true" maxLength="255" validationMessageElement="{streetValidation}" />
                  <g:HTMLPanel tag="span" ui:field="streetValidation" styleName="{style.errorPanel}" />
                </div>
                <div class="{resources.grid.col} {resources.grid.col_2} {resources.grid.col_t_3} {resources.grid.col_m_12}">
                  <label for="streetNumber"><ui:text from="{messages.postalAddressStreetNumber}" /></label>
                </div>
                <div class="{resources.grid.col} {resources.grid.col_4} {resources.grid.col_t_9} {resources.grid.col_m_12}">
                  <w:TextBox debugId="streetNumber" ui:field="streetNumber" required="true" maxLength="10" validationMessageElement="{streetNumberValidation}" />
                  <g:HTMLPanel tag="span" ui:field="streetNumberValidation" styleName="{style.errorPanel}" />
                </div>
              </div>
              <div class="{resources.grid.row} {style.row}">
                <div class="{resources.grid.col} {resources.grid.col_2} {resources.grid.col_t_3} {resources.grid.col_m_12}">
                  <label for="postalCode"><ui:text from="{messages.postalAddressPostalCode}" /></label>
                </div>
                <div class="{resources.grid.col} {resources.grid.col_4} {resources.grid.col_t_9} {resources.grid.col_m_12}">
                  <w:PostalCodeTextBox debugId="postalCode" ui:field="postalCode" required="true" validationMessageElement="{postalCodeValidation}" />
                  <g:HTMLPanel tag="span" ui:field="postalCodeValidation" styleName="{style.errorPanel}" />
                </div>
                <div class="{resources.grid.col} {resources.grid.col_2} {resources.grid.col_t_3} {resources.grid.col_m_12}">
                  <label for="locality"><ui:text from="{messages.postalAddressLocality}" /></label>
                </div>
                <div class="{resources.grid.col} {resources.grid.col_4} {resources.grid.col_t_9} {resources.grid.col_m_12}">
                  <w:TextBox debugId="locality" ui:field="locality" required="true" maxLength="255" validationMessageElement="{localityValidation}" />
                  <g:HTMLPanel tag="span" ui:field="localityValidation" styleName="{style.errorPanel}" />
                </div>
              </div>
              <div class="{resources.grid.row} {style.row}">
                <div class="{resources.grid.col} {resources.grid.col_2} {resources.grid.col_t_3} {resources.grid.col_m_12}">
                  <label for="countryCode"><ui:text from="{messages.postalAddressCountryCode}" /></label>
                </div>
                <div class="{resources.grid.col} {resources.grid.col_4} {resources.grid.col_t_9} {resources.grid.col_m_12}">
                  <w:CountryListBox debugId="countryCode" ui:field="countryCode" required="true" sort="NAME_ASC" validationMessageElement="{countryCodeValidation}" />
                  <g:HTMLPanel tag="span" ui:field="countryCodeValidation" styleName="{style.errorPanel}" />
                </div>
                <div class="{resources.grid.col} {resources.grid.col_2} {resources.grid.col_t_3} {resources.grid.col_m_12}">
                  <label for="region"><ui:text from="{messages.postalAddressRegion}" /></label>
                </div>
                <div class="{resources.grid.col} {resources.grid.col_4} {resources.grid.col_t_9} {resources.grid.col_m_12}">
                  <w:TextBox debugId="region" ui:field="region" maxLength="255" validationMessageElement="{regionValidation}" />
                  <g:HTMLPanel tag="span" ui:field="regionValidation" styleName="{style.errorPanel}" />
                </div>
              </div>
            </g:HTMLPanel>
          </ui:UiBinder>""";

  final String uiBinderEmailDataEntryXml =
      """
          <!DOCTYPE ui:UiBinder SYSTEM "http://dl.google.com/gwt/DTD/xhtml.ent">
          <ui:UiBinder xmlns:ui="urn:ui:com.google.gwt.uibinder"
            xmlns:g="urn:import:com.google.gwt.user.client.ui"
            xmlns:w="urn:import:de.knightsoftnet.mtwidgets.client.ui.widget">

            <ui:with field="messages" type="de.knightsoftnet.gwtp.spring.annotation.processor.PersonMessages" />
            <ui:with field="resources" type="de.knightsoftnet.gwtp.spring.client.resources.ResourcesFile" />
            <ui:style src="../../basepage/Global.gss"/>

            <g:HTMLPanel styleName="{resources.grid.row} {style.row}">
                <div class="{resources.grid.col} {resources.grid.col_6} {resources.grid.col_t_12}">
                  <w:EmailTextBox ui:field="email" required="true" validationMessageElement="{emailValidation}" />
                  <g:HTMLPanel tag="span" ui:field="emailValidation" styleName="{style.errorPanel}" />
                </div>
                <div class="{resources.grid.col} {resources.grid.col_5} {resources.grid.col_t_12}">
                  <w:IdAndNameListBox ui:field="type" validationMessageElement="{typeValidation}" />
                  <g:HTMLPanel tag="span" ui:field="typeValidation" styleName="{style.errorPanel}" />
                </div>
                <div class="{resources.grid.col} {resources.grid.col_1} {resources.grid.col_t_2} {resources.grid.col_m_12}">
                  <w:LongBox ui:field="id" enabled="false" visible="false"/>
                  <g:Button ui:field="deleteRow">␡</g:Button>
                </div>
            </g:HTMLPanel>
          </ui:UiBinder>""";

  final String module =
      """
          package de.knightsoftnet.gwtp.spring.annotation.processor;

          import de.knightsoftnet.gwtp.spring.annotation.processor.PersonPresenter.MyProxy;
          import de.knightsoftnet.gwtp.spring.annotation.processor.PersonPresenter.MyView;
          import de.knightsoftnet.gwtp.spring.annotation.processor.PersonView.Driver;

          import com.gwtplatform.mvp.client.gin.AbstractPresenterModule;

          import jakarta.inject.Singleton;

          public class PersonModule extends AbstractPresenterModule {

            @Override
            protected void configure() {
              bindPresenter(PersonPresenter.class, MyView.class, PersonView.class, MyProxy.class);
              bind(Driver.class).to(PersonView_Driver_Impl.class).in(Singleton.class);
              bind(PersonEmailDataEditor.Driver.class).to(PersonEmailDataEditor_Driver_Impl.class).in(Singleton.class);
            }
          }""";

  @Test
  void compileTest() {
    final Compilation compilation = javac().withProcessors(new BackofficeClientGeneratorProcessor(),
        new ValidationDriverProcessor(), new DriverProcessor()).compile(source);

    CompilationSubject.assertThat(compilation).succeeded();

    CompilationSubject.assertThat(compilation)
        .generatedSourceFile("de.knightsoftnet.gwtp.spring.annotation.processor.PersonRestService")
        .contentsAsUtf8String().contains(restService);

    CompilationSubject.assertThat(compilation)
        .generatedSourceFile(
            "de.knightsoftnet.gwtp.spring.annotation.processor.CostCenterEntityWidgetRestService")
        .contentsAsUtf8String().contains(costCenterEntityWidgetRestService);

    CompilationSubject.assertThat(compilation)
        .generatedSourceFile("de.knightsoftnet.gwtp.spring.annotation.processor.PersonMessages")
        .contentsAsUtf8String().contains(messages);

    CompilationSubject.assertThat(compilation)
        .generatedFile(StandardLocation.SOURCE_OUTPUT,
            "de.knightsoftnet.gwtp.spring.annotation.processor", "PersonMessages_de.properties")
        .contentsAsUtf8String().contains(localizationProperties);

    CompilationSubject.assertThat(compilation)
        .generatedFile(StandardLocation.SOURCE_OUTPUT,
            "de.knightsoftnet.gwtp.spring.annotation.processor", "PersonMessages_en.properties")
        .contentsAsUtf8String().contains(localizationProperties);

    CompilationSubject.assertThat(compilation)
        .generatedSourceFile("de.knightsoftnet.gwtp.spring.annotation.processor.PersonPresenter")
        .contentsAsUtf8String().contains(presenter);

    CompilationSubject.assertThat(compilation)
        .generatedSourceFile("de.knightsoftnet.gwtp.spring.annotation.processor.PersonView")
        .contentsAsUtf8String().contains(view);

    CompilationSubject.assertThat(compilation)
        .generatedSourceFile(
            "de.knightsoftnet.gwtp.spring.annotation.processor.PersonPostalAddressView")
        .contentsAsUtf8String().contains(viewAddress);

    CompilationSubject.assertThat(compilation)
        .generatedSourceFile(
            "de.knightsoftnet.gwtp.spring.annotation.processor.PersonEmailDataEditorEntry")
        .contentsAsUtf8String().contains(viewPersonEmailDataEditorEntry);

    CompilationSubject.assertThat(compilation)
        .generatedSourceFile(
            "de.knightsoftnet.gwtp.spring.annotation.processor.PersonEmailDataEditor")
        .contentsAsUtf8String().contains(viewPersonEmailDataEditor);

    CompilationSubject.assertThat(compilation)
        .generatedSourceFile(
            "de.knightsoftnet.gwtp.spring.annotation.processor.CostCenterEntityWidgetSuggestion")
        .contentsAsUtf8String().contains(costCenterEntityWidgetSuggestion);

    CompilationSubject.assertThat(compilation)
        .generatedSourceFile(
            "de.knightsoftnet.gwtp.spring.annotation.processor.CostCenterEntityWidget")
        .contentsAsUtf8String().contains(costCenterEntityWidget);

    CompilationSubject.assertThat(compilation)
        .generatedFile(StandardLocation.SOURCE_OUTPUT,
            "de.knightsoftnet.gwtp.spring.annotation.processor", "PersonView.ui.xml")
        .contentsAsUtf8String().contains(uiBinderXml);

    CompilationSubject.assertThat(compilation)
        .generatedFile(StandardLocation.SOURCE_OUTPUT,
            "de.knightsoftnet.gwtp.spring.annotation.processor", "PersonPostalAddressView.ui.xml")
        .contentsAsUtf8String().contains(uiBinderAddressXml);

    CompilationSubject.assertThat(compilation).generatedFile(StandardLocation.SOURCE_OUTPUT,
        "de.knightsoftnet.gwtp.spring.annotation.processor", "PersonEmailDataEditorEntry.ui.xml")
        .contentsAsUtf8String().contains(uiBinderEmailDataEntryXml);

    CompilationSubject.assertThat(compilation)
        .generatedSourceFile("de.knightsoftnet.gwtp.spring.annotation.processor.PersonModule")
        .contentsAsUtf8String().contains(module);
  }
}

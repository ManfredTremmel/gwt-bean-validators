package de.knightsoftnet.gwtp.spring.annotation.processor;

import de.knightsoftnet.validators.shared.data.FieldTypeEnum;

import org.hamcrest.MatcherAssert;
import org.hamcrest.collection.IsIterableContainingInOrder;
import org.junit.Test;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class FlatteningTest {

  @Test
  public void testFlatteningWidgets() {
    // Given
    final List<BackofficeWidget> source = List.of(

        BackofficeWidget.of(null, null, "1", null, null, null, false, null, FieldTypeEnum.NUMERIC,
            Collections.emptyList(), null, false, true, false, null),

        BackofficeWidget.of(null, null, "2", null, null, null, false, null, FieldTypeEnum.EMBEDDED,
            List.of(

                BackofficeWidget.of(null, null, "1", null, null, null, false, null,
                    FieldTypeEnum.STRING, Collections.emptyList(), null, false, true, false, null),
                BackofficeWidget.of(null, null, "2", null, null, null, false, null,
                    FieldTypeEnum.STRING, Collections.emptyList(), null, false, true, false, null)

            ), null, false, true, false, null),

        BackofficeWidget.of(null, null, "3", null, null, null, false, null, FieldTypeEnum.NUMERIC,
            Collections.emptyList(), null, false, true, false, null),

        BackofficeWidget.of(null, null, "4", null, null, null, false, null, FieldTypeEnum.EMBEDDED,
            List.of(

                BackofficeWidget.of(null, null, "1", null, null, null, false, null,
                    FieldTypeEnum.STRING,
                    List.of(
                        BackofficeWidget.of(null, null, "1", null, null, null, false, null,
                            FieldTypeEnum.STRING, Collections.emptyList(), null, false, true, false,
                            null),
                        BackofficeWidget.of(null, null, "2", null, null, null, false, null,
                            FieldTypeEnum.STRING, Collections.emptyList(), null, false, true, false,
                            null)),
                    null, false, true, false, null),
                BackofficeWidget.of(null, null, "2", null, null, null, false, null,
                    FieldTypeEnum.STRING, Collections.emptyList(), null, false, true, false, null)

            ), null, false, true, false, null)

    );

    // When
    final List<String> result =
        source.stream().flatMap(BackofficeWidget::streamFlatBackofficeWidget)
            .map(BackofficeWidget::getName).collect(Collectors.toList());

    // Then
    MatcherAssert.assertThat(result, IsIterableContainingInOrder.contains(//
        "1", //
        "2", "2.1", "2.2", //
        "3", //
        "4", "4.1", "4.1.1", "4.1.2", "4.2" //
    ));
  }

}

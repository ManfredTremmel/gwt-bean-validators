package de.knightsoftnet.gwtp.spring.annotation.processor;

import static com.google.testing.compile.Compiler.javac;

import de.knightsoftnet.gwtp.spring.server.annotation.processor.BackofficeGeneratorProcessor;

import com.google.testing.compile.Compilation;
import com.google.testing.compile.CompilationSubject;
import com.google.testing.compile.JavaFileObjects;

import org.junit.jupiter.api.Test;

import javax.tools.JavaFileObject;

/**
 * compile test with code generation.
 */
class BackofficeServerGeneratorTest {
  final JavaFileObject source = JavaFileObjects
      .forSourceString("de.knightsoftnet.gwtp.spring.annotation.processor.GeneratingBackoffice", """
          package de.knightsoftnet.gwtp.spring.annotation.processor;

          import de.knightsoftnet.gwtp.spring.annotation.processor.Person;
          import de.knightsoftnet.gwtp.spring.server.annotation.BackofficeGenerator;

          public class GeneratingBackoffice {
            @BackofficeGenerator("/api/rest/person")
            private Person person;
          }""");

  final String repository = """
      package de.knightsoftnet.gwtp.spring.annotation.processor;

      import de.knightsoftnet.gwtp.spring.annotation.processor.Person;
      import de.knightsoftnet.gwtp.spring.annotation.processor.QPerson;
      import de.knightsoftnet.gwtp.spring.server.jpa.AdminJpaRepository;

      public interface PersonRepository extends AdminJpaRepository<Person, Long, QPerson> {
      }""";

  final String controller = """
      package de.knightsoftnet.gwtp.spring.annotation.processor;

      import de.knightsoftnet.gwtp.spring.annotation.processor.Person;
      import de.knightsoftnet.gwtp.spring.server.controller.AbstractAdminController;

      import org.springframework.http.MediaType;
      import org.springframework.web.bind.annotation.RequestMapping;
      import org.springframework.web.bind.annotation.RestController;

      import jakarta.inject.Inject;

      @RestController
      @RequestMapping(value = "/api/rest/person", produces = MediaType.APPLICATION_JSON_VALUE)
      public class PersonController
          extends AbstractAdminController<Person, PersonRepository, PersonPredicatesBuilder> {

        @Inject
        public PersonController(final PersonRepository repository) {
          super(repository, new PersonPredicatesBuilder());
        }
      }""";

  final String predicate =
      """
          package de.knightsoftnet.gwtp.spring.annotation.processor;

          import de.knightsoftnet.gwtp.spring.annotation.processor.Person;
          import de.knightsoftnet.gwtp.spring.annotation.processor.QEmailData;
          import de.knightsoftnet.gwtp.spring.annotation.processor.QPerson;
          import de.knightsoftnet.gwtp.spring.server.querydsl.AbstractPredicate;
          import de.knightsoftnet.gwtp.spring.shared.search.SearchCriteriaServer;

          import com.querydsl.core.types.dsl.BooleanExpression;
          import com.querydsl.jpa.JPAExpressions;

          public class PersonPredicate extends AbstractPredicate<Person> {

            public PersonPredicate(final SearchCriteriaServer criteria) {
              super(criteria, "person", Person.class);
            }

            @Override
            public BooleanExpression getPredicate() {

              switch (criteria.getKey()) {
                case "salutation":
                  return enumCompare(QPerson.person.salutation,
                      (de.knightsoftnet.gwtp.spring.annotation.processor.SalutationEnum) criteria.getValue());
                case "postalAddress.countryCode":
                  return enumCompare(QPerson.person.postalAddress.countryCode,
                      (de.knightsoftnet.validators.shared.data.CountryEnum) criteria.getValue());
                case "emails.email":
                  return QPerson.person.emails.contains(
                      JPAExpressions.selectFrom(QEmailData.emailData)
                          .where(QEmailData.emailData.person.eq(QPerson.person)
                              .and(stringCompare(QEmailData.emailData.email,
                                  (java.lang.String) criteria.getValue()))));
                case "emails.type":
                  return QPerson.person.emails.contains(
                      JPAExpressions.selectFrom(QEmailData.emailData)
                          .where(QEmailData.emailData.person.eq(QPerson.person)
                              .and(enumCompare(QEmailData.emailData.type,
                                  (de.knightsoftnet.gwtp.spring.annotation.processor.EmailTypeEnum) criteria.getValue()))));
                case "costCenter":
                  return stringCompare(QPerson.person.costCenter.number, criteria.getValue());
                default:
                  return super.getPredicate();
              }
            }
          }""";

  final String predicateBuilder = """
      package de.knightsoftnet.gwtp.spring.annotation.processor;

      import de.knightsoftnet.gwtp.spring.server.querydsl.AbstractPredictatesBuilder;
      import de.knightsoftnet.gwtp.spring.shared.search.SearchCriteriaServer;
      import de.knightsoftnet.gwtp.spring.shared.search.SearchOperation;

      import org.apache.commons.lang3.StringUtils;
      import org.apache.commons.lang3.tuple.Triple;

      public class PersonPredicatesBuilder extends AbstractPredictatesBuilder<PersonPredicate> {

        @Override
        protected PersonPredicate createPredicate(final SearchCriteriaServer criteria) {
          return new PersonPredicate(criteria);
        }

        @Override
        protected SearchCriteriaServer mapTripleToSearchCriteria(
            final Triple<String, String, String> triple) {
          switch (triple.getLeft()) {
            case "id":
              return new SearchCriteriaServer(triple.getLeft(),
                  SearchOperation.getSimpleOperation(triple.getMiddle().charAt(0)),
                  java.lang.Long.valueOf(triple.getRight()));
            case "salutation":
              return new SearchCriteriaServer(triple.getLeft(),
                  SearchOperation.getSimpleOperation(triple.getMiddle().charAt(0)),
                  de.knightsoftnet.gwtp.spring.annotation.processor.SalutationEnum.valueOf(
                      StringUtils.upperCase(triple.getRight())));
            case "birthday":
              return new SearchCriteriaServer(triple.getLeft(),
                  SearchOperation.getSimpleOperation(triple.getMiddle().charAt(0)),
                  java.time.LocalDate.parse(triple.getRight()));
            case "postalAddress.countryCode":
              return new SearchCriteriaServer(triple.getLeft(),
                  SearchOperation.getSimpleOperation(triple.getMiddle().charAt(0)),
                  de.knightsoftnet.validators.shared.data.CountryEnum.valueOf(
                      StringUtils.upperCase(triple.getRight())));
            case "emails.type":
              return new SearchCriteriaServer(triple.getLeft(),
                  SearchOperation.getSimpleOperation(triple.getMiddle().charAt(0)),
                  de.knightsoftnet.gwtp.spring.annotation.processor.EmailTypeEnum.valueOf(
                      StringUtils.upperCase(triple.getRight())));
            default:
              return super.mapTripleToSearchCriteria(triple);
          }
        }
      }""";

  final String costCenterWidgetRepository =
      """
          package de.knightsoftnet.gwtp.spring.annotation.processor;

          import de.knightsoftnet.gwtp.spring.annotation.processor.CostCenterEntity;

          import org.springframework.data.jpa.repository.JpaRepository;

          import java.util.List;

          public interface CostCenterEntityWidgetRepository extends JpaRepository<CostCenterEntity, Long> {
            CostCenterEntity getReferenceByNumber(String number);
            List<CostCenterEntity> findByNumberContaining(String number);
          }""";

  final String costCenterWidgetController =
      """
          package de.knightsoftnet.gwtp.spring.annotation.processor;

          import de.knightsoftnet.gwtp.spring.annotation.processor.CostCenterEntity;

          import org.springframework.http.MediaType;
          import org.springframework.web.bind.annotation.GetMapping;
          import org.springframework.web.bind.annotation.PathVariable;
          import org.springframework.web.bind.annotation.RequestMapping;
          import org.springframework.web.bind.annotation.RestController;

          import java.util.List;

          import jakarta.inject.Inject;
          import jakarta.validation.constraints.NotEmpty;

          @RestController
          @RequestMapping(value = "/api/rest/costcenterentitywidget", produces = MediaType.APPLICATION_JSON_VALUE)
          public class CostCenterEntityWidgetController {

            private final CostCenterEntityWidgetRepository repository;

            @Inject
            public CostCenterEntityWidgetController(final CostCenterEntityWidgetRepository repository) {
              super();
              this.repository = repository;
            }

            @GetMapping(value = "/{number}")
            public CostCenterEntity getReferenceByNumber(
                @PathVariable(value = "number") @NotEmpty final String number) {
              return repository.getReferenceByNumber(number);
            }

            @GetMapping(value = "/suggestions/{number}")
            public List<CostCenterEntity> findByNumberContaining(
                @PathVariable(value = "number") @NotEmpty final String number) {
              return repository.findByNumberContaining(number);
            }
          }""";

  @Test
  void compileTest() {
    final Compilation compilation =
        javac().withProcessors(new BackofficeGeneratorProcessor()).compile(source);

    CompilationSubject.assertThat(compilation).succeeded();

    CompilationSubject.assertThat(compilation)
        .generatedSourceFile("de.knightsoftnet.gwtp.spring.annotation.processor.PersonRepository")
        .contentsAsUtf8String().contains(repository);

    CompilationSubject.assertThat(compilation)
        .generatedSourceFile("de.knightsoftnet.gwtp.spring.annotation.processor.PersonController")
        .contentsAsUtf8String().contains(controller);

    CompilationSubject.assertThat(compilation)
        .generatedSourceFile("de.knightsoftnet.gwtp.spring.annotation.processor.PersonPredicate")
        .contentsAsUtf8String().contains(predicate);

    CompilationSubject.assertThat(compilation)
        .generatedSourceFile(
            "de.knightsoftnet.gwtp.spring.annotation.processor.PersonPredicatesBuilder")
        .contentsAsUtf8String().contains(predicateBuilder);

    CompilationSubject.assertThat(compilation)
        .generatedSourceFile(
            "de.knightsoftnet.gwtp.spring.annotation.processor.CostCenterEntityWidgetRepository")
        .contentsAsUtf8String().contains(costCenterWidgetRepository);

    CompilationSubject.assertThat(compilation)
        .generatedSourceFile(
            "de.knightsoftnet.gwtp.spring.annotation.processor.CostCenterEntityWidgetController")
        .contentsAsUtf8String().contains(costCenterWidgetController);
  }
}

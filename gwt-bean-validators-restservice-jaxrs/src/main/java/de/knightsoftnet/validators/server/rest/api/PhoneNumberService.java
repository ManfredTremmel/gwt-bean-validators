/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.server.rest.api;

import de.knightsoftnet.validators.shared.Parameters;
import de.knightsoftnet.validators.shared.ResourcePaths.PhoneNumber;
import de.knightsoftnet.validators.shared.data.CreatePhoneCountryConstantsClass;
import de.knightsoftnet.validators.shared.data.PhoneCountryData;
import de.knightsoftnet.validators.shared.util.LocaleUtil;

import org.apache.commons.lang3.StringUtils;

import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * phone number web service brings phone number util functions to client.
 *
 * @author Manfred Tremmel
 */
@Path(PhoneNumber.ROOT_WITHOUT_BASE)
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class PhoneNumberService {

  /**
   * return phone country constants.
   *
   * @param language language for which should language specific values be requested
   * @return PhoneCountrySharedConstants
   */
  @GET
  @Path(PhoneNumber.GET_PHONE_COUNTRY_CONSTANTS + "/{" + Parameters.LANGUAGE + "}")
  public Map<String, PhoneCountryData> getPhoneCountryConstants(
      @PathParam(Parameters.LANGUAGE) final String language) {
    if (StringUtils.isEmpty(language)) {
      return CreatePhoneCountryConstantsClass.create().getCountriesMap();
    }
    return CreatePhoneCountryConstantsClass.create(LocaleUtil.convertLanguageToLocale(language))
        .getCountriesMap();
  }
}

gwt-bean-validators-restservice-jaxrs
=====================================

This package contains the server side rest services based on jaxrs for [gwt-bean-validators-rest-resolver](../gwt-bean-validators-rest-resolver).


Maven integraten
----------------

```xml
    <dependency>
      <groupId>de.knightsoft-net</groupId>
      <artifactId>gwt-bean-validators-restservice-jaxrs</artifactId>
      <version>2.4.1</version>
    </dependency>
```

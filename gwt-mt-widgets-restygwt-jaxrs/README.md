# gwt-mt-widgets-restygwt-jaxrs
A set of widgets and handlers for gwt applications using gwt-bean-validators - REST calls based on restygwt
Using this inherits instead of [gwt-mt-widgets](https://gitlab.com/ManfredTremmel/gwt-bean-validators/tree/master/gwt-mt-widgets) itself, means the needed data for PhoneNumber*SuggestBox, IbanTextBox and BicSuggestBox is not compiled into client side Java-Script code, it's read by rest call from the server.

Maven integraten
----------------

The dependency itself for GWT-Projects:

```xml
    <dependency>
      <groupId>de.knightsoft-net</groupId>
      <artifactId>gwt-mt-widgets-restygwt-jaxrs</artifactId>
      <version>2.4.1</version>
    </dependency>
```

GWT Integration
---------------

What you still have to do, inherit GwtMtWidgets into your project .gwt.xml file:

```xml
<inherits name="de.knightsoftnet.mtwidgets.GwtMtWidgetsResty" />
```

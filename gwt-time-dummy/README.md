gwt-time-dummy
==============

Dummy GWT emulation classes for `java.time.Clock` and `java.time.Duration` without functionality!
It's only used to enable build and use of validation engine, no need to use this in any other project.

Maven integraten
----------------

The dependency itself for GWT-Projects:

```xml
    <dependency>
      <groupId>de.knightsoft-net</groupId>
      <artifactId>gwt-time-dummy</artifactId>
      <version>2.4.1</version>
    </dependency>
```

GWT Integration
---------------

```xml
<inherits name="de.knightsoftnet.time.GwtTimeDummy" />
```

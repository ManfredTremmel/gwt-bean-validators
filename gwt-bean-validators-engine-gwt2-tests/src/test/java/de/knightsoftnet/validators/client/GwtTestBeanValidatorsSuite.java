/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.client;

import com.google.gwt.junit.tools.GWTTestSuite;

import junit.framework.Test;

/**
 * combine the single tests to speed up run.
 *
 * @author Manfred Tremmel
 *
 */
public class GwtTestBeanValidatorsSuite extends GWTTestSuite {
  /**
   * build a suite of all gwt unit tests.
   *
   * @return the test suite
   */
  public static Test suite() { // NOPMD
    final GWTTestSuite suite = new GWTTestSuite("validation engine client side integration tests");

    GwtTestBeanValidatorsSuite.appendHibernateBeanValidatorTests(suite);
    GwtTestBeanValidatorsSuite.appendOtherBeanValidatorTests(suite);
    return suite;
  }

  private static void appendHibernateBeanValidatorTests(final GWTTestSuite suite) {
    suite.addTestSuite(GwtTstHibernateAssertFalse.class);
    suite.addTestSuite(GwtTstHibernateAssertTrue.class);
    suite.addTestSuite(GwtTstHibernateCodePointLength.class);
    suite.addTestSuite(GwtTstHibernateDecimalMinMax.class);
    suite.addTestSuite(GwtTstHibernateDigits.class);
    suite.addTestSuite(GwtTstHibernateEmail.class);
    suite.addTestSuite(GwtTstHibernateLuhnCheck.class);
    suite.addTestSuite(GwtTstHibernateMinMax.class);
    suite.addTestSuite(GwtTstHibernateMinMaxPrimitive.class);
    suite.addTestSuite(GwtTstHibernateMod10Check.class);
    suite.addTestSuite(GwtTstHibernateMod11Check.class);
    suite.addTestSuite(GwtTstHibernateNegative.class);
    suite.addTestSuite(GwtTstHibernateNegativeOrZero.class);
    suite.addTestSuite(GwtTstHibernateNotNull.class);
    suite.addTestSuite(GwtTstHibernateNotBlank.class);
    suite.addTestSuite(GwtTstHibernateNotEmpty.class);
    suite.addTestSuite(GwtTstHibernateNotEmptyExtended.class);
    suite.addTestSuite(GwtTstHibernateNotEmptyBug11.class);
    suite.addTestSuite(GwtTstHibernateNull.class);
    suite.addTestSuite(GwtTstHibernatePattern.class);
    suite.addTestSuite(GwtTstHibernatePatternBug10.class);
    suite.addTestSuite(GwtTstHibernatePositive.class);
    suite.addTestSuite(GwtTstHibernatePositiveOrZero.class);
    suite.addTestSuite(GwtTstHibernateSize.class);
    suite.addTestSuite(GwtTstHibernateSizeArray.class);
    suite.addTestSuite(GwtTstHibernateSizeCollection.class);
    suite.addTestSuite(GwtTstHibernateFuture.class);
    suite.addTestSuite(GwtTstHibernateFutureOrPresent.class);
    suite.addTestSuite(GwtTstHibernatePast.class);
    suite.addTestSuite(GwtTstHibernatePastOrPresent.class);
    suite.addTestSuite(GwtTstHibernateEan8.class);
    suite.addTestSuite(GwtTstHibernateEan13.class);
    suite.addTestSuite(GwtTstHibernateIsbn10.class);
    suite.addTestSuite(GwtTstHibernateIsbn13.class);
    suite.addTestSuite(GwtTstHibernateLength.class);
    suite.addTestSuite(GwtTstHibernateCreditCardNumber.class);
    suite.addTestSuite(GwtTstHibernateUrl.class);
    suite.addTestSuite(GwtTstHibernateCnpj.class);
    suite.addTestSuite(GwtTstHibernateCpf.class);
    suite.addTestSuite(GwtTstHibernateNip.class);
    suite.addTestSuite(GwtTstHibernatePesel.class);
    suite.addTestSuite(GwtTstHibernateRange.class);
    suite.addTestSuite(GwtTstHibernateRegon.class);
    suite.addTestSuite(GwtTstHibernateUniqueElements.class);
    suite.addTestSuite(GwtTstHibernateTituloEleitoral.class);
    suite.addTestSuite(GwtTstHibernateInn.class);
    suite.addTestSuite(GwtTstHibernateUuid.class);
  }

  private static void appendOtherBeanValidatorTests(final GWTTestSuite suite) {
    suite.addTestSuite(GwtTstFoo.class);
    suite.addTestSuite(GwtTstFooFoo.class);
    suite.addTestSuite(GwtTstNestedClass.class);
    suite.addTestSuite(GwtTstNestedClass2.class);
    suite.addTestSuite(GwtTstNestedClass3.class);
    suite.addTestSuite(GwtTstBug17.class);
    suite.addTestSuite(GwtTstBug20.class);
    suite.addTestSuite(GwtTstMyModel.class);
    suite.addTestSuite(GwtTstBug25.class);
    suite.addTestSuite(GwtTstHibernateNotEmptyBug25.class);
    suite.addTestSuite(GwtTstBug27.class);
    suite.addTestSuite(GwtTstBug27B.class);
    suite.addTestSuite(GwtTstBug28.class);
  }
}

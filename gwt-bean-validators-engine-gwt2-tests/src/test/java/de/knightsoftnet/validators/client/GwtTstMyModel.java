/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.client;

import de.knightsoftnet.validators.shared.beans.MyModel;

/**
 * test for hibernate not null validator with reported class.
 *
 * @author Manfred Tremmel
 *
 */
public class GwtTstMyModel extends AbstractValidationTst<MyModel> {

  public final void testWithEntryShouldBeOk() {
    final MyModel testBean = new MyModel();
    testBean.setImageBytes("test".getBytes());
    validationTest(testBean, true, null);
  }

  public final void testWithNullEntryShouldReturnError() {
    final MyModel testBean = new MyModel();
    testBean.setImageBytes(null);
    validationTest(testBean, false,
        "org.hibernate.validator.internal.constraintvalidators.bv.NotNullValidator");
  }
}

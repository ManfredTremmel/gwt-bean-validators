/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.client;

import de.knightsoftnet.validators.shared.beans.Bug25TestBean;
import de.knightsoftnet.validators.shared.testcases.Bug25TestCases;

/**
 * test for bug 25.
 *
 * @author Manfred Tremmel
 *
 */
public class GwtTstBug25 extends AbstractValidationTst<Bug25TestBean> {

  private static final String VALIDATION_CLASS =
      "org.hibernate.validator.internal.constraintvalidators.bv.notempty."
          + "NotEmptyValidatorForCharSequence";

  private static final String VALIDATION_SIZE_CLASS =
      "org.hibernate.validator.internal.constraintvalidators.bv.size."
          + "SizeValidatorForCharSequence";

  /**
   * empty not empty is not allowed.
   */
  public final void testEmptyNotEmptyIsWrong() {
    validationTest(Bug25TestCases.getEmptyTestBean(), false, VALIDATION_CLASS);
  }

  /**
   * correct not empty is allowed.
   */
  public final void testCorrectNotEmptyAreAllowed() {
    for (final Bug25TestBean testBean : Bug25TestCases.getCorrectTestBeans()) {
      validationTest(testBean, true, null);
    }
  }

  /**
   * wrong not empty is not allowed.
   */
  public final void testWrongNotEmptyAreWrong() {
    for (final Bug25TestBean testBean : Bug25TestCases.getWrongEmptyBeans()) {
      validationTest(testBean, false, VALIDATION_CLASS);
    }
  }

  /**
   * wrong size is not allowed.
   */
  public final void testWrongSizeAreWrong() {
    for (final Bug25TestBean testBean : Bug25TestCases.getWrongSizeTestBeans()) {
      validationTest(testBean, false, VALIDATION_SIZE_CLASS);
    }
  }
}

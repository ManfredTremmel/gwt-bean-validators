/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.client;

import de.knightsoftnet.validators.shared.beans.NestedClass2TestBean;
import de.knightsoftnet.validators.shared.testcases.NestedClass2TestCases;

/**
 * test for hibernate not empty validator.
 *
 * @author Manfred Tremmel
 *
 */
public class GwtTstNestedClass2 extends AbstractValidationTst<NestedClass2TestBean> {

  private static final String VALIDATION_CLASS =
      "org.hibernate.validator.internal.constraintvalidators.bv.notempty."
          + "NotEmptyValidatorForCharSequence";

  /**
   * empty is allowed.
   */
  public final void testEmptyIsAllowed() {
    validationTest(NestedClass2TestCases.getEmptyTestBean(), true, null);
  }

  /**
   * correct not empty is allowed.
   */
  public final void testCorrectNotEmptyAreAllowed() {
    for (final NestedClass2TestBean testBean : NestedClass2TestCases.getCorrectTestBeans()) {
      validationTest(testBean, true, null);
    }
  }

  /**
   * wrong not empty is not allowed.
   */
  public final void testWrongNotEmptyAreWrong() {
    for (final NestedClass2TestBean testBean : NestedClass2TestCases.getWrongTestBeans()) {
      validationTest(testBean, false, VALIDATION_CLASS);
    }
  }
}

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.client;

import de.knightsoftnet.validators.shared.beans.Bug28TestBean;
import de.knightsoftnet.validators.shared.testcases.Bug28TestCases;

/**
 * test with getter that doesn't match the naming conventions.
 *
 * @author Manfred Tremmel
 *
 */
public class GwtTstBug28 extends AbstractValidationTst<Bug28TestBean> {

  /**
   * correct min max are allowed.
   */
  public final void testCorrectMinMaxsAreAllowed() {
    for (final Bug28TestBean testBean : Bug28TestCases.getCorrectTestBeans()) {
      validationTest(testBean, true, null);
    }
  }

  /**
   * to small min max are not allowed.
   */
  public final void testToSmallMinMaxsAreWrong() {
    for (final Bug28TestBean testBean : Bug28TestCases.getWrongtoSmallTestBeans()) {
      validationTest(testBean, false, null);
    }
  }

  /**
   * to big min max are not allowed.
   */
  public final void testToBigMinMaxsAreWrong() {
    for (final Bug28TestBean testBean : Bug28TestCases.getWrongtoBigTestBeans()) {
      validationTest(testBean, false, null);
    }
  }
}

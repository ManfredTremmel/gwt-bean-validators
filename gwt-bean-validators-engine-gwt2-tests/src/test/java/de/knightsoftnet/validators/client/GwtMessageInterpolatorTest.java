package de.knightsoftnet.validators.client;

import de.knightsoftnet.validators.client.impl.ConstraintDescriptorImpl;
import de.knightsoftnet.validators.client.impl.MessageInterpolatorContextImpl;

import org.junit.Assert;
import org.junit.Test;

import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import jakarta.validation.MessageInterpolator.Context;

public class GwtMessageInterpolatorTest {

  final GwtMessageInterpolator messageInterpolator = new GwtMessageInterpolator();

  @Test
  public void testMaxMessage() {
    // Given
    final Context context = new MessageInterpolatorContextImpl(
        ConstraintDescriptorImpl.builder().setAttributes(Stream.of(new Object[][] {

            {"value", "5"},

        }).collect(Collectors.toMap(data -> Objects.toString(data[0]), data -> data[1]))).build(),
        null);

    // When
    final String result =
        messageInterpolator.gwtInterpolate("must be less than or equal to {value}", context);

    // Then
    Assert.assertEquals("must be less than or equal to 5", result);
  }

  @Test
  public void testDigitsMessage() {
    // Given
    final Context context = new MessageInterpolatorContextImpl(ConstraintDescriptorImpl.builder()
        .setAttributes(Stream.of(new Object[][] {

            {"integer", 5}, //
            {"fraction", 2},

        }).collect(Collectors.toMap(data -> Objects.toString(data[0]), data -> data[1]))).build(),
        null);

    // When
    final String result = messageInterpolator.gwtInterpolate(
        "numeric value out of bounds (<{integer} digits>.<{fraction} digits> expected)", context);

    // Then
    Assert.assertEquals("numeric value out of bounds (5.2 expected)", result);
  }

  @Test
  public void testDecimalMinInclusiveMessage() {
    // Given
    final Context context = new MessageInterpolatorContextImpl(ConstraintDescriptorImpl.builder()
        .setAttributes(Stream.of(new Object[][] {

            {"value", 5}, //
            {"inclusive", true},

        }).collect(Collectors.toMap(data -> Objects.toString(data[0]), data -> data[1]))).build(),
        5);

    // When
    final String result = messageInterpolator.gwtInterpolate(
        "must be greater than ${inclusive == true ? 'or equal to ' : ''}{value}", context);

    // Then
    Assert.assertEquals("must be greater than or equal to 5", result);
  }

  @Test
  public void testDecimalMinExclusiveMessage() {
    // Given
    final Context context = new MessageInterpolatorContextImpl(ConstraintDescriptorImpl.builder()
        .setAttributes(Stream.of(new Object[][] {

            {"value", 5}, //
            {"inclusive", false},

        }).collect(Collectors.toMap(data -> Objects.toString(data[0]), data -> data[1]))).build(),
        5);

    // When
    final String result = messageInterpolator.gwtInterpolate(
        "must be greater than ${inclusive == true ? 'or equal to ' : ''}{value}", context);

    // Then
    Assert.assertEquals("must be greater than 5", result);
  }

  @Test
  public void testModCheckMessage() {
    // Given
    final Context context = new MessageInterpolatorContextImpl(
        ConstraintDescriptorImpl.builder().setAttributes(Stream.of(new Object[][] {

            {"modType", "modx"},

        }).collect(Collectors.toMap(data -> Objects.toString(data[0]), data -> data[1]))).build(),
        5);

    // When
    final String result = messageInterpolator.gwtInterpolate(
        "the check digit for ${validatedValue} is invalid, {modType} checksum failed", context);

    // Then
    Assert.assertEquals("the check digit for 5 is invalid, modx checksum failed", result);
  }

  @Test
  public void testDurationInclusiveMaxMessage() {
    // Given
    final Context context = new MessageInterpolatorContextImpl(ConstraintDescriptorImpl.builder()
        .setAttributes(Stream.of(new Object[][] {

            {"inclusive", true}, //
            {"days", 0}, //
            {"hours", 1}, //
            {"minutes", 25}, //
            {"seconds", 10}, //
            {"millis", 0}, //
            {"nanos", 0}, //

        }).collect(Collectors.toMap(data -> Objects.toString(data[0]), data -> data[1]))).build(),
        5);

    // When
    final String result =
        messageInterpolator.gwtInterpolate(
            "must be shorter than${inclusive == true ? ' or equal to' : ''}"
                + "${days == 0 ? '' : days == 1 ? ' 1 day' : ' ' += days += ' days'}"
                + "${hours == 0 ? '' : hours == 1 ? ' 1 hour' : ' ' += hours += ' hours'}"
                + "${minutes == 0 ? '' : minutes == 1 ? ' 1 minute' : ' ' += minutes += ' minutes'}"
                + "${seconds == 0 ? '' : seconds == 1 ? ' 1 second' : ' ' += seconds += ' seconds'}"
                + "${millis == 0 ? '' : millis == 1 ? ' 1 milli' : ' ' += millis += ' millis'}"
                + "${nanos == 0 ? '' : nanos == 1 ? ' 1 nano' : ' ' += nanos += ' nanos'}",
            context);

    // Then
    Assert.assertEquals("must be shorter than or equal to 1 hour 25 minutes 10 seconds", result);
  }

  @Test
  public void testDurationMaxExclusiveMessage() {
    // Given
    final Context context = new MessageInterpolatorContextImpl(ConstraintDescriptorImpl.builder()
        .setAttributes(Stream.of(new Object[][] {

            {"inclusive", false}, //
            {"days", 0}, //
            {"hours", 1}, //
            {"minutes", 25}, //
            {"seconds", 10}, //
            {"millis", 0}, //
            {"nanos", 0}, //

        }).collect(Collectors.toMap(data -> Objects.toString(data[0]), data -> data[1]))).build(),
        5);

    // When
    final String result = messageInterpolator
        .gwtInterpolate("must be shorter than${inclusive == true ? ' or equal to' : ''}"
            + "${days == 0 ? '' : ' ' += days += ' days'}"
            + "${hours == 0 ? '' : ' ' += hours += ' hours'}"
            + "${minutes == 0 ? '' : ' ' += minutes += ' minutes'}"
            + "${seconds == 0 ? '' : ' ' += seconds += ' seconds'}"
            + "${millis == 0 ? '' : ' ' += millis += ' millis'}"
            + "${nanos == 0 ? '' : ' ' += nanos += ' nanos'}", context);

    // Then
    Assert.assertEquals("must be shorter than 1 hours 25 minutes 10 seconds", result);
  }
}

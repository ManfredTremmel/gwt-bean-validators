/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.client;

import de.knightsoftnet.validators.shared.beans.HibernateInnTestBean;
import de.knightsoftnet.validators.shared.testcases.HibernateInnTestCases;

/**
 * test for inn validator.
 *
 * @author Manfred Tremmel
 *
 */
public class GwtTstHibernateInn extends AbstractValidationTst<HibernateInnTestBean> {

  /**
   * empty inn is allowed.
   */
  public final void testEmptyHibernateInnIsAllowed() {
    validationTest(HibernateInnTestCases.getEmptyTestBean(), true, null);
  }

  /**
   * correct inns are allowed.
   */
  public final void testCorrectHibernateInnsAreAllowed() {
    for (final HibernateInnTestBean testBean : HibernateInnTestCases.getCorrectTestBeans()) {
      validationTest(testBean, true, null);
    }
  }

  /**
   * wrong inns are not allowed.
   */
  public final void testWrongHibernateInnsAreWrong() {
    for (final HibernateInnTestBean testBean : HibernateInnTestCases.getWrongTestBeans()) {
      validationTest(testBean, false,
          "org.hibernate.validator.internal.constraintvalidators.hv.ru.INNValidator");
    }
  }
}

/*
 * Hibernate Validator, declare and validate application constraints
 *
 * License: Apache License, Version 2.0 See the license.txt file in the root directory or
 * <http://www.apache.org/licenses/LICENSE-2.0>.
 */
package org.hibernate.validator.internal.constraintvalidators.bv.time;

import org.hibernate.validator.constraintvalidation.HibernateConstraintValidator;
import org.hibernate.validator.constraintvalidation.HibernateConstraintValidatorInitializationContext;

import java.lang.annotation.Annotation;
import java.time.Clock;
import java.time.Duration;
import java.time.temporal.TemporalAccessor;

import jakarta.validation.ClockProvider;
import jakarta.validation.ConstraintValidatorContext;
import jakarta.validation.metadata.ConstraintDescriptor;

/**
 * Base class for all time validators that are based on the {@code java.time} package.
 *
 * @author Alaa Nassef
 * @author Guillaume Smet
 */
public abstract class AbstractJavaTimeValidator<C extends Annotation, T extends TemporalAccessor & Comparable<? super T>>
    implements HibernateConstraintValidator<C, T> {

  protected Clock referenceClock = Clock.systemUTC();

  @Override
  public void initialize(final ConstraintDescriptor<C> constraintDescriptor,
      final HibernateConstraintValidatorInitializationContext initializationContext) {
    try {
      this.referenceClock = Clock.systemDefaultZone();
    } catch (final Exception e) {
      this.referenceClock = Clock.systemUTC();
    }
  }

  @Override
  public boolean isValid(final T value, final ConstraintValidatorContext context) {
    // null values are valid
    if (value == null) {
      return true;
    }

    final int result = value.compareTo(getReferenceValue(referenceClock));

    return isValid(result);
  }

  /**
   * Returns the temporal validation tolerance to apply.
   */
  protected abstract Duration getEffectiveTemporalValidationTolerance(
      Duration absoluteTemporalValidationTolerance);

  /**
   * Returns an object of the validated type corresponding to the current time reference as provided
   * by the {@link ClockProvider}.
   */
  protected abstract T getReferenceValue(Clock reference);

  /**
   * Returns whether the result of the comparison between the validated value and the time reference
   * is considered valid.
   */
  protected abstract boolean isValid(int result);

}

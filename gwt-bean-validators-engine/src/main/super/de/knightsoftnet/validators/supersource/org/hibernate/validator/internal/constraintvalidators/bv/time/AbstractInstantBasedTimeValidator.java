/*
 * Hibernate Validator, declare and validate application constraints
 *
 * License: Apache License, Version 2.0 See the license.txt file in the root directory or
 * <http://www.apache.org/licenses/LICENSE-2.0>.
 */
package org.hibernate.validator.internal.constraintvalidators.bv.time;

import org.gwtproject.core.shared.GWT;
import org.hibernate.validator.constraintvalidation.HibernateConstraintValidator;
import org.hibernate.validator.constraintvalidation.HibernateConstraintValidatorInitializationContext;

import java.lang.annotation.Annotation;
import java.time.Duration;
import java.time.Instant;

import jakarta.validation.ConstraintValidatorContext;
import jakarta.validation.metadata.ConstraintDescriptor;

/**
 * Base class for all time validators that use an {@link Instant} to be compared to the time
 * reference.
 *
 * @author Alaa Nassef
 * @author Guillaume Smet
 */
public abstract class AbstractInstantBasedTimeValidator<C extends Annotation, T>
    implements HibernateConstraintValidator<C, T> {

  protected Instant referenceInstant = Instant.now();

  @Override
  public void initialize(final ConstraintDescriptor<C> constraintDescriptor,
      final HibernateConstraintValidatorInitializationContext initializationContext) {
    try {
      referenceInstant = Instant.now();
    } catch (final Exception e) {
      GWT.log(e.getMessage(), e);
      throw e;
    }
  }

  @Override
  public boolean isValid(final T value, final ConstraintValidatorContext context) {
    // null values are valid
    if (value == null) {
      return true;
    }

    final int result = getInstant(value).compareTo(referenceInstant);

    return isValid(result);
  }

  /**
   * Returns the temporal validation tolerance to apply.
   */
  protected abstract Duration getEffectiveTemporalValidationTolerance(
      Duration absoluteTemporalValidationTolerance);

  /**
   * Returns the {@link Instant} measured from Epoch.
   */
  protected abstract Instant getInstant(T value);

  /**
   * Returns whether the result of the comparison between the validated value and the time reference
   * is considered valid.
   */
  protected abstract boolean isValid(int result);

}

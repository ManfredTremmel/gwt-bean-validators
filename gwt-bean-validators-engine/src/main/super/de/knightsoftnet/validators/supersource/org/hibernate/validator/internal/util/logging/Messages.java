/*
 * Hibernate Validator, declare and validate application constraints
 *
 * License: Apache License, Version 2.0 See the license.txt file in the root directory or
 * <http://www.apache.org/licenses/LICENSE-2.0>.
 */
package org.hibernate.validator.internal.util.logging;

import org.hibernate.validator.internal.util.StringHelper;

/**
 * @author Hardy Ferentschik
 */
public class Messages {
  /**
   * The messages.
   */
  public static final Messages MESSAGES = new Messages();

  protected Messages() {
    super();
  }

  public String mustNotBeNull() {
    return "must not be null.";
  }

  public String mustNotBeNull(String parameterName) {
    return StringHelper.format("%s must not be null.", parameterName);
  }

  public String parameterMustNotBeNull(String parameterName) {
    return StringHelper.format("The parameter \"%s\" must not be null.", parameterName);
  }

  public String parameterMustNotBeEmpty(String parameterName) {
    return StringHelper.format("The parameter \"%s\" must not be empty.", parameterName);
  }

  public String parameterShouldBeGreaterThanOrEqualTo(String parameterName, int val) {
    return StringHelper.format("The parameter \"%s\" should be greater than or equal to %d.",
        parameterName, val);
  }

  public String parameterShouldBeLessThanOrEqualTo(String parameterName, int val) {
    return StringHelper.format("The parameter \"%s\" should be less than or equal to %d.",
        parameterName, val);
  }

  public String beanTypeCannotBeNull() {
    return "The bean type cannot be null.";
  }

  public String propertyPathCannotBeNull() {
    return "null is not allowed as property path.";
  }

  public String propertyNameMustNotBeEmpty() {
    return "The property name must not be empty.";
  }

  public String groupMustNotBeNull() {
    return "null passed as group name.";
  }

  public String beanTypeMustNotBeNull() {
    return "The bean type must not be null when creating a constraint mapping.";
  }

  public String methodNameMustNotBeNull() {
    return "The method name must not be null.";
  }

  public String validatedObjectMustNotBeNull() {
    return "The object to be validated must not be null.";
  }

  public String validatedMethodMustNotBeNull() {
    return "The method to be validated must not be null.";
  }

  public String classCannotBeNull() {
    return "The class cannot be null.";
  }

  public String classIsNull() {
    return "Class is null.";
  }

  public String validatedConstructorMustNotBeNull() {
    return "The constructor to be validated must not be null.";
  }

  public String validatedParameterArrayMustNotBeNull() {
    return "The method parameter array cannot not be null.";
  }

  public String validatedConstructorCreatedInstanceMustNotBeNull() {
    return "The created instance must not be null.";
  }

  public String inputStreamCannotBeNull() {
    return "The input stream for #addMapping() cannot be null.";
  }

  public String constraintOnConstructorOfNonStaticInnerClass() {
    return "Constraints on the parameters of constructors of non-static inner classes "
        + "are not supported if those parameters have a generic type due to JDK bug JDK-5087240.";
  }

  public String parameterizedTypesWithMoreThanOneTypeArgument() {
    return "Custom parameterized types with more than one type argument are not supported and "
        + "will not be checked for type use constraints.";
  }

  public String unableToUseResourceBundleAggregation() {
    return "Hibernate Validator cannot instantiate AggregateResourceBundle.CONTROL. "
        + "This can happen most notably in a Google App Engine environment or when running Hibernate Validator as Java 9 named module. "
        + "A PlatformResourceBundleLocator without bundle aggregation was created. "
        + "This only affects you in case you are using multiple ConstraintDefinitionContributor JARs. "
        + "ConstraintDefinitionContributors are a Hibernate Validator specific feature. All Jakarta Bean Validation "
        + "features work as expected. See also https://hibernate.atlassian.net/browse/HV-1023.";
  }

  public String annotationTypeMustNotBeNull() {
    return "The annotation type must not be null when creating a constraint definition.";
  }

  public String annotationTypeMustBeAnnotatedWithConstraint() {
    return "The annotation type must be annotated with @jakarta.validation.Constraint when creating a constraint definition.";
  }
}

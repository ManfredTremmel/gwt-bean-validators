/*
 * Hibernate Validator, declare and validate application constraints
 *
 * License: Apache License, Version 2.0 See the license.txt file in the root directory or
 * <http://www.apache.org/licenses/LICENSE-2.0>.
 */

package org.hibernate.validator.internal.constraintvalidators.hv;

import org.gwtproject.regexp.shared.RegExp;

import org.hibernate.validator.internal.util.logging.Log;
import org.hibernate.validator.internal.util.logging.LoggerFactory;

import java.lang.invoke.MethodHandles;
import java.util.ArrayList;
import java.util.List;

import jakarta.validation.ConstraintValidatorContext;

/**
 * ModCheckBase contains all shared methods and options used by Mod Check Validators.
 * http://en.wikipedia.org/wiki/Check_digit
 *
 * @author George Gastaldi
 * @author Hardy Ferentschik
 * @author Victor Rezende dos Santos
 * @author Manfred Tremmel - GWT port
 */
public abstract class ModCheckBase { // NOPMD

  private static final Log LOG = LoggerFactory.make(MethodHandles.lookup()); // NOPMD

  private static final RegExp NUMBERS_ONLY_REGEXP = RegExp.compile("[^0-9]", "g");

  private static final int DEC_RADIX = 10;

  /**
   * The start index for the checksum calculation.
   */
  private int startIndex;

  /**
   * The end index for the checksum calculation.
   */
  private int endIndex;

  /**
   * The index of the checksum digit.
   */
  private int checkDigitIndex;

  private boolean ignoreNonDigitCharacters;

  /**
   * valid check.
   *
   * @param value value to check.
   * @param context constraint validator context
   * @return true if valid
   */
  public boolean isValid(final CharSequence value, final ConstraintValidatorContext context) {
    if (value == null) {
      return true;
    }

    final String valueAsString = value.toString();
    String digitsAsString;
    char checkDigit;
    try {
      digitsAsString = extractVerificationString(valueAsString);
      checkDigit = extractCheckDigit(valueAsString);
    } catch (final IndexOutOfBoundsException e) {
      return false;
    }
    digitsAsString = stripNonDigitsIfRequired(digitsAsString);

    List<Integer> digits;
    try {
      digits = extractDigits(digitsAsString);
    } catch (final NumberFormatException e) {
      return false;
    }

    return isCheckDigitValid(digits, checkDigit);
  }

  public abstract boolean isCheckDigitValid(List<Integer> digits, char checkDigit);

  protected void initialize(final int startIndex, final int endIndex, final int checkDigitIndex,
      final boolean ignoreNonDigitCharacters) {
    this.startIndex = startIndex;
    this.endIndex = endIndex;
    this.checkDigitIndex = checkDigitIndex;
    this.ignoreNonDigitCharacters = ignoreNonDigitCharacters;

    validateOptions();
  }

  /**
   * Returns the numeric {@code int} value of a {@code char}.
   *
   * @param value the input {@code char} to be parsed
   *
   * @return the numeric {@code int} value represented by the character.
   *
   * @throws NumberFormatException in case character is not a digit
   */
  protected int extractDigit(final char value) throws NumberFormatException {
    if (Character.isDigit(value)) {
      return Character.digit(value, DEC_RADIX);
    } else {
      throw LOG.getCharacterIsNotADigitException(value);
    }
  }

  /**
   * Parses the {@link String} value as a {@link List} of {@link Integer} objects.
   *
   * @param value the input string to be parsed
   *
   * @return List of {@code Integer} objects.
   *
   * @throws NumberFormatException in case any of the characters is not a digit
   */
  private List<Integer> extractDigits(final String value) throws NumberFormatException {
    final List<Integer> digits = new ArrayList<>(value.length());
    final char[] chars = value.toCharArray();
    for (final char c : chars) {
      digits.add(extractDigit(c));
    }
    return digits;
  }

  private boolean validateOptions() {
    if (startIndex < 0) {
      throw LOG.getStartIndexCannotBeNegativeException(startIndex);
    }

    if (endIndex < 0) {
      throw LOG.getEndIndexCannotBeNegativeException(endIndex);
    }

    if (startIndex > endIndex) {
      throw LOG.getInvalidRangeException(startIndex, endIndex);
    }

    if (checkDigitIndex > 0 && startIndex <= checkDigitIndex && endIndex > checkDigitIndex) {
      throw LOG.getInvalidCheckDigitException(startIndex, endIndex);
    }

    return true;
  }

  private String stripNonDigitsIfRequired(final String value) {
    if (ignoreNonDigitCharacters) {
      return NUMBERS_ONLY_REGEXP.replace(value, "");
    } else {
      return value;
    }
  }

  private String extractVerificationString(final String value) throws IndexOutOfBoundsException {
    // the string contains the check digit, just return the digits to verify
    if (endIndex == Integer.MAX_VALUE) {
      return value.substring(0, value.length() - 1);
    } else if (checkDigitIndex == -1) {
      return value.substring(startIndex, endIndex);
    } else {
      return value.substring(startIndex, endIndex + 1);
    }
  }

  private char extractCheckDigit(final String value) throws IndexOutOfBoundsException {
    // take last character of string to be validated unless the index is given explicitly
    if (checkDigitIndex == -1) {
      if (endIndex == Integer.MAX_VALUE) {
        return value.charAt(value.length() - 1);
      } else {
        return value.charAt(endIndex);
      }
    } else {
      return value.charAt(checkDigitIndex);
    }
  }

}

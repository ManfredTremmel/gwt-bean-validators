package org.hibernate.validator.internal.util.logging;

import java.util.List;
import java.util.Set;

/**
 * localizeable log messages.
 *
 * @author Manfred Tremmel - GWT implementation
 */
public class LogMessages {

  public String version(final String arg0) {
    return "Hibernate Validator " + arg0;
  }

  public String ignoringXmlConfiguration() {
    return "Ignoring XML configuration.";
  }

  public String usingConstraintFactory(final String arg0) {
    return "Using " + arg0 + " as constraint factory.";
  }

  public String usingMessageInterpolator(final String arg0) {
    return "Using " + arg0 + " as message interpolator.";
  }

  public String usingTraversableResolver(final String arg0) {
    return "Using " + arg0 + " as traversable resolver.";
  }

  public String usingValidationProvider(final String arg0) {
    return "Using " + arg0 + " as validation provider.";
  }

  public String parsingXMLFile(final String arg0) {
    return arg0 + " found. Parsing XML based configuration.";
  }

  public String unableToCloseInputStream() {
    return "Unable to close input stream.";
  }

  public String unableToCloseXMLFileInputStream(final String arg0) {
    return "Unable to close input stream for " + arg0 + ".";
  }

  public String unableToCreateSchema(final String arg0, final String arg1) {
    return "Unable to create schema for " + arg0 + ": " + arg1;
  }

  public String getUnableToCreateAnnotationForConfiguredConstraintException() {
    return "Unable to create annotation for configured constraint";
  }

  public String getUnableToFindPropertyWithAccessException(final String arg0, final String arg1,
      final String arg2) {
    return "The class " + arg0 + " does not have a property '" + arg1 + "' with access " + arg2
        + ".";
  }

  public String getUnableToFindMethodException(final Class<?> arg0, final String arg1) {
    return "Type " + arg0 + " doesn't have a method " + arg1 + ".";
  }

  public String getInvalidBigDecimalFormatException(final String arg0) {
    return arg0 + " does not represent a valid BigDecimal format.";
  }

  public String getInvalidLengthForIntegerPartException() {
    return "The length of the integer part cannot be negative.";
  }

  public String getInvalidLengthForFractionPartException() {
    return "The length of the fraction part cannot be negative.";
  }

  public String getMinCannotBeNegativeException() {
    return "The min parameter cannot be negative.";
  }

  public String getMaxCannotBeNegativeException() {
    return "The max parameter cannot be negative.";
  }

  public String getLengthCannotBeNegativeException() {
    return "The length cannot be negative.";
  }

  public String getInvalidRegularExpressionException() {
    return "Invalid regular expression.";
  }

  public String getErrorDuringScriptExecutionException(final String arg0) {
    return "Error during execution of script \"" + arg0 + "\" occurred.";
  }

  public String getScriptMustReturnTrueOrFalseExceptionSingle(final String arg0) {
    return "Script \"" + arg0 + "\" returned null, but must return either true or false.";
  }

  public String getScriptMustReturnTrueOrFalseException(final String arg0, final String arg1,
      final String arg2) {
    return "Script \"" + arg0 + "\" returned " + arg1 + " (of type " + arg2
        + "), but must return either true or false.";
  }

  public String getInconsistentConfigurationException() {
    return "Assertion error: inconsistent ConfigurationImpl construction.";
  }

  public String getUnableToFindProviderException(final Class<?> arg0) {
    return "Unable to find provider: " + arg0 + ".";
  }

  public String getExceptionDuringIsValidCallException() {
    return "Unexpected exception during isValid call.";
  }

  public String getConstraintFactoryMustNotReturnNullException(final String arg0) {
    return "Constraint factory returned null when trying to create instance of " + arg0 + ".";
  }

  public String getNoValidatorFoundForTypeException(final String arg0, final String arg1,
      final String arg2) {
    return "No validator could be found for constraint '" + arg0 + "' validating type '" + arg1
        + "'. Check configuration for '" + arg2 + "'";
  }

  public String getUnableToInitializeConstraintValidatorException(final String arg0) {
    return "Unable to initialize " + arg0 + ".";
  }

  public String getAtLeastOneCustomMessageMustBeCreatedException() {
    return "At least one custom message must be created if the default error message gets disabled.";
  }

  public String getInvalidJavaIdentifierException(final String arg0) {
    return arg0 + " is not a valid Java Identifier.";
  }

  public String getUnableToParsePropertyPathException(final String arg0) {
    return "Unable to parse property path " + arg0 + ".";
  }

  public String getTypeNotSupportedForUnwrappingException(final Class<?> arg0) {
    return "Type " + arg0 + " not supported for unwrapping.";
  }

  public String getInconsistentFailFastConfigurationException() {
    return "Inconsistent fail fast configuration. Fail fast enabled via programmatic API, "
        + "but explicitly disabled via properties.";
  }

  public String getInvalidPropertyPathExceptionNoArg() {
    return "Invalid property path.";
  }

  public String getInvalidPropertyPathException(final String arg0, final String arg1) {
    return "Invalid property path. Either there is no property " + arg1 + " in entity " + arg0
        + " or it is not possible to cascade to the property.";
  }

  public String getPropertyPathMustProvideIndexOrMapKeyException() {
    return "Property path must provide index or map key.";
  }

  public String getErrorDuringCallOfTraversableResolverIsReachableException() {
    return "Call to TraversableResolver.isReachable() threw an exception.";
  }

  public String getErrorDuringCallOfTraversableResolverIsCascadableException() {
    return "Call to TraversableResolver.isCascadable() threw an exception.";
  }

  public String getUnableToExpandDefaultGroupListException(final List<?> arg0, final List<?> arg1) {
    return "Unable to expand default group list " + arg0.size() + " into sequence " + arg1.size()
        + ".";
  }

  public String getAtLeastOneGroupHasToBeSpecifiedException() {
    return "At least one group has to be specified.";
  }

  public String getGroupHasToBeAnInterfaceException(final String arg0) {
    return "A group has to be an interface. " + arg0 + " is not.";
  }

  public String getSequenceDefinitionsNotAllowedException() {
    return "Sequence definitions are not allowed as composing parts of a sequence.";
  }

  public String getCyclicDependencyInGroupsDefinitionException() {
    return "Cyclic dependency in groups definition";
  }

  public String getUnableToExpandGroupSequenceException() {
    return "Unable to expand group sequence.";
  }

  public String getInvalidDefaultGroupSequenceDefinitionException() {
    return "Default group sequence and default group sequence provider cannot be defined at the "
        + "same time.";
  }

  public String getNoDefaultGroupInGroupSequenceException() {
    return "'Default.class' cannot appear in default group sequence list.";
  }

  public String getBeanClassMustBePartOfRedefinedDefaultGroupSequenceException(final String arg0) {
    return arg0 + " must be part of the redefined default group sequence.";
  }

  public String getWrongDefaultGroupSequenceProviderTypeException(final String arg0) {
    return "The default group sequence provider defined for " + arg0 + " has the wrong type";
  }

  public String getInvalidExecutableParameterIndexException(final String arg0, final int arg1) {
    return "Method or constructor " + arg0 + " doesn't have a parameter with index " + arg1 + ".";
  }

  public String getUnableToRetrieveAnnotationParameterValueException() {
    return "Unable to retrieve annotation parameter value.";
  }

  public String getInvalidLengthOfParameterMetaDataListException(final String arg0, final int arg1,
      final int arg2) {
    return "Method or constructor " + arg0 + " has " + arg1
        + " parameters, but the passed list of parameter meta data has a size of " + arg2 + ".";
  }

  public String getUnableToInstantiateExceptionOneArg(final String arg0) {
    return "Unable to instantiate " + arg0 + ".";
  }

  public String getUnableToInstantiateException(final String arg0, final Class<?> arg1) {
    return "Unable to instantiate " + arg0 + ": " + arg1 + ".";
  }

  public String getUnableToLoadClassException(final String arg0, final Object arg1) {
    return "Unable to load class: " + arg0 + " from " + arg1 + ".";
  }

  public String getStartIndexCannotBeNegativeException(final int arg0) {
    return "Start index cannot be negative: " + arg0 + ".";
  }

  public String getEndIndexCannotBeNegativeException(final int arg0) {
    return "End index cannot be negative: " + arg0 + ".";
  }

  public String getInvalidRangeException(final int arg0, final int arg1) {
    return "Invalid Range: " + arg0 + " > " + arg1 + ".";
  }

  public String getInvalidCheckDigitException(final int arg0, final int arg1) {
    return "A explicitly specified check digit must lie outside the interval: [" + arg0 + ", "
        + arg1 + "].";
  }

  public String getCharacterIsNotADigitException(final char arg0) {
    return "'" + arg0 + "' is not a digit.";
  }

  public String getConstraintParametersCannotStartWithValidException() {
    return "Parameters starting with 'valid' are not allowed in a constraint.";
  }

  public String getConstraintWithoutMandatoryParameterException(final String arg0,
      final String arg1) {
    return arg1 + " contains Constraint annotation, but does not contain a " + arg0 + " parameter.";
  }

  public String getWrongDefaultValueForPayloadParameterException(final String arg0) {
    return arg0
        + " contains Constraint annotation, but the payload parameter default value is not the empty "
        + "array.";
  }

  public String getWrongTypeForPayloadParameterException(final String arg0) {
    return arg0 + " contains Constraint annotation, but the payload parameter is of wrong type.";
  }

  public String getWrongDefaultValueForGroupsParameterException(final String arg0) {
    return arg0
        + " contains Constraint annotation, but the groups parameter default value is not the empty "
        + "array.";
  }

  public String getWrongTypeForGroupsParameterException(final String arg0) {
    return arg0 + " contains Constraint annotation, but the groups parameter is of wrong type.";
  }

  public String getWrongTypeForMessageParameterException(final String arg0) {
    return arg0 + " contains Constraint annotation, but the message parameter is not of type "
        + "String.";
  }

  public String getOverriddenConstraintAttributeNotFoundException(final String arg0) {
    return "Overridden constraint does not define an attribute with name " + arg0 + ".";
  }

  public String getWrongAttributeTypeForOverriddenConstraintException(final String arg0,
      final Class<?> arg1) {
    return "The overriding type of a composite constraint must be identical to the overridden one."
        + " Expected " + arg0 + " found " + arg1 + ".";
  }

  public String getWrongParameterTypeException(final String arg0, final String arg1) {
    return "Wrong parameter type. Expected: " + arg0 + " Actual: " + arg1 + ".";
  }

  public String getUnableToFindAnnotationParameterException(final String arg0) {
    return "The specified annotation defines no parameter '" + arg0 + "'.";
  }

  public String getUnableToGetAnnotationParameterException(final String arg0, final String arg1) {
    return "Unable to get '" + arg0 + "' from " + arg1 + ".";
  }

  public String getNoValueProvidedForAnnotationParameterException(final String arg0,
      final String arg1) {
    return "No value provided for parameter '" + arg0 + "' of annotation " + arg1 + ".";
  }

  public String getTryingToInstantiateAnnotationWithUnknownParametersException(final Class<?> arg0,
      final Set<String> arg1) {
    return "Trying to instantiate " + arg0 + " with unknown parameter(s): " + arg1 + ".";
  }

  public String getPropertyNameCannotBeNullOrEmptyException() {
    return "Property name cannot be null or empty.";
  }

  public String getElementTypeHasToBeFieldOrMethodException() {
    return "Element type has to be FIELD or METHOD.";
  }

  public String getUnableToAccessMemberException(final String arg0) {
    return "Unable to access " + arg0 + ".";
  }

  public String getHasToBeAPrimitiveTypeException(final Class<?> arg0) {
    return "" + arg0 + " has to be a primitive type.";
  }

  public String getNullIsAnInvalidTypeForAConstraintValidatorException() {
    return "null is an invalid type for a constraint validator.";
  }

  public String getUnableToInstantiateConstraintFactoryClassException(final String arg0) {
    return "Unable to instantiate constraint factory class " + arg0 + ".";
  }

  public String getUnableToOpenInputStreamForMappingFileException(final String arg0) {
    return "Unable to open input stream for mapping file " + arg0 + ".";
  }

  public String getUnableToInstantiateMessageInterpolatorClassException(final String arg0) {
    return "Unable to instantiate message interpolator class " + arg0 + ".";
  }

  public String getUnableToInstantiateTraversableResolverClassException(final String arg0) {
    return "Unable to instantiate traversable resolver class " + arg0 + ".";
  }

  public String getUnableToInstantiateValidationProviderClassException(final String arg0) {
    return "Unable to instantiate validation provider class " + arg0 + ".";
  }

  public String getUnableToParseValidationXmlFileException(final String arg0) {
    return "Unable to parse " + arg0 + ".";
  }

  public String getIsNotAnAnnotationException(final String arg0) {
    return arg0 + " is not an annotation.";
  }

  public String getIsNotAConstraintValidatorClassException(final Class<?> arg0) {
    return "" + arg0 + " is not a constraint validator class.";
  }

  public String getBeanClassHasAlreadyBeenConfiguredInXmlException(final String arg0) {
    return arg0 + " is configured at least twice in xml.";
  }

  public String getIsDefinedTwiceInMappingXmlForBeanException(final String arg0,
      final String arg1) {
    return arg0 + " is defined twice in mapping xml for bean " + arg1 + ".";
  }

  public String getBeanDoesNotContainTheFieldException(final String arg0, final String arg1) {
    return arg0 + " does not contain the fieldType " + arg1 + ".";
  }

  public String getBeanDoesNotContainThePropertyException(final String arg0, final String arg1) {
    return arg0 + " does not contain the property " + arg1 + ".";
  }

  public String getAnnotationDoesNotContainAParameterException(final String arg0,
      final String arg1) {
    return "Annotation of type " + arg0 + " does not contain a parameter " + arg1 + ".";
  }

  public String getAttemptToSpecifyAnArrayWhereSingleValueIsExpectedException() {
    return "Attempt to specify an array where single value is expected.";
  }

  public String getUnexpectedParameterValueException() {
    return "Unexpected parameter value.";
  }

  public String getInvalidNumberFormatException(final String arg0) {
    return "Invalid " + arg0 + " format.";
  }

  public String getInvalidCharValueException(final String arg0) {
    return "Invalid char value: " + arg0 + ".";
  }

  public String getInvalidReturnTypeException(final Class<?> arg0) {
    return "Invalid return type: " + arg0 + ". Should be a enumeration type.";
  }

  public String getReservedParameterNamesException(final String arg0, final String arg1,
      final String arg2) {
    return arg0 + ", " + arg1 + ", " + arg2 + " are reserved parameter names.";
  }

  public String getWrongPayloadClassException(final String arg0) {
    return "Specified payload class " + arg0 + " does not implement jakarta.validation.Payload";
  }

  public String getErrorParsingMappingFileException() {
    return "Error parsing mapping file.";
  }

  public String getIllegalArgumentException(final String arg0) {
    return arg0;
  }

  public String usingParameterNameProvider(final String arg0) {
    return "Using " + arg0 + " as parameter name provider.";
  }

  public String getUnableToInstantiateParameterNameProviderClassException(final String arg0) {
    return "Unable to instantiate parameter name provider class " + arg0 + ".";
  }

  public String getUnableToDetermineSchemaVersionException(final String arg0) {
    return "Unable to parse " + arg0 + ".";
  }

  public String getUnsupportedSchemaVersionException(final String arg0, final String arg1) {
    return "Unsupported schema version for " + arg0 + ": " + arg1 + ".";
  }

  public String getMultipleGroupConversionsForSameSourceException(final Class<?> arg0,
      final Set<Class<?>> arg1) {
    return "Found multiple group conversions for source group " + arg0 + ": " + arg1 + ".";
  }

  public String getGroupConversionOnNonCascadingElementException(final String arg0) {
    return "Found group conversions for non-cascading element: " + arg0 + ".";
  }

  public String getGroupConversionForSequenceException(final Class<?> arg0) {
    return "Found group conversion using a group sequence as source: " + arg0 + ".";
  }

  public String unknownPropertyInExpressionLanguage(final String arg0) {
    return "EL expression '" + arg0 + "' references an unknown property";
  }

  public String errorInExpressionLanguage(final String arg0) {
    return "Error in EL expression '" + arg0 + "'";
  }

  public String getBeanDoesNotContainConstructorException(final String arg0, final String arg1) {
    return arg0 + " does not contain a constructor with the parameter types " + arg1 + ".";
  }

  public String getInvalidParameterTypeException(final String arg0, final String arg1) {
    return "Unable to load parameter of type '" + arg0 + "' in " + arg1 + ".";
  }

  public String getBeanDoesNotContainMethodException(final String arg0, final String arg1,
      final List<Class<?>> arg2) {
    return arg0 + " does not contain a method with the name '" + arg1 + "' and parameter types "
        + arg2.size() + ".";
  }

  public String getUnableToLoadConstraintAnnotationClassException(final String arg0) {
    return "The specified constraint annotation class " + arg0 + " cannot be loaded.";
  }

  public String getMethodIsDefinedTwiceInMappingXmlForBeanException(final String arg0,
      final String arg1) {
    return "The method '" + arg0 + "' is defined twice in the mapping xml for bean " + arg1 + ".";
  }

  public String getConstructorIsDefinedTwiceInMappingXmlForBeanException(final String arg0,
      final String arg1) {
    return "The constructor '" + arg0 + "' is defined twice in the mapping xml for bean " + arg1
        + ".";
  }

  public String getMultipleCrossParameterValidatorClassesException(final String arg0) {
    return "The constraint '" + arg0
        + "' defines multiple cross parameter validators. Only one is allowed.";
  }

  public String getImplicitConstraintTargetInAmbiguousConfigurationException(final String arg0) {
    return "The constraint " + arg0
        + " used ConstraintTarget#IMPLICIT where the target cannot be inferred.";
  }

  public String getCrossParameterConstraintOnMethodWithoutParametersException(final String arg0,
      final String arg1) {
    return "Cross parameter constraint " + arg0
        + " is illegally placed on a parameterless method or constructor '" + arg1 + "'.";
  }

  public String getCrossParameterConstraintOnClassException(final String arg0) {
    return "Cross parameter constraint " + arg0 + " is illegally placed on class level.";
  }

  public String getCrossParameterConstraintOnFieldException(final String arg0, final String arg1) {
    return "Cross parameter constraint " + arg0 + " is illegally placed on field '" + arg1 + "'.";
  }

  public String getParameterNodeAddedForNonCrossParameterConstraintException(
      final jakarta.validation.Path arg0) {
    return "No parameter nodes may be added since path " + arg0
        + " doesn't refer to a cross-parameter constraint.";
  }

  public String getConstrainedElementConfiguredMultipleTimesException(final String arg0) {
    return arg0
        + " is configured multiple times (note, <getter> and <method> nodes for the same method are"
        + " not allowed)";
  }

  public String evaluatingExpressionLanguageExpressionCausedException(final String arg0) {
    return "An exception occurred during evaluation of EL expression '" + arg0 + "'";
  }

  public String getExceptionOccurredDuringMessageInterpolationException() {
    return "An exception occurred during message interpolation";
  }

  public String getMultipleValidatorsForSameTypeException(final String arg0, final String arg1) {
    return "The constraint '" + arg0 + "' defines multiple validators for the type '" + arg1
        + "'. Only one is allowed.";
  }

  public String getCrossParameterConstraintHasNoValidatorException(final String arg0) {
    return "Cross parameter constraint " + arg0 + " has no cross-parameter validator.";
  }

  public String getGenericAndCrossParameterConstraintDoesNotDefineValidationAppliesToParameterException(
      final String arg0) {
    return "Constraints with generic as well as cross-parameter validators must define an "
        + "attribute validationAppliesTo(), but constraint " + arg0 + " doesn't.";
  }

  public String getValidationAppliesToParameterMustHaveReturnTypeConstraintTargetException(
      final String arg0) {
    return "Return type of the attribute validationAppliesTo() of the constraint " + arg0
        + " must be jakarta.validation.ConstraintTarget.";
  }

  public String getValidationAppliesToParameterMustHaveDefaultValueImplicitException(
      final String arg0) {
    return "Default value of the attribute validationAppliesTo() of the constraint " + arg0
        + " must be ConstraintTarget#IMPLICIT.";
  }

  public String getValidationAppliesToParameterMustNotBeDefinedForNonGenericAndCrossParameterConstraintException(
      final String arg0) {
    return "Only constraints with generic as well as cross-parameter validators must define"
        + " an attribute validationAppliesTo(), but constraint " + arg0 + " does.";
  }

  public String getValidatorForCrossParameterConstraintMustEitherValidateObjectOrObjectArrayException(
      final String arg0) {
    return "Validator for cross-parameter constraint " + arg0
        + " does not validate Object nor Object[].";
  }

  public String getHasToBeABoxedTypeException(final Class<?> arg0) {
    return "" + arg0 + " has to be a auto-boxed type.";
  }

  public String getMixingImplicitWithOtherExecutableTypesException() {
    return "Mixing IMPLICIT and other executable types is not allowed.";
  }

  public String getOverridingConstraintDefinitionsInMultipleMappingFilesException(
      final String arg0) {
    return "A given constraint definition can only be overridden in one mapping file. " + arg0
        + " is overridden in multiple files";
  }

  public String getUnbalancedBeginEndParameterException(final String arg0, final char arg1) {
    return "The message descriptor '" + arg0 + "' contains an unbalanced meta character '" + arg1
        + "' parameter.";
  }

  public String getNestedParameterException(final String arg0) {
    return "The message descriptor '" + arg0 + "' has nested parameters.";
  }

  public String getCreationOfScriptExecutorFailedException(final String arg0) {
    return "No JSR-223 scripting engine could be bootstrapped for language \"" + arg0 + "\".";
  }

  public String getBeanClassHasAlreadyBeConfiguredViaProgrammaticApiException(final String arg0) {
    return arg0 + " is configured more than once via the programmatic constraint declaration API.";
  }

  public String getPropertyHasAlreadyBeConfiguredViaProgrammaticApiException(final String arg0,
      final String arg1) {
    return "Property \"" + arg1 + "\" of type " + arg0
        + " is configured more than once via the programmatic constraint declaration API.";
  }

  public String getMethodHasAlreadyBeenConfiguredViaProgrammaticApiException(final String arg0,
      final String arg1) {
    return "Method " + arg1 + " of type " + arg0
        + " is configured more than once via the programmatic constraint declaration API.";
  }

  public String getParameterHasAlreadyBeConfiguredViaProgrammaticApiException(final String arg0,
      final String arg1, final int arg2) {
    return "Parameter " + arg2 + " of method or constructor " + arg1 + " of type " + arg0
        + " is configured more than once via the programmatic constraint declaration API.";
  }

  public String getReturnValueHasAlreadyBeConfiguredViaProgrammaticApiException(final String arg0,
      final String arg1) {
    return "The return value of method or constructor " + arg1 + " of type " + arg0
        + " is configured more than once via the programmatic constraint declaration API.";
  }

  public String getConstructorHasAlreadyBeConfiguredViaProgrammaticApiException(final String arg0,
      final String arg1) {
    return "Constructor " + arg1 + " of type " + arg0
        + " is configured more than once via the programmatic constraint declaration API.";
  }

  public String getCrossParameterElementHasAlreadyBeConfiguredViaProgrammaticApiException(
      final String arg0, final String arg1) {
    return "Cross-parameter constraints for the method or constructor " + arg1 + " of type " + arg0
        + " are declared more than once via the programmatic constraint declaration API.";
  }

  public String getMultiplierCannotBeNegativeException(final int arg0) {
    return "Multiplier cannot be negative: " + arg0 + ".";
  }

  public String getWeightCannotBeNegativeException(final int arg0) {
    return "Weight cannot be negative: " + arg0 + ".";
  }

  public String getTreatCheckAsIsNotADigitNorALetterException(final int arg0) {
    return "'" + arg0 + "' is not a digit nor a letter.";
  }

  public String getInvalidParameterCountForExecutableException(final String arg0, final int arg1,
      final int arg2) {
    return "Wrong number of parameters. Method or constructor " + arg0 + " expects " + arg1
        + " parameters, but got " + arg2 + ".";
  }

  public String getNoUnwrapperFoundForTypeException(final String arg0) {
    return "No validation value unwrapper is registered for type '" + arg0 + "'.";
  }

  public String getMissingELDependenciesException() {
    return "Unable to load 'javax.el.ExpressionFactory'. Check that you have the EL dependencies on"
        + " the classpath, or use ParameterMessageInterpolator instead";
  }

  public String creationOfParameterMessageInterpolation() {
    return "ParameterMessageInterpolator has been chosen, EL interpolation will not be supported";
  }

  public String getElUnsupported(final String arg0) {
    return "Message contains EL expression: " + arg0
        + ", which is unsupported with chosen Interpolator";
  }

  public String getConstraintValidatorExistsForWrapperAndWrappedValueException(final String arg0,
      final String arg1, final String arg2) {
    return "The constraint of type '" + arg1 + "' defined on '" + arg0
        + "' has multiple matching constraint validators which is due to an additional value "
        + "handler of type '" + arg2
        + "'. It is unclear which value needs validating. Clarify configuration via "
        + "@UnwrapValidatedValue.";
  }

  public String getTypeAnnotationConstraintOnIterableRequiresUseOfValidAnnotationException(
      final String arg0, final String arg1) {
    return "When using type annotation constraints on parameterized iterables or map @Valid must be"
        + " used. Check " + arg0 + "#" + arg1;
  }

  public String parameterizedTypeWithMoreThanOneTypeArgumentIsNotSupported(final String arg0) {
    return "Parameterized type with more than one argument is not supported: " + arg0;
  }

  public String getInconsistentValueUnwrappingConfigurationBetweenFieldAndItsGetterException(
      final String arg0, final String arg1) {
    return "The configuration of value unwrapping for property '" + arg0 + "' of bean '" + arg1
        + "' is inconsistent between the field and its getter.";
  }

  public String getUnableToCreateXMLEventReader(final String arg0) {
    return "Unable to parse " + arg0 + ".";
  }

  public String validatedValueUnwrapperCannotBeCreated(final String arg0) {
    return "Error creating unwrapper: " + arg0;
  }

  public String unknownJvmVersion(final String arg0) {
    return "Couldn't determine Java version from value " + arg0
        + "; Not enabling features requiring Java 8";
  }

  public String getConstraintHasAlreadyBeenConfiguredViaProgrammaticApiException(
      final String arg0) {
    return arg0 + " is configured more than once via the programmatic constraint definition API.";
  }

  public String getEmptyElementOnlySupportedWhenCharSequenceIsExpectedExpection() {
    return "An empty element is only supported when a CharSequence is expected.";
  }

  public String getUnableToReachPropertyToValidateException(final Object arg0, final String arg1) {
    return "Unable to reach the property to validate for the bean " + arg0
        + " and the property path " + arg1 + ". A property is null along the way.";
  }

  public String getUnableToConvertTypeToClassException(final String arg0) {
    return "Unable to convert the Type " + arg0 + " to a Class.";
  }

  public String logConstraintValidatorPayload(final Object arg0) {
    return "Constraint validator payload set to " + arg0 + ".";
  }

  public String logUnknownElementInXmlConfiguration(final String arg0) {
    return "Encountered unsupported element " + arg0 + " while parsing the XML configuration.";
  }

  public String logUnableToLoadOrInstantiateJPAAwareResolver(final String arg0) {
    return "Unable to load or instantiate JPA aware resolver " + arg0
        + ". All properties will per default be traversable.";
  }

  public String getConstraintValidatorDefinitionConstraintMismatchException(final String arg0,
      final String arg1, final String arg2) {
    return "Constraint " + arg1 + " references constraint validator type " + arg0
        + ", but this validator is defined for constraint type " + arg2 + ".";
  }

  public String getUnexpectedConstraintElementType(final String arg0, final String arg1) {
    return "ConstrainedElement expected class was " + arg0 + ", but instead received " + arg1 + ".";
  }

  public String getUnsupportedConstraintElementType(final String arg0) {
    return "Allowed constraint element types are FIELD and GETTER, but instead received " + arg0
        + ".";
  }

  public String usingGetterPropertySelectionStrategy(final String arg0) {
    return "Using " + arg0 + " as getter property selection strategy.";
  }

  public String getUnableToInstantiateGetterPropertySelectionStrategyClassException(
      final String arg0) {
    return "Unable to instantiate getter property selection strategy class " + arg0 + ".";
  }

  public String unableToGetXmlSchema(final String arg0) {
    return "Unable to get an XML schema named " + arg0 + ".";
  }

  public String uninitializedLocale(final String arg0) {
    return "Uninitialized locale: " + arg0
        + ". Please register your locale as a locale to initialize when initializing your"
        + " ValidatorFactory.";
  }

  public String unableToLoadInstanceOfService(final String arg0) {
    return "An error occurred while loading an instance of service " + arg0 + ".";
  }

  public String usingPropertyNodeNameProvider(final String arg0) {
    return "Using " + arg0 + " as property node name provider.";
  }

  public String getUnableToInstantiatePropertyNodeNameProviderClassException(final String arg0) {
    return "Unable to instantiate property node name provider class " + arg0 + ".";
  }

  public String missingParameterMetadataWithSyntheticOrImplicitParameters(final String arg0) {
    return "Missing parameter metadata for " + arg0
        + ", which declares implicit or synthetic parameters. Automatic resolution of generic type"
        + " information for method parameters may yield incorrect results if multiple parameters"
        + " have the same erasure. To solve this, compile your code with the -parameters flag.";
  }

  public String usingLocaleResolver(final String arg0) {
    return "Using " + arg0 + " as locale resolver.";
  }

  public String getUnableToInstantiateLocaleResolverClassException(final String arg0) {
    return "Unable to instantiate locale resolver class " + arg0 + ".";
  }

  public String getExpressionVariablesDefinedWithExpressionLanguageNotEnabled(final String arg0) {
    return "Expression variables have been defined for constraint " + arg0
        + " while Expression Language is not enabled.";
  }

  public String getExpressionsNotResolvedWhenExpressionLanguageFeaturesDisabled() {
    return "Expressions should not be resolved when Expression Language features are disabled.";
  }

  public String getExpressionsLanguageFeatureLevelNotSupported() {
    return "Provided Expression Language feature level is not supported.";
  }

  public String getLogConstraintExpressionLanguageFeatureLevel(final String arg0) {
    return "Expression Language feature level for constraints set to " + arg0 + ".";
  }

  public String getLogCustomViolationExpressionLanguageFeatureLevel(final String arg0) {
    return "Expression Language feature level for custom violations set to " + arg0 + ".";
  }

  public String getInvalidExpressionLanguageFeatureLevelValue(final String arg0) {
    return "Unable to find an expression language feature level for value " + arg0 + ".";
  }

  public String getUnknownMethodInExpressionLanguage(final String arg0) {
    return "EL expression '" + arg0 + "' references an unknown method.";
  }

  public String getDisabledFeatureInExpressionLanguage(final String arg0) {
    return "Unable to interpolate EL expression '" + arg0 + "' as it uses a disabled feature.";
  }

  public String getConstraintValidatorFactoryMustNotReturnNullException(final String arg0) {
    return "Constraint factory returned null when trying to create instance of " + arg0 + ".";
  }

  public String getWrongAnnotationAttributeTypeException(final String arg0, final String arg1,
      final String arg2, final String arg3) {
    return "Wrong type for attribute '" + arg1 + "' of annotation " + arg0 + ". Expected: " + arg2
        + ". Actual: " + arg3 + ".";
  }

  public String getUnableToFindAnnotationAttributeException(final String arg0, final String arg1) {
    return "The specified annotation " + arg0 + " defines no attribute '" + arg1 + "'.";
  }

  public String getUnableToGetAnnotationAttributeException(String arg0, String arg1) {
    return "Unable to get attribute '" + arg1 + "' from annotation " + arg0 + ".";
  }

  public String getNoValueProvidedForAnnotationAttributeException(String arg0,
      String arg1) {
    return "No value provided for attribute '" + arg0 + "' of annotation " + arg1 + ".";
  }

  public String getTryingToInstantiateAnnotationWithUnknownAttributesException(String arg0,
      String arg1) {
    return "Trying to instantiate annotation " + arg0 + " with unknown attribute(s): " + arg1 + ".";
  }

  public String getMultipleValidatorsForSameTypeException(String arg0, String arg1,
      String arg2, String arg3) {
    return "The constraint " + arg0 + " defines multiple validators for the type " + arg1 + ": "
      + arg2 + ", " + arg3 + ". Only one is allowed.";
  }

  public String getMissingActualTypeArgumentForTypeParameterException(String arg0) {
    return "Missing actual type argument for type parameter: " + arg0 + ".";
  }

  public String getMemberIsNeitherAFieldNorAMethodException(String arg0) {
    return "Member " + arg0 + " is neither a field nor a method.";
  }
}

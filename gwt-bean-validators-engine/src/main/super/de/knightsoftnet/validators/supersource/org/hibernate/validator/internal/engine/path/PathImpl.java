/*
 * Hibernate pathImpl.
 *
 * License: Apache License, Version 2.0 See the license.txt file in the root directory or
 * <http://www.apache.org/licenses/LICENSE-2.0>.
 */

package org.hibernate.validator.internal.engine.path;

import org.gwtproject.regexp.shared.MatchResult;
import org.gwtproject.regexp.shared.RegExp;

import org.hibernate.validator.internal.util.Contracts;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import jakarta.validation.ElementKind;
import jakarta.validation.Path;

/**
 * Default implementation of {@code jakarta.validation.Path}.
 *
 * @author Hardy Ferentschik
 * @author Gunnar Morling
 * @author Kevin Pollet &lt;kevin.pollet@serli.com&gt; (C) 2011 SERLI
 * @author Manfred Tremmel ported to gwt
 */
@SuppressWarnings("checkstyle:javadocmethod")
public class PathImpl implements Path, Serializable {
  private static final long serialVersionUID = 7564511574909882392L;

  private static final String PROPERTY_PATH_SEPARATOR = ".";
  private static final int[] A = new int[256];

  /**
   * Regular expression used to split a string path into its elements.
   *
   * @see <a href="http://www.regexplanet.com/simple/index.jsp">Regular expression tester</a>
   */
  private static final String LEADING_PROPERTY_GROUP = "[^\\[\\.]+"; // everything up to a [ or .
  private static final String OPTIONAL_INDEX_GROUP = "\\[(\\w*)\\]";
  private static final String REMAINING_PROPERTY_STRING = "\\.(.*)"; // processed recursively

  private static final RegExp PATH_PATTERN = RegExp.compile("(" + LEADING_PROPERTY_GROUP + ")("
      + OPTIONAL_INDEX_GROUP + ")?(" + REMAINING_PROPERTY_STRING + ")*");
  private static final int PROPERTY_NAME_GROUP = 1;
  private static final int INDEXED_GROUP = 2;
  private static final int INDEX_GROUP = 3;
  private static final int REMAINING_STRING_GROUP = 5;

  private List<Node> nodeList;
  private boolean nodeListRequiresCopy;
  private NodeImpl currentLeafNode;
  private int hashCodeEntry;

  /**
   * Returns a {@code Path} instance representing the path described by the given string. To create
   * a root node the empty string should be passed.
   *
   * @param propertyPath the path as string representation.
   *
   * @return a {@code Path} instance representing the path described by the given string.
   *
   * @throws IllegalArgumentException in case {@code property == null} or {@code property} cannot be
   *         parsed.
   */
  public static PathImpl createPathFromString(final String propertyPath) {
    if (propertyPath == null) {
      throw new IllegalArgumentException("parameter propertyPath must not be null");
    }

    if (propertyPath.length() == 0) {
      return createRootPath();
    }

    return parseProperty(propertyPath);
  }

  // public static PathImpl createPathForExecutable(final ExecutableMetaData executable) {
  // Contracts.assertNotNull(executable,
  // "A method is required to create a method return value path.");
  //
  // final PathImpl path = createRootPath();
  //
  // if (executable.getKind() == ElementKind.CONSTRUCTOR) {
  // path.addConstructorNode(executable.getName(), executable.getParameterTypes());
  // } else {
  // path.addMethodNode(executable.getName(), executable.getParameterTypes());
  // }
  //
  // return path;
  // }

  public static PathImpl createRootPath() {
    final PathImpl path = new PathImpl();
    path.addBeanNode();
    return path;
  }

  public static PathImpl createCopy(final PathImpl path) {
    return new PathImpl(path);
  }

  public static PathImpl createCopyWithoutLeafNode(final PathImpl path) {
    return new PathImpl(path.nodeList.subList(0, path.nodeList.size() - 1));
  }

  public boolean isRootPath() {
    return nodeList.size() == 1 && nodeList.get(0).getName() == null;
  }

  public NodeImpl addPropertyNode(final String nodeName) {
    requiresWriteableNodeList();

    final NodeImpl parent = currentLeafNode;
    currentLeafNode = NodeImpl.createPropertyNode(nodeName, parent); // NOPMD
    nodeList.add(currentLeafNode);
    resetHashCode();
    return currentLeafNode;
  }

  public NodeImpl addContainerElementNode(final String nodeName) {
    requiresWriteableNodeList();

    final NodeImpl parent = currentLeafNode;
    currentLeafNode = NodeImpl.createContainerElementNode(nodeName, parent); // NOPMD
    nodeList.add(currentLeafNode);
    resetHashCode();
    return currentLeafNode;
  }

  public NodeImpl addParameterNode(final String nodeName, final int index) {
    requiresWriteableNodeList();

    final NodeImpl parent = currentLeafNode;
    currentLeafNode = NodeImpl.createParameterNode(nodeName, parent, index); // NOPMD
    nodeList.add(currentLeafNode);
    resetHashCode();
    return currentLeafNode;
  }

  public NodeImpl addCrossParameterNode() {
    requiresWriteableNodeList();

    final NodeImpl parent = currentLeafNode;
    currentLeafNode = NodeImpl.createCrossParameterNode(parent); // NOPMD
    nodeList.add(currentLeafNode);
    resetHashCode();
    return currentLeafNode;
  }

  public NodeImpl addBeanNode() {
    requiresWriteableNodeList();

    final NodeImpl parent = currentLeafNode;
    currentLeafNode = NodeImpl.createBeanNode(parent); // NOPMD
    nodeList.add(currentLeafNode);
    resetHashCode();
    return currentLeafNode;
  }

  public NodeImpl addReturnValueNode() {
    requiresWriteableNodeList();

    final NodeImpl parent = currentLeafNode;
    currentLeafNode = NodeImpl.createReturnValue(parent); // NOPMD
    nodeList.add(currentLeafNode);
    resetHashCode();
    return currentLeafNode;
  }

  public NodeImpl makeLeafNodeIterable() {
    requiresWriteableNodeList();

    currentLeafNode = NodeImpl.makeIterable(currentLeafNode); // NOPMD

    nodeList.set(nodeList.size() - 1, currentLeafNode);
    resetHashCode();
    return currentLeafNode;
  }

  public NodeImpl makeLeafNodeIterableAndSetIndex(final Integer index) {
    requiresWriteableNodeList();

    currentLeafNode = NodeImpl.makeIterableAndSetIndex(currentLeafNode, index); // NOPMD

    nodeList.set(nodeList.size() - 1, currentLeafNode);
    resetHashCode();
    return currentLeafNode;
  }

  public NodeImpl makeLeafNodeIterableAndSetMapKey(final Object key) {
    requiresWriteableNodeList();

    currentLeafNode = NodeImpl.makeIterableAndSetMapKey(currentLeafNode, key); // NOPMD

    nodeList.set(nodeList.size() - 1, currentLeafNode);
    resetHashCode();
    return currentLeafNode;
  }

  public NodeImpl setLeafNodeValueIfRequired(final Object value) {
    // The value is only exposed for property and container element nodes
    if (currentLeafNode.getKind() == ElementKind.PROPERTY
        || currentLeafNode.getKind() == ElementKind.CONTAINER_ELEMENT) {
      requiresWriteableNodeList();

      currentLeafNode = NodeImpl.setPropertyValue(currentLeafNode, value); // NOPMD

      nodeList.set(nodeList.size() - 1, currentLeafNode);

      // the property value is not part of the NodeImpl hashCode so we don't need to reset the
      // PathImpl hashCode
    }
    return currentLeafNode;
  }

  public NodeImpl setLeafNodeTypeParameter(final Class<?> containerClass,
      final Integer typeArgumentIndex) {
    requiresWriteableNodeList();

    currentLeafNode = NodeImpl.setTypeParameter(currentLeafNode, containerClass, typeArgumentIndex); // NOPMD

    nodeList.set(nodeList.size() - 1, currentLeafNode);
    resetHashCode();
    return currentLeafNode;
  }

  public void removeLeafNode() {
    if (!nodeList.isEmpty()) {
      requiresWriteableNodeList();

      nodeList.remove(nodeList.size() - 1);
      currentLeafNode = nodeList.isEmpty() ? null : (NodeImpl) nodeList.get(nodeList.size() - 1);
      resetHashCode();
    }
  }

  public NodeImpl getLeafNode() {
    return currentLeafNode;
  }

  @Override
  public Iterator<Path.Node> iterator() {
    if (nodeList.size() == 0) {
      return Collections.<Path.Node>emptyList().iterator();
    }
    if (nodeList.size() == 1) {
      return nodeList.iterator();
    }
    return nodeList.subList(1, nodeList.size()).iterator();
  }

  /**
   * serialize as string.
   *
   * @return string value of the node
   */
  public String asString() {
    final StringBuilder builder = new StringBuilder();
    boolean first = true;
    for (int i = 1; i < nodeList.size(); i++) {
      final NodeImpl nodeImpl = (NodeImpl) nodeList.get(i);
      final String name = nodeImpl.asString();
      if (name.isEmpty()) {
        // skip the node if it does not contribute to the string representation of the path, eg
        // class level constraints
        continue;
      }

      if (!first) {
        builder.append(PROPERTY_PATH_SEPARATOR);
      }

      builder.append(nodeImpl.asString());

      first = false;
    }
    return builder.toString();
  }

  private void requiresWriteableNodeList() {
    if (!nodeListRequiresCopy) {
      return;
    }

    // Usually, the write operation is about adding one more node, so let's make the list one
    // element larger.
    final List<Node> newNodeList = new ArrayList<>(nodeList.size() + 1);
    newNodeList.addAll(nodeList);
    nodeList = newNodeList;
    nodeListRequiresCopy = false;
  }

  @Override
  public String toString() {
    return asString();
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (this.getClass() != obj.getClass()) {
      return false;
    }
    final PathImpl other = (PathImpl) obj;
    if (!Objects.equals(nodeList, other.nodeList)) {
      return false;
    }
    return true;
  }

  @Override
  // deferred hash code building
  public int hashCode() {
    if (hashCodeEntry == -1) {
      hashCodeEntry = buildHashCode();
    }

    return hashCodeEntry;
  }

  private int buildHashCode() {
    return Objects.hashCode(nodeList);
  }

  /**
   * Copy constructor.
   *
   * @param path the path to make a copy of.
   */
  private PathImpl(final PathImpl path) {
    this(path.nodeList);
    currentLeafNode = (NodeImpl) nodeList.get(nodeList.size() - 1);
  }

  protected PathImpl() {
    nodeList = new ArrayList<>(1);
    resetHashCode();
    nodeListRequiresCopy = false;
  }

  private PathImpl(final List<Node> nodeList) {
    this.nodeList = nodeList;
    currentLeafNode = (NodeImpl) nodeList.get(nodeList.size() - 1);
    resetHashCode();
    nodeListRequiresCopy = true;
  }

  private void resetHashCode() {
    hashCodeEntry = -1;
  }

  @SuppressWarnings("checkstyle:rightCurly")
  private static PathImpl parseProperty(final String propertyName) {
    final PathImpl path = createRootPath();
    String tmp = propertyName;
    do {
      final MatchResult matcher = PATH_PATTERN.exec(tmp);
      if (matcher == null) {
        throw new IllegalArgumentException("parameter propertyName is no valid path");
      } else {

        final String value = matcher.getGroup(PROPERTY_NAME_GROUP);
        if (!isValidJavaIdentifier(value)) {
          throw new IllegalArgumentException("parameter propertyName contains no valid name group");
        }

        // create the node
        path.addPropertyNode(value);

        // is the node indexable
        if (matcher.getGroup(INDEXED_GROUP) != null) {
          path.makeLeafNodeIterable();
        }

        // take care of the index/key if one exists
        final String indexOrKey = matcher.getGroup(INDEX_GROUP);
        if (indexOrKey != null && indexOrKey.length() > 0) {
          try {
            final Integer i = Integer.parseInt(indexOrKey);
            path.makeLeafNodeIterableAndSetIndex(i);
          } catch (final NumberFormatException e) {
            path.makeLeafNodeIterableAndSetMapKey(indexOrKey);
          }
        }

        // match the remaining string
        tmp = matcher.getGroup(REMAINING_STRING_GROUP);
      }
    } while (tmp != null);

    if (path.getLeafNode().isIterable()) {
      path.addBeanNode();
    }

    return path;
  }

  /**
   * Validate that the given identifier is a valid Java identifier according to the Java Language.
   * Specification,
   * <a href="http://docs.oracle.com/javase/specs/jls/se6/html/lexical.html#3.8">chapter 3.8</a>
   *
   * @param identifier string identifier to validate
   *
   * @return true if the given identifier is a valid Java Identifier
   *
   * @throws IllegalArgumentException if the given identifier is {@code null}
   */
  private static boolean isValidJavaIdentifier(final String identifier) {
    Contracts.assertNotNull(identifier, "identifier param cannot be null");

    if (identifier.length() == 0 || !isJavaIdentifierStart(identifier.charAt(0))) {
      return false;
    }

    for (int i = 1; i < identifier.length(); i++) {
      if (!isJavaIdentifierPart(identifier.charAt(i))) {
        return false;
      }
    }
    return true;
  }

  private static boolean isJavaIdentifierStart(final int ch) {
    final int prop = getProperties(ch);
    return (prop & 0x00007000) >= 0x00005000;
  }

  private static boolean isJavaIdentifierPart(final int ch) {
    final int prop = getProperties(ch);
    return (prop & 0x00003000) != 0;
  }

  private static int getProperties(final int var1) {
    final char var2 = (char) var1;
    return A[var2];
  }

  static {
    @SuppressWarnings("checkstyle:avoidEscapedUnicodeCharacters")
    final char[] var0 = ( //
    "䠀ဏ䠀ဏ䠀ဏ䠀ဏ䠀ဏ䠀ဏ䠀ဏ䠀ဏ䠀ဏ堀䀏倀䀏堀䀏怀䀏倀䀏䠀ဏ䠀ဏ" //
        + "䠀ဏ䠀ဏ䠀ဏ䠀ဏ䠀ဏ䠀ဏ䠀ဏ䠀ဏ䠀ဏ䠀ဏ䠀ဏ䠀ဏ倀䀏倀䀏倀䀏堀䀏怀䀌栀" //
        + "\u0018栀\u0018⠀\u0018⠀怚⠀\u0018栀\u0018栀\u0018\ue800\u0015\ue800" //
        + "\u0016栀\u0018 \u0019㠀\u0018 \u0014㠀\u0018㠀\u0018᠀㘉᠀㘉᠀㘉᠀㘉᠀㘉" //
        + "᠀㘉᠀㘉᠀㘉᠀㘉᠀㘉㠀\u0018栀\u0018\ue800\u0019栀\u0019\ue800\u0019栀" //
        + "\u0018栀\u0018\u0082翡\u0082翡\u0082翡\u0082翡\u0082翡\u0082翡" //
        + "\u0082翡\u0082翡\u0082翡\u0082翡\u0082翡\u0082翡\u0082翡\u0082翡" //
        + "\u0082翡\u0082翡\u0082翡\u0082翡\u0082翡\u0082翡\u0082翡\u0082翡" //
        + "\u0082翡\u0082翡\u0082翡\u0082翡\ue800\u0015栀\u0018\ue800\u0016栀" //
        + "\u001b栀倗栀\u001b\u0081翢\u0081翢\u0081翢\u0081翢\u0081翢\u0081翢" //
        + "\u0081翢\u0081翢\u0081翢\u0081翢\u0081翢\u0081翢\u0081翢\u0081翢" //
        + "\u0081翢\u0081翢\u0081翢\u0081翢\u0081翢\u0081翢\u0081翢\u0081翢" //
        + "\u0081翢\u0081翢\u0081翢\u0081翢\ue800\u0015栀\u0019\ue800\u0016栀" //
        + "\u0019䠀ဏ䠀ဏ䠀ဏ䠀ဏ䠀ဏ䠀ဏ倀ဏ䠀ဏ䠀ဏ䠀ဏ䠀ဏ䠀ဏ䠀ဏ䠀ဏ䠀ဏ䠀" //
        + "ဏ䠀ဏ䠀ဏ䠀ဏ䠀ဏ䠀ဏ䠀ဏ䠀ဏ䠀ဏ䠀ဏ䠀ဏ䠀ဏ䠀ဏ䠀ဏ䠀ဏ䠀ဏ䠀ဏ䠀ဏ㠀" //
        + "\f栀\u0018⠀怚⠀怚⠀怚⠀怚栀\u001c栀\u0018栀\u001b栀\u001c\u0000瀅\ue800" //
        + "\u001d栀\u0019䠀တ栀\u001c栀\u001b⠀\u001c⠀\u0019᠀؋᠀؋栀\u001b\u07fd瀂栀" //
        + "\u0018栀\u0018栀\u001b᠀ԋ\u0000瀅\ue800\u001e栀ࠋ栀ࠋ栀ࠋ栀\u0018\u0082瀁" //
        + "\u0082瀁\u0082瀁\u0082瀁\u0082瀁\u0082瀁\u0082瀁\u0082瀁\u0082瀁\u0082瀁" //
        + "\u0082瀁\u0082瀁\u0082瀁\u0082瀁\u0082瀁\u0082瀁\u0082瀁\u0082瀁\u0082瀁" //
        + "\u0082瀁\u0082瀁\u0082瀁\u0082瀁栀\u0019\u0082瀁\u0082瀁\u0082瀁\u0082瀁" //
        + "\u0082瀁\u0082瀁\u0082瀁\u07fd瀂\u0081瀂\u0081瀂\u0081瀂\u0081瀂\u0081瀂" //
        + "\u0081瀂\u0081瀂\u0081瀂\u0081瀂\u0081瀂\u0081瀂\u0081瀂\u0081瀂\u0081瀂" //
        + "\u0081瀂\u0081瀂\u0081瀂\u0081瀂\u0081瀂\u0081瀂\u0081瀂\u0081瀂\u0081瀂栀" //
        + "\u0019\u0081瀂\u0081瀂\u0081瀂\u0081瀂\u0081瀂\u0081瀂\u0081瀂\u061d瀂").toCharArray();
    assert var0.length == 512;

    int var1 = 0;

    int var3;
    for (int var2 = 0; var1 < 512; A[var2++] = var3 | var0[var1++]) {
      var3 = var0[var1++] << 16;
    }
  }
}

package de.knightsoftnet.validators.client;

import java.util.HashMap;
import java.util.Map;

public class GwtReflectorMap {

  private static final Map<Class<?>, Map<String, Object>> REFLECTOR_MAP = new HashMap<>();

  /**
   * put entry into map.
   *
   * @param clazz class to get data from
   * @param map field value map
   */
  public static void put(final Class<?> clazz, final Map<String, Object> map) {
    REFLECTOR_MAP.put(clazz, map);
  }

  /**
   * read entry from reflector map.
   *
   * @param clazz class to get data from
   * @param fieldName name of the field to get value from
   * @return object
   * @throws NoSuchMethodException if no getter exists for fieldName
   * @throws ReflectiveOperationException if class is not reflected
   */
  public static Object get(final Class<?> clazz, final String fieldName)
      throws NoSuchMethodException, ReflectiveOperationException {
    final Map<String, Object> map = REFLECTOR_MAP.get(clazz);
    if (map == null) {
      throw new ReflectiveOperationException("Class " + clazz.getName() + " is not reflected");
    }
    if (!map.containsKey(fieldName)) {
      throw new NoSuchMethodException(clazz.getName() + " has no getter for porperty " + fieldName);
    }
    return map.get(fieldName);
  }
}

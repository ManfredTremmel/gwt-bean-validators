package de.knightsoftnet.validators.client.validation;

import de.knightsoftnet.validators.client.impl.AbstractGwtSpecificValidator;
import de.knightsoftnet.validators.client.impl.AbstractGwtValidator;
import de.knightsoftnet.validators.client.impl.metadata.ValidationGroupsMetadata;

import java.util.Collections;

public class EmptyGwtValidatorImpl extends AbstractGwtValidator
    implements EmptyValidatorFactory.GwtValidator {

  public EmptyGwtValidatorImpl() {
    super(Collections.emptyMap(), createValidationGroupsMetadata());
  }

  private static ValidationGroupsMetadata createValidationGroupsMetadata() {
    return ValidationGroupsMetadata.builder().addGroup(jakarta.validation.groups.Default.class)
        .build();
  }

  @Override
  protected <T> AbstractGwtSpecificValidator<T> getValidatorForClass(final Class<T> clazz,
      final Object object) throws IllegalArgumentException {
    throw new IllegalArgumentException("ValidatorFactory.GwtValidator can not validate "
        + clazz.getName() + ". " + "To use this function, define your own ValidatorFactory.");
  }

  @Override
  protected <T> AbstractGwtSpecificValidator<T> getValidatorForInstanceClass(final Object object) {
    return null;
  }
}

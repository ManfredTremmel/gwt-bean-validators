package de.knightsoftnet.validators.client.spi;

import de.knightsoftnet.validators.client.AbstractGwtValidatorFactory;
import de.knightsoftnet.validators.client.validation.EmptyValidatorFactory;

public interface ValidatorFactoryInterface {
  AbstractGwtValidatorFactory INSTANCE = new EmptyValidatorFactory();
}

/*
 * Copyright 2010 Google Inc. Copyright 2016 Manfred Tremmel
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.client;

import de.knightsoftnet.validators.client.spi.GwtValidationProvider;

import java.util.List;

import jakarta.validation.ValidationProviderResolver;
import jakarta.validation.spi.ValidationProvider;

/**
 * The default GWT {@link ValidationProviderResolver}. This always returns the single default
 * ValidationProvider.
 */
public final class GwtValidationProviderResolver implements ValidationProviderResolver {

  private static final GwtValidationProvider validationProvider = new GwtValidationProvider();
  private static final List<ValidationProvider<?>> DEFAULT_LIST = List.of(validationProvider);

  @Override
  public List<ValidationProvider<?>> getValidationProviders() {
    return DEFAULT_LIST;
  }

  public void setValidatorFactory(final AbstractGwtValidatorFactory validatorFactory) {
    validationProvider.setValidatorFactory(validatorFactory);
  }
}

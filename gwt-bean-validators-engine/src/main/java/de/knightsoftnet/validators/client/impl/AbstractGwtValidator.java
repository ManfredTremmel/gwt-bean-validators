/*
 * Copyright 2010 Google Inc. Copyright 2016 Manfred Tremmel
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.client.impl;

import de.knightsoftnet.validators.client.impl.metadata.ValidationGroupsMetadata;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import jakarta.validation.ConstraintValidatorFactory;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.MessageInterpolator;
import jakarta.validation.ParameterNameProvider;
import jakarta.validation.TraversableResolver;
import jakarta.validation.ValidationException;
import jakarta.validation.Validator;
import jakarta.validation.executable.ExecutableValidator;
import jakarta.validation.metadata.BeanDescriptor;

/**
 * Base methods for implementing {@link Validator} in GWT.
 * <p>
 * All methods that do not need to be generated go here.
 * </p>
 */
public abstract class AbstractGwtValidator implements Validator {

  private final Set<Class<?>> validGroups;
  private final ValidationGroupsMetadata validationGroupsMetadata;
  private ConstraintValidatorFactory contraintValidatorFactory;
  private MessageInterpolator messageInterpolator;
  private TraversableResolver traversableResolver;
  private ParameterNameProvider parameterNameProvider;

  private final Map<Class<?>, AbstractGwtSpecificValidator<?>> classToValidatorMap;

  /**
   * Creates a validator initialized with the default group inheritance map.
   *
   * @see #AbstractGwtValidator(ValidationGroupsMetadata)
   */
  protected AbstractGwtValidator() {
    this(ValidationGroupsMetadata.builder().build());
  }

  /**
   * constructor.
   *
   * @param validationGroupsMetadata Validation group metadata.
   */
  protected AbstractGwtValidator(final ValidationGroupsMetadata validationGroupsMetadata) {
    this(Collections.emptyMap(), validationGroupsMetadata);
  }

  /**
   * constructor.
   *
   * @param classToValidatorMap map with class and matching validators
   * @param validationGroupsMetadata Validation group metadata.
   */
  protected AbstractGwtValidator(
      final Map<Class<?>, AbstractGwtSpecificValidator<?>> classToValidatorMap,
      final ValidationGroupsMetadata validationGroupsMetadata) {
    validGroups = validationGroupsMetadata.getAllGroupsAndSequences();
    this.validationGroupsMetadata = validationGroupsMetadata;
    this.classToValidatorMap = classToValidatorMap;
  }

  public ValidationGroupsMetadata getValidationGroupsMetadata() {
    return validationGroupsMetadata;
  }

  /**
   * initialize values.
   *
   * @param contraintValidatorFactory constraint validator factory to set
   * @param messageInterpolator message interpolator to set
   * @param traversableResolver traversable resolver to set
   * @param parameterNameProvider parameter resolver to set
   */
  public void init(final ConstraintValidatorFactory contraintValidatorFactory,
      final MessageInterpolator messageInterpolator, final TraversableResolver traversableResolver,
      final ParameterNameProvider parameterNameProvider) {
    this.contraintValidatorFactory = contraintValidatorFactory;
    this.messageInterpolator = messageInterpolator;
    this.traversableResolver = traversableResolver;
    this.parameterNameProvider = parameterNameProvider;
  }

  @Override
  public <T> T unwrap(final Class<T> type) {
    throw new ValidationException();
  }

  protected void checkGroups(final Class<?>... groups) {
    checkGroups(List.of(groups));
  }

  protected void checkGroups(final Collection<? extends Class<?>> groups) {
    if (!validGroups.containsAll(groups)) {
      final HashSet<Class<?>> unknown = new HashSet<>();
      unknown.addAll(groups);
      unknown.removeAll(validGroups);
      throw new IllegalArgumentException(getClass() + " only processes the following groups "
          + validGroups + ". " + "The following groups could not be processed " + unknown);
    }
  }

  protected void checkNotNull(final Object object, final String name)
      throws IllegalArgumentException {
    if (object == null) {
      throw new IllegalArgumentException(name + " can not be null.");
    }
  }

  protected ConstraintValidatorFactory getConstraintValidatorFactory() {
    return contraintValidatorFactory;
  }

  public MessageInterpolator getMessageInterpolator() {
    return messageInterpolator;
  }

  public TraversableResolver getTraversableResolver() {
    return traversableResolver;
  }

  protected ParameterNameProvider getParameterNameProvider() {
    return parameterNameProvider;
  }

  @Override
  public ExecutableValidator forExecutables() {
    // not implemented in GWT
    return null;
  }

  @SuppressWarnings("unchecked")
  @Override
  public <T> Set<ConstraintViolation<T>> validate(final T object, final Class<?>... groups) {
    checkNotNull(object, "object");
    checkNotNull(groups, "groups");
    checkGroups(groups);
    final AbstractGwtSpecificValidator<T> validatorImpl =
        getValidatorForClass((Class<T>) object.getClass(), object);
    final GwtValidationContext<T> context = new GwtValidationContext<>((Class<T>) object.getClass(),
        object, validatorImpl.getConstraints(getValidationGroupsMetadata()),
        getMessageInterpolator(), getTraversableResolver(), this);
    return validatorImpl.validate(context, object, groups);
  }

  /**
   * Validates all constraints on {@code object}.
   *
   * @param context gwt validaton context
   * @param object object to validate
   * @param groups the group or list of groups targeted for validation (defaults to
   *        {@link jakarta.validation.groups.Default})
   * @param <T> the type of the object to validate
   * @return constraint violations or an empty set if none
   * @throws IllegalArgumentException if object is {@code null} or if {@code null} is passed to the
   *         varargs groups
   * @throws ValidationException if a non recoverable error happens during the validation process
   */
  @SuppressWarnings("unchecked")
  public <T> Set<ConstraintViolation<T>> validate(final GwtValidationContext<T> context,
      final Object object, final Class<?>... groups) {
    checkNotNull(context, "context");
    checkNotNull(object, "object");
    checkNotNull(groups, "groups");
    checkGroups(groups);
    return getValidatorForClass((Class<T>) object.getClass(), object).validate(context, (T) object,
        groups);
  }

  @SuppressWarnings("unchecked")
  @Override
  public <T> Set<ConstraintViolation<T>> validateProperty(final T object, final String propertyName,
      final Class<?>... groups) {
    checkNotNull(object, "object");
    checkNotNull(propertyName, "propertyName");
    checkNotNull(groups, "groups");
    checkGroups(groups);
    final AbstractGwtSpecificValidator<T> validatorImpl =
        getValidatorForClass((Class<T>) object.getClass(), object);
    final GwtValidationContext<T> context = new GwtValidationContext<>((Class<T>) object.getClass(),
        object, validatorImpl.getConstraints(getValidationGroupsMetadata()),
        getMessageInterpolator(), getTraversableResolver(), this);
    return validatorImpl.validateProperty(context, object, propertyName, groups);
  }

  @Override
  public <T> Set<ConstraintViolation<T>> validateValue(final Class<T> beanType,
      final String propertyName, final Object value, final Class<?>... groups) {
    checkNotNull(beanType, "beanType");
    checkNotNull(propertyName, "propertyName");
    checkNotNull(groups, "groups");
    checkGroups(groups);
    final AbstractGwtSpecificValidator<T> validatorImpl = getValidatorForClass(beanType, value);
    final GwtValidationContext<T> context = new GwtValidationContext<>(beanType, null,
        validatorImpl.getConstraints(getValidationGroupsMetadata()), getMessageInterpolator(),
        getTraversableResolver(), this);
    return validatorImpl.validateValue(context, beanType, propertyName, value, groups);
  }

  @Override
  public BeanDescriptor getConstraintsForClass(final Class<?> clazz) {
    checkNotNull(clazz, "clazz");
    return getValidatorForClass(clazz, null).getConstraints(getValidationGroupsMetadata());
  }

  @SuppressWarnings("unchecked")
  protected <T> AbstractGwtSpecificValidator<T> getValidatorForClass(final Class<T> clazz,
      final Object object) throws IllegalArgumentException {
    if (classToValidatorMap.containsKey(clazz)) {
      return (AbstractGwtSpecificValidator<T>) classToValidatorMap.get(clazz);
    }
    if (object != null) {
      final AbstractGwtSpecificValidator<T> instanceofValidator =
          getValidatorForInstanceClass(object);
      if (instanceofValidator != null) {
        return instanceofValidator;
      }
    }
    throw new IllegalArgumentException("ValidatorFactory.GwtValidator can not validate "
        + clazz.getName() + ". Valid types are [" + classToValidatorMap.keySet().stream()
            .map(Class::getName).collect(Collectors.joining(", "))
        + "]");
  }

  protected abstract <T> AbstractGwtSpecificValidator<T> getValidatorForInstanceClass(
      final Object object);
}

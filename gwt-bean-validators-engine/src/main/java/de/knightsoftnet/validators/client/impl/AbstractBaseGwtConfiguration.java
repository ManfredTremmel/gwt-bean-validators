/*
 * Copyright 2010 Google Inc. Copyright 2016 Manfred Tremmel
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.client.impl;

import de.knightsoftnet.validators.client.AbstractGwtValidatorFactory;
import de.knightsoftnet.validators.client.DefaultTraversableResolver;
import de.knightsoftnet.validators.client.GwtConstraintValidatorFactory;
import de.knightsoftnet.validators.client.GwtMessageInterpolator;
import de.knightsoftnet.validators.client.spi.GwtConfigurationState;
import de.knightsoftnet.validators.client.spi.GwtValidationProvider;

import java.util.HashMap;
import java.util.Map;

import jakarta.validation.Configuration;
import jakarta.validation.ConstraintValidatorFactory;
import jakarta.validation.MessageInterpolator;
import jakarta.validation.TraversableResolver;
import jakarta.validation.ValidatorFactory;
import jakarta.validation.spi.BootstrapState;
import jakarta.validation.spi.ValidationProvider;

/**
 * Base GWT {@link Configuration}.
 */
public abstract class AbstractBaseGwtConfiguration
    implements Configuration<AbstractBaseGwtConfiguration> {

  protected final ValidationProvider<AbstractBaseGwtConfiguration> provider;
  protected final BootstrapState state;
  protected final Map<String, String> properties = new HashMap<>();
  protected ConstraintValidatorFactory constraintValidatorFactoryProperty;
  protected MessageInterpolator messageInterpolatorProperty;
  protected TraversableResolver traversableResolverProperty;

  protected AbstractBaseGwtConfiguration(
      final ValidationProvider<AbstractBaseGwtConfiguration> gwtValidationProvider,
      final BootstrapState state) {
    provider = gwtValidationProvider;
    this.state = state;
  }

  /**
   * set validator factory at given validation provider.
   *
   * @param validatorFactory validator factory to set
   * @return abstract base gwt validator factory
   */
  public final AbstractBaseGwtConfiguration gwtFactory(
      final AbstractGwtValidatorFactory validatorFactory) {
    if (provider instanceof final GwtValidationProvider gwtValidationProvider) {
      gwtValidationProvider.setValidatorFactory(validatorFactory);
    }
    return this;
  }

  @Override
  public final AbstractBaseGwtConfiguration addProperty(final String name, final String value) {
    properties.put(name, value);
    return this;
  }

  @Override
  public final ValidatorFactory buildValidatorFactory() {
    final GwtConfigurationState configurationState =
        new GwtConfigurationState(constraintValidatorFactoryProperty, //
            messageInterpolatorProperty, //
            properties, //
            traversableResolverProperty);
    return provider.buildValidatorFactory(configurationState);
  }

  @Override
  public final AbstractBaseGwtConfiguration constraintValidatorFactory(
      final ConstraintValidatorFactory constraintValidatorFactory) {
    constraintValidatorFactoryProperty = constraintValidatorFactory;
    return this;
  }

  @Override
  public final ConstraintValidatorFactory getDefaultConstraintValidatorFactory() {
    return new GwtConstraintValidatorFactory();
  }

  @Override
  public final MessageInterpolator getDefaultMessageInterpolator() {
    return new GwtMessageInterpolator();
  }

  @Override
  public final TraversableResolver getDefaultTraversableResolver() {
    return new DefaultTraversableResolver();
  }

  @Override
  public final AbstractBaseGwtConfiguration ignoreXmlConfiguration() {
    // Always ignore XML anyway
    return this;
  }

  @Override
  public final AbstractBaseGwtConfiguration messageInterpolator(
      final MessageInterpolator interpolator) {
    messageInterpolatorProperty = interpolator;
    return this;
  }

  @Override
  public final AbstractBaseGwtConfiguration traversableResolver(
      final TraversableResolver resolver) {
    traversableResolverProperty = resolver;
    return this;
  }
}

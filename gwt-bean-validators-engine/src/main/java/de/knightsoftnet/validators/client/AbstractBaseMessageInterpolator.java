/*
 * Copyright 2010 Google Inc. Copyright 2016 Manfred Tremmel
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.client;

import org.gwtproject.regexp.shared.MatchResult;
import org.gwtproject.regexp.shared.RegExp;

import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import jakarta.validation.MessageInterpolator;

/**
 * Base GWT {@link MessageInterpolator}.
 */
abstract class AbstractBaseMessageInterpolator implements MessageInterpolator {

  // local version because guava is not included.
  private interface Function<F, T> {
    T apply(F from);
  }

  private static final RegExp MESSAGE_PARAMETER_PATTERN =
      RegExp.compile("(<{0,1}\\{[^\\}]+?\\}( digits>){0,1})", "g");
  private static final RegExp MESSAGE_CONDITION_PATTERN =
      RegExp.compile("(\\$\\{[^\\} ]+ *[=<>]+ *[^\\} ]+ *\\? *'[^']*' *: *'[^']*'\\})", "g");
  private static final RegExp MESSAGE_CONDITION_SPLIT_PATTERN = RegExp
      .compile("\\$\\{([^\\} ]+) *([=<>]+) *([^\\} ]+) *\\? *'([^']*)' *: *'([^']*)'\\}", "g");
  private static final RegExp MESSAGE_DURATION_PATTERN = RegExp.compile(
      "\\$\\{[^\\} ]+ *== *0 *\\? *'[^']*' *: " + "*([^\\} ]+ *== *1 *\\? *'[^']*' *:){0,1} "
          + "*'[^']*' *\\+= *[^\\} ]+ *\\+= *'[^']*'\\}",
      "g");
  private static final RegExp MESSAGE_DURATION_SPLIT_PATTERN =
      RegExp.compile("\\$\\{([^\\} ]+) *== *0 *\\? *'([^']*)' *: "
          + "*([^\\} ]+ *== *1 *\\? *'([^']*)' *:){0,1} *'([^']*)' *\\+= *"
          + "[^\\} ]+ *\\+= *'([^']*)'\\}", "g");

  // Visible for testing
  static Function<String, String> createAnnotationReplacer(final Map<String, Object> map) {
    return from -> {
      final Object object = map.get(from);
      return object == null ? null : object.toString();
    };
  }

  private static Function<String, String> createReplacer(
      final ValidationMessageResolver messageResolver) {
    return from -> {
      final Object object = messageResolver.get(from);
      return object == null ? null : object.toString();
    };
  }

  /**
   * Replaces keys using the Default Validation Provider properties.
   */
  private final Function<String, String> providerReplacer =
      createReplacer(new HibernateValidationMessageResolver());

  /**
   * Replaces keys using the Validation User custom properties.
   */
  private final Function<String, String> userReplacer;

  protected AbstractBaseMessageInterpolator(
      final UserValidationMessagesResolver userValidationMessagesResolver) {
    userReplacer = createReplacer(userValidationMessagesResolver);
  }

  @Override
  public final String interpolate(final String messageTemplate, final Context context) {
    return gwtInterpolate(messageTemplate, context);
  }

  @Override
  public String interpolate(final String messageTemplate, final Context context,
      final Locale locale) {
    // The super sourced GWT version of this calls gwtInterpolate
    return messageTemplate;
  }

  @SuppressWarnings("checkstyle:rightCurly")
  protected final String gwtInterpolate(final String message, final Context context) {
    // see Section 4.3.1.1
    String resolvedMessage = message;
    String step1message;

    // TODO(nchalko) Add a safety to make sure this does not loop forever.
    do {
      do {
        step1message = resolvedMessage;

        // Step 1 Replace message parameters using custom user messages
        // repeat
        resolvedMessage = replaceParameters(resolvedMessage, userReplacer);
      } while (!step1message.equals(resolvedMessage));

      // Step2 Replace message parameter using the default provider messages.
      resolvedMessage = replaceParameters(resolvedMessage, providerReplacer);

      // Step 3 repeat from step 1 if step 2 made changes.
    } while (!step1message.equals(resolvedMessage));

    // step 4 resolve annotation attributes
    resolvedMessage = replaceParameters(resolvedMessage,
        createAnnotationReplacer(context.getConstraintDescriptor().getAttributes()));

    // step 5 resolve value
    resolvedMessage =
        resolvedMessage.replace("${validatedValue}", Objects.toString(context.getValidatedValue()));

    // step 6 resolve conditions
    resolvedMessage =
        replaceConditions(resolvedMessage, context.getConstraintDescriptor().getAttributes());

    // step 7 resolve duration conditions
    resolvedMessage = replaceDurationConditions(resolvedMessage,
        context.getConstraintDescriptor().getAttributes());

    // Remove escapes (4.3.1)
    resolvedMessage = resolvedMessage.replace("\\{", "{");
    resolvedMessage = resolvedMessage.replace("\\}", "}");
    resolvedMessage = resolvedMessage.replace("\\\\", "\\");
    return resolvedMessage;
  }

  protected final String replaceParameters(final String message,
      final Function<String, String> replacer) {
    final StringBuilder sb = new StringBuilder();
    int index = 0;

    MatchResult matcher = MESSAGE_PARAMETER_PATTERN.exec(message);
    while (matcher != null) {
      final String matched = matcher.getGroup(0);
      sb.append(message.substring(index, matcher.getIndex()));
      final Object value = replacer.apply(removeCurlyBrace(matched));
      sb.append(value == null ? matched : value);
      index = MESSAGE_PARAMETER_PATTERN.getLastIndex();
      matcher = MESSAGE_PARAMETER_PATTERN.exec(message);
    }
    if (index < message.length()) {
      sb.append(message.substring(index));
    }
    return sb.toString();
  }

  protected final String replaceConditions(final String message, final Map<String, Object> map) {
    final StringBuilder sb = new StringBuilder();
    int index = 0;

    MatchResult matcher = MESSAGE_CONDITION_PATTERN.exec(message);
    while (matcher != null) {
      final String matched = matcher.getGroup(0);
      sb.append(message.substring(index, matcher.getIndex()));
      final Object value = replaceCondition(matched, map);
      sb.append(value == null ? matched : value);
      index = MESSAGE_CONDITION_PATTERN.getLastIndex();
      matcher = MESSAGE_CONDITION_PATTERN.exec(message);
    }
    if (index < message.length()) {
      sb.append(message.substring(index));
    }
    return sb.toString();
  }

  protected final String replaceCondition(final String message, final Map<String, Object> map) {
    String result = message;
    MatchResult matcher = MESSAGE_CONDITION_SPLIT_PATTERN.exec(message);
    while (matcher != null && matcher.getGroupCount() == 6) {
      final String field = matcher.getGroup(1);
      if (map.containsKey(field)) {
        final String condition = matcher.getGroup(2);
        final String compareValue = matcher.getGroup(3);
        final String trueValue = matcher.getGroup(4);
        final String falseValue = matcher.getGroup(5);
        final Object fieldValue = map.get(field);
        final int compareResult;
        if (fieldValue instanceof final Boolean fieldBoolean) {
          compareResult = compareObjects(fieldBoolean, Boolean.valueOf(compareValue));
        } else if (fieldValue instanceof final Number fieldNumber) {
          compareResult =
              compareObjects(fieldNumber.doubleValue(), Double.parseDouble(compareValue));
        } else {
          compareResult = compareObjects(Objects.toString(fieldValue, ""), compareValue);
        }
        final boolean matches = switch (condition) {
          case "==" -> compareResult == 0;
          case "!=" -> compareResult != 0;
          case ">=" -> compareResult >= 0;
          case ">" -> compareResult > 0;
          case "<" -> compareResult < 0;
          case "<=" -> compareResult <= 0;
          default -> false;
        };
        result = matches ? trueValue : falseValue;
      }
      matcher = MESSAGE_CONDITION_SPLIT_PATTERN.exec("");
    }
    return result;
  }

  private <T extends Comparable<? super T>> int compareObjects(final T c1, final T c2) {
    if (c1 == c2) {
      return 0;
    } else if (c1 == null) {
      return -1;
    } else if (c2 == null) {
      return 1;
    }
    return c1.compareTo(c2);
  }

  protected final String replaceDurationConditions(final String message,
      final Map<String, Object> map) {
    final StringBuilder sb = new StringBuilder();
    int index = 0;

    MatchResult matcher = MESSAGE_DURATION_PATTERN.exec(message);
    while (matcher != null) {
      final String matched = matcher.getGroup(0);
      sb.append(message.substring(index, matcher.getIndex()));
      final Object value = replaceDurationCondition(matched, map);
      sb.append(value == null ? matched : value);
      index = MESSAGE_DURATION_PATTERN.getLastIndex();
      matcher = MESSAGE_DURATION_PATTERN.exec(message);
    }
    if (index < message.length()) {
      sb.append(message.substring(index));
    }
    return sb.toString();
  }

  protected final String replaceDurationCondition(final String message,
      final Map<String, Object> map) {
    String result = message;
    MatchResult matcher = MESSAGE_DURATION_SPLIT_PATTERN.exec(message);
    while (matcher != null && matcher.getGroupCount() == 7) {
      final String field = matcher.getGroup(1);
      if (map.containsKey(field)) {
        final Object fieldValue = map.get(field);
        int fieldNumber;
        if (fieldValue instanceof final Number fieldValueNumber) {
          fieldNumber = fieldValueNumber.intValue();
        } else {
          fieldNumber = Integer.valueOf(Objects.toString(fieldValue, ""));
        }
        final String textEmpty = matcher.getGroup(2);
        final String textOne = matcher.getGroup(4);
        final String textManyBefore = matcher.getGroup(5);
        final String textManyAfter = matcher.getGroup(6);
        result = fieldNumber == 0 ? textEmpty
            : fieldNumber == 1 && textOne != null ? textOne
                : textManyBefore + fieldNumber + textManyAfter;
      }
      matcher = MESSAGE_DURATION_SPLIT_PATTERN.exec("");
    }
    return result;
  }

  private String removeCurlyBrace(final String parameter) {
    return parameter.replaceFirst("<{0,1}\\{", "").replaceAll("\\}( digits>){0,1}", "");
  }
}

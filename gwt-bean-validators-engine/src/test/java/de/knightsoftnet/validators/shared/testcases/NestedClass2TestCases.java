/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.testcases;

import de.knightsoftnet.validators.shared.beans.NestedClass2TestBean;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * get test cases for nested class validation.
 *
 * @author Manfred Tremmel
 *
 */
public class NestedClass2TestCases {
  /**
   * get empty test bean.
   *
   * @return empty test bean
   */
  public static final NestedClass2TestBean getEmptyTestBean() {
    return new NestedClass2TestBean(null);
  }

  /**
   * get correct test beans.
   *
   * @return correct test beans
   */
  public static final List<NestedClass2TestBean> getCorrectTestBeans() {
    final List<NestedClass2TestBean> correctCases = new ArrayList<>();
    correctCases.add(new NestedClass2TestBean(Collections.emptyList()));
    correctCases
        .add(new NestedClass2TestBean(List.of(new NestedClass2TestBean.NestedClass2("test"))));
    correctCases.add(new NestedClass2TestBean(List.of(new NestedClass2TestBean.NestedClass2("one"),
        new NestedClass2TestBean.NestedClass2("two"))));

    return correctCases;
  }

  /**
   * get wrong test beans.
   *
   * @return wrong test beans
   */
  public static final List<NestedClass2TestBean> getWrongTestBeans() {
    final List<NestedClass2TestBean> wrongCases = new ArrayList<>();
    wrongCases.add(new NestedClass2TestBean(List.of(new NestedClass2TestBean.NestedClass2(null))));
    wrongCases.add(new NestedClass2TestBean(List.of(new NestedClass2TestBean.NestedClass2(""))));

    return wrongCases;
  }
}

package de.knightsoftnet.validators.shared.beans;

/**
 * enum for bug 20 test.
 */
public enum Bug20TestEnum {
  EQUALS("="),
  NOT_EQUALS("!="),
  GREATER_THAN(">"),
  GREATER_THAN_OR_EQUAL_TO(">="),
  LESS_THAN("<"),
  LESS_THAN_OR_EQUAL_TO("<=");
  
  private String display;

  private Bug20TestEnum(String display) {
    this.display = display;
  }
 
  @Override
  public String toString() {
    return display;
  }
}

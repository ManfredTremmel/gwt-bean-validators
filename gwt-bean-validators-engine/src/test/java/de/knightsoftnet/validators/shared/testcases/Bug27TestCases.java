/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.testcases;

import de.knightsoftnet.validators.shared.beans.Bug27TestBeanA;
import de.knightsoftnet.validators.shared.beans.Bug27TestBeanB;
import de.knightsoftnet.validators.shared.beans.Bug27TestBeanD;

import java.util.ArrayList;
import java.util.List;

/**
 * get test cases for hibernate not empty test.
 *
 * @author Manfred Tremmel
 *
 */
public class Bug27TestCases {

  /**
   * get correct test beans.
   *
   * @return correct test beans
   */
  public static final List<Bug27TestBeanA> getCorrectTestBeans() {
    final List<Bug27TestBeanA> correctCases = new ArrayList<Bug27TestBeanA>();
    correctCases
        .add(new Bug27TestBeanA(List.of(new Bug27TestBeanB<Bug27TestBeanD>(new Bug27TestBeanD()))));

    return correctCases;
  }

  /**
   * get wrong test beans.
   *
   * @return wrong test beans
   */
  public static final List<Bug27TestBeanA> getWrongEmptyBeans() {
    final List<Bug27TestBeanA> wrongCases = new ArrayList<Bug27TestBeanA>();
    wrongCases.add(new Bug27TestBeanA(List.of(new Bug27TestBeanB<Bug27TestBeanD>(null))));

    return wrongCases;
  }
}

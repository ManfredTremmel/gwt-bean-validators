package de.knightsoftnet.validators.shared.beans;

import jakarta.validation.constraints.NotNull;

public class Bug27TestBeanB<T extends Bug27TestBeanC> {

  @NotNull
  private T classC;

  public Bug27TestBeanB(@NotNull T classC) {
    super();
    this.classC = classC;
  }

  public T getClassC() {
    return classC;
  }
}

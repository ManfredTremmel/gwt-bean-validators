/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.beans;

import java.util.List;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotEmpty;

public class NestedClass2TestBean {

  public static class NestedClass2 {
    @NotEmpty
    private final String value;

    public NestedClass2(final String value) {
      super();
      this.value = value;
    }

    public final String getValue() {
      return value;
    }

    @Override
    public String toString() {
      return "NestedClass2 [value=" + value + "]";
    }
  }

  @Valid
  private final List<NestedClass2> nestedList;

  public NestedClass2TestBean(final List<NestedClass2> nestedList) {
    super();
    this.nestedList = nestedList;
  }

  public List<NestedClass2> getNestedList() {
    return nestedList;
  }

  @Override
  public String toString() {
    return "NestedClass2TestBean [nestedList=" + nestedList + "]";
  }
}

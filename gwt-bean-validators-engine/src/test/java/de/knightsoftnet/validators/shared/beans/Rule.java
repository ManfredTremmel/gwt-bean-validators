package de.knightsoftnet.validators.shared.beans;

/**
 * validation groups.
 */
public interface Rule {

  /**
   * all group.
   */
  public interface All {
    
  }

  /**
   * conditions group.
   */
  public interface Conditions {
    
  }

  /**
   * conditions group.
   */
  public interface Other {
    
  }
}

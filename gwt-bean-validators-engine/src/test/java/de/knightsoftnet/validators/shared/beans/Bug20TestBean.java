package de.knightsoftnet.validators.shared.beans;

import jakarta.validation.Valid;

/**
 * bug 20 test bean for group handling.
 */
public class Bug20TestBean {

  @Valid
  private final Bug20TestCompareBean value;

  public Bug20TestBean(Bug20TestEnum value) {
    super();
    this.value = new Bug20TestCompareBean(value);
  }

  public Bug20TestCompareBean getValue() {
    return value;
  }

  @Override
  public String toString() {
    return "Bug20TestBean [value=" + value + "]";
  }
}

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.testcases;

import de.knightsoftnet.validators.shared.beans.HibernateUuidTestBean;

import java.util.ArrayList;
import java.util.List;

/**
 * get test cases for hibernate Uuid test.
 *
 * @author Manfred Tremmel
 *
 */
public class HibernateUuidTestCases {
  /**
   * get uuid test bean.
   *
   * @return empty test bean
   */
  public static final HibernateUuidTestBean getEmptyTestBean() {
    return new HibernateUuidTestBean(null);
  }

  /**
   * get correct test beans.
   *
   * @return correct test beans
   */
  public static final List<HibernateUuidTestBean> getCorrectTestBeans() {
    final List<HibernateUuidTestBean> correctCases = new ArrayList<HibernateUuidTestBean>();
    correctCases.add(new HibernateUuidTestBean("62f07feb-7c36-4b2a-a832-cfd9b4b990c1"));
    correctCases.add(new HibernateUuidTestBean("62F07FEB-7C36-4B2A-A832-CFD9B4B990C1"));
    correctCases.add(new HibernateUuidTestBean("62F07FEB-7C36-4B2A-A832-CFD9B4B990c1"));
    correctCases.add(new HibernateUuidTestBean("2d5614ff-891e-47a8-b49e-d758506a9bab"));

    return correctCases;
  }

  /**
   * get wrong test beans.
   *
   * @return wrong test beans
   */
  public static final List<HibernateUuidTestBean> getWrongtoSmallTestBeans() {
    final List<HibernateUuidTestBean> wrongCases = new ArrayList<HibernateUuidTestBean>();
    wrongCases.add(new HibernateUuidTestBean("3069d388-d27a-427f-ab12-5144ae9e79f"));
    wrongCases.add(new HibernateUuidTestBean("411d9f38-21fc-4b04-aa28-8fb8b8f99565a"));
    wrongCases.add(new HibernateUuidTestBean("1e7fe6b0d-d28-48d9-bec2-7524574ed499"));
    wrongCases.add(new HibernateUuidTestBean("1e7fe6b0-dd284-8d9-bec2-7524574ed499"));
    wrongCases.add(new HibernateUuidTestBean("1e7fe6b0-dd28-48d9b-ec2-7524574ed499"));
    wrongCases.add(new HibernateUuidTestBean("1e7fe6b0-dd28-48d9-bec27-524574ed499"));
    wrongCases.add(new HibernateUuidTestBean("f6fab973-5c7c-4feb-a38f-b79df6d6d04x"));

    return wrongCases;
  }
}

package de.knightsoftnet.validators.shared.validators;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class Bug17bValidationValidator implements ConstraintValidator<Bug17bValidation, Object> {

  @Override
  public void initialize(final Bug17bValidation constraintAnnotation) {}

  @Override
  public boolean isValid(final Object value, final ConstraintValidatorContext context) {
    return false;
  }
}

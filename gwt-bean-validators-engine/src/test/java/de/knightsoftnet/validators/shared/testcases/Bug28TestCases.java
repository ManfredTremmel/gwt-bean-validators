/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.testcases;

import de.knightsoftnet.validators.shared.beans.Bug28TestBean;

import java.util.ArrayList;
import java.util.List;

/**
 * get test cases for hibernate min max test.
 *
 * @author Manfred Tremmel
 *
 */
public class Bug28TestCases {

  /**
   * get correct test beans.
   *
   * @return correct test beans
   */
  public static final List<Bug28TestBean> getCorrectTestBeans() {
    final List<Bug28TestBean> correctCases = new ArrayList<Bug28TestBean>();
    correctCases.add(new Bug28TestBean(1));
    correctCases.add(new Bug28TestBean(2));
    correctCases.add(new Bug28TestBean(3));
    correctCases.add(new Bug28TestBean(4));
    correctCases.add(new Bug28TestBean(5));

    return correctCases;
  }

  /**
   * get wrong, to small test beans.
   *
   * @return wrong, to small test beans
   */
  public static final List<Bug28TestBean> getWrongtoSmallTestBeans() {
    final List<Bug28TestBean> wrongCases = new ArrayList<Bug28TestBean>();
    wrongCases.add(new Bug28TestBean(Integer.MIN_VALUE));
    wrongCases.add(new Bug28TestBean(-1));
    wrongCases.add(new Bug28TestBean(0));

    return wrongCases;
  }

  /**
   * get wrong, to big test beans.
   *
   * @return wrong, to big test beans
   */
  public static final List<Bug28TestBean> getWrongtoBigTestBeans() {
    final List<Bug28TestBean> wrongCases = new ArrayList<Bug28TestBean>();
    wrongCases.add(new Bug28TestBean(6));
    wrongCases.add(new Bug28TestBean(100));
    wrongCases.add(new Bug28TestBean(Integer.MAX_VALUE));

    return wrongCases;
  }
}

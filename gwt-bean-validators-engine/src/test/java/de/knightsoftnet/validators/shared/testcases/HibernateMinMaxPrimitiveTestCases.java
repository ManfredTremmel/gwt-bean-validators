/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.testcases;

import de.knightsoftnet.validators.shared.beans.HibernateMinMaxPrimitiveTestBean;

import java.util.ArrayList;
import java.util.List;

/**
 * get test cases for hibernate min max primitive test.
 *
 * @author Manfred Tremmel
 *
 */
public class HibernateMinMaxPrimitiveTestCases {

  /**
   * get correct test beans.
   *
   * @return correct test beans
   */
  public static final List<HibernateMinMaxPrimitiveTestBean> getCorrectTestBeans() {
    final List<HibernateMinMaxPrimitiveTestBean> correctCases =
        new ArrayList<>();
    correctCases.add(new HibernateMinMaxPrimitiveTestBean(100L));
    correctCases.add(new HibernateMinMaxPrimitiveTestBean(5000L));
    correctCases.add(new HibernateMinMaxPrimitiveTestBean(99999L));
    correctCases.add(new HibernateMinMaxPrimitiveTestBean(500000L));

    return correctCases;
  }

  /**
   * get wrong, to small test beans.
   *
   * @return wrong, to small test beans
   */
  public static final List<HibernateMinMaxPrimitiveTestBean> getWrongtoSmallTestBeans() {
    final List<HibernateMinMaxPrimitiveTestBean> wrongCases =
        new ArrayList<>();
    wrongCases.add(new HibernateMinMaxPrimitiveTestBean(0L));
    wrongCases.add(new HibernateMinMaxPrimitiveTestBean(10L));
    wrongCases.add(new HibernateMinMaxPrimitiveTestBean(50L));
    wrongCases.add(new HibernateMinMaxPrimitiveTestBean(99L));

    return wrongCases;
  }

  /**
   * get wrong, to big test beans.
   *
   * @return wrong, to big test beans
   */
  public static final List<HibernateMinMaxPrimitiveTestBean> getWrongtoBigTestBeans() {
    final List<HibernateMinMaxPrimitiveTestBean> wrongCases =
        new ArrayList<>();
    wrongCases.add(new HibernateMinMaxPrimitiveTestBean(500001L));
    wrongCases.add(new HibernateMinMaxPrimitiveTestBean(1000000L));
    wrongCases.add(new HibernateMinMaxPrimitiveTestBean(5000000L));
    wrongCases.add(new HibernateMinMaxPrimitiveTestBean(990000000L));

    return wrongCases;
  }
}

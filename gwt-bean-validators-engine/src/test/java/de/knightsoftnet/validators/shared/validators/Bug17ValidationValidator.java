package de.knightsoftnet.validators.shared.validators;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class Bug17ValidationValidator implements ConstraintValidator<Bug17Validation, Object> {

  @Override
  public void initialize(final Bug17Validation constraintAnnotation) {}

  @Override
  public boolean isValid(final Object value, final ConstraintValidatorContext context) {
    return false;
  }
}

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.testcases;

import de.knightsoftnet.validators.shared.beans.HibernateNotEmptyBug11TestBean;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * get test cases for hibernate not empty test with property.
 *
 */
public class HibernateNotEmptyPropertyTestCases {
  /**
   * get empty test bean.
   *
   * @return empty test bean
   */
  public static final HibernateNotEmptyBug11TestBean getEmptyTestBean() {
    return new HibernateNotEmptyBug11TestBean(null, null);
  }

  /**
   * get correct test beans.
   *
   * @return correct test beans
   */
  public static final List<HibernateNotEmptyBug11TestBean> getCorrectTestBeans() {
    final List<HibernateNotEmptyBug11TestBean> correctCases = new ArrayList<>();
    correctCases
        .add(new HibernateNotEmptyBug11TestBean(" ", Stream.of(1).collect(Collectors.toList())));
    correctCases.add(
        new HibernateNotEmptyBug11TestBean("\t", Stream.of(1, 2).collect(Collectors.toList())));
    correctCases.add(
        new HibernateNotEmptyBug11TestBean("\n", Stream.of(0).collect(Collectors.toList())));
    correctCases
        .add(new HibernateNotEmptyBug11TestBean("abcde", Collections.singletonList(null)));

    return correctCases;
  }

  /**
   * get wrong test beans.
   *
   * @return wrong test beans
   */
  public static final List<HibernateNotEmptyBug11TestBean> getWrongtoSmallTestBeans() {
    final List<HibernateNotEmptyBug11TestBean> wrongCases = new ArrayList<>();
    wrongCases.add(new HibernateNotEmptyBug11TestBean(null, null));
    wrongCases.add(new HibernateNotEmptyBug11TestBean("", new ArrayList<>()));

    return wrongCases;
  }
}

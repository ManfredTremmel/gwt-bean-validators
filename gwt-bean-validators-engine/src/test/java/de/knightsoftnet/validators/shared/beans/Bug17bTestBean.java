package de.knightsoftnet.validators.shared.beans;

import de.knightsoftnet.validators.shared.validators.Bug17Validation;
import de.knightsoftnet.validators.shared.validators.Bug17bValidation;

import jakarta.validation.constraints.NotNull;

@Bug17Validation(Object.class)
@Bug17bValidation(Object.class)
public class Bug17bTestBean {

  @NotNull
  private String zzz;

  public String getZzz() {
    return zzz;
  }
}

package de.knightsoftnet.validators.shared.beans;

import jakarta.validation.constraints.NotNull;

public class Bug27BTestBeanE<T extends Bug27BInterfaceB> {
  @NotNull
  private Bug27BTestBeanA classA;

  public Bug27BTestBeanE(@NotNull Bug27BTestBeanA classA) {
    super();
    this.classA = classA;
  }

  public Bug27BTestBeanA getClassA() {
    return classA;
  }
}

package de.knightsoftnet.validators.shared.beans;

import jakarta.validation.constraints.NotNull;

public class Bug27BTestBeanF extends Bug27BTestBeanE<Bug27BTestBeanD> {

  public Bug27BTestBeanF(@NotNull Bug27BTestBeanA classA) {
    super(classA);
  }
}

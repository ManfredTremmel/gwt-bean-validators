/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.testcases;

import de.knightsoftnet.validators.shared.beans.Bug25TestBean;

import java.util.ArrayList;
import java.util.List;

/**
 * get test cases for hibernate not empty test.
 *
 * @author Manfred Tremmel
 *
 */
public class Bug25TestCases {
  /**
   * get empty test bean.
   *
   * @return empty test bean
   */
  public static final Bug25TestBean getEmptyTestBean() {
    return new Bug25TestBean(null);
  }

  /**
   * get correct test beans.
   *
   * @return correct test beans
   */
  public static final List<Bug25TestBean> getCorrectTestBeans() {
    final List<Bug25TestBean> correctCases = new ArrayList<Bug25TestBean>();
    correctCases.add(new Bug25TestBean(" "));
    correctCases.add(new Bug25TestBean("\t"));
    correctCases.add(new Bug25TestBean("\n"));
    correctCases.add(new Bug25TestBean("abcde"));

    return correctCases;
  }

  /**
   * get wrong test beans (empty).
   *
   * @return wrong test beans
   */
  public static final List<Bug25TestBean> getWrongEmptyBeans() {
    final List<Bug25TestBean> wrongCases = new ArrayList<Bug25TestBean>();
    wrongCases.add(new Bug25TestBean(null));
    wrongCases.add(new Bug25TestBean(""));

    return wrongCases;
  }

  /**
   * get wrong test beans (size).
   *
   * @return wrong test beans
   */
  public static final List<Bug25TestBean> getWrongSizeTestBeans() {
    final List<Bug25TestBean> wrongCases = new ArrayList<Bug25TestBean>();
    wrongCases.add(new Bug25TestBean("01234567890"));
    wrongCases.add(new Bug25TestBean("Very long Text which can't validate"));

    return wrongCases;
  }
}

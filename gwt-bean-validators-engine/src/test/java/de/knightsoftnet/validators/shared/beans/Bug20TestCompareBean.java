package de.knightsoftnet.validators.shared.beans;

import jakarta.validation.constraints.NotNull;

/**
 * bug 20 test bean for group handling.
 */
public class Bug20TestCompareBean {

  @NotNull(groups = {Rule.Conditions.class, Rule.All.class})
  private final Bug20TestEnum value;

  public Bug20TestCompareBean(Bug20TestEnum value) {
    super();
    this.value = value;
  }

  public Bug20TestEnum getValue() {
    return value;
  }

  @Override
  public String toString() {
    return "Bug20TestCompareBean [value=" + value + "]";
  }
}

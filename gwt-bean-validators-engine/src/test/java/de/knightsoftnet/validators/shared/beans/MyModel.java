package de.knightsoftnet.validators.shared.beans;

import jakarta.validation.constraints.NotNull;

public class MyModel {
  @NotNull(message = "Image should be provided")
  private byte[] imageBytes;

  public byte[] getImageBytes() {
    return imageBytes;
  }

  public void setImageBytes(final byte[] imageBytes) {
    this.imageBytes = imageBytes;
  }
}

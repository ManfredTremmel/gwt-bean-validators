/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.testcases;

import de.knightsoftnet.validators.shared.beans.HibernatePatternBug10TestBean;

import java.util.ArrayList;
import java.util.List;

/**
 * get test cases for hibernate pattern test.
 *
 * @author Manfred Tremmel
 *
 */
public class HibernatePatternBug10TestCases {
  /**
   * get empty test bean.
   *
   * @return empty test bean
   */
  public static final HibernatePatternBug10TestBean getEmptyTestBean() {
    return new HibernatePatternBug10TestBean(null);
  }

  /**
   * get correct test beans.
   *
   * @return correct test beans
   */
  public static final List<HibernatePatternBug10TestBean> getCorrectTestBeans() {
    final List<HibernatePatternBug10TestBean> correctCases = new ArrayList<>();
    correctCases.add(new HibernatePatternBug10TestBean(null));
    correctCases.add(new HibernatePatternBug10TestBean(""));
    correctCases.add(new HibernatePatternBug10TestBean("foo"));
    correctCases.add(new HibernatePatternBug10TestBean("a b c foo"));

    return correctCases;
  }

  /**
   * get wrong test beans.
   *
   * @return wrong test beans
   */
  public static final List<HibernatePatternBug10TestBean> getWrongTestBeans() {
    final List<HibernatePatternBug10TestBean> wrongCases = new ArrayList<>();
    wrongCases.add(new HibernatePatternBug10TestBean("bla\nbla"));

    return wrongCases;
  }
}

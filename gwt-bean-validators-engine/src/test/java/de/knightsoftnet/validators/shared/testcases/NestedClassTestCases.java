/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.testcases;

import de.knightsoftnet.validators.shared.beans.NestedClassTestBean;

import java.util.ArrayList;
import java.util.List;

/**
 * get test cases for nested class validation.
 *
 * @author Manfred Tremmel
 *
 */
public class NestedClassTestCases {
  /**
   * get empty test bean.
   *
   * @return empty test bean
   */
  public static final NestedClassTestBean.NestedClass getEmptyTestBean() {
    return new NestedClassTestBean.NestedClass(null);
  }

  /**
   * get correct test beans.
   *
   * @return correct test beans
   */
  public static final List<NestedClassTestBean.NestedClass> getCorrectTestBeans() {
    final List<NestedClassTestBean.NestedClass> correctCases = new ArrayList<>();
    correctCases.add(new NestedClassTestBean.NestedClass(" "));
    correctCases.add(new NestedClassTestBean.NestedClass("\t"));
    correctCases.add(new NestedClassTestBean.NestedClass("\n"));
    correctCases.add(new NestedClassTestBean.NestedClass("abcde"));

    return correctCases;
  }

  /**
   * get wrong test beans.
   *
   * @return wrong test beans
   */
  public static final List<NestedClassTestBean.NestedClass> getWrongTestBeans() {
    final List<NestedClassTestBean.NestedClass> wrongCases = new ArrayList<>();
    wrongCases.add(new NestedClassTestBean.NestedClass(null));
    wrongCases.add(new NestedClassTestBean.NestedClass(""));

    return wrongCases;
  }
}

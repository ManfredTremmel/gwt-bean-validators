package de.knightsoftnet.validators.shared.beans;

import java.util.ArrayList;
import java.util.List;

import jakarta.validation.Valid;

public class Bug27TestBeanA {

  @Valid
  private List<Bug27TestBeanB<Bug27TestBeanD>> classC;

  public Bug27TestBeanA() {
    super();
    classC = new ArrayList<>();
  }

  public Bug27TestBeanA(@Valid List<Bug27TestBeanB<Bug27TestBeanD>> classC) {
    super();
    this.classC = classC;
  }

  public List<Bug27TestBeanB<Bug27TestBeanD>> getClassC() {
    return classC;
  }
}

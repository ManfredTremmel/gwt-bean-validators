gwt-bean-validators-engine
==========================

The engine is the heart of the gwt-bean-validators framework and implements bean validation [Jakarta Bean Validation 3.0](ttps://beanvalidation.org/3.0/) on frontend side. It's based on the work of the gwt validation implementation, which was no longer maintained and deprecated in gwt 2.8rc1. In the last years it made a lot of progress, so it's not only a drop-in replacement.

Here are some highlights:

* gwt implementation based on outdated validation-api 1.0.0.GA and hibernate-validator 4.1.0 to implement [JSR-303](https://beanvalidation.org/1.0/), gwt-bean-validators-engine uses validation-api 3.0.2 and hibernate-validator 8.0.2 to implement the improved [Jakarta Bean Validation 3.0](https://beanvalidation.org/3.0/)
* few of the validators, hibernate provided, really worked in gwt, now the list is nearly complete (see below)
* a reflection emulation, based on getters, allows to access bean values with apache commons `BeanUtils` and `PropertyUtils`
* code generator was ported from gwt code generator to annotation processing to be prepared for gwt 3
* can be used in pure java without `GWT.creat()` or `JSNI`, so tests can run in jvm

Supported Validation Annotations
--------------------------------
This is the state of the client side support of [hibernate-validator](https://docs.jboss.org/hibernate/stable/validator/reference/en-US/html_single/#section-builtin-constraints) built-in constraints:

|Annotation | Support state 
|------|------------------
|AssertFalse | works
|AssertTrue | works
|DecimalMax | works
|DecimalMin | works
|Digits | works
|Email | works
|Future | limited to `Date` and `Calendar`, see below
|FutureOrPresent | limited to `Date` and `Calendar`, see below
|Max | works
|Min | works
|NotBlank | works
|NotEmpty | works
|NotNull | works
|Negative | works
|NegativeOrZero | works
|Null | works
|Past | limited to `Date` and `Calendar`, see below
|PastOrPresent | limited to `Date` and `Calendar`, see below
|Pattern | works
|Positive | works
|PositiveOrZero | works
|Size | works
|Currency | not supported because of a missing JavaMoney ([JSR 354](https://jcp.org/en/jsr/detail?id=354)) implementation
|DurationMax | not supported, see below
|DurationMin | not supported, see below
|EAN | works
|ISBN | works
|Length | works
|CodePointLength | works
|LuhnCheck | works
|Mod10Check | works
|Mod11Check | works
|Range | works
|ScriptAssert | not supported
|UniqueElements | works
|URL | works
|CNPJ | works
|CPF | works
|TituloEleitoral | works
|NIP | works
|PESEL | works
|REGON | works
|INN | works
|UUID | works

The java.time (date/time) types ([JSR 310](https://jcp.org/en/jsr/detail?id=310)) are not included in gwt, so they are not supported by default. But there's an additional project [gwt-time](https://github.com/foal/gwt-time) which has ported it to gwt. If you need support for it, you can switch to the extended [gwt-bean-validators-engine-jsr310](../gwt-bean-validators-engine-jsr310) it references gwt-time project and extends the validation to support this types.

There's also a number of validatiors I've written and provide as [mt-bean-validators](../mt-bean-validators), they work together with gwt, but also need a little extension of the engine, which can be included when using [gwt-bean-validators-engine-mt](../gwt-bean-validators-engine-mt).

Maven integration
----------------

Add the dependencies itself for GWT-Projects:

```xml
    <dependency>
      <groupId>de.knightsoft-net</groupId>
      <artifactId>gwt-bean-validators-engine</artifactId>
      <version>2.4.1</version>
    </dependency>
```

You can remove **gwt-user** dependency, it'll be included by **gwt-bean-validators-engine**, but if you keep it, exclude **validation-api** dependency (otherwise old 1.0.0.GA will be included which conflicts with new one):

```xml
    <dependency>
      <groupId>org.gwtproject</groupId>
      <artifactId>gwt-user</artifactId>
      <version>2.12.1</version>
      <exclusions>
        <exclusion>
          <groupId>javax.validation</groupId>
          <artifactId>validation-api</artifactId>
        </exclusion>
      </exclusions>
    </dependency>
```

GWT Integration
---------------

Add this inherits entry to your projects gwt.xml configuration file:

```xml
<inherits name="de.knightsoftnet.validators.GwtBeanValidatorsEngine" />
```

No additional entries are required or used by the engine.

Getting Started with GWT Validation
-----------------------------------

### General

You have to use `de.knightsoftnet.validators` classes and not `org.gwtproject.validation` in the project, don't mix it. For existing projects which used gwt validation before, you should replace both strings in the whole project.

### Setting Constraints

GWT validation is done by annotating beans, getters and properties with constraint annotations. See the [Jakarta Bean Validation 3.0](ttps://beanvalidation.org/3.0/) specification for more information on how to define and use constraints.
Here is a little example which ensures, the name has a minimum length of four characters if filled (Size annotation allows null entry, so you have additional add `@NotNull` or `@NotEmpty` annotation if you don't allow null value):

```java
public class Person {
  @Size(min = 4)
  private String name;
}
```

### Creating a Validator Factory

A validator factory is required to bootstrap the validation process. To create a validator factory you must make a class which extends `AbstractGwtValidatorFactory` and implements the `createValidator()` method. This method should return the validator object which will be used to perform the validation.

```java
import de.knightsoftnet.validators.client.AbstractGwtValidatorFactory;
import de.knightsoftnet.validators.client.GwtValidation;
import de.knightsoftnet.validators.client.impl.AbstractGwtValidator;

import jakarta.validation.Validator;

public final class SampleValidatorFactory extends AbstractGwtValidatorFactory {

  /**
   * Validator marker for the Validation Sample project. Only the classes and groups listed
   * in the {@link GwtValidation} annotation can be validated.
   */
  @GwtValidation(Person.class)
  public interface GwtValidator extends Validator {
  }

  @Override
  public AbstractGwtValidator createValidator() {
    return new GwtValidatorImpl();
  }
}
```

The generator creates a new validator implementation which has the same name as the annotated interface, concatenated with suffix `Impl`, it can be simply instantiated with new and given back in `createValidator()` method. The generation can be configured by some parameters in the `@GwtValidation` annotation:
* groups: array of validation groups to use on validation, default value is `jakarta.validation.groups.Default.class`
* value: a list of classes which can be validated. no default value!
* reflect: a list of classes, to generate reflection emulation code for, it must be a subset of the `value` list, or empty. In case of a empty list, for each constraint class the code is generated. Default is a empty list
* forceUsingGetter: boolean, hint for validation code generator to use getters to access values and do not generate JSNI workaround to access private fields, default is `false`
* generateReflectionGetter: boolean, hint for validation code generator to generate reflection helpers using getters to access values by name, default is `true`. The reflection helper stuff is generated for all constrained beans (class based valdiation or `@Valid` annotation), if you are sure, you don't need it, you can disable it with this option
* generateValidatorFactoryInterface: boolean, generate the interface as super-source to automatically integrate validation factory into process, default is `true`
* languages: array of strings with the languages, the message for this languages are integrated into the generated code, default is `{"de", "fr", "en"}`

### Validation Messages

The validation Messages are provided in properties files, `ValidationMessages.properties` and `ValidationMessages_${lang}.properties`. The code generator looks into claaspath and as fallback into package `org.hibernate.validator`, to read the messages and generates required stuff. So simply copy existing files into your classpath (in maven projects, into resources folder) and change or extend them with your messages.

### Validating Constraints

Use the standard validation bootstrap with the default factory to get the generated `Validator` for your objects. You may use this validator to validate an entire bean object or just specific properties of a bean. This way works only when you let the generator create the `ValidatorFactoryInterface`.

```java
final Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
Set<ConstraintViolation<Person>> violations = validator.validate(person);
```

### Validating Constraints (without `ValidatorFactoryInterface`)

When no `ValidatorFactoryInterface` is generated, you have to integrate `ValidatorFactory` manually:

```java
final Validator validator = Validation.byDefaultProvider()
    .configure()
    .gwtFactory(new SampleValidatorFactory())
    .buildValidatorFactory()
    .getValidator();
Set<ConstraintViolation<Person>> violations = validator.validate(person);
```

or use the short variant:

```java
final Validator validator = Validation
    .buildGwtValidatorFactory(new SampleValidatorFactory())
    .getValidator();
Set<ConstraintViolation<Person>> violations = validator.validate(person);
```


In this case, you can't use `jakarta.validation.Validation` for generation, use the extended `de.knightsoftnet.validators.client.impl.Validation`, which has the additional gwt stuff included.

### Validation Groups

You may also put constraints on your objects into “validation groups,” and then perform the validation only on certain subsets of constraints.

All constraints are automatically a part of the `Default` group unless you specify otherwise. Creating a new group is as simple as making an empty interface:

```java
/** Validates a minimal set of constraints */
public interface Minimal { }
```

If you are using any validation groups other than `Default` in client code, it is important that you list them in the “groups” parameter of the `@GwtValidation` annotation.

```java
@GwtValidation(value = {Person.class, Address.class}, groups = {Default.class, Minimal.class})
public interface GwtValidator extends Validator {
}
```

After that, you can specify this group in the “groups” parameter of any constraint:

```java
public class Address {
  @NotEmpty(groups = Minimal.class)
  @Size(max=50)
  private String street;
  
  @NotEmpty
  private String city;
  
  @NotEmpty(groups = {Minimal.class, Default.class})
  private String zipCode;
  ...
}
```

From here you can validate an `Address` object using the `Default` group, which contains three constraints (`@Size` on “street”, `@NotEmpty` on “city”, and `@NotEmpty` on “zipCode”):

```java
Address address = new Address();
validator.validate(address);
```

Or validate using the Minimal group, which contains @NotEmpty on “street” and @NotEmpty on “zipCode”:

```java
validator.validate(address, Minimal.class);
```

Best Practices
--------------
What do you do with the validation result? If you use the editor framework, you simply can give it to the editor driver to notify each widget using `driver.setConstraintViolations(violations);`. But it's much simpler to let the editor driver do the job itself, take a look at the [gwt-bean-validators-editor](../gwt-bean-validators-editor), it also makes a definition of a `ValidatorFactory` obsolete.


New in version 1.5.0 or later
-----------------------------
If you use a validation factory for validating, replace in method `createValidator()` the `return GWT.create(GwtValidator.class);` by `return new GwtValidatorImpl();`. GwtValidatorImpl will be generated by annotation driven code generation, so you need one build until IDE can find this file. The rest should work as before.

New in version 1.7.0 or later
-----------------------------
* in the past you had to add a replacement entry in your `module.gwt.xml`, like the following, you can remove it, it's no longer used

```xml
<replace-with class="org.sample.validation.client.SampleValidatorFactory">
  <when-type-is class="javax.validation.ValidatorFactory" />
</replace-with>
```
* to integrate own validation messages, in the past you had to add ValidationMessages.properties and create a interface with one entry for each message. Forget the interface, add the ValidationMessages.properties into classpath (in maven projects, simply copy it into resources folder), that's all.

New in version 1.7.0 or later
-----------------------------
There's no need to include validation-api any longer ad dependency to your project. All you need is included.

New in version 2.0.0 or later
-----------------------------
Switching to new api version 3.0 requires to replace all `javax.validation` imports to `jakarta.validation`, and the switch to hibernate-validator 8 sets the supported java versions to 11 or later.

New in version 2.3.0 or later
-----------------------------
Spring 3.3.x has a requirement of Java 17, with gwt 2.12.0 also gwt supports Java 17 and a lot of workarounds could be removed to bring both together.
gwt-bean-validators 2.3.0 has taken the chance and migrated to gwt 2.12.1 and uses Java 17 including all the cool stuff by pattern matching, text blocks and switch statements.
This also means, Java 17 and gwt >= 2.12.0 are required!

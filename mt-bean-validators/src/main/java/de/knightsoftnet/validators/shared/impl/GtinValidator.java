/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.impl;

import de.knightsoftnet.validators.shared.Gtin;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.routines.checkdigit.EAN13CheckDigit;

import java.util.Objects;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

/**
 * Check a string if it's a valid GTIN/EAN.
 *
 * @author Manfred Tremmel
 *
 */
public class GtinValidator implements ConstraintValidator<Gtin, Object> {

  /**
   * apache commons class to check/calculate GTIN/EAN check sums.
   */
  private static final EAN13CheckDigit CHECK_GTIN = new EAN13CheckDigit();

  /**
   * {@inheritDoc} initialize the validator.
   *
   * @see jakarta.validation.ConstraintValidator#initialize(java.lang.annotation.Annotation)
   */
  @Override
  public final void initialize(final Gtin constraintAnnotation) {
    // nothing to do
  }

  /**
   * {@inheritDoc} check if given string is a valid gtin.
   *
   * @see jakarta.validation.ConstraintValidator#isValid(java.lang.Object,
   *      jakarta.validation.ConstraintValidatorContext)
   */
  @Override
  public final boolean isValid(final Object value, final ConstraintValidatorContext context) {
    final String valueAsString = Objects.toString(value, null);
    if (StringUtils.isEmpty(valueAsString)) {
      return true;
    }
    if (!StringUtils.isNumeric(valueAsString)) {
      // EAN must be numeric, but that's handled by digits annotation
      return true;
    }
    if (valueAsString.length() != Gtin8Validator.GTIN8_LENGTH
        && valueAsString.length() != Gtin13Validator.GTIN13_LENGTH) {
      // EAN size is wrong, but that's handled by alternate size annotation
      return true;
    }
    // calculate and check checksum (GTIN/EAN)
    return CHECK_GTIN.isValid(valueAsString);
  }
}

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.data;

import org.apache.commons.lang3.StringUtils;

import java.util.Objects;

/**
 * phone number data.
 *
 * @author Manfred Tremmel
 *
 */
public class PhoneNumberData implements PhoneNumberExtendedInterface, ValidationInterface {

  private String countryCode;
  private String countryName;
  private String areaCode;
  private String areaName;
  private String lineNumber;
  private String extension;
  private boolean valid;

  /**
   * default constructor.
   */
  public PhoneNumberData() {
    super();
    valid = true;
  }

  /**
   * constructor initializing fields.
   *
   * @param countryCode country code
   * @param areaCode area code
   * @param lineNumber phone number
   * @param extension extension
   */
  public PhoneNumberData(final String countryCode, final String areaCode, final String lineNumber,
      final String extension) {
    this();
    this.countryCode = countryCode;
    this.areaCode = areaCode;
    this.lineNumber = lineNumber;
    this.extension = extension;
  }

  /**
   * constructor initializing fields.
   *
   * @param phoneNumber phone number data
   */
  public PhoneNumberData(final PhoneNumberInterface phoneNumber) {
    this(phoneNumber.getCountryCode(), phoneNumber.getAreaCode(), phoneNumber.getLineNumber(),
        phoneNumber.getExtension());
    if (phoneNumber instanceof final PhoneNumberExtendedInterface phoneNumberExtended) {
      countryName = phoneNumberExtended.getCountryName();
      areaName = phoneNumberExtended.getAreaName();
    }
    if (phoneNumber instanceof final ValidationInterface validationInterface) {
      valid = validationInterface.isValid();
    }
  }


  @Override
  public final String getCountryCode() {
    return countryCode;
  }

  @Override
  public final void setCountryCode(final String countryCode) {
    this.countryCode = countryCode;
  }

  @Override
  public final String getCountryName() {
    return countryName;
  }

  @Override
  public final void setCountryName(final String countryName) {
    this.countryName = countryName;
  }

  @Override
  public final String getAreaCode() {
    return areaCode;
  }

  @Override
  public final void setAreaCode(final String areaCode) {
    this.areaCode = areaCode;
  }

  @Override
  public final String getAreaName() {
    return areaName;
  }

  @Override
  public final void setAreaName(final String areaName) {
    this.areaName = areaName;
  }

  @Override
  public final String getLineNumber() {
    return lineNumber;
  }

  @Override
  public final void setLineNumber(final String lineNumber) {
    this.lineNumber = lineNumber;
  }

  @Override
  public final String getExtension() {
    return extension;
  }

  @Override
  public final void setExtension(final String extension) {
    this.extension = extension;
  }

  @Override
  public final boolean isValid() {
    return valid;
  }

  @Override
  public final void setValid(final boolean valid) {
    this.valid = valid;
  }

  @Override
  public int hashCode() {
    return Objects.hash(countryCode, areaCode, lineNumber, extension);
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (this.getClass() != obj.getClass()) {
      return false;
    }
    final PhoneNumberData other = (PhoneNumberData) obj;
    return StringUtils.equals(countryCode, other.countryCode)
        && StringUtils.equals(areaCode, other.areaCode)
        && StringUtils.equals(lineNumber, other.lineNumber)
        && StringUtils.equals(extension, other.extension);
  }

  @Override
  public String toString() {
    return "PhoneNumberData [countryCode=" + countryCode + ", areaCode=" + areaCode
        + ", phoneNumber=" + lineNumber + ", extension=" + extension + "]";
  }
}

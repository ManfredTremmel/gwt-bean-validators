/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.util;

import de.knightsoftnet.validators.shared.data.PhoneCountrySharedConstants;

import java.util.Locale;

/**
 * utility interface for phone country constants provider.
 *
 * @author Manfred Tremmel
 *
 */
public interface PhoneCountryConstantsProvider {
  /**
   * get phone country constants for current local.
   *
   * @return PhoneCountrySharedConstants
   */
  PhoneCountrySharedConstants getPhoneCountryConstants();

  /**
   * get phone country constants for given local.
   *
   * @param locale local to get for
   * @return PhoneCountrySharedConstants
   */
  PhoneCountrySharedConstants getPhoneCountryConstants(final Locale locale);

  /**
   * set phone country constants when they are available.
   *
   * @param receiver where to set phone country constants
   */
  void setPhoneCountrySharedConstantsWhenAvailable(
      final HasSetPhoneCountrySharedConstants receiver);

  /**
   * set phone country constants for given local when they are available.
   *
   * @param locale local to get for
   * @param receiver where to set phone country constants
   */
  void setPhoneCountrySharedConstantsWhenAvailable(final Locale locale,
      final HasSetPhoneCountrySharedConstants receiver);

  /**
   * detect phone country constants data are available.
   *
   * @return true when data is available
   */
  boolean hasPhoneCountryConstants();
}

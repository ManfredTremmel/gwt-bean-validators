/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.util;

import de.knightsoftnet.validators.shared.data.CountryEnum;

import org.apache.commons.lang3.StringUtils;

/**
 * bic Util, helper methodes for bics.
 *
 * @author Manfred Tremmel
 *
 */
public class BicUtil {

  /**
   * character used for separating blocks.
   */
  public static final char SEPARATOR = ' ';

  /**
   * compress bic, remove all blanks inside.
   *
   * @param string string to compress
   * @return bic without spaces
   */
  public static String bicCompress(final String string) {
    return StringUtils.remove(string, SEPARATOR);
  }

  /**
   * get country of bic.
   *
   * @param string string with bic
   * @return country
   */
  public static CountryEnum getCountryOfBic(final String string) {
    if (string == null || string.length() < 6) {
      return null;
    }
    try {
      return CountryEnum.valueOf(StringUtils.substring(string, 4, 6));
    } catch (final IllegalArgumentException e) {
      return null;
    }
  }
}

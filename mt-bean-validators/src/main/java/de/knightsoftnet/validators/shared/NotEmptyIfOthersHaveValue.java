/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared;

import de.knightsoftnet.validators.shared.impl.NotEmptyIfOthersHaveValueValidator;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

/**
 * The annotated bean must contain at least two properties:
 * <ul>
 * <li>a field that has to be checked (option <code>field</code>)</li>
 * <li>a array of fields and entries to be compared (option
 * <code>otherFieldValueMapping</code>)</li>
 * </ul>
 * if the entries of <code>otherFieldValueMapping.fieldCompare</code> matches one of the
 * corresponding entries in <code>otherFieldValueMapping.valueCompare</code> (array of possible
 * values), <code>field</code> must not be empty (null or "").<br>
 * Supported types are beans, <code>null</code> elements are considered valid.<br>
 *
 */
@Constraint(validatedBy = {NotEmptyIfOthersHaveValueValidator.class})
@Target({ElementType.TYPE, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface NotEmptyIfOthersHaveValue {
  /**
   * validation message.
   *
   * @return the validation message
   */
  String message() default "{de.knightsoftnet.validators.shared.NotEmptyIfOthersHaveValue"
      + ".message}";

  /**
   * validation groups.
   *
   * @return the validation groups
   */
  Class<?>[] groups() default {};

  /**
   * name of the field to be validated.
   *
   * @return the name of the field to be validated
   */
  String field();

  /**
   * optional minimum value that the field should have.
   *
   * @return the minimum value that the field should have, -Double.MAX_VALUE if not specified.
   */
  double minValue() default -Double.MAX_VALUE;

  /**
   * array of FieldValueMapping's.
   *
   * @return an array of FieldValueMapping's
   */
  FieldValueMapping[] otherFieldValueMapping();

  /**
   * payload classes.
   *
   * @return the payload classes
   */
  Class<? extends Payload>[] payload() default {};

  /**
   * Defines several {@code NotEmptyIfOthersHaveValue} annotations on the same element.
   */
  @Target({ElementType.TYPE, ElementType.ANNOTATION_TYPE})
  @Retention(RetentionPolicy.RUNTIME)
  @interface List {
    /**
     * not empty if others have value.
     *
     * @return value
     */
    NotEmptyIfOthersHaveValue[] value();
  }
}

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.impl;

import org.apache.commons.lang3.StringUtils;

import java.lang.annotation.Annotation;
import java.util.Objects;

import jakarta.validation.ConstraintValidator;

/**
 * Check if a field's size has one of the two given sizes.
 *
 * @author Manfred Tremmel
 *
 */
public abstract class AbstractSizeWithoutSeparatorsValidator<A extends Annotation>
    implements ConstraintValidator<A, Object> {

  /**
   * true if whitespaces should be ignored.
   */
  protected boolean ignoreWhiteSpaces;

  /**
   * true if minus should be ignored.
   */
  protected boolean ignoreMinus;

  /**
   * true if slashes should be ignored.
   */
  protected boolean ignoreSlashes;

  /**
   * get string size without filtered parameters.
   *
   * @param value containing string
   * @return size without filtered parameters
   */
  protected int stringSizeWithoutParameters(final Object value) {
    String valueAsString = Objects.toString(value, StringUtils.EMPTY);
    if (StringUtils.isEmpty(valueAsString)) {
      return -1;
    }
    if (ignoreWhiteSpaces) {
      valueAsString = valueAsString.replaceAll("\\s", StringUtils.EMPTY);
    }
    if (ignoreMinus) {
      valueAsString = valueAsString.replaceAll("-", StringUtils.EMPTY);
    }
    if (ignoreSlashes) {
      valueAsString = valueAsString.replaceAll("/", StringUtils.EMPTY);
    }
    return valueAsString.length();
  }
}

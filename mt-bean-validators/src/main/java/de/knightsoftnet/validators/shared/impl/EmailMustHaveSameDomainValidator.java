/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.impl;

import de.knightsoftnet.validators.shared.EmailMustHaveSameDomain;
import de.knightsoftnet.validators.shared.util.BeanPropertyReaderUtil;

import org.apache.commons.lang3.StringUtils;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

/**
 * Check if two field entries (must be eMails) have the same domain.
 *
 * @author Manfred Tremmel
 *
 */
public class EmailMustHaveSameDomainValidator
    implements ConstraintValidator<EmailMustHaveSameDomain, Object> {

  private static final char SPLIT_CHAR = '@';

  /**
   * error message key.
   */
  private String message;
  /**
   * field1 name to compare.
   */
  private String field1Name;
  /**
   * field2 name to compare.
   */
  private String field2Name;

  /**
   * add error to field1.
   */
  private boolean addErrorToField1;

  /**
   * add error to field2.
   */
  private boolean addErrorToField2;

  /**
   * {@inheritDoc} initialize the validator.
   *
   * @see jakarta.validation.ConstraintValidator#initialize(java.lang.annotation.Annotation)
   */
  @Override
  public final void initialize(final EmailMustHaveSameDomain constraintAnnotation) {
    message = constraintAnnotation.message();
    field1Name = constraintAnnotation.field1();
    field2Name = constraintAnnotation.field2();
    addErrorToField1 = constraintAnnotation.addErrorToField1();
    addErrorToField2 = constraintAnnotation.addErrorToField2();
  }

  /**
   * {@inheritDoc} check if given object is valid.
   *
   * @see jakarta.validation.ConstraintValidator#isValid(Object,
   *      jakarta.validation.ConstraintValidatorContext)
   */
  @Override
  public final boolean isValid(final Object value, final ConstraintValidatorContext context) {
    if (value == null) {
      return true;
    }
    try {
      final String field1Value =
          getDomainOf(BeanPropertyReaderUtil.getNullSaveStringProperty(value, field1Name));
      final String field2Value =
          getDomainOf(BeanPropertyReaderUtil.getNullSaveStringProperty(value, field2Name));
      if (!StringUtils.equals(field1Value, field2Value)) {
        switchContext(context);
        return false;
      }
      return true;
    } catch (final Exception ignore) {
      switchContext(context);
      return false;
    }
  }

  private String getDomainOf(final String mail) {
    final String domain1;
    if (StringUtils.contains(mail, SPLIT_CHAR)) {
      domain1 = StringUtils.split(mail, SPLIT_CHAR)[1];
    } else {
      domain1 = mail;
    }
    return domain1;
  }

  private void switchContext(final ConstraintValidatorContext context) {
    if (addErrorToField1 || addErrorToField2) {
      context.disableDefaultConstraintViolation();
      if (addErrorToField1) {
        context.buildConstraintViolationWithTemplate(message).addPropertyNode(field1Name)
            .addConstraintViolation();
      }
      if (addErrorToField2) {
        context.buildConstraintViolationWithTemplate(message).addPropertyNode(field2Name)
            .addConstraintViolation();
      }
    }
  }
}

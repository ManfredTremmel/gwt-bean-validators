/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.util;

import de.knightsoftnet.validators.shared.data.CountryEnum;

import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * Country Util, get country code for language string.
 *
 * @author Manfred Tremmel
 *
 */
public class CountryUtil extends AbstractLocaleCountryUtil {
  private static final Map<String, CountryEnum> DEFAULT_COUNTRY_CODE_MAP = new HashMap<>();

  static {
    DEFAULT_COUNTRY_CODE_MAP.put("EN", CountryEnum.GB);
  }

  /**
   * convert language string to CountryEnum.
   *
   * @param language string with language value
   * @return CountryEnum for the language
   */
  public static CountryEnum convertLanguageToCountryEnum(final String language) {
    final CountryEnum result;
    final String[] localeSplitted = tokenizeLanguage(language);
    if (localeSplitted.length == 0) {
      result = null;
    } else {
      final String countryPart =
          StringUtils.upperCase(localeSplitted[localeSplitted.length == 1 ? 0 : 1]);
      if (DEFAULT_COUNTRY_CODE_MAP.containsKey(countryPart)) {
        result = DEFAULT_COUNTRY_CODE_MAP.get(countryPart);
      } else {
        try {
          result = CountryEnum.valueOf(countryPart);
        } catch (final IllegalArgumentException e) {
          return null;
        }
      }
    }
    return result;
  }
}

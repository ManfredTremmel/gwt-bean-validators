/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.data;

import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.Objects;

/**
 * phone number area code data.
 *
 * @author Manfred Tremmel
 *
 */
public class PhoneAreaCodeData implements Comparable<PhoneAreaCodeData>, Serializable {
  private static final long serialVersionUID = 2048090963587911035L;

  private String areaCode;
  private boolean regEx;
  private String areaName;
  private int minLength;
  private int maxLength;

  public PhoneAreaCodeData() {
    super();
  }

  /**
   * constructor initializing fields.
   *
   * @param areaCode area code
   * @param areaName area name
   * @param countryCodeLength length of the country code
   */
  public PhoneAreaCodeData(final String areaCode, final String areaName,
      final int countryCodeLength) {
    super();
    this.areaCode = StringUtils.replace(areaCode, "u5b", "[");
    regEx = !StringUtils.isNumeric(areaCode);
    if (StringUtils.contains(areaName, '¡')) {
      final String[] splittedName = StringUtils.defaultString(areaName).split("¡");
      this.areaName = splittedName[0];
      if (splittedName.length > 1) {
        minLength = Integer.parseInt(splittedName[1]);
      } else {
        minLength = 2;
      }
      if (splittedName.length > 2) {
        maxLength = Integer.parseInt(splittedName[2]);
      } else {
        maxLength = 15 - countryCodeLength - (regEx ? 3 : StringUtils.length(areaCode));
      }
    } else {
      this.areaName = areaName;
      minLength = 2;
      maxLength = 15 - countryCodeLength - (regEx ? 2 : StringUtils.length(areaCode));
    }
  }

  public String getAreaCode() {
    return areaCode;
  }

  public boolean isRegEx() {
    return regEx;
  }

  public String getAreaName() {
    return areaName;
  }

  public int getMinLength() {
    return minLength;
  }

  public int getMaxLength() {
    return maxLength;
  }

  public void setAreaCode(final String areaCode) {
    this.areaCode = areaCode;
  }

  public void setRegEx(final boolean regEx) {
    this.regEx = regEx;
  }

  public void setAreaName(final String areaName) {
    this.areaName = areaName;
  }

  public void setMinLength(final int minLength) {
    this.minLength = minLength;
  }

  public void setMaxLength(final int maxLength) {
    this.maxLength = maxLength;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(areaCode);
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (this.getClass() != obj.getClass()) {
      return false;
    }
    final PhoneAreaCodeData other = (PhoneAreaCodeData) obj;
    return StringUtils.equals(areaCode, other.areaCode);
  }

  @Override
  public int compareTo(final PhoneAreaCodeData compare) {
    if (equals(compare)) {
      return 0;
    }
    if (compare == null) {
      return 1;
    }
    if (regEx == compare.regEx) {
      if (areaCode == null) {
        return -1;
      }
      if (areaCode.equals(compare.areaCode)) {
        return 0;
      }
      if (compare.areaCode == null) {
        return 1;
      }
      if (areaCode.startsWith(compare.areaCode)) {
        return -1;
      }
      if (compare.areaCode.startsWith(areaCode)) {
        return 1;
      }
      return areaCode.compareTo(compare.areaCode);
    }
    return regEx ? 1 : -1;
  }
}

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.data;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class IbanLengthMapConstantsImpl implements IbanLengthMapSharedConstants, Serializable {

  private static final long serialVersionUID = -6914526389390131821L;

  private final Map<CountryEnum, IbanLengthDefinition> ibanLengthMap;

  public IbanLengthMapConstantsImpl() {
    super();
    ibanLengthMap = new HashMap<>();
  }

  /**
   * constructor initializing map.
   *
   * @param map map to put
   */
  public IbanLengthMapConstantsImpl(final Map<String, String> map) {
    ibanLengthMap = map.entrySet().stream()
        .collect(Collectors.toMap(entry -> CountryEnum.valueOf(entry.getKey()), // NOPMD
            entry -> new IbanLengthDefinition(entry.getValue())));
  }

  @Override
  public Map<CountryEnum, IbanLengthDefinition> getIbanLengthMap() {
    return ibanLengthMap;
  }

  /**
   * setter for iban length map.
   *
   * @param ibanLengthMap map to set
   */
  public void setIbanLengthMap(final Map<CountryEnum, IbanLengthDefinition> ibanLengthMap) {
    this.ibanLengthMap.clear();
    if (ibanLengthMap != null) {
      this.ibanLengthMap.putAll(ibanLengthMap);
    }
  }
}

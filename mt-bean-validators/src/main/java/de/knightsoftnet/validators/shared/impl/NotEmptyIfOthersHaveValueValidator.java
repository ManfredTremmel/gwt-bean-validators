/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.impl;

import de.knightsoftnet.validators.shared.FieldValueMapping;
import de.knightsoftnet.validators.shared.NotEmptyIfOthersHaveValue;
import de.knightsoftnet.validators.shared.util.BeanPropertyReaderUtil;

import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

/**
 * Check if a field is filled if other field have given values.
 *
 */
public class NotEmptyIfOthersHaveValueValidator
    implements ConstraintValidator<NotEmptyIfOthersHaveValue, Object> {

  private final Map<String, List<String>> otherFieldToValueCompareMap = new HashMap<>();
  private String message;
  private String fieldCheckName;
  private double minValue;

  @Override
  public void initialize(final NotEmptyIfOthersHaveValue constraintAnnotation) {
    final FieldValueMapping[] otherFieldValueMapping =
        constraintAnnotation.otherFieldValueMapping();
    for (final FieldValueMapping mapping : otherFieldValueMapping) {
      otherFieldToValueCompareMap.put(mapping.fieldCompare(),
          Arrays.asList(mapping.valueCompare()));
    }
    message = constraintAnnotation.message();
    fieldCheckName = constraintAnnotation.field();
    minValue = constraintAnnotation.minValue();
  }

  @Override
  public boolean isValid(final Object objectValue,
      final ConstraintValidatorContext constraintValidatorContext) {
    if (objectValue == null) {
      return true;
    } else {
      try {
        final String fieldCheckValue =
            BeanPropertyReaderUtil.getNullSaveStringProperty(objectValue, fieldCheckName);
        final Map<String, String> otherFieldToActualValueMapping = new HashMap<>();
        for (final String otherFieldName : otherFieldToValueCompareMap.keySet()) {
          otherFieldToActualValueMapping.put(otherFieldName,
              BeanPropertyReaderUtil.getNullSaveStringProperty(objectValue, otherFieldName));
        }

        if (!allOtherFieldValuesMatch(otherFieldToActualValueMapping)) {
          return true;
        }

        final boolean isValid = StringUtils.isNotEmpty(fieldCheckValue)
            && (minValue == -Double.MAX_VALUE || hasProperNumericValue(fieldCheckValue));
        if (!isValid) {
          switchContext(constraintValidatorContext);
        }
        return isValid;
      } catch (final Exception exception) {
        switchContext(constraintValidatorContext);
        return false;
      }
    }
  }

  private boolean hasProperNumericValue(final String fieldCheckValue) {
    try {
      return Double.parseDouble(fieldCheckValue) >= minValue;
    } catch (final NumberFormatException exception) {
      return false;
    }
  }

  private boolean allOtherFieldValuesMatch(final Map<String, String> otherFieldsToActualValueMap) {
    for (final Map.Entry<String, String> fieldNameValueEntry : otherFieldsToActualValueMap
        .entrySet()) {
      final List<String> compareValues =
          otherFieldToValueCompareMap.get(fieldNameValueEntry.getKey());
      if (!compareValues.contains(fieldNameValueEntry.getValue())) {
        return false;
      }
    }
    return true;
  }

  private void switchContext(final ConstraintValidatorContext constraintValidatorContext) {
    constraintValidatorContext.disableDefaultConstraintViolation();
    constraintValidatorContext.buildConstraintViolationWithTemplate(message)
        .addPropertyNode(fieldCheckName).addConstraintViolation();
  }
}

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.util;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Locale Util, get locale for language string.
 *
 * @author Manfred Tremmel
 *
 */
public class LocaleUtil extends AbstractLocaleCountryUtil {
  private static final Map<String, Locale> DEFAULT_LOCALE_MAP = new HashMap<>();

  static {
    DEFAULT_LOCALE_MAP.put("DE_DE", Locale.GERMANY);
    DEFAULT_LOCALE_MAP.put("DE", Locale.GERMAN);
    DEFAULT_LOCALE_MAP.put("EN_US", Locale.US);
    DEFAULT_LOCALE_MAP.put("EN_UK", Locale.UK);
    DEFAULT_LOCALE_MAP.put("EN_CA", Locale.CANADA);
    DEFAULT_LOCALE_MAP.put("FR_FR", Locale.FRANCE);
    DEFAULT_LOCALE_MAP.put("FR_CA", Locale.CANADA_FRENCH);
    DEFAULT_LOCALE_MAP.put("ZH_CN", Locale.CHINA);
    DEFAULT_LOCALE_MAP.put("ZH_TW", Locale.TAIWAN);
    DEFAULT_LOCALE_MAP.put("ZH", Locale.CHINESE);
    DEFAULT_LOCALE_MAP.put("JP", Locale.JAPAN);
    DEFAULT_LOCALE_MAP.put("IT", Locale.ITALY);
    DEFAULT_LOCALE_MAP.put("KO", Locale.KOREA);
  }

  /**
   * convert language string to Locale.
   *
   * @param language string with language value
   * @return Locale for the language
   */
  public static Locale convertLanguageToLocale(final String language) {
    final Locale locale;
    final String[] localeSplitted = tokenizeLanguage(language);
    if (localeSplitted.length == 0) {
      locale = null;
    } else {
      final String localeStringUp = language.toUpperCase().replace('-', '_');
      if (DEFAULT_LOCALE_MAP.containsKey(localeStringUp)) {
        locale = DEFAULT_LOCALE_MAP.get(localeStringUp);
      } else if (localeSplitted.length == 2) {
        locale = new Locale(localeSplitted[0].toLowerCase(), localeSplitted[1]);
      } else if (localeSplitted.length > 2) {
        locale = new Locale(localeSplitted[0].toLowerCase(), localeSplitted[1], localeSplitted[2]);
      } else {
        locale = new Locale(language);
      }
    }
    return locale;
  }
}

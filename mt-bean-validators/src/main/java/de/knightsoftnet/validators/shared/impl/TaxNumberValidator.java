/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.impl;

import de.knightsoftnet.validators.shared.TaxNumber;
import de.knightsoftnet.validators.shared.data.CountryEnum;
import de.knightsoftnet.validators.shared.data.CreateTaxNumberMapConstantsClass;
import de.knightsoftnet.validators.shared.data.TaxNumberMapSharedConstants;
import de.knightsoftnet.validators.shared.util.BeanPropertyReaderUtil;
import de.knightsoftnet.validators.shared.util.TaxNumberUtil;

import org.apache.commons.lang3.StringUtils;

import jakarta.validation.ConstraintValidatorContext;

/**
 * Check if a Tax Number field is valid for the selected country.
 *
 * @author Manfred Tremmel
 *
 */
public class TaxNumberValidator extends AbstractTaxTinNumberValidator<TaxNumber> {

  /**
   * map of vat id regex values for the different countries.
   */
  private static final TaxNumberMapSharedConstants TAX_NUMBER_MAP =
      CreateTaxNumberMapConstantsClass.create();

  /**
   * field name of the postal code field.
   */
  private String fieldPostalCode;

  /**
   * convert old tax numbers before validating (true/false).
   */
  private boolean convertOldNumbers;

  /**
   * field name of the vat id field.
   */
  private String fieldTaxNumber;

  /**
   * {@inheritDoc} initialize the validator.
   *
   * @see jakarta.validation.ConstraintValidator#initialize(java.lang.annotation.Annotation)
   */
  @Override
  public final void initialize(final TaxNumber constraintAnnotation) {
    message = constraintAnnotation.message();
    fieldCountryCode = constraintAnnotation.fieldCountryCode();
    fieldPostalCode = constraintAnnotation.fieldPostalCode();
    allowLowerCaseCountryCode = constraintAnnotation.allowLowerCaseCountryCode();
    convertOldNumbers = constraintAnnotation.convertOldNumbers();
    fieldTaxNumber = constraintAnnotation.fieldTaxNumber();
  }

  @Override
  public final boolean isValid(final Object value, final ConstraintValidatorContext context) {
    if (value == null) {
      return true;
    }
    try {
      String countryCode =
          BeanPropertyReaderUtil.getNullSaveStringProperty(value, fieldCountryCode);
      if (StringUtils.isEmpty(countryCode)) {
        return true;
      }

      if (allowLowerCaseCountryCode) {
        countryCode = StringUtils.upperCase(countryCode);
      }

      final String regExCheck = TAX_NUMBER_MAP.taxNumbers().get(countryCode);
      if (regExCheck == null) {
        return true;
      }

      final CountryEnum country = CountryEnum.valueOf(countryCode);

      final String postalCode =
          BeanPropertyReaderUtil.getNullSaveStringProperty(value, fieldPostalCode);

      final String taxNumber =
          convertOldNumbers
              ? TaxNumberUtil.regionTaxToNational(
                  BeanPropertyReaderUtil.getNullSaveStringProperty(value, fieldTaxNumber), country,
                  postalCode)
              : BeanPropertyReaderUtil.getNullSaveStringProperty(value, fieldTaxNumber);
      if (StringUtils.isEmpty(taxNumber)) {
        return true;
      }

      if (taxNumber.matches(regExCheck) && checkSumTest(country, taxNumber)) {
        return true;
      }
      switchContext(context);
      return false;
    } catch (final Exception ignore) {
      switchContext(context);
      return false;
    }
  }

  private void switchContext(final ConstraintValidatorContext context) {
    context.disableDefaultConstraintViolation();
    context.buildConstraintViolationWithTemplate(message).addPropertyNode(fieldTaxNumber)
        .addConstraintViolation();
  }

  private boolean checkSumTest(final CountryEnum countryCode, final String taxNumber) {
    return switch (countryCode) {
      case AT -> checkAtNumber(taxNumber);
      case DE -> checkDeTaxNumber(taxNumber);
      case HR -> checkModulo11(taxNumber);
      case DK -> checkDk(taxNumber);
      case EE, LT -> checkEe(taxNumber);
      case ES -> checkEs(taxNumber);
      case BA, ME, MK -> checkUniqueMasterCitizenNumber(taxNumber);
      case NL -> checkNl(taxNumber);
      case PL -> checkPl(taxNumber);
      default -> true;
    };
  }

  /**
   * check the Tax Identification Number number, country version for Germany.
   *
   * @param taxNumber vat id to check
   * @return true if checksum is ok
   */
  private boolean checkDeTaxNumber(final String taxNumber) {
    final int fa = Integer.parseInt(StringUtils.substring(taxNumber, 2, 4));
    final int sb = Integer.parseInt(StringUtils.substring(taxNumber, 5, 8));
    final int checkSum = taxNumber.charAt(12) - '0';
    if (StringUtils.startsWith(taxNumber, "11")) {
      // Berlin
      final int calculatedCheckSum;
      if (fa >= 27 && fa <= 30 //
          || fa < 31 && (sb < 201 || sb > 693) //
          || fa == 19 && (sb < 200 || sb > 639 && sb < 680 || sb > 680 && sb < 684 || sb > 684) //
          || fa == 37) {
        calculatedCheckSum = ((taxNumber.charAt(5) - '0') * 7 //
            + (taxNumber.charAt(6) - '0') * 6 //
            + (taxNumber.charAt(7) - '0') * 5 //
            + (taxNumber.charAt(8) - '0') * 8 //
            + (taxNumber.charAt(9) - '0') * 4 //
            + (taxNumber.charAt(10) - '0') * 3 //
            + (taxNumber.charAt(11) - '0') * 2) //
            % MODULO_11;
      } else {
        calculatedCheckSum = ((taxNumber.charAt(2) - '0') * 3 //
            + (taxNumber.charAt(3) - '0') * 2 //
            + (taxNumber.charAt(4) - '0') * 9 //
            + (taxNumber.charAt(5) - '0') * 8 //
            + (taxNumber.charAt(6) - '0') * 7 //
            + (taxNumber.charAt(7) - '0') * 6 //
            + (taxNumber.charAt(8) - '0') * 5 //
            + (taxNumber.charAt(9) - '0') * 4 //
            + (taxNumber.charAt(10) - '0') * 3 //
            + (taxNumber.charAt(11) - '0') * 2) //
            % MODULO_11;
      }
      return checkSum == calculatedCheckSum;
    }
    // TODO find checksum calculation routines for the rest
    return true;
  }
}

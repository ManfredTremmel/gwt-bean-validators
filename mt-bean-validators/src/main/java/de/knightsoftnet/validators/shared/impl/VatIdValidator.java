/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.impl;

import de.knightsoftnet.validators.shared.VatId;
import de.knightsoftnet.validators.shared.data.CountryEnum;
import de.knightsoftnet.validators.shared.data.CreateVatIdMapConstantsClass;
import de.knightsoftnet.validators.shared.data.VatIdMapSharedConstants;
import de.knightsoftnet.validators.shared.util.BeanPropertyReaderUtil;

import org.apache.commons.lang3.StringUtils;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

/**
 * Check if a vat id field is valid for the selected country.
 *
 * @author Manfred Tremmel
 *
 */
public class VatIdValidator implements ConstraintValidator<VatId, Object> {
  /**
   * modulo 11.
   */
  private static final int MODULO_11 = 11;
  /**
   * modulo 97.
   */
  private static final int MODULO_97 = 97;

  /**
   * map of vat id regex values for the different countries.
   */
  private static final VatIdMapSharedConstants VATID_MAP = CreateVatIdMapConstantsClass.create();

  /**
   * error message key.
   */
  private String message;

  /**
   * field name of the country code field.
   */
  private String fieldCountryCode;

  /**
   * are lower case country codes allowed (true/false).
   */
  private boolean allowLowerCaseCountryCode;

  /**
   * field name of the vat id field.
   */
  private String fieldVatId;

  /**
   * {@inheritDoc} initialize the validator.
   *
   * @see jakarta.validation.ConstraintValidator#initialize(java.lang.annotation.Annotation)
   */
  @Override
  public final void initialize(final VatId constraintAnnotation) {
    message = constraintAnnotation.message();
    fieldCountryCode = constraintAnnotation.fieldCountryCode();
    allowLowerCaseCountryCode = constraintAnnotation.allowLowerCaseCountryCode();
    fieldVatId = constraintAnnotation.fieldVatId();
  }

  @Override
  public final boolean isValid(final Object value, final ConstraintValidatorContext context) {
    if (value == null) {
      return true;
    }
    try {
      String countryCode =
          BeanPropertyReaderUtil.getNullSaveStringProperty(value, fieldCountryCode);
      if (StringUtils.isEmpty(countryCode)) {
        return true;
      }
      final String vatId = BeanPropertyReaderUtil.getNullSaveStringProperty(value, fieldVatId);
      if (StringUtils.isEmpty(vatId)) {
        return true;
      }

      if (allowLowerCaseCountryCode) {
        countryCode = StringUtils.upperCase(countryCode);
      }
      final CountryEnum countryEnum = countryToEnum(countryCode);

      final String regExCheck = VATID_MAP.vatIds().get(countryEnum);
      if (regExCheck == null) {
        return true;
      }
      if (vatId.matches(regExCheck) && checkSumTest(countryEnum, vatId)) {
        return true;
      }
      switchContext(context);
      return false;
    } catch (final Exception ignore) {
      switchContext(context);
      return false;
    }
  }

  private CountryEnum countryToEnum(final String string) {
    if (string == null) {
      return null;
    }
    try {
      return CountryEnum.valueOf(string);
    } catch (final IllegalArgumentException e) {
      return null;
    }
  }

  private void switchContext(final ConstraintValidatorContext context) {
    context.disableDefaultConstraintViolation();
    context.buildConstraintViolationWithTemplate(message).addPropertyNode(fieldVatId)
        .addConstraintViolation();
  }

  private boolean checkSumTest(final CountryEnum countryCode, final String vatId) {
    return switch (countryCode) {
      case AT -> checkAtVatId(vatId);
      case BE -> checkBeVatId(vatId);
      case DE -> checkDeVatId(vatId);
      case DK -> checkDkVatId(vatId);
      case ES -> checkEsVatId(vatId);
      case FI -> checkFiVatId(vatId);
      case FR -> checkFrVatId(vatId);
      case GR -> checkGrVatId(vatId);
      case IE -> checkIeVatId(vatId);
      case IT -> checkItVatId(vatId);
      case LU -> checkLuVatId(vatId);
      case NL -> checkNlVatId(vatId);
      case NO -> checkNoVatId(vatId);
      case PL -> checkPlVatId(vatId);
      case PT -> checkPtVatId(vatId);
      case SE -> checkSeVatId(vatId);
      case SI -> checkSiVatId(vatId);
      default -> true; // for other countries, I haven't found checksum rules
    };
  }

  /**
   * check the VAT identification number, country version for Austria.
   *
   * @param vatId vat id to check
   * @return true if checksum is ok
   */
  private boolean checkAtVatId(final String vatId) {
    final int checkSum = vatId.charAt(10) - '0';
    final int sum = vatId.charAt(3) - '0' + squareSum((vatId.charAt(4) - '0') * 2) + vatId.charAt(5)
        - '0' + squareSum((vatId.charAt(6) - '0') * 2) + vatId.charAt(7) - '0'
        + squareSum((vatId.charAt(8) - '0') * 2) + vatId.charAt(9) - '0';
    final int calculatedCheckSum = (96 - sum) % 10;
    return checkSum == calculatedCheckSum;
  }

  /**
   * check the VAT identification number, country version for Belgium.
   *
   * @param vatId vat id to check
   * @return true if checksum is ok
   */
  private boolean checkBeVatId(final String vatId) {
    final int numberPart = Integer.parseInt(vatId.substring(2, 10));
    final int checkSum = Integer.parseInt(vatId.substring(10));
    final int calculatedCheckSumBe = MODULO_97 - numberPart % MODULO_97;
    return checkSum == calculatedCheckSumBe;
  }

  /**
   * check the VAT identification number, country version for Denmark.
   *
   * @param vatId vat id to check
   * @return true if checksum is ok
   */
  private boolean checkDkVatId(final String vatId) {
    final int sum = (vatId.charAt(2) - '0') * 2 + (vatId.charAt(3) - '0') * 7
        + (vatId.charAt(4) - '0') * 6 + (vatId.charAt(5) - '0') * 5 + (vatId.charAt(6) - '0') * 4
        + (vatId.charAt(7) - '0') * 3 + (vatId.charAt(8) - '0') * 2 + vatId.charAt(9) - '0';
    return sum % MODULO_11 == 0;
  }

  /**
   * check the VAT identification number, country version for Germany.
   *
   * @param vatId vat id to check
   * @return true if checksum is ok
   */
  private boolean checkDeVatId(final String vatId) {
    final int checkSum = vatId.charAt(10) - '0';
    int sum = 10;
    for (int i = 2; i < 10; i++) {
      int summe = (vatId.charAt(i) - '0' + sum) % 10;
      if (summe == 0) {
        summe = 10;
      }
      sum = 2 * summe % MODULO_11;
    }
    int calculatedCheckSum = MODULO_11 - sum;
    if (calculatedCheckSum == 10) {
      calculatedCheckSum = 0;
    }
    return checkSum == calculatedCheckSum;
  }

  /**
   * check the VAT identification number, country version for Finland.
   *
   * @param vatId vat id to check
   * @return true if checksum is ok
   */
  private boolean checkFiVatId(final String vatId) {
    final int checkSum = vatId.charAt(9) - '0';
    final int sum = (vatId.charAt(2) - '0') * 7 + (vatId.charAt(3) - '0') * 9
        + (vatId.charAt(4) - '0') * 10 + (vatId.charAt(5) - '0') * 5 + (vatId.charAt(6) - '0') * 8
        + (vatId.charAt(7) - '0') * 4 + (vatId.charAt(8) - '0') * 2;
    final int calculatedCheckSum = MODULO_11 - sum % MODULO_11;
    return checkSum == calculatedCheckSum;
  }

  /**
   * check the VAT identification number, country version for France.
   *
   * @param vatId vat id to check
   * @return true if checksum is ok
   */
  private boolean checkFrVatId(final String vatId) {
    final int checkSum = Integer.parseInt(vatId.substring(2, 4));
    final int sum = Integer.parseInt(vatId.substring(4));
    final int calculatedCheckSum = (12 + 3 * (sum % MODULO_97)) % MODULO_97;
    return checkSum == calculatedCheckSum;
  }

  /**
   * check the VAT identification number, country version for Greece.
   *
   * @param vatId vat id to check
   * @return true if checksum is ok
   */
  private boolean checkGrVatId(final String vatId) {
    final int checkSum = vatId.charAt(10) - '0';
    final int sum = (vatId.charAt(2) - '0') * 256 + (vatId.charAt(3) - '0') * 128
        + (vatId.charAt(4) - '0') * 64 + (vatId.charAt(5) - '0') * 32 + (vatId.charAt(6) - '0') * 16
        + (vatId.charAt(7) - '0') * 8 + (vatId.charAt(8) - '0') * 4 + (vatId.charAt(9) - '0') * 2;
    int calculatedCheckSum = sum % MODULO_11;
    if (calculatedCheckSum > 9) {
      calculatedCheckSum = 0;
    }
    return checkSum == calculatedCheckSum;
  }

  /**
   * check the VAT identification number, country version for Ireland.
   *
   * @param vatId vat id to check
   * @return true if checksum is ok
   */
  private boolean checkIeVatId(final String vatId) {
    final char checkSum = vatId.charAt(9);
    String workVatId = vatId;
    if (vatId.charAt(3) >= 'A' && vatId.charAt(3) <= 'Z') {
      // old id, can be transfered to new form
      workVatId = vatId.substring(0, 2) + "0" + vatId.substring(4, 9) + vatId.substring(2, 3)
          + vatId.substring(9);
    }
    final int sum = (workVatId.charAt(2) - '0') * 8 + (workVatId.charAt(3) - '0') * 7
        + (workVatId.charAt(4) - '0') * 6 + (workVatId.charAt(5) - '0') * 5
        + (workVatId.charAt(6) - '0') * 4 + (workVatId.charAt(7) - '0') * 3
        + (workVatId.charAt(8) - '0') * 2;
    final int calculatedCheckSum = sum % 23;
    final char calculatedCheckSumCharIe;
    if (calculatedCheckSum == 0) {
      calculatedCheckSumCharIe = 'W';
    } else {
      calculatedCheckSumCharIe = (char) ('A' + calculatedCheckSum - 1);
    }
    return checkSum == calculatedCheckSumCharIe;
  }

  /**
   * check the VAT identification number, country version for Italy.
   *
   * @param vatId vat id to check
   * @return true if checksum is ok
   */
  private boolean checkItVatId(final String vatId) {
    final int checkSum = vatId.charAt(12) - '0';
    final int sum = vatId.charAt(2) - '0' + squareSum((vatId.charAt(3) - '0') * 2) + vatId.charAt(4)
        - '0' + squareSum((vatId.charAt(5) - '0') * 2) + vatId.charAt(6) - '0'
        + squareSum((vatId.charAt(7) - '0') * 2) + vatId.charAt(8) - '0'
        + squareSum((vatId.charAt(9) - '0') * 2) + vatId.charAt(10) - '0'
        + squareSum((vatId.charAt(11) - '0') * 2);
    final int calculatedCheckSumIt = 10 - sum % 10;
    return checkSum == calculatedCheckSumIt;
  }

  /**
   * check the VAT identification number, country version for Luxembourg.
   *
   * @param vatId vat id to check
   * @return true if checksum is ok
   */
  private boolean checkLuVatId(final String vatId) {
    final int numberPart = Integer.parseInt(vatId.substring(2, 8));
    final int checkSum = Integer.parseInt(vatId.substring(8));
    final int calculatedCheckSum = numberPart % 89;
    return checkSum == calculatedCheckSum;
  }

  /**
   * check the VAT identification number, country version for Netherlands.
   *
   * @param vatId vat id to check
   * @return true if checksum is ok
   */
  private boolean checkNlVatId(final String vatId) {
    final int checkSum = vatId.charAt(10) - '0';
    final int calculatedCheckSum = calcNlPtSum(vatId) % MODULO_11;
    return checkSum == calculatedCheckSum;
  }

  private int calcNlPtSum(final String vatId) {
    return (vatId.charAt(2) - '0') * 9 //
        + (vatId.charAt(3) - '0') * 8 //
        + (vatId.charAt(4) - '0') * 7 //
        + (vatId.charAt(5) - '0') * 6 //
        + (vatId.charAt(6) - '0') * 5 //
        + (vatId.charAt(7) - '0') * 4 //
        + (vatId.charAt(8) - '0') * 3 //
        + (vatId.charAt(9) - '0') * 2;
  }

  /**
   * check the VAT identification number, country version for Norway.
   *
   * @param vatId vat id to check
   * @return true if checksum is ok
   */
  private boolean checkNoVatId(final String vatId) {
    final int checkSum = vatId.charAt(10) - '0';
    final int sum = (vatId.charAt(2) - '0') * 3 + (vatId.charAt(3) - '0') * 2
        + (vatId.charAt(4) - '0') * 7 + (vatId.charAt(5) - '0') * 6 + (vatId.charAt(6) - '0') * 5
        + (vatId.charAt(7) - '0') * 4 + (vatId.charAt(8) - '0') * 3 + (vatId.charAt(9) - '0') * 2;
    final int calculatedCheckSum = MODULO_11 - sum % MODULO_11;
    return checkSum == calculatedCheckSum;
  }

  /**
   * check the VAT identification number, country version for Poland.
   *
   * @param vatId vat id to check
   * @return true if checksum is ok
   */
  private boolean checkPlVatId(final String vatId) {
    final int checkSum = vatId.charAt(11) - '0';
    final int sum = (vatId.charAt(2) - '0') * 6 + (vatId.charAt(3) - '0') * 5
        + (vatId.charAt(4) - '0') * 7 + (vatId.charAt(5) - '0') * 2 + (vatId.charAt(6) - '0') * 3
        + (vatId.charAt(7) - '0') * 4 + (vatId.charAt(8) - '0') * 5 + (vatId.charAt(9) - '0') * 6
        + (vatId.charAt(10) - '0') * 7;
    final int calculatedCheckSumPl = sum % MODULO_11;
    return checkSum == calculatedCheckSumPl;
  }

  /**
   * check the VAT identification number, country version for Portugal.
   *
   * @param vatId vat id to check
   * @return true if checksum is ok
   */
  private boolean checkPtVatId(final String vatId) {
    final int checkSum = vatId.charAt(10) - '0';
    int calculatedCheckSum = MODULO_11 - calcNlPtSum(vatId) % MODULO_11;
    if (calculatedCheckSum > 9) {
      calculatedCheckSum = 0;
    }
    return checkSum == calculatedCheckSum;
  }

  /**
   * check the VAT identification number, country version for Sweden.
   *
   * @param vatId vat id to check
   * @return true if checksum is ok
   */
  private boolean checkSeVatId(final String vatId) {
    final int checkSum = vatId.charAt(11) - '0';
    final int sum = squareSum((vatId.charAt(2) - '0') * 2) + vatId.charAt(3) - '0'
        + squareSum((vatId.charAt(4) - '0') * 2) + vatId.charAt(5) - '0'
        + squareSum((vatId.charAt(6) - '0') * 2) + vatId.charAt(7) - '0'
        + squareSum((vatId.charAt(8) - '0') * 2) + vatId.charAt(9) - '0'
        + squareSum((vatId.charAt(10) - '0') * 2);
    int calculatedCheckSum = 10 - sum % 10;
    if (calculatedCheckSum == 10) {
      calculatedCheckSum = 0;
    }
    return checkSum == calculatedCheckSum;
  }

  /**
   * check the VAT identification number, country version for Slovenia.
   *
   * @param vatId vat id to check
   * @return true if checksum is ok
   */
  private boolean checkSiVatId(final String vatId) {
    boolean checkSumOk;
    final int checkSum = vatId.charAt(9) - '0';
    final int sum = (vatId.charAt(2) - '0') * 8 + (vatId.charAt(3) - '0') * 7
        + (vatId.charAt(4) - '0') * 6 + (vatId.charAt(5) - '0') * 5 + (vatId.charAt(6) - '0') * 4
        + (vatId.charAt(7) - '0') * 3 + (vatId.charAt(8) - '0') * 2;
    int calculatedCheckSum = MODULO_11 - sum % MODULO_11;
    if (calculatedCheckSum == 11) {
      checkSumOk = false;
    } else {
      if (calculatedCheckSum == 10) {
        calculatedCheckSum = 0;
      }
      checkSumOk = checkSum == calculatedCheckSum;
    }
    return checkSumOk;
  }

  /**
   * check the VAT identification number, country version for Spain. TODO
   * http://www.pruefziffernberechnung.de/U/USt-IdNr.shtml routine doesn't work for alphabetic
   * checksums, so it's not used at the moment
   *
   * @param vatId vat id to check
   * @return true if checksum is ok
   */
  private boolean checkEsVatId(final String vatId) { // NOPMD
    // final boolean checkSumOk = true;
    // if (vatId.charAt(2) >= 'A' && vatId.charAt(2) <= 'Z') {
    // final int checkSumEs;
    // if (vatId.charAt(10) >= 'A' && vatId.charAt(10) <= 'Z') {
    // checkSumEs = vatId.charAt(10) - 'A';
    // } else {
    // checkSumEs = vatId.charAt(10) - '0';
    // }
    // final int sumEs =
    // squareSum((vatId.charAt(3) - '0') * 2) + vatId.charAt(4) - '0'
    // + squareSum((vatId.charAt(5) - '0') * 2) + vatId.charAt(6) - '0'
    // + squareSum((vatId.charAt(7) - '0') * 2) + vatId.charAt(8) - '0'
    // + squareSum((vatId.charAt(9) - '0') * 2);
    // int calculatedCheckSumEs = 10 - sumEs % 10;
    // if (calculatedCheckSumEs == 10) {
    // calculatedCheckSumEs = 0;
    // }
    // checkSumOk = checkSumEs == calculatedCheckSumEs;
    // } else {
    // final char checkSumEs = vatId.charAt(10);
    // final int sumEs = Integer.parseInt(vatId.substring(2, 10));
    // final char calculatedCheckSumEs;
    // switch (sumEs % 23) {
    // case 0:
    // calculatedCheckSumEs = 'T';
    // break;
    // case 1:
    // calculatedCheckSumEs = 'R';
    // break;
    // case 2:
    // calculatedCheckSumEs = 'W';
    // break;
    // case 3:
    // calculatedCheckSumEs = 'A';
    // break;
    // case 4:
    // calculatedCheckSumEs = 'G';
    // break;
    // case 5:
    // calculatedCheckSumEs = 'M';
    // break;
    // case 6:
    // calculatedCheckSumEs = 'Y';
    // break;
    // case 7:
    // calculatedCheckSumEs = 'F';
    // break;
    // case 8:
    // calculatedCheckSumEs = 'P';
    // break;
    // case 9:
    // calculatedCheckSumEs = 'D';
    // break;
    // case 10:
    // calculatedCheckSumEs = 'X';
    // break;
    // case 11:
    // calculatedCheckSumEs = 'B';
    // break;
    // case 12:
    // calculatedCheckSumEs = 'N';
    // break;
    // case 13:
    // calculatedCheckSumEs = 'J';
    // break;
    // case 14:
    // calculatedCheckSumEs = 'Z';
    // break;
    // case 15:
    // calculatedCheckSumEs = 'S';
    // break;
    // case 16:
    // calculatedCheckSumEs = 'Q';
    // break;
    // case 17:
    // calculatedCheckSumEs = 'V';
    // break;
    // case 18:
    // calculatedCheckSumEs = 'H';
    // break;
    // case 19:
    // calculatedCheckSumEs = 'L';
    // break;
    // case 20:
    // calculatedCheckSumEs = 'C';
    // break;
    // case 21:
    // calculatedCheckSumEs = 'K';
    // break;
    // case 22:
    // calculatedCheckSumEs = 'E';
    // break;
    // default:
    // calculatedCheckSumEs = ' ';
    // break;
    // }
    // checkSumOk = calculatedCheckSumEs == checkSumEs;
    // }
    // return checkSumOk;
    return true;
  }

  /**
   * calculate square sum.
   *
   * @param value value to calculate square sum for
   * @return square sum
   */
  private static int squareSum(final int value) {
    int result = 0;
    for (final char valueDigit : String.valueOf(value).toCharArray()) {
      result += Character.digit(valueDigit, 10);
    }
    return result;
  }
}

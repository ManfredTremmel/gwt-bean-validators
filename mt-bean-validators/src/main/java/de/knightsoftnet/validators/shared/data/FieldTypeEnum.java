/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.data;

/**
 * Field type enumeration.
 *
 * @author Manfred Tremmel
 *
 */
public enum FieldTypeEnum {
  /**
   * string/text.
   */
  STRING,

  /**
   * string/text localized.
   */
  STRING_LOCALIZED,

  /**
   * numeric.
   */
  NUMERIC,

  /**
   * boolean.
   */
  BOOLEAN,

  /**
   * date.
   */
  DATE,

  /**
   * time.
   */
  TIME,

  /**
   * date and time.
   */
  DATETIME,

  /**
   * enumeration with fixed values.
   */
  ENUM_FIXED,

  /**
   * enumeration read from database.
   */
  ENUM_SQL,

  /**
   * embedded substructure.
   */
  EMBEDDED,

  /**
   * list of referenced entities.
   */
  ONE_TO_MANY,

  /**
   * reference to another entity.
   */
  MANY_TO_ONE
}

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.impl;

import de.knightsoftnet.validators.shared.interfaces.LocalizedValue;

import java.lang.annotation.Annotation;
import java.util.Map.Entry;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

/**
 * abstract validatior for localized value.
 *
 * @author Manfred Tremmel
 *
 * @param <A> annotation interface for check
 */
public abstract class AbstractValidatorForLocalizedValue<A extends Annotation>
    implements ConstraintValidator<A, LocalizedValue<?, ?>> {

  protected String message;


  /**
   * Checks that the localized value.
   *
   * @param localizedValue the localized value to validate
   * @param constraintValidatorContext context in which the constraint is evaluated
   * @return returns {@code true} if the string is not {@code null} and the length of the trimmed
   *         {@code charSequence} is strictly superior to 0, {@code false} otherwise
   */
  @Override
  public boolean isValid(final LocalizedValue<?, ?> localizedValue,
      final ConstraintValidatorContext constraintValidatorContext) {
    if (localizedValue == null || localizedValue.getLocalizedText() == null) {
      return true;
    }
    final long numInvalid = localizedValue.getLocalizedText().entrySet().stream()
        .filter(entry -> !checkValue(entry, constraintValidatorContext)).count();
    return numInvalid == 0L;
  }

  protected boolean checkValue(final Entry<?, ?> entry,
      final ConstraintValidatorContext constraintValidatorContext) {
    final boolean valid = checkEntryValue(entry.getValue());
    if (!valid) {
      constraintValidatorContext.disableDefaultConstraintViolation();
      constraintValidatorContext.buildConstraintViolationWithTemplate(message)
          .addPropertyNode("localizedText[" + entry.getKey() + "]").addPropertyNode("<map value>")
          .addConstraintViolation();
      extendValidationContext(constraintValidatorContext);
    }
    return valid;
  }

  protected void extendValidationContext(
      final ConstraintValidatorContext constraintValidatorContext) {
    // nothing to do here, but can be extended
  }

  /**
   * check single localized value.
   *
   * @param value object to check
   * @return true if it's valid
   */
  protected abstract boolean checkEntryValue(final Object value);
}

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.impl;

import de.knightsoftnet.validators.shared.MustBeBiggerOrEqual;

/**
 * Check if entry of field1 is Bigger then field2.
 *
 * @author Manfred Tremmel
 *
 */
public class MustBeBiggerOrEqualValidator
    extends AbstractCompareFieldsValidator<MustBeBiggerOrEqual> {

  /**
   * {@inheritDoc} initialize the validator.
   *
   * @see jakarta.validation.ConstraintValidator#initialize(java.lang.annotation.Annotation)
   */
  @Override
  public final void initialize(final MustBeBiggerOrEqual mustBeBiggerOrEqualAnnotation) {
    message = mustBeBiggerOrEqualAnnotation.message();
    field1Name = mustBeBiggerOrEqualAnnotation.field1();
    field2Name = mustBeBiggerOrEqualAnnotation.field2();
    addErrorToField1 = mustBeBiggerOrEqualAnnotation.addErrorToField1();
    addErrorToField2 = mustBeBiggerOrEqualAnnotation.addErrorToField2();
  }

  @Override
  @SuppressWarnings({"unchecked"})
  protected boolean comparissonIsValid(final Object field1Value, final Object field2Value) {
    if (field2Value == null) {
      return true;
    }
    if (field1Value == null) {
      return false;
    }
    return field1Value instanceof final Comparable comparable
        && comparable.compareTo(field2Value) >= 0;
  }
}

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.data;

import com.fasterxml.jackson.annotation.JsonIgnore;

import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.Collections;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;

/**
 * phone number region/country code data.
 *
 * @author Manfred Tremmel
 *
 */
public class PhoneCountryCodeData implements Comparable<PhoneCountryCodeData>, Serializable {
  private static final long serialVersionUID = -3243230053577520263L;

  private String countryCode;
  private String countryCodeName;
  @JsonIgnore
  private PhoneCountryData phoneCountryData;
  private final Set<PhoneAreaCodeData> areaCodeData;

  public PhoneCountryCodeData() {
    this(null, null);
  }

  /**
   * constructor initializing fields.
   *
   * @param countryCode country code
   * @param countryCodeName country code name
   */
  public PhoneCountryCodeData(final String countryCode, final String countryCodeName) {
    super();
    this.countryCode = countryCode;
    this.countryCodeName = countryCodeName;
    areaCodeData = new TreeSet<>();
  }

  public String getCountryCode() {
    return countryCode;
  }

  public String getCountryCodeName() {
    return countryCodeName;
  }

  public PhoneCountryData getPhoneCountryData() {
    return phoneCountryData;
  }

  public void setPhoneCountryData(final PhoneCountryData phoneCountryData) {
    this.phoneCountryData = phoneCountryData;
  }

  public Set<PhoneAreaCodeData> getAreaCodeData() {
    return Collections.unmodifiableSet(areaCodeData);
  }

  public void addAreaCodeData(final PhoneAreaCodeData areaCodeData) {
    this.areaCodeData.add(areaCodeData);
  }

  public void setCountryCode(final String countryCode) {
    this.countryCode = countryCode;
  }

  public void setCountryCodeName(final String countryCodeName) {
    this.countryCodeName = countryCodeName;
  }

  /**
   * setter for are code data set.
   *
   * @param areaCodeData the set to set
   */
  public void setAreaCodeData(final Set<PhoneAreaCodeData> areaCodeData) {
    this.areaCodeData.clear();
    if (areaCodeData != null) {
      this.areaCodeData.addAll(areaCodeData);
    }
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(countryCode);
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (this.getClass() != obj.getClass()) {
      return false;
    }
    final PhoneCountryCodeData other = (PhoneCountryCodeData) obj;
    return StringUtils.equals(countryCode, other.countryCode);
  }

  @Override
  public int compareTo(final PhoneCountryCodeData compare) {
    if (countryCode == null) {
      return -1;
    }
    if (countryCode.equals(compare.countryCode)) {
      return 0;
    }
    if (compare.countryCode == null) {
      return 1;
    }
    if (countryCode.startsWith(compare.countryCode)) {
      return 1;
    }
    if (compare.countryCode.startsWith(countryCode)) {
      return -1;
    }
    return countryCode.compareTo(compare.countryCode);
  }
}

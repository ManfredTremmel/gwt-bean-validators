/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.data;

import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.Objects;

/**
 * country and bank account data.
 *
 * @author Manfred Tremmel
 *
 */
public class CountryBankAccountBicData implements Serializable {

  private static final long serialVersionUID = -4944870755732044237L;

  private CountryEnum countryCode;
  private String bankAccount;
  private String bic;

  /**
   * default constructor.
   */
  public CountryBankAccountBicData() {
    this(null, null, null);
  }

  /**
   * constructor initializing fields.
   *
   * @param countryCode country code
   * @param bankAccount bank account
   * @param bic sepa bic
   */
  public CountryBankAccountBicData(final CountryEnum countryCode, final String bankAccount,
      final String bic) {
    super();
    this.countryCode = countryCode;
    this.bankAccount = bankAccount;
    this.bic = bic;
  }

  public CountryEnum getCountryCode() {
    return countryCode;
  }

  public void setCountryCode(final CountryEnum countryCode) {
    this.countryCode = countryCode;
  }

  public String getBankAccount() {
    return bankAccount;
  }

  public void setBankAccount(final String bankAccount) {
    this.bankAccount = bankAccount;
  }

  public String getBic() {
    return bic;
  }

  public void setBic(final String bic) {
    this.bic = bic;
  }

  @Override
  public int hashCode() {
    return Objects.hash(countryCode, bankAccount);
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (this.getClass() != obj.getClass()) {
      return false;
    }
    final CountryBankAccountBicData other = (CountryBankAccountBicData) obj;
    return countryCode == other.countryCode //
        && StringUtils.equals(bankAccount, other.bankAccount);
  }
}

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.impl;

import de.knightsoftnet.validators.shared.data.PhoneNumberData;
import de.knightsoftnet.validators.shared.util.BeanPropertyReaderUtil;
import de.knightsoftnet.validators.shared.util.PhoneNumberUtil;

import org.apache.commons.lang3.StringUtils;

import java.lang.annotation.Annotation;
import java.util.Objects;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

/**
 * Check a string if it's a valid PhoneNumber.
 *
 * @author Manfred Tremmel
 *
 * @param <V> the value type
 */
public abstract class AbstractPhoneNumberValueValidator<V extends Annotation>
    implements ConstraintValidator<V, Object> {

  /**
   * error message key.
   */
  protected String message;

  /**
   * field name of the country code field.
   */
  protected String fieldCountryCode;

  /**
   * field name of the phone number field.
   */
  protected String fieldPhoneNumber;

  /**
   * are lower case country codes allowed (true/false).
   */
  protected boolean allowLowerCaseCountryCode;

  /**
   * allow din 5008 format (true/false).
   */
  protected boolean allowDin5008;

  /**
   * allow E123 format (true/false).
   */
  protected boolean allowE123;

  /**
   * allow uri format (true/false).
   */
  protected boolean allowUri;

  /**
   * allow microsoft format (true/false).
   */
  protected boolean allowMs;

  /**
   * allow not standardized but common format (true/false).
   */
  protected boolean allowCommon;

  /**
   * phone number utils to test phone numbers.
   */
  protected PhoneNumberUtil phoneNumberUtil;

  /**
   * {@inheritDoc} initialize the validator.
   *
   * @see jakarta.validation.ConstraintValidator#initialize(java.lang.annotation.Annotation)
   */
  @Override
  public abstract void initialize(final V constraintAnnotation);

  /**
   * {@inheritDoc} check if given string is a valid gln.
   *
   * @see jakarta.validation.ConstraintValidator#isValid(java.lang.Object,
   *      jakarta.validation.ConstraintValidatorContext)
   */
  @Override
  public final boolean isValid(final Object value, final ConstraintValidatorContext context) {
    final String valueAsString = Objects.toString(value, null);

    if (StringUtils.isEmpty(valueAsString)) {
      // empty field is ok
      return true;
    }
    if (!phoneNumberUtil.isInitialized()) {
      // when phone number utils are not initialized, we can't check
      return true;
    }
    try {
      String countryCode =
          BeanPropertyReaderUtil.getNullSaveStringProperty(value, fieldCountryCode);
      final String phoneNumber =
          BeanPropertyReaderUtil.getNullSaveStringProperty(value, fieldPhoneNumber);
      if (StringUtils.isEmpty(phoneNumber)) {
        return true;
      }

      if (allowLowerCaseCountryCode) {
        countryCode = StringUtils.upperCase(countryCode);
      }
      phoneNumberUtil.setCountryCode(countryCode);
      final PhoneNumberData parsedNumber = phoneNumberUtil.parsePhoneNumber(phoneNumber);

      if (parsedNumber.isValid()) {
        if (allowDin5008
            && (StringUtils.equals(phoneNumber, phoneNumberUtil.formatDin5008National(parsedNumber))
                || StringUtils.equals(phoneNumber,
                    phoneNumberUtil.formatDin5008International(parsedNumber)))) {
          return true;
        }
        if (allowE123
            && (StringUtils.equals(phoneNumber, phoneNumberUtil.formatE123National(parsedNumber))
                || StringUtils.equals(phoneNumber,
                    phoneNumberUtil.formatE123International(parsedNumber)))) {
          return true;
        }
        if (allowUri && StringUtils.equals(phoneNumber, phoneNumberUtil.formatUrl(parsedNumber))) {
          return true;
        }
        if (allowMs && StringUtils.equals(phoneNumber, phoneNumberUtil.formatMs(parsedNumber))) {
          return true;
        }
        if (allowCommon
            && (StringUtils.equals(phoneNumber, phoneNumberUtil.formatCommonNational(parsedNumber))
                || StringUtils.equals(phoneNumber,
                    phoneNumberUtil.formatCommonInternational(parsedNumber)))) {
          return true;
        }
      }
      switchContext(context);
      return false;
    } catch (final Exception ignore) {
      switchContext(context);
      return false;
    }
  }

  private void switchContext(final ConstraintValidatorContext context) {
    context.disableDefaultConstraintViolation();
    context.buildConstraintViolationWithTemplate(message).addPropertyNode(fieldPhoneNumber)
        .addConstraintViolation();
  }
}

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.impl;

import de.knightsoftnet.validators.shared.BankCountry;
import de.knightsoftnet.validators.shared.data.CountryEnum;
import de.knightsoftnet.validators.shared.util.AbstractIbanUtil;
import de.knightsoftnet.validators.shared.util.BeanPropertyReaderUtil;
import de.knightsoftnet.validators.shared.util.BicUtil;
import de.knightsoftnet.validators.shared.util.IbanUtil;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

/**
 * Check if a country field and the country in iban and bic match, implementation.
 *
 * @author Manfred Tremmel
 *
 */
public class BankCountryValidator implements ConstraintValidator<BankCountry, Object> {

  private static final String NOT_EMPTY_MESSAGE =
      "{jakarta.validation.constraints.NotEmpty.message}";

  /**
   * error message key.
   */
  private String message;

  /**
   * error message key.
   */
  private String messageWrongBic;

  /**
   * field name of the country code field.
   */
  private String fieldCountryCode;

  /**
   * are lower case country codes allowed (true/false).
   */
  private boolean allowLowerCaseCountryCode;

  /**
   * field name of the iban field.
   */
  private String fieldIban;

  /**
   * field name of the bic field.
   */
  private String fieldBic;

  /**
   * {@inheritDoc} initialize the validator.
   *
   * @see jakarta.validation.ConstraintValidator#initialize(java.lang.annotation.Annotation)
   */
  @Override
  public final void initialize(final BankCountry constraintAnnotation) {
    fieldCountryCode = constraintAnnotation.fieldCountryCode();
    allowLowerCaseCountryCode = constraintAnnotation.allowLowerCaseCountryCode();
    fieldIban = constraintAnnotation.fieldIban();
    fieldBic = constraintAnnotation.fieldBic();
    message = constraintAnnotation.message();
    messageWrongBic = constraintAnnotation.messageWrongBic();
  }

  /**
   * {@inheritDoc} check if given object is valid.
   *
   * @see jakarta.validation.ConstraintValidator#isValid(Object,
   *      jakarta.validation.ConstraintValidatorContext)
   */
  @Override
  public final boolean isValid(final Object value, final ConstraintValidatorContext context) {
    if (value == null) {
      return true;
    }
    try {
      final IbanUtil ibanUtil = new IbanUtil();
      final String valueCountry =
          BeanPropertyReaderUtil.getNullSaveStringProperty(value, fieldCountryCode);
      final String valueIban = BeanPropertyReaderUtil.getNullSaveStringProperty(value, fieldIban);
      final String valueBic = BeanPropertyReaderUtil.getNullSaveStringProperty(value, fieldBic);
      final String bicOfIban = ibanUtil.getBicOfIban(valueIban);

      if (StringUtils.isEmpty(valueIban) && StringUtils.isEmpty(valueBic)) {
        return true;
      } else if (StringUtils.isEmpty(valueIban)) {
        context.disableDefaultConstraintViolation();
        context.buildConstraintViolationWithTemplate(NOT_EMPTY_MESSAGE).addPropertyNode(fieldIban)
            .addConstraintViolation();
        return false;
      } else if (StringUtils.isEmpty(valueBic)) {
        context.disableDefaultConstraintViolation();
        context.buildConstraintViolationWithTemplate(NOT_EMPTY_MESSAGE).addPropertyNode(fieldBic)
            .addConstraintViolation();
        return false;
      } else if (StringUtils.length(valueIban) >= IbanValidator.IBAN_LENGTH_MIN
          && StringUtils.length(valueBic) >= BicValidator.BIC_LENGTH_MIN) {
        final CountryEnum countryIban =
            AbstractIbanUtil.getCountryOfIban(AbstractIbanUtil.ibanCompress(
                allowLowerCaseCountryCode ? StringUtils.upperCase(valueIban) : valueIban));
        final CountryEnum countryBic = BicUtil.getCountryOfBic(BicUtil
            .bicCompress(allowLowerCaseCountryCode ? StringUtils.upperCase(valueBic) : valueBic));
        final CountryEnum countryValue = ObjectUtils.defaultIfNull(
            AbstractIbanUtil.getCountryOfIban(AbstractIbanUtil.ibanCompress(
                allowLowerCaseCountryCode ? StringUtils.upperCase(valueCountry) : valueCountry)),
            countryIban);

        boolean ibanCodeMatches = false;
        boolean bicCodeMatches = false;
        final boolean bicIbanMatches = bicOfIban == null || StringUtils.equals(bicOfIban, valueBic)
            || StringUtils.equals(bicOfIban, valueBic + "XXX");
        switch (countryValue) {
          case GF, // French Guyana
              GP, // Guadeloupe
              MQ, // Martinique
              RE, // Reunion
              PF, // French Polynesia
              YT, // Mayotte
              NC, // New Caledonia
              BL, // Saint Barthelemy
              MF, // Saint Martin
              PM, // Saint Pierre et Miquelon
              WF: // Wallis and Futuna Islands
            // special solution for French oversea teritorials with french registry
            ibanCodeMatches = CountryEnum.FR == countryIban;
            bicCodeMatches = CountryEnum.FR == countryBic;
            break;
          case JE, // Jersey
              GG: // Guernsey
            // they can use GB or FR registry, but iban and bic code must match
            ibanCodeMatches = (CountryEnum.GB == countryIban || CountryEnum.FR == countryIban)
                && countryBic == countryIban;
            bicCodeMatches = CountryEnum.GB == countryBic || CountryEnum.FR == countryBic;
            break;
          default:
            ibanCodeMatches = countryValue == countryIban;
            bicCodeMatches = countryValue == countryBic;
            break;
        }
        if (ibanCodeMatches && bicCodeMatches && bicIbanMatches) {
          return true;
        }
        context.disableDefaultConstraintViolation();
        if (!ibanCodeMatches) {
          context.buildConstraintViolationWithTemplate(message).addPropertyNode(fieldIban)
              .addConstraintViolation();
        }
        if (!bicCodeMatches) {
          context.buildConstraintViolationWithTemplate(message).addPropertyNode(fieldBic)
              .addConstraintViolation();
        }
        if (!bicIbanMatches) {
          context.buildConstraintViolationWithTemplate(messageWrongBic).addPropertyNode(fieldBic)
              .addConstraintViolation();
        }
        return false;
      } else {
        // wrong format, should be handled by other validators
        return true;
      }
    } catch (final Exception ignore) {
      return false;
    }
  }
}

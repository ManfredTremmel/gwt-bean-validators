/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.impl;

import de.knightsoftnet.validators.shared.Password;

import org.apache.commons.lang3.StringUtils;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

/**
 * Check passwords if they fulfill some complexity rules.
 * <ul>
 * <li>lowercase</li>
 * <li>upercase</li>
 * <li>digits</li>
 * <li>special character</li>
 * </ul>
 * Using <code>blacklist</code> you can give a array of words which are not allowed to be part of
 * the password. Default is no entry.<br>
 * Using <code>disalowedStartChars</code> you can define an array of characters (String) which are
 * not allowed as first character in the password. Default is no entry.<br>
 * With <code>maxRepeatChar</code> you can limit the repeat of a single character, default is 0
 * which means no limitation.<br>
 * size limits should be added by separate size annotation.
 *
 * @author Manfred Tremmel
 *
 */
public class PasswordValidator implements ConstraintValidator<Password, Object> {

  /**
   * patterns to check.
   */
  private static final String[] PATTERNS = new String[] {"[a-z]", "[A-Z]", "[0-9]", "[^\\s]"};

  /**
   * localized message if blacklisted.
   */
  private String messageBlacklist;

  /**
   * localized message if start character is not allowed.
   */
  private String messageStartCharacters;

  /**
   * localized message if maximum repeat of a char is reached.
   */
  private String messageMaxRepeat;

  /**
   * minimum number rules that have to be fulfilled.
   */
  private int minRules;

  /**
   * Comma separated list of words which are not allowed as part of the password.
   */
  private List<String> blacklist;

  /**
   * Characters which are not allowed at the beginning of a password.
   */
  private List<String> disalowedStartChars;

  /**
   * maximum repeats of a single character in a row.
   */
  private int maxRepeatChar;

  /**
   * {@inheritDoc} initialize the validator.
   *
   * @see jakarta.validation.ConstraintValidator#initialize(java.lang.annotation.Annotation)
   */
  @Override
  public final void initialize(final Password constraintAnnotation) {
    messageBlacklist = constraintAnnotation.messageBlacklist();
    messageStartCharacters = constraintAnnotation.messageStartCharacters();
    messageMaxRepeat = constraintAnnotation.messageMaxRepeat();
    minRules = constraintAnnotation.minRules();
    if (constraintAnnotation.blacklist() == null || constraintAnnotation.blacklist().length == 0) {
      blacklist = Collections.emptyList();
    } else if (constraintAnnotation.blacklist().length == 1
        && StringUtils.contains(constraintAnnotation.blacklist()[0], ',')) {
      blacklist = Stream.of(StringUtils.split(constraintAnnotation.blacklist()[0], ','))
          .filter(entry -> StringUtils.isNotEmpty(entry)).map(entry -> entry.trim().toLowerCase())
          .collect(Collectors.toList());
    } else {
      blacklist =
          Stream.of(constraintAnnotation.blacklist()).filter(entry -> StringUtils.isNotEmpty(entry))
              .map(entry -> entry.trim().toLowerCase()).collect(Collectors.toList());
    }
    if (constraintAnnotation.disalowedStartChars() == null
        || constraintAnnotation.disalowedStartChars().length == 0) {
      disalowedStartChars = null;
    } else if (constraintAnnotation.disalowedStartChars().length == 1
        && StringUtils.length(constraintAnnotation.disalowedStartChars()[0]) > 1) {
      disalowedStartChars = Stream.of(constraintAnnotation.disalowedStartChars()[0].toCharArray())
          .map(character -> String.valueOf(character)).collect(Collectors.toList());
    } else {
      disalowedStartChars = List.of(constraintAnnotation.disalowedStartChars());
    }
    maxRepeatChar = constraintAnnotation.maxRepeatChar();
  }

  /**
   * {@inheritDoc} check if given object is valid.
   *
   * @see jakarta.validation.ConstraintValidator#isValid(Object,
   *      jakarta.validation.ConstraintValidatorContext)
   */
  @Override
  public final boolean isValid(final Object value, final ConstraintValidatorContext context) {
    final String valueAsString = Objects.toString(value, null);
    return StringUtils.isEmpty(valueAsString)
        || countCriteriaMatches(valueAsString) >= minRules && !isBlacklist(context, valueAsString)
            && !startsWithDisalowedCharacter(context, valueAsString)
            && !maxRepeatCharacterExceded(context, valueAsString);
  }

  private int countCriteriaMatches(final String password) {
    String workPassword = password;
    int fulFilledCriterias = 0;
    for (final String pattern : PATTERNS) {
      if (workPassword.matches(".*" + pattern + ".*")) {
        fulFilledCriterias += 1;
        workPassword = workPassword.replaceAll(pattern, StringUtils.EMPTY);
      }
    }
    return fulFilledCriterias;
  }

  private boolean isBlacklist(final ConstraintValidatorContext context,
      final String valueAsString) {
    if (!blacklist.isEmpty()) {
      final String valueLowerCase = valueAsString.toLowerCase();
      for (final String blacklistEntry : blacklist) {
        if (valueLowerCase.contains(blacklistEntry)) {
          context.disableDefaultConstraintViolation();
          context.buildConstraintViolationWithTemplate(messageBlacklist).addConstraintViolation();
          return true;
        }
      }
    }
    return false;
  }

  private boolean startsWithDisalowedCharacter(final ConstraintValidatorContext context,
      final String valueAsString) {
    if (disalowedStartChars != null) {
      for (final String startChar : disalowedStartChars) {
        if (StringUtils.startsWith(valueAsString, startChar)) {
          context.disableDefaultConstraintViolation();
          context.buildConstraintViolationWithTemplate(messageStartCharacters)
              .addConstraintViolation();
          return true;
        }
      }
    }
    return false;
  }

  private boolean maxRepeatCharacterExceded(final ConstraintValidatorContext context,
      final String valueAsString) {
    if (maxRepeatChar <= 1) {
      return false;
    }
    int currentCount = 0;
    char oldChar = Character.MIN_VALUE;
    for (final char singleChar : valueAsString.toLowerCase().toCharArray()) {
      if (singleChar == oldChar) {
        currentCount++;
        if (currentCount >= maxRepeatChar) {
          context.disableDefaultConstraintViolation();
          context.buildConstraintViolationWithTemplate(messageMaxRepeat).addConstraintViolation();
          return true;
        }
      } else {
        currentCount = 0;
        oldChar = singleChar;
      }
    }
    return false;
  }
}

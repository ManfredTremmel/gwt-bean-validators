/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.impl;

import de.knightsoftnet.validators.shared.SizeWithoutSeparators;

import jakarta.validation.ConstraintValidatorContext;

/**
 * Check if a field's size has one of the two given sizes.
 *
 * @author Manfred Tremmel
 *
 */
public class SizeWithoutSeparatorsValidator
    extends AbstractSizeWithoutSeparatorsValidator<SizeWithoutSeparators> {

  /**
   * size the element must be higher or equal to.
   */
  private int min;

  /**
   * size the element must be lower or equal to.
   */
  private int max;

  /**
   * {@inheritDoc} initialize the validator.
   *
   * @see jakarta.validation.ConstraintValidator#initialize(java.lang.annotation.Annotation)
   */
  @Override
  public final void initialize(final SizeWithoutSeparators sizeWithoutSeparatorsAnnotation) {
    min = sizeWithoutSeparatorsAnnotation.min();
    max = sizeWithoutSeparatorsAnnotation.max();
    ignoreWhiteSpaces = sizeWithoutSeparatorsAnnotation.ignoreWhiteSpaces();
    ignoreMinus = sizeWithoutSeparatorsAnnotation.ignoreMinus();
    ignoreSlashes = sizeWithoutSeparatorsAnnotation.ignoreSlashes();
    validateParameters();
  }

  /**
   * {@inheritDoc} check the given object.
   *
   * @see jakarta.validation.ConstraintValidator#isValid(Object,
   *      jakarta.validation.ConstraintValidatorContext)
   */
  @Override
  public final boolean isValid(final Object value, final ConstraintValidatorContext context) {
    final int length = stringSizeWithoutParameters(value);
    return length < 0 || length >= min && length <= max;
  }

  /**
   * check validity of the the parameters.
   */
  private void validateParameters() {
    if (min < 0) {
      throw new IllegalArgumentException("The min parameter cannot be negative.");
    }
    if (max < 0) {
      throw new IllegalArgumentException("The max parameter cannot be negative.");
    }
    if (max < min) {
      throw new IllegalArgumentException("The length cannot be negative.");
    }
  }
}

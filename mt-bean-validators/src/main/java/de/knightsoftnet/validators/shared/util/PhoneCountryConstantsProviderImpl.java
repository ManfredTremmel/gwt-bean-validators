/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.util;

import de.knightsoftnet.validators.shared.data.CreatePhoneCountryConstantsClass;
import de.knightsoftnet.validators.shared.data.PhoneCountrySharedConstants;

import java.util.Locale;

/**
 * utility class for phone country constants provider, using CreatePhoneCountryConstantsClass as
 * source.
 *
 * @author Manfred Tremmel
 *
 */
public class PhoneCountryConstantsProviderImpl implements PhoneCountryConstantsProvider {

  @Override
  public PhoneCountrySharedConstants getPhoneCountryConstants() {
    return CreatePhoneCountryConstantsClass.create();
  }

  @Override
  public PhoneCountrySharedConstants getPhoneCountryConstants(final Locale locale) {
    return CreatePhoneCountryConstantsClass.create(locale);
  }

  @Override
  public void setPhoneCountrySharedConstantsWhenAvailable(
      final HasSetPhoneCountrySharedConstants receiver) {
    receiver.setPhoneCountrySharedConstants(getPhoneCountryConstants());
  }

  @Override
  public void setPhoneCountrySharedConstantsWhenAvailable(final Locale locale,
      final HasSetPhoneCountrySharedConstants receiver) {
    receiver.setPhoneCountrySharedConstants(getPhoneCountryConstants(locale));
  }

  @Override
  public boolean hasPhoneCountryConstants() {
    return true;
  }
}

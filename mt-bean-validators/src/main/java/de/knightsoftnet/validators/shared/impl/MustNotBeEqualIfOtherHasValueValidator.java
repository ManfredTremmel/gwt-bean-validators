/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.impl;

import de.knightsoftnet.validators.shared.MustNotBeEqualIfOtherHasValue;

import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.Objects;

/**
 * Check if two field entries are not equal.
 *
 * @author Manfred Tremmel
 *
 */
public class MustNotBeEqualIfOtherHasValueValidator
    extends AbstractCompareFieldsIfOtherHasValueValidator<MustNotBeEqualIfOtherHasValue> {

  /**
   * {@inheritDoc} initialize the validator.
   *
   * @see jakarta.validation.ConstraintValidator#initialize(java.lang.annotation.Annotation)
   */
  @Override
  public final void initialize(
      final MustNotBeEqualIfOtherHasValue mustNotBeEqualIfOtherHasValueAnnotation) {
    message = mustNotBeEqualIfOtherHasValueAnnotation.message();
    field1Name = mustNotBeEqualIfOtherHasValueAnnotation.field1();
    field2Name = mustNotBeEqualIfOtherHasValueAnnotation.field2();
    fieldCompareName = mustNotBeEqualIfOtherHasValueAnnotation.fieldCompare();
    valueCompare = Arrays.asList(mustNotBeEqualIfOtherHasValueAnnotation.valueCompare());
    addErrorToField1 = mustNotBeEqualIfOtherHasValueAnnotation.addErrorToField1();
    addErrorToField2 = mustNotBeEqualIfOtherHasValueAnnotation.addErrorToField2();
  }

  @Override
  protected boolean comparissonIsValid(final Object fieldFirstValue,
      final Object fieldSecondValue) {
    if ((fieldFirstValue == null || fieldFirstValue instanceof final String fieldFirstString
        && StringUtils.isEmpty(fieldFirstString))
        && (fieldSecondValue == null || fieldSecondValue instanceof final String fieldSecondString
            && StringUtils.isEmpty(fieldSecondString))) {
      return true;
    }
    return !Objects.equals(fieldFirstValue, fieldSecondValue);
  }
}

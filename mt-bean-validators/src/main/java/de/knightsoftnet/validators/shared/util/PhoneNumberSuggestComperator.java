/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.util;

import de.knightsoftnet.validators.shared.data.PhoneNumberData;

import org.apache.commons.lang3.StringUtils;

import java.util.Comparator;
import java.util.Objects;

/**
 * comperator to sort phone numbers for suggestion entries. sort order is:
 * <ul>
 * <li>country code, shorter entries are sorted in first, same length entries are sorted by alphabet
 * </li>
 * <li>area code, shorter entries are sorted in first, same length entries are sorted by alphabet
 * </li>
 * <li>line number, sorted by alphabet</li>
 * <li>extension, sorted by alphabet</li>
 * </ul>
 *
 * @author Manfred Tremmel
 */
public class PhoneNumberSuggestComperator implements Comparator<PhoneNumberData> {

  @Override
  public int compare(final PhoneNumberData arg1, final PhoneNumberData arg2) {
    if (Objects.equals(arg1, arg2)) {
      return 0;
    }
    if (arg1 == null) {
      return -1;
    }
    if (arg2 == null) {
      return 1;
    }
    if (StringUtils.equals(arg1.getCountryCode(), arg2.getCountryCode())) {
      if (StringUtils.equals(arg1.getAreaCode(), arg2.getAreaCode())) {
        if (StringUtils.equals(arg1.getLineNumber(), arg2.getLineNumber())) {
          return StringUtils.defaultString(arg1.getExtension())
              .compareTo(StringUtils.defaultString(arg2.getExtension()));
        }
        return StringUtils.defaultString(arg1.getLineNumber())
            .compareTo(StringUtils.defaultString(arg2.getLineNumber()));
      }
      if (StringUtils.length(arg1.getAreaCode()) == StringUtils.length(arg2.getAreaCode())) {
        return arg1.getAreaCode().compareTo(arg2.getAreaCode());
      }
      return StringUtils.length(arg1.getAreaCode()) - StringUtils.length(arg2.getAreaCode());
    }
    if (StringUtils.length(arg1.getCountryCode()) == StringUtils.length(arg2.getCountryCode())) {
      return arg1.getCountryCode().compareTo(arg2.getCountryCode());
    }
    return StringUtils.length(arg1.getCountryCode()) - StringUtils.length(arg2.getCountryCode());
  }
}

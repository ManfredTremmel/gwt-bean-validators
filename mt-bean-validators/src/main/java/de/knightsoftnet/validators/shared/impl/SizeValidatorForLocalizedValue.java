/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.impl;

import de.knightsoftnet.validators.shared.interfaces.LocalizedValue;

import org.hibernate.validator.internal.util.logging.Log;
import org.hibernate.validator.internal.util.logging.LoggerFactory;

import java.lang.invoke.MethodHandles;
import java.util.Map.Entry;
import java.util.Objects;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import jakarta.validation.constraints.Size;

public class SizeValidatorForLocalizedValue
    implements ConstraintValidator<Size, LocalizedValue<?, ?>> {

  private static final Log LOG = LoggerFactory.make(MethodHandles.lookup());

  private int min;
  private int max;
  private String message;

  @Override
  public void initialize(final Size parameters) {
    min = parameters.min();
    max = parameters.max();
    message = parameters.message();
    validateParameters();
  }

  /**
   * Checks the length of the specified localized value.
   *
   * @param localizedValue The character sequence to validate.
   * @param constraintValidatorContext context in which the constraint is evaluated.
   *
   * @return Returns {@code true} if the string is {@code null} or the length of
   *         {@code charSequence} between the specified {@code min} and {@code max} values
   *         (inclusive), {@code false} otherwise.
   */
  @Override
  public boolean isValid(final LocalizedValue<?, ?> localizedValue,
      final ConstraintValidatorContext constraintValidatorContext) {
    if (localizedValue == null || localizedValue.getLocalizedText() == null) {
      return true;
    }
    final long numInvalid = localizedValue.getLocalizedText().entrySet().stream()
        .filter(entry -> !checkValue(entry, constraintValidatorContext)).count();
    return numInvalid == 0L;
  }

  private boolean checkValue(final Entry<?, ?> entry,
      final ConstraintValidatorContext constraintValidatorContext) {
    if (entry.getValue() == null) {
      return true;
    }
    final int length = Objects.toString(entry.getValue()).length();
    final boolean valid = length >= min && length <= max;
    if (!valid) {
      constraintValidatorContext.disableDefaultConstraintViolation();
      constraintValidatorContext.buildConstraintViolationWithTemplate(message)
          .addPropertyNode("localizedText[" + entry.getKey() + "]").addPropertyNode("<map value>")
          .addConstraintViolation();
    }
    return valid;
  }

  private void validateParameters() {
    if (min < 0) {
      throw LOG.getMinCannotBeNegativeException();
    }
    if (max < 0) {
      throw LOG.getMaxCannotBeNegativeException();
    }
    if (max < min) {
      throw LOG.getLengthCannotBeNegativeException();
    }
  }
}

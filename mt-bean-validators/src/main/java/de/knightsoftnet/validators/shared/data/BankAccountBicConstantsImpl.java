/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.data;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class BankAccountBicConstantsImpl implements BankAccountBicSharedConstants, Serializable {

  private static final long serialVersionUID = 296812536528436305L;

  private final Map<CountryBankAccountData, String> bankAccountBicMap;

  public BankAccountBicConstantsImpl() {
    super();
    bankAccountBicMap = new HashMap<>();
  }

  public BankAccountBicConstantsImpl(final Map<CountryBankAccountData, String> map) {
    super();
    bankAccountBicMap = map;
  }

  @Override
  public Map<CountryBankAccountData, String> getBankAccountBicMap() {
    return bankAccountBicMap;
  }

  /**
   * setter for bank account big map.
   *
   * @param bankAccountBicMap map to set
   */
  public void setBankAccountBicMap(final Map<CountryBankAccountData, String> bankAccountBicMap) {
    this.bankAccountBicMap.clear();
    if (bankAccountBicMap != null) {
      this.bankAccountBicMap.putAll(bankAccountBicMap);
    }
  }

  @Override
  public void addBankAccounts(final CountryEnum country, final Map<String, String> map) {
    bankAccountBicMap.putAll(map.entrySet().stream().collect(Collectors.toMap(

        entry -> new CountryBankAccountData(country, entry.getKey()), //
        entry -> entry.getValue())));
  }
}

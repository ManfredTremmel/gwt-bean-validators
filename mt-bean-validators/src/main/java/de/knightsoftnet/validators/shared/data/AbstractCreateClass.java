/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.data;

import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Read gwt constants from properties file, server and client side shared parts.
 *
 * @author Manfred Tremmel
 *
 */
public abstract class AbstractCreateClass { // NOPMD, can't include abstract static methods

  private AbstractCreateClass() {
    super();
  }

  protected static PhoneCountryConstantsImpl createPhoneCountryConstants(final Locale locale) {
    final Map<String, String> phoneCountryNames =
        CreatePhoneCountryConstantsClass.readPhoneCountryNames(locale);
    final Map<String, String> phoneCountryCodes =
        CreatePhoneCountryConstantsClass.readPhoneCountryCodes(locale);
    final Set<PhoneCountryCodeData> countryCodeData =
        readPhoneCountryProperties(locale, phoneCountryNames, phoneCountryCodes);
    return new PhoneCountryConstantsImpl(countryCodeData,
        createMapFromPhoneCountry(locale, countryCodeData, phoneCountryNames, phoneCountryCodes));
  }

  protected static Set<PhoneCountryCodeData> readPhoneCountryProperties(final Locale locale,
      final Map<String, String> phoneCountryNames, final Map<String, String> phoneCountryCodes) {
    return phoneCountryCodes.entrySet().stream().map(country -> {
      final PhoneCountryCodeData countryEntry =
          new PhoneCountryCodeData(country.getKey(), phoneCountryNames.get(country.getValue()));
      CreatePhoneCountryConstantsClass.readPhoneRegionCodes(country.getKey(), locale).entrySet()
          .stream().forEachOrdered(
              region -> countryEntry.addAreaCodeData(new PhoneAreaCodeData(region.getKey(),
                  region.getValue(), StringUtils.length(country.getKey()))));
      return countryEntry;
    }).collect(Collectors.toSet());
  }

  protected static Map<String, PhoneCountryData> createMapFromPhoneCountry(final Locale locale,
      final Set<PhoneCountryCodeData> countries, final Map<String, String> phoneCountryNames,
      final Map<String, String> phoneCountryCodes) {
    final Map<String, PhoneCountryData> countryPhoneMap = new HashMap<>();
    final Map<String, String> phoneTrunkAndExitCodes =
        CreatePhoneCountryConstantsClass.readPhoneTrunkAndExitCodes(locale);

    countries.stream().forEachOrdered(entry -> {
      final String countryCode = phoneCountryCodes.get(entry.getCountryCode());
      if (StringUtils.contains(countryCode, '-')) {
        List.of(StringUtils.split(countryCode, '-')).stream().forEachOrdered(
            singleCountryCode -> entry.setPhoneCountryData(addCountryToPhoneMap(phoneCountryNames,
                countryPhoneMap, phoneTrunkAndExitCodes, entry, singleCountryCode))

        );
      } else {
        entry.setPhoneCountryData(addCountryToPhoneMap(phoneCountryNames, countryPhoneMap,
            phoneTrunkAndExitCodes, entry, countryCode));
      }
    });
    return countryPhoneMap;
  }

  protected static PhoneCountryData addCountryToPhoneMap(
      final Map<String, String> phoneCountryNames,
      final Map<String, PhoneCountryData> countryPhoneMap,
      final Map<String, String> phoneTrunkAndExitCodes, final PhoneCountryCodeData entry,
      final String countryCode) {
    final String trunkAndExitCodes = phoneTrunkAndExitCodes.get(countryCode);
    final String countryCodeName = phoneCountryNames.get(countryCode);
    final String[] splittedTrunkAndExitCodes =
        StringUtils.defaultString(trunkAndExitCodes).split(",");
    final String trunkCode =
        splittedTrunkAndExitCodes.length >= 1 ? splittedTrunkAndExitCodes[0] : StringUtils.EMPTY;
    final String exitCode =
        splittedTrunkAndExitCodes.length >= 2 ? splittedTrunkAndExitCodes[1] : StringUtils.EMPTY;
    final boolean areaCodeMustBeFilled =
        splittedTrunkAndExitCodes.length >= 3 && "1".equals(splittedTrunkAndExitCodes[2]);
    final PhoneCountryData countryData = new PhoneCountryData(countryCode, countryCodeName,
        trunkCode, exitCode, areaCodeMustBeFilled, entry);
    countryPhoneMap.put(countryCode, countryData);
    return countryData;
  }
}

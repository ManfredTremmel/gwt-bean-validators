/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.data;

import de.knightsoftnet.validators.shared.annotation.ReadProperties;

/**
 * Read constants from properties file.
 *
 * @author Manfred Tremmel
 *
 */
public class CreateTaxNumberMapConstantsClass {

  @ReadProperties(propertyName = "TaxNumberMapConstants")
  public interface TaxNumberMapConstantsProperties extends PropertiesMapConstants {
  }

  private static volatile TaxNumberMapConstantsImpl taxNumberMapConstants = null;

  /**
   * Instantiates a class via deferred binding.
   *
   * @return the new instance, which must be cast to the requested class
   */
  public static TaxNumberMapSharedConstants create() {
    if (taxNumberMapConstants == null) { // NOPMD it's thread save!
      synchronized (TaxNumberMapConstantsImpl.class) {
        if (taxNumberMapConstants == null) {
          final TaxNumberMapConstantsProperties provider =
              new TaxNumberMapConstantsPropertiesImpl();
          taxNumberMapConstants = new TaxNumberMapConstantsImpl(provider.properties());
        }
      }
    }
    return taxNumberMapConstants;
  }
}

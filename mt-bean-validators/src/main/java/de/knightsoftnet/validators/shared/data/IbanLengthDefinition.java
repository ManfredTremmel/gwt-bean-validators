/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.data;

import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;

public class IbanLengthDefinition implements Serializable {
  private static final long serialVersionUID = 5381709598709280335L;
  private int length;
  private int bankNumberStart;
  private int bankNumberEnd;
  private int accountNumberStart;
  private int accountNumberEnd;

  /**
   * default constructor.
   */
  public IbanLengthDefinition() {
    super();
  }

  /**
   * constructor initializing data.
   *
   * @param length length of the iban
   * @param bankNumberStart bank number start position
   * @param bankNumberEnd bank number end position
   * @param accountNumberStart account number start position
   * @param accountNumberEnd account number end position
   */
  public IbanLengthDefinition(final int length, final int bankNumberStart, final int bankNumberEnd,
      final int accountNumberStart, final int accountNumberEnd) {
    super();
    this.length = length;
    this.bankNumberStart = bankNumberStart;
    this.bankNumberEnd = bankNumberEnd;
    this.accountNumberStart = accountNumberStart;
    this.accountNumberEnd = accountNumberEnd;
  }

  public static IbanLengthDefinition of(final int length, final int bankNumberStart,
      final int bankNumberEnd, final int accountNumberStart, final int accountNumberEnd) {
    return new IbanLengthDefinition(length, bankNumberStart, bankNumberEnd, accountNumberStart,
        accountNumberEnd);
  }

  /**
   * constructor parsing value from string.
   *
   * @param pvalueToParse semicolon separated string with values
   */
  public IbanLengthDefinition(final String pvalueToParse) {
    super();
    final String[] splittedValue = StringUtils.split(pvalueToParse, ';');
    if (splittedValue != null) {
      if (splittedValue.length >= 5) {
        accountNumberEnd = Integer.parseInt(splittedValue[4]);
      }
      if (splittedValue.length >= 4) {
        accountNumberStart = Integer.parseInt(splittedValue[3]);
      }
      if (splittedValue.length >= 3) {
        bankNumberEnd = Integer.parseInt(splittedValue[2]);
      }
      if (splittedValue.length >= 2) {
        bankNumberStart = Integer.parseInt(splittedValue[1]);
      }
      if (splittedValue.length >= 1) {
        length = Integer.parseInt(splittedValue[0]);
      }
    }
  }

  public int getLength() {
    return length;
  }

  public void setLength(final int length) {
    this.length = length;
  }

  public int getBankNumberStart() {
    return bankNumberStart;
  }

  public void setBankNumberStart(final int bankNumberStart) {
    this.bankNumberStart = bankNumberStart;
  }

  public int getBankNumberEnd() {
    return bankNumberEnd;
  }

  public void setBankNumberEnd(final int bankNumberEnd) {
    this.bankNumberEnd = bankNumberEnd;
  }

  public int getAccountNumberStart() {
    return accountNumberStart;
  }

  public void setAccountNumberStart(final int accountNumberStart) {
    this.accountNumberStart = accountNumberStart;
  }

  public final int getAccountNumberEnd() {
    return accountNumberEnd;
  }

  public void setAccountNumberEnd(final int accountNumberEnd) {
    this.accountNumberEnd = accountNumberEnd;
  }
}

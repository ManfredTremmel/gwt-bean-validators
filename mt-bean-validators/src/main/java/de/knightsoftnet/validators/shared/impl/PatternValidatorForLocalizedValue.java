/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.impl;

import org.hibernate.validator.constraintvalidation.HibernateConstraintValidatorContext;
import org.hibernate.validator.internal.engine.messageinterpolation.util.InterpolationHelper;
import org.hibernate.validator.internal.util.logging.Log;
import org.hibernate.validator.internal.util.logging.LoggerFactory;

import java.lang.invoke.MethodHandles;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.PatternSyntaxException;

import jakarta.validation.ConstraintValidatorContext;
import jakarta.validation.constraints.Pattern;

/**
 * pattern validator.
 *
 * @author Manfred Tremmel
 */
public class PatternValidatorForLocalizedValue extends AbstractValidatorForLocalizedValue<Pattern> {


  private static final Log LOG = LoggerFactory.make(MethodHandles.lookup());

  private java.util.regex.Pattern pattern;
  private String escapedRegexp;


  @Override
  public void initialize(final Pattern constraintAnnotation) {
    message = constraintAnnotation.message();
    final Pattern.Flag[] flags = constraintAnnotation.flags();
    int intFlag = 0;
    for (final Pattern.Flag flag : flags) {
      intFlag = intFlag | flag.getValue();
    }

    try {
      pattern = java.util.regex.Pattern.compile(constraintAnnotation.regexp(), intFlag);
    } catch (final PatternSyntaxException e) {
      throw LOG.getInvalidRegularExpressionException(e);
    }

    escapedRegexp = InterpolationHelper.escapeMessageParameter(constraintAnnotation.regexp());
  }

  @Override
  protected void extendValidationContext(
      final ConstraintValidatorContext constraintValidatorContext) {
    if (constraintValidatorContext instanceof HibernateConstraintValidatorContext) {
      constraintValidatorContext.unwrap(HibernateConstraintValidatorContext.class)
          .addMessageParameter("regexp", escapedRegexp);
    }
  }

  @Override
  protected boolean checkEntryValue(final Object value) {
    final Matcher matcher = pattern.matcher(Objects.toString(value, null));
    return matcher.matches();
  }
}

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.interfaces;

import java.util.Map;

/**
 * localized text definition.
 *
 * @param <K> key type
 * @param <V> value type
 * @author Manfred Tremmel
 */
public interface LocalizedValue<K, V> {

  /**
   * set localized text.
   *
   * @param localizedText map of localized texts
   */
  void setLocalizedText(final Map<K, V> localizedText);

  /**
   * add or update a text for the given language.
   *
   * @param language the language for the text
   * @param text localized text
   */
  void putLocalizedText(final K language, final V text);

  /**
   * get map of localized texts.
   *
   * @return map with languages and corresponding texts
   */
  Map<K, V> getLocalizedText();

  /**
   * get localized text for specific language.
   *
   * @param language language for which the text should be retuned
   * @return text for the language or zero
   */
  V getLocalizedText(final K language);
}

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.data;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.Objects;

/**
 * country and bank account data.
 *
 * @author Manfred Tremmel
 *
 */
public class CountryBankAccountData implements Comparable<CountryBankAccountData>, Serializable {

  private static final long serialVersionUID = -7696337189703736512L;
  private CountryEnum countryCode;
  private String bankAccount;

  /**
   * default constructor.
   */
  public CountryBankAccountData() {
    this(null, null);
  }

  /**
   * constructor initializing fields.
   *
   * @param countryCode country code
   * @param bankAccount bank account
   */
  public CountryBankAccountData(final CountryEnum countryCode, final String bankAccount) {
    super();
    this.countryCode = countryCode;
    this.bankAccount = bankAccount;
  }

  public CountryEnum getCountryCode() {
    return countryCode;
  }

  public void setCountryCode(final CountryEnum countryCode) {
    this.countryCode = countryCode;
  }

  public String getBankAccount() {
    return bankAccount;
  }

  public void setBankAccount(final String bankAccount) {
    this.bankAccount = bankAccount;
  }

  @Override
  public int hashCode() {
    return Objects.hash(countryCode, bankAccount);
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (this.getClass() != obj.getClass()) {
      return false;
    }
    final CountryBankAccountData other = (CountryBankAccountData) obj;
    return countryCode == other.countryCode //
        && StringUtils.equals(bankAccount, other.bankAccount);
  }

  @Override
  public int compareTo(final CountryBankAccountData compare) {
    if (equals(compare)) {
      return 0;
    }
    if (compare == null) {
      return 1;
    }
    final int countryCompare = ObjectUtils.compare(countryCode, compare.countryCode);
    if (countryCompare != 0) {
      return countryCompare;
    }
    return StringUtils.compare(bankAccount, compare.bankAccount);
  }
}

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.data;

import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.Objects;

/**
 * phone number country data.
 *
 * @author Manfred Tremmel
 *
 */
public class PhoneCountryData implements Serializable {
  private static final long serialVersionUID = 4015869056935311929L;
  private String countryCode;
  private String countryCodeName;
  private String trunkCode;
  private String exitCode;
  private boolean areaCodeMustBeFilled;
  private PhoneCountryCodeData countryCodeData;


  public PhoneCountryData() {
    super();
  }

  /**
   * constructor initializing fields.
   *
   * @param countryCode country iso code
   * @param countryCodeName country name
   * @param trunkCode trunk code (used for country internal calls)
   * @param exitCode exit code (used for international calls
   * @param areaCodeMustBeFilled true if area code must be filled in this country
   * @param countryCodeData country code data
   */
  public PhoneCountryData(final String countryCode, final String countryCodeName,
      final String trunkCode, final String exitCode, final boolean areaCodeMustBeFilled,
      final PhoneCountryCodeData countryCodeData) {
    super();
    this.countryCode = countryCode;
    this.countryCodeName = countryCodeName;
    this.trunkCode = trunkCode;
    this.exitCode = exitCode;
    this.areaCodeMustBeFilled = areaCodeMustBeFilled;
    this.countryCodeData = countryCodeData;
  }

  public String getCountryCode() {
    return countryCode;
  }

  public String getCountryCodeName() {
    return countryCodeName;
  }

  public String getTrunkCode() {
    return trunkCode;
  }

  public String getExitCode() {
    return exitCode;
  }

  public boolean isAreaCodeMustBeFilled() {
    return areaCodeMustBeFilled;
  }

  public PhoneCountryCodeData getCountryCodeData() {
    return countryCodeData;
  }

  public void setCountryCode(final String countryCode) {
    this.countryCode = countryCode;
  }

  public void setCountryCodeName(final String countryCodeName) {
    this.countryCodeName = countryCodeName;
  }

  public void setTrunkCode(final String trunkCode) {
    this.trunkCode = trunkCode;
  }

  public void setExitCode(final String exitCode) {
    this.exitCode = exitCode;
  }

  public void setAreaCodeMustBeFilled(final boolean areaCodeMustBeFilled) {
    this.areaCodeMustBeFilled = areaCodeMustBeFilled;
  }

  public void setCountryCodeData(final PhoneCountryCodeData countryCodeData) {
    this.countryCodeData = countryCodeData;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(countryCode);
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (this.getClass() != obj.getClass()) {
      return false;
    }
    final PhoneCountryData other = (PhoneCountryData) obj;
    return StringUtils.equals(countryCode, other.countryCode);
  }
}

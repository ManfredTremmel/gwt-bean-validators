/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.impl;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.routines.checkdigit.EAN13CheckDigit;
import org.apache.commons.validator.routines.checkdigit.ISBN10CheckDigit;

import java.lang.annotation.Annotation;
import java.util.Objects;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

/**
 * Abstract check a string if it's a valid ISBN.
 *
 * @param <A> the annotation type handled by an implementation
 *
 * @author Manfred Tremmel
 *
 */
public abstract class AbstractIsbnValidator<A extends Annotation>
    implements ConstraintValidator<A, Object> {
  /**
   * definition of isbn10 length.
   */
  public static final int ISBN10_LENGTH = 10;

  /**
   * definition of isbn13 length.
   */
  public static final int ISBN13_LENGTH = 13;

  /**
   * apache commons class to check/calculate ISBN10 check sums.
   */
  protected static final ISBN10CheckDigit CHECK_ISBN10 = new ISBN10CheckDigit();

  /**
   * apache commons class to check/calculate GLN/EAN13 check sums (it's the same for isbn13).
   */
  protected static final EAN13CheckDigit CHECK_ISBN13 = new EAN13CheckDigit();

  /**
   * should separating minus signs be ignored (true/false).
   */
  protected boolean ignoreSeparators;

  /**
   * should isbn10 be checked (true/false).
   */
  protected boolean checkIsbn10;

  /**
   * should isbn13 be checked (true/false).
   */
  protected boolean checkIsbn13;

  /**
   * {@inheritDoc} check if given string is a valid isbn.
   *
   * @see jakarta.validation.ConstraintValidator#isValid(java.lang.Object,
   *      jakarta.validation.ConstraintValidatorContext)
   */
  @Override
  public boolean isValid(final Object value, final ConstraintValidatorContext context) {
    final String valueAsString;
    if (ignoreSeparators) {
      valueAsString =
          Objects.toString(value, StringUtils.EMPTY).replaceAll("-", StringUtils.EMPTY);
    } else {
      valueAsString = Objects.toString(value, null);
    }
    if (StringUtils.isEmpty(valueAsString)) {
      return true;
    }
    if (!StringUtils.isNumeric(valueAsString)) {
      return false;
    }
    if (checkIsbn10 && valueAsString.length() == ISBN10_LENGTH) {
      // we do have 10 digits, lets test the checksum
      return CHECK_ISBN10.isValid(valueAsString);
    }
    if (checkIsbn13 && valueAsString.length() == ISBN13_LENGTH) {
      // we do have 13 digits, lets test the checksum
      return CHECK_ISBN13.isValid(valueAsString);
    }
    // other sizes are wrong, but this is reported by AlternateSize
    return true;
  }
}

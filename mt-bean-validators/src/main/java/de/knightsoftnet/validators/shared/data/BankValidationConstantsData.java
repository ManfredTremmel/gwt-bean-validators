/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.data;

import de.knightsoftnet.validators.shared.util.BankConstantsProvider;
import de.knightsoftnet.validators.shared.util.HasSetBankAccountBicSharedConstants;
import de.knightsoftnet.validators.shared.util.HasSetBicMapSharedConstants;
import de.knightsoftnet.validators.shared.util.HasSetIbanLengthMapSharedConstants;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

public class BankValidationConstantsData implements BankConstantsProvider, Serializable {
  private static final long serialVersionUID = -1579146529754135205L;
  private IbanLengthMapConstantsImpl ibanLengthMapSharedConstants;
  private List<CountryBankAccountBicData> bankAccountBicConstants;
  private BicMapConstantsImpl bicMapConstants;

  public BankValidationConstantsData() {
    this(null, null, null);
  }

  /**
   * constructor initializing values.
   *
   * @param ibanLengthMapSharedConstants iban length map constants
   * @param bankAccountBicSharedConstants bank account bic constants
   * @param bicMapConstants bic map constants
   */
  public BankValidationConstantsData(final IbanLengthMapConstantsImpl ibanLengthMapSharedConstants,
      final BankAccountBicConstantsImpl bankAccountBicSharedConstants,
      final BicMapConstantsImpl bicMapConstants) {
    super();
    this.ibanLengthMapSharedConstants = ibanLengthMapSharedConstants;
    bankAccountBicConstants = bankAccountBicSharedConstants == null ? null
        : bankAccountBicSharedConstants.getBankAccountBicMap().entrySet().stream().map(

            entry -> new CountryBankAccountBicData(entry.getKey().getCountryCode(),
                entry.getKey().getBankAccount(), entry.getValue()))
            .collect(Collectors.toList());
    this.bicMapConstants = bicMapConstants;
  }

  public IbanLengthMapConstantsImpl getIbanLengthMapSharedConstants() {
    return ibanLengthMapSharedConstants;
  }

  public void setIbanLengthMapSharedConstants(
      final IbanLengthMapConstantsImpl ibanLengthMapSharedConstants) {
    this.ibanLengthMapSharedConstants = ibanLengthMapSharedConstants;
  }

  /**
   * getter for bankAccountBicSharedConstants.
   *
   * @return BankAccountBicConstantsImpl
   */
  @JsonIgnore
  public BankAccountBicConstantsImpl getBankAccountBicSharedConstants() {
    return bankAccountBicConstants == null ? null
        : new BankAccountBicConstantsImpl(bankAccountBicConstants.stream().collect(Collectors.toMap(

            entry -> new CountryBankAccountData(entry.getCountryCode(), entry.getBankAccount()),
            entry -> entry.getBic())));
  }

  public List<CountryBankAccountBicData> getBankAccountBicConstants() {
    return bankAccountBicConstants;
  }

  public void setBankAccountBicConstants(
      final List<CountryBankAccountBicData> bankAccountBicConstants) {
    this.bankAccountBicConstants = bankAccountBicConstants;
  }

  public BicMapConstantsImpl getBicMapConstants() {
    return bicMapConstants;
  }

  public void setBicMapConstants(final BicMapConstantsImpl bicMapConstants) {
    this.bicMapConstants = bicMapConstants;
  }

  @JsonIgnore
  @Override
  public void setIbanLengthMapSharedConstantsWhenAvailable(
      final HasSetIbanLengthMapSharedConstants receiver) {
    receiver.setIbanLengthMapSharedConstants(ibanLengthMapSharedConstants);
  }

  @JsonIgnore
  @Override
  public void setBankAccountBicSharedConstantsWhenAvailable(
      final HasSetBankAccountBicSharedConstants receiver) {
    receiver.setBankAccountBicSharedConstants(getBankAccountBicSharedConstants());
  }

  @JsonIgnore
  @Override
  public void setBicMapSharedConstantsWhenAvailable(final HasSetBicMapSharedConstants receiver) {
    receiver.setBicMapSharedConstants(bicMapConstants);
  }
}

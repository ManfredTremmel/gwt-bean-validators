/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.util;

import de.knightsoftnet.validators.shared.data.BankAccountBicSharedConstants;
import de.knightsoftnet.validators.shared.data.CountryBankAccountData;
import de.knightsoftnet.validators.shared.data.CountryEnum;
import de.knightsoftnet.validators.shared.data.IbanLengthDefinition;
import de.knightsoftnet.validators.shared.data.IbanLengthMapSharedConstants;
import de.knightsoftnet.validators.shared.data.ValueWithPos;

import org.apache.commons.lang3.CharUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * Iban Util, format and compress ibans.
 *
 * @author Manfred Tremmel
 *
 */
public abstract class AbstractIbanUtil implements BankConstantsProvider,
    HasSetIbanLengthMapSharedConstants, HasSetBankAccountBicSharedConstants {

  /**
   * character used for separating blocks.
   */
  public static final char SEPARATOR = ' ';

  protected static final int BLOCK_LENGTH = 4;

  private final BankConstantsProvider bankConstantsProvider;
  private BankAccountBicSharedConstants bankAccountBicSharedConstants;
  private IbanLengthMapSharedConstants ibanLengthMapSharedConstants;

  /**
   * constructor.
   *
   * @param bankConstantsProvider provider for bank account stuff
   */
  protected AbstractIbanUtil(final BankConstantsProvider bankConstantsProvider) {
    super();
    this.bankConstantsProvider = bankConstantsProvider;
    bankConstantsProvider.setBankAccountBicSharedConstantsWhenAvailable(this);
    bankConstantsProvider.setIbanLengthMapSharedConstantsWhenAvailable(this);
  }

  @Override
  public void setBankAccountBicSharedConstants(
      final BankAccountBicSharedConstants bankAccountBicSharedConstants) {
    this.bankAccountBicSharedConstants = bankAccountBicSharedConstants;
  }

  @Override
  public void setIbanLengthMapSharedConstants(
      final IbanLengthMapSharedConstants ibanLengthMapSharedConstants) {
    this.ibanLengthMapSharedConstants = ibanLengthMapSharedConstants;
  }

  /**
   * format iban to four character blocks.
   *
   * @param entry string to format and cursor position
   * @return formated string with new cursor position
   */
  public static ValueWithPos<String> ibanFormatWithPos(final ValueWithPos<String> entry) {
    if (entry == null) {
      return null;
    }
    entry.setOriginalValue(entry.getValue());
    if (StringUtils.isNotEmpty(entry.getValue())) {
      final StringBuilder ibanSb = new StringBuilder(entry.getValue().length());
      int pos = 0;
      int posformated = 0;
      for (final char charCode : entry.getValue().toCharArray()) {
        if (CharUtils.isAsciiAlphaUpper(charCode) || CharUtils.isAsciiNumeric(charCode)) {
          if (pos > 0 && pos % BLOCK_LENGTH == 0) {
            ibanSb.append(SEPARATOR);
            if (posformated <= entry.getPos()) {
              entry.setPos(entry.getPos() + 1);
            }
            posformated++;
          }
          ibanSb.append(charCode);
          pos++;
          posformated++;
        } else {
          if (posformated < entry.getPos()) {
            entry.setPos(entry.getPos() - 1);
          }
        }
      }
      entry.setValue(ibanSb.toString());
      if (entry.getPos() < 0) {
        entry.setPos(0);
      } else if (entry.getPos() >= ibanSb.length()) {
        entry.setPos(ibanSb.length());
      }
    }
    return entry;
  }

  /**
   * format iban to four character blocks.
   *
   * @param string string to format
   * @return formated string
   */
  public static String ibanFormat(final String string) {
    if (string == null) {
      return null;
    }
    final ValueWithPos<String> formatedValue = ibanFormatWithPos(new ValueWithPos<>(string, -1));
    return formatedValue.getValue();
  }

  /**
   * compress iban, remove all blanks inside.
   *
   * @param string string to compress
   * @return iban without spaces
   */
  public static String ibanCompress(final String string) {
    return StringUtils.remove(string, SEPARATOR);
  }

  /**
   * get country of iban.
   *
   * @param string string with iban
   * @return country
   */
  public static CountryEnum getCountryOfIban(final String string) {
    if (string == null || string.length() < 2) {
      return null;
    }
    try {
      return CountryEnum.valueOf(StringUtils.substring(string, 0, 2));
    } catch (final IllegalArgumentException e) {
      return null;
    }
  }

  /**
   * get bank number of iban.
   *
   * @param string string with iban
   * @return bank number
   */
  public String getBankNumberOfIban(final String string) {
    final String compressedIban = ibanCompress(string);
    final CountryEnum country = getCountryOfIban(compressedIban);
    final IbanLengthDefinition length = ibanLengthMapSharedConstants == null ? null
        : ibanLengthMapSharedConstants.getIbanLengthMap().get(country);
    return length == null ? null
        : StringUtils.substring(compressedIban, length.getBankNumberStart(),
            length.getBankNumberEnd());
  }

  /**
   * get account number of iban.
   *
   * @param string string with iban
   * @return account number
   */
  public String getAccountNumberOfIban(final String string) {
    final String compressedIban = ibanCompress(string);
    final CountryEnum country = getCountryOfIban(compressedIban);
    final IbanLengthDefinition length = ibanLengthMapSharedConstants == null ? null
        : ibanLengthMapSharedConstants.getIbanLengthMap().get(country);
    return length == null ? null
        : StringUtils.substring(compressedIban, length.getAccountNumberStart(),
            length.getAccountNumberEnd());
  }

  /**
   * get bic of iban.
   *
   * @param string string with iban
   * @return bic
   */
  public String getBicOfIban(final String string) {
    final String bankNumber = getBankNumberOfIban(string);
    return bankAccountBicSharedConstants == null ? null
        : bankAccountBicSharedConstants.getBankAccountBicMap()
            .get(new CountryBankAccountData(getCountryOfIban(string), bankNumber));
  }

  @Override
  public void setIbanLengthMapSharedConstantsWhenAvailable(
      final HasSetIbanLengthMapSharedConstants receiver) {
    bankConstantsProvider.setIbanLengthMapSharedConstantsWhenAvailable(receiver);
  }

  @Override
  public void setBankAccountBicSharedConstantsWhenAvailable(
      final HasSetBankAccountBicSharedConstants receiver) {
    bankConstantsProvider.setBankAccountBicSharedConstantsWhenAvailable(receiver);
  }

  @Override
  public void setBicMapSharedConstantsWhenAvailable(final HasSetBicMapSharedConstants receiver) {
    bankConstantsProvider.setBicMapSharedConstantsWhenAvailable(receiver);
  }
}

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.impl;

import de.knightsoftnet.validators.shared.AlternateSize;

import jakarta.validation.ConstraintValidatorContext;

/**
 * Check if a field's size has one of the two given sizes.
 *
 * @author Manfred Tremmel
 *
 */
public class AlternateSizeValidator extends AbstractSizeWithoutSeparatorsValidator<AlternateSize> {
  /**
   * first possible size.
   */
  private int size1;
  /**
   * second possible size.
   */
  private int size2;

  /**
   * {@inheritDoc} initialize the validator.
   *
   * @see jakarta.validation.ConstraintValidator#initialize(java.lang.annotation.Annotation)
   */
  @Override
  public final void initialize(final AlternateSize alternateSizeAnnotation) {
    size1 = alternateSizeAnnotation.size1();
    size2 = alternateSizeAnnotation.size2();
    ignoreWhiteSpaces = alternateSizeAnnotation.ignoreWhiteSpaces();
    ignoreMinus = alternateSizeAnnotation.ignoreMinus();
    ignoreSlashes = alternateSizeAnnotation.ignoreSlashes();
    validateParameters();
  }

  /**
   * {@inheritDoc} check if given object is valid.
   *
   * @see jakarta.validation.ConstraintValidator#isValid(Object,
   *      jakarta.validation.ConstraintValidatorContext)
   */
  @Override
  public final boolean isValid(final Object value, final ConstraintValidatorContext context) {
    final int length = stringSizeWithoutParameters(value);
    return length < 0 || length == size1 || length == size2;
  }

  /**
   * check validity of the the parameters.
   */
  private void validateParameters() {
    if (size1 < 0) {
      throw new IllegalArgumentException("The size1 parameter cannot be negative.");
    }
    if (size2 < 0) {
      throw new IllegalArgumentException("The size2 parameter cannot be negative.");
    }
  }
}

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.impl;

import de.knightsoftnet.validators.shared.MustBeSmallerOrEqual;

/**
 * Check if entry of field1 is smaller then field2.
 *
 * @author Manfred Tremmel
 *
 */
public class MustBeSmallerOrEqualValidator
    extends AbstractCompareFieldsValidator<MustBeSmallerOrEqual> {

  /**
   * {@inheritDoc} initialize the validator.
   *
   * @see jakarta.validation.ConstraintValidator#initialize(java.lang.annotation.Annotation)
   */
  @Override
  public final void initialize(final MustBeSmallerOrEqual mustBeSamllerOrEqualAnnotation) {
    message = mustBeSamllerOrEqualAnnotation.message();
    field1Name = mustBeSamllerOrEqualAnnotation.field1();
    field2Name = mustBeSamllerOrEqualAnnotation.field2();
    addErrorToField1 = mustBeSamllerOrEqualAnnotation.addErrorToField1();
    addErrorToField2 = mustBeSamllerOrEqualAnnotation.addErrorToField2();
  }

  @Override
  @SuppressWarnings({"unchecked"})
  protected boolean comparissonIsValid(final Object field1Value, final Object field2Value) {
    if (field1Value == null) {
      return true;
    }
    if (field2Value == null) {
      return false;
    }
    return field1Value instanceof final Comparable comparable
        && comparable.compareTo(field2Value) <= 0;
  }
}

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.impl;

import de.knightsoftnet.validators.shared.Tin;
import de.knightsoftnet.validators.shared.data.CreateTinMapConstantsClass;
import de.knightsoftnet.validators.shared.data.TinMapSharedConstants;
import de.knightsoftnet.validators.shared.util.BeanPropertyReaderUtil;

import org.apache.commons.lang3.StringUtils;

import jakarta.validation.ConstraintValidatorContext;

/**
 * Check if a Tax Identification Number (TIN) field is valid for the selected country.
 *
 * @author Manfred Tremmel
 *
 */
public class TinValidator extends AbstractTaxTinNumberValidator<Tin> {

  /**
   * map of vat id regex values for the different countries.
   */
  private static final TinMapSharedConstants TIN_MAP = CreateTinMapConstantsClass.create();

  /**
   * field name of the vat id field.
   */
  private String fieldTin;

  /**
   * {@inheritDoc} initialize the validator.
   *
   * @see jakarta.validation.ConstraintValidator#initialize(java.lang.annotation.Annotation)
   */
  @Override
  public final void initialize(final Tin constraintAnnotation) {
    message = constraintAnnotation.message();
    fieldCountryCode = constraintAnnotation.fieldCountryCode();
    allowLowerCaseCountryCode = constraintAnnotation.allowLowerCaseCountryCode();
    fieldTin = constraintAnnotation.fieldTin();
  }

  @Override
  public final boolean isValid(final Object value, final ConstraintValidatorContext context) {
    if (value == null) {
      return true;
    }
    try {
      String countryCode =
          BeanPropertyReaderUtil.getNullSaveStringProperty(value, fieldCountryCode);
      if (StringUtils.isEmpty(countryCode)) {
        return true;
      }
      final String Tin = BeanPropertyReaderUtil.getNullSaveStringProperty(value, fieldTin);
      if (StringUtils.isEmpty(Tin)) {
        return true;
      }

      if (allowLowerCaseCountryCode) {
        countryCode = StringUtils.upperCase(countryCode);
      }

      final String regExCheck = TIN_MAP.tins().get(countryCode);
      if (regExCheck == null) {
        return true;
      }
      if (Tin.matches(regExCheck) && checkSumTest(countryCode, Tin)) {
        return true;
      }
      switchContext(context);
      return false;
    } catch (final Exception ignore) {
      switchContext(context);
      return false;
    }
  }

  private void switchContext(final ConstraintValidatorContext context) {
    context.disableDefaultConstraintViolation();
    context.buildConstraintViolationWithTemplate(message).addPropertyNode(fieldTin)
        .addConstraintViolation();
  }

  private boolean checkSumTest(final String countryCode, final String tin) {
    return switch (countryCode) {
      case "AT" -> checkAtNumber(tin);
      case "DE", "HR" -> checkModulo11(tin);
      case "DK" -> checkDk(tin);
      case "EE", "LT" -> checkEe(tin);
      case "ES" -> checkEs(tin);
      case "BA", "ME", "MK" -> checkUniqueMasterCitizenNumber(tin);
      case "NL" -> checkNl(tin);
      case "PL" -> checkPl(tin);
      default -> true; // for other countries, I haven't found checksum rules
    };
  }
}

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.impl;

import de.knightsoftnet.validators.shared.MustBeEqual;

import java.util.Objects;

/**
 * Check if two field entries are equal.
 *
 * @author Manfred Tremmel
 *
 */
public class MustBeEqualValidator extends AbstractCompareFieldsValidator<MustBeEqual> {

  /**
   * {@inheritDoc} initialize the validator.
   *
   * @see jakarta.validation.ConstraintValidator#initialize(java.lang.annotation.Annotation)
   */
  @Override
  @SuppressWarnings("CPD-START")
  public final void initialize(final MustBeEqual mustBeEqualAnnotation) {
    message = mustBeEqualAnnotation.message();
    field1Name = mustBeEqualAnnotation.field1();
    field2Name = mustBeEqualAnnotation.field2();
    addErrorToField1 = mustBeEqualAnnotation.addErrorToField1();
    addErrorToField2 = mustBeEqualAnnotation.addErrorToField2();
  }

  @Override
  protected boolean comparissonIsValid(final Object field1Value, final Object field2Value) {
    return Objects.equals(field1Value, field2Value);
  }
}

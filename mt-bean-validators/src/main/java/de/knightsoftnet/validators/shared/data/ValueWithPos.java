/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.data;

import java.util.Objects;

/**
 * bean with value and cursor position.
 *
 * @author Manfred Tremmel
 *
 * @param <E> Type of value
 */
public class ValueWithPos<E> {
  private E value;
  private E originalValue;
  private int pos;

  /**
   * default constructor.
   */
  public ValueWithPos() {
    this(null, null, 0);
  }

  /**
   * constructor initializing fields.
   *
   * @param value value entry
   * @param pos cursor position
   */
  public ValueWithPos(final E value, final int pos) {
    this(null, value, pos);
  }

  /**
   * constructor initializing fields.
   *
   * @param originalValue value before transformation
   * @param value value entry
   * @param pos cursor position
   */
  public ValueWithPos(final E originalValue, final E value, final int pos) {
    super();
    this.originalValue = originalValue;
    this.value = value;
    this.pos = pos;
  }

  public final E getOriginalValue() {
    return originalValue;
  }

  public final void setOriginalValue(final E originalValue) {
    this.originalValue = originalValue;
  }

  public final E getValue() {
    return this.value;
  }

  public final void setValue(final E value) {
    this.value = value;
  }

  public final int getPos() {
    return this.pos;
  }

  public final void setPos(final int pos) {
    this.pos = pos;
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.value, this.originalValue, Integer.valueOf(this.pos));
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (this.getClass() != obj.getClass()) {
      return false;
    }
    @SuppressWarnings("unchecked")
    final ValueWithPos<E> other = (ValueWithPos<E>) obj;
    return Objects.equals(this.value, other.value) //
        && Objects.equals(this.originalValue, other.originalValue) //
        && this.pos == other.pos;
  }

  @Override
  public String toString() {
    return "ValueWithPos [value=" + value + ", originalValue=" + originalValue + ", pos=" + pos
        + "]";
  }
}

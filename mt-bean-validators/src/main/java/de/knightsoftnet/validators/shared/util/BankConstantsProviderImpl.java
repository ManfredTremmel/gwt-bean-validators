/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.util;

import de.knightsoftnet.validators.shared.data.BankAccountBicSharedConstants;
import de.knightsoftnet.validators.shared.data.BicMapSharedConstants;
import de.knightsoftnet.validators.shared.data.CreateBankAccountBicMapConstantsClass;
import de.knightsoftnet.validators.shared.data.CreateBicMapConstantsClass;
import de.knightsoftnet.validators.shared.data.CreateIbanLengthMapConstantsClass;
import de.knightsoftnet.validators.shared.data.IbanLengthMapSharedConstants;

/**
 * utility interface for bank constants provider.
 *
 * @author Manfred Tremmel
 *
 */
public class BankConstantsProviderImpl implements BankConstantsProvider {
  private static final IbanLengthMapSharedConstants IBAN_LENGTH_MAP =
      CreateIbanLengthMapConstantsClass.create();
  private static final BankAccountBicSharedConstants BANK_ACCOINT_BIC_MAP =
      CreateBankAccountBicMapConstantsClass.create();
  private static final BicMapSharedConstants BIC_MAP = CreateBicMapConstantsClass.create();

  @Override
  public void setIbanLengthMapSharedConstantsWhenAvailable(
      final HasSetIbanLengthMapSharedConstants receiver) {
    receiver.setIbanLengthMapSharedConstants(IBAN_LENGTH_MAP);
  }

  @Override
  public void setBankAccountBicSharedConstantsWhenAvailable(
      final HasSetBankAccountBicSharedConstants receiver) {
    receiver.setBankAccountBicSharedConstants(BANK_ACCOINT_BIC_MAP);
  }

  @Override
  public void setBicMapSharedConstantsWhenAvailable(final HasSetBicMapSharedConstants receiver) {
    receiver.setBicMapSharedConstants(BIC_MAP);
  }

  public IbanLengthMapSharedConstants getIbanLengthMapSharedConstants() {
    return IBAN_LENGTH_MAP;
  }

  public BankAccountBicSharedConstants getBankAccountBicSharedConstants() {
    return BANK_ACCOINT_BIC_MAP;
  }

  public BicMapSharedConstants getBicMapConstants() {
    return BIC_MAP;
  }
}

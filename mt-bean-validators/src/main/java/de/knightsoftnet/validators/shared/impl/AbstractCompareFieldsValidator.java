/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.impl;

import de.knightsoftnet.validators.shared.util.BeanPropertyReaderUtil;

import java.lang.annotation.Annotation;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

/**
 * Check if entry of field1 compared to entry of field2 fulfills needs.
 *
 * @author Manfred Tremmel
 *
 */
public abstract class AbstractCompareFieldsValidator<A extends Annotation>
    implements ConstraintValidator<A, Object> {

  /**
   * error message key.
   */
  protected String message;
  /**
   * field1 name to compare.
   */
  protected String field1Name;
  /**
   * field2 name to compare.
   */
  protected String field2Name;

  /**
   * add error to field1.
   */
  protected boolean addErrorToField1;

  /**
   * add error to field2.
   */
  protected boolean addErrorToField2;

  /**
   * {@inheritDoc} check if given object is valid.
   *
   * @see jakarta.validation.ConstraintValidator#isValid(Object,
   *      jakarta.validation.ConstraintValidatorContext)
   */
  @Override
  public boolean isValid(final Object value, final ConstraintValidatorContext context) {
    if (value == null) {
      return true;
    }
    try {
      final Object field1Value = BeanPropertyReaderUtil.getNullSaveProperty(value, field1Name);
      final Object field2Value = BeanPropertyReaderUtil.getNullSaveProperty(value, field2Name);
      if (!comparissonIsValid(field1Value, field2Value)) {
        switchContext(context);
        return false;
      }
      return true;
    } catch (final Exception ignore) {
      switchContext(context);
      return false;
    }
  }

  protected abstract boolean comparissonIsValid(final Object field1Value,
      final Object field2Value);

  protected void switchContext(final ConstraintValidatorContext context) {
    if (addErrorToField1 || addErrorToField2) {
      context.disableDefaultConstraintViolation();
      if (addErrorToField1) {
        context.buildConstraintViolationWithTemplate(message).addPropertyNode(field1Name)
            .addConstraintViolation();
      }
      if (addErrorToField2) {
        context.buildConstraintViolationWithTemplate(message).addPropertyNode(field2Name)
            .addConstraintViolation();
      }
    }
  }
}

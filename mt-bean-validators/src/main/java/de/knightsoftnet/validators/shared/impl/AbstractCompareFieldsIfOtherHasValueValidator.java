/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.impl;

import de.knightsoftnet.validators.shared.util.BeanPropertyReaderUtil;

import java.lang.annotation.Annotation;
import java.util.List;

import jakarta.validation.ConstraintValidatorContext;

/**
 * Compare two field entries depending on another fields value.
 *
 * @author Manfred Tremmel
 *
 */
public abstract class AbstractCompareFieldsIfOtherHasValueValidator<A extends Annotation>
    extends AbstractCompareFieldsValidator<A> {

  /**
   * field name to compare.
   */
  protected String fieldCompareName;
  /**
   * field name to compare.
   */
  protected List<String> valueCompare;

  /**
   * {@inheritDoc} check if given object is valid.
   *
   * @see jakarta.validation.ConstraintValidator#isValid(Object,
   *      jakarta.validation.ConstraintValidatorContext)
   */
  @Override
  public final boolean isValid(final Object value, final ConstraintValidatorContext context) {
    if (value == null) {
      return true;
    }
    try {
      final Object field1Value = BeanPropertyReaderUtil.getNullSaveProperty(value, field1Name);
      final Object field2Value = BeanPropertyReaderUtil.getNullSaveProperty(value, field2Name);
      final String fieldCompareValue =
          BeanPropertyReaderUtil.getNullSaveStringProperty(value, fieldCompareName);
      if (valueCompare.contains(fieldCompareValue)
          && !comparissonIsValid(field1Value, field2Value)) {
        switchContext(context);
        return false;
      }
      return true;
    } catch (final Exception ignore) {
      switchContext(context);
      return false;
    }
  }
}

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.data;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

public class PhoneCountryConstantsImpl implements PhoneCountrySharedConstants, Serializable {
  private static final long serialVersionUID = 4729099755080438767L;

  private final Set<PhoneCountryCodeData> countryCode;
  private final Map<String, PhoneCountryData> countriesMap;

  /**
   * default constructor.
   */
  public PhoneCountryConstantsImpl() {
    this(new TreeSet<>(), new HashMap<>());
  }

  /**
   * constructor initializing set.
   *
   * @param countryCode set of country codes
   * @param countriesMap map of countries with corresponding PhoneCountryCodeData
   */
  public PhoneCountryConstantsImpl(final Set<PhoneCountryCodeData> countryCode,
      final Map<String, PhoneCountryData> countriesMap) {
    super();
    this.countryCode = countryCode;
    this.countriesMap = countriesMap;
  }

  @Override
  public Set<PhoneCountryCodeData> getCountryCode() {
    return countryCode;
  }

  @Override
  public Map<String, PhoneCountryData> getCountriesMap() {
    return countriesMap;
  }

  /**
   * setter for country code set.
   *
   * @param countryCode set of country codes.
   */
  public void setCountryCode(final Set<PhoneCountryCodeData> countryCode) {
    this.countryCode.clear();
    if (countryCode != null) {
      this.countryCode.addAll(countryCode);
    }
  }

  /**
   * map of countries.
   *
   * @param countriesMap map to set
   */
  public void setCountriesMap(final Map<String, PhoneCountryData> countriesMap) {
    this.countriesMap.clear();
    if (countriesMap != null) {
      this.countriesMap.putAll(countriesMap);
    }
  }
}

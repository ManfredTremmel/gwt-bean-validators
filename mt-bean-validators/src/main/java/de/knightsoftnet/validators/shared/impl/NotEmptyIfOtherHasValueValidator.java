/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.impl;

import de.knightsoftnet.validators.shared.NotEmptyIfOtherHasValue;
import de.knightsoftnet.validators.shared.util.BeanPropertyReaderUtil;

import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.List;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

/**
 * Check if a field is filled if another field has a given value.
 *
 * @author Manfred Tremmel
 *
 */
public class NotEmptyIfOtherHasValueValidator
    implements ConstraintValidator<NotEmptyIfOtherHasValue, Object> {

  /**
   * error message key.
   */
  private String message;
  /**
   * field name to check.
   */
  private String fieldCheckName;
  /**
   * field name to compare.
   */
  private String fieldCompareName;

  /**
   * value to compare with field name to compare.
   */
  private List<String> valueCompare;

  /**
   * {@inheritDoc} initialize the validator.
   *
   * @see jakarta.validation.ConstraintValidator#initialize(java.lang.annotation.Annotation)
   */
  @Override
  public final void initialize(final NotEmptyIfOtherHasValue constraintAnnotation) {
    message = constraintAnnotation.message();
    fieldCheckName = constraintAnnotation.field();
    fieldCompareName = constraintAnnotation.fieldCompare();
    valueCompare = Arrays.asList(constraintAnnotation.valueCompare());
  }

  /**
   * {@inheritDoc} check if given object is valid.
   *
   * @see jakarta.validation.ConstraintValidator#isValid(Object,
   *      jakarta.validation.ConstraintValidatorContext)
   */
  @Override
  public final boolean isValid(final Object value, final ConstraintValidatorContext context) {
    if (value == null) {
      return true;
    }
    try {
      final String fieldCheckValue =
          BeanPropertyReaderUtil.getNullSaveStringProperty(value, fieldCheckName);
      final String fieldCompareValue =
          BeanPropertyReaderUtil.getNullSaveStringProperty(value, fieldCompareName);
      if (StringUtils.isEmpty(fieldCheckValue) && valueCompare.contains(fieldCompareValue)) {
        switchContext(context);
        return false;
      }
      return true;
    } catch (final Exception ignore) {
      switchContext(context);
      return false;
    }
  }

  private void switchContext(final ConstraintValidatorContext context) {
    context.disableDefaultConstraintViolation();
    context.buildConstraintViolationWithTemplate(message).addPropertyNode(fieldCheckName)
        .addConstraintViolation();
  }
}

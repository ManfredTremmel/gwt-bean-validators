package de.knightsoftnet.validators.shared.beans;

import jakarta.validation.constraints.NotEmpty;

public class RecursiveTestEmbeddable {

  @NotEmpty
  private String test1;
  private String test2;

  public RecursiveTestEmbeddable() {
    super();
  }

  public RecursiveTestEmbeddable(@NotEmpty final String test1, final String test2) {
    super();
    this.test1 = test1;
    this.test2 = test2;
  }

  public static RecursiveTestEmbeddable of(@NotEmpty final String test1, final String test2) {
    return new RecursiveTestEmbeddable(test1, test2);
  }

  public String getTest1() {
    return test1;
  }

  public void setTest1(final String test1) {
    this.test1 = test1;
  }

  public String getTest2() {
    return test2;
  }

  public void setTest2(final String test2) {
    this.test2 = test2;
  }

  @Override
  public String toString() {
    return "TestEmbeddable [test1=" + test1 + ", test2=" + test2 + "]";
  }
}

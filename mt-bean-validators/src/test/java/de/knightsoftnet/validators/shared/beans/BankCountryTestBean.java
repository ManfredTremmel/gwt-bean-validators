/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.beans;

import de.knightsoftnet.validators.shared.BankCountry;
import de.knightsoftnet.validators.shared.Bic;
import de.knightsoftnet.validators.shared.Iban;

@BankCountry
public class BankCountryTestBean {

  private final String countryCode;

  @Iban
  private final String iban;

  @Bic
  private final String bic;

  private String bankAccountHolder;

  private String bankName;

  /**
   * constructor initializing fields.
   *
   * @param countryCode country code to set
   * @param iban iban to set
   * @param bic bic to set
   */
  public BankCountryTestBean(final String countryCode, final String iban, final String bic) {
    super();
    this.countryCode = countryCode;
    this.iban = iban;
    this.bic = bic;
  }

  public String getCountryCode() {
    return countryCode;
  }

  public String getIban() {
    return iban;
  }

  public String getBic() {
    return bic;
  }

  public String getBankAccountHolder() {
    return bankAccountHolder;
  }

  public void setBankAccountHolder(final String bankAccountHolder) {
    this.bankAccountHolder = bankAccountHolder;
  }

  public String getBankName() {
    return bankName;
  }

  public void setBankName(final String bankName) {
    this.bankName = bankName;
  }

  @Override
  public String toString() {
    return "BankCountryTestBean [countryCode=" + countryCode + ", iban=" + iban + ", bic=" + bic
        + ", bankAccountHolder=" + bankAccountHolder + ", bankName=" + bankName + "]";
  }
}

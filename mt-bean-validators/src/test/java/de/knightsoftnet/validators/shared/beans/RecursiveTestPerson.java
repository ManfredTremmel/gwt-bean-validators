/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.beans;

import de.knightsoftnet.validators.shared.PhoneNumberValue;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.PastOrPresent;
import jakarta.validation.constraints.Size;

/**
 * person data.
 *
 * @author Manfred Tremmel
 */
@PhoneNumberValue(fieldCountryCode = "postalAddress.countryCode", fieldPhoneNumber = "phoneNumber",
    allowCommon = false, allowDin5008 = false, allowE123 = false, allowUri = false, allowMs = true)
public class RecursiveTestPerson {

  @NotNull
  private RecursiveTestSalutationEnum salutation;
  @NotEmpty
  @Size(max = 50)
  private String firstName;
  @NotEmpty
  @Size(max = 50)
  private String lastName;
  @PastOrPresent
  private LocalDate birthday;

  private String phoneNumber;

  @NotNull
  @Valid
  private RecursiveTestCostCenterEntity costCenter;

  @Valid
  private RecursiveTestCostCenterEntity secondaryCostCenter;

  @NotNull
  @Valid
  private RecursiveTestPostalAddress postalAddress;

  @NotNull
  @Valid
  @Size(min = 1)
  private final List<RecursiveTestEmailEntity> emails;

  /**
   * default constructor.
   */
  public RecursiveTestPerson() {
    super();
    postalAddress = new RecursiveTestPostalAddress();
    emails = new ArrayList<>();
    costCenter = new RecursiveTestCostCenterEntity();
  }

  public RecursiveTestPerson(@NotNull final RecursiveTestSalutationEnum salutation,
      @NotEmpty @Size(max = 50) final String firstName,
      @NotEmpty @Size(max = 50) final String lastName, @PastOrPresent final LocalDate birthday,
      final String phoneNumber, @NotNull @Valid final RecursiveTestCostCenterEntity costCenter,
      @Valid final RecursiveTestCostCenterEntity secondaryCostCenter,
      @NotNull @Valid final RecursiveTestPostalAddress postalAddress,
      @NotNull @Valid @Size(min = 1) final List<RecursiveTestEmailEntity> emails) {
    super();
    this.salutation = salutation;
    this.firstName = firstName;
    this.lastName = lastName;
    this.birthday = birthday;
    this.phoneNumber = phoneNumber;
    this.costCenter = costCenter;
    this.secondaryCostCenter = secondaryCostCenter;
    this.postalAddress = postalAddress;
    this.emails = emails;
    if (this.emails != null && this.emails.size() > 0) {
      this.emails.forEach(email -> email.setPerson(this));
    }
  }

  public static RecursiveTestPerson of(@NotNull final RecursiveTestSalutationEnum salutation,
      @NotEmpty @Size(max = 50) final String firstName,
      @NotEmpty @Size(max = 50) final String lastName, @PastOrPresent final LocalDate birthday,
      final String phoneNumber, @NotNull @Valid final RecursiveTestCostCenterEntity costCenter,
      @Valid final RecursiveTestCostCenterEntity secondaryCostCenter,
      @NotNull @Valid final RecursiveTestPostalAddress postalAddress,
      @NotNull @Valid @Size(min = 1) final List<RecursiveTestEmailEntity> emails) {
    return new RecursiveTestPerson(salutation, firstName, lastName, birthday, phoneNumber,
        costCenter, secondaryCostCenter, postalAddress, emails);
  }

  public RecursiveTestSalutationEnum getSalutation() {
    return salutation;
  }

  public void setSalutation(final RecursiveTestSalutationEnum salutation) {
    this.salutation = salutation;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(final String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(final String lastName) {
    this.lastName = lastName;
  }

  public LocalDate getBirthday() {
    return birthday;
  }

  public void setBirthday(final LocalDate birthday) {
    this.birthday = birthday;
  }

  public String getPhoneNumber() {
    return phoneNumber;
  }

  public void setPhoneNumber(final String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  public RecursiveTestCostCenterEntity getCostCenter() {
    return costCenter;
  }

  public void setCostCenter(final RecursiveTestCostCenterEntity costCenter) {
    this.costCenter = costCenter;
  }

  public RecursiveTestCostCenterEntity getSecondaryCostCenter() {
    return secondaryCostCenter;
  }

  public void setSecondaryCostCenter(final RecursiveTestCostCenterEntity secondaryCostCenter) {
    this.secondaryCostCenter = secondaryCostCenter;
  }

  public RecursiveTestPostalAddress getPostalAddress() {
    return postalAddress;
  }

  public void setPostalAddress(final RecursiveTestPostalAddress postalAddress) {
    this.postalAddress = postalAddress;
  }

  public List<RecursiveTestEmailEntity> getEmails() {
    emails.forEach(email -> email.setPerson(this));
    return emails;
  }

  /**
   * setter for email list.
   *
   * @param emails list of emails to set
   */
  public void setEmails(final List<RecursiveTestEmailEntity> emails) {
    this.emails.clear();
    if (emails != null && emails.size() > 0) {
      emails.forEach(email -> email.setPerson(this));
      this.emails.addAll(emails);
    }
  }

  @Override
  public String toString() {
    return "Person [salutation=" + salutation + ", firstName=" + firstName + ", lastName="
        + lastName + ", birthday=" + birthday + ", phoneNumber=" + phoneNumber + ", costCenter="
        + costCenter + ", secondaryCostCenter=" + secondaryCostCenter + ", postalAddress="
        + postalAddress + ", emails=" + emails + "]";
  }
}

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.testcases;

import de.knightsoftnet.validators.shared.beans.IbanFormatedTestBean;

import java.util.ArrayList;
import java.util.List;

/**
 * get test cases for iban formated test.
 *
 * @author Manfred Tremmel
 *
 */
public class IbanFormatedTestCases {
  /**
   * get empty test bean.
   *
   * @return empty test bean
   */
  public static final IbanFormatedTestBean getEmptyTestBean() {
    return new IbanFormatedTestBean(null);
  }

  /**
   * get correct test beans.
   *
   * @return correct test beans
   */
  public static final List<IbanFormatedTestBean> getCorrectTestBeans() {
    final List<IbanFormatedTestBean> correctCases = new ArrayList<>();
    correctCases.add(new IbanFormatedTestBean("AD12 0001 2030 2003 5910 0100"));
    correctCases.add(new IbanFormatedTestBean("AE07 0331 2345 6789 0123 456"));
    correctCases.add(new IbanFormatedTestBean("AL47 2121 1009 0000 0002 3569 8741"));
    correctCases.add(new IbanFormatedTestBean("AT24 2011 1822 2121 9800"));
    correctCases.add(new IbanFormatedTestBean("AT61 1904 3002 3457 3201"));
    correctCases.add(new IbanFormatedTestBean("AZ21 NABZ 0000 0000 1370 1000 1944"));
    correctCases.add(new IbanFormatedTestBean("BA39 1290 0794 0102 8494"));
    correctCases.add(new IbanFormatedTestBean("BE51 3630 3644 5162"));
    correctCases.add(new IbanFormatedTestBean("BE68 5390 0754 7034"));
    correctCases.add(new IbanFormatedTestBean("BG80 BNBG 9661 1020 3456 78"));
    correctCases.add(new IbanFormatedTestBean("BH67 BMAG 0000 1299 1234 56"));
    correctCases.add(new IbanFormatedTestBean("BR18 0036 0305 0000 1000 9795 493C 1"));
    correctCases.add(new IbanFormatedTestBean("BY13 NBRB 3600 9000 0000 2Z00 AB00"));
    correctCases.add(new IbanFormatedTestBean("CH16 0900 0000 8777 6876 6"));
    correctCases.add(new IbanFormatedTestBean("CH93 0076 2011 6238 5295 7"));
    correctCases.add(new IbanFormatedTestBean("CR05 0152 0200 1026 2840 66"));
    correctCases.add(new IbanFormatedTestBean("CY17 0020 0128 0000 0012 0052 7600"));
    correctCases.add(new IbanFormatedTestBean("CZ65 0800 0000 1920 0014 5399"));
    correctCases.add(new IbanFormatedTestBean("DE16 7016 0000 0000 5554 44"));
    correctCases.add(new IbanFormatedTestBean("DE49 4306 0967 0000 0334 01"));
    correctCases.add(new IbanFormatedTestBean("DE89 3704 0044 0532 0130 00"));
    correctCases.add(new IbanFormatedTestBean("DJ21 0001 0000 0001 5400 0100 186"));
    correctCases.add(new IbanFormatedTestBean("DK50 0040 0440 1162 43"));
    correctCases.add(new IbanFormatedTestBean("DK62 8065 0002 0071 98"));
    correctCases.add(new IbanFormatedTestBean("DO28 BAGR 0000 0001 2124 5361 1324"));
    correctCases.add(new IbanFormatedTestBean("EE38 2200 2210 2014 5685"));
    correctCases.add(new IbanFormatedTestBean("ES91 2100 0418 4502 0005 1332"));
    correctCases.add(new IbanFormatedTestBean("FI21 1234 5600 0007 85"));
    correctCases.add(new IbanFormatedTestBean("FK88 SC12 3456 7890 12"));
    correctCases.add(new IbanFormatedTestBean("FO62 6460 0001 6316 34"));
    correctCases.add(new IbanFormatedTestBean("FR14 2004 1010 0505 0001 3M02 606"));
    correctCases.add(new IbanFormatedTestBean("GB29 NWBK 6016 1331 9268 19"));
    correctCases.add(new IbanFormatedTestBean("GE29 NB00 0000 0101 9049 17"));
    correctCases.add(new IbanFormatedTestBean("GI75 NWBK 0000 0000 7099 453"));
    correctCases.add(new IbanFormatedTestBean("GL89 6471 0001 0002 06"));
    correctCases.add(new IbanFormatedTestBean("GR16 0110 1250 0000 0001 2300 695"));
    correctCases.add(new IbanFormatedTestBean("GT82 TRAJ 0102 0000 0012 1002 9690"));
    correctCases.add(new IbanFormatedTestBean("HR12 1001 0051 8630 0016 0"));
    correctCases.add(new IbanFormatedTestBean("HU42 1177 3016 1111 1018 0000 0000"));
    correctCases.add(new IbanFormatedTestBean("IE29 AIBK 9311 5212 3456 78"));
    correctCases.add(new IbanFormatedTestBean("IL62 0108 0000 0009 9999 999"));
    correctCases.add(new IbanFormatedTestBean("IQ98 NBIQ 8501 2345 6789 012"));
    correctCases.add(new IbanFormatedTestBean("IS14 0159 2600 7654 5510 7303 39"));
    correctCases.add(new IbanFormatedTestBean("IT60 X054 2811 1010 0000 0123 456"));
    correctCases.add(new IbanFormatedTestBean("IT73 O050 1803 2000 0000 0125 125"));
    correctCases.add(new IbanFormatedTestBean("JO94 CBJO 0010 0000 0000 0131 0003 02"));
    correctCases.add(new IbanFormatedTestBean("KW81 CBKU 0000 0000 0000 1234 5601 01"));
    correctCases.add(new IbanFormatedTestBean("KZ86 125K ZT50 0410 0100"));
    correctCases.add(new IbanFormatedTestBean("LB62 0999 0000 0001 0019 0122 9114"));
    correctCases.add(new IbanFormatedTestBean("LC55 HEMM 0001 0001 0012 0012 0002 3015"));
    correctCases.add(new IbanFormatedTestBean("LI21 0881 0000 2324 013A A"));
    correctCases.add(new IbanFormatedTestBean("LT12 1000 0111 0100 1000"));
    correctCases.add(new IbanFormatedTestBean("LU28 0019 4006 4475 0000"));
    correctCases.add(new IbanFormatedTestBean("LV80 BANK 0000 4351 9500 1"));
    correctCases.add(new IbanFormatedTestBean("MC58 1122 2000 0101 2345 6789 030"));
    correctCases.add(new IbanFormatedTestBean("MD24 AG00 0225 1000 1310 4168"));
    correctCases.add(new IbanFormatedTestBean("ME25 5050 0001 2345 6789 51"));
    correctCases.add(new IbanFormatedTestBean("MK07 2501 2000 0058 984"));
    correctCases.add(new IbanFormatedTestBean("MN12 1234 1234 5678 9123"));
    correctCases.add(new IbanFormatedTestBean("MR13 0002 0001 0100 0012 3456 753"));
    correctCases.add(new IbanFormatedTestBean("MT84 MALT 0110 0001 2345 MTLC AST0 01S"));
    correctCases.add(new IbanFormatedTestBean("MU17 BOMM 0101 1010 3030 0200 000M UR"));
    correctCases.add(new IbanFormatedTestBean("NI79 BAMC 0000 0000 0000 0312 3123"));
    correctCases.add(new IbanFormatedTestBean("NL42 INGB 0006 3919 52"));
    correctCases.add(new IbanFormatedTestBean("NL91 ABNA 0417 1643 00"));
    correctCases.add(new IbanFormatedTestBean("NO93 8601 1117 947"));
    correctCases.add(new IbanFormatedTestBean("OM81 0180 0000 0129 9123 456"));
    correctCases.add(new IbanFormatedTestBean("PK36 SCBL 0000 0011 2345 6702"));
    correctCases.add(new IbanFormatedTestBean("PL61 1090 1014 0000 0712 1981 2874"));
    correctCases.add(new IbanFormatedTestBean("PS92 PALS 0000 0000 0400 1234 5670 2"));
    correctCases.add(new IbanFormatedTestBean("PT50 0002 0123 1234 5678 9015 4"));
    correctCases.add(new IbanFormatedTestBean("QA58 DOHB 0000 1234 5678 90AB CDEF G"));
    correctCases.add(new IbanFormatedTestBean("RO49 AAAA 1B31 0075 9384 0000"));
    correctCases.add(new IbanFormatedTestBean("RS35 2600 0560 1001 6113 79"));
    correctCases.add(new IbanFormatedTestBean("RU02 0445 2560 0407 0281 0412 3456 7890 1"));
    correctCases.add(new IbanFormatedTestBean("SA03 8000 0000 6080 1016 7519"));
    correctCases.add(new IbanFormatedTestBean("SC18 SSCB 1101 0000 0000 0000 1497 USD"));
    correctCases.add(new IbanFormatedTestBean("SD21 2901 0501 2340 01"));
    correctCases.add(new IbanFormatedTestBean("SE28 5000 0000 0530 4100 2965"));
    correctCases.add(new IbanFormatedTestBean("SE45 5000 0000 0583 9825 7466"));
    correctCases.add(new IbanFormatedTestBean("SI56 0201 0001 1603 397"));
    correctCases.add(new IbanFormatedTestBean("SI56 2633 0001 2039 086"));
    correctCases.add(new IbanFormatedTestBean("SK31 1200 0000 1987 4263 7541"));
    correctCases.add(new IbanFormatedTestBean("SM86 U032 2509 8000 0000 0270 100"));
    correctCases.add(new IbanFormatedTestBean("SO21 1000 0010 0100 0100 141"));
    correctCases.add(new IbanFormatedTestBean("ST23 0001 0001 0051 8453 1014 6"));
    correctCases.add(new IbanFormatedTestBean("TL38 0080 0123 4567 8910 157"));
    correctCases.add(new IbanFormatedTestBean("TN59 1000 6035 1835 9847 8831"));
    correctCases.add(new IbanFormatedTestBean("TR33 0006 1005 1978 6457 8413 26"));
    correctCases.add(new IbanFormatedTestBean("UA21 3223 1300 0002 6007 2335 6600 1"));
    correctCases.add(new IbanFormatedTestBean("VG96 VPVG 0000 0123 4567 8901"));
    correctCases.add(new IbanFormatedTestBean("XK05 1212 0123 4567 8906"));
    return correctCases;
  }

  /**
   * get wrong test beans.
   *
   * @return wrong test beans
   */
  public static final List<IbanFormatedTestBean> getWrongTestBeans() {
    final List<IbanFormatedTestBean> wrongCases = new ArrayList<>();
    wrongCases.add(new IbanFormatedTestBean("XY16 7016 0000 0000 5554 44"));
    return wrongCases;
  }

  /**
   * get wrong test beans.
   *
   * @return wrong test beans
   */
  public static final List<IbanFormatedTestBean> getToSmallTestBeans() {
    final List<IbanFormatedTestBean> wrongCases = new ArrayList<>();
    wrongCases.add(new IbanFormatedTestBean("DE12 3456 1"));
    return wrongCases;
  }

  /**
   * get wrong test beans.
   *
   * @return wrong test beans
   */
  public static final List<IbanFormatedTestBean> getToBigTestBeans() {
    final List<IbanFormatedTestBean> wrongCases = new ArrayList<>();
    wrongCases.add(new IbanFormatedTestBean("DE16 7016 0000 0000 5554 4412 3456 7890 123"));
    return wrongCases;
  }

  /**
   * get wrong test beans.
   *
   * @return wrong test beans
   */
  public static final List<IbanFormatedTestBean> getWrongChecksumTestBeans() {
    final List<IbanFormatedTestBean> wrongCases = new ArrayList<>();
    wrongCases.add(new IbanFormatedTestBean("DE16 7061 0000 0000 5554 44"));
    return wrongCases;
  }

  /**
   * get wrong test beans.
   *
   * @return wrong test beans
   */
  public static final List<IbanFormatedTestBean> getWrongFormatedTestBeans() {
    final List<IbanFormatedTestBean> wrongCases = new ArrayList<>();
    wrongCases.add(new IbanFormatedTestBean("DE167 016 0000 0000 5554 44"));
    wrongCases.add(new IbanFormatedTestBean("DE49 4306 09670000 0334 01"));
    wrongCases.add(new IbanFormatedTestBean("AT24 2011 1822 2121 980 0"));
    wrongCases.add(new IbanFormatedTestBean("CH16-0900-0000-8777-6876-6"));
    return wrongCases;
  }
}

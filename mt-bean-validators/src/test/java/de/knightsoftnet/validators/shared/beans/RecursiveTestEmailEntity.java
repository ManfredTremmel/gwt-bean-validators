package de.knightsoftnet.validators.shared.beans;

import de.knightsoftnet.validators.shared.Email;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;

/**
 * email entity.
 */
public class RecursiveTestEmailEntity {

  @Email
  @NotEmpty
  private String email;

  @NotNull
  private RecursiveTestEmailTypeEnum type;

  private RecursiveTestPerson person;

  @Valid
  private RecursiveTestEmbeddable embeddThree;

  public RecursiveTestEmailEntity() {
    super();
    embeddThree = new RecursiveTestEmbeddable();
  }

  public RecursiveTestEmailEntity(@NotEmpty final String email,
      @NotNull final RecursiveTestEmailTypeEnum type,
      @Valid final RecursiveTestEmbeddable embeddThree) {
    super();
    this.email = email;
    this.type = type;
    this.embeddThree = embeddThree;
  }

  public RecursiveTestEmailEntity(@NotEmpty final String email,
      @NotNull final RecursiveTestEmailTypeEnum type, final RecursiveTestPerson person,
      @Valid final RecursiveTestEmbeddable embeddThree) {
    super();
    this.email = email;
    this.type = type;
    this.person = person;
    this.embeddThree = embeddThree;
  }

  public static RecursiveTestEmailEntity of(@NotEmpty final String email,
      @NotNull final RecursiveTestEmailTypeEnum type,
      @Valid final RecursiveTestEmbeddable embeddThree) {
    return new RecursiveTestEmailEntity(email, type, embeddThree);
  }

  public static RecursiveTestEmailEntity of(@NotEmpty final String email,
      @NotNull final RecursiveTestEmailTypeEnum type, final RecursiveTestPerson person,
      @Valid final RecursiveTestEmbeddable embeddThree) {
    return new RecursiveTestEmailEntity(email, type, person, embeddThree);
  }

  public void setEmail(final String email) {
    this.email = email;
  }

  public String getEmail() {
    return email;
  }

  public RecursiveTestEmailTypeEnum getType() {
    return type;
  }

  public void setType(final RecursiveTestEmailTypeEnum type) {
    this.type = type;
  }

  public RecursiveTestPerson getPerson() {
    return person;
  }

  public void setPerson(final RecursiveTestPerson person) {
    this.person = person;
  }

  public RecursiveTestEmbeddable getEmbeddThree() {
    return embeddThree;
  }

  public void setEmbeddThree(final RecursiveTestEmbeddable embeddThree) {
    this.embeddThree = embeddThree;
  }

  @Override
  public String toString() {
    return "EmailEntity [email=" + email + ", type=" + type + ", embeddThree=" + embeddThree + "]";
  }
}

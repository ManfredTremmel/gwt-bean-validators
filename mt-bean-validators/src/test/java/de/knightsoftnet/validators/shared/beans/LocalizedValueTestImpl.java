/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.beans;

import de.knightsoftnet.validators.shared.interfaces.LocalizedValue;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jakarta.validation.constraints.NotEmpty;

public class LocalizedValueTestImpl implements LocalizedValue<String, String> {

  private final Map<@NotEmpty String, String> localizedText;

  public LocalizedValueTestImpl() {
    super();
    localizedText = new HashMap<>();
  }

  @SafeVarargs
  public LocalizedValueTestImpl(final String[]... values) {
    super();
    localizedText = new HashMap<>();
    List.of(values).forEach(entry -> localizedText.put(entry[0], entry[1]));
  }

  @Override
  public void setLocalizedText(final Map<String, String> localizedText) {
    this.localizedText.clear();
    if (localizedText != null && !localizedText.isEmpty()) {
      this.localizedText.putAll(localizedText);
    }
  }

  @Override
  public void putLocalizedText(final String language, final String text) {
    localizedText.put(language, text);
  }

  @Override
  public Map<String, String> getLocalizedText() {
    return localizedText;
  }

  @Override
  public String getLocalizedText(final String language) {
    return localizedText.get(language);
  }

  @Override
  public String toString() {
    final StringBuilder toStringvalue = new StringBuilder();
    toStringvalue.append("LocalizedValueTestImpl [");
    localizedText.forEach((key, value) -> {
      toStringvalue.append("localizedText[").append(key).append("]=").append(value).append(',');
    });
    toStringvalue.append(']');
    return toStringvalue.toString();
  }
}

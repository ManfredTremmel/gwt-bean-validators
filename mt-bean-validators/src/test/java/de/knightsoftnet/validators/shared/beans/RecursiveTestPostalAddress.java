package de.knightsoftnet.validators.shared.beans;

import de.knightsoftnet.validators.shared.PostalCode;
import de.knightsoftnet.validators.shared.data.CountryEnum;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

/**
 * postal address jpa embeddable.
 */
@PostalCode(fieldCountryCode = "countryCode", fieldPostalCode = "postalCode")
public class RecursiveTestPostalAddress {

  /**
   * default length limit.
   */
  public static final int LENGTH_DEFAULT = 255;

  /**
   * length limit of the street name.
   */
  public static final int LENGTH_STREET_NUMBER = 10;

  /**
   * length limit postal code.
   */
  public static final int LENGTH_POSTAL_CODE = 10;

  /**
   * street name.
   */
  @Size(max = LENGTH_DEFAULT)
  @NotEmpty
  private String street;

  /**
   * street number.
   */
  @Size(max = LENGTH_STREET_NUMBER)
  @NotEmpty
  private String streetNumber;

  /**
   * postal code.
   */
  @Size(max = LENGTH_POSTAL_CODE)
  @NotEmpty
  private String postalCode;

  /**
   * localized name of the locality (city/town).
   */
  @Size(max = LENGTH_DEFAULT)
  @NotEmpty
  private String locality;

  /**
   * country code.
   */
  @NotNull
  private CountryEnum countryCode;

  /**
   * localized name of the region (state).
   */
  @Size(max = LENGTH_DEFAULT)
  private String region;

  @NotNull
  @Valid
  private RecursiveTestEmbeddable embeddOne;

  @Valid
  private RecursiveTestEmbeddable embeddTwo;

  public RecursiveTestPostalAddress() {
    super();
    embeddOne = new RecursiveTestEmbeddable();
    embeddTwo = new RecursiveTestEmbeddable();
  }

  public RecursiveTestPostalAddress(@Size(max = LENGTH_DEFAULT) @NotEmpty final String street,
      @Size(max = LENGTH_STREET_NUMBER) @NotEmpty final String streetNumber,
      @Size(max = LENGTH_POSTAL_CODE) @NotEmpty final String postalCode,
      @Size(max = LENGTH_DEFAULT) @NotEmpty final String locality,
      @NotNull final CountryEnum countryCode, @Size(max = LENGTH_DEFAULT) final String region,
      @NotNull @Valid final RecursiveTestEmbeddable embeddOne,
      @Valid final RecursiveTestEmbeddable embeddTwo) {
    super();
    this.street = street;
    this.streetNumber = streetNumber;
    this.postalCode = postalCode;
    this.locality = locality;
    this.countryCode = countryCode;
    this.region = region;
    this.embeddOne = embeddOne;
    this.embeddTwo = embeddTwo;
  }

  public static RecursiveTestPostalAddress of(
      @Size(max = LENGTH_DEFAULT) @NotEmpty final String street,
      @Size(max = LENGTH_STREET_NUMBER) @NotEmpty final String streetNumber,
      @Size(max = LENGTH_POSTAL_CODE) @NotEmpty final String postalCode,
      @Size(max = LENGTH_DEFAULT) @NotEmpty final String locality,
      @NotNull final CountryEnum countryCode, @Size(max = LENGTH_DEFAULT) final String region,
      @NotNull @Valid final RecursiveTestEmbeddable embeddOne,
      @Valid final RecursiveTestEmbeddable embeddTwo) {
    return new RecursiveTestPostalAddress(street, streetNumber, postalCode, locality, countryCode,
        region, embeddOne, embeddTwo);
  }

  public String getStreet() {
    return street;
  }

  public void setStreet(final String street) {
    this.street = street;
  }

  public String getStreetNumber() {
    return streetNumber;
  }

  public void setStreetNumber(final String streetNumber) {
    this.streetNumber = streetNumber;
  }

  public String getPostalCode() {
    return postalCode;
  }

  public void setPostalCode(final String postalCode) {
    this.postalCode = postalCode;
  }

  public String getLocality() {
    return locality;
  }

  public void setLocality(final String locality) {
    this.locality = locality;
  }

  public CountryEnum getCountryCode() {
    return countryCode;
  }

  public void setCountryCode(final CountryEnum countryCode) {
    this.countryCode = countryCode;
  }

  public String getRegion() {
    return region;
  }

  public void setRegion(final String region) {
    this.region = region;
  }

  public RecursiveTestEmbeddable getEmbeddOne() {
    return embeddOne;
  }

  public void setEmbeddOne(final RecursiveTestEmbeddable embeddOne) {
    this.embeddOne = embeddOne;
  }

  public RecursiveTestEmbeddable getEmbeddTwo() {
    return embeddTwo;
  }

  public void setEmbeddTwo(final RecursiveTestEmbeddable embeddTwo) {
    this.embeddTwo = embeddTwo;
  }

  @Override
  public String toString() {
    return "PostalAddress [street=" + street + ", streetNumber=" + streetNumber + ", postalCode="
        + postalCode + ", locality=" + locality + ", countryCode=" + countryCode + ", region="
        + region + "]";
  }
}

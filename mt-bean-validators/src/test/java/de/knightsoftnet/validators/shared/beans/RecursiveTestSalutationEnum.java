package de.knightsoftnet.validators.shared.beans;

/**
 * possible salutations.
 *
 * @author Manfred Tremmel
 */
public enum RecursiveTestSalutationEnum {
  // Mister
  MR,
  // Misses
  MRS,
  // Inter
  INTER
}

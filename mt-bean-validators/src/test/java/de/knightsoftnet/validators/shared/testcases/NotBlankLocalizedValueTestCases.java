/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.testcases;

import de.knightsoftnet.validators.shared.beans.LocalizedValueTestImpl;
import de.knightsoftnet.validators.shared.beans.NotBlankLocalizedValueTestBean;

import java.util.ArrayList;
import java.util.List;

/**
 * get test cases for not blank test with localized value.
 *
 * @author Manfred Tremmel
 *
 */
public class NotBlankLocalizedValueTestCases {
  /**
   * get empty test bean.
   *
   * @return empty test bean
   */
  public static final NotBlankLocalizedValueTestBean getEmptyTestBean() {
    return new NotBlankLocalizedValueTestBean(null);
  }

  /**
   * get correct test beans.
   *
   * @return correct test beans
   */
  public static final List<NotBlankLocalizedValueTestBean> getCorrectTestBeans() {
    final List<NotBlankLocalizedValueTestBean> correctCases = new ArrayList<>();
    final String de[] = {"de", "Eintrag"};
    final String en[] = {"en", "Value"};
    correctCases.add(new NotBlankLocalizedValueTestBean(new LocalizedValueTestImpl(de, en)));
    return correctCases;
  }

  /**
   * get wrong test beans.
   *
   * @return wrong test beans
   */
  public static final List<NotBlankLocalizedValueTestBean> getWrongTestBeans() {
    final List<NotBlankLocalizedValueTestBean> wrongCases = new ArrayList<>();
    final String deEmpty[] = {"de", ""};
    final String en[] = {"en", "to long value"};
    wrongCases.add(new NotBlankLocalizedValueTestBean(new LocalizedValueTestImpl(deEmpty, en)));
    final String deNull[] = {"de", null};
    wrongCases.add(new NotBlankLocalizedValueTestBean(new LocalizedValueTestImpl(deNull, en)));
    final String deBlank[] = {"de", "  "};
    wrongCases.add(new NotBlankLocalizedValueTestBean(new LocalizedValueTestImpl(deBlank, en)));
    return wrongCases;
  }
}

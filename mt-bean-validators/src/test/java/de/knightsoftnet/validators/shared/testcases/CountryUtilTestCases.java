/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.testcases;

import de.knightsoftnet.validators.shared.data.CountryEnum;

import java.util.HashMap;
import java.util.Map;

/**
 * get test cases for country util test.
 *
 * @author Manfred Tremmel
 *
 */
public class CountryUtilTestCases {

  /**
   * get language to countryEnum test cases.
   *
   * @return map of strings with expected format result strings
   */
  public static final Map<CountryEnum, String> getLanguageToCountryEnum() {
    final Map<CountryEnum, String> testData = new HashMap<>();
    testData.put(CountryEnum.DE, "de_DE");
    testData.put(CountryEnum.AT, "de_AT");
    testData.put(CountryEnum.DE, "de");
    testData.put(CountryEnum.US, "en_US");
    testData.put(CountryEnum.GB, "en_EN");
    testData.put(CountryEnum.GB, "en");
    return testData;
  }
}

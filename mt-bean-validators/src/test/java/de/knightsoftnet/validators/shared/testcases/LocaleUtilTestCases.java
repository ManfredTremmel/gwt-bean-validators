/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.testcases;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * get test cases for locale util test.
 *
 * @author Manfred Tremmel
 *
 */
public class LocaleUtilTestCases {

  /**
   * get language to local test cases.
   *
   * @return map of strings with expected results
   */
  public static final Map<Locale, String> getLanguageToLocaleCases() {
    final Map<Locale, String> testData = new HashMap<>();
    testData.put(Locale.GERMANY, "de_DE");
    testData.put(Locale.GERMAN, "de_AT");
    testData.put(Locale.GERMAN, "de");
    testData.put(Locale.US, "en_US");
    testData.put(Locale.ENGLISH, "en_EN");
    testData.put(Locale.ENGLISH, "en");
    return testData;
  }
}

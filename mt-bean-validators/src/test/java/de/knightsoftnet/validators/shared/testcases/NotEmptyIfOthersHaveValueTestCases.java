/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.testcases;

import de.knightsoftnet.validators.shared.beans.NotEmptyIfOthersHaveValueTestBean;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * test cases for NotEmptyIfOthersHaveValue annotation tests.
 *
 * @author Manfred Tremmel
 *
 */
public class NotEmptyIfOthersHaveValueTestCases {
  /**
   * get correct test beans.
   *
   * @return correct test beans
   */
  public static final List<NotEmptyIfOthersHaveValueTestBean> getCorrectTestBeans() {
    final List<NotEmptyIfOthersHaveValueTestBean> correctCases = new ArrayList<>();
    correctCases.add(new NotEmptyIfOthersHaveValueTestBean("not empty", "A", "A"));
    correctCases.add(new NotEmptyIfOthersHaveValueTestBean("not empty", "A", "B"));
    correctCases.add(new NotEmptyIfOthersHaveValueTestBean("not empty", "A", "C"));
    correctCases.add(new NotEmptyIfOthersHaveValueTestBean(null, "B", "A"));
    correctCases.add(new NotEmptyIfOthersHaveValueTestBean(null, "A", "X"));
    correctCases.add(new NotEmptyIfOthersHaveValueTestBean(StringUtils.EMPTY, "B", "A"));
    correctCases.add(new NotEmptyIfOthersHaveValueTestBean(StringUtils.EMPTY, "A", "X"));
    return correctCases;
  }

  /**
   * get wrong test beans.
   *
   * @return wrong test beans
   */
  public static final List<NotEmptyIfOthersHaveValueTestBean> getWrongEmptyTestBeans() {
    final List<NotEmptyIfOthersHaveValueTestBean> wrongCases = new ArrayList<>();
    wrongCases.add(new NotEmptyIfOthersHaveValueTestBean(null, "A", "A"));
    wrongCases.add(new NotEmptyIfOthersHaveValueTestBean(null, "A", "B"));
    wrongCases.add(new NotEmptyIfOthersHaveValueTestBean(null, "A", "C"));
    wrongCases.add(new NotEmptyIfOthersHaveValueTestBean(StringUtils.EMPTY, "A", "A"));
    wrongCases.add(new NotEmptyIfOthersHaveValueTestBean(StringUtils.EMPTY, "A", "B"));
    wrongCases.add(new NotEmptyIfOthersHaveValueTestBean(StringUtils.EMPTY, "A", "C"));
    return wrongCases;
  }
}

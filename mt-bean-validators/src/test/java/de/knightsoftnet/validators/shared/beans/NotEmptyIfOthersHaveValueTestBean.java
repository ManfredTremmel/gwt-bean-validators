/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.beans;

import de.knightsoftnet.validators.shared.FieldValueMapping;
import de.knightsoftnet.validators.shared.NotEmptyIfOthersHaveValue;

/**
 * Test bean for NotEmptyIfOthersHaveValue validation.
 */
@NotEmptyIfOthersHaveValue(field = "baseField",
    otherFieldValueMapping = {
        @FieldValueMapping(fieldCompare = "compareFieldOne", valueCompare = "A"),
        @FieldValueMapping(fieldCompare = "compareFiledTwo", valueCompare = {"A", "B", "C"})})
public class NotEmptyIfOthersHaveValueTestBean {

  private final String baseField;

  private final String compareFieldOne;

  private final String compareFiledTwo;

  /**
   * constructor initializing fields.
   *
   * @param baseField base field
   * @param compareFieldOne compare field number one
   * @param compareFiledTwo compare field number two
   */
  public NotEmptyIfOthersHaveValueTestBean(final String baseField,
      final String compareFieldOne, final String compareFiledTwo) {
    super();
    this.baseField = baseField;
    this.compareFieldOne = compareFieldOne;
    this.compareFiledTwo = compareFiledTwo;
  }

  public String getBaseField() {
    return baseField;
  }

  public String getCompareFieldOne() {
    return compareFieldOne;
  }

  public String getCompareFiledTwo() {
    return compareFiledTwo;
  }

  @Override
  public String toString() {
    return "NotEmptyAlternateIfOthersHaveValueTestBean [baseField=" + baseField
        + ", compareFieldOne=" + compareFieldOne + ", compareFiledTwo=" + compareFiledTwo + "]";
  }
}

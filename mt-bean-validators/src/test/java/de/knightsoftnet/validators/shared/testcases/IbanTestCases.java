/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.testcases;

import de.knightsoftnet.validators.shared.beans.IbanTestBean;

import java.util.ArrayList;
import java.util.List;

/**
 * get test cases for iban test.
 *
 * @author Manfred Tremmel
 *
 */
public class IbanTestCases {
  /**
   * get empty test bean.
   *
   * @return empty test bean
   */
  public static final IbanTestBean getEmptyTestBean() {
    return new IbanTestBean(null);
  }

  /**
   * get correct test beans.
   *
   * @return correct test beans
   */
  public static final List<IbanTestBean> getCorrectTestBeans() {
    final List<IbanTestBean> correctCases = new ArrayList<>();
    correctCases.add(new IbanTestBean("AD1200012030200359100100"));
    correctCases.add(new IbanTestBean("AE070331234567890123456"));
    correctCases.add(new IbanTestBean("AL47212110090000000235698741"));
    correctCases.add(new IbanTestBean("AT242011182221219800"));
    correctCases.add(new IbanTestBean("AT611904300234573201"));
    correctCases.add(new IbanTestBean("AZ21NABZ00000000137010001944"));
    correctCases.add(new IbanTestBean("BA391290079401028494"));
    correctCases.add(new IbanTestBean("BE51363036445162"));
    correctCases.add(new IbanTestBean("BE68539007547034"));
    correctCases.add(new IbanTestBean("BG80BNBG96611020345678"));
    correctCases.add(new IbanTestBean("BH67BMAG00001299123456"));
    correctCases.add(new IbanTestBean("BI4210000100010000332045181"));
    correctCases.add(new IbanTestBean("BR1800360305000010009795493C1"));
    correctCases.add(new IbanTestBean("BY13NBRB3600900000002Z00AB00"));
    correctCases.add(new IbanTestBean("CH1609000000877768766"));
    correctCases.add(new IbanTestBean("CH9300762011623852957"));
    correctCases.add(new IbanTestBean("CR05015202001026284066"));
    correctCases.add(new IbanTestBean("CY17002001280000001200527600"));
    correctCases.add(new IbanTestBean("CZ6508000000192000145399"));
    correctCases.add(new IbanTestBean("DE16701600000000555444"));
    correctCases.add(new IbanTestBean("DE49430609670000033401"));
    correctCases.add(new IbanTestBean("DE48360100439999999999"));
    correctCases.add(new IbanTestBean("DE89370400440532013000"));
    correctCases.add(new IbanTestBean("DJ2100010000000154000100186"));
    correctCases.add(new IbanTestBean("DK5000400440116243"));
    correctCases.add(new IbanTestBean("DK6280650002007198"));
    correctCases.add(new IbanTestBean("DO28BAGR00000001212453611324"));
    correctCases.add(new IbanTestBean("EE382200221020145685"));
    correctCases.add(new IbanTestBean("EG380019000500000000263180002"));
    correctCases.add(new IbanTestBean("ES9121000418450200051332"));
    correctCases.add(new IbanTestBean("FI2112345600000785"));
    correctCases.add(new IbanTestBean("FK88SC123456789012"));
    correctCases.add(new IbanTestBean("FO6264600001631634"));
    correctCases.add(new IbanTestBean("FR1420041010050500013M02606"));
    correctCases.add(new IbanTestBean("GB29NWBK60161331926819"));
    correctCases.add(new IbanTestBean("GE29NB0000000101904917"));
    correctCases.add(new IbanTestBean("GI75NWBK000000007099453"));
    correctCases.add(new IbanTestBean("GL8964710001000206"));
    correctCases.add(new IbanTestBean("GR1601101250000000012300695"));
    correctCases.add(new IbanTestBean("GT82TRAJ01020000001210029690"));
    correctCases.add(new IbanTestBean("HR1210010051863000160"));
    correctCases.add(new IbanTestBean("HU42117730161111101800000000"));
    correctCases.add(new IbanTestBean("IE29AIBK93115212345678"));
    correctCases.add(new IbanTestBean("IL620108000000099999999"));
    correctCases.add(new IbanTestBean("IQ98NBIQ850123456789012"));
    correctCases.add(new IbanTestBean("IS140159260076545510730339"));
    correctCases.add(new IbanTestBean("IT60X0542811101000000123456"));
    correctCases.add(new IbanTestBean("IT73O0501803200000000125125"));
    correctCases.add(new IbanTestBean("JO94CBJO0010000000000131000302"));
    correctCases.add(new IbanTestBean("KW81CBKU0000000000001234560101"));
    correctCases.add(new IbanTestBean("KZ86125KZT5004100100"));
    correctCases.add(new IbanTestBean("LB62099900000001001901229114"));
    correctCases.add(new IbanTestBean("LC55HEMM000100010012001200023015"));
    correctCases.add(new IbanTestBean("LI21088100002324013AA"));
    correctCases.add(new IbanTestBean("LT121000011101001000"));
    correctCases.add(new IbanTestBean("LU280019400644750000"));
    correctCases.add(new IbanTestBean("LV80BANK0000435195001"));
    correctCases.add(new IbanTestBean("LY83002048000020100120361"));
    correctCases.add(new IbanTestBean("MC5811222000010123456789030"));
    correctCases.add(new IbanTestBean("MD24AG000225100013104168"));
    correctCases.add(new IbanTestBean("ME25505000012345678951"));
    correctCases.add(new IbanTestBean("MK07250120000058984"));
    correctCases.add(new IbanTestBean("MN121234123456789123"));
    correctCases.add(new IbanTestBean("MR1300020001010000123456753"));
    correctCases.add(new IbanTestBean("MT84MALT011000012345MTLCAST001S"));
    correctCases.add(new IbanTestBean("MU17BOMM0101101030300200000MUR"));
    correctCases.add(new IbanTestBean("NI79BAMC00000000000003123123"));
    correctCases.add(new IbanTestBean("NL42INGB0006391952"));
    correctCases.add(new IbanTestBean("NL91ABNA0417164300"));
    correctCases.add(new IbanTestBean("NO9386011117947"));
    correctCases.add(new IbanTestBean("OM810180000001299123456"));
    correctCases.add(new IbanTestBean("PK36SCBL0000001123456702"));
    correctCases.add(new IbanTestBean("PL61109010140000071219812874"));
    correctCases.add(new IbanTestBean("PS92PALS000000000400123456702"));
    correctCases.add(new IbanTestBean("PT50000201231234567890154"));
    correctCases.add(new IbanTestBean("QA58DOHB00001234567890ABCDEFG"));
    correctCases.add(new IbanTestBean("RO49AAAA1B31007593840000"));
    correctCases.add(new IbanTestBean("RS35260005601001611379"));
    correctCases.add(new IbanTestBean("RU0204452560040702810412345678901"));
    correctCases.add(new IbanTestBean("SA0380000000608010167519"));
    correctCases.add(new IbanTestBean("SC18SSCB11010000000000001497USD"));
    correctCases.add(new IbanTestBean("SD2129010501234001"));
    correctCases.add(new IbanTestBean("SE2850000000053041002965"));
    correctCases.add(new IbanTestBean("SE4550000000058398257466"));
    correctCases.add(new IbanTestBean("SI56020100011603397"));
    correctCases.add(new IbanTestBean("SI56263300012039086"));
    correctCases.add(new IbanTestBean("SK3112000000198742637541"));
    correctCases.add(new IbanTestBean("SM86U0322509800000000270100"));
    correctCases.add(new IbanTestBean("SO211000001001000100141"));
    correctCases.add(new IbanTestBean("ST23000100010051845310146"));
    correctCases.add(new IbanTestBean("SV62CENR00000000000000700025"));
    correctCases.add(new IbanTestBean("TL380080012345678910157"));
    correctCases.add(new IbanTestBean("TN5910006035183598478831"));
    correctCases.add(new IbanTestBean("TR330006100519786457841326"));
    correctCases.add(new IbanTestBean("UA213223130000026007233566001"));
    correctCases.add(new IbanTestBean("VA59001123000012345678"));
    correctCases.add(new IbanTestBean("VG96VPVG0000012345678901"));
    correctCases.add(new IbanTestBean("XK051212012345678906"));
    return correctCases;
  }

  /**
   * get wrong test beans.
   *
   * @return wrong test beans
   */
  public static final List<IbanTestBean> getWrongTestBeans() {
    final List<IbanTestBean> wrongCases = new ArrayList<>();
    wrongCases.add(new IbanTestBean("XY16701600000000555444"));
    return wrongCases;
  }

  /**
   * get wrong test beans.
   *
   * @return wrong test beans
   */
  public static final List<IbanTestBean> getToSmallTestBeans() {
    final List<IbanTestBean> wrongCases = new ArrayList<>();
    wrongCases.add(new IbanTestBean("DE123"));
    return wrongCases;
  }

  /**
   * get wrong test beans.
   *
   * @return wrong test beans
   */
  public static final List<IbanTestBean> getToBigTestBeans() {
    final List<IbanTestBean> wrongCases = new ArrayList<>();
    wrongCases.add(new IbanTestBean("DE167016000000005554441234567890123"));
    return wrongCases;
  }

  /**
   * get wrong test beans.
   *
   * @return wrong test beans
   */
  public static final List<IbanTestBean> getWrongChecksumTestBeans() {
    final List<IbanTestBean> wrongCases = new ArrayList<>();
    wrongCases.add(new IbanTestBean("DE16706100000000555444"));
    return wrongCases;
  }
}

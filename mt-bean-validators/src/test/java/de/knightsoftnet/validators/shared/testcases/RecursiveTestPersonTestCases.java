/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.testcases;

import de.knightsoftnet.validators.shared.beans.RecursiveTestCostCenterEntity;
import de.knightsoftnet.validators.shared.beans.RecursiveTestEmailEntity;
import de.knightsoftnet.validators.shared.beans.RecursiveTestEmailTypeEnum;
import de.knightsoftnet.validators.shared.beans.RecursiveTestEmbeddable;
import de.knightsoftnet.validators.shared.beans.RecursiveTestPerson;
import de.knightsoftnet.validators.shared.beans.RecursiveTestPostalAddress;
import de.knightsoftnet.validators.shared.beans.RecursiveTestSalutationEnum;
import de.knightsoftnet.validators.shared.data.CountryEnum;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * get test cases for recursive test person test.
 *
 * @author Manfred Tremmel
 *
 */
public class RecursiveTestPersonTestCases {

  /**
   * get correct test beans.
   *
   * @return correct test beans
   */
  public static final List<RecursiveTestPerson> getCorrectTestBeans() {
    final List<RecursiveTestPerson> correctCases = new ArrayList<>();
    correctCases.add(//
        RecursiveTestPerson.of(RecursiveTestSalutationEnum.MR, "first", "last",
            LocalDate.of(2000, 1, 1), "+49 (30) 1234567",
            RecursiveTestCostCenterEntity.of("100100", "firstCC"), //
            null, //
            RecursiveTestPostalAddress.of("street", "number", "12345", "testhausen", CountryEnum.DE,
                null, RecursiveTestEmbeddable.of("test1", null), null),
            List.of(//
                RecursiveTestEmailEntity.of("test@test.de", RecursiveTestEmailTypeEnum.HOME,
                    null))));
    return correctCases;
  }

  /**
   * get wrong test beans.
   *
   * @return wrong test beans
   */
  public static final List<RecursiveTestPerson> getWrongCountryTestBeans() {
    final List<RecursiveTestPerson> wrongCases = new ArrayList<>();
    wrongCases.add(//
        RecursiveTestPerson.of(null, "first", "last", LocalDate.of(2000, 1, 1), "+49 (30) 1234567",
            RecursiveTestCostCenterEntity.of("100100", "firstCC"), //
            null, //
            RecursiveTestPostalAddress.of("street", "number", "12345", "testhausen", CountryEnum.DE,
                null, RecursiveTestEmbeddable.of("test1", null), null),
            List.of(//
                RecursiveTestEmailEntity.of("test@test.de", RecursiveTestEmailTypeEnum.HOME,
                    null))));
    return wrongCases;
  }
}

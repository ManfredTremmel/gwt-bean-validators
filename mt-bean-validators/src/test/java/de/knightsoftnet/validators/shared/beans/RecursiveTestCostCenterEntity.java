package de.knightsoftnet.validators.shared.beans;

import jakarta.validation.constraints.NotEmpty;

/**
 * cost center entity.
 */
public class RecursiveTestCostCenterEntity {

  public RecursiveTestCostCenterEntity() {
    super();
  }

  public RecursiveTestCostCenterEntity(@NotEmpty final String number, @NotEmpty final String name) {
    super();
    this.number = number;
    this.name = name;
  }

  public static RecursiveTestCostCenterEntity of(@NotEmpty final String number,
      @NotEmpty final String name) {
    return new RecursiveTestCostCenterEntity(number, name);
  }

  @NotEmpty
  private String number;
  @NotEmpty
  private String name;

  public String getNumber() {
    return number;
  }

  public void setNumber(final String number) {
    this.number = number;
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  @Override
  public String toString() {
    return "CostCenterEntity [number=" + number + ", name=" + name + "]";
  }
}

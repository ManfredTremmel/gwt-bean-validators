/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.util;

import de.knightsoftnet.validators.shared.data.CountryEnum;
import de.knightsoftnet.validators.shared.testcases.CountryUtilTestCases;

import org.junit.Assert;
import org.junit.Test;

import java.util.Map.Entry;

public class CountryUtilTest {

  /**
   * test convert language to local.
   */
  @Test
  public void testConvertLanguageToCountryEnum() {
    Assert.assertNull("local should be null", CountryUtil.convertLanguageToCountryEnum(null));
    Assert.assertNull("local should be null", CountryUtil.convertLanguageToCountryEnum(""));
    Assert.assertNull("local should be null", CountryUtil.convertLanguageToCountryEnum("xx"));
    for (final Entry<CountryEnum, String> entry : CountryUtilTestCases.getLanguageToCountryEnum()
        .entrySet()) {
      Assert.assertEquals("local detection failed", entry.getKey(),
          CountryUtil.convertLanguageToCountryEnum(entry.getValue()));
    }
  }
}

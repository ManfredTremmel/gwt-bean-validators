/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.server;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;

import java.util.Set;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;

/**
 * Abstract validation test.
 *
 * @author Manfred Tremmel
 *
 * @param <E> type of bean to test
 */
public abstract class AbstractValidationTest<E> { // NOPMD

  /**
   * logger for logging messages.
   */
  private static final Logger LOG = LogManager.getLogger(AbstractValidationTest.class);

  protected static final String SIZE_VALIDATOR = "org.hibernate.validator.internal."
      + "constraintvalidators.bv.size.SizeValidatorForCharSequence";
  protected static final String DIGITS_VALIDATOR = "org.hibernate.validator.internal."
      + "constraintvalidators.bv.DigitsValidatorForCharSequence";

  /**
   * test validation.
   *
   * @param bean the bean to test
   * @param shouldBeOk true if it's expected, that the test brings no validation error
   * @param excpetedValidationClass the validator class that will report an error
   * @return violation list for additional tests
   */
  public final Set<ConstraintViolation<E>> validationTest(final E bean, final boolean shouldBeOk,
      final String excpetedValidationClass) {
    final Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

    final Set<ConstraintViolation<E>> cv1 = validator.validate(bean);

    if (shouldBeOk) {
      Assert.assertTrue("Should have no validation error " + bean.toString(), cv1.isEmpty());
    } else {
      Assert.assertFalse("Should have a validation error " + bean.toString(), cv1.isEmpty());
    }
    for (final ConstraintViolation<E> violation : cv1) {
      Assert.assertEquals("Should be reported by special validator", excpetedValidationClass,
          violation.getConstraintDescriptor().getConstraintValidatorClasses().get(0).getName());
      AbstractValidationTest.LOG.debug("Error Message of type "
          + violation.getConstraintDescriptor().getConstraintValidatorClasses() + " for field \""
          + violation.getPropertyPath().toString() + "\" with value \"" + bean.toString()
          + "\", message: " + violation.getMessage());
    }
    return cv1;
  }
}

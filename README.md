gwt-bean-validators-parent
==========================

parent project of the modules

* [mt-bean-validators](./mt-bean-validators) - a collection of JSR-303/JSR-349/JSR-380 bean validators.
* [gwt-time-dummy](./gwt-time-dummy) - dummy GWT emulation classes for `java.time.Clock` and `java.time.Duration` without functionality!
* [gwt-bean-validators-engine](./gwt-bean-validators-engine) - the validation engine for gwt
* [gwt-bean-validators-engine-jsr310](./gwt-bean-validators-engine-jsr310) - extended validation engine with additional JSR-310 support
* [gwt-bean-validators-engine-mt](./gwt-bean-validators-engine-mt) - extended validation engine with mt-bean-validators integration
* [gwt-bean-validators-editor](./gwt-bean-validators-editor) - editor driver with integrated validation
* [gwt-bean-validators-decorators](./gwt-bean-validators-decorators) - decorators to diesplay validation results, for widgets without support
* [gwt-bean-validators](./gwt-bean-validators) - metapackage for gwt-bean-validators-engine-mt, gwt-bean-validators-editor and gwt-bean-validators-decorators
* [gwt-bean-validators-rest-constants](./gwt-bean-validators-rest-constants) - helper package with some shared constants
* [gwt-bean-validators-rest-resolver](./gwt-bean-validators-rest-resolver) - client side resolver to receive data by rest instead of compile it into code
* [gwt-bean-validators-restservice-jaxrs](./gwt-bean-validators-restservice-jaxrs) - server implementation of rest services for rest resolver  based on jaxrs
* [gwt-bean-validators-restservice-spring](./gwt-bean-validators-restservice-spring) - server implementation of rest services for rest resolver  based on spring rest controller
* [gwt-jackson-datatype-jsr310](./gwt-jackson-datatype-jsr310) - integration of JSR-310 datatypes in gwt-jackson.
* [gwtp-spring-integration-shared](./gwtp-spring-integration-shared) - basic functionality to integrate gwtp with spring, shared part
* [gwtp-spring-integration-server](./gwtp-spring-integration-server) - basic functionality to integrate gwtp with spring, server part
* [gwtp-spring-integration-client](./gwtp-spring-integration-client) - basic functionality to integrate gwtp with spring, client part
* [gwtp-dynamic-navigation](./gwtp-dynamic-navigation) - a view helper classes to generate a dynamic navigation in a gwtp project
* [gwt-bean-validators-restygwt-jaxrs](./gwt-bean-validators-restygwt-jaxrs) - metapackage including gwt-bean-validators-engine-mt, gwt-bean-validators-rest-resolver and gwt-bean-validators-restservice-jaxrs
* [gwt-bean-validators-spring-gwtp](./gwt-bean-validators-spring-gwtp) - metapackage including gwt-bean-validators-engine-mt, gwt-bean-validators-rest-resolver and gwt-bean-validators-restservice-spring
* [gwt-mt-widgets](./gwt-mt-widgets) - a set of widgets and handlers for gwt applications using gwt-bean-validators
* [gwt-mt-widgets-restygwt-jaxrs](./gwt-mt-widgets-restygwt-jaxrs) - metapackage for gwt-mt-widgets and gwt-bean-validators-restygwt-jaxrs
* [gwt-mt-widgets-spring-gwtp](./gwt-mt-widgets-spring-gwtp) - extends gwt-mt-widgets by some spring helper widgets

for more informations look at the modules

Also take a look at the [gwt-bean-validators-example](https://gitlab.com/ManfredTremmel/gwt-bean-validators-example) project and the [wiki](https://gitlab.com/ManfredTremmel/gwt-bean-validators-example/-/wikis/home) which shows, how to use the stuff.
If you are interested in a real world application, visit my [machine sharing platform](https://www.m-share.de/).
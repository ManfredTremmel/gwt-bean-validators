/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.navigation.client.ui.navigation;

import de.knightsoftnet.gwtp.spring.client.event.ChangeUserEvent;
import de.knightsoftnet.gwtp.spring.client.event.ChangeUserEvent.ChangeUserHandler;
import de.knightsoftnet.gwtp.spring.client.session.Session;
import de.knightsoftnet.navigation.client.event.ChangePlaceEvent;
import de.knightsoftnet.navigation.client.event.ChangePlaceEvent.ChangePlaceHandler;
import de.knightsoftnet.navigation.client.ui.navigation.NavigationPresenter.MyProxy;
import de.knightsoftnet.navigation.client.ui.navigation.NavigationPresenter.MyView;

import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.gwtplatform.mvp.client.Presenter;
import com.gwtplatform.mvp.client.View;
import com.gwtplatform.mvp.client.annotations.NoGatekeeper;
import com.gwtplatform.mvp.client.annotations.ProxyStandard;
import com.gwtplatform.mvp.client.proxy.PlaceManager;
import com.gwtplatform.mvp.client.proxy.Proxy;
import com.gwtplatform.mvp.shared.proxy.PlaceRequest;

import org.apache.commons.lang3.StringUtils;

/**
 * Presenter of the navigation page.
 *
 * @author Manfred Tremmel
 * @version $Rev$, $Date$
 *
 */
public class NavigationPresenter extends Presenter<MyView, MyProxy>
    implements ChangeUserHandler, ChangePlaceHandler {

  public interface MyView extends View {
    /**
     * set a reference to the presenter/activity.
     *
     * @param presenter reference to set
     */
    void setPresenter(NavigationPresenter presenter);

    /**
     * create navigation.
     *
     * @param navigation the structure with the navigation data
     */
    void createNavigation(final NavigationStructure navigation);

    /**
     * set new active entry.
     *
     * @param newEntry new active entry
     */
    void setSelectedItem(final NavigationEntryInterface newEntry);
  }

  @ProxyStandard
  @NoGatekeeper
  public interface MyProxy extends Proxy<NavigationPresenter> {
  }

  private final PlaceManager placeManager;
  private final NavigationStructure navigationStructure;

  private String loginToken;
  private String logoutToken;

  /**
   * constructor with injected parameters.
   *
   * @param eventBus event bus
   * @param view navigation view
   * @param proxy navigation proxy
   * @param placeManager place manager
   * @param currentSession session data
   * @param navigationStructure place data
   */
  @Inject
  public NavigationPresenter(final EventBus eventBus, final MyView view, final MyProxy proxy,
      final PlaceManager placeManager, final Session currentSession,
      final NavigationStructure navigationStructure) {
    super(eventBus, view, proxy);
    view.setPresenter(this);
    this.placeManager = placeManager;
    this.navigationStructure = navigationStructure;

    loginToken = NameTokens.LOGIN;
    logoutToken = NameTokens.LOGOUT;

    eventBus.addHandler(ChangeUserEvent.getType(), this);
    navigationStructure.buildVisibleNavigation(null);

    getView().createNavigation(this.navigationStructure);

    eventBus.addHandler(ChangePlaceEvent.getType(), this);

    currentSession.readSessionData();
  }

  @Override
  public void onChangeUser(final ChangeUserEvent event) {
    if (tokenEquals(placeManager.getCurrentPlaceRequest().getNameToken(), logoutToken)) {
      final PlaceRequest loginPlaceRequest =
          new PlaceRequest.Builder().nameToken(loginToken).build();
      placeManager.revealPlace(loginPlaceRequest);
    } else {
      if (placeManager.getHierarchyDepth() > 1) {
        placeManager.revealRelativePlace(-1);
      } else if (tokenEquals(placeManager.getCurrentPlaceRequest().getNameToken(), loginToken)
          && event.getUser() != null && event.getUser().isLoggedIn()) {
        placeManager.revealDefaultPlace();
      } else if (event.getUser() == null || !event.getUser().isLoggedIn()) {
        // user not logged in, load page once again, maybe we are no longer allowed to see
        placeManager.revealCurrentPlace();
      }
    }
    navigationStructure.buildVisibleNavigation(event.getUser());
    getView().createNavigation(navigationStructure);
  }

  private boolean tokenEquals(final String url1, final String url2) {
    return StringUtils.equals(StringUtils.removeEnd(StringUtils.removeStart(url1, "/"), "/"),
        StringUtils.removeEnd(StringUtils.removeStart(url2, "/"), "/"));
  }

  @Override
  public void onChangePlace(final ChangePlaceEvent event) {
    if (event != null && StringUtils.isNotEmpty(event.getToken())) {
      getView().setSelectedItem(navigationStructure.getNavigationForToken(event.getToken()));
    }
  }

  public String getLoginToken() {
    return loginToken;
  }

  public void setLoginToken(final String loginToken) {
    this.loginToken = loginToken;
  }

  public String getLogoutToken() {
    return logoutToken;
  }

  public void setLogoutToken(final String logoutToken) {
    this.logoutToken = logoutToken;
  }
}

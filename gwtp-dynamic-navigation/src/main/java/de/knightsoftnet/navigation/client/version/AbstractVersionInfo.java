/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.navigation.client.version;

import com.google.gwt.i18n.shared.DateTimeFormat;
import com.google.gwt.i18n.shared.DateTimeFormat.PredefinedFormat;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;

import org.apache.commons.lang3.StringUtils;

import java.util.Date;

/**
 * The <code>AbstractVersionInfo</code> class provides a view version informations.
 *
 * @author Manfred Tremmel
 */
public abstract class AbstractVersionInfo implements VersionInfoInterface {
  /**
   * version date format.
   */
  private final DateTimeFormat versionDateFormat;

  /**
   * date format to display in dialog.
   */
  private final DateTimeFormat dateFormatDisplay;

  private String copyrightText;
  private String versionNumber;
  private String versionDate;
  private String author;

  /**
   * default constructor.
   */
  protected AbstractVersionInfo() {
    super();
    versionDateFormat = DateTimeFormat.getFormat("yyyyMMdd-HHmm");
    dateFormatDisplay = DateTimeFormat.getFormat(PredefinedFormat.DATE_MEDIUM);
  }

  public void setCopyrightText(final String copyrightText) {
    this.copyrightText = copyrightText;
  }

  public void setVersionNumber(final String versionNumber) {
    this.versionNumber = versionNumber;
  }

  public void setVersionDate(final String versionDate) {
    this.versionDate = versionDate;
  }

  public void setAuthor(final String author) {
    this.author = author;
  }

  @Override
  public final SafeHtml getCopyrightText() {
    return SafeHtmlUtils.fromString("© " + copyrightText);
  }

  @Override
  public final SafeHtml getVersionNumber() {
    return SafeHtmlUtils.fromString(versionNumber);
  }

  @Override
  public final SafeHtml getVersionDate() {
    return SafeHtmlUtils.fromString(parseAndFormatDate(versionDate));
  }

  @Override
  public final SafeHtml getAuthor() {
    return SafeHtmlUtils.fromString(author);
  }

  /**
   * parse and format a date string.
   *
   * @param versionDate string with date in versionDateFormat
   * @return the same date formated as dateFormatDisplay
   */
  protected final String parseAndFormatDate(final String versionDate) {
    Date date;
    if (StringUtils.isEmpty(versionDate)) {
      date = new Date();
    } else {
      try {
        date = versionDateFormat.parse(versionDate);
      } catch (final IllegalArgumentException e) {
        date = new Date();
      }
    }
    return dateFormatDisplay.format(date);
  }
}

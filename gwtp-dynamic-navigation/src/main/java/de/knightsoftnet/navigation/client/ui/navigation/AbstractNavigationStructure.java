/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.navigation.client.ui.navigation;

import de.knightsoftnet.gwtp.spring.client.session.Session;
import de.knightsoftnet.gwtp.spring.shared.models.User;
import de.knightsoftnet.navigation.client.event.ChangePlaceEvent;
import de.knightsoftnet.navigation.client.event.ChangePlaceEvent.ChangePlaceHandler;

import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.user.client.ui.AbstractImagePrototype;
import com.google.web.bindery.event.shared.EventBus;

import org.apache.commons.lang3.StringUtils;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import jakarta.inject.Inject;

/**
 * The <code>AbstractNavigationPlace</code> defines the methods which are used to handle
 * navigation/menu entries.
 *
 * @author Manfred Tremmel
 */
public abstract class AbstractNavigationStructure
    implements ChangePlaceHandler, NavigationStructure {

  /**
   * map to find navigation entries for places.
   */
  private final Map<String, NavigationEntryInterface> placeMap;

  /**
   * list of all navigation entries.
   */
  private List<NavigationEntryInterface> fullNavigationList;

  /**
   * list of active navigation entries (if there are not all displayed).
   */
  private List<NavigationEntryInterface> navigationList;

  /**
   * selected navigation entry.
   */
  private String activeToken;

  /**
   * token for login.
   */
  private String loginToken;

  /**
   * default constructor.
   */
  @Inject
  protected AbstractNavigationStructure(final EventBus eventBus) {
    this(eventBus, NameTokens.LOGIN);
  }

  /**
   * constructor.
   *
   * @param eventBus event bus
   * @param loginToken login token
   */
  protected AbstractNavigationStructure(final EventBus eventBus, final String loginToken) {
    super();
    placeMap = new HashMap<>();
    eventBus.addHandler(ChangePlaceEvent.getType(), this);
    this.loginToken = loginToken;
  }

  @Inject
  public void init(final Session session) {
    buildVisibleNavigation(session == null ? null : session.getUser());
  }

  @Override
  public final void buildVisibleNavigation(final User user) {
    fullNavigationList = recursiveGetEntries(buildNavigation());
    generateMapRecursive(fullNavigationList);
    navigationList = fullNavigationList;
  }

  /**
   * build the navigation list.
   *
   * @return list of navigation entries
   */
  protected abstract List<NavigationEntryInterface> buildNavigation();

  /**
   * create map out of the navigation list.
   *
   * @param navigationEntries list of navigation entries
   */
  private void generateMapRecursive(final List<NavigationEntryInterface> navigationEntries) {
    for (final NavigationEntryInterface entryToAdd : navigationEntries) {
      String token = entryToAdd.getToken();
      if (entryToAdd.getMenuValue() != null && token != null) {
        if (token.endsWith("/" + StringUtils.removeStart(loginToken, "/"))) {
          token = loginToken;
        }
        if (!placeMap.containsKey(token)) {
          placeMap.put(token, entryToAdd);
        }
      }
      if (entryToAdd instanceof final NavigationEntryFolder folder) {
        generateMapRecursive(folder.getSubEntries());
      }
    }
  }

  /**
   * get all navigation entries that can be displayed by a given user.
   *
   * @param navigationEntries entries to test
   * @param puser the user to test
   * @return the navigationEntries
   */
  private List<NavigationEntryInterface> recursiveGetEntries(
      final List<NavigationEntryInterface> navigationEntries) {
    if (navigationEntries == null) {
      return Collections.emptyList();
    }
    return navigationEntries.stream().filter(entry -> entry.canReveal()).map(entry -> {
      if (entry instanceof final NavigationEntryFolder folder) {
        return new NavigationEntryFolder(entry.getMenuValue(), entry.isOpenOnStartup(),
            recursiveGetEntries(folder.getSubEntries()));
      } else {
        return entry;
      }
    }).collect(Collectors.toList());
  }

  @Override
  public final List<NavigationEntryInterface> getFullNavigationList() {
    return fullNavigationList;
  }

  @Override
  public final List<NavigationEntryInterface> getNavigationList() {
    return navigationList;
  }

  @Override
  public final void setNavigationList(final List<NavigationEntryInterface> navigationList) {
    this.navigationList = navigationList;
  }

  @Override
  public final NavigationEntryInterface getActiveNavigationEntryInterface() {
    return getNavigationForToken(activeToken);
  }

  @Override
  public final void setActiveNavigationEntryInterface(
      final NavigationEntryInterface activeNavigationEntryInterface) {
    activeToken =
        activeNavigationEntryInterface == null ? null : activeNavigationEntryInterface.getToken();
  }

  @Override
  public final void setActiveNavigationEntryInterface(final String token) {
    activeToken = token;
  }

  @Override
  public final NavigationEntryInterface getNavigationForToken(final String token) {
    NavigationEntryInterface entry = placeMap.get(token);
    if (entry == null && StringUtils.contains(token, '?')) {
      final int posSeparator = token.indexOf('?');
      if (posSeparator > 0) {
        entry = placeMap.get(token.substring(0, posSeparator));
      }
    }
    return entry;
  }

  @Override
  public void onChangePlace(final ChangePlaceEvent event) {
    this.setActiveNavigationEntryInterface(event.getToken());
  }

  @Override
  public final String getLoginToken() {
    return loginToken;
  }

  @Override
  public final void setLoginToken(final String loginToken) {
    this.loginToken = loginToken;
  }

  protected SafeHtml createMenuEntry(final ImageResource image, final String text) {
    final SafeHtmlBuilder menuShb = new SafeHtmlBuilder();
    menuShb.append(AbstractImagePrototype.create(image).getSafeHtml()).append(' ')
        .appendEscaped(text);
    return menuShb.toSafeHtml();
  }
}

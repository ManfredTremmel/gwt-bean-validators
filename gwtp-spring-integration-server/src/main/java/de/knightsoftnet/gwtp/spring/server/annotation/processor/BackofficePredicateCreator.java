package de.knightsoftnet.gwtp.spring.server.annotation.processor;

import de.knightsoftnet.gwtp.spring.annotation.processor.AbstractBackofficeCreator;
import de.knightsoftnet.gwtp.spring.annotation.processor.BackofficeWidget;
import de.knightsoftnet.gwtp.spring.server.annotation.BackofficeGenerator;
import de.knightsoftnet.validators.shared.data.FieldTypeEnum;

import org.apache.commons.lang3.StringUtils;

import java.io.PrintWriter;
import java.util.List;
import java.util.Objects;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;

/**
 * Create predicate.
 */
public class BackofficePredicateCreator extends AbstractBackofficeCreator<BackofficeGenerator> {

  protected static final String CLASS_SUFFIX = "Predicate";

  public BackofficePredicateCreator() {
    super(CLASS_SUFFIX);
  }

  @Override
  protected void addAdditionalImports(final String serverPackage, final Element element,
      final BackofficeGenerator annotationInterface, final List<BackofficeWidget> widgets,
      final ProcessingEnvironment processingEnv) {
    addImport(element.asType());
    addImport(StringUtils.replace(element.asType().toString(),
        "." + getEntityNameOfElement(element), ".Q" + getEntityNameOfElement(element)));
    addImports(//
        "de.knightsoftnet.gwtp.spring.server.querydsl.AbstractPredicate", //
        "de.knightsoftnet.gwtp.spring.shared.search.SearchCriteriaServer");

    final List<BackofficeWidget> oneToManyWidgetList = detectOneToManyWidgets(widgets);

    final List<BackofficeWidget> enumWidgetList = detectEnumWidgets(widgets, oneToManyWidgetList);

    if (!enumWidgetList.isEmpty()) {
      addImports(//
          "com.querydsl.core.types.dsl.BooleanExpression", //
          StringUtils.replace(element.asType().toString(), "." + getEntityNameOfElement(element),
              ".Q" + getEntityNameOfElement(element)));
    }

    if (!oneToManyWidgetList.isEmpty()) {
      addImports(//
          "com.querydsl.core.types.dsl.BooleanExpression", //
          "com.querydsl.jpa.JPAExpressions", //
          StringUtils.replace(element.asType().toString(), "." + getEntityNameOfElement(element),
              ".Q" + getEntityNameOfElement(element)));
      oneToManyWidgetList.forEach(widget -> addImport(
          StringUtils.replace(getDeclaredTypeOfFirstTypeElement(widget.getField()).toString(),
              "." + getEntityNameOfFirstTypeElement(widget.getField()),
              ".Q" + getEntityNameOfFirstTypeElement(widget.getField()))));
    }
  }

  @Override
  protected void writeBody(final PrintWriter out, final String serverPackage, final Element element,
      final BackofficeGenerator annotationInterface, final List<BackofficeWidget> widgets,
      final ProcessingEnvironment processingEnv) {
    final String entityName = getEntityNameOfElement(element);

    out.print("public class ");
    out.print(entityName);
    out.print(suffix);
    out.print(" extends AbstractPredicate<");
    out.print(entityName);
    out.println("> {");

    out.println();
    out.print("  public ");
    out.print(entityName);
    out.print(suffix);
    out.println("(final SearchCriteriaServer criteria) {");
    out.print("    super(criteria, \"");
    out.print(getEntityNameOfElementLower(element));
    out.print("\", ");
    out.print(entityName);
    out.println(".class);");
    out.println("  }");

    overwriteGetPredicate(out, element, widgets);

    out.println("}");
  }

  private void overwriteGetPredicate(final PrintWriter out, final Element element,
      final List<BackofficeWidget> widgets) {

    final List<BackofficeWidget> oneToManyWidgetList = detectOneToManyWidgets(widgets);

    final List<BackofficeWidget> manyToOneWidgetList = detectManyToOneWidgets(widgets);

    final List<BackofficeWidget> enumWidgetList = detectEnumWidgets(widgets, oneToManyWidgetList);

    if (oneToManyWidgetList.isEmpty() && enumWidgetList.isEmpty()
        && manyToOneWidgetList.isEmpty()) {
      return;
    }

    out.println();
    out.println("  @Override");
    out.println("  public BooleanExpression getPredicate() {");
    out.println();
    out.println("    switch (criteria.getKey()) {");

    enumWidgetList.stream().forEach(widget -> {
      out.print("      case \"");
      out.print(widget.getName());
      out.println("\":");

      out.print("        return enumCompare(Q");
      out.print(getEntityNameOfElement(element));
      out.print(".");
      out.print(getEntityNameOfElementLower(element));
      out.print(".");
      out.print(widget.getName());
      out.println(",");

      out.print("            (");
      out.print(getEnumName(widget.getField().asType()));
      out.println(") criteria.getValue());");
    });

    oneToManyWidgetList.stream().forEach(widget -> writeOneToManyCases(out, widget,
        "Q" + getEntityNameOfElement(element) + "." + getEntityNameOfElementLower(element)));

    manyToOneWidgetList.stream().forEach(widget -> writeManyToOneCases(out, widget,
        "Q" + getEntityNameOfElement(element) + "." + getEntityNameOfElementLower(element)));

    out.println("      default:");
    out.println("        return super.getPredicate();");
    out.println("    }");
    out.println("  }");
  }

  private void writeOneToManyCases(final PrintWriter out, final BackofficeWidget oneToManyWidget,
      final String containsString) {
    oneToManyWidget.getChildWidgets().stream().flatMap(BackofficeWidget::streamFlatBackofficeWidget)
        .filter(widget -> !widget.isIgnore() && !"id".equals(widget.getName())
            && widget.getFieldType() != FieldTypeEnum.EMBEDDED)
        .forEach(widget -> {
          out.print("      case \"");
          out.print(oneToManyWidget.getName());
          out.print(".");
          out.print(widget.getName());
          out.println("\":");

          out.print("        return ");
          out.print(containsString);
          out.print(".");
          out.print(oneToManyWidget.getName());
          out.println(".contains(");

          out.print("            JPAExpressions.selectFrom(Q");
          out.print(getEntityNameOfFirstTypeElement(oneToManyWidget.getField()));
          out.print(".");
          out.print(getEntityNameOfFirstTypeElementLower(oneToManyWidget.getField()));
          out.println(")");

          out.print("                .where(Q");
          out.print(getEntityNameOfFirstTypeElement(oneToManyWidget.getField()));
          out.print(".");
          out.print(getEntityNameOfFirstTypeElementLower(oneToManyWidget.getField()));
          out.print(".");
          out.print(Objects.toString(oneToManyWidget.getAdditional().get("mappedBy")));
          out.print(".eq(");
          out.print(containsString);
          out.println(")");

          out.print("                    .and(");
          out.print(detectCompare(widget.getFieldType()));
          out.print("(Q");
          out.print(getEntityNameOfFirstTypeElement(oneToManyWidget.getField()));
          out.print(".");
          out.print(getEntityNameOfFirstTypeElementLower(oneToManyWidget.getField()));
          out.print(".");
          out.print(widget.getName());
          out.println(",");

          out.print("                        (");
          out.print(widget.getField().asType().toString());
          out.println(") criteria.getValue()))));");
        });
  }

  private void writeManyToOneCases(final PrintWriter out, final BackofficeWidget oneToManyWidget,
      final String containsString) {
    out.print("      case \"");
    out.print(oneToManyWidget.getName());
    out.println("\":");

    out.print("        return stringCompare(");
    out.print(containsString);
    out.print(".");
    out.print(oneToManyWidget.getName());
    out.print(".");
    out.print(oneToManyWidget.getAdditional().get("keyfield"));
    out.println(", criteria.getValue());");
  }

  private String detectCompare(final FieldTypeEnum fieldType) {
    return switch (fieldType) {
      case STRING_LOCALIZED -> "stringCompare";
      case NUMERIC -> "numberCompare";
      case BOOLEAN -> "stringCompare";
      case DATE, TIME, DATETIME -> "temporalCompare";
      case ENUM_FIXED, ENUM_SQL -> "enumCompare";
      default -> "stringCompare";
    };
  }

  protected List<BackofficeWidget> detectEnumWidgets(final List<BackofficeWidget> widgets,
      final List<BackofficeWidget> oneToManyWidgetList) {
    return detectBackofficeWidgetsOfElementFlat(widgets).stream()
        .filter(widget -> !widget.isIgnore() && widget.getFieldType() == FieldTypeEnum.ENUM_FIXED
            && !oneToManyWidgetList.stream()
                .anyMatch(otmWidget -> otmWidget.getChildWidgets().stream()
                    .anyMatch(subWidget -> widget.getField().equals(subWidget.getField()))))
        .toList();
  }

  protected List<BackofficeWidget> detectOneToManyWidgets(final List<BackofficeWidget> widgets) {
    return detectBackofficeWidgetsOfElementFlat(widgets).stream()
        .filter(widget -> !widget.isIgnore() && widget.getFieldType() == FieldTypeEnum.ONE_TO_MANY)
        .toList();
  }

  protected List<BackofficeWidget> detectManyToOneWidgets(final List<BackofficeWidget> widgets) {
    return detectBackofficeWidgetsOfElementFlat(widgets).stream()
        .filter(widget -> !widget.isIgnore() && widget.getFieldType() == FieldTypeEnum.MANY_TO_ONE)
        .toList();
  }
}

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.gwtp.spring.server.security;

import de.knightsoftnet.gwtp.spring.server.converter.UserDetailsConverter;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Objects;

import jakarta.inject.Inject;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 * authentication success handler for gwt applications. based on the work of
 * https://github.com/imrabti/gwtp-spring-security
 *
 * @author Manfred Tremmel
 */
@Component
public class AuthSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {
  private static final Logger LOGGER = LoggerFactory.getLogger(AuthSuccessHandler.class);

  private final ObjectMapper mapper;
  private final CsrfCookieHandler csrfCookieHandler;
  private final UserDetailsConverter userDetailsConverter;

  @Inject
  protected AuthSuccessHandler(final CsrfCookieHandler csrfCookieHandler,
      final UserDetailsConverter userDetailsConverter) {
    super();
    final MappingJackson2HttpMessageConverter pmessageConverter =
        new MappingJackson2HttpMessageConverter();
    mapper = pmessageConverter.getObjectMapper();
    this.csrfCookieHandler = csrfCookieHandler;
    this.userDetailsConverter = userDetailsConverter;
  }

  @Override
  public void onAuthenticationSuccess(final HttpServletRequest request,
      final HttpServletResponse response, final Authentication authentication)
      throws IOException, ServletException {
    csrfCookieHandler.setCookie(request, response);

    if (authentication.isAuthenticated()) {
      response.setStatus(HttpServletResponse.SC_OK);
      response.setContentType(MediaType.APPLICATION_JSON_VALUE);
      LOGGER.info("User is authenticated!");
      LOGGER.debug(Objects.toString(authentication, StringUtils.EMPTY).replaceAll("[\r\n]",
          StringUtils.EMPTY));

      final PrintWriter writer = response.getWriter();
      mapper.writeValue(writer,
          userDetailsConverter.convert((UserDetails) authentication.getPrincipal()));
      writer.flush();
    } else {
      response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
    }
  }
}

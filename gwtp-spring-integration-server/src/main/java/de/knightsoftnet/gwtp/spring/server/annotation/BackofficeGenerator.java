/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.gwtp.spring.server.annotation;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.SOURCE;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * Annotates a to generate server side backoffice stuff.
 */
@Documented
@Target(FIELD)
@Retention(SOURCE)
public @interface BackofficeGenerator {

  /**
   * server path where to reach rest interface.
   *
   * @return path
   */
  String value();

  /**
   * generate repository for entity.
   *
   * @return boolean, true when generating repository
   */
  boolean generateRepository() default true;

  /**
   * generate predicate for entity.
   *
   * @return boolean, true when generating predicate
   */
  boolean generatePredicate() default true;

  /**
   * generate predicate builder for entity.
   *
   * @return boolean, true when generating predicate builder
   */
  boolean generatePredicateBuilder() default true;

  /**
   * generate controller for entity.
   *
   * @return boolean, true when generating controller
   */
  boolean generateController() default true;
}

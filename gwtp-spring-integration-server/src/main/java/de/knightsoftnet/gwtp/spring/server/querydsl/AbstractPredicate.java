/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.gwtp.spring.server.querydsl;

import de.knightsoftnet.gwtp.spring.shared.search.SearchCriteriaServer;

import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.BooleanPath;
import com.querydsl.core.types.dsl.DatePath;
import com.querydsl.core.types.dsl.DateTimePath;
import com.querydsl.core.types.dsl.EnumPath;
import com.querydsl.core.types.dsl.MapPath;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.PathBuilder;
import com.querydsl.core.types.dsl.StringPath;
import com.querydsl.core.types.dsl.TemporalExpression;
import com.querydsl.core.types.dsl.TimePath;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Date;
import java.util.Objects;

/**
 * predicate for query dsl search.
 *
 * @author Manfred Tremmel
 *
 * @param <T> path type
 */
public abstract class AbstractPredicate<T> {

  protected SearchCriteriaServer criteria;
  protected final String dbName;
  protected final Class<? extends T> type;

  /**
   * constructor.
   */
  protected AbstractPredicate(final SearchCriteriaServer criteria, final String dbName,
      final Class<? extends T> type) {
    super();
    this.criteria = criteria;
    this.dbName = dbName;
    this.type = type;
  }

  /**
   * get predicate.
   *
   * @return created predicate for search
   */
  public BooleanExpression getPredicate() {
    final PathBuilder<T> entityPath = new PathBuilder<>(type, dbName);

    if (criteria.isLocalizedText()) {
      final MapPath<String, String, PathBuilder<String>> path =
          entityPath.getMap(criteria.getKey(), String.class, String.class);
      return path.containsValue(Objects.toString(criteria.getValue()));
    } else if (criteria.getValue() instanceof final Byte value) {
      final NumberPath<Byte> path = entityPath.getNumber(criteria.getKey(), Byte.class);
      return numberCompare(path, value);
    } else if (criteria.getValue() instanceof final Short value) {
      final NumberPath<Short> path = entityPath.getNumber(criteria.getKey(), Short.class);
      return numberCompare(path, value);
    } else if (criteria.getValue() instanceof final Integer value) {
      final NumberPath<Integer> path = entityPath.getNumber(criteria.getKey(), Integer.class);
      return numberCompare(path, value);
    } else if (criteria.getValue() instanceof final Long value) {
      final NumberPath<Long> path = entityPath.getNumber(criteria.getKey(), Long.class);
      return numberCompare(path, value);
    } else if (criteria.getValue() instanceof final Float value) {
      final NumberPath<Float> path = entityPath.getNumber(criteria.getKey(), Float.class);
      return numberCompare(path, value);
    } else if (criteria.getValue() instanceof final Double value) {
      final NumberPath<Double> path = entityPath.getNumber(criteria.getKey(), Double.class);
      return numberCompare(path, value);
    } else if (criteria.getValue() instanceof final BigDecimal value) {
      final NumberPath<BigDecimal> path = entityPath.getNumber(criteria.getKey(), BigDecimal.class);
      return numberCompare(path, value);
    } else if (criteria.getValue() instanceof final BigInteger value) {
      final NumberPath<BigInteger> path = entityPath.getNumber(criteria.getKey(), BigInteger.class);
      return numberCompare(path, value);
    } else if (criteria.getValue() instanceof final LocalTime value) {
      final TimePath<LocalTime> path = entityPath.getTime(criteria.getKey(), LocalTime.class);
      return temporalCompare(path, value);
    } else if (criteria.getValue() instanceof final LocalDateTime value) {
      final DateTimePath<LocalDateTime> path =
          entityPath.getDateTime(criteria.getKey(), LocalDateTime.class);
      return temporalCompare(path, value);
    } else if (criteria.getValue() instanceof final LocalDate value) {
      final DatePath<LocalDate> path = entityPath.getDate(criteria.getKey(), LocalDate.class);
      return temporalCompare(path, value);
    } else if (criteria.getValue() instanceof final Date value) {
      final DatePath<Date> path = entityPath.getDate(criteria.getKey(), Date.class);
      return temporalCompare(path, value);
    } else if (criteria.getValue() instanceof final Boolean value) {
      final BooleanPath booleanPath = entityPath.getBoolean(criteria.getKey());
      return booleanCompare(booleanPath, value);
    } else {
      return stringCompare(entityPath.getString(criteria.getKey()), criteria.getValue());
    }
  }

  protected BooleanExpression booleanCompare(final BooleanPath booleanPath, final Boolean value) {
    return switch (criteria.getOperation()) {
      case EQUALITY -> booleanPath.eq(value);
      case NEGATION -> booleanPath.ne(value);
      case GREATER_THEN -> booleanPath.gt(value);
      case GREATER_OR_EQUAL_THEN -> booleanPath.goe(value);
      case LESS_THEN -> booleanPath.lt(value);
      case LESS_OR_EQUAL_THEN -> booleanPath.loe(value);
      default -> null;
    };
  }

  protected BooleanExpression stringCompare(final StringPath stringPath, final Object value) {
    final String valueString = Objects.toString(value);
    return switch (criteria.getOperation()) {
      case EQUALITY -> stringPath.equalsIgnoreCase(valueString);
      case NEGATION -> stringPath.notEqualsIgnoreCase(valueString);
      case GREATER_THEN -> stringPath.gt(valueString);
      case GREATER_OR_EQUAL_THEN -> stringPath.goe(valueString);
      case LESS_THEN -> stringPath.lt(valueString);
      case LESS_OR_EQUAL_THEN -> stringPath.loe(valueString);
      case LIKE -> stringPath.like(valueString);
      case STARTS_WITH -> stringPath.like(valueString + "%");
      case ENDS_WITH -> stringPath.like("%" + valueString);
      case CONTAINS -> stringPath.like("%" + valueString + "%");
      default -> null;
    };
  }

  protected <D extends Comparable<?>> BooleanExpression temporalCompare(
      final TemporalExpression<D> temporalPath, final D value) {
    return switch (criteria.getOperation()) {
      case EQUALITY -> temporalPath.eq(value);
      case NEGATION -> temporalPath.ne(value);
      case GREATER_THEN -> temporalPath.gt(value);
      case GREATER_OR_EQUAL_THEN -> temporalPath.goe(value);
      case LESS_THEN -> temporalPath.lt(value);
      case LESS_OR_EQUAL_THEN -> temporalPath.loe(value);
      default -> null;
    };
  }

  protected <N extends Number & Comparable<?>> BooleanExpression numberCompare(
      final NumberPath<N> numberPath, final N value) {
    return switch (criteria.getOperation()) {
      case EQUALITY -> numberPath.eq(value);
      case NEGATION -> numberPath.ne(value);
      case GREATER_THEN -> numberPath.gt(value);
      case GREATER_OR_EQUAL_THEN -> numberPath.goe(value);
      case LESS_THEN -> numberPath.lt(value);
      case LESS_OR_EQUAL_THEN -> numberPath.loe(value);
      case LIKE -> numberPath.like(Objects.toString(value));
      case STARTS_WITH -> numberPath.like(Objects.toString(value) + "%");
      case ENDS_WITH -> numberPath.like("%" + Objects.toString(value));
      case CONTAINS -> numberPath.like("%" + Objects.toString(value) + "%");
      default -> null;
    };
  }

  protected <E extends Enum<E>> BooleanExpression enumCompare(final EnumPath<E> enumPath,
      final E value) {
    return switch (criteria.getOperation()) {
      case EQUALITY -> enumPath.eq(value);
      case NEGATION -> enumPath.ne(value);
      default -> null;
    };
  }

  public SearchCriteriaServer getCriteria() {
    return criteria;
  }

  public void setCriteria(final SearchCriteriaServer criteria) {
    this.criteria = criteria;
  }
}

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.gwtp.spring.server.security;

import de.knightsoftnet.validators.shared.ResourcePaths;

import org.apache.commons.lang3.StringUtils;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.stereotype.Component;
import org.springframework.web.util.WebUtils;

import java.io.IOException;
import java.util.Objects;

import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 * set csrf/xsrf cookie.
 *
 * @author Manfred Tremmel
 */
@Component
public class CsrfCookieHandler {

  /**
   * set csrf/xsrf cookie.
   *
   * @param request http servlet request
   * @param response http servlet response
   * @throws IOException when io operation fails
   */
  public void setCookie(final HttpServletRequest request, final HttpServletResponse response)
      throws IOException {
    final CsrfToken csrf = (CsrfToken) request.getAttribute(CsrfToken.class.getName());
    if (csrf != null) {
      Cookie cookie = WebUtils.getCookie(request, ResourcePaths.XSRF_COOKIE);
      final String token = csrf.getToken();
      if (cookie == null || token != null && !token.equals(cookie.getValue())) {
        cookie = new Cookie(ResourcePaths.XSRF_COOKIE, token);
        cookie.setPath(Objects.toString(StringUtils.trimToNull(request.getContextPath()), "/"));
        cookie.setSecure(request.isSecure());
        response.addCookie(cookie);
      }
    }
  }
}

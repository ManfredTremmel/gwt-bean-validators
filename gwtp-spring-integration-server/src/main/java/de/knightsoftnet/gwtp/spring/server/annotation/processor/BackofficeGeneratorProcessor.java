package de.knightsoftnet.gwtp.spring.server.annotation.processor;

import de.knightsoftnet.gwtp.spring.annotation.processor.AbstractBackofficeCreator;
import de.knightsoftnet.gwtp.spring.annotation.processor.BackofficeWidget;
import de.knightsoftnet.gwtp.spring.server.annotation.BackofficeGenerator;

import com.google.auto.service.AutoService;

import java.util.List;
import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Processor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.TypeElement;

/**
 * Backoffice generator processor.
 */
@SupportedAnnotationTypes("de.knightsoftnet.gwtp.spring.server.annotation.BackofficeGenerator")
@SupportedSourceVersion(SourceVersion.RELEASE_17)
@AutoService(Processor.class)
public class BackofficeGeneratorProcessor extends AbstractProcessor {

  private static final BackofficeRepositoryCreator REPOSITORY_CREATOR =
      new BackofficeRepositoryCreator();
  private static final BackofficeManyToOneWidgetRepositoryCreator REPOSITORY_MTO_WIDGET_CREATOR =
      new BackofficeManyToOneWidgetRepositoryCreator();
  private static final BackofficePredicateCreator PREDICATE_CREATOR =
      new BackofficePredicateCreator();
  private static final BackofficePredicateBuilderCreator PREDICATE_BUILDER_CREATOR =
      new BackofficePredicateBuilderCreator();
  private static final BackofficeControllerCreator CONTROLLER_CREATOR =
      new BackofficeControllerCreator();
  private static final BackofficeManyToOneWidgetControllerCreator CONTROLLER_MTO_WIDGET_CREATOR =
      new BackofficeManyToOneWidgetControllerCreator();

  @Override
  public boolean process(final Set<? extends TypeElement> annotations,
      final RoundEnvironment roundEnv) {
    if (annotations == null || annotations.isEmpty() || roundEnv == null || processingEnv == null) {
      return false;
    }
    if (!roundEnv.processingOver()) {
      roundEnv.getElementsAnnotatedWith(BackofficeGenerator.class).forEach(element -> {

        final BackofficeGenerator generator = element.getAnnotation(BackofficeGenerator.class);
        final List<BackofficeWidget> widgets = AbstractBackofficeCreator
            .detectBackofficeWidgetsOfElement("", null, element, true, processingEnv);

        if (generator.generateRepository()) {
          REPOSITORY_CREATOR.writeClassOrInterface(element, generator, widgets, processingEnv);
          REPOSITORY_MTO_WIDGET_CREATOR.writeClassOrInterface(element, generator, widgets,
              processingEnv);
        }

        if (generator.generatePredicate()) {
          PREDICATE_CREATOR.writeClassOrInterface(element, generator, widgets, processingEnv);
        }

        if (generator.generatePredicateBuilder()) {
          PREDICATE_BUILDER_CREATOR.writeClassOrInterface(element, generator, widgets,
              processingEnv);
        }

        if (generator.generateController()) {
          CONTROLLER_CREATOR.writeClassOrInterface(element, generator, widgets, processingEnv);
          CONTROLLER_MTO_WIDGET_CREATOR.writeClassOrInterface(element, generator, widgets,
              processingEnv);
        }
      });
    }

    return true;
  }
}

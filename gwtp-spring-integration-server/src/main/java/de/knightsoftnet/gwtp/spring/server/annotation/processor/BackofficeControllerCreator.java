package de.knightsoftnet.gwtp.spring.server.annotation.processor;

import de.knightsoftnet.gwtp.spring.annotation.processor.AbstractBackofficeCreator;
import de.knightsoftnet.gwtp.spring.annotation.processor.BackofficeWidget;
import de.knightsoftnet.gwtp.spring.server.annotation.BackofficeGenerator;

import java.io.PrintWriter;
import java.util.List;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;

/**
 * Create controller.
 */
public class BackofficeControllerCreator extends AbstractBackofficeCreator<BackofficeGenerator> {

  protected static final String CLASS_SUFFIX = "Controller";

  public BackofficeControllerCreator() {
    super(CLASS_SUFFIX);
  }

  @Override
  protected void addAdditionalImports(final String serverPackage, final Element element,
      final BackofficeGenerator annotationInterface, final List<BackofficeWidget> widgets,
      final ProcessingEnvironment processingEnv) {
    addImport(element.asType());
    addImports(//
        "de.knightsoftnet.gwtp.spring.server.controller.AbstractAdminController", //
        "org.springframework.http.MediaType", //
        "org.springframework.web.bind.annotation.RequestMapping", //
        "org.springframework.web.bind.annotation.RestController", //
        "jakarta.inject.Inject");
  }

  @Override
  protected void writeBody(final PrintWriter out, final String serverPackage, final Element element,
      final BackofficeGenerator annotationInterface, final List<BackofficeWidget> widgets,
      final ProcessingEnvironment processingEnv) {
    final String entityName = getEntityNameOfElement(element);

    out.println("@RestController");
    out.print("@RequestMapping(value = \"");
    out.print(annotationInterface.value());
    out.println("\", produces = MediaType.APPLICATION_JSON_VALUE)");

    out.print("public class ");
    out.print(entityName);
    out.println(CLASS_SUFFIX);
    out.print("    extends AbstractAdminController<");
    out.print(entityName);
    out.print(", ");
    out.print(entityName);
    out.print(BackofficeRepositoryCreator.CLASS_SUFFIX);
    out.print(", ");
    out.print(entityName);
    out.print(BackofficePredicateBuilderCreator.CLASS_SUFFIX);
    out.println("> {");

    out.println();
    out.println("  @Inject");
    out.print("  public ");
    out.print(entityName);
    out.print(CLASS_SUFFIX);
    out.print("(final ");
    out.print(entityName);
    out.print(BackofficeRepositoryCreator.CLASS_SUFFIX);
    out.println(" repository) {");
    out.print("    super(repository, new ");
    out.print(entityName);
    out.print(BackofficePredicateBuilderCreator.CLASS_SUFFIX);
    out.println("());");
    out.println("  }");

    out.println("}");
  }
}

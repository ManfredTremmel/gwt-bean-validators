/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.gwtp.spring.server.controller;

import de.knightsoftnet.gwtp.spring.server.jpa.AdminJpaRepository;
import de.knightsoftnet.gwtp.spring.server.querydsl.AbstractPredictatesBuilder;
import de.knightsoftnet.gwtp.spring.shared.data.AdminResult;
import de.knightsoftnet.validators.shared.Parameters;
import de.knightsoftnet.validators.shared.validationgroup.ValidationGroup.ServerControllerValidation;

import com.querydsl.core.types.dsl.BooleanExpression;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Persistable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import jakarta.servlet.http.HttpServletResponse;

/**
 * Abstract administration web service.
 *
 * @author Manfred Tremmel
 */
public class AbstractAdminController<T extends Persistable<Long>, //
    R extends AdminJpaRepository<T, Long, ?>, P extends AbstractPredictatesBuilder<?>> {

  private static final Logger LOG = LogManager.getLogger(AbstractAdminController.class);
  private static final String LINEBREAK = "[\r\n]";

  protected final R repository;
  protected final P predicatesBuilder;

  /**
   * constructor.
   */
  protected AbstractAdminController(final R repository, final P predicatesBuilder) {
    super();
    this.repository = Objects.requireNonNull(repository);
    this.predicatesBuilder = Objects.requireNonNull(predicatesBuilder);
  }

  /**
   * get first entry.
   *
   * @param response http servlet response
   * @return administration result with first entry
   */
  @GetMapping
  @Secured("ROLE_ADMIN")
  public AdminResult<Long, T> getEntry(final HttpServletResponse response) {
    final Optional<T> firstEntry = repository.findFirstByOrderByIdAsc();
    final Long firstId = !firstEntry.isPresent() ? null : firstEntry.get().getId();
    if (firstId != null) {
      return getEntryById(firstId, response);
    }
    response.setStatus(HttpServletResponse.SC_NOT_FOUND);
    return new AdminResult<>();
  }

  /**
   * get entry by id.
   *
   * @param id id of the entry to read
   * @param response http servlet response
   * @return administration result with first entry
   */
  @GetMapping(value = "/{" + Parameters.ID + "}")
  @Secured("ROLE_ADMIN")
  public AdminResult<Long, T> getEntryById(@PathVariable(value = Parameters.ID) final Long id,
      final HttpServletResponse response) {
    final Optional<T> currentEntry = repository.findById(id);
    if (!currentEntry.isPresent()) {
      response.setStatus(HttpServletResponse.SC_NOT_FOUND);
    }
    return new AdminResult<>(getId(currentEntry, repository.findFirstByOrderByIdAsc()),
        getId(currentEntry, repository.findFirstByIdLessThanOrderByIdDesc(id)),
        getId(currentEntry, repository.findFirstByIdGreaterThanOrderByIdAsc(id)),
        getId(currentEntry, repository.findFirstByOrderByIdDesc()), currentEntry.orElse(null));
  }

  /**
   * search in repository.
   *
   * @param search search query
   * @param pageable sort and paging informations
   * @return page list of entries
   */
  @GetMapping(value = "/" + Parameters.SEARCH + "/{" + Parameters.LANGUAGE + "}")
  @Secured("ROLE_ADMIN")
  public ResponseEntity<Page<T>> search(
      @PathVariable(value = Parameters.LANGUAGE) final String language,
      @RequestParam(value = Parameters.SEARCH) final String search, final Pageable pageable) {
    final Map<String, Comparator<T>> manualSortableFields = getManualSortableFields(language);
    final Collection<Comparator<T>> manualComparators =
        manualSortableFields.entrySet().stream().map(entry -> {
          final Order order = pageable.getSortOr(Sort.unsorted()).getOrderFor(entry.getKey());
          if (order == null) {
            return null;
          }
          if (order.isAscending()) {
            return entry.getValue();
          }
          return entry.getValue().reversed();
        }).filter(Objects::nonNull).toList();
    final BooleanExpression searchParsed = predicatesBuilder.parse(search).build();
    List<T> fullResultList;
    try {
      if (CollectionUtils.isEmpty(manualComparators)) {
        return ResponseEntity.ok(repository.findAll(searchParsed, pageable));
      }
      Stream<T> fullResult =
          StreamSupport.stream(repository.findAll(searchParsed).spliterator(), false);
      for (final Comparator<T> comparator : manualComparators) {
        fullResult = fullResult.sorted(comparator);
      }
      fullResultList = fullResult.toList();
    } catch (final IllegalArgumentException e) {
      LOG.error(StringUtils.defaultString(e.getMessage()).replaceAll(LINEBREAK, StringUtils.EMPTY),
          e);
      fullResultList = Collections.emptyList();
    }
    return ResponseEntity.ok(new PageImpl<>(
        fullResultList.subList(
            Math.min(Math.max((int) pageable.getOffset(), 0), fullResultList.size()),
            Math.min((int) (pageable.getOffset() + pageable.getPageSize()), fullResultList.size())),
        pageable, fullResultList.size()));
  }

  /**
   * create entry.
   *
   * @param entry entity to create
   * @param response http servlet response
   * @return administration result with first entry
   */
  @PostMapping
  @Secured("ROLE_ADMIN")
  public AdminResult<Long, T> createEntry(
      @Validated(ServerControllerValidation.class) @RequestBody final T entry,
      final HttpServletResponse response) {
    if (entry.isNew()) {
      final T savedEntry = repository.saveAndFlush(entry);
      final Long savedId =
          savedEntry == null || savedEntry.getId() == null ? null : savedEntry.getId();
      if (savedId == null) {
        return new AdminResult<>();
      }
      return getEntryById(savedId, response);
    }
    try {
      response.sendError(HttpServletResponse.SC_BAD_REQUEST);
    } catch (final IOException e) {
      LOG.error(StringUtils.defaultString(e.getMessage()).replaceAll(LINEBREAK, StringUtils.EMPTY),
          e);
    }
    return new AdminResult<>();
  }

  /**
   * change entry.
   *
   * @param id id of the entry to change
   * @param entry entity to change
   * @param response http servlet response
   * @return administration result with first entry
   */
  @PutMapping(value = "/{" + Parameters.ID + "}")
  @Secured("ROLE_ADMIN")
  public AdminResult<Long, T> changeEntry(@PathVariable(value = Parameters.ID) final Long id,
      @Validated(ServerControllerValidation.class) @RequestBody final T entry,
      final HttpServletResponse response) {
    final T savedEntry = repository.saveAndFlush(entry);
    final Long savedId =
        savedEntry == null || savedEntry.getId() == null ? null : savedEntry.getId();
    if (savedId == null || !Objects.equals(savedId, id)) {
      try {
        response.sendError(HttpServletResponse.SC_GONE);
      } catch (final IOException e) {
        LOG.error(
            StringUtils.defaultString(e.getMessage()).replaceAll(LINEBREAK, StringUtils.EMPTY), e);
      }
      return new AdminResult<>();
    }
    return getEntryById(savedId, response);
  }

  /**
   * delete entry.
   *
   * @param id id of the entry to delete
   * @param response http servlet response
   * @return administration result with first entry
   */
  @DeleteMapping(value = "/{" + Parameters.ID + "}")
  @Secured("ROLE_ADMIN")
  public AdminResult<Long, T> deleteEntry(@PathVariable(value = Parameters.ID) final Long id,
      final HttpServletResponse response) {
    final AdminResult<Long, T> entryToDelete = getEntryById(id, response);
    if (entryToDelete.getEntry() == null) {
      return entryToDelete;
    }
    try {
      repository.delete(entryToDelete.getEntry());
    } catch (final DataIntegrityViolationException se) {
      try {
        response.sendError(HttpServletResponse.SC_NOT_ACCEPTABLE);
        LOG.error("data integrity violation", se);
      } catch (final IOException e) {
        LOG.error(
            StringUtils.defaultString(e.getMessage()).replaceAll(LINEBREAK, StringUtils.EMPTY), e);
      }
    }
    return getEntryById(entryToDelete.getNavigation().getNextId() == null
        ? entryToDelete.getNavigation().getPreviousId()
        : entryToDelete.getNavigation().getNextId(), response);
  }

  private Long getId(final Optional<T> currentEntry, final Optional<T> checkEntry) {
    return !checkEntry.isPresent() || checkEntry.equals(currentEntry) ? null
        : checkEntry.get().getId();
  }

  protected Map<String, Comparator<T>> getManualSortableFields(final String local) {
    return Collections.emptyMap();
  }
}

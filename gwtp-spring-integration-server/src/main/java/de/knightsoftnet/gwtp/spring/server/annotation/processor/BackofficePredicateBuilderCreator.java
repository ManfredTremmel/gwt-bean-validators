package de.knightsoftnet.gwtp.spring.server.annotation.processor;

import de.knightsoftnet.gwtp.spring.annotation.processor.AbstractBackofficeCreator;
import de.knightsoftnet.gwtp.spring.annotation.processor.BackofficeWidget;
import de.knightsoftnet.gwtp.spring.server.annotation.BackofficeGenerator;
import de.knightsoftnet.validators.shared.data.FieldTypeEnum;

import java.io.PrintWriter;
import java.util.List;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeMirror;

/**
 * Create predicate builder.
 */
public class BackofficePredicateBuilderCreator
    extends AbstractBackofficeCreator<BackofficeGenerator> {

  private static final List<String> VALUE_OF_LIST = List.of(//
      "java.lang.Boolean", //
      "java.lang.Byte", //
      "java.lang.Short", //
      "java.lang.Integer", //
      "java.lang.Long", //
      "java.lang.Character", //
      "java.lang.Float", //
      "java.lang.Double");
  private static final List<String> PARSE_LIST = List.of(//
      "java.time.LocalTime", //
      "java.time.LocalDateTime", //
      "java.time.LocalDate", //
      "java.util.Date");
  private static final List<String> CONST_LIST = List.of(//
      "java.math.BigDecimal", //
      "java.math.BigInteger");
  protected static final String CLASS_SUFFIX = "PredicatesBuilder";

  public BackofficePredicateBuilderCreator() {
    super(CLASS_SUFFIX);
  }

  @Override
  protected void addAdditionalImports(final String serverPackage, final Element element,
      final BackofficeGenerator annotationInterface, final List<BackofficeWidget> widgets,
      final ProcessingEnvironment processingEnv) {
    addImports(//
        "de.knightsoftnet.gwtp.spring.server.querydsl.AbstractPredictatesBuilder", //
        "de.knightsoftnet.gwtp.spring.shared.search.SearchCriteriaServer");
    final List<BackofficeWidget> widgetList =
        widgetListRequireSearchCriteriaOverride(element, widgets, processingEnv);
    if (!widgetList.isEmpty()) {
      addImports("org.apache.commons.lang3.tuple.Triple", //
          "de.knightsoftnet.gwtp.spring.shared.search.SearchOperation");
      if (widgetList.stream()
          .anyMatch(widget -> widget.getFieldType() == FieldTypeEnum.ENUM_FIXED)) {
        addImport("org.apache.commons.lang3.StringUtils");
      }
    }
  }

  @Override
  protected void writeBody(final PrintWriter out, final String serverPackage, final Element element,
      final BackofficeGenerator annotationInterface, final List<BackofficeWidget> widgets,
      final ProcessingEnvironment processingEnv) {
    final String entityName = getEntityNameOfElement(element);

    out.print("public class ");
    out.print(entityName);
    out.print(suffix);
    out.print(" extends AbstractPredictatesBuilder<");
    out.print(entityName);
    out.print(BackofficePredicateCreator.CLASS_SUFFIX);
    out.println("> {");

    out.println();
    out.println("  @Override");
    out.print("  protected ");
    out.print(entityName);
    out.print(BackofficePredicateCreator.CLASS_SUFFIX);
    out.println(" createPredicate(final SearchCriteriaServer criteria) {");
    out.print("    return new ");
    out.print(entityName);
    out.print(BackofficePredicateCreator.CLASS_SUFFIX);
    out.println("(criteria);");
    out.println("  }");

    overwriteMapTripleToSearchCriteria(out, element, widgets, processingEnv);

    out.println("}");
  }

  private void overwriteMapTripleToSearchCriteria(final PrintWriter out, final Element element,
      final List<BackofficeWidget> widgets, final ProcessingEnvironment processingEnv) {

    final List<BackofficeWidget> widgetList =
        widgetListRequireSearchCriteriaOverride(element, widgets, processingEnv);

    if (widgetList.isEmpty()) {
      return;
    }

    out.println();
    out.println("  @Override");
    out.println("  protected SearchCriteriaServer mapTripleToSearchCriteria(");
    out.println("      final Triple<String, String, String> triple) {");
    out.println("    switch (triple.getLeft()) {");

    widgetList.forEach(widget -> {
      out.print("      case \"");
      out.print(widget.getName());
      out.println("\":");
      out.println("        return new SearchCriteriaServer(triple.getLeft(),");
      out.println("            SearchOperation.getSimpleOperation(triple.getMiddle().charAt(0)),");
      out.print("            ");

      if (widget.getFieldType() == FieldTypeEnum.ENUM_FIXED) {
        out.print(getEnumName(widget.getField().asType()));
        out.println(".valueOf(");
        out.println("                StringUtils.upperCase(triple.getRight())));");
      } else if (VALUE_OF_LIST.contains(
          detectFieldTypeName(widget.getContaining(), widget.getField(), processingEnv))) {
        out.print(detectFieldTypeName(widget.getContaining(), widget.getField(), processingEnv));
        out.println(".valueOf(triple.getRight()));");
      } else if (PARSE_LIST.contains(
          detectFieldTypeName(widget.getContaining(), widget.getField(), processingEnv))) {
        out.print(detectFieldTypeName(widget.getContaining(), widget.getField(), processingEnv));
        out.println(".parse(triple.getRight()));");
      } else {
        out.print(" new ");
        out.print(detectFieldTypeName(widget.getContaining(), widget.getField(), processingEnv));
        out.println("(triple.getRight()));");
      }
    });

    out.println("      default:");
    out.println("        return super.mapTripleToSearchCriteria(triple);");
    out.println("    }");
    out.println("  }");
  }

  protected List<BackofficeWidget> widgetListRequireSearchCriteriaOverride(final Element element,
      final List<BackofficeWidget> widgets, final ProcessingEnvironment processingEnv) {
    return detectBackofficeWidgetsOfElementFlat(widgets).stream()
        .filter(widget -> !widget.isIgnore() && (widget.getFieldType() == FieldTypeEnum.ENUM_FIXED
            || VALUE_OF_LIST.contains(
                detectFieldTypeName(widget.getContaining(), widget.getField(), processingEnv))
            || PARSE_LIST.contains(
                detectFieldTypeName(widget.getContaining(), widget.getField(), processingEnv))
            || CONST_LIST.contains(
                detectFieldTypeName(widget.getContaining(), widget.getField(), processingEnv))))
        .toList();
  }

  private String detectFieldTypeName(final DeclaredType parent, final Element field,
      final ProcessingEnvironment processingEnv) {
    final TypeMirror fieldType = processingEnv.getTypeUtils().asMemberOf(parent, field);
    if (fieldType.getKind().isPrimitive()) {
      return processingEnv.getTypeUtils()
          .boxedClass(processingEnv.getTypeUtils().getPrimitiveType(fieldType.getKind()))
          .toString();
    } else {
      return fieldType.toString();
    }
  }
}

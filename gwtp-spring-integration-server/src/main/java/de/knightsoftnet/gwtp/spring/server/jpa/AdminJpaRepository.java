/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.gwtp.spring.server.jpa;

import com.querydsl.core.types.EntityPath;
import com.querydsl.core.types.dsl.StringExpression;
import com.querydsl.core.types.dsl.StringPath;

import org.springframework.data.domain.Persistable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;
import org.springframework.data.querydsl.binding.SingleValueBinding;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.Optional;

/**
 * extended jpa repository interface with search and navigation functionality.
 *
 * @author Manfred Tremmel
 *
 * @param <T> persistable entity
 * @param <I> key type
 * @param <Q> query dsl generated class corespondint to entity
 */
@NoRepositoryBean
public interface AdminJpaRepository<T extends Persistable<?>, I, Q extends EntityPath<?>>
    extends JpaRepository<T, I>, QuerydslPredicateExecutor<T>, QuerydslBinderCustomizer<Q> {

  /**
   * Retrieves the first entity by id in database.
   *
   * @return the first entity in database {@literal Optional#empty()} if none found
   * @throws IllegalArgumentException if {@code id} is {@literal null}.
   */
  Optional<T> findFirstByOrderByIdAsc();

  /**
   * Retrieves the last entity by id in database.
   *
   * @return the first entity in database {@literal Optional#empty()} if none found
   * @throws IllegalArgumentException if {@code id} is {@literal null}.
   */
  Optional<T> findFirstByOrderByIdDesc();

  /**
   * Retrieves the next entity by its id.
   *
   * @param id must not be {@literal null}.
   * @return the entity with the lowest id which is higher then the given id or
   *         {@literal Optional#empty()} if none found
   * @throws IllegalArgumentException if {@code id} is {@literal null}.
   */
  Optional<T> findFirstByIdGreaterThanOrderByIdAsc(I id);


  /**
   * Retrieves the previous entity by its id.
   *
   * @param id must not be {@literal null}.
   * @return the entity with the highest id which is lower then the given id or
   *         {@literal Optional#empty()} if none found
   * @throws IllegalArgumentException if {@code id} is {@literal null}.
   */
  Optional<T> findFirstByIdLessThanOrderByIdDesc(I id);

  @Override
  default void customize(final QuerydslBindings bindings, final Q root) {
    bindings.bind(String.class)
        .first((SingleValueBinding<StringPath, String>) StringExpression::containsIgnoreCase);
  }
}

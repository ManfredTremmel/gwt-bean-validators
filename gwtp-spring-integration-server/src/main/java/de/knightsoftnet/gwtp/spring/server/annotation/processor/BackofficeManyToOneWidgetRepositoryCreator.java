package de.knightsoftnet.gwtp.spring.server.annotation.processor;

import de.knightsoftnet.gwtp.spring.annotation.processor.AbstractBackofficeManyToOneWidgetCreator;
import de.knightsoftnet.gwtp.spring.annotation.processor.BackofficeWidget;
import de.knightsoftnet.gwtp.spring.server.annotation.BackofficeGenerator;

import java.io.PrintWriter;
import java.util.List;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;

/**
 * Create JPA repository interface for many to one reference widgets.
 */
public class BackofficeManyToOneWidgetRepositoryCreator
    extends AbstractBackofficeManyToOneWidgetCreator<BackofficeGenerator> {

  protected static final String CLASS_SUFFIX = "Repository";

  public BackofficeManyToOneWidgetRepositoryCreator() {
    super(CLASS_SUFFIX);
  }

  @Override
  protected void addAdditionalImports(final String serverPackage, final Element element,
      final BackofficeGenerator annotationInterface, final List<BackofficeWidget> widgets,
      final ProcessingEnvironment processingEnv) {
    addImport(element.asType());
    addImports("org.springframework.data.jpa.repository.JpaRepository", //
        "java.util.List");
  }

  @Override
  protected void writeBody(final Element element, final BackofficeGenerator annotationInterface,
      final List<BackofficeWidget> widgets, final String keyfield, final String valuefield,
      final String entityType, final String entityName, final PrintWriter out) {
    out.println();

    out.print("public interface ");
    out.print(entityType);
    out.print(suffix);
    out.print(" extends JpaRepository<");
    out.print(entityName);
    out.print(", Long");
    out.println("> {");

    out.print("  ");
    out.print(entityName);
    out.print(" getReferenceBy");
    out.print(fieldNameFirstUper(keyfield));
    out.print("(String ");
    out.print(keyfield);
    out.println(");");

    out.print("  List<");
    out.print(entityName);
    out.print("> findBy");
    out.print(fieldNameFirstUper(keyfield));
    out.print("Containing(String ");
    out.print(keyfield);
    out.println(");");

    out.println("}");
  }
}

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.gwtp.spring.server.querydsl;

import de.knightsoftnet.gwtp.spring.shared.search.SearchCriteriaServer;
import de.knightsoftnet.gwtp.spring.shared.search.SearchOperation;

import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.Expressions;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Triple;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * abstract predicate builder for query dsl searches.
 *
 * @author Manfred Tremmel
 *
 * @param <T> type of the predicate
 */
public abstract class AbstractPredictatesBuilder<T extends AbstractPredicate<?>> {

  private static final Logger LOG = LogManager.getLogger(AbstractPredictatesBuilder.class);

  protected final List<SearchCriteriaServer> params;

  protected AbstractPredictatesBuilder() {
    params = new ArrayList<>();
  }

  /**
   * predicate builder with operation.
   *
   * @param key field to search for
   * @param operation search operation to use
   * @param value the value to search for
   * @return this predicate builder
   */
  public AbstractPredictatesBuilder<T> with(final String key, final String operation,
      final Object value) {
    params.add(new SearchCriteriaServer(key,
        SearchOperation.getSimpleOperation(operation.charAt(0)), value));
    return this;
  }

  /**
   * predicate builder with search string to parse.
   *
   * @param search string with key, operation and value
   * @return this predicate builder
   */
  public AbstractPredictatesBuilder<T> parse(final String search) {
    params.clear();
    params.addAll(splitSearch(search).stream().map(this::mapTripleToSearchCriteria)
        .filter(Objects::nonNull).toList());
    return this;
  }

  protected List<Triple<String, String, String>> splitSearch(final String search) {
    if (StringUtils.isEmpty(search)) {
      return Collections.emptyList();
    }

    final Pattern pattern = Pattern.compile(
        "([a-zA-Z_0-9\\.]+?)(" + SearchOperation.getAllOperationsAsRegExString() + ")([^,]+?),");
    final Matcher matcher = pattern.matcher(search + ",");
    final List<Triple<String, String, String>> result = new ArrayList<>();
    while (matcher.find()) {
      result.add(Triple.of(matcher.group(1), matcher.group(2), matcher.group(3)));
    }
    return result;
  }

  protected SearchCriteriaServer mapTripleToSearchCriteria(
      final Triple<String, String, String> triple) {
    final String searchString = triple.getRight();
    final Object searchValue;
    try {
      if ("true".equalsIgnoreCase(searchString)) {
        searchValue = Boolean.TRUE;
      } else if ("false".equalsIgnoreCase(searchString)) {
        searchValue = Boolean.FALSE;
      } else if (searchString
          .matches("^[0-9]{4}-[01][0-9]-[0-3][0-9]T[012][0-9]:[0-5][0-9]:[0-5][0-9]$")) {
        final DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        searchValue = formatter.parse(searchString);
      } else if (searchString.matches("^[0-9]{4}-[01][0-9]-[0-3][0-9]T[012][0-9]:[0-5][0-9]$")) {
        final DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
        searchValue = formatter.parse(searchString);
      } else if (searchString.matches("^[0-9]{4}-[01][0-9]-[0-3][0-9]$")) {
        final DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        searchValue = formatter.parse(searchString);
      } else if (searchString.matches("^[012][0-9]:[0-5][0-9]:[0-5][0-9]$")) {
        final DateFormat formatter = new SimpleDateFormat("HH:mm:ss");
        searchValue = formatter.parse(searchString);
      } else if (searchString.matches("^[012][0-9]:[0-5][0-9]$")) {
        final DateFormat formatter = new SimpleDateFormat("HH:mm");
        searchValue = formatter.parse(searchString);
      } else {
        searchValue = StringUtils.replaceChars(searchString, '，', ',');
      }
      return new SearchCriteriaServer(triple.getLeft(),
          SearchOperation.getSimpleOperation(triple.getMiddle().charAt(0)), searchValue);
    } catch (final ParseException e) {
      LOG.error(StringUtils.defaultString(e.getMessage()).replaceAll("[\r\n]", StringUtils.EMPTY),
          e);
      return null;
    }
  }

  /**
   * build the expression.
   *
   * @return BooleanExpression for search
   */
  public BooleanExpression build() {
    if (params.isEmpty()) {
      return null;
    }

    final List<BooleanExpression> predicates = params.stream().map(param -> {
      final T predicate = createPredicate(param);
      return predicate.getPredicate();
    }).filter(Objects::nonNull).toList();

    BooleanExpression result = Expressions.asBoolean(true).isTrue();
    for (final BooleanExpression predicate : predicates) {
      result = result.and(predicate);
    }
    return result;
  }

  protected abstract T createPredicate(final SearchCriteriaServer criteria);

  static class BooleanExpressionWrapper {

    private BooleanExpression result;

    public BooleanExpressionWrapper(final BooleanExpression result) {
      super();
      this.result = result;
    }

    public BooleanExpression getResult() {
      return result;
    }

    public void setResult(final BooleanExpression result) {
      this.result = result;
    }
  }
}

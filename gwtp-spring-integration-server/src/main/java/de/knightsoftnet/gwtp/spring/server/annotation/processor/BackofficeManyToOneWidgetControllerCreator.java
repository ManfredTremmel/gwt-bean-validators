package de.knightsoftnet.gwtp.spring.server.annotation.processor;

import de.knightsoftnet.gwtp.spring.annotation.processor.AbstractBackofficeManyToOneWidgetCreator;
import de.knightsoftnet.gwtp.spring.annotation.processor.BackofficeWidget;
import de.knightsoftnet.gwtp.spring.server.annotation.BackofficeGenerator;

import org.apache.commons.lang3.StringUtils;

import java.io.PrintWriter;
import java.util.List;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;

/**
 * Create JPA repository interface for many to one reference widgets.
 */
public class BackofficeManyToOneWidgetControllerCreator
    extends AbstractBackofficeManyToOneWidgetCreator<BackofficeGenerator> {

  protected static final String CLASS_SUFFIX = "Controller";

  public BackofficeManyToOneWidgetControllerCreator() {
    super(CLASS_SUFFIX);
  }

  @Override
  protected void addAdditionalImports(final String serverPackage, final Element element,
      final BackofficeGenerator annotationInterface, final List<BackofficeWidget> widgets,
      final ProcessingEnvironment processingEnv) {
    addImport(element.asType());
    addImports("org.springframework.http.MediaType", //
        "org.springframework.web.bind.annotation.GetMapping", //
        "org.springframework.web.bind.annotation.PathVariable", //
        "org.springframework.web.bind.annotation.RequestMapping", //
        "org.springframework.web.bind.annotation.RestController", //
        "java.util.List", //
        "jakarta.inject.Inject", //
        "jakarta.validation.constraints.NotEmpty");
  }

  @Override
  protected void writeBody(final Element element, final BackofficeGenerator annotationInterface,
      final List<BackofficeWidget> widgets, final String keyfield, final String valuefield,
      final String entityType, final String entityName, final PrintWriter out) {
    out.println();

    out.println("@RestController");
    out.print("@RequestMapping(value = \"");
    out.print(StringUtils.replace(annotationInterface.value(),
        StringUtils.lowerCase(getEntityNameOfElement(element)), StringUtils.lowerCase(entityType)));
    out.println("\", produces = MediaType.APPLICATION_JSON_VALUE)");
    out.print("public class ");
    out.print(entityType);
    out.print(suffix);
    out.println(" {");

    out.println();

    out.print("  private final ");
    out.print(entityType);
    out.print(BackofficeRepositoryCreator.CLASS_SUFFIX);
    out.println(" repository;");

    out.println();

    out.println("  @Inject");
    out.print("  public ");
    out.print(entityType);
    out.print(suffix);
    out.print("(final ");
    out.print(entityType);
    out.print(BackofficeRepositoryCreator.CLASS_SUFFIX);
    out.println(" repository) {");
    out.println("    super();");
    out.println("    this.repository = repository;");
    out.println("  }");

    out.println();

    out.print("  @GetMapping(value = \"/{");
    out.print(StringUtils.lowerCase(keyfield));
    out.println("}\")");
    out.print("  public ");
    out.print(entityName);
    out.print(" getReferenceBy");
    out.print(fieldNameFirstUper(keyfield));
    out.println("(");
    out.print("      @PathVariable(value = \"");
    out.print(keyfield);
    out.print("\") @NotEmpty final String ");
    out.print(keyfield);
    out.println(") {");
    out.print("    return repository.getReferenceBy");
    out.print(fieldNameFirstUper(keyfield));
    out.print("(");
    out.print(keyfield);
    out.println(");");
    out.println("  }");

    out.println();

    out.print("  @GetMapping(value = \"/suggestions/{");
    out.print(StringUtils.lowerCase(keyfield));
    out.println("}\")");
    out.print("  public List<");
    out.print(entityName);
    out.print("> findBy");
    out.print(fieldNameFirstUper(keyfield));
    out.println("Containing(");
    out.print("      @PathVariable(value = \"");
    out.print(keyfield);
    out.print("\") @NotEmpty final String ");
    out.print(keyfield);
    out.println(") {");
    out.print("    return repository.findBy");
    out.print(fieldNameFirstUper(keyfield));
    out.print("Containing(");
    out.print(keyfield);
    out.println(");");
    out.println("  }");
    out.println("}");
  }
}

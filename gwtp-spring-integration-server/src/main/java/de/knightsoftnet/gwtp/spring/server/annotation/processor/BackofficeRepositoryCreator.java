package de.knightsoftnet.gwtp.spring.server.annotation.processor;

import de.knightsoftnet.gwtp.spring.annotation.processor.AbstractBackofficeCreator;
import de.knightsoftnet.gwtp.spring.annotation.processor.BackofficeWidget;
import de.knightsoftnet.gwtp.spring.server.annotation.BackofficeGenerator;

import java.io.PrintWriter;
import java.util.List;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;

/**
 * Create JPA repository interface.
 */
public class BackofficeRepositoryCreator extends AbstractBackofficeCreator<BackofficeGenerator> {

  protected static final String CLASS_SUFFIX = "Repository";

  public BackofficeRepositoryCreator() {
    super(CLASS_SUFFIX);
  }

  @Override
  protected void addAdditionalImports(final String serverPackage, final Element element,
      final BackofficeGenerator annotationInterface, final List<BackofficeWidget> widgets,
      final ProcessingEnvironment processingEnv) {
    addImport(element.asType());
    addImports(//
        getPackageOfElement(element) + ".Q" + getEntityNameOfElement(element), //
        "de.knightsoftnet.gwtp.spring.server.jpa.AdminJpaRepository");
  }

  @Override
  protected void writeBody(final PrintWriter out, final String serverPackage, final Element element,
      final BackofficeGenerator annotationInterface, final List<BackofficeWidget> widgets,
      final ProcessingEnvironment processingEnv) {
    final String entityName = getEntityNameOfElement(element);
    out.print("public interface ");
    out.print(entityName);
    out.print(suffix);
    out.print(" extends AdminJpaRepository<");
    out.print(entityName);
    out.print(", Long, Q");
    out.print(entityName);
    out.println("> {");
    out.println("}");
  }
}

gwt-bean-validators-spring-gwtp
==================================

This package contains the rest service based on spring rest controller and gwtp rest dispatcher for phone number and bank data. It also overrides PhoneNumberUtil and IbanUtil to get data by rest for validation, so it's no longer compiled into client Java-Script code and keeps client side code small. 

Metapackage
-----------

The package itself contains no code, it's a meta package for:
* [gwt-bean-validators-rest-resolver](../gwt-bean-validators-rest-resolver)
* [gwt-bean-validators-restservice-spring](../gwt-bean-validators-restservice-spring)


Maven integraten
----------------

The dependency itself for GWT-Projects:

```xml
    <dependency>
      <groupId>de.knightsoft-net</groupId>
      <artifactId>gwt-bean-validators-spring-gwtp</artifactId>
      <version>2.4.1</version>
    </dependency>
```

GWT Integration
---------------

```xml
<inherits name="de.knightsoftnet.validators.GwtBeanValidatorsSpringGwtp" />
```

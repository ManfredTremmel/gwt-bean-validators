/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.client;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;

import de.knightsoftnet.validators.client.factories.ValidatorFactory;
import de.knightsoftnet.validators.client.impl.Validation;

import org.gwtproject.core.shared.GWT;

import java.util.Set;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validator;

/**
 * J2CL JUnit <b>integration</b> tests. Abstract validation test.
 *
 * @author Manfred Tremmel
 *
 * @param <E> type of bean to test
 */
public abstract class AbstractValidationTst<E> {

  protected static final String SIZE_VALIDATOR = "org.hibernate.validator.internal."
      + "constraintvalidators.bv.size.SizeValidatorForCharSequence";
  protected static final String DIGITS_VALIDATOR = "org.hibernate.validator.internal."
      + "constraintvalidators.bv.DigitsValidatorForCharSequence";

  /**
   * test validation.
   *
   * @param bean the bean to test
   * @param shouldBeOk true if it's expected, that the test brings no validation error
   * @param excpetedValidationClass the validator class that will report an error
   * @param groups validation group(s)
   * @return violation list for additional tests
   */
  public final Set<ConstraintViolation<E>> validationTest(final E bean, final boolean shouldBeOk,
      final String excpetedValidationClass, final Class<?>... groups) {
    final Validator validator =
        Validation.buildGwtValidatorFactory(new ValidatorFactory()).getValidator();

    final Set<ConstraintViolation<E>> cv1 = validator.validate(bean, groups);

    if (shouldBeOk) {
      assertTrue("Should have no validation error " + bean.toString(), cv1.isEmpty());
    } else {
      assertFalse("Should have a validation error " + bean.toString(), cv1.isEmpty());
    }
    for (final ConstraintViolation<E> violation : cv1) {
      if (violation.getConstraintDescriptor().getConstraintValidatorClasses() != null
          && !violation.getConstraintDescriptor().getConstraintValidatorClasses().isEmpty()
          && excpetedValidationClass != null) {
        assertEquals("Should be reported by special validator", excpetedValidationClass,
            violation.getConstraintDescriptor().getConstraintValidatorClasses().get(0).getName());
      }
      GWT.log("Error Message of type "
          + violation.getConstraintDescriptor().getConstraintValidatorClasses() + " for field \""
          + violation.getPropertyPath().toString() + "\" with value \"" + bean.toString()
          + "\", message: " + violation.getMessage());
    }
    return cv1;
  }
}

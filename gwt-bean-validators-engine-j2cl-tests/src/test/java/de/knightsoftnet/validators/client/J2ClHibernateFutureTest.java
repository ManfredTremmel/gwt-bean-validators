/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.client;

import de.knightsoftnet.validators.shared.beans.HibernateFutureTestBean;
import de.knightsoftnet.validators.shared.testcases.HibernateFutureTestCases;

import com.google.j2cl.junit.apt.J2clTestInput;

import org.junit.Test;

/**
 * test for hibernate future validator.
 *
 * @author Manfred Tremmel
 *
 */
@J2clTestInput(J2ClHibernateFutureTest.class)
public class J2ClHibernateFutureTest extends AbstractValidationTst<HibernateFutureTestBean> {

  /**
   * empty future is allowed.
   */
  @Test
  public final void testEmptyFutureIsAllowed() {
    validationTest(HibernateFutureTestCases.getEmptyTestBean(), true, null);
  }

  /**
   * correct future is allowed.
   */
  @Test
  public final void testCorrectFutureAreAllowed() {
    for (final HibernateFutureTestBean testBean : HibernateFutureTestCases.getCorrectTestBeans()) {
      validationTest(testBean, true, null);
    }
  }

  /**
   * wrong future is not allowed.
   */
  @Test
  public final void testWrongFutureAreWrong() {
    for (final HibernateFutureTestBean testBean : HibernateFutureTestCases
        .getWrongtoSmallTestBeans()) {
      validationTest(testBean, false,
          "org.hibernate.validator.internal.constraintvalidators.bv.time.future."
              + "FutureValidatorForDate");
    }
  }
}

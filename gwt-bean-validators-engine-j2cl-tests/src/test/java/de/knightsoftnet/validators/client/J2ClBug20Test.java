/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.client;

import de.knightsoftnet.validators.shared.beans.Bug20TestBean;
import de.knightsoftnet.validators.shared.beans.Rule;
import de.knightsoftnet.validators.shared.testcases.Bug20TestCases;

import com.google.j2cl.junit.apt.J2clTestInput;

import org.junit.Test;

/**
 * test for bug 20 validator.
 *
 * @author Manfred Tremmel
 *
 */
@J2clTestInput(J2ClBug20Test.class)
public class J2ClBug20Test extends AbstractValidationTst<Bug20TestBean> {

  /**
   * correct not null cases are allowed.
   */
  @Test
  public final void testCorrectNotNullAreAllowed() {
    for (final Bug20TestBean testBean : Bug20TestCases.getCorrectTestBeans()) {
      validationTest(testBean, true, null, Rule.Conditions.class);
    }
  }

  /**
   * wrong null are not allowed.
   */
  @Test
  public final void testWrongNullAreWrong() {
    for (final Bug20TestBean testBean : Bug20TestCases.getWrongNullTestBeans()) {
      validationTest(testBean, false,
          "org.hibernate.validator.internal.constraintvalidators.bv.NotNullValidator",
          Rule.Conditions.class);
    }
  }

  /**
   * wrong null with other group checked is allowed.
   */
  @Test
  public final void testWrongNullWithOtherGroupIsAllowed() {
    for (final Bug20TestBean testBean : Bug20TestCases.getWrongNullTestBeans()) {
      validationTest(testBean, true, null, Rule.Other.class);
    }
  }
}

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.client.factories;

import de.knightsoftnet.validators.client.AbstractGwtValidatorFactory;
import de.knightsoftnet.validators.client.GwtValidation;
import de.knightsoftnet.validators.client.impl.AbstractGwtValidator;
import de.knightsoftnet.validators.shared.beans.Bar;
import de.knightsoftnet.validators.shared.beans.BarBar;
import de.knightsoftnet.validators.shared.beans.Bug17TestBean;
import de.knightsoftnet.validators.shared.beans.Bug17bTestBean;
import de.knightsoftnet.validators.shared.beans.Bug17cTestBean;
import de.knightsoftnet.validators.shared.beans.Bug20TestBean;
import de.knightsoftnet.validators.shared.beans.Bug25TestBean;
import de.knightsoftnet.validators.shared.beans.Bug27BTestBeanF;
import de.knightsoftnet.validators.shared.beans.Bug27TestBeanA;
import de.knightsoftnet.validators.shared.beans.Bug28TestBean;
import de.knightsoftnet.validators.shared.beans.Foo;
import de.knightsoftnet.validators.shared.beans.FooFoo;
import de.knightsoftnet.validators.shared.beans.HibernateAssertFalseTestBean;
import de.knightsoftnet.validators.shared.beans.HibernateAssertTrueTestBean;
import de.knightsoftnet.validators.shared.beans.HibernateCnpjTestBean;
import de.knightsoftnet.validators.shared.beans.HibernateCodePointLengthTestBean;
import de.knightsoftnet.validators.shared.beans.HibernateCpfTestBean;
import de.knightsoftnet.validators.shared.beans.HibernateCreditCardNumberTestBean;
import de.knightsoftnet.validators.shared.beans.HibernateDecimalMinMaxTestBean;
import de.knightsoftnet.validators.shared.beans.HibernateDigitsTestBean;
import de.knightsoftnet.validators.shared.beans.HibernateEan13TestBean;
import de.knightsoftnet.validators.shared.beans.HibernateEan8TestBean;
import de.knightsoftnet.validators.shared.beans.HibernateEmailTestBean;
import de.knightsoftnet.validators.shared.beans.HibernateFutureOrPresentTestBean;
import de.knightsoftnet.validators.shared.beans.HibernateFutureTestBean;
import de.knightsoftnet.validators.shared.beans.HibernateInnTestBean;
import de.knightsoftnet.validators.shared.beans.HibernateIsbn10TestBean;
import de.knightsoftnet.validators.shared.beans.HibernateIsbn13TestBean;
import de.knightsoftnet.validators.shared.beans.HibernateLengthTestBean;
import de.knightsoftnet.validators.shared.beans.HibernateLuhnCheckTestBean;
import de.knightsoftnet.validators.shared.beans.HibernateMinMaxPrimitiveTestBean;
import de.knightsoftnet.validators.shared.beans.HibernateMinMaxTestBean;
import de.knightsoftnet.validators.shared.beans.HibernateMod10CheckTestBean;
import de.knightsoftnet.validators.shared.beans.HibernateMod11CheckTestBean;
import de.knightsoftnet.validators.shared.beans.HibernateNegativeOrZeroTestBean;
import de.knightsoftnet.validators.shared.beans.HibernateNegativeTestBean;
import de.knightsoftnet.validators.shared.beans.HibernateNipTestBean;
import de.knightsoftnet.validators.shared.beans.HibernateNotBlankTestBean;
import de.knightsoftnet.validators.shared.beans.HibernateNotEmptyBug11TestBean;
import de.knightsoftnet.validators.shared.beans.HibernateNotEmptyBug25TestBean;
import de.knightsoftnet.validators.shared.beans.HibernateNotEmptyTestBean;
import de.knightsoftnet.validators.shared.beans.HibernateNotNullTestBean;
import de.knightsoftnet.validators.shared.beans.HibernateNullTestBean;
import de.knightsoftnet.validators.shared.beans.HibernatePastOrPresentTestBean;
import de.knightsoftnet.validators.shared.beans.HibernatePastTestBean;
import de.knightsoftnet.validators.shared.beans.HibernatePatternBug10TestBean;
import de.knightsoftnet.validators.shared.beans.HibernatePatternTestBean;
import de.knightsoftnet.validators.shared.beans.HibernatePeselTestBean;
import de.knightsoftnet.validators.shared.beans.HibernatePositiveOrZeroTestBean;
import de.knightsoftnet.validators.shared.beans.HibernatePositiveTestBean;
import de.knightsoftnet.validators.shared.beans.HibernateRangeTestBean;
import de.knightsoftnet.validators.shared.beans.HibernateRegonTestBean;
import de.knightsoftnet.validators.shared.beans.HibernateSizeArrayTestBean;
import de.knightsoftnet.validators.shared.beans.HibernateSizeCollectionTestBean;
import de.knightsoftnet.validators.shared.beans.HibernateSizeTestBean;
import de.knightsoftnet.validators.shared.beans.HibernateTituloEleitoralTestBean;
import de.knightsoftnet.validators.shared.beans.HibernateUniqueElementsTestBean;
import de.knightsoftnet.validators.shared.beans.HibernateUrlTestBean;
import de.knightsoftnet.validators.shared.beans.HibernateUuidTestBean;
import de.knightsoftnet.validators.shared.beans.IdType;
import de.knightsoftnet.validators.shared.beans.MyModel;
import de.knightsoftnet.validators.shared.beans.NestedClass2TestBean;
import de.knightsoftnet.validators.shared.beans.NestedClass3TestBean;
import de.knightsoftnet.validators.shared.beans.NestedClassTestBean;
import de.knightsoftnet.validators.shared.beans.Rule;

import jakarta.validation.Validator;
import jakarta.validation.groups.Default;

/**
 * The <code>ValidatorFactory</code> class is used for client side validation by annotation. All
 * beans which should be checked by bean validators have to be added to @GwtValidation annotation.
 *
 * @author Manfred Tremmel
 *
 */
public class ValidatorFactory extends AbstractGwtValidatorFactory {

  /**
   * Validator marker for the Validation Sample project. Only the classes and groups listed in the
   * {@link GwtValidation} annotation can be validated.
   */
  @GwtValidation(value = {

      // test beans for hibernate validators
      HibernateAssertFalseTestBean.class, HibernateAssertTrueTestBean.class,
      HibernateCodePointLengthTestBean.class, HibernateDecimalMinMaxTestBean.class,
      HibernateDigitsTestBean.class, HibernateEmailTestBean.class, HibernateLuhnCheckTestBean.class,
      HibernateNegativeTestBean.class, HibernateNegativeOrZeroTestBean.class,
      HibernateNotNullTestBean.class, HibernateNullTestBean.class, HibernatePatternTestBean.class,
      HibernateMinMaxTestBean.class, HibernateMinMaxPrimitiveTestBean.class,
      HibernateSizeTestBean.class, HibernateSizeArrayTestBean.class,
      HibernateSizeCollectionTestBean.class, HibernateFutureTestBean.class,
      HibernateFutureOrPresentTestBean.class, HibernateNotBlankTestBean.class,
      HibernateNotEmptyTestBean.class, HibernatePastTestBean.class,
      HibernatePastOrPresentTestBean.class, HibernatePositiveTestBean.class,
      HibernatePositiveOrZeroTestBean.class, HibernateEan8TestBean.class,
      HibernateEan13TestBean.class, HibernateLengthTestBean.class,
      HibernateCreditCardNumberTestBean.class, HibernateUrlTestBean.class,
      HibernateCnpjTestBean.class, HibernateCpfTestBean.class, HibernateNipTestBean.class,
      HibernatePeselTestBean.class, HibernateRegonTestBean.class, HibernateMod10CheckTestBean.class,
      HibernateMod11CheckTestBean.class, HibernateRangeTestBean.class,
      HibernateUniqueElementsTestBean.class, HibernateTituloEleitoralTestBean.class,
      HibernateIsbn10TestBean.class, HibernateIsbn13TestBean.class,
      HibernatePatternBug10TestBean.class, HibernateNotEmptyBug11TestBean.class,
      HibernateInnTestBean.class, HibernateUuidTestBean.class,

      // test beans from https://github.com/gwtproject/gwt/issues/7263
      IdType.class, Foo.class, Bar.class, FooFoo.class, BarBar.class,

      // test beans from https://gitlab.com/ManfredTremmel/gwt-bean-validators/-/issues/16
      NestedClassTestBean.NestedClass.class, NestedClass2TestBean.class, NestedClass3TestBean.class,

      // test beans from https://gitlab.com/ManfredTremmel/gwt-bean-validators/-/issues/17
      Bug17TestBean.class, Bug17bTestBean.class, Bug17cTestBean.class,

      // test beans from https://gitlab.com/ManfredTremmel/gwt-bean-validators/-/issues/20
      Bug20TestBean.class,

      // test beans from https://gitlab.com/ManfredTremmel/gwt-bean-validators/-/issues/25
      Bug25TestBean.class, HibernateNotEmptyBug25TestBean.class,

      // gitter reported class
      MyModel.class,

      // test beans from https://gitlab.com/ManfredTremmel/gwt-bean-validators/-/issues/27
      Bug27TestBeanA.class, Bug27BTestBeanF.class,

      // test beans from https://gitlab.com/ManfredTremmel/gwt-bean-validators/-/issues/28
      Bug28TestBean.class

  }, groups = {Default.class, Rule.Conditions.class, Rule.All.class, Rule.Other.class},
      forceUsingGetter = true, generateReflectionGetter = false,
      generateValidatorFactoryInterface = false)
  public interface GwtValidator extends Validator {
  }

  @Override
  public final AbstractGwtValidator createValidator() {
    return new GwtValidatorImpl();
  }
}

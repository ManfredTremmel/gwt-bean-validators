/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.client;

import de.knightsoftnet.validators.shared.beans.HibernateNotEmptyBug25TestBean;
import de.knightsoftnet.validators.shared.testcases.HibernateNotEmptyBug25TestCases;

import com.google.j2cl.junit.apt.J2clTestInput;

import org.junit.Test;

/**
 * test for hibernate not empty property validator.
 *
 * @author Manfred Tremmel
 *
 */
@J2clTestInput(J2ClHibernateNotEmptyBug25Test.class)
public class J2ClHibernateNotEmptyBug25Test
    extends AbstractValidationTst<HibernateNotEmptyBug25TestBean> {

  /**
   * empty not empty is not allowed.
   */
  @Test
  public final void testEmptyNotEmptyPropertyIsWrong() {
    validationTest(HibernateNotEmptyBug25TestCases.getEmptyTestBean(), false, null);
  }

  /**
   * correct not empty is allowed.
   */
  @Test
  public final void testCorrectNotEmptyAreAllowed() {
    for (final HibernateNotEmptyBug25TestBean testBean : HibernateNotEmptyBug25TestCases
        .getCorrectTestBeans()) {
      validationTest(testBean, true, null);
    }
  }

  /**
   * wrong not empty is not allowed.
   */
  @Test
  public final void testWrongNotEmptyPropertyAreWrong() {
    for (final HibernateNotEmptyBug25TestBean testBean : HibernateNotEmptyBug25TestCases
        .getWrongtoSmallTestBeans()) {
      validationTest(testBean, false, null);
    }
  }
}

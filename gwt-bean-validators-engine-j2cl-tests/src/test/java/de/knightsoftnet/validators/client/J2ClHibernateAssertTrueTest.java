/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.client;

import de.knightsoftnet.validators.shared.beans.HibernateAssertTrueTestBean;
import de.knightsoftnet.validators.shared.testcases.HibernateAssertTrueTestCases;

import com.google.j2cl.junit.apt.J2clTestInput;

import org.junit.Test;

/**
 * test for hibernate assert true validator.
 *
 * @author Manfred Tremmel
 *
 */
@J2clTestInput(J2ClHibernateAssertTrueTest.class)
public class J2ClHibernateAssertTrueTest
    extends AbstractValidationTst<HibernateAssertTrueTestBean> {

  /**
   * empty value is allowed.
   */
  @Test
  public final void testEmptyEntryIsAllowed() {
    validationTest(HibernateAssertTrueTestCases.getEmptyTestBean(), true, null);
  }

  /**
   * true or null is allowed.
   */
  @Test
  public final void testCorrectEntriesAreAllowed() {
    for (final HibernateAssertTrueTestBean testBean : HibernateAssertTrueTestCases
        .getCorrectTestBeans()) {
      validationTest(testBean, true, null);
    }
  }

  /**
   * false is not allowed.
   */
  @Test
  public final void testWrongEntriesAreWrong() {
    for (final HibernateAssertTrueTestBean testBean : HibernateAssertTrueTestCases
        .getWrongTestBeans()) {
      validationTest(testBean, false,
          "org.hibernate.validator.internal.constraintvalidators.bv.AssertTrueValidator");
    }
  }
}

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.client;

import de.knightsoftnet.validators.shared.beans.HibernateSizeArrayTestBean;
import de.knightsoftnet.validators.shared.testcases.HibernateSizeArrayTestCases;

import com.google.j2cl.junit.apt.J2clTestInput;

import org.junit.Test;

/**
 * test for hibernate size array validator.
 *
 * @author Manfred Tremmel
 *
 */
@J2clTestInput(J2ClHibernateSizeArrayTest.class)
public class J2ClHibernateSizeArrayTest extends AbstractValidationTst<HibernateSizeArrayTestBean> {

  /**
   * correct size are allowed.
   */
  @Test
  public final void testCorrectSizeArrayAreAllowed() {
    for (final HibernateSizeArrayTestBean testBean : HibernateSizeArrayTestCases
        .getCorrectTestBeans()) {
      validationTest(testBean, true, null);
    }
  }

  /**
   * wrong size are not allowed.
   */
  @Test
  public final void testWrongSizeArrayAreWrong() {
    for (final HibernateSizeArrayTestBean testBean : HibernateSizeArrayTestCases
        .getWrongtoSmallTestBeans()) {
      validationTest(testBean, false,
          "org.hibernate.validator.internal.constraintvalidators.bv.size."
              + "SizeValidatorForArray");
    }
  }
}

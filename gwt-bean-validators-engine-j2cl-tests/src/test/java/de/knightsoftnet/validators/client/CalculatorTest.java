package de.knightsoftnet.validators.client;

import static junit.framework.TestCase.assertEquals;

import com.google.j2cl.junit.apt.J2clTestInput;

import org.junit.Test;

/**
 * calculator test.
 *
 * @author Manfred Tremmel
 */
@J2clTestInput(CalculatorTest.class)
public class CalculatorTest {

  @Test
  public final void addTwoToTwoShouldReturnFour() {
    // Given
    final Calculator calculator = new Calculator();

    // When
    final int result = calculator.add(2, 2);

    // Then
    assertEquals("2 + 2 = 4", 4, result);
  }
}

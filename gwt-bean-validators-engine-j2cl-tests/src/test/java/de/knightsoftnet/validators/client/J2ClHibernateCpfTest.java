/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.client;

import de.knightsoftnet.validators.shared.beans.HibernateCpfTestBean;
import de.knightsoftnet.validators.shared.testcases.HibernateCpfTestCases;

import com.google.j2cl.junit.apt.J2clTestInput;

import org.junit.Test;

/**
 * test for cpf validator.
 *
 * @author Manfred Tremmel
 *
 */
@J2clTestInput(J2ClHibernateCpfTest.class)
public class J2ClHibernateCpfTest extends AbstractValidationTst<HibernateCpfTestBean> {

  /**
   * empty cpf is allowed.
   */
  @Test
  public final void testEmptyHibernateCpfIsAllowed() {
    validationTest(HibernateCpfTestCases.getEmptyTestBean(), true, null);
  }

  /**
   * correct cpfs are allowed.
   */
  @Test
  public final void testCorrectHibernateCpfsAreAllowed() {
    for (final HibernateCpfTestBean testBean : HibernateCpfTestCases.getCorrectTestBeans()) {
      validationTest(testBean, true, null);
    }
  }

  /**
   * wrong cpfs are not allowed.
   */
  @Test
  public final void testWrongHibernateCpfsAreWrong() {
    for (final HibernateCpfTestBean testBean : HibernateCpfTestCases.getWrongTestBeans()) {
      validationTest(testBean, false,
          "org.hibernate.validator.internal.constraintvalidators.hv.br.CPFValidator");
    }
  }
}

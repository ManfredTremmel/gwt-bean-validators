/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.client;

import de.knightsoftnet.validators.shared.beans.HibernateMinMaxPrimitiveTestBean;
import de.knightsoftnet.validators.shared.testcases.HibernateMinMaxPrimitiveTestCases;

import com.google.j2cl.junit.apt.J2clTestInput;

import org.junit.Test;

/**
 * test for hibernate min max primitive validator.
 *
 * @author Manfred Tremmel
 *
 */
@J2clTestInput(J2ClHibernateMinMaxPrimitiveTest.class)
public class J2ClHibernateMinMaxPrimitiveTest
    extends AbstractValidationTst<HibernateMinMaxPrimitiveTestBean> {

  /**
   * correct min max are allowed.
   */
  @Test
  public final void testCorrectMinMaxsAreAllowed() {
    for (final HibernateMinMaxPrimitiveTestBean testBean : HibernateMinMaxPrimitiveTestCases
        .getCorrectTestBeans()) {
      validationTest(testBean, true, null);
    }
  }

  /**
   * to small min max are not allowed.
   */
  @Test
  public final void testToSmallMinMaxsAreWrong() {
    for (final HibernateMinMaxPrimitiveTestBean testBean : HibernateMinMaxPrimitiveTestCases
        .getWrongtoSmallTestBeans()) {
      validationTest(testBean, false, null);
    }
  }

  /**
   * to big min max are not allowed.
   */
  @Test
  public final void testToBigMinMaxsAreWrong() {
    for (final HibernateMinMaxPrimitiveTestBean testBean : HibernateMinMaxPrimitiveTestCases
        .getWrongtoBigTestBeans()) {
      validationTest(testBean, false, null);
    }
  }
}

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.client;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import de.knightsoftnet.validators.client.factories.ValidatorFactory;
import de.knightsoftnet.validators.client.impl.Validation;
import de.knightsoftnet.validators.shared.beans.Bug17TestBean;
import de.knightsoftnet.validators.shared.beans.Bug17bTestBean;
import de.knightsoftnet.validators.shared.beans.Bug17cTestBean;

import com.google.j2cl.junit.apt.J2clTestInput;

import org.junit.Test;

import java.util.Set;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validator;

/**
 * GWT JUnit <b>integration</b> tests must extend GWTTestCase. Test for bugs reported in #17.
 *
 * @author Manfred Tremmel
 */
@J2clTestInput(J2ClHibernateAssertFalseTest.class)
public class J2ClBug17Test {

  private final Validator validator = Validation.byDefaultProvider() //
      .configure() //
      .gwtFactory(new ValidatorFactory()) //
      .buildValidatorFactory() //
      .getValidator();

  @Test
  public final void testShouldAlwaysFail() {
    final Bug17TestBean bean = new Bug17TestBean();

    final Set<ConstraintViolation<Bug17TestBean>> cv1 = validator.validate(bean);

    assertFalse("Should always return one error", cv1.isEmpty());
  }

  @Test
  public final void testShouldReturnThreeErrors() {
    final Bug17bTestBean bean = new Bug17bTestBean();

    final Set<ConstraintViolation<Bug17bTestBean>> cv1 = validator.validate(bean);

    assertEquals("Should return three errors", 3, cv1.size());
  }

  @Test
  public final void testShouldReturnThreeErrorsOnExtendedClass() {
    final Bug17cTestBean bean = new Bug17cTestBean();

    final Set<ConstraintViolation<Bug17cTestBean>> cv1 = validator.validate(bean);

    assertEquals("Should return three errors", 3, cv1.size());
  }
}

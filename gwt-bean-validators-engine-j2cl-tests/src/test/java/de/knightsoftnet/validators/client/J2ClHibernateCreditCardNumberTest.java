/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.client;

import de.knightsoftnet.validators.shared.beans.HibernateCreditCardNumberTestBean;
import de.knightsoftnet.validators.shared.testcases.HibernateCreditCardNumberTestCases;

import com.google.j2cl.junit.apt.J2clTestInput;

import org.junit.Test;

/**
 * test for hibernate credit card number validator.
 *
 * @author Manfred Tremmel
 *
 */
@J2clTestInput(J2ClHibernateCreditCardNumberTest.class)
public class J2ClHibernateCreditCardNumberTest
    extends AbstractValidationTst<HibernateCreditCardNumberTestBean> {

  /**
   * empty credit card is allowed.
   */
  @Test
  public final void testEmptyHibernateCreditCardIsAllowed() {
    validationTest(HibernateCreditCardNumberTestCases.getEmptyTestBean(), true, null);
  }

  /**
   * correct credit card numbers are allowed.
   */
  @Test
  public final void testCorrectHibernateCreditCardsAreAllowed() {
    for (final HibernateCreditCardNumberTestBean testBean : HibernateCreditCardNumberTestCases
        .getCorrectTestBeans()) {
      validationTest(testBean, true, null);
    }
  }

  /**
   * wrong credit card numbers are not allowed.
   */
  public final void testWrongHibernateCreditCardsAreWrong() {
    for (final HibernateCreditCardNumberTestBean testBean : HibernateCreditCardNumberTestCases
        .getWrongTestBeans()) {
      validationTest(testBean, false, null);
    }
  }

  /**
   * wrong credit card numbers are not allowed.
   */
  @Test
  public final void testWrongHibernateCreditCardsSizeAreWrong() {
    for (final HibernateCreditCardNumberTestBean testBean : HibernateCreditCardNumberTestCases
        .getWrongSizeTestBeans()) {
      validationTest(testBean, false, null);
    }
  }
}

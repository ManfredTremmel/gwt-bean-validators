/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.client;

import de.knightsoftnet.validators.shared.beans.HibernateIsbn13TestBean;
import de.knightsoftnet.validators.shared.testcases.HibernateIsbn13TestCases;

import com.google.j2cl.junit.apt.J2clTestInput;

import org.junit.Test;

/**
 * test for isbn 13 validator.
 *
 * @author Manfred Tremmel
 *
 */
@J2clTestInput(J2ClHibernateIsbn13Test.class)
public class J2ClHibernateIsbn13Test extends AbstractValidationTst<HibernateIsbn13TestBean> {

  /**
   * empty isbn13 is allowed.
   */
  @Test
  public final void testEmptyIsbn13IsAllowed() {
    validationTest(HibernateIsbn13TestCases.getEmptyTestBean(), true, null);
  }

  /**
   * correct isbn13 is allowed.
   */
  @Test
  public final void testCorrectIsbn13IsAllowed() {
    for (final HibernateIsbn13TestBean testBean : HibernateIsbn13TestCases.getCorrectTestBeans()) {
      validationTest(testBean, true, null);
    }
  }

  /**
   * isbn13 with wrong checksum.
   */
  @Test
  public final void testWrongChecksumIsbn13IsWrong() {
    for (final HibernateIsbn13TestBean testBean : HibernateIsbn13TestCases.getWrongTestBeans()) {
      validationTest(testBean, false,
          "org.hibernate.validator.internal.constraintvalidators.hv.ISBNValidator");
    }
  }

  /**
   * isbn13 which is to small.
   */
  @Test
  public final void testToSmallIsbn13IsWrong() {
    for (final HibernateIsbn13TestBean testBean : HibernateIsbn13TestCases.getToSmallTestBeans()) {
      validationTest(testBean, false,
          "org.hibernate.validator.internal.constraintvalidators.hv.ISBNValidator");
    }
  }

  /**
   * isbn13 which is to big.
   */
  @Test
  public final void testToBigIsbn13IsWrong() {
    for (final HibernateIsbn13TestBean testBean : HibernateIsbn13TestCases.getToBigTestBeans()) {
      validationTest(testBean, false,
          "org.hibernate.validator.internal.constraintvalidators.hv.ISBNValidator");
    }
  }

  /**
   * isbn13 which is not numeric.
   */
  @Test
  public final void testNotNumericIsbn13IsWrong() {
    for (final HibernateIsbn13TestBean testBean : HibernateIsbn13TestCases
        .getNotNumericTestBeans()) {
      validationTest(testBean, false,
          "org.hibernate.validator.internal.constraintvalidators.hv.ISBNValidator");
    }
  }
}

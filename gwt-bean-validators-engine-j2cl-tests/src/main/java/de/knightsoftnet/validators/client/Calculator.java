package de.knightsoftnet.validators.client;

/**
 * calculator class for testing only.
 *
 * @author Manfred Tremmel
 */
public class Calculator {

  public int add(final int param1, final int param2) {
    return param1 + param2;
  }
}

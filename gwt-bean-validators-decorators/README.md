gwt-bean-validators-decorators
==============================

If you use widgets in your application which do not support handling validation results (like the ones gwt includes), decorators can do the job.


Maven integration
----------------

Add the dependencies itself for GWT-Projects:

```xml
    <dependency>
      <groupId>de.knightsoft-net</groupId>
      <artifactId>gwt-bean-validators-decorators</artifactId>
      <version>2.4.1</version>
    </dependency>
```

GWT Integration
---------------

What you still have to do, inherit Decorators into your project .gwt.xml file:

```
<inherits name="de.knightsoftnet.validators.GwtBeanValidatorsDecorators" />
```

Two Decorators, the decent [UniversalDecorator.java](blob/master/gwt-bean-validators-decorators/src/main/java/de/knightsoftnet/validators/client/decorators/UniversalDecorator.java) and the more decorativ [UniversalDecoratorWithIcon.java](blob/master/gwt-bean-validators-decorators/src/main/java/de/knightsoftnet/validators/client/decorators/UniversalDecoratorWithIcons.java) can be used to display the validation results on every input field. For own Designs, take [UniversalDecoratorWithIcon.java](blob/master/gwt-bean-validators-decorators/src/main/java/de/knightsoftnet/validators/client/decorators/UniversalDecoratorWithIcons.java) as base and replace the Stylesheet. In the UiBinder you simply surround your widget with the Decorator tags like this:

```xml
<!DOCTYPE ui:UiBinder SYSTEM "http://dl.google.com/gwt/DTD/xhtml.ent">
<ui:UiBinder xmlns:ui="urn:ui:com.google.gwt.uibinder"
  xmlns:g="urn:import:com.google.gwt.user.client.ui"
  xmlns:e="urn:import:de.knightsoftnet.validators.client.decorators">
...
        <e:UniversalDecoratorWithIcons errorLocation="RIGHT" ui:field="checkbox" >
          <e:widget>
            <g:CheckBox />
          </e:widget>
        </e:UniversalDecoratorWithIcons>
...
</ui:UiBinder>
```

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.client.decorators;

import de.knightsoftnet.validators.client.editor.ValueBoxBase;
import de.knightsoftnet.validators.client.editor.ValueBoxEditor;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.Style.Display;
import com.google.gwt.event.dom.client.HasKeyPressHandlers;
import com.google.gwt.event.dom.client.HasKeyUpHandlers;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.i18n.client.LocaleInfo;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.uibinder.client.UiChild;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Focusable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasEnabled;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

import org.apache.commons.lang3.StringUtils;
import org.gwtproject.editor.client.EditorError;
import org.gwtproject.editor.client.TakesValue;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import jsinterop.base.Js;

/**
 * This abstract class combines some methods which are used by decorators.
 *
 * @param <T> the type of data being edited
 */
public abstract class AbstractDecorator<T> extends Composite implements IsDecorator<T> {

  /**
   * A ClientBundle that provides images and decoratorStyle sheets for the decorator.
   */
  public interface Resources extends ClientBundle {

    /**
     * The styles used in this widget.
     *
     * @return decorator style
     */
    @Source("EditorDecorator.gss")
    DecoratorStyle decoratorStyle();
  }

  /**
   * the default resources.
   */
  private static volatile Resources defaultResource;

  /**
   * content panel.
   */
  protected final SimplePanel contents = new SimplePanel();

  /**
   * value box resource with css.
   */
  protected final DecoratorStyle decoratorStyle;

  /**
   * the widget.
   */
  protected Widget widget;

  /**
   * label to display error message.
   */
  @Ignore
  private final HTML errorLabel = new HTML();

  /**
   * editor to handle input and errors.
   */
  private ExtendedValueBoxEditor<T> editor;

  /**
   * validation state of the entry.
   */
  private boolean focusOnError;

  /**
   * panel which surrounds the widget.
   */
  private final FlowPanel layout;

  /**
   * Constructs a Decorator.
   *
   * @param errorLocation location of the error text
   */
  protected AbstractDecorator(final PanelLocationEnum errorLocation) {
    this(errorLocation, getDefaultResources());
  }

  /**
   * Constructs a Decorator using a {@link Widget} widget with error location and style sheet.
   *
   * @param widget the widget
   * @param errorLocation location of the error text
   * @param decoratorResource resource with css information
   */
  protected AbstractDecorator(final Widget widget, final PanelLocationEnum errorLocation,
      final Resources decoratorResource) {
    this(errorLocation, decoratorResource);
    this.widget = widget;
    contents.add(widget);
  }

  /**
   * Constructs a Decorator using a {@link Widget} widget with error location.
   *
   * @param widget the widget
   * @param errorLocation location of the error text
   */
  protected AbstractDecorator(final Widget widget, final PanelLocationEnum errorLocation) {
    this(widget, errorLocation, getDefaultResources());
  }

  /**
   * Constructs a Decorator using a {@link Widget} widget with error location.
   *
   * @param widget the widget
   */
  protected AbstractDecorator(final Widget widget) {
    this(widget, PanelLocationEnum.RIGHT, getDefaultResources()); // NOPMD
  }

  /**
   * Constructs a Decorator.
   *
   * @param errorLocation location of the error text
   * @param resource resource with css information
   */
  protected AbstractDecorator(final PanelLocationEnum errorLocation, final Resources resource) {
    super();
    // Inject the stylesheet.
    decoratorStyle = resource.decoratorStyle();
    decoratorStyle.ensureInjected();

    layout = createWidgetPanel(errorLocation);
    initWidget(layout);
  }

  private FlowPanel createWidgetPanel(final PanelLocationEnum errorLocation) {
    final boolean contentFirst =
        errorLocation == PanelLocationEnum.LEFT && LocaleInfo.getCurrentLocale().isRTL() // NOPMD
            || errorLocation == PanelLocationEnum.RIGHT // NOPMD
                && !LocaleInfo.getCurrentLocale().isRTL() // NOPMD
            || errorLocation == PanelLocationEnum.BOTTOM; // NOPMD
    final FlowPanel layout = new FlowPanel();
    if (errorLocation == PanelLocationEnum.TOP) { // NOPMD
      layout.add(errorLabel);
      layout.add(contents);
    } else {
      layout.add(contents);
      layout.add(errorLabel);
    }
    switch (errorLocation) {
      case TOP:
        errorLabel.setStylePrimaryName(decoratorStyle.errorLabelStyleTop());
        contents.setStylePrimaryName(decoratorStyle.contentContainerStyleTop());
        break;
      case BOTTOM:
        errorLabel.setStylePrimaryName(decoratorStyle.errorLabelStyleBottom());
        contents.setStylePrimaryName(decoratorStyle.contentContainerStyleBottom());
        break;
      default:
        if (contentFirst) {
          errorLabel.setStylePrimaryName(decoratorStyle.errorLabelStyleRight());
          contents.setStylePrimaryName(decoratorStyle.contentContainerStyleRight());
        } else {
          errorLabel.setStylePrimaryName(decoratorStyle.errorLabelStyleLeft());
          contents.setStylePrimaryName(decoratorStyle.contentContainerStyleLeft());
        }
        break;
    }
    errorLabel.getElement().getStyle().setDisplay(Display.NONE);
    focusOnError = true;
    return layout;
  }

  @Override
  public FlowPanel getLayout() {
    return layout;
  }

  /**
   * get default resource, if not set, create one.
   *
   * @return default resource.
   */
  protected static Resources getDefaultResources() {
    if (defaultResource == null) { // NOPMD it's thread save!
      synchronized (Resources.class) {
        if (defaultResource == null) {
          defaultResource = GWT.create(Resources.class);
        }
      }
    }
    return defaultResource;
  }

  @Override
  @SuppressWarnings("unchecked")
  @UiChild(limit = 1, tagname = "widget")
  public void setChildWidget(final Widget widget) {
    this.widget = widget;
    contents.add(this.widget);
    if (widget instanceof final ValueBoxBase<?> widgetValueBoxBase) {
      setEditor(new ExtendedValueBoxEditor<>((ValueBoxBase<T>) widgetValueBoxBase, this));
    } else if (widget instanceof final com.google.gwt.user.client.ui.ValueBoxBase<?> widgetValueB) {
      setEditor(new ExtendedValueBoxEditor<>(ValueBoxBase.oldToNew(
          Js.<Element>uncheckedCast(widgetValueB.getElement()), (HasValue<T>) widgetValueB), this));
    } else if (widget instanceof final TakesValue<?> widgetTagesValue) {
      setEditor(new ExtendedValueBoxEditor<>((TakesValue<T>) widgetTagesValue, this));
    } else if (widget instanceof final HasValue<?> widgetTakesValue) {
      setEditor(new ExtendedValueBoxEditor<>(
          ValueBoxBase.oldToNew(widget.getElement(), (HasValue<T>) widgetTakesValue), this));
    }
  }

  @SuppressWarnings("unchecked")
  @Override
  public final HandlerRegistration addValueChangeHandler(final ValueChangeHandler<T> handler) {
    if (contents.getWidget() instanceof final HasValueChangeHandlers<?> widget) {
      return ((HasValueChangeHandlers<T>) widget).addValueChangeHandler(handler);
    }
    return null;
  }

  /**
   * add a change handler to clear error state on value change.
   *
   * @param widget widget to set the handler to
   */
  protected void addValueChangeHandler(final HasValueChangeHandlers<T> widget) {
    widget.addValueChangeHandler(event -> clearErrors());
  }

  /**
   * clear errors.
   */
  @Override
  public void clearErrors() {
    errorLabel.setText(StringUtils.EMPTY);
    errorLabel.getElement().getStyle().setDisplay(Display.NONE);
    if (contents.getWidget() != null) {
      contents.getWidget().removeStyleName(decoratorStyle.errorInputStyle());
      contents.getWidget().removeStyleName(decoratorStyle.validInputStyle());
    }
  }

  /**
   * The default implementation will display, but not consume, received errors whose
   * {@link EditorError#getEditor() getEditor()} method returns the Editor passed into
   * {@link #setEditor}.
   *
   * @param errors a List of {@link EditorError} instances
   */
  @Override
  public void showErrors(final List<EditorError> errors) {
    final Set<String> messages = errors.stream().filter(error -> editorErrorMatches(error))
        .map(error -> error.getMessage()).distinct().collect(Collectors.toSet());

    if (messages.isEmpty()) {
      errorLabel.setText(StringUtils.EMPTY);
      errorLabel.getElement().getStyle().setDisplay(Display.NONE);
      if (contents.getWidget() != null) {
        contents.getWidget().removeStyleName(decoratorStyle.errorInputStyle());
        contents.getWidget().addStyleName(decoratorStyle.validInputStyle());
      }
    } else {
      if (contents.getWidget() != null) {
        contents.getWidget().removeStyleName(decoratorStyle.validInputStyle());
        contents.getWidget().addStyleName(decoratorStyle.errorInputStyle());
        if (focusOnError) {
          setFocus(true);
        }
      }
      final SafeHtmlBuilder sb = new SafeHtmlBuilder();
      messages.forEach(message -> {
        sb.appendEscaped(message);
        sb.appendHtmlConstant("<br />");
      });
      errorLabel.setHTML(sb.toSafeHtml());
      errorLabel.getElement().getStyle().setDisplay(Display.TABLE);
    }
  }

  /**
   * Checks if a error belongs to this widget.
   *
   * @param error editor error to check
   * @return true if the error belongs to this widget
   */
  protected boolean editorErrorMatches(final EditorError error) {
    return error != null && error.getEditor() != null
        && (equals(error.getEditor()) || error.getEditor().equals(editor));
  }

  @SuppressWarnings("unchecked")
  @Override
  public final T getValue() {
    if (contents.getWidget() instanceof final TakesValue<?> widget) {
      return ((TakesValue<T>) widget).getValue();
    } else if (contents
        .getWidget() instanceof final com.google.gwt.user.client.TakesValue<?> widget) {
      return ((com.google.gwt.user.client.TakesValue<T>) widget).getValue();
    }
    return null;
  }

  @Override
  public final void setValue(final T value) {
    setValue(value, false);
  }

  @SuppressWarnings("unchecked")
  @Override
  public void setValue(final T value, final boolean fireEvents) {
    if (contents.getWidget() instanceof final TakesValue<?> widget) {
      clearErrors();
      if (widget instanceof HasValue<?>) {
        ((HasValue<T>) widget).setValue(value, fireEvents);
      } else {
        ((TakesValue<T>) widget).setValue(value);
      }
    } else if (contents
        .getWidget() instanceof final com.google.gwt.user.client.TakesValue<?> widget) {
      clearErrors();
      if (widget instanceof HasValue<?>) {
        ((HasValue<T>) widget).setValue(value, fireEvents);
      } else {
        ((com.google.gwt.user.client.TakesValue<T>) widget).setValue(value);
      }
    }
  }

  @Override
  public final void setTabIndex(final int index) {
    if (contents.getWidget() instanceof final Focusable widget) {
      widget.setTabIndex(index);
    }
  }

  @Override
  public final int getTabIndex() {
    if (contents.getWidget() instanceof final Focusable widget) {
      return widget.getTabIndex();
    }
    return 0;
  }

  @Override
  public final void setAccessKey(final char key) {
    if (contents.getWidget() instanceof final Focusable widget) {
      widget.setAccessKey(key);
    }
  }

  @Override
  public final void setFocus(final boolean focused) {
    if (contents.getWidget() instanceof final Focusable widget) {
      widget.setFocus(focused);
    }
  }

  @Override
  public boolean isEnabled() {
    if (contents.getWidget() instanceof final HasEnabled widget) {
      widget.isEnabled();
    }
    return false;
  }

  @Override
  public void setEnabled(final boolean enabled) {
    if (contents.getWidget() instanceof final HasEnabled widget) {
      widget.setEnabled(enabled);
    }
  }

  /**
   * get the widget.
   *
   * @return the widget
   */
  @Override
  public final Widget getWidget() {
    return widget;
  }

  /**
   * Returns the associated {@link ValueBoxEditor}.
   *
   * @return a {@link ValueBoxEditor} instance
   * @see #setEditor(ExtendedValueBoxEditor)
   */
  @Override
  public ValueBoxEditor<T> asEditor() {
    return editor;
  }

  /**
   * Sets the associated {@link ValueBoxEditor}.
   *
   * @param editor a {@link ValueBoxEditor} instance
   * @see #asEditor()
   */
  public final void setEditor(final ExtendedValueBoxEditor<T> editor) {
    this.editor = editor;
  }

  /**
   * check if focus on error is active.
   *
   * @return true if widget should get focus on error
   */
  @Override
  public final boolean isFocusOnError() {
    return focusOnError;
  }

  /**
   * set focus on error flag, if it's true, the widget get's the focus if validation finds an error.
   *
   * @param focusOnError the focusOnError to set
   */
  @Override
  public final void setFocusOnError(final boolean focusOnError) {
    this.focusOnError = focusOnError;
  }

  @Override
  public final HandlerRegistration addKeyPressHandler(final KeyPressHandler handler) {
    if (contents.getWidget() instanceof final HasKeyPressHandlers widget) {
      return widget.addKeyPressHandler(handler);
    }
    return null;
  }

  @Override
  public HandlerRegistration addKeyUpHandler(final KeyUpHandler keyUpHandler) {
    if (contents.getWidget() instanceof final HasKeyUpHandlers widget) {
      return widget.addKeyUpHandler(keyUpHandler);
    }
    return null;
  }
}

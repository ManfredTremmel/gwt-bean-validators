package de.knightsoftnet.validators.client.decorators;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.HasBlurHandlers;
import com.google.gwt.event.dom.client.HasFocusHandlers;
import com.google.gwt.uibinder.client.UiChild;
import com.google.gwt.user.client.ui.Widget;

import org.apache.commons.lang3.StringUtils;
import org.gwtproject.editor.client.IsEditor;
import org.gwtproject.editor.client.TakesValue;

import java.util.Objects;

/**
 * Abstract implementation of the decorator with label.
 *
 * @author Manfred Tremmel
 *
 * @param <T> type to handle
 */
public abstract class AbstractDecoratorWithLabel<T> extends AbstractDecorator<T> {

  /**
   * A ClientBundle that provides images and decoratorStyle sheets for the decorator.
   */
  public interface ResourcesLabel extends Resources {

    /**
     * The styles used in this widget.
     *
     * @return decorator style
     */
    @Override
    @Source("EditorWithLabelDecorator.gss")
    DecoratorStyleWithLabel decoratorStyle();
  }

  /**
   * the default resources.
   */
  private static volatile ResourcesLabel defaultResource;


  /**
   * label of the widget.
   */
  @Ignore
  private Widget label;

  /**
   * Constructor.
   *
   * @param errorLocation location to place error message
   */
  protected AbstractDecoratorWithLabel(final PanelLocationEnum errorLocation) {
    this(errorLocation, getDefaultResources());
  }

  /**
   * Constructor.
   *
   * @param errorLocation location to place error message
   * @param resource resource for the label
   */
  protected AbstractDecoratorWithLabel(final PanelLocationEnum errorLocation,
      final ResourcesLabel resource) {
    super(errorLocation, resource);
  }

  /**
   * get default resource, if not set, create one.
   *
   * @return default resource.
   */
  protected static ResourcesLabel getDefaultResources() {
    if (defaultResource == null) { // NOPMD it's thread save!
      synchronized (Resources.class) {
        if (defaultResource == null) {
          defaultResource = GWT.create(ResourcesLabel.class);
        }
      }
    }
    return defaultResource;
  }

  /**
   * Set the label of widget.
   *
   * @param label a label widget
   */
  @UiChild(limit = 1, tagname = "label")
  public void setChildLabel(final Widget label) {
    this.label = label;
    getLayout().add(this.label);
  }

  /**
   * Set the widget that the EditorPanel will display. This method will automatically call
   * {@link #setEditor}.
   *
   * @param widget a {@link IsEditor} widget
   */
  @Override
  @UiChild(limit = 1, tagname = "widget")
  public void setChildWidget(final Widget widget) {
    super.setChildWidget(widget);
    if (widget instanceof final HasFocusHandlers hasFocusHandlers) {
      hasFocusHandlers.addFocusHandler(event -> addStyleToLabel());
    }
    if (widget instanceof final HasBlurHandlers hasBlurHandlers) {
      hasBlurHandlers.addBlurHandler(event -> {
        boolean hide = true;
        if (this.widget instanceof final TakesValue<?> takesValue) {
          hide = StringUtils.isEmpty(Objects.toString(takesValue.getValue(), null));
        } else if (this.widget instanceof final com.google.gwt.user.client.TakesValue<?> takesVal) {
          hide = StringUtils.isEmpty(Objects.toString(takesVal.getValue(), null));
        }
        if (hide) {
          removeStyleFromLabel();
        }
      });
    }
  }

  @Override
  public void setValue(final T value, final boolean fireEvents) {
    super.setValue(value, fireEvents);
    if (StringUtils.isEmpty(Objects.toString(value, null))) {
      removeStyleFromLabel();
    } else {
      addStyleToLabel();
    }
  }

  @Override
  public void clearErrors() {
    super.clearErrors();
    if (contents.getWidget() instanceof final TakesValue<?> takesValue) {
      if (StringUtils.isEmpty(Objects.toString(takesValue.getValue(), null))) {
        removeStyleFromLabel();
      } else {
        addStyleToLabel();
      }
    } else if (contents
        .getWidget() instanceof final com.google.gwt.user.client.TakesValue<?> takesValue) {
      if (StringUtils.isEmpty(Objects.toString(takesValue.getValue(), null))) {
        removeStyleFromLabel();
      } else {
        addStyleToLabel();
      }
    }
  }

  private void addStyleToLabel() {
    if (!label.getElement()
        .hasClassName(((DecoratorStyleWithLabel) decoratorStyle).labelStyleFocused())) {
      label.getElement()
          .addClassName(((DecoratorStyleWithLabel) decoratorStyle).labelStyleFocused());
    }
  }

  private void removeStyleFromLabel() {
    if (label.getElement()
        .hasClassName(((DecoratorStyleWithLabel) decoratorStyle).labelStyleFocused())) {
      label.getElement()
          .removeClassName(((DecoratorStyleWithLabel) decoratorStyle).labelStyleFocused());
    }
  }
}

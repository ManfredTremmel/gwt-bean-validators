gwt-bean-validators-engine-gwt-user
===================================

GWT-User workarounds for GWT Bean Validators Engine (to work around integrated validation stuff).

Maven integration
----------------

Add the dependencies itself for GWT-Projects (don't integrate this at it's one, it includes only a workaround for GWT Bean Validators Engine, that can be removed, when gwt-user removes integrated validation engine):

```xml
    <dependency>
      <groupId>de.knightsoft-net</groupId>
      <artifactId>gwt-bean-validators-engine-gwt-user</artifactId>
      <version>2.4.1</version>
    </dependency>
```

GWT Integration
---------------

Add this inherits entry to your projects gwt.xml configuration file:

```xml
<inherits name="de.knightsoftnet.validators.gwtuser.GwtBeanValidatorsEngineGwtUser" />
```

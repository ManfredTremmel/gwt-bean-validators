package javax.validation.metadata;

import java.lang.annotation.Annotation;

public interface ConstraintDescriptor<T extends Annotation>
    extends jakarta.validation.metadata.ConstraintDescriptor<T> {

}

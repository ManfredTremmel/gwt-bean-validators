/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.google.gwt.validation.client.impl;

import jakarta.validation.ElementKind;

/**
 * Dummy to fix compile problems as long validation-api 1.0 implementation is part of gwt.
 *
 * @author Manfred Tremmel
 *
 */
public class NodeImpl extends org.hibernate.validator.internal.engine.path.NodeImpl {

  private static final long serialVersionUID = -7481234506359509905L;

  public NodeImpl(final String name,
      final org.hibernate.validator.internal.engine.path.NodeImpl parent, final boolean isIterable,
      final Integer index, final Object key, final ElementKind kind,
      final Class<?>[] parameterTypes, final Integer parameterIndex, final Object value) {
    super(name, parent, isIterable, index, key, kind, parameterTypes, parameterIndex, value,
        (Class<?>) null, (Integer) null);
  }
}

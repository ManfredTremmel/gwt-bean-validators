# gwt-jackson-datatype-jsr310
Integration of JSR-310 datatypes in gwt-jackson.

Maven integraten
----------------

The dependency itself for GWT-Projects:

```xml
    <dependency>
      <groupId>de.knightsoft-net</groupId>
      <artifactId>gwt-jackson-datatype-jsr310</artifactId>
      <version>2.4.1</version>
    </dependency>
```

GWT Integration
---------------

What you still have to do, inherit GwtMtWidgets into your project .gwt.xml file:

```xml
<inherits name="de.knightsoftnet.datatype.GwtJacksonJsr310" />
```

Included Types
--------------

- LocaleDate
- LocaleDateTime
- LocaleTime

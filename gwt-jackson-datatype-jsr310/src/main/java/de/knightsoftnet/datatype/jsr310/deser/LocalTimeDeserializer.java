/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.datatype.jsr310.deser;

import com.github.nmorel.gwtjackson.client.JsonDeserializationContext;
import com.github.nmorel.gwtjackson.client.JsonDeserializer;
import com.github.nmorel.gwtjackson.client.JsonDeserializerParameters;
import com.github.nmorel.gwtjackson.client.stream.JsonReader;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class LocalTimeDeserializer extends JsonDeserializer<LocalTime> {

  private static final LocalTimeDeserializer INSTANCE = new LocalTimeDeserializer();

  /**
   * static get instance method.
   *
   * @return an instance of {@link LocalTimeDeserializer}
   */
  public static LocalTimeDeserializer getInstance() {
    return INSTANCE;
  }

  private LocalTimeDeserializer() {
    super();
  }

  @Override
  protected LocalTime doDeserialize(final JsonReader reader, final JsonDeserializationContext ctx,
      final JsonDeserializerParameters params) {
    final String string = reader.nextString();
    if (string == null || string.length() == 0) {
      return null;
    }
    return LocalTime.parse(string.trim(), DateTimeFormatter.ISO_LOCAL_TIME);
  }
}

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.datatype.jsr310;

import de.knightsoftnet.datatype.jsr310.deser.LocalDateDeserializer;
import de.knightsoftnet.datatype.jsr310.deser.LocalDateTimeDeserializer;
import de.knightsoftnet.datatype.jsr310.deser.LocalTimeDeserializer;
import de.knightsoftnet.datatype.jsr310.ser.LocalDateSerializer;
import de.knightsoftnet.datatype.jsr310.ser.LocalDateTimeSerializer;
import de.knightsoftnet.datatype.jsr310.ser.LocalTimeSerializer;

import com.github.nmorel.gwtjackson.client.AbstractConfiguration;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class JacksonGuavaConfiguration extends AbstractConfiguration {

  @Override
  protected void configure() {
    type(LocalDate.class).serializer(LocalDateSerializer.class)
        .deserializer(LocalDateDeserializer.class);
    type(LocalDateTime.class).serializer(LocalDateTimeSerializer.class)
        .deserializer(LocalDateTimeDeserializer.class);
    type(LocalTime.class).serializer(LocalTimeSerializer.class)
        .deserializer(LocalTimeDeserializer.class);
  }
}

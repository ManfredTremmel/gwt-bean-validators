/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.datatype.jsr310.ser;

import com.github.nmorel.gwtjackson.client.JsonSerializationContext;
import com.github.nmorel.gwtjackson.client.JsonSerializer;
import com.github.nmorel.gwtjackson.client.JsonSerializerParameters;
import com.github.nmorel.gwtjackson.client.stream.JsonWriter;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class LocalDateTimeSerializer extends JsonSerializer<LocalDateTime> {

  private static final LocalDateTimeSerializer INSTANCE = new LocalDateTimeSerializer();

  /**
   * static get instance method.
   *
   * @return an instance of {@link LocalDateTimeSerializer}
   */
  public static LocalDateTimeSerializer getInstance() {
    return INSTANCE;
  }

  private LocalDateTimeSerializer() {
    super();
  }

  @Override
  protected void doSerialize(final JsonWriter writer, final LocalDateTime value,
      final JsonSerializationContext ctx, final JsonSerializerParameters params) {
    if (value == null) {
      writer.nullValue();
    } else {
      writer.value(value.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
    }
  }
}

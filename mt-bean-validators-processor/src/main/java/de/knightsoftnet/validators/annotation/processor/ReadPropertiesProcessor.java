package de.knightsoftnet.validators.annotation.processor;

import de.knightsoftnet.validators.shared.annotation.ReadProperties;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.FilerException;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.tools.Diagnostic;
import javax.tools.JavaFileObject;

@SupportedAnnotationTypes("de.knightsoftnet.validators.shared.annotation.ReadProperties")
@SupportedSourceVersion(SourceVersion.RELEASE_17)
public class ReadPropertiesProcessor extends AbstractProcessor {

  @Override
  public boolean process(final Set<? extends TypeElement> annotations,
      final RoundEnvironment roundEnv) {
    if (annotations == null || annotations.isEmpty() || roundEnv == null || processingEnv == null) {
      return false;
    }
    if (!roundEnv.processingOver()) {
      for (final Element element : roundEnv.getElementsAnnotatedWith(ReadProperties.class)) {
        final String packageName =
            processingEnv.getElementUtils().getPackageOf(element).getQualifiedName().toString();
        final String interfaceName = element.getSimpleName().toString();
        final String factoryClass = element.getEnclosingElement().getSimpleName().toString();
        final ReadProperties readProperties = element.getAnnotation(ReadProperties.class);
        final Locale locale = StringUtils.isEmpty(readProperties.locale()) ? Locale.ROOT
            : new Locale(readProperties.locale());
        createClass(packageName, interfaceName, factoryClass,
            CreateClassHelper.readMapFromProperties(readProperties.propertyName(), locale));
      }
    }

    return true;
  }

  private void createClass(final String packageName, final String interfaceName,
      final String factoryClass, final Map<String, String> readMapFromProperties) {
    try {
      final JavaFileObject builderFile =
          processingEnv.getFiler().createSourceFile(packageName + "." + interfaceName + "Impl");
      try (PrintWriter out = new PrintWriter(builderFile.openWriter())) {
        out.println("package " + packageName + ";");
        out.println();

        out.println("import java.util.HashMap;");
        out.println("import java.util.Map;");

        out.println();

        out.println("public class " + interfaceName + "Impl implements " + factoryClass + "."
            + interfaceName + " {");
        out.println();
        out.println("  private final Map<String, String> propertiesMap;");
        out.println();

        int initMethodCount = 0;
        int posCount = 0;

        for (final Entry<String, String> entry : readMapFromProperties.entrySet()) {
          if (posCount > 0 && posCount % 500 == 0) {
            out.println("  }");
            out.println();
          }
          if (posCount == 0 || posCount % 500 == 0) {
            out.println("  private void fillMap" + initMethodCount + "() {");
            initMethodCount++;
          }
          out.print("    propertiesMap.put(\"");
          out.print(StringEscapeUtils.escapeJava(entry.getKey()));
          out.print("\", \"");
          out.print(StringEscapeUtils.escapeJava(entry.getValue()));
          out.println("\");");
          posCount++;
        }

        out.println("  }");
        out.println();

        out.println("  public " + interfaceName + "Impl() {");
        out.println("    propertiesMap = new HashMap<>();");
        for (int i = 0; i < initMethodCount; i++) {
          out.println("    fillMap" + i + "();");
        }
        out.println("  }");
        out.println();

        out.println("  @Override");
        out.println("  public Map<String, String> properties() {");
        out.println("    return propertiesMap;");
        out.println("  }");
        out.println("}");
      }
    } catch (final FilerException e) {
      // happens when trying to recreate an existing interface
      processingEnv.getMessager().printMessage(Diagnostic.Kind.NOTE, e.getMessage());
    } catch (final IOException e) {
      processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR, e.getMessage());
      e.printStackTrace();
    }
  }
}

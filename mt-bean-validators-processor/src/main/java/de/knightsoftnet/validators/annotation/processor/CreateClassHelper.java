/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.annotation.processor;

import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

/**
 * Read gwt constants from properties file on server side.
 *
 * @author Manfred Tremmel
 *
 */
public class CreateClassHelper {

  private static final String PROPERTY_PACKAGE = "de.knightsoftnet.validators.client.data.";

  /**
   * read map from properties.
   *
   * @param mapName name of the properties file
   * @return map with key and value
   */
  public static Map<String, String> readMapFromProperties(final String mapName) {
    return readMapFromProperties(mapName, Locale.ROOT);
  }

  /**
   * read map from properties for given locale.
   *
   * @param mapName name of the properties file
   * @param locale the local to get data for
   * @return map with key and value
   */
  public static Map<String, String> readMapFromProperties(final String mapName,
      final Locale locale) {
    final ResourceBundle bundle =
        ResourceBundle.getBundle(PROPERTY_PACKAGE + mapName, locale, new Utf8Control());
    return bundle.keySet().stream()
        .collect(Collectors.toMap(key -> key, key -> bundle.getString(key)));
  }
}

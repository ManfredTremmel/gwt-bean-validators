package de.knightsoftnet.validators.shared.annotation;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.SOURCE;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * Annotates to read properties into a map.
 */
@Documented
@Target(TYPE)
@Retention(SOURCE)
public @interface ReadProperties {

  /**
   * name of the property file.
   *
   * @return name of the property
   */
  String propertyName();

  /**
   * locale to use for reading property.
   *
   * @return default no locale
   */
  String locale() default "";
}

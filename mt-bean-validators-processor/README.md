mt-bean-validators-processor
============================

Contains annotation processor for mt-bean-validators.


Maven integraten
----------------

Don't do it, it's only used by mt-bean-validators!

To integrate, add the following to your POM file, that's all:

```xml
    <dependency>
      <groupId>de.knightsoft-net</groupId>
      <artifactId>mt-bean-validators-processor</artifactId>
      <version>2.4.1</version>
    </dependency>
```

gwt-bean-validators-rest-resolver
=================================

When you include `IbanUtil` (used by `@BicValue` validator and `BicSuggestBox` widget) or `PhoneNumberUtil` (used by `@PhoneNumberValue` validator and `PhoneNumber*SuggestBox` widgets) a lot of data is compiled into client code. If you include this package, the data is requested by a rest call from server instead of adding it into the client code.

You also need a server side implementation of the rest service on server side, there are two implementations prepared:
* [gwt-bean-validators-restservice-jaxrs](../gwt-bean-validators-restservice-jaxrs) is a jaxrs based rest service
* [gwt-bean-validators-restservice-spring](../gwt-bean-validators-restservice-spring) is a spring controller based rest service

Maven integraten
----------------

The dependency itself for GWT-Projects:

```xml
    <dependency>
      <groupId>de.knightsoft-net</groupId>
      <artifactId>gwt-bean-validators-rest-resolver</artifactId>
      <version>2.4.1</version>
    </dependency>
```

GWT Integration
---------------

```xml
<inherits name="de.knightsoftnet.validators.GwtBeanValidatorsRestResolver" />
```

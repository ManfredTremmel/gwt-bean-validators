/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.client.util;

import de.knightsoftnet.validators.client.rest.path.PathDefinitionInterface;
import de.knightsoftnet.validators.shared.ResourcePaths.BankData;
import de.knightsoftnet.validators.shared.data.BankValidationConstantsData;
import de.knightsoftnet.validators.shared.util.BankConstantsProvider;
import de.knightsoftnet.validators.shared.util.HasSetBankAccountBicSharedConstants;
import de.knightsoftnet.validators.shared.util.HasSetBicMapSharedConstants;
import de.knightsoftnet.validators.shared.util.HasSetIbanLengthMapSharedConstants;

import com.github.nmorel.gwtjackson.client.ObjectMapper;
import com.google.gwt.core.client.GWT;

import elemental2.dom.XMLHttpRequest;

import java.util.ArrayList;
import java.util.List;

/**
 * utility interface for bank constants provider.
 *
 * @author Manfred Tremmel
 *
 */
public class BankConstantsProviderImpl implements BankConstantsProvider {

  protected interface BankValidationConstantsMapper
      extends ObjectMapper<BankValidationConstantsData> {
  }

  private static BankValidationConstantsData CONSTANTS = new BankValidationConstantsData();
  private static List<HasSetIbanLengthMapSharedConstants> IBAN_RECIVERS = new ArrayList<>();
  private static List<HasSetBankAccountBicSharedConstants> BANK_RECIVERS = new ArrayList<>();
  private static List<HasSetBicMapSharedConstants> BIC_RECIVERS = new ArrayList<>();

  static {
    final PathDefinitionInterface pathDefinition = GWT.create(PathDefinitionInterface.class);
    final String url = pathDefinition.getRestBasePath() + BankData.ROOT;
    final XMLHttpRequest xmlHttp = new XMLHttpRequest();
    final BankValidationConstantsMapper mapper = GWT.create(BankValidationConstantsMapper.class);
    xmlHttp.open("GET", url, true); // true for asynchronous request
    xmlHttp.onload = result -> {
      CONSTANTS = mapper.read(xmlHttp.responseText);
      IBAN_RECIVERS.forEach(receiver -> receiver
          .setIbanLengthMapSharedConstants(CONSTANTS.getIbanLengthMapSharedConstants()));
      IBAN_RECIVERS.clear();
      BANK_RECIVERS.forEach(receiver -> receiver
          .setBankAccountBicSharedConstants(CONSTANTS.getBankAccountBicSharedConstants()));
      BANK_RECIVERS.clear();
      BIC_RECIVERS
          .forEach(receiver -> receiver.setBicMapSharedConstants(CONSTANTS.getBicMapConstants()));
      BIC_RECIVERS.clear();
    };
    xmlHttp.send();
  }

  @Override
  public void setIbanLengthMapSharedConstantsWhenAvailable(
      final HasSetIbanLengthMapSharedConstants receiver) {
    if (CONSTANTS.getIbanLengthMapSharedConstants() == null) {
      IBAN_RECIVERS.add(receiver);
    } else {
      receiver.setIbanLengthMapSharedConstants(CONSTANTS.getIbanLengthMapSharedConstants());
    }
  }

  @Override
  public void setBankAccountBicSharedConstantsWhenAvailable(
      final HasSetBankAccountBicSharedConstants receiver) {
    if (CONSTANTS.getBankAccountBicSharedConstants() == null) {
      BANK_RECIVERS.add(receiver);
    } else {
      receiver.setBankAccountBicSharedConstants(CONSTANTS.getBankAccountBicSharedConstants());
    }
  }

  @Override
  public void setBicMapSharedConstantsWhenAvailable(final HasSetBicMapSharedConstants receiver) {
    if (CONSTANTS.getBicMapConstants() == null) {
      BIC_RECIVERS.add(receiver);
    } else {
      receiver.setBicMapSharedConstants(CONSTANTS.getBicMapConstants());
    }
  }
}

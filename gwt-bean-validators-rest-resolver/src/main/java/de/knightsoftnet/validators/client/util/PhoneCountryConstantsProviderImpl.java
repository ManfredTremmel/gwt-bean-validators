/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.client.util;

import de.knightsoftnet.validators.client.rest.path.PathDefinitionInterface;
import de.knightsoftnet.validators.shared.ResourcePaths.PhoneNumber;
import de.knightsoftnet.validators.shared.data.PhoneCountryConstantsImpl;
import de.knightsoftnet.validators.shared.data.PhoneCountryData;
import de.knightsoftnet.validators.shared.data.PhoneCountrySharedConstants;
import de.knightsoftnet.validators.shared.util.HasSetPhoneCountrySharedConstants;
import de.knightsoftnet.validators.shared.util.PhoneCountryConstantsProvider;

import com.github.nmorel.gwtjackson.client.ObjectMapper;
import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.LocaleInfo;

import elemental2.dom.XMLHttpRequest;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * utility class for phone country constants provider, using CreatePhoneCountryConstantsClass as
 * source.
 *
 * @author Manfred Tremmel
 *
 */
public class PhoneCountryConstantsProviderImpl implements PhoneCountryConstantsProvider {

  protected interface PhoneCountryConstantsMapper
      extends ObjectMapper<Map<String, PhoneCountryData>> {
  }

  private static PhoneCountryConstantsImpl CONSTANTS = new PhoneCountryConstantsImpl();
  private static List<HasSetPhoneCountrySharedConstants> RECIVERS = new ArrayList<>();

  static {
    final PathDefinitionInterface pathDefinition = GWT.create(PathDefinitionInterface.class);
    final String url = pathDefinition.getRestBasePath() + PhoneNumber.ROOT
        + PhoneNumber.GET_PHONE_COUNTRY_CONSTANTS + "/"
        + LocaleInfo.getCurrentLocale().getLocaleName();
    final XMLHttpRequest xmlHttp = new XMLHttpRequest();
    final PhoneCountryConstantsMapper mapper = GWT.create(PhoneCountryConstantsMapper.class);
    xmlHttp.open("GET", url, true); // true for asynchronous request
    xmlHttp.onload = result -> {
      CONSTANTS.setCountriesMap(mapper.read(xmlHttp.responseText));
      CONSTANTS.getCountriesMap().values().stream()
          .forEach(entry -> entry.getCountryCodeData().setPhoneCountryData(entry));
      CONSTANTS.setCountryCode(CONSTANTS.getCountriesMap().values().stream()
          .map(entry -> entry.getCountryCodeData()).collect(Collectors.toSet()));
      RECIVERS.forEach(receiver -> receiver.setPhoneCountrySharedConstants(CONSTANTS));
      RECIVERS.clear();
    };
    xmlHttp.send();
  }

  @Override
  public PhoneCountrySharedConstants getPhoneCountryConstants() {
    return CONSTANTS;
  }

  @Override
  public PhoneCountrySharedConstants getPhoneCountryConstants(final Locale locale) {
    return CONSTANTS;
  }

  @Override
  public void setPhoneCountrySharedConstantsWhenAvailable(
      final HasSetPhoneCountrySharedConstants receiver) {
    setPhoneCountrySharedConstantsWhenAvailable(null, receiver);
  }

  @Override
  public void setPhoneCountrySharedConstantsWhenAvailable(final Locale locale,
      final HasSetPhoneCountrySharedConstants receiver) {
    if (hasPhoneCountryConstants()) {
      receiver.setPhoneCountrySharedConstants(CONSTANTS);
    } else {
      RECIVERS.add(receiver);
    }
  }

  @Override
  public boolean hasPhoneCountryConstants() {
    return !CONSTANTS.getCountryCode().isEmpty();
  }
}

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.gwtp.spring.client.converter;

import de.knightsoftnet.gwtp.spring.shared.data.ValidationResultInterface;

import org.hibernate.validator.internal.engine.ConstraintViolationImpl;
import org.hibernate.validator.internal.engine.path.PathImpl;
import org.springframework.core.convert.converter.Converter;

import java.util.Collections;
import java.util.stream.Collectors;

import jakarta.validation.ConstraintViolation;

/**
 * convert ValidationResultData from server to a ArrayList&lt;ConstraintViolation&lt;?&gt;&gt; which
 * can be handled by gwt.
 *
 * @author Manfred Tremmel
 *
 * @param <E> validation type
 */
public class ValidationResultDataConverter<E>
    implements Converter<ValidationResultInterface, Iterable<ConstraintViolation<?>>> {

  @Override
  public Iterable<ConstraintViolation<?>> convert(final ValidationResultInterface source) {
    return this.convert(source, null);
  }

  /**
   * convert ValidationResultData from server to a ArrayList&lt;ConstraintViolation&lt;?&gt;&gt;
   * which can be handled by gwt.
   *
   * @param source ValidationResultData to convert
   * @param bean the validated bean (which is not transfered back to client)
   * @return ArrayList&lt;ConstraintViolation&lt;?&gt;&gt;
   */
  @SuppressWarnings("unchecked")
  public Iterable<ConstraintViolation<?>> convert(final ValidationResultInterface source,
      final E bean) {
    if (source == null) {
      return null;
    }
    return source.getValidationErrorSet().stream()
        .map(violation -> ConstraintViolationImpl.forBeanValidation(null, //
            Collections.emptyMap(), //
            Collections.emptyMap(), //
            violation.getMessage(), //
            (Class<E>) (bean == null ? null : bean.getClass()), //
            bean, //
            null, //
            null, //
            PathImpl.createPathFromString(violation.getPropertyPath()), //
            null, //
            null))
        .collect(Collectors.toList());
  }
}

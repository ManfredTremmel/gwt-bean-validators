package de.knightsoftnet.gwtp.spring.client.rest.helper;

import de.knightsoftnet.gwtp.spring.shared.data.ValidationResultInterface;
import de.knightsoftnet.gwtp.spring.shared.data.ValidationValueInterface;

import java.util.ArrayList;
import java.util.List;

public class ValidationResultImpl implements ValidationResultInterface {
  /**
   * validation result, should be empty if validation is ok.
   */
  private List<ValidationValueInterface> validationErrorSet;

  public ValidationResultImpl() {
    super();
    validationErrorSet = new ArrayList<>();
  }

  public ValidationResultImpl(
      final List<ValidationValueInterface> validationErrorSet) {
    super();
    this.validationErrorSet = validationErrorSet;
  }

  public static ValidationResultImpl of(
      final List<ValidationValueInterface> validationErrorSet) {
    return new ValidationResultImpl(validationErrorSet);
  }

  @Override
  public final List<ValidationValueInterface> getValidationErrorSet() {
    return validationErrorSet;
  }

  @Override
  public final void setValidationErrorSet(final List<ValidationValueInterface> validationErrorSet) {
    this.validationErrorSet = validationErrorSet;
  }
}

package de.knightsoftnet.gwtp.spring.annotation.processor;

import de.knightsoftnet.gwtp.spring.client.annotation.BackofficeClientGenerator;
import de.knightsoftnet.validators.shared.data.FieldTypeEnum;

import java.io.PrintWriter;
import java.util.List;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;

/**
 * Create gwtp module class.
 */
public class BackofficeModuleCreator extends AbstractBackofficeCreator<BackofficeClientGenerator> {

  protected static final String CLASS_SUFFIX = "Module";

  public BackofficeModuleCreator() {
    super(CLASS_SUFFIX);
  }

  @Override
  protected void addAdditionalImports(final String serverPackage, final Element element,
      final BackofficeClientGenerator annotationInterface, final List<BackofficeWidget> widgets,
      final ProcessingEnvironment processingEnv) {
    addImports(//
        "com.gwtplatform.mvp.client.gin.AbstractPresenterModule", //
        "jakarta.inject.Singleton");

    final String entityName = getEntityNameOfElement(element);
    addImport(
        serverPackage + "." + entityName + BackofficePresenterCreator.CLASS_SUFFIX + ".MyProxy");
    addImport(
        serverPackage + "." + entityName + BackofficePresenterCreator.CLASS_SUFFIX + ".MyView");
    addImport(serverPackage + "." + entityName + CLASS_SUFFIX_VIEW + ".Driver");
  }

  @Override
  protected void writeBody(final PrintWriter out, final String serverPackage, final Element element,
      final BackofficeClientGenerator annotationInterface, final List<BackofficeWidget> widgets,
      final ProcessingEnvironment processingEnv) {
    final String entityName = getEntityNameOfElement(element);

    out.print("public class ");
    out.print(entityName);
    out.print(suffix);
    out.println(" extends AbstractPresenterModule {");
    out.println();

    out.println("  @Override");
    out.println("  protected void configure() {");
    out.print("    bindPresenter(");
    out.print(entityName);
    out.print(BackofficePresenterCreator.CLASS_SUFFIX);
    out.print(".class, MyView.class, ");
    out.print(entityName);
    out.print(CLASS_SUFFIX_VIEW);
    out.println(".class, MyProxy.class);");

    out.print("    bind(Driver.class).to(");
    out.print(entityName);
    out.print(CLASS_SUFFIX_VIEW);
    out.println("_Driver_Impl.class).in(Singleton.class);");

    widgets.stream().filter(widget -> widget.getFieldType() == FieldTypeEnum.ONE_TO_MANY)
        .forEach(widget -> {
          out.print("    bind(");
          out.print(widget.getWidgetName());
          out.print(".Driver.class).to(");
          out.print(widget.getWidgetName());
          out.println("_Driver_Impl.class).in(Singleton.class);");
        });

    out.println("  }");
    out.println("}");
  }
}

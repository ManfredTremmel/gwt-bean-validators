package de.knightsoftnet.gwtp.spring.annotation.processor;

import de.knightsoftnet.gwtp.spring.client.annotation.BackofficeClientGenerator;

import com.google.auto.service.AutoService;

import java.util.List;
import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Processor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.TypeElement;

/**
 * Backoffice client generator processor.
 */
@SupportedAnnotationTypes("de.knightsoftnet.gwtp.spring.client.annotation."
    + "BackofficeClientGenerator")
@SupportedSourceVersion(SourceVersion.RELEASE_17)
@AutoService(Processor.class)
public class BackofficeClientGeneratorProcessor extends AbstractProcessor {

  private static final BackofficeServiceCreator SERVICE_CREATOR = new BackofficeServiceCreator();
  private static final BackofficeManyToOneWidgetServiceCreator SERVICE_MTO_CREATOR =
      new BackofficeManyToOneWidgetServiceCreator();
  private static final BackofficeLocalPropertyCreator LOCAL_PROPETY_CREATOR =
      new BackofficeLocalPropertyCreator();
  private static final BackofficeLocalInterfaceCreator LOCAL_INTERFACE_CREATOR =
      new BackofficeLocalInterfaceCreator();
  private static final BackofficePresenterCreator PRESENTER_CREATOR =
      new BackofficePresenterCreator();
  private static final BackofficeViewCreator VIEW_CREATOR = new BackofficeViewCreator();
  private static final BackofficeUiBinderCreator UI_BINDER_CREATOR =
      new BackofficeUiBinderCreator();
  private static final BackofficeModuleCreator MODULE_CREATOR = new BackofficeModuleCreator();
  private static final BackofficeManyToOneWidgetSuggestionCreator SERVICE_MTO_SUGGESTIONS_CREATOR =
      new BackofficeManyToOneWidgetSuggestionCreator();
  private static final BackofficeManyToOneWidgetCreator SERVICE_MTO_WIDGET_CREATOR =
      new BackofficeManyToOneWidgetCreator();

  @Override
  public boolean process(final Set<? extends TypeElement> annotations,
      final RoundEnvironment roundEnv) {
    if (annotations == null || annotations.isEmpty() || roundEnv == null || processingEnv == null) {
      return false;
    }
    if (!roundEnv.processingOver()) {
      roundEnv.getElementsAnnotatedWith(BackofficeClientGenerator.class).forEach(element -> {

        final BackofficeClientGenerator generator =
            element.getAnnotation(BackofficeClientGenerator.class);
        final List<BackofficeWidget> widgets = AbstractBackofficeCreator
            .detectBackofficeWidgetsOfElement("", null, element, true, processingEnv);

        if (generator.generateRestService()) {
          SERVICE_CREATOR.writeClassOrInterface(element, generator, widgets, processingEnv);
          SERVICE_MTO_CREATOR.writeClassOrInterface(element, generator, widgets, processingEnv);
        }

        if (generator.generateLocalizationProperties()) {
          for (final String language : generator.languages()) {
            LOCAL_PROPETY_CREATOR.writeProperty(element, language, widgets, processingEnv);
          }
        }

        if (generator.generateLocalizationInterface()) {
          LOCAL_INTERFACE_CREATOR.writeClassOrInterface(element, generator, widgets, processingEnv);
        }

        if (generator.generatePresenter()) {
          PRESENTER_CREATOR.writeClassOrInterface(element, generator, widgets, processingEnv);
        }

        if (generator.generateView()) {
          SERVICE_MTO_SUGGESTIONS_CREATOR.writeClassOrInterface(element, generator, widgets,
              processingEnv);
          SERVICE_MTO_WIDGET_CREATOR.writeClassOrInterface(element, generator, widgets,
              processingEnv);
          VIEW_CREATOR.writeClassOrInterface(element, generator, widgets, processingEnv);
        }

        if (generator.generateUiBinder()) {
          UI_BINDER_CREATOR.writeUiBinder(element, generator, widgets, processingEnv);
        }

        if (generator.generateGinModule()) {
          MODULE_CREATOR.writeClassOrInterface(element, generator, widgets, processingEnv);
        }
      });
    }

    return true;
  }
}

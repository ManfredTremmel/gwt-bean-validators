package de.knightsoftnet.gwtp.spring.annotation.processor;

import de.knightsoftnet.gwtp.spring.client.annotation.BackofficeClientGenerator;
import de.knightsoftnet.validators.annotation.processor.TypeUtils;
import de.knightsoftnet.validators.shared.data.FieldTypeEnum;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.annotation.processing.FilerException;
import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.DeclaredType;
import javax.tools.Diagnostic;
import javax.tools.FileObject;
import javax.tools.StandardLocation;

/**
 * Create localization property file.
 */
public class BackofficeLocalPropertyCreator
    extends AbstractBackofficeCreator<BackofficeClientGenerator> {

  public BackofficeLocalPropertyCreator() {
    super("Messages_");
  }

  /**
   * write property file.
   */
  public void writeProperty(final Element element, final String language,
      final List<BackofficeWidget> widgets, final ProcessingEnvironment processingEnv) {
    final String serverPackage = detectPackage(element, processingEnv);
    final String resourceName = getEntityNameOfElement(element) + suffix + language + ".properties";

    try {
      final FileObject builderFile = processingEnv.getFiler()
          .createResource(StandardLocation.SOURCE_OUTPUT, serverPackage, resourceName);
      try (PrintWriter out = new PrintWriter(builderFile.openWriter())) {
        out.print("header=");
        out.println(getEntityNameOfElement(element));
        detectBackofficeWidgetsOfElementFlat(widgets).stream()
            .forEach(widget -> singleProperty(out, widget, processingEnv));
      }
    } catch (final FilerException e) {
      // happens when trying to recreate an existing interface
      processingEnv.getMessager().printMessage(Diagnostic.Kind.NOTE, e.getMessage());
    } catch (final IOException e) {
      e.printStackTrace();
    }
  }

  private void singleProperty(final PrintWriter out, final BackofficeWidget widget,
      final ProcessingEnvironment processingEnv) {
    out.print(widget.getName());
    out.print("=");
    out.println(widget.getName());
    if (widget.getFieldType() == FieldTypeEnum.ENUM_FIXED
        && !"de.knightsoftnet.validators.shared.data.CountryEnum".equals(TypeUtils.getClassName(
            processingEnv.getTypeUtils().asMemberOf(widget.getContaining(), widget.getField())))) {
      for (final String enumValue : getEnumValues((TypeElement) ((DeclaredType) processingEnv
          .getTypeUtils().asMemberOf(widget.getContaining(), widget.getField())).asElement())) {
        out.print(widget.getName());
        out.print("[");
        out.print(enumValue);
        out.print("]=");
        out.println(enumValue);
      }
    }
  }

  @Override
  protected void addAdditionalImports(final String serverPackage, final Element element,
      final BackofficeClientGenerator annotationInterface, final List<BackofficeWidget> widgets,
      final ProcessingEnvironment processingEnv) {
    // ignore
  }

  @Override
  protected void writeBody(final PrintWriter out, final String serverPackage, final Element element,
      final BackofficeClientGenerator annotationInterface, final List<BackofficeWidget> widgets,
      final ProcessingEnvironment processingEnv) {
    // ignore
  }
}

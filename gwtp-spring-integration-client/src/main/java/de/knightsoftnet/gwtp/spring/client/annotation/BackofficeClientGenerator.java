/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.gwtp.spring.client.annotation;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.SOURCE;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * Annotates a to generate client side backoffice stuff.
 */
@Documented
@Target(FIELD)
@Retention(SOURCE)
public @interface BackofficeClientGenerator {

  String GSSS_RESOURCE_FILE = "de.knightsoftnet.gwtp.spring.client.resources.ResourcesFile";
  String GSSS_UIB_RESOURCE_FILE =
      "<ui:with field=\"resources\" type=\"" + GSSS_RESOURCE_FILE + "\" />";
  String GSSS_JAVA_COLUMN = "resources.grid().col()";
  String GSSS_JAVA_COLUMN_1 = "resources.grid().col_1()";
  String GSSS_JAVA_COLUMN_2 = "resources.grid().col_2()";
  String GSSS_JAVA_COLUMN_3 = "resources.grid().col_3()";
  String GSSS_JAVA_COLUMN_4 = "resources.grid().col_4()";
  String GSSS_JAVA_COLUMN_5 = "resources.grid().col_5()";
  String GSSS_JAVA_COLUMN_6 = "resources.grid().col_6()";
  String GSSS_JAVA_COLUMN_7 = "resources.grid().col_7()";
  String GSSS_JAVA_COLUMN_8 = "resources.grid().col_8()";
  String GSSS_JAVA_COLUMN_9 = "resources.grid().col_9()";
  String GSSS_JAVA_COLUMN_10 = "resources.grid().col_10()";
  String GSSS_JAVA_COLUMN_11 = "resources.grid().col_11()";
  String GSSS_JAVA_COLUMN_12 = "resources.grid().col_12()";
  String GSSS_JAVA_COLUMN_T_1 = "resources.grid().col_t_1()";
  String GSSS_JAVA_COLUMN_T_2 = "resources.grid().col_t_2()";
  String GSSS_JAVA_COLUMN_T_3 = "resources.grid().col_t_3()";
  String GSSS_JAVA_COLUMN_T_4 = "resources.grid().col_t_4()";
  String GSSS_JAVA_COLUMN_T_5 = "resources.grid().col_t_5()";
  String GSSS_JAVA_COLUMN_T_6 = "resources.grid().col_t_6()";
  String GSSS_JAVA_COLUMN_T_7 = "resources.grid().col_t_7()";
  String GSSS_JAVA_COLUMN_T_8 = "resources.grid().col_t_8()";
  String GSSS_JAVA_COLUMN_T_9 = "resources.grid().col_t_9()";
  String GSSS_JAVA_COLUMN_T_10 = "resources.grid().col_t_10()";
  String GSSS_JAVA_COLUMN_T_11 = "resources.grid().col_t_11()";
  String GSSS_JAVA_COLUMN_T_12 = "resources.grid().col_t_12()";
  String GSSS_JAVA_COLUMN_M_1 = "resources.grid().col_m_1()";
  String GSSS_JAVA_COLUMN_M_2 = "resources.grid().col_m_2()";
  String GSSS_JAVA_COLUMN_M_3 = "resources.grid().col_m_3()";
  String GSSS_JAVA_COLUMN_M_4 = "resources.grid().col_m_4()";
  String GSSS_JAVA_COLUMN_M_5 = "resources.grid().col_m_5()";
  String GSSS_JAVA_COLUMN_M_6 = "resources.grid().col_m_6()";
  String GSSS_JAVA_COLUMN_M_7 = "resources.grid().col_m_7()";
  String GSSS_JAVA_COLUMN_M_8 = "resources.grid().col_m_8()";
  String GSSS_JAVA_COLUMN_M_9 = "resources.grid().col_m_9()";
  String GSSS_JAVA_COLUMN_M_10 = "resources.grid().col_m_10()";
  String GSSS_JAVA_COLUMN_M_11 = "resources.grid().col_m_11()";
  String GSSS_JAVA_COLUMN_M_12 = "resources.grid().col_m_12()";
  String GSSS_UIB_ROW = "{resources.grid.row}";
  String GSSS_UIB_COLUMN = "{resources.grid.col}";
  String GSSS_UIB_COLUMN_1 = "{resources.grid.col_1}";
  String GSSS_UIB_COLUMN_2 = "{resources.grid.col_2}";
  String GSSS_UIB_COLUMN_3 = "{resources.grid.col_3}";
  String GSSS_UIB_COLUMN_4 = "{resources.grid.col_4}";
  String GSSS_UIB_COLUMN_5 = "{resources.grid.col_5}";
  String GSSS_UIB_COLUMN_6 = "{resources.grid.col_6}";
  String GSSS_UIB_COLUMN_7 = "{resources.grid.col_7}";
  String GSSS_UIB_COLUMN_8 = "{resources.grid.col_8}";
  String GSSS_UIB_COLUMN_9 = "{resources.grid.col_9}";
  String GSSS_UIB_COLUMN_10 = "{resources.grid.col_10}";
  String GSSS_UIB_COLUMN_11 = "{resources.grid.col_11}";
  String GSSS_UIB_COLUMN_12 = "{resources.grid.col_12}";
  String GSSS_UIB_COLUMN_T_1 = "{resources.grid.col_t_1}";
  String GSSS_UIB_COLUMN_T_2 = "{resources.grid.col_t_2}";
  String GSSS_UIB_COLUMN_T_3 = "{resources.grid.col_t_3}";
  String GSSS_UIB_COLUMN_T_4 = "{resources.grid.col_t_4}";
  String GSSS_UIB_COLUMN_T_5 = "{resources.grid.col_t_5}";
  String GSSS_UIB_COLUMN_T_6 = "{resources.grid.col_t_6}";
  String GSSS_UIB_COLUMN_T_7 = "{resources.grid.col_t_7}";
  String GSSS_UIB_COLUMN_T_8 = "{resources.grid.col_t_8}";
  String GSSS_UIB_COLUMN_T_9 = "{resources.grid.col_t_9}";
  String GSSS_UIB_COLUMN_T_10 = "{resources.grid.col_t_10}";
  String GSSS_UIB_COLUMN_T_11 = "{resources.grid.col_t_11}";
  String GSSS_UIB_COLUMN_T_12 = "{resources.grid.col_t_12}";
  String GSSS_UIB_COLUMN_M_1 = "{resources.grid.col_m_1}";
  String GSSS_UIB_COLUMN_M_2 = "{resources.grid.col_m_2}";
  String GSSS_UIB_COLUMN_M_3 = "{resources.grid.col_m_3}";
  String GSSS_UIB_COLUMN_M_4 = "{resources.grid.col_m_4}";
  String GSSS_UIB_COLUMN_M_5 = "{resources.grid.col_m_5}";
  String GSSS_UIB_COLUMN_M_6 = "{resources.grid.col_m_6}";
  String GSSS_UIB_COLUMN_M_7 = "{resources.grid.col_m_7}";
  String GSSS_UIB_COLUMN_M_8 = "{resources.grid.col_m_8}";
  String GSSS_UIB_COLUMN_M_9 = "{resources.grid.col_m_9}";
  String GSSS_UIB_COLUMN_M_10 = "{resources.grid.col_m_10}";
  String GSSS_UIB_COLUMN_M_11 = "{resources.grid.col_m_11}";
  String GSSS_UIB_COLUMN_M_12 = "{resources.grid.col_m_12}";
  String GSSS_UIB_RESPONSIVE_COLUMN_1 = GSSS_UIB_COLUMN + " " + GSSS_UIB_COLUMN_1 + " "
      + GSSS_UIB_COLUMN_T_2 + " " + GSSS_UIB_COLUMN_M_12;
  String GSSS_UIB_RESPONSIVE_COLUMN_2 = GSSS_UIB_COLUMN + " " + GSSS_UIB_COLUMN_2 + " "
      + GSSS_UIB_COLUMN_T_3 + " " + GSSS_UIB_COLUMN_M_12;
  String GSSS_UIB_RESPONSIVE_COLUMN_3 = GSSS_UIB_COLUMN + " " + GSSS_UIB_COLUMN_3 + " "
      + GSSS_UIB_COLUMN_T_6 + " " + GSSS_UIB_COLUMN_M_12;
  String GSSS_UIB_RESPONSIVE_COLUMN_4 = GSSS_UIB_COLUMN + " " + GSSS_UIB_COLUMN_4 + " "
      + GSSS_UIB_COLUMN_T_9 + " " + GSSS_UIB_COLUMN_M_12;
  String GSSS_UIB_RESPONSIVE_COLUMN_5 =
      GSSS_UIB_COLUMN + " " + GSSS_UIB_COLUMN_5 + " " + GSSS_UIB_COLUMN_T_12;
  String GSSS_UIB_RESPONSIVE_COLUMN_6 =
      GSSS_UIB_COLUMN + " " + GSSS_UIB_COLUMN_6 + " " + GSSS_UIB_COLUMN_T_12;
  String GSSS_UIB_RESPONSIVE_COLUMN_7 =
      GSSS_UIB_COLUMN + " " + GSSS_UIB_COLUMN_7 + " " + GSSS_UIB_COLUMN_T_12;
  String GSSS_UIB_RESPONSIVE_COLUMN_8 =
      GSSS_UIB_COLUMN + " " + GSSS_UIB_COLUMN_8 + " " + GSSS_UIB_COLUMN_T_12;
  String GSSS_UIB_RESPONSIVE_COLUMN_9 =
      GSSS_UIB_COLUMN + " " + GSSS_UIB_COLUMN_9 + " " + GSSS_UIB_COLUMN_T_12;
  String GSSS_UIB_RESPONSIVE_COLUMN_10 =
      GSSS_UIB_COLUMN + " " + GSSS_UIB_COLUMN_10 + " " + GSSS_UIB_COLUMN_T_12;
  String GSSS_UIB_RESPONSIVE_COLUMN_11 =
      GSSS_UIB_COLUMN + " " + GSSS_UIB_COLUMN_11 + " " + GSSS_UIB_COLUMN_T_12;
  String GSSS_UIB_RESPONSIVE_COLUMN_12 = GSSS_UIB_COLUMN + " " + GSSS_UIB_COLUMN_12;
  String GSSS_JAVA_RESPONSIVE_COLUMN_1 = GSSS_JAVA_COLUMN + ", " + GSSS_JAVA_COLUMN_1 + ", "
      + GSSS_JAVA_COLUMN_T_2 + ", " + GSSS_JAVA_COLUMN_M_12;
  String GSSS_JAVA_RESPONSIVE_COLUMN_2 = GSSS_JAVA_COLUMN + ", " + GSSS_JAVA_COLUMN_2 + ", "
      + GSSS_JAVA_COLUMN_T_3 + ", " + GSSS_JAVA_COLUMN_M_12;
  String GSSS_JAVA_RESPONSIVE_COLUMN_3 = GSSS_JAVA_COLUMN + ", " + GSSS_JAVA_COLUMN_3 + ", "
      + GSSS_JAVA_COLUMN_T_6 + ", " + GSSS_JAVA_COLUMN_M_12;
  String GSSS_JAVA_RESPONSIVE_COLUMN_4 = GSSS_JAVA_COLUMN + ", " + GSSS_JAVA_COLUMN_4 + ", "
      + GSSS_JAVA_COLUMN_T_9 + ", " + GSSS_JAVA_COLUMN_M_12;
  String GSSS_JAVA_RESPONSIVE_COLUMN_5 =
      GSSS_JAVA_COLUMN + ", " + GSSS_JAVA_COLUMN_5 + ", " + GSSS_JAVA_COLUMN_T_12;
  String GSSS_JAVA_RESPONSIVE_COLUMN_6 =
      GSSS_JAVA_COLUMN + ", " + GSSS_JAVA_COLUMN_6 + ", " + GSSS_JAVA_COLUMN_T_12;
  String GSSS_JAVA_RESPONSIVE_COLUMN_7 =
      GSSS_JAVA_COLUMN + ", " + GSSS_JAVA_COLUMN_7 + ", " + GSSS_JAVA_COLUMN_T_12;
  String GSSS_JAVA_RESPONSIVE_COLUMN_8 =
      GSSS_JAVA_COLUMN + ", " + GSSS_JAVA_COLUMN_8 + ", " + GSSS_JAVA_COLUMN_T_12;
  String GSSS_JAVA_RESPONSIVE_COLUMN_9 =
      GSSS_JAVA_COLUMN + ", " + GSSS_JAVA_COLUMN_9 + ", " + GSSS_JAVA_COLUMN_T_12;
  String GSSS_JAVA_RESPONSIVE_COLUMN_10 =
      GSSS_JAVA_COLUMN + ", " + GSSS_JAVA_COLUMN_10 + ", " + GSSS_JAVA_COLUMN_T_12;
  String GSSS_JAVA_RESPONSIVE_COLUMN_11 =
      GSSS_JAVA_COLUMN + ", " + GSSS_JAVA_COLUMN_11 + ", " + GSSS_JAVA_COLUMN_T_12;
  String GSSS_JAVA_RESPONSIVE_COLUMN_12 = GSSS_JAVA_COLUMN + ", " + GSSS_JAVA_COLUMN_12;

  /**
   * server path where to reach rest interface.
   *
   * @return path
   */
  String value();

  /**
   * The token to access page, id will be added automatically.
   *
   * @return baseToken
   */
  String baseToken();

  /**
   * The list of languages to generate messages for.
   *
   * @return array of strings
   */
  String[] languages() default {"en"};

  /**
   * The list of additional imports for presenter.
   *
   * @return array of strings
   */
  String[] additionalPresenterImports() default {GSSS_RESOURCE_FILE};

  /**
   * The list of additional constructor parameters for presenter.
   *
   * @return array of strings
   */
  String[] additionalPresenterConstructorPrarameters() default {"final ResourcesFile resources"};

  /**
   * The list of search result fields displayed in search result list, comma separated css classes
   * are attached.
   *
   * @return array of SearchResultEntry definitions
   */
  SearchField[] searchResults();

  /**
   * The list of ui binder header lines.
   *
   * @return array of ui binder header lines
   */
  String[] uiBinderHeader() default {GSSS_UIB_RESOURCE_FILE};

  /**
   * CSS Style for the central html panel.
   *
   * @return uiBinderStyleCentralPanel
   */
  String uiBinderStyleCentralPanel() default "";

  /**
   * CSS Style for rows.
   *
   * @return uiBinderStyleRow
   */
  String uiBinderStyleRow() default GSSS_UIB_ROW;

  /**
   * CSS Style for the header block.
   *
   * @return uiBinderStyleHeader
   */
  String uiBinderStyleHeader() default GSSS_UIB_RESPONSIVE_COLUMN_12;

  /**
   * CSS Style for the label block.
   *
   * @return uiBinderStyleLabel
   */
  String uiBinderStyleLabel() default GSSS_UIB_RESPONSIVE_COLUMN_2;

  /**
   * CSS Style for the widget block.
   *
   * @return uiBinderStyleWidget
   */
  String uiBinderStyleWidget() default GSSS_UIB_RESPONSIVE_COLUMN_4;

  /**
   * CSS Style for the error block.
   *
   * @return uiBinderErrorStyle
   */
  String uiBinderErrorStyle() default "";

  /**
   * The list of ui binder class entries for the desktop.
   *
   * @return array of ui binder desktop classes
   */
  String[] uiBinderGridClassesColumn() default {//
      GSSS_UIB_RESPONSIVE_COLUMN_1, //
      GSSS_UIB_RESPONSIVE_COLUMN_2, //
      GSSS_UIB_RESPONSIVE_COLUMN_3, //
      GSSS_UIB_RESPONSIVE_COLUMN_4, //
      GSSS_UIB_RESPONSIVE_COLUMN_5, //
      GSSS_UIB_RESPONSIVE_COLUMN_6, //
      GSSS_UIB_RESPONSIVE_COLUMN_7, //
      GSSS_UIB_RESPONSIVE_COLUMN_8, //
      GSSS_UIB_RESPONSIVE_COLUMN_9, //
      GSSS_UIB_RESPONSIVE_COLUMN_10, //
      GSSS_UIB_RESPONSIVE_COLUMN_11, //
      GSSS_UIB_RESPONSIVE_COLUMN_12};

  /**
   * CSS Style for the error block.
   *
   * @return uiBinderErrorStyle
   */
  String[] javaGridClassesColumn() default {//
      GSSS_JAVA_RESPONSIVE_COLUMN_1, //
      GSSS_JAVA_RESPONSIVE_COLUMN_2, //
      GSSS_JAVA_RESPONSIVE_COLUMN_3, //
      GSSS_JAVA_RESPONSIVE_COLUMN_4, //
      GSSS_JAVA_RESPONSIVE_COLUMN_5, //
      GSSS_JAVA_RESPONSIVE_COLUMN_6, //
      GSSS_JAVA_RESPONSIVE_COLUMN_7, //
      GSSS_JAVA_RESPONSIVE_COLUMN_8, //
      GSSS_JAVA_RESPONSIVE_COLUMN_9, //
      GSSS_JAVA_RESPONSIVE_COLUMN_10, //
      GSSS_JAVA_RESPONSIVE_COLUMN_11, //
      GSSS_JAVA_RESPONSIVE_COLUMN_12};

  /**
   * generate rest service for entity.
   *
   * @return boolean, true when generating service
   */
  boolean generateRestService() default true;

  /**
   * generate presenter.
   *
   * @return boolean, true when generating presenter
   */
  boolean generatePresenter() default true;

  /**
   * generate view .
   *
   * @return boolean, true when generating view
   */
  boolean generateView() default true;

  /**
   * generate gin module file.
   *
   * @return boolean, true when generating gin module
   */
  boolean generateGinModule() default true;

  /**
   * generate ui-binder xml file.
   *
   * @return boolean, true when generating ui-Binder
   */
  boolean generateUiBinder() default true;

  /**
   * generate localization interface.
   *
   * @return boolean, true when generating localization interface
   */
  boolean generateLocalizationInterface() default true;

  /**
   * generate localization properties.
   *
   * @return boolean, true when generating localization properties
   */
  boolean generateLocalizationProperties() default true;
}

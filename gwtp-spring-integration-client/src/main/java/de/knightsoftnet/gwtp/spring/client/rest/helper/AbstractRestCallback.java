/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.gwtp.spring.client.rest.helper;

import de.knightsoftnet.gwtp.spring.client.session.Session;

import com.google.gwt.core.client.GWT;
import com.google.gwt.http.client.Response;
import com.google.web.bindery.autobean.shared.Splittable;
import com.google.web.bindery.autobean.shared.impl.StringQuoter;
import com.gwtplatform.dispatch.rest.client.RestCallback;
import com.gwtplatform.dispatch.shared.ActionException;

import org.hibernate.validator.internal.engine.ConstraintViolationImpl;
import org.hibernate.validator.internal.engine.path.PathImpl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import jakarta.validation.ConstraintViolation;

/**
 * Async callback implementation with error handling.
 *
 * @author Manfred Tremmel
 *
 * @param <P> presenter type
 * @param <D> data type given to the server
 * @param <V> view or widget which implements EditorWithErrorHandling interface
 * @param <R> rest result type
 * @param <H> http response message type
 */
public abstract class AbstractRestCallback<P, D, V extends EditorWithErrorHandling<P, D>, R, //
    H extends HttpMessages> implements RestCallback<R> {
  private static final String VALIDATION_ERROR_SET = "validationErrorSet";
  private static final String MESSAGE = "message";
  private static final String PROPERTY_PATH = "propertyPath";

  protected final V view;
  protected final D data;
  protected final Session session;
  private final H httpMessage;
  private Response response;

  /**
   * constructor.
   *
   * @param view view
   * @param data date to handle
   * @param session session data
   */
  protected AbstractRestCallback(final V view, final D data, final Session session) {
    this(view, data, session, GWT.create(HttpMessages.class));
  }

  /**
   * constructor.
   *
   * @param view view
   * @param data date to handle
   * @param session session data
   * @param httpMessage http code messages to show
   */
  protected AbstractRestCallback(final V view, final D data, final Session session,
      final H httpMessage) {
    super();
    this.view = view;
    this.data = data;
    this.session = session;
    this.httpMessage = httpMessage;
  }

  @Override
  public void onFailure(final Throwable caught) {
    try {
      throw caught;
    } catch (final ActionException e) {
      switch (response.getStatusCode()) {
        case 400: // server side validation error, give results to editor
          view.setConstraintViolations(
              deserializeValidationResultInterface(response.getText(), data));
          break;
        case 401, // Unauthorized: happens when session times out
            403: // Forbidden: happens when csrf token times out
          session.readSessionData();
          break;
        default:
          view.showMessage(httpMessage.messageHttpCode(response.getStatusCode()));
          break;
      }
    } catch (final Throwable e) {
      view.showMessage(e.getMessage());
    }
  }

  @SuppressWarnings("unchecked")
  private List<ConstraintViolation<?>> deserializeValidationResultInterface(final String jsonString,
      final D bean) {
    final Splittable splittable = StringQuoter.split(jsonString);
    final List<ConstraintViolation<?>> validationValues = new ArrayList<>();
    if (!splittable.isNull(VALIDATION_ERROR_SET)) {
      for (int i = 0; i < splittable.get(VALIDATION_ERROR_SET).size(); i++) {
        final Splittable errorEntry = splittable.get(VALIDATION_ERROR_SET).get(i);
        validationValues.add(ConstraintViolationImpl.forBeanValidation(null, //
            Collections.emptyMap(), //
            Collections.emptyMap(), //
            errorEntry.get(MESSAGE).asString(), //
            (Class<D>) (bean == null ? null : bean.getClass()), //
            bean, //
            null, //
            null, //
            PathImpl.createPathFromString(errorEntry.get(PROPERTY_PATH).asString()), //
            null, //
            null));
      }
    }
    return validationValues;
  }

  @Override
  public void setResponse(final Response response) {
    this.response = response;
  }

  @Override
  public abstract void onSuccess(final R result);
}

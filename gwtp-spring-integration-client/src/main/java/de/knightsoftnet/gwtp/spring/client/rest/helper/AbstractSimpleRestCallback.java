/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.gwtp.spring.client.rest.helper;

import de.knightsoftnet.gwtp.spring.client.session.Session;

import com.google.gwt.core.client.GWT;
import com.google.gwt.http.client.Response;
import com.gwtplatform.dispatch.rest.client.RestCallback;
import com.gwtplatform.dispatch.shared.ActionException;

/**
 * Async callback implementation with error handling.
 *
 * @author Manfred Tremmel
 *
 * @param <V> view or widget which implements HasShowMessage interface
 * @param <R> rest result type
 * @param <H> http response message type
 */
public abstract class AbstractSimpleRestCallback<V extends HasShowMessage, R, //
    H extends HttpMessages> implements RestCallback<R> {

  protected final V view;
  protected final Session session;
  private final H httpMessage;
  private Response response;

  /**
   * constructor.
   *
   * @param view view
   * @param session session data
   */
  protected AbstractSimpleRestCallback(final V view, final Session session) {
    this(view, session, GWT.create(HttpMessages.class));
  }

  /**
   * constructor.
   *
   * @param view view
   * @param session session data
   * @param httpMessage http code messages to show
   */
  protected AbstractSimpleRestCallback(final V view, final Session session, final H httpMessage) {
    super();
    this.view = view;
    this.session = session;
    this.httpMessage = httpMessage;
  }

  @Override
  public void onFailure(final Throwable caught) {
    try {
      throw caught;
    } catch (final ActionException e) {
      switch (response.getStatusCode()) {
        case 401, // Unauthorized: happens when session times out
            403: // Forbidden: happens when csrf token times out
          session.readSessionData();
          break;
        default:
          view.showMessage(httpMessage.messageHttpCode(response.getStatusCode()));
          break;
      }
    } catch (final Throwable e) {
      view.showMessage(e.getMessage());
    }
  }

  @Override
  public void setResponse(final Response response) {
    this.response = response;
  }

  @Override
  public abstract void onSuccess(final R result);
}

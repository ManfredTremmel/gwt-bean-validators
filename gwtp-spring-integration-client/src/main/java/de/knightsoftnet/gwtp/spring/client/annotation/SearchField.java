/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.gwtp.spring.client.annotation;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.SOURCE;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * search field result definition.
 */
@Target(FIELD)
@Retention(SOURCE)
public @interface SearchField {
  /**
   * name of the field that should be added to search result list.
   *
   * @return the name of the field that should be added to search result list
   */
  String field();

  /**
   * alternate to gss styling, you can give number columns in the grid.
   *
   * @return number columns of a used grid
   */
  int gridColumns() default 0;

  /**
   * array of gss styling entries for ui binder.
   *
   * @return the array of gss styling entries for ui binder
   */
  String[] styling() default {};
}

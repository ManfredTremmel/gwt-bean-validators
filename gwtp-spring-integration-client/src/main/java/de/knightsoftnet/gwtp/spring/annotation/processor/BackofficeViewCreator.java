package de.knightsoftnet.gwtp.spring.annotation.processor;

import de.knightsoftnet.gwtp.spring.client.annotation.BackofficeClientGenerator;
import de.knightsoftnet.validators.annotation.processor.TypeUtils;
import de.knightsoftnet.validators.shared.data.FieldTypeEnum;

import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.processing.FilerException;
import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;
import javax.tools.Diagnostic;
import javax.tools.JavaFileObject;

/**
 * Create gwtp view class.
 */
public class BackofficeViewCreator extends AbstractBackofficeCreator<BackofficeClientGenerator> {

  protected static final String CLASS_SUFFIX = CLASS_SUFFIX_VIEW;

  public BackofficeViewCreator() {
    super(CLASS_SUFFIX);
  }

  @Override
  protected void addAdditionalImports(final String serverPackage, final Element element,
      final BackofficeClientGenerator annotationInterface, final List<BackofficeWidget> widgets,
      final ProcessingEnvironment processingEnv) {
    addImports(//
        "de.knightsoftnet.mtwidgets.client.ui.page.admin.AbstractAdminView", //
        "de.knightsoftnet.mtwidgets.client.ui.widget.AdminNavigationWidget", //
        "de.knightsoftnet.validators.client.editor.BeanValidationEditorDriver", //
        "de.knightsoftnet.validators.client.editor.annotation.IsValidationDriver", //
        "com.google.gwt.uibinder.client.UiBinder", //
        "com.google.gwt.uibinder.client.UiField", //
        "com.google.gwt.user.client.ui.Widget", //
        "jakarta.inject.Inject", //
        "jakarta.inject.Provider");

    final String entityName = getEntityNameOfElement(element);
    addImport(element.asType());
    addImport(serverPackage + "." + entityName + BackofficeLocalInterfaceCreator.CLASS_SUFFIX);
    addImport(
        serverPackage + "." + entityName + BackofficePresenterCreator.CLASS_SUFFIX + ".MyProxy");
    addImport(
        serverPackage + "." + entityName + BackofficePresenterCreator.CLASS_SUFFIX + ".MyView");
    detectBackofficeWidgetsOfElement("", null, element, true, processingEnv).forEach(
        widget -> widget.getImports().stream().forEach(importEntry -> addImport(importEntry)));
    if (detectBackofficeWidgetsOfElement("", null, element, true, processingEnv).stream()
        .anyMatch(widget -> !widget.isIgnore() && widget.isProviderNeeded())) {
      addImport("com.google.gwt.uibinder.client.UiFactory");
    }
  }

  @Override
  protected void writeBody(final PrintWriter out, final String serverPackage, final Element element,
      final BackofficeClientGenerator annotationInterface, final List<BackofficeWidget> widgets,
      final ProcessingEnvironment processingEnv) {
    final String entityName = getEntityNameOfElement(element);

    out.print("public class ");
    out.print(entityName);
    out.print(suffix);
    out.print(" extends AbstractAdminView<");
    out.print(entityName);
    out.println(", MyProxy, MyView>");
    out.println("    implements MyView {");
    out.println();

    out.print("  public static final String TOKEN = \"");
    out.print(annotationInterface.baseToken());
    out.println("\";");
    out.println("  public static final String TOKEN_WITH_ID = TOKEN + \"{id}\";");
    out.println();

    out.print("  interface Binder extends UiBinder<Widget, ");
    out.print(entityName);
    out.print(suffix);
    out.println("> {");
    out.println("  }");
    out.println();

    out.println("  @IsValidationDriver");
    out.print("  interface Driver extends BeanValidationEditorDriver<");
    out.print(entityName);
    out.print(", ");
    out.print(entityName);
    out.print(suffix);
    out.println("> {");
    out.println("  }");
    out.println();

    writeUiFieldDefinitions(out, widgets, element, annotationInterface, processingEnv);

    out.println();
    out.println("  @Inject");
    out.print("  public ");
    out.print(entityName);
    out.print(suffix);
    out.println("(final Driver driver, final Binder uiBinder,");
    out.print("      final Provider<AdminNavigationWidget<");
    out.print(entityName);
    out.println(">> adminNavigationProvider,");
    writeProviderParameters(out, widgets);
    out.print("      final ");
    out.print(entityName);
    out.print(BackofficeLocalInterfaceCreator.CLASS_SUFFIX);
    out.println(" messages) {");

    out.println("    super(driver, adminNavigationProvider);");

    writeInitializationProvidedUiFields(out, widgets);
    writeProviderInitialization(out, widgets);
    out.println("    initWidget(uiBinder.createAndBindUi(this));");
    writeUiFieldDrivers(out, widgets);
    out.println("    driver.initialize(this);");
    out.println("    driver.setSubmitButton(adminNavigation.getSaveEntry());");
    out.println("    driver.addFormSubmitHandler(this);");
    writeUiFieldValueSetting(out, widgets);
    out.println("  }");

    writeUiHandlers(out, widgets);

    writeProviderMethods(out, widgets);

    out.println("}");
  }

  private void writeUiFieldDefinitions(final PrintWriter out, final List<BackofficeWidget> widgets,
      final Element element, final BackofficeClientGenerator annotationInterface,
      final ProcessingEnvironment processingEnv) {
    widgets.stream().filter(widget -> !widget.isIgnore()).forEach(widget -> {
      switch (widget.getFieldType()) {
        case EMBEDDED:
          writeEmbeddedView(widget, element, annotationInterface, processingEnv);
          break;
        case ONE_TO_MANY:
          writeOneToManyEntryView(widget, element, annotationInterface, processingEnv);
          writeOneToManyListEditor(widget, element, processingEnv);
          break;
        default:
          break;
      }
      if (widget.isProvided()) {
        out.println("  @UiField(provided = true)");
      } else {
        out.println("  @UiField");
      }
      out.print("  ");
      out.print(widget.getWidgetName());
      out.print(" ");
      out.print(widget.getName());
      out.println(";");
    });

    final List<String> providerWidgets = allWidgetNamesOfProviderWidgets(widgets);
    if (!providerWidgets.isEmpty()) {
      out.println();
      providerWidgets.forEach(widget -> {
        out.print("  private final ");
        out.print(providerDefinition(widget));
        out.println(";");
      });
    }
  }

  protected List<String> allWidgetNamesOfProviderWidgets(final List<BackofficeWidget> widgets) {
    final List<String> providerWidgets =
        widgets.stream().filter(widget -> !widget.isIgnore() && widget.isProviderNeeded())
            .map(BackofficeWidget::getWidgetName).distinct().toList();
    return providerWidgets;
  }

  private void writeProviderParameters(final PrintWriter out,
      final List<BackofficeWidget> widgets) {
    final List<String> providerWidgets = allWidgetNamesOfProviderWidgets(widgets);
    if (!providerWidgets.isEmpty()) {
      providerWidgets.forEach(widget -> {
        out.print("      final ");
        out.print(providerDefinition(widget));
        out.println(",");
      });
    }
  }

  private void writeProviderInitialization(final PrintWriter out,
      final List<BackofficeWidget> widgets) {
    final List<String> providerWidgets = allWidgetNamesOfProviderWidgets(widgets);
    if (!providerWidgets.isEmpty()) {
      providerWidgets.forEach(widget -> {
        out.print("    this.");
        out.print(providerFieldName(widget));
        out.print(" = ");
        out.print(providerFieldName(widget));
        out.println(";");
      });
    }
  }

  private void writeProviderMethods(final PrintWriter out, final List<BackofficeWidget> widgets) {
    final List<String> providerWidgets = allWidgetNamesOfProviderWidgets(widgets);
    if (!providerWidgets.isEmpty()) {
      providerWidgets.forEach(widget -> {
        out.println();
        out.println("  @Ignore");
        out.println("  @UiFactory");
        out.print("  public ");
        out.print(widget);
        out.print(" build");
        out.print(widget);
        out.println("() {");
        out.print("    return ");
        out.print(providerFieldName(widget));
        out.println(".get();");
        out.println("  }");
      });
    }
  }

  private String providerDefinition(final String widgetName) {
    return "Provider<" + widgetName + "> " + providerFieldName(widgetName);
  }

  private String providerFieldName(final String widgetName) {
    return StringUtils.uncapitalize(widgetName) + "Provider";
  }

  private void writeInitializationProvidedUiFields(final PrintWriter out,
      final List<BackofficeWidget> widgets) {
    widgets.stream().filter(widget -> widget.isProvided()).forEach(widget -> {
      out.print("    ");
      out.print(widget.getName());
      out.print(" = ");
      out.println(widget.getProvidedConstruction());
    });
  }

  private void writeUiFieldValueSetting(final PrintWriter out,
      final List<BackofficeWidget> widgets) {
    widgets.stream().filter(widget -> !widget.isIgnore()).forEach(widget -> widget.getParameters()
        .stream().filter(param -> !param.isUiBinder()).forEach(param -> {
          out.print("    ");
          out.print(widget.getName());
          out.print(".");
          out.print("set" + StringUtils.capitalize(param.getName()));
          out.print("(");
          out.print(param.getValue());
          out.println(");");
        }));
  }

  private void writeUiFieldDrivers(final PrintWriter out, final List<BackofficeWidget> widgets) {
    widgets.stream()
        .filter(widget -> !widget.isIgnore() && widget.getFieldType() == FieldTypeEnum.ONE_TO_MANY)
        .forEach(widget -> {
          out.print("    ");
          out.print(widget.getName());
          out.println(".setParentDriver(this.driver);");
        });
  }

  private void writeUiHandlers(final PrintWriter out, final List<BackofficeWidget> widgets) {
    widgets.stream()
        .filter(widget -> !widget.isIgnore() && widget.getFieldType() == FieldTypeEnum.ONE_TO_MANY)
        .forEach(widget -> {
          out.println();
          out.print("  @UiHandler(\"");
          out.print(fieldNameToCamelCase("add." + widget.getName()));
          out.println("\")");

          out.print("  public void ");
          out.print(fieldNameToCamelCase("add." + widget.getName()));
          out.println("(final ClickEvent event) {");

          out.print("    ");
          out.print(widget.getName());
          out.println(".addNewEntry();");

          out.println("  }");
        });
  }

  /**
   * write class of a embedded view.
   *
   * @param widget for which we have to generate the view
   * @param element the element which represents the entity
   * @param annotationInterface annotation interface
   * @param processingEnv processing environment
   */
  private void writeEmbeddedView(final BackofficeWidget widget, final Element element,
      final BackofficeClientGenerator annotationInterface,
      final ProcessingEnvironment processingEnv) {
    final String serverPackage = detectPackage(element, processingEnv);
    final String entityName = getEntityNameOfElement(widget.getField());
    final String entityType = widget.getWidgetName();

    try {
      final JavaFileObject builderFile =
          processingEnv.getFiler().createSourceFile(serverPackage + "." + entityType);
      try (PrintWriter out = new PrintWriter(builderFile.openWriter())) {
        writePackage(serverPackage, out);

        writeImports(embeddedImports(widget), out, serverPackage);

        out.println();

        out.print("public class ");
        out.print(entityType);
        out.print(" extends Composite implements HasEditorDelegate<");
        out.print(entityName);
        out.println("> {");
        out.println();

        writeUiBinderInterface(entityType, out);

        writeUiFieldDefinitions(out, widget.getChildWidgets(), element, annotationInterface,
            processingEnv);

        writeConstructor(widget, entityType, out);

        out.println();
        out.println("  @Override");
        out.print("  public void setDelegate(final EditorDelegate<");
        out.print(entityName);
        out.println("> delegate) {");
        out.println("    delegate.subscribe();");
        out.println("  }");
        out.println("}");
      }
    } catch (final FilerException e) {
      // happens when trying to recreate an existing interface
      processingEnv.getMessager().printMessage(Diagnostic.Kind.NOTE, e.getMessage());
    } catch (final IOException e) {
      processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR, e.getMessage());
      e.printStackTrace();
    }
  }

  private void writeUiBinderInterface(final String entityType, final PrintWriter out) {
    out.print("  interface Binder extends UiBinder<Widget, ");
    out.print(entityType);
    out.println("> {");
    out.println("  }");
    out.println();
  }

  private void writeConstructor(final BackofficeWidget widget, final String entityType,
      final PrintWriter out) {
    out.println();
    out.println("  @Inject");
    out.print("  public ");
    out.print(entityType);
    out.print("(final Binder uiBinder");
    if (widget.getChildWidgets().stream().anyMatch(childWidget -> childWidget.isProvided())) {
      out.print(", final ");
      out.print(TypeUtils.getClassNameWithoutPath(widget.getContaining()));
      out.print(BackofficeLocalInterfaceCreator.CLASS_SUFFIX);
      out.print(" messages");
    }
    out.println(") {");
    out.println("    super();");
    writeInitializationProvidedUiFields(out, widget.getChildWidgets());
    out.println("    initWidget(uiBinder.createAndBindUi(this));");
    writeUiFieldValueSetting(out, widget.getChildWidgets());
    out.println("  }");
  }

  private List<String> embeddedImports(final BackofficeWidget widget) {
    final List<String> imports = new ArrayList<>();

    imports.add(widget.getField().asType().toString());
    imports.add("com.google.gwt.uibinder.client.UiBinder");
    imports.add("com.google.gwt.uibinder.client.UiField");
    imports.add("com.google.gwt.user.client.ui.Composite");
    imports.add("com.google.gwt.user.client.ui.Widget");

    imports.add("org.gwtproject.editor.client.EditorDelegate");
    imports.add("org.gwtproject.editor.client.HasEditorDelegate");

    imports.add("jakarta.inject.Inject");

    widget.getImports().stream().forEach(importEntry -> imports.add(importEntry));
    widget.getChildWidgets().forEach(fieldWidget -> fieldWidget.getImports().stream()
        .forEach(importEntry -> imports.add(importEntry)));

    return imports;
  }

  /**
   * write class of a one to many entry view.
   *
   * @param widget for which we have to generate the view
   * @param element the element which represents the entity
   * @param annotationInterface annotation interface
   * @param processingEnv processing environment
   */
  private void writeOneToManyEntryView(final BackofficeWidget widget, final Element element,
      final BackofficeClientGenerator annotationInterface,
      final ProcessingEnvironment processingEnv) {
    final String serverPackage = detectPackage(element, processingEnv);
    final String entityType = widget.getWidgetName() + "Entry";
    final String entityName = getEntityNameOfFirstTypeElement(widget.getField());

    try {
      final JavaFileObject builderFile =
          processingEnv.getFiler().createSourceFile(serverPackage + "." + entityType);
      try (PrintWriter out = new PrintWriter(builderFile.openWriter())) {
        writePackage(serverPackage, out);

        writeImports(oneToManyEntryImports(widget), out, serverPackage);

        out.println();

        out.print("public class ");
        out.print(entityType);
        out.print(" extends AbstractListItemView<");
        out.print(entityName);
        out.println("> {");
        out.println();

        writeUiBinderInterface(entityType, out);

        writeUiFieldDefinitions(out, widget.getChildWidgets(), element, annotationInterface,
            processingEnv);

        writeConstructor(widget, entityType, out);

        out.println();
        out.println("  @UiHandler(\"deleteRow\")");
        out.println("  public void onDeleteRow(final ClickEvent event) {");
        out.println("    removeThisEntry();");
        out.println("  }");
        out.println("}");
      }
    } catch (final FilerException e) {
      // happens when trying to recreate an existing interface
      processingEnv.getMessager().printMessage(Diagnostic.Kind.NOTE, e.getMessage());
    } catch (final IOException e) {
      processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR, e.getMessage());
      e.printStackTrace();
    }
  }

  private List<String> oneToManyEntryImports(final BackofficeWidget widget) {
    final List<String> imports = new ArrayList<>();

    imports.add("de.knightsoftnet.mtwidgets.client.ui.widget.helper.AbstractListItemView");

    imports.add("com.google.gwt.event.dom.client.ClickEvent");
    imports.add("com.google.gwt.uibinder.client.UiBinder");
    imports.add("com.google.gwt.uibinder.client.UiField");
    imports.add("com.google.gwt.user.client.ui.Widget");

    imports.add("jakarta.inject.Inject");

    widget.getImports().stream().forEach(importEntry -> imports.add(importEntry));
    widget.getChildWidgets().stream().filter(childWidget -> !childWidget.isIgnore())
        .forEach(fieldWidget -> fieldWidget.getImports().stream()
            .forEach(importEntry -> imports.add(importEntry)));

    return imports;
  }

  /**
   * write class of a one to many list editor.
   *
   * @param widget for which we have to generate the view
   * @param element the element which represents the entity
   * @param processingEnv processing environment
   */
  private void writeOneToManyListEditor(final BackofficeWidget widget, final Element element,
      final ProcessingEnvironment processingEnv) {
    final String serverPackage = detectPackage(element, processingEnv);
    final String entityType = widget.getWidgetName();
    final String entityName = getEntityNameOfFirstTypeElement(widget.getField());

    try {
      final JavaFileObject builderFile =
          processingEnv.getFiler().createSourceFile(serverPackage + "." + entityType);
      try (PrintWriter out = new PrintWriter(builderFile.openWriter())) {
        writePackage(serverPackage, out);

        writeImports(oneToManyListEditorImports(widget), out, serverPackage);

        out.println();

        out.print("public class ");
        out.print(entityType);
        out.print(" extends AbstractListEditorWithProvider<");
        out.print(entityName);
        out.print(", ");
        out.print(entityType);
        out.println("Entry> {");
        out.println();

        out.println("  @IsDriver");
        out.print("  interface Driver extends SimpleBeanEditorDriver<List<");
        out.print(entityName);
        out.println(">,");
        out.print("      ListValidationEditor<");
        out.print(entityName);
        out.print(", ");
        out.print(entityType);
        out.println("Entry>> {");
        out.println("  }");
        out.println();

        out.print("  private final ListValidationEditor<");
        out.print(entityName);
        out.print(", ");
        out.print(entityType);
        out.println("Entry> editor;");
        out.println();

        out.println("  @Inject");
        out.print("  public ");
        out.print(entityType);
        out.print("(final Driver driver, final Provider<");
        out.print(entityName);
        out.println("> dataProvider,");
        out.print("      final Provider<");
        out.print(entityType);
        out.println("Entry> viewProvider) {");
        out.println("    super(dataProvider);");
        out.println("    editor = ListValidationEditor.of("
            + "new ItemEditorSourceWithProvider<>(this, viewProvider));");
        out.println("    driver.initialize(editor);");
        out.println("  }");

        out.println();
        out.println("  @Override");
        out.print("  public ListValidationEditor<");
        out.print(entityName);
        out.print(", ");
        out.print(entityType);
        out.println("Entry> asEditor() {");
        out.println("    return editor;");
        out.println("  }");

        out.println("}");
      }
    } catch (final FilerException e) {
      // happens when trying to recreate an existing interface
      processingEnv.getMessager().printMessage(Diagnostic.Kind.NOTE, e.getMessage());
    } catch (final IOException e) {
      processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR, e.getMessage());
      e.printStackTrace();
    }
  }

  private List<String> oneToManyListEditorImports(final BackofficeWidget widget) {
    final List<String> imports = new ArrayList<>();

    imports
        .add("de.knightsoftnet.mtwidgets.client.ui.widget.helper.AbstractListEditorWithProvider");
    imports.add("de.knightsoftnet.mtwidgets.client.ui.widget.helper.ItemEditorSourceWithProvider");
    imports.add("de.knightsoftnet.validators.client.editor.impl.ListValidationEditor");

    imports.add("org.gwtproject.editor.client.SimpleBeanEditorDriver");
    imports.add("org.gwtproject.editor.client.annotation.IsDriver");

    imports.add("java.util.List");

    imports.add("jakarta.inject.Inject");
    imports.add("jakarta.inject.Provider");

    widget.getImports().stream().forEach(importEntry -> imports.add(importEntry));

    return imports;
  }
}

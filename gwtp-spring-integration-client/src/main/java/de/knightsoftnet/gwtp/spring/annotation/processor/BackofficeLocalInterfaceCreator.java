package de.knightsoftnet.gwtp.spring.annotation.processor;

import de.knightsoftnet.gwtp.spring.client.annotation.BackofficeClientGenerator;
import de.knightsoftnet.validators.annotation.processor.TypeUtils;
import de.knightsoftnet.validators.shared.data.FieldTypeEnum;

import java.io.PrintWriter;
import java.util.List;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;

/**
 * Create JPA repository interface.
 */
public class BackofficeLocalInterfaceCreator
    extends AbstractBackofficeCreator<BackofficeClientGenerator> {

  protected static final String CLASS_SUFFIX = "Messages";

  public BackofficeLocalInterfaceCreator() {
    super(CLASS_SUFFIX);
  }

  @Override
  protected void addAdditionalImports(final String serverPackage, final Element element,
      final BackofficeClientGenerator annotationInterface, final List<BackofficeWidget> widgets,
      final ProcessingEnvironment processingEnv) {
    addImports(//
        "com.google.gwt.i18n.client.Messages");
  }

  @Override
  protected void writeBody(final PrintWriter out, final String serverPackage, final Element element,
      final BackofficeClientGenerator annotationInterface, final List<BackofficeWidget> widgets,
      final ProcessingEnvironment processingEnv) {
    final String entityName = getEntityNameOfElement(element);

    out.print("public interface ");
    out.print(entityName);
    out.print(suffix);
    out.println(" extends Messages {");
    out.println("  String header();");
    detectBackofficeWidgetsOfElementFlat(widgets).stream()
        .forEach(widget -> singleProperty(out, widget, processingEnv));

    out.println("}");
  }

  private void singleProperty(final PrintWriter out, final BackofficeWidget widget,
      final ProcessingEnvironment processingEnv) {
    writeKeyAnnotation(out, widget.getName());
    out.print("  String ");
    out.print(fieldNameToCamelCase(widget.getName()));
    out.println("();");
    if (widget.getFieldType() == FieldTypeEnum.ENUM_FIXED
        && !"de.knightsoftnet.validators.shared.data.CountryEnum".equals(TypeUtils.getClassName(
            processingEnv.getTypeUtils().asMemberOf(widget.getContaining(), widget.getField())))) {
      writeKeyAnnotation(out, widget.getName());
      out.print("  String ");
      out.print(fieldNameToCamelCase(widget.getName()));
      out.print("(@Select ");
      out.print(widget.getField().asType().toString());
      out.print(" ");
      out.print(widget.getField().getSimpleName());
      out.println(");");
    }
  }

  private void writeKeyAnnotation(final PrintWriter out, final String fieldname) {
    if (fieldNameIsConcatenated(fieldname)) {
      out.print("  @Key(\"");
      out.print(fieldname);
      out.println("\")");
    }
  }
}

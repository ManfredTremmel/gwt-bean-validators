package de.knightsoftnet.gwtp.spring.annotation.processor;

import de.knightsoftnet.gwtp.spring.client.annotation.BackofficeClientGenerator;
import de.knightsoftnet.validators.shared.data.FieldTypeEnum;

import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.annotation.processing.FilerException;
import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;
import javax.tools.Diagnostic;
import javax.tools.FileObject;
import javax.tools.StandardLocation;

/**
 * Create localization property file.
 */
public class BackofficeUiBinderCreator
    extends AbstractBackofficeCreator<BackofficeClientGenerator> {

  protected static final String CLASS_EXTENSION = ".ui.xml";
  protected static final List<List<Integer>> GRID_COLUMNS = List.of(

      List.of(11, 1), //
      List.of(6, 5, 1), //
      List.of(4, 4, 3, 1), //
      List.of(3, 3, 3, 2, 1), //
      List.of(4, 4, 4, 4, 7, 1), //
      List.of(4, 4, 4, 4, 4, 3, 1), //
      List.of(3, 3, 3, 3, 3, 3, 5, 1), //
      List.of(3, 3, 3, 3, 3, 3, 3, 2, 1), //
      List.of(3, 3, 3, 3, 3, 3, 3, 3, 11, 1), //
      List.of(3, 3, 3, 3, 3, 3, 3, 3, 3, 8, 1), //
      List.of(3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 5, 1), //
      List.of(3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 2, 1)

  );

  public BackofficeUiBinderCreator() {
    super(CLASS_SUFFIX_VIEW + CLASS_EXTENSION);
  }

  /**
   * write ui binder file.
   */
  public void writeUiBinder(final Element element, final BackofficeClientGenerator generator,
      final List<BackofficeWidget> widgets, final ProcessingEnvironment processingEnv) {
    final String serverPackage = detectPackage(element, processingEnv);
    final String resourceName = getEntityNameOfElement(element) + suffix;

    try {
      final FileObject builderFile = processingEnv.getFiler()
          .createResource(StandardLocation.SOURCE_OUTPUT, serverPackage, resourceName);
      try (PrintWriter out = new PrintWriter(builderFile.openWriter())) {
        writeUiBinder(out, element, generator, serverPackage, widgets, processingEnv);
      }
    } catch (final FilerException e) {
      // happens when trying to recreate an existing interface
      processingEnv.getMessager().printMessage(Diagnostic.Kind.NOTE, e.getMessage());
    } catch (final IOException e) {
      e.printStackTrace();
    }
  }


  private void writeUiBinder(final PrintWriter out, final Element element,
      final BackofficeClientGenerator generator, final String serverPackage,
      final List<BackofficeWidget> widgets, final ProcessingEnvironment processingEnv) {
    final String entityName = getEntityNameOfElement(element);

    writeXmlHeader(out, generator, serverPackage, entityName, widgets);
    writeXmlTokenImports(out, serverPackage, entityName);

    out.print("  <g:HTMLPanel styleName=\"");
    out.print(generator.uiBinderStyleCentralPanel());
    out.println("\">");

    writeXmlAdminNavigation(out, generator);
    writeXmlFieldWidgets(out, generator, widgets, serverPackage, entityName, StringUtils.EMPTY,
        true, processingEnv);

    out.println("  </g:HTMLPanel>");
    out.println("</ui:UiBinder>");
  }

  private void writeEmbeddedUiBinder(final BackofficeWidget widget,
      final BackofficeClientGenerator generator, final String serverPackage,
      final String entityName, final String prefix, final ProcessingEnvironment processingEnv,
      final List<Integer> gridColumns) {
    final List<BackofficeWidget> widgets = widget.getChildWidgets();

    try {
      final FileObject builderFile = processingEnv.getFiler().createResource(
          StandardLocation.SOURCE_OUTPUT, serverPackage, widget.getWidgetName() + CLASS_EXTENSION);
      try (PrintWriter out = new PrintWriter(builderFile.openWriter())) {

        writeXmlHeader(out, generator, serverPackage, entityName, widgets);

        out.println("  <g:HTMLPanel>");

        if (gridColumns == null) {
          writeXmlFieldWidgets(out, generator, widgets, serverPackage, entityName, prefix, false,
              processingEnv);
        } else {
          int pos = 0;
          for (final BackofficeWidget displayWidget : widgets) {
            if (!widget.isIgnore() && !"id".equals(widget.getName())) {
              writeWidget(out, displayWidget, generator, serverPackage, entityName, prefix,
                  processingEnv, List.of(gridColumns.get(pos++)));
            }
          }
        }

        out.println("  </g:HTMLPanel>");
        out.println("</ui:UiBinder>");
      }
    } catch (final FilerException e) {
      // happens when trying to recreate an existing interface
      processingEnv.getMessager().printMessage(Diagnostic.Kind.NOTE, e.getMessage());
    } catch (final IOException e) {
      e.printStackTrace();
    }
  }

  private void writeXmlFieldWidgets(final PrintWriter out,
      final BackofficeClientGenerator generator, final List<BackofficeWidget> widgets,
      final String serverPackage, final String entityName, final String prefix,
      final boolean autofocusHandling, final ProcessingEnvironment processingEnv) {
    boolean divOpen = false;
    boolean autofocusSet = !autofocusHandling;
    boolean idExists = false;
    int widgetsInLine = 2;
    for (final BackofficeWidget widget : widgets) {
      if (!widget.isIgnore()) {
        if (widgetsInLine >= 2 || widget.getFieldType() == FieldTypeEnum.EMBEDDED
            || widget.getFieldType() == FieldTypeEnum.ONE_TO_MANY) {
          if (divOpen) {
            out.println("    </div>");
          }
          if (widget.getFieldType() == FieldTypeEnum.EMBEDDED
              || widget.getFieldType() == FieldTypeEnum.ONE_TO_MANY) {
            divOpen = false;
            widgetsInLine = 2;
          } else {
            out.print("    <div class=\"");
            out.print(generator.uiBinderStyleRow());
            out.println("\">");
            divOpen = true;
            widgetsInLine = 0;
          }
        }

        if ("id".equals(widget.getName())) {
          idExists = true;
        } else if (!autofocusSet && idExists) {
          widget.getParameters().add(BackofficeWidgetParameter.of("autofocus", "true", true));
          autofocusSet = true;
        }
        writeWidgetWithLabel(out, widget, generator, serverPackage, entityName, prefix,
            processingEnv);
        widgetsInLine++;
      }
    }
    if (divOpen) {
      out.println("    </div>");
    }
  }

  private void writeXmlAdminNavigation(final PrintWriter out,
      final BackofficeClientGenerator generator) {
    out.print("    <div class=\"");
    out.print(generator.uiBinderStyleRow());
    out.println("\">");

    out.print("      <div class=\"");
    out.print(generator.uiBinderStyleHeader());
    out.println("\">");
    out.println("        <h1>");
    out.println("          <ui:text from=\"{messages.header}\" />");
    out.println("        </h1>");
    out.println("      </div>");
    out.println("    </div>");

    out.print("    <div class=\"");
    out.print(generator.uiBinderStyleRow());
    out.println("\">");

    out.println("      <w:AdminNavigationWidget ui:field=\"adminNavigation\"");
    out.println("        link=\"{TOKEN}\"");
    out.println("        linkWithParameter=\"{TOKEN_WITH_ID}\"");
    out.println("        allowNew=\"true\" />");
    out.println("    </div>");
  }

  private void writeXmlTokenImports(final PrintWriter out, final String serverPackage,
      final String entityName) {
    out.print("  <ui:import field=\"");
    out.print(serverPackage + "." + entityName + CLASS_SUFFIX_VIEW);
    out.println(".TOKEN\"/>");
    out.print("  <ui:import field=\"");
    out.print(serverPackage + "." + entityName + CLASS_SUFFIX_VIEW);
    out.println(".TOKEN_WITH_ID\"/>");
    out.println("");
  }

  private void writeXmlHeader(final PrintWriter out, final BackofficeClientGenerator generator,
      final String serverPackage, final String entityName, final List<BackofficeWidget> widgets) {
    out.println("<!DOCTYPE ui:UiBinder SYSTEM \"http://dl.google.com/gwt/DTD/xhtml.ent\">");
    out.println("<ui:UiBinder xmlns:ui=\"urn:ui:com.google.gwt.uibinder\"");
    if (widgets.stream().anyMatch(widget -> widget.getFieldType() == FieldTypeEnum.EMBEDDED
        || widget.getFieldType() == FieldTypeEnum.ONE_TO_MANY)) {
      out.print("  xmlns:e=\"urn:import:");
      out.print(serverPackage);
      out.println("\"");
    }
    out.println("  xmlns:g=\"urn:import:com.google.gwt.user.client.ui\"");
    out.println("  xmlns:w=\"urn:import:de.knightsoftnet.mtwidgets.client.ui.widget\">");
    out.println("");
    out.print("  <ui:with field=\"messages\" type=\"");
    out.print(serverPackage + "." + entityName + BackofficeLocalInterfaceCreator.CLASS_SUFFIX);
    out.println("\" />");

    for (final String header : generator.uiBinderHeader()) {
      out.print("  ");
      out.println(header);
    }
    out.println("");
  }

  private void writeWidgetWithLabel(final PrintWriter out, final BackofficeWidget widget,
      final BackofficeClientGenerator generator, final String serverPackage,
      final String entityName, final String prefix, final ProcessingEnvironment processingEnv) {
    if (widget.getFieldType() != FieldTypeEnum.EMBEDDED
        && widget.getFieldType() != FieldTypeEnum.ONE_TO_MANY) {
      out.print("      <div class=\"");
      out.print(generator.uiBinderStyleLabel());
      out.println("\">");
      out.print("        <label for=\"");
      out.print(widget.getName());
      out.print("\"><ui:text from=\"{messages.");
      out.print(fieldNameToCamelCase(prefix + widget.getName()));
      out.println("}\" /></label>");
      out.println("      </div>");

      out.print("      <div class=\"");
      out.print(generator.uiBinderStyleWidget());
      out.println("\">");
    }
    writeWidget(out, widget, generator, serverPackage, entityName, prefix, processingEnv, null);
    if (widget.getFieldType() != FieldTypeEnum.EMBEDDED
        && widget.getFieldType() != FieldTypeEnum.ONE_TO_MANY) {
      out.println("      </div>");
    }
  }

  private void writeWidget(final PrintWriter out, final BackofficeWidget widget,
      final BackofficeClientGenerator generator, final String serverPackage,
      final String entityName, final String prefix, final ProcessingEnvironment processingEnv,
      final List<Integer> gridColumns) {
    switch (widget.getFieldType()) {
      case EMBEDDED:
        writeEmbededWidget(out, widget, generator, serverPackage, entityName, prefix, processingEnv,
            gridColumns);
        break;
      case ONE_TO_MANY:
        writeOneToManyWidget(out, widget, generator, serverPackage, entityName, prefix,
            processingEnv);
        break;
      default:
        if (gridColumns != null) {
          writeColumnDiv(out, gridColumns.get(0), generator);
        }
        if (widget.getFieldType() == FieldTypeEnum.MANY_TO_ONE) {
          out.print("        <e:");
        } else {
          out.print("        <w:");
        }
        if (StringUtils.contains(widget.getWidgetName(), "<")) {
          out.print(StringUtils.split(widget.getWidgetName(), '<')[0]);
        } else {
          out.print(widget.getWidgetName());
        }
        if (widget.isWriteDebugId()) {
          out.print(" debugId=\"");
          out.print(widget.getName());
          out.print("\"");
        }
        out.print(" ui:field=\"");
        out.print(widget.getName());
        out.print("\"");

        widget.getParameters().stream().filter(param -> param.isUiBinder()).forEach(param -> {
          out.print(" ");
          out.print(param.getName());
          out.print("=\"");
          out.print(param.getValue());
          out.print("\"");
        });

        out.print(" validationMessageElement=\"{");
        out.print(widget.getName());
        out.println("Validation}\" />");

        out.print("        <g:HTMLPanel tag=\"span\" ui:field=\"");
        out.print(widget.getName());
        out.print("Validation\" styleName=\"");
        out.print(generator.uiBinderErrorStyle());
        out.println("\" />");

        if (gridColumns != null) {
          out.println("      </div>");
        }
        break;
    }
  }

  private void writeEmbededWidget(final PrintWriter out, final BackofficeWidget widget,
      final BackofficeClientGenerator generator, final String serverPackage,
      final String entityName, final String prefix, final ProcessingEnvironment processingEnv,
      final List<Integer> gridColumns) {
    out.print("    <e:");
    out.print(widget.getWidgetName());
    out.print(" ui:field=\"");
    out.print(widget.getName());
    out.println("\" />");
    final String newPrefix = prefix + widget.getName() + ".";
    writeEmbeddedUiBinder(widget, generator, serverPackage, entityName, newPrefix, processingEnv,
        gridColumns);
  }

  private void writeOneToManyWidget(final PrintWriter out, final BackofficeWidget widget,
      final BackofficeClientGenerator generator, final String serverPackage,
      final String entityName, final String prefix, final ProcessingEnvironment processingEnv) {
    final String newPrefix = prefix + widget.getName() + ".";

    out.print("    <div class=\"");
    out.print(generator.uiBinderStyleRow());
    out.println("\">");

    final List<BackofficeWidget> displayWidgets = extractBackofficeWidgetsFlat(widget);
    int gridPos = 0;
    for (final BackofficeWidget displayWidget : displayWidgets) {
      writeColumnDiv(out, GRID_COLUMNS.get(displayWidgets.size() - 1).get(gridPos++), generator);

      out.print("        <ui:text from=\"{messages.");
      out.print(fieldNameToCamelCase(newPrefix + displayWidget.getName()));
      out.println("}\" />");

      out.println("      </div>");
    }
    writeColumnDiv(out, GRID_COLUMNS.get(displayWidgets.size() - 1).get(gridPos), generator);
    out.println("        &nbsp;");
    out.println("      </div>");

    out.println("    </div>");

    out.print("    <e:");
    out.print(widget.getWidgetName());
    out.print(" ui:field=\"");
    out.print(prefix + widget.getName());
    out.print("\" validationMessageElement=\"{");
    out.print(prefix + widget.getName());
    out.println("Validation}\" />");

    out.print("    <div class=\"");
    out.print(generator.uiBinderStyleRow());
    out.println("\">");
    out.print("      <div class=\"");
    out.print(generator.uiBinderGridClassesColumn()[11]);
    out.println("\">");
    out.print("        <g:HTMLPanel tag=\"span\" ui:field=\"");
    out.print(prefix + widget.getName());
    out.print("Validation\" styleName=\"");
    out.print(generator.uiBinderErrorStyle());
    out.println("\" />");
    out.println("      </div>");
    out.println("    </div>");

    out.print("    <div class=\"");
    out.print(generator.uiBinderStyleRow());
    out.println("\">");
    out.print("      <div class=\"");
    out.print(generator.uiBinderGridClassesColumn()[11]);
    out.println("\">");
    out.print("        <g:Button ui:field=\"");
    out.print(fieldNameToCamelCase("add." + prefix + widget.getName()));
    out.println("\">");
    out.print("          ➕ <ui:text from=\"{messages.");
    out.print(fieldNameToCamelCase(prefix + widget.getName()));
    out.println("}\" />");
    out.println("        </g:Button>");
    out.println("      </div>");
    out.println("    </div>");

    writeOneToManyUiBinder(widget, generator, serverPackage, entityName, newPrefix, processingEnv);
  }

  private void writeColumnDiv(final PrintWriter out, final int gridWidth,
      final BackofficeClientGenerator generator) {
    out.print("      <div class=\"");
    out.print(generator.uiBinderGridClassesColumn()[gridWidth - 1]);
    out.println("\">");
  }

  private List<BackofficeWidget> extractBackofficeWidgetsFlat(final BackofficeWidget widget) {
    return widget.getChildWidgets().stream().flatMap(BackofficeWidget::streamFlatBackofficeWidget)
        .filter(refwidget -> !(refwidget.isIgnore() || "id".equals(refwidget.getName())
            || refwidget.getFieldType() == FieldTypeEnum.EMBEDDED))
        .toList();
  }

  private List<BackofficeWidget> extractBackofficeWidgets(final BackofficeWidget widget) {
    return widget.getChildWidgets().stream()
        .filter(refwidget -> !(refwidget.isIgnore() || "id".equals(refwidget.getName()))).toList();
  }

  @SuppressWarnings("unused")
  private void writeOneToManyUiBinder(final BackofficeWidget widget,
      final BackofficeClientGenerator generator, final String serverPackage,
      final String entityName, final String prefix, final ProcessingEnvironment processingEnv) {
    try {
      final FileObject builderFile =
          processingEnv.getFiler().createResource(StandardLocation.SOURCE_OUTPUT, serverPackage,
              widget.getWidgetName() + "Entry" + CLASS_EXTENSION);
      try (PrintWriter out = new PrintWriter(builderFile.openWriter())) {

        writeXmlHeader(out, generator, serverPackage, entityName, widget.getChildWidgets());

        out.print("  <g:HTMLPanel styleName=\"");
        out.print(generator.uiBinderStyleRow());
        out.println("\">");

        final List<BackofficeWidget> displayWidgetsFlat = extractBackofficeWidgetsFlat(widget);
        final List<BackofficeWidget> displayWidgets = extractBackofficeWidgets(widget);
        int gridPos = 0;
        for (final BackofficeWidget displayWidget : displayWidgets) {
          final List<Integer> gridColumns = new ArrayList<>();
          if (displayWidget.getFieldType() == FieldTypeEnum.EMBEDDED) {
            for (final BackofficeWidget backofficeWidget : displayWidgetsFlat.stream()
                .filter(flatWidget -> !flatWidget.isIgnore()
                    && flatWidget.getName().startsWith(displayWidget.getName() + "."))
                .toList()) {
              gridColumns.add(GRID_COLUMNS.get(displayWidgetsFlat.size() - 1).get(gridPos++));
            }
          } else {
            gridColumns.add(GRID_COLUMNS.get(displayWidgetsFlat.size() - 1).get(gridPos++));
          }
          writeWidget(out, displayWidget, generator, serverPackage, entityName, prefix,
              processingEnv, gridColumns);
        }

        writeColumnDiv(out, GRID_COLUMNS.get(displayWidgetsFlat.size() - 1).get(gridPos++),
            generator);
        final Optional<BackofficeWidget> idWidget = widget.getChildWidgets().stream()
            .filter(refwidget -> "id".equals(refwidget.getName())).findFirst();
        if (idWidget.isPresent()) {
          out.print("        <w:");
          out.print(idWidget.get().getWidgetName());
          if (idWidget.get().isWriteDebugId()) {
            out.print(" debugId=\"");
            out.print(idWidget.get().getName());
            out.print("\"");
          }
          out.print(" ui:field=\"");
          out.print(idWidget.get().getName());
          out.print("\"");

          idWidget.get().getParameters().stream().filter(param -> param.isUiBinder())
              .forEach(param -> {
                out.print(" ");
                out.print(param.getName());
                out.print("=\"");
                out.print(param.getValue());
                out.print("\"");
              });
          out.println(" visible=\"false\"/>");
        }
        out.println("        <g:Button ui:field=\"deleteRow\">␡</g:Button>");
        out.println("      </div>");

        out.println("  </g:HTMLPanel>");
        out.println("</ui:UiBinder>");
      }
    } catch (final FilerException e) {
      // happens when trying to recreate an existing interface
      processingEnv.getMessager().printMessage(Diagnostic.Kind.NOTE, e.getMessage());
    } catch (final IOException e) {
      e.printStackTrace();
    }
  }

  @Override
  protected void addAdditionalImports(final String serverPackage, final Element element,
      final BackofficeClientGenerator annotationInterface, final List<BackofficeWidget> widgets,
      final ProcessingEnvironment processingEnv) {
    // ignore
  }

  @Override
  protected void writeBody(final PrintWriter out, final String serverPackage, final Element element,
      final BackofficeClientGenerator annotationInterface, final List<BackofficeWidget> widgets,
      final ProcessingEnvironment processingEnv) {
    // ignore
  }
}

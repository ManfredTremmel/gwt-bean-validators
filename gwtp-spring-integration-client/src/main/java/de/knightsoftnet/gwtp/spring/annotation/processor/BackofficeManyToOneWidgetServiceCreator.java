package de.knightsoftnet.gwtp.spring.annotation.processor;

import de.knightsoftnet.gwtp.spring.client.annotation.BackofficeClientGenerator;

import org.apache.commons.lang3.StringUtils;

import java.io.PrintWriter;
import java.util.List;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;

/**
 * Create rest service interface for many to one reference widgets.
 */
public class BackofficeManyToOneWidgetServiceCreator
    extends AbstractBackofficeManyToOneWidgetCreator<BackofficeClientGenerator> {

  protected static final String CLASS_SUFFIX = "RestService";

  public BackofficeManyToOneWidgetServiceCreator() {
    super(CLASS_SUFFIX);
  }

  @Override
  protected void addAdditionalImports(final String serverPackage, final Element element,
      final BackofficeClientGenerator annotationInterface, final List<BackofficeWidget> widgets,
      final ProcessingEnvironment processingEnv) {
    addImport(element.asType());
    addImports("de.knightsoftnet.mtwidgets.client.services.ManyToOneSuggestionRestService", //
        "java.util.List", //
        "javax.ws.rs.GET", //
        "javax.ws.rs.Path", //
        "javax.ws.rs.PathParam");
  }

  @Override
  protected void writeBody(final Element element,
      final BackofficeClientGenerator annotationInterface, final List<BackofficeWidget> widgets,
      final String keyfield, final String valuefield, final String entityType,
      final String entityName, final PrintWriter out) {
    out.println();

    out.print("@Path(\"");
    out.print(StringUtils.replace(annotationInterface.value(),
        StringUtils.lowerCase(getEntityNameOfElement(element)), StringUtils.lowerCase(entityType)));
    out.println("\")");

    out.print("public interface ");
    out.print(entityType);
    out.println(suffix);
    out.print("    extends ManyToOneSuggestionRestService<");
    out.print(entityName);
    out.println("> {");
    out.println();

    out.println("  @Override");
    out.println("  @GET");
    out.print("  @Path(\"/{");
    out.print(keyfield);
    out.println("}\")");
    out.print("  ");
    out.print(entityName);
    out.print(" getReferenceByKey(@PathParam(\"");
    out.print(keyfield);
    out.println("\") final String key);");
    out.println();

    out.println("  @Override");
    out.println("  @GET");
    out.print("  @Path(\"/suggestions/{");
    out.print(keyfield);
    out.println("}\")");
    out.print("  List<");
    out.print(entityName);
    out.print("> findByKeyContaining(@PathParam(\"");
    out.print(keyfield);
    out.println("\") final String key);");

    out.println("}");
  }
}

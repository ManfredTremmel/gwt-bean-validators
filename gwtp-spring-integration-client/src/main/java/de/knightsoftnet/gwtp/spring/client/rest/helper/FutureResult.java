/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.gwtp.spring.client.rest.helper;

import static de.knightsoftnet.gwtp.spring.client.rest.helper.FutureResult.State.FAILED;
import static de.knightsoftnet.gwtp.spring.client.rest.helper.FutureResult.State.INCOMPLETE;
import static de.knightsoftnet.gwtp.spring.client.rest.helper.FutureResult.State.SUCCEEDED;

import com.google.gwt.user.client.rpc.AsyncCallback;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * future result for asynchronous call caching. Based on RPC Future Result from
 * http://stackoverflow.com/questions/34851180/how-to-cache-server-results-in-gwt-with-guava
 *
 * @author Manfred Tremmel
 *
 * @param <T> type of the result
 */
public class FutureResult<T> implements AsyncCallback<T> {
  protected enum State {
    SUCCEEDED, FAILED, INCOMPLETE;
  }

  private State state = INCOMPLETE;
  private final Set<AsyncCallback<T>> listeners = new LinkedHashSet<>();
  private T value;
  private Throwable error;

  /**
   * get result of the call.
   *
   * @return result of call
   * @throws IllegalStateException if call isn't done.
   */
  public T get() throws IllegalStateException {
    switch (state) {
      case INCOMPLETE:
        // Do not block browser so just throw ex
        throw new IllegalStateException("The server response did not yet recieved.");
      case FAILED:
        throw new IllegalStateException(error);
      case SUCCEEDED:
        return value;
      default:
        throw new IllegalStateException("Something very unclear");
    }
  }

  /**
   * add callback which is informed about changes.
   *
   * @param callback callback which is informed about changes
   */
  public void addCallback(final AsyncCallback<T> callback) {
    if (callback != null) {
      listeners.add(callback);
    }
  }

  public boolean isDone() {
    return state == SUCCEEDED;
  }

  @Override
  public void onFailure(final Throwable exception) {
    state = FAILED;
    error = exception;
    for (final AsyncCallback<T> callback : listeners) {
      callback.onFailure(exception);
    }
  }

  @Override
  public void onSuccess(final T response) {
    value = response;
    state = SUCCEEDED;
    for (final AsyncCallback<T> callback : listeners) {
      callback.onSuccess(response);
    }
  }
}

package de.knightsoftnet.gwtp.spring.client.resources;

import com.google.gwt.debug.client.DebugInfo;

import org.apache.commons.lang3.StringUtils;

import jakarta.inject.Inject;

public class ResourceLoader {
  @Inject
  ResourceLoader(final ResourcesFile appResources) {
    appResources.grid().ensureInjected();
    DebugInfo.setDebugIdPrefix(StringUtils.EMPTY);
  }
}

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.gwtp.spring.client.rest.helper;

import de.knightsoftnet.gwtp.spring.client.session.Session;
import de.knightsoftnet.gwtp.spring.shared.models.User;

import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * Async callback implementation with error handling.
 *
 * @author Manfred Tremmel
 *
 */
public class RestCallbackBuilder {

  /**
   * create rest callback implementation.
   *
   * @param callbackOnSuccess on success callback
   * @param callbackOnFailure on failure callback
   * @param <R> rest result type
   * @return RestCallbackImpl
   */
  public static <R> AsyncCallback<R> build(final AsyncCallbackOnSuccess<R> callbackOnSuccess,
      final AsyncCallbackOnFailure callbackOnFailure) {
    return new AsyncCallback<>() {

      @Override
      public void onFailure(final Throwable caught) {
        callbackOnFailure.onFailure(caught);
      }

      @Override
      public void onSuccess(final R result) {
        callbackOnSuccess.onSuccess(result);
      }
    };
  }

  /**
   * create rest callback implementation.
   *
   * @param view view of the
   * @param session session
   * @param callbackOnSuccess on success callback
   * @param <V> view or widget which implements EditorWithErrorHandling interface
   * @param <R> rest result type
   * @param <H> http messages
   * @return RestCallbackImpl
   */
  public static <V extends HasShowMessage, R, H extends HttpMessages> RestCallbackSimpleImpl<V, //
      R, H> build(final V view, final Session session,
          final AsyncCallbackOnSuccess<R> callbackOnSuccess) {
    return new RestCallbackSimpleImpl<>(view, session, callbackOnSuccess);
  }

  /**
   * create rest callback implementation.
   *
   * @param view view of the
   * @param session session
   * @param callbackOnSuccess on success callback
   * @param httpMessage http code messages to show
   * @param <V> view or widget which implements EditorWithErrorHandling interface
   * @param <R> rest result type
   * @param <H> http messages
   * @return RestCallbackImpl
   */
  public static <V extends HasShowMessage, R, H extends HttpMessages> RestCallbackSimpleImpl<V, //
      R, H> build(final V view, final Session session,
          final AsyncCallbackOnSuccess<R> callbackOnSuccess, final H httpMessage) {
    return new RestCallbackSimpleImpl<>(view, session, httpMessage, callbackOnSuccess);
  }

  /**
   * create rest callback implementation.
   *
   * @param view view of the
   * @param data data given from server
   * @param session session
   * @param callbackOnSuccess on success callback
   * @param <P> presenter type
   * @param <D> data type given to the server
   * @param <V> view or widget which implements EditorWithErrorHandling interface
   * @param <R> rest result type
   * @param <H> http messages
   * @return RestCallbackImpl
   */
  public static <P, D, V extends EditorWithErrorHandling<P, D>, R, //
      H extends HttpMessages> RestCallbackImpl<P, D, V, R, H> build(final V view, final D data,
          final Session session, final AsyncCallbackOnSuccess<R> callbackOnSuccess) {
    return new RestCallbackImpl<>(view, data, session, callbackOnSuccess);
  }

  /**
   * create rest callback implementation.
   *
   * @param view view of the
   * @param data data given from server
   * @param session session
   * @param httpMessage http code messages to show
   * @param callbackOnSuccess on success callback
   * @param <P> presenter type
   * @param <D> data type given to the server
   * @param <V> view or widget which implements EditorWithErrorHandling interface
   * @param <R> rest result type
   * @param <H> http messages
   * @return RestCallbackImpl
   */
  public static <P, D, V extends EditorWithErrorHandling<P, D>, R, //
      H extends HttpMessages> RestCallbackImpl<P, D, V, R, H> build(final V view, final D data,
          final Session session, final H httpMessage,
          final AsyncCallbackOnSuccess<R> callbackOnSuccess) {
    return new RestCallbackImpl<>(view, data, session, httpMessage, callbackOnSuccess);
  }

  /**
   * create login callback implementation.
   *
   * @param view view of the
   * @param session session
   * @param <T> user data
   * @param <V> view or widget which implements EditorWithErrorHandling interface
   * @param <M> login messages
   * @param <H> http messages
   * @return LoginCallback
   */
  public static <T extends User, V extends EditorWithErrorHandling<?, ?>, //
      M extends LoginMessages, H extends HttpMessages> LoginCallback<T, V, M, H> buildLoginCallback(
          final V view, final Session session) {
    return new LoginCallback<>(view, session);
  }

  /**
   * create login callback implementation.
   *
   * @param view view of the
   * @param session session
   * @param loginErrorMessage error message to show
   * @param <T> user data
   * @param <V> view or widget which implements EditorWithErrorHandling interface
   * @param <M> login messages
   * @param <H> http messages
   * @return LoginCallback
   */
  public static <T extends User, V extends EditorWithErrorHandling<?, ?>, //
      M extends LoginMessages, H extends HttpMessages> LoginCallback<T, V, M, H> buildLoginCallback(
          final V view, final Session session, final M loginErrorMessage) {
    return new LoginCallback<>(view, session, loginErrorMessage);
  }

  /**
   * create login callback implementation.
   *
   * @param view view of the
   * @param session session
   * @param loginErrorMessage error message to show
   * @param httpMessage http code messages to show
   * @param <T> user data
   * @param <V> view or widget which implements EditorWithErrorHandling interface
   * @param <M> login messages
   * @param <H> http messages
   * @return LoginCallback
   */
  public static <T extends User, V extends EditorWithErrorHandling<?, ?>, //
      M extends LoginMessages, H extends HttpMessages> LoginCallback<T, V, M, H> buildLoginCallback(
          final V view, final Session session, final M loginErrorMessage, final H httpMessage) {
    return new LoginCallback<>(view, session, loginErrorMessage, httpMessage);
  }
}

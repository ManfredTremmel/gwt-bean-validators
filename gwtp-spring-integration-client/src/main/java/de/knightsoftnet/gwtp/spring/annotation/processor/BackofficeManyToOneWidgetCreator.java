package de.knightsoftnet.gwtp.spring.annotation.processor;

import de.knightsoftnet.gwtp.spring.client.annotation.BackofficeClientGenerator;

import java.io.PrintWriter;
import java.util.List;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;

/**
 * Create suggestion for many to one reference widgets.
 */
public class BackofficeManyToOneWidgetCreator
    extends AbstractBackofficeManyToOneWidgetCreator<BackofficeClientGenerator> {

  protected static final String CLASS_SUFFIX = "";

  public BackofficeManyToOneWidgetCreator() {
    super(CLASS_SUFFIX);
  }

  @Override
  protected void addAdditionalImports(final String serverPackage, final Element element,
      final BackofficeClientGenerator annotationInterface, final List<BackofficeWidget> widgets,
      final ProcessingEnvironment processingEnv) {
    addImport(element.asType());
    addImports("de.knightsoftnet.mtwidgets.client.ui.widget.AbstractManyToOneEntityWidget", //
        "com.gwtplatform.dispatch.rest.delegates.client.ResourceDelegate", //
        "jakarta.inject.Inject");
  }

  @Override
  protected void writeBody(final Element element,
      final BackofficeClientGenerator annotationInterface, final List<BackofficeWidget> widgets,
      final String keyfield, final String valuefield, final String entityType,
      final String entityName, final PrintWriter out) {
    out.println();

    out.print("public class ");
    out.println(entityType);
    out.print("    extends AbstractManyToOneEntityWidget<");
    out.print(entityName);
    out.print(", ");
    out.print(entityType);
    out.print(BackofficeManyToOneWidgetSuggestionCreator.CLASS_SUFFIX);
    out.println("> {");
    out.println();

    out.println("  @Inject");
    out.print("  public ");
    out.print(entityType);
    out.print(suffix);
    out.println("(final Binder uiBinder,");
    out.print("      final ResourceDelegate<");
    out.print(entityType);
    out.print(BackofficeManyToOneWidgetServiceCreator.CLASS_SUFFIX);
    out.println("> restDelegate) {");
    out.print("    super(uiBinder, restDelegate, new ");
    out.print(entityType);
    out.print(BackofficeManyToOneWidgetSuggestionCreator.CLASS_SUFFIX);
    out.println("(null, null));");
    out.println("  }");
    out.println("}");
  }
}

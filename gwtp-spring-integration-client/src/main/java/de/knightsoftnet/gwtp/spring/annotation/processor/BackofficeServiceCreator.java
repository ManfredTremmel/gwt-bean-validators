package de.knightsoftnet.gwtp.spring.annotation.processor;

import de.knightsoftnet.gwtp.spring.client.annotation.BackofficeClientGenerator;

import java.io.PrintWriter;
import java.util.List;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;

/**
 * Create JPA repository interface.
 */
public class BackofficeServiceCreator extends AbstractBackofficeCreator<BackofficeClientGenerator> {

  protected static final String CLASS_SUFFIX = "RestService";

  public BackofficeServiceCreator() {
    super(CLASS_SUFFIX);
  }

  @Override
  protected void addAdditionalImports(final String serverPackage, final Element element,
      final BackofficeClientGenerator annotationInterface, final List<BackofficeWidget> widgets,
      final ProcessingEnvironment processingEnv) {
    addImport(element.asType());
    addImports(//
        "de.knightsoftnet.mtwidgets.client.services.AdminService", //
        "javax.ws.rs.Path");
  }

  @Override
  protected void writeBody(final PrintWriter out, final String serverPackage, final Element element,
      final BackofficeClientGenerator annotationInterface, final List<BackofficeWidget> widgets,
      final ProcessingEnvironment processingEnv) {
    final String entityName = getEntityNameOfElement(element);

    out.print("@Path(\"");
    out.print(annotationInterface.value());
    out.println("\")");
    out.print("public interface ");
    out.print(entityName);
    out.print(suffix);
    out.print(" extends AdminService<");
    out.print(entityName);
    out.println("> {");
    out.println("}");
  }
}

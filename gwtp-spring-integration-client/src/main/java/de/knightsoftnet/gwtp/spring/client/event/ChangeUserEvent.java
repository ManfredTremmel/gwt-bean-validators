/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.gwtp.spring.client.event;

import de.knightsoftnet.gwtp.spring.client.event.ChangeUserEvent.ChangeUserHandler;
import de.knightsoftnet.gwtp.spring.shared.models.User;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HasHandlers;
import com.google.web.bindery.event.shared.HandlerRegistration;

/**
 * event used for user changes (e.g. login or logout).
 *
 * @author Manfred Tremmel
 *
 */
public class ChangeUserEvent extends GwtEvent<ChangeUserHandler> {
  private static final Type<ChangeUserHandler> TYPE = new Type<>();

  public interface ChangeUserHandler extends EventHandler {
    void onChangeUser(ChangeUserEvent event);
  }

  public interface ChangeUserHandlers extends HasHandlers {
    HandlerRegistration addHasChangeUserEventHandler(ChangeUserHandler handler);
  }

  private final User user;

  public ChangeUserEvent(final User user) {
    super();
    this.user = user;
  }

  public static Type<ChangeUserHandler> getType() {
    return TYPE;
  }

  @Override
  protected void dispatch(final ChangeUserHandler handler) {
    handler.onChangeUser(this);
  }

  @Override
  public Type<ChangeUserHandler> getAssociatedType() {
    return TYPE;
  }

  public User getUser() {
    return user;
  }
}

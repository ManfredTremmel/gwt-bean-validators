package de.knightsoftnet.gwtp.spring.annotation.processor;

import de.knightsoftnet.gwtp.spring.client.annotation.BackofficeClientGenerator;

import org.apache.commons.lang3.StringUtils;

import java.io.PrintWriter;
import java.util.List;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;

/**
 * Create suggestion for many to one reference widgets.
 */
public class BackofficeManyToOneWidgetSuggestionCreator
    extends AbstractBackofficeManyToOneWidgetCreator<BackofficeClientGenerator> {

  protected static final String CLASS_SUFFIX = "Suggestion";

  public BackofficeManyToOneWidgetSuggestionCreator() {
    super(CLASS_SUFFIX);
  }

  @Override
  protected void addAdditionalImports(final String serverPackage, final Element element,
      final BackofficeClientGenerator annotationInterface, final List<BackofficeWidget> widgets,
      final ProcessingEnvironment processingEnv) {
    addImport(element.asType());
    addImports("de.knightsoftnet.mtwidgets.client.ui.widget.oracle.AbstractManyToOneSuggestion", //
        "com.google.gwt.user.client.ui.SuggestOracle.Request");
  }

  @Override
  protected void writeBody(final Element element,
      final BackofficeClientGenerator annotationInterface, final List<BackofficeWidget> widgets,
      final String keyfield, final String valuefield, final String entityType,
      final String entityName, final PrintWriter out) {
    out.println();

    out.print("public class ");
    out.print(entityType);
    out.print(suffix);
    out.print(" extends AbstractManyToOneSuggestion<");
    out.print(entityName);
    out.println("> {");
    out.println();

    out.print("  public ");
    out.print(entityType);
    out.print(suffix);
    out.print("(");
    out.print(entityName);
    out.println(" entity, Request request) {");
    out.println("    super(entity, request);");
    out.println("  }");
    out.println();

    out.println("  @Override");
    out.print("  public AbstractManyToOneSuggestion<");
    out.print(entityName);
    out.print("> of(");
    out.print(entityName);
    out.println(" entity,");
    out.println("      Request request) {");
    out.print("    return new ");
    out.print(entityType);
    out.print(suffix);
    out.println("(entity, request);");
    out.println("  }");
    out.println();

    out.println("  @Override");
    out.println("  public String getReplacementString() {");
    out.print("    return entity.get");
    out.print(StringUtils.capitalize(keyfield));
    out.println("();");
    out.println("  }");
    out.println();


    out.println("  @Override");
    out.println("  public String getNameString() {");
    out.print("    return entity.get");
    out.print(StringUtils.capitalize(valuefield));
    out.println("();");
    out.println("  }");

    out.println("}");
  }
}

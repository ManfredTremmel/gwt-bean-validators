package de.knightsoftnet.gwtp.spring.client.rest.helper;

import de.knightsoftnet.gwtp.spring.shared.data.ValidationValueInterface;

public class ValidationValueImpl implements ValidationValueInterface {

  private String message;
  private String propertyPath;

  public ValidationValueImpl() {
    super();
  }

  /**
   * constructor.
   */
  public ValidationValueImpl(final String message, final String propertyPath) {
    super();
    this.message = message;
    this.propertyPath = propertyPath;
  }

  public static ValidationValueImpl of(final String message,
      final String propertyPath) {
    return new ValidationValueImpl(message, propertyPath);
  }

  @Override
  public String getMessage() {
    return message;
  }

  @Override
  public void setMessage(final String message) {
    this.message = message;
  }

  @Override
  public String getPropertyPath() {
    return propertyPath;
  }

  @Override
  public void setPropertyPath(final String propertyPath) {
    this.propertyPath = propertyPath;
  }
}

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.gwtp.spring.client.rest.helper;

import de.knightsoftnet.gwtp.spring.client.session.Session;
import de.knightsoftnet.gwtp.spring.shared.models.User;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.http.client.Response;
import com.gwtplatform.dispatch.rest.client.RestCallback;
import com.gwtplatform.dispatch.shared.ActionException;

/**
 * Login Callback for login.
 *
 * @author Manfred Tremmel
 *
 */
public class LoginCallback<T extends User, V extends EditorWithErrorHandling<?, ?>, //
    M extends LoginMessages, H extends HttpMessages> implements RestCallback<T> {

  protected final V view;
  protected final Session session;
  protected final M loginErrorMessage;
  protected final H httpMessage;
  protected Response response;

  /**
   * constructor.
   *
   * @param view view of the login page
   * @param session session data
   */
  public LoginCallback(final V view, final Session session) {
    this(view, session, GWT.create(LoginMessages.class), GWT.create(HttpMessages.class));
  }

  /**
   * constructor.
   *
   * @param view view of the login page
   * @param session session data
   * @param loginErrorMessage error message to show
   */
  public LoginCallback(final V view, final Session session, final M loginErrorMessage) {
    this(view, session, loginErrorMessage, GWT.create(HttpMessages.class));
  }

  /**
   * constructor.
   *
   * @param view view of the login page
   * @param session session data
   * @param loginErrorMessage error message to show
   * @param httpMessage http code messages to show
   */
  public LoginCallback(final V view, final Session session, final M loginErrorMessage,
      final H httpMessage) {
    super();
    this.view = view;
    this.session = session;
    this.loginErrorMessage = loginErrorMessage;
    this.httpMessage = httpMessage;
  }

  @Override
  public void onFailure(final Throwable caught) {
    try {
      throw caught;
    } catch (final ActionException e) {
      switch (response.getStatusCode()) {
        case 200: // Successful, but serialization problem
          view.showMessage(httpMessage.messageHttpCode(response.getStatusCode()));
          GWT.log(e.getMessage(), e);
          session.readSessionData();
          break;
        case 401: // Unauthorized: happens when login fails
          view.showMessage(loginErrorMessage.messageLoginError(response.getText()));
          break;
        case 403: // Forbidden: happens when csrf token times out
          session.readSessionData();
          break;
        default:
          view.showMessage(httpMessage.messageHttpCode(response.getStatusCode()));
          break;
      }
    } catch (final Throwable e) {
      view.showMessage(loginErrorMessage.messageOtherError());
    }
  }

  @Override
  public void setResponse(final Response response) {
    this.response = response;
  }

  @Override
  public void onSuccess(final T result) {
    session.setUser(result);
  }
}

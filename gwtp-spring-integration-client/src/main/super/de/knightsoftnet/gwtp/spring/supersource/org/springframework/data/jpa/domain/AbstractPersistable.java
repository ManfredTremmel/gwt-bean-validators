/*
 * Copyright 2008-2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package org.springframework.data.jpa.domain;

import org.springframework.data.domain.Persistable;
import org.springframework.lang.Nullable;

import java.io.Serializable;

import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.MappedSuperclass;
import jakarta.persistence.Transient;

/**
 * Abstract base class for entities. Allows parameterization of id type, chooses auto-generation and
 * implements {@link #equals(Object)} and {@link #hashCode()} based on that id.
 *
 * @author Oliver Gierke
 * @author Thomas Darimont
 * @author Mark Paluch
 * @author Greg Turnquist
 * @param <PK> the type of the identifier.
 */
@MappedSuperclass
public abstract class AbstractPersistable<PK extends Serializable> implements Persistable<PK> {

  @Id
  @GeneratedValue
  private @Nullable PK id;

  @Nullable
  @Override
  public PK getId() {
    return id;
  }

  /**
   * Sets the id of the entity.
   *
   * @param id the id to set
   */
  protected void setId(@Nullable final PK id) {
    this.id = id;
  }

  /**
   * Must be {@link Transient} in order to ensure that no JPA provider complains because of a
   * missing setter.
   *
   * @see org.springframework.data.domain.Persistable#isNew()
   */
  @Transient // DATAJPA-622
  @Override
  public boolean isNew() {
    return null == getId();
  }

  @Override
  public String toString() {
    return "Entity of type " + this.getClass().getName() + " with id: " + getId();
  }

  @Override
  public boolean equals(final Object obj) {

    if (null == obj) {
      return false;
    }

    if (this == obj) {
      return true;
    }

    if (getClass() != obj.getClass()) {
      return false;
    }

    final AbstractPersistable<?> that = (AbstractPersistable<?>) obj;

    return null == this.getId() ? false : this.getId().equals(that.getId());
  }

  @Override
  public int hashCode() {

    int hashCode = 17;

    hashCode += null == getId() ? 0 : getId().hashCode() * 31;

    return hashCode;
  }
}

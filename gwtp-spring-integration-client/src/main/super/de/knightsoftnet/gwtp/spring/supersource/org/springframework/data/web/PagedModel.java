/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package org.springframework.data.web;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import org.springframework.data.domain.Page;
import org.springframework.lang.Nullable;
import org.springframework.util.Assert;

import java.util.List;
import java.util.Objects;

/**
 * DTO to build stable JSON representations of a Spring Data {@link Page}. It can either be
 * selectively used in controller methods by calling {@code new PagedModel<>(page)} or generally
 * activated as representation model for {@link org.springframework.data.domain.PageImpl} instances
 * by setting {@link org.springframework.data.web.config.EnableSpringDataWebSupport}'s
 * {@code pageSerializationMode} to
 * {@link org.springframework.data.web.config.EnableSpringDataWebSupport.PageSerializationMode#VIA_DTO}.
 *
 * @author Oliver Drotbohm
 * @author Greg Turnquist
 * @author Manfred Tremmel
 * @since 3.3
 */
public class PagedModel<T> {

  private final List<T> content;

  private final PageMetadata metadata;

  /**
   * Creates a new {@link PagedModel} for the given {@link Page}.
   *
   * @param page must not be {@literal null}.
   */
  public PagedModel(final Page<T> page) {

    Assert.notNull(page, "Page must not be null");

    content = page.getContent();
    metadata = new PageMetadata(page.getSize(), page.getNumber(), page.getTotalElements(),
        page.getTotalPages());
  }

  @JsonCreator
  public PagedModel(@JsonProperty("content") final List<T> content,
      @JsonProperty("page") final PageMetadata metadata) {
    super();
    this.content = content;
    this.metadata = metadata;
  }

  @JsonProperty
  public List<T> getContent() {
    return content;
  }

  @Nullable
  @JsonProperty("page")
  public PageMetadata getMetadata() {
    return metadata;
  }

  @Override
  public boolean equals(@Nullable final Object obj) {

    if (this == obj) {
      return true;
    }

    if (!(obj instanceof final PagedModel<?> that)) {
      return false;
    }

    return Objects.equals(content, that.content) && Objects.equals(metadata, that.metadata);
  }

  @Override
  public int hashCode() {
    return Objects.hash(content, metadata);
  }

  public static class PageMetadata {
    private final long size;
    private final long number;
    private final long totalElements;
    private final long totalPages;

    @JsonCreator
    public PageMetadata(@JsonProperty("size") final long size,
        @JsonProperty("number") final long number,
        @JsonProperty("totalElements") final long totalElements,
        @JsonProperty("totalPages") final long totalPages) {
      super();
      Assert.isTrue(size > -1, "Size must not be negative!");
      Assert.isTrue(number > -1, "Number must not be negative!");
      Assert.isTrue(totalElements > -1, "Total elements must not be negative!");
      Assert.isTrue(totalPages > -1, "Total pages must not be negative!");
      this.size = size;
      this.number = number;
      this.totalElements = totalElements;
      this.totalPages = totalPages;
    }

    public long size() {
      return size;
    }

    public long number() {
      return number;
    }

    public long totalElements() {
      return totalElements;
    }

    public long totalPages() {
      return totalPages;
    }
  }
}

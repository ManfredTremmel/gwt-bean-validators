/*
 * Copyright 2002-2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package org.springframework.util;

import org.springframework.lang.Nullable;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Spliterator;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * Unmodifiable wrapper for {@link MultiValueMap}.
 *
 * @author Arjen Poutsma
 * @since 6.0
 * @param <K> the key type
 * @param <V> the value element type
 */
final class UnmodifiableMultiValueMap<K, V> implements MultiValueMap<K, V>, Serializable {

  private static final long serialVersionUID = -8697084563854098920L;

  private final MultiValueMap<K, V> delegate;

  @Nullable
  private transient Set<K> keySet;

  @Nullable
  private transient Set<Entry<K, List<V>>> entrySet;

  @Nullable
  private transient Collection<List<V>> values;


  @SuppressWarnings("unchecked")
  public UnmodifiableMultiValueMap(final MultiValueMap<? extends K, ? extends V> delegate) {
    Assert.notNull(delegate, "Delegate must not be null");
    this.delegate = (MultiValueMap<K, V>) delegate;
  }


  // delegation

  @Override
  public int size() {
    return delegate.size();
  }

  @Override
  public boolean isEmpty() {
    return delegate.isEmpty();
  }

  @Override
  public boolean containsKey(final Object key) {
    return delegate.containsKey(key);
  }

  @Override
  public boolean containsValue(final Object value) {
    return delegate.containsValue(value);
  }

  @Override
  @Nullable
  public List<V> get(final Object key) {
    final List<V> result = delegate.get(key);
    return result != null ? Collections.unmodifiableList(result) : null;
  }

  @Override
  public V getFirst(final K key) {
    return delegate.getFirst(key);
  }

  @Override
  public List<V> getOrDefault(final Object key, final List<V> defaultValue) {
    List<V> result = delegate.getOrDefault(key, defaultValue);
    if (result != defaultValue) {
      result = Collections.unmodifiableList(result);
    }
    return result;
  }

  @Override
  public void forEach(final BiConsumer<? super K, ? super List<V>> action) {
    delegate.forEach((k, vs) -> action.accept(k, Collections.unmodifiableList(vs)));
  }

  @Override
  public Map<K, V> toSingleValueMap() {
    return delegate.toSingleValueMap();
  }


  @Override
  public boolean equals(@Nullable final Object other) {
    return this == other || delegate.equals(other);
  }

  @Override
  public int hashCode() {
    return delegate.hashCode();
  }

  @Override
  public String toString() {
    return delegate.toString();
  }


  // lazy init

  @Override
  public Set<K> keySet() {
    if (keySet == null) {
      keySet = Collections.unmodifiableSet(delegate.keySet());
    }
    return keySet;
  }

  @Override
  public Set<Entry<K, List<V>>> entrySet() {
    if (entrySet == null) {
      entrySet = new UnmodifiableEntrySet<>(delegate.entrySet());
    }
    return entrySet;
  }

  @Override
  public Collection<List<V>> values() {
    if (values == null) {
      values = new UnmodifiableValueCollection<>(delegate.values());
    }
    return values;
  }

  // unsupported

  @Nullable
  @Override
  public List<V> put(final K key, final List<V> value) {
    throw new UnsupportedOperationException();
  }

  @Override
  public List<V> putIfAbsent(final K key, final List<V> value) {
    throw new UnsupportedOperationException();
  }

  @Override
  public void putAll(final Map<? extends K, ? extends List<V>> m) {
    throw new UnsupportedOperationException();
  }

  @Override
  public List<V> remove(final Object key) {
    throw new UnsupportedOperationException();
  }

  @Override
  public void add(final K key, @Nullable final V value) {
    throw new UnsupportedOperationException();
  }

  @Override
  public void addAll(final K key, final List<? extends V> values) {
    throw new UnsupportedOperationException();
  }

  @Override
  public void addAll(final MultiValueMap<K, V> values) {
    throw new UnsupportedOperationException();
  }

  @Override
  public void addIfAbsent(final K key, @Nullable final V value) {
    throw new UnsupportedOperationException();
  }

  @Override
  public void set(final K key, @Nullable final V value) {
    throw new UnsupportedOperationException();
  }

  @Override
  public void setAll(final Map<K, V> values) {
    throw new UnsupportedOperationException();
  }

  @Override
  public void replaceAll(final BiFunction<? super K, ? super List<V>, ? extends List<V>> function) {
    throw new UnsupportedOperationException();
  }

  @Override
  public boolean remove(final Object key, final Object value) {
    throw new UnsupportedOperationException();
  }

  @Override
  public boolean replace(final K key, final List<V> oldValue, final List<V> newValue) {
    throw new UnsupportedOperationException();
  }

  @Override
  public List<V> replace(final K key, final List<V> value) {
    throw new UnsupportedOperationException();
  }

  @Override
  public List<V> computeIfAbsent(final K key,
      final Function<? super K, ? extends List<V>> mappingFunction) {
    throw new UnsupportedOperationException();
  }

  @Override
  public List<V> computeIfPresent(final K key,
      final BiFunction<? super K, ? super List<V>, ? extends List<V>> remappingFunction) {
    throw new UnsupportedOperationException();
  }

  @Override
  public List<V> compute(final K key,
      final BiFunction<? super K, ? super List<V>, ? extends List<V>> remappingFunction) {
    throw new UnsupportedOperationException();
  }

  @Override
  public List<V> merge(final K key, final List<V> value,
      final BiFunction<? super List<V>, ? super List<V>, ? extends List<V>> remappingFunction) {
    throw new UnsupportedOperationException();
  }

  @Override
  public void clear() {
    throw new UnsupportedOperationException();
  }


  private static class UnmodifiableEntrySet<K, V>
      implements Set<Map.Entry<K, List<V>>>, Serializable {

    private static final long serialVersionUID = 2407578793783925203L;

    private final Set<Entry<K, List<V>>> delegate;

    @SuppressWarnings("unchecked")
    public UnmodifiableEntrySet(
        final Set<? extends Entry<? extends K, ? extends List<? extends V>>> delegate) {
      this.delegate = (Set<Entry<K, List<V>>>) delegate;
    }

    // delegation

    @Override
    public int size() {
      return delegate.size();
    }

    @Override
    public boolean isEmpty() {
      return delegate.isEmpty();
    }

    @Override
    public boolean contains(final Object o) {
      return delegate.contains(o);
    }

    @Override
    public boolean containsAll(final Collection<?> c) {
      return delegate.containsAll(c);
    }

    @Override
    public Iterator<Entry<K, List<V>>> iterator() {
      final Iterator<? extends Entry<? extends K, ? extends List<? extends V>>> iterator =
          delegate.iterator();
      return new Iterator<>() {
        @Override
        public boolean hasNext() {
          return iterator.hasNext();
        }

        @Override
        public Entry<K, List<V>> next() {
          return new UnmodifiableEntry<>(iterator.next());
        }
      };
    }

    @Override
    public Object[] toArray() {
      final Object[] result = delegate.toArray();
      filterArray(result);
      return result;
    }

    @Override
    public <T> T[] toArray(final T[] a) {
      final T[] result = delegate.toArray(a);
      filterArray(result);
      return result;
    }

    @SuppressWarnings("unchecked")
    private void filterArray(final Object[] result) {
      for (int i = 0; i < result.length; i++) {
        if (result[i] instanceof Map.Entry) {
          final Map.Entry<?, ?> entry = (Map.Entry<?, ?>) result[i];
          result[i] = new UnmodifiableEntry<>((Entry<K, List<V>>) entry);
        }
      }
    }

    @Override
    public void forEach(final Consumer<? super Entry<K, List<V>>> action) {
      delegate.forEach(e -> action.accept(new UnmodifiableEntry<>(e)));
    }

    @Override
    public Stream<Entry<K, List<V>>> stream() {
      return StreamSupport.stream(spliterator(), false);
    }

    @Override
    public Stream<Entry<K, List<V>>> parallelStream() {
      return StreamSupport.stream(spliterator(), true);
    }

    @Override
    public Spliterator<Entry<K, List<V>>> spliterator() {
      return new UnmodifiableEntrySpliterator<>(delegate.spliterator());
    }

    @Override
    public boolean equals(@Nullable final Object other) {
      return this == other || other instanceof Set && size() == ((Set<?>) other).size()
          && containsAll((Set<?>) other);
    }

    @Override
    public int hashCode() {
      return delegate.hashCode();
    }

    @Override
    public String toString() {
      return delegate.toString();
    }

    // unsupported

    @Override
    public boolean add(final Entry<K, List<V>> kListEntry) {
      throw new UnsupportedOperationException();
    }

    @Override
    public boolean remove(final Object o) {
      throw new UnsupportedOperationException();
    }

    @Override
    public boolean removeIf(final Predicate<? super Entry<K, List<V>>> filter) {
      throw new UnsupportedOperationException();
    }

    @Override
    public boolean addAll(final Collection<? extends Entry<K, List<V>>> c) {
      throw new UnsupportedOperationException();
    }

    @Override
    public boolean retainAll(final Collection<?> c) {
      throw new UnsupportedOperationException();
    }

    @Override
    public boolean removeAll(final Collection<?> c) {
      throw new UnsupportedOperationException();
    }

    @Override
    public void clear() {
      throw new UnsupportedOperationException();
    }


    private static class UnmodifiableEntrySpliterator<K, V>
        implements Spliterator<Entry<K, List<V>>> {

      private final Spliterator<Entry<K, List<V>>> delegate;

      @SuppressWarnings("unchecked")
      public UnmodifiableEntrySpliterator(
          final Spliterator<? extends Entry<? extends K, ? extends List<? extends V>>> delegate) {

        this.delegate = (Spliterator<Entry<K, List<V>>>) delegate;
      }

      @Override
      public boolean tryAdvance(final Consumer<? super Entry<K, List<V>>> action) {
        return delegate.tryAdvance(entry -> action.accept(new UnmodifiableEntry<>(entry)));
      }

      @Override
      public void forEachRemaining(final Consumer<? super Entry<K, List<V>>> action) {
        delegate.forEachRemaining(entry -> action.accept(new UnmodifiableEntry<>(entry)));
      }

      @Override
      @Nullable
      public Spliterator<Entry<K, List<V>>> trySplit() {
        final Spliterator<? extends Entry<? extends K, ? extends List<? extends V>>> split =
            delegate.trySplit();
        if (split != null) {
          return new UnmodifiableEntrySpliterator<>(split);
        } else {
          return null;
        }
      }

      @Override
      public long estimateSize() {
        return delegate.estimateSize();
      }

      @Override
      public long getExactSizeIfKnown() {
        return delegate.getExactSizeIfKnown();
      }

      @Override
      public int characteristics() {
        return delegate.characteristics();
      }

      @Override
      public boolean hasCharacteristics(final int characteristics) {
        return delegate.hasCharacteristics(characteristics);
      }

      @Override
      public Comparator<? super Entry<K, List<V>>> getComparator() {
        return delegate.getComparator();
      }
    }


    private static class UnmodifiableEntry<K, V> implements Map.Entry<K, List<V>> {

      private final Entry<K, List<V>> delegate;

      @SuppressWarnings("unchecked")
      public UnmodifiableEntry(final Entry<? extends K, ? extends List<? extends V>> delegate) {
        Assert.notNull(delegate, "Delegate must not be null");
        this.delegate = (Entry<K, List<V>>) delegate;
      }

      @Override
      public K getKey() {
        return delegate.getKey();
      }

      @Override
      public List<V> getValue() {
        return Collections.unmodifiableList(delegate.getValue());
      }

      @Override
      public List<V> setValue(final List<V> value) {
        throw new UnsupportedOperationException();
      }

      @Override
      public boolean equals(@Nullable final Object other) {
        return this == other
            || other instanceof Map.Entry && getKey().equals(((Map.Entry<?, ?>) other).getKey())
                && getValue().equals(((Map.Entry<?, ?>) other).getValue());
      }

      @Override
      public int hashCode() {
        return delegate.hashCode();
      }

      @Override
      public String toString() {
        return delegate.toString();
      }
    }
  }


  private static class UnmodifiableValueCollection<V> implements Collection<List<V>>, Serializable {

    private static final long serialVersionUID = 5518377583904339588L;

    private final Collection<List<V>> delegate;

    public UnmodifiableValueCollection(final Collection<List<V>> delegate) {
      this.delegate = delegate;
    }

    // delegation

    @Override
    public int size() {
      return delegate.size();
    }

    @Override
    public boolean isEmpty() {
      return delegate.isEmpty();
    }

    @Override
    public boolean contains(final Object o) {
      return delegate.contains(o);
    }

    @Override
    public boolean containsAll(final Collection<?> c) {
      return delegate.containsAll(c);
    }

    @Override
    public Object[] toArray() {
      final Object[] result = delegate.toArray();
      filterArray(result);
      return result;
    }

    @Override
    public <T> T[] toArray(final T[] a) {
      final T[] result = delegate.toArray(a);
      filterArray(result);
      return result;
    }

    private void filterArray(final Object[] array) {
      for (int i = 0; i < array.length; i++) {
        if (array[i] instanceof List) {
          final List<?> other = (List<?>) array[i];
          array[i] = Collections.unmodifiableList(other);
        }
      }
    }

    @Override
    public Iterator<List<V>> iterator() {
      final Iterator<List<V>> iterator = delegate.iterator();
      return new Iterator<>() {
        @Override
        public boolean hasNext() {
          return iterator.hasNext();
        }

        @Override
        public List<V> next() {
          return Collections.unmodifiableList(iterator.next());
        }
      };
    }

    @Override
    public void forEach(final Consumer<? super List<V>> action) {
      delegate.forEach(list -> action.accept(Collections.unmodifiableList(list)));
    }

    @Override
    public Spliterator<List<V>> spliterator() {
      return new UnmodifiableValueSpliterator<>(delegate.spliterator());
    }

    @Override
    public Stream<List<V>> stream() {
      return StreamSupport.stream(spliterator(), false);
    }

    @Override
    public Stream<List<V>> parallelStream() {
      return StreamSupport.stream(spliterator(), true);
    }

    @Override
    public boolean equals(@Nullable final Object other) {
      return this == other || delegate.equals(other);
    }

    @Override
    public int hashCode() {
      return delegate.hashCode();
    }

    @Override
    public String toString() {
      return delegate.toString();
    }

    // unsupported

    @Override
    public boolean add(final List<V> ts) {
      throw new UnsupportedOperationException();
    }

    @Override
    public boolean remove(final Object o) {
      throw new UnsupportedOperationException();
    }

    @Override
    public boolean addAll(final Collection<? extends List<V>> c) {
      throw new UnsupportedOperationException();
    }

    @Override
    public boolean removeAll(final Collection<?> c) {
      throw new UnsupportedOperationException();
    }

    @Override
    public boolean retainAll(final Collection<?> c) {
      throw new UnsupportedOperationException();
    }

    @Override
    public boolean removeIf(final Predicate<? super List<V>> filter) {
      throw new UnsupportedOperationException();
    }

    @Override
    public void clear() {
      throw new UnsupportedOperationException();
    }


    private static class UnmodifiableValueSpliterator<T> implements Spliterator<List<T>> {

      private final Spliterator<List<T>> delegate;

      public UnmodifiableValueSpliterator(final Spliterator<List<T>> delegate) {
        this.delegate = delegate;
      }

      @Override
      public boolean tryAdvance(final Consumer<? super List<T>> action) {
        return delegate.tryAdvance(l -> action.accept(Collections.unmodifiableList(l)));
      }

      @Override
      public void forEachRemaining(final Consumer<? super List<T>> action) {
        delegate.forEachRemaining(l -> action.accept(Collections.unmodifiableList(l)));
      }

      @Override
      @Nullable
      public Spliterator<List<T>> trySplit() {
        final Spliterator<List<T>> split = delegate.trySplit();
        if (split != null) {
          return new UnmodifiableValueSpliterator<>(split);
        } else {
          return null;
        }
      }

      @Override
      public long estimateSize() {
        return delegate.estimateSize();
      }

      @Override
      public long getExactSizeIfKnown() {
        return delegate.getExactSizeIfKnown();
      }

      @Override
      public int characteristics() {
        return delegate.characteristics();
      }

      @Override
      public boolean hasCharacteristics(final int characteristics) {
        return delegate.hasCharacteristics(characteristics);
      }

      @Override
      public Comparator<? super List<T>> getComparator() {
        return delegate.getComparator();
      }
    }
  }

}

/*
 * Copyright 2012-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package org.springframework.hateoas;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Objects;

/**
 * General helper to easily create a wrapper for a collection of entities.
 *
 * @author Oliver Gierke
 * @author Greg Turnquist
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Resources<T> implements Iterable<T> {

  private final Iterable<T> content;

  /**
   * Creates an empty {@link Resources} instance.
   */
  protected Resources() {
    this(new ArrayList<T>());
  }

  /**
   * Creates a {@link Resources} instance with the given content and {@link Link}s.
   *
   * @param content must not be {@literal null}.
   * @param links the links to be added to the {@link Resources}.
   */
  @JsonCreator
  public Resources(final Iterable<T> content) {

    Assert.notNull(content, "Content must not be null!");

    this.content = new ArrayList<>();

    for (final T element : content) {
      ((ArrayList<T>) this.content).add(element);
    }
  }

  /**
   * Creates a new {@link Resources} instance by wrapping the given domain class instances into a
   * {@link Resource}.
   *
   * @param content must not be {@literal null}.
   * @return
   */
  @SuppressWarnings("unchecked")
  public static <T extends Resource<S>, S> Resources<T> wrap(final Iterable<S> content) {

    Assert.notNull(content, "Content must not be null!");
    final ArrayList<T> resources = new ArrayList<>();

    for (final S element : content) {
      resources.add((T) new Resource<>(element));
    }

    return new Resources<>(resources);
  }

  /**
   * Returns the underlying elements.
   *
   * @return the content will never be {@literal null}.
   */
  @JsonProperty("_embedded")
  public Iterable<T> getContent() {
    return this.content;
  }

  /*
   * (non-Javadoc)
   *
   * @see java.lang.Iterable#iterator()
   */
  @Override
  public Iterator<T> iterator() {
    return content.iterator();
  }

  /*
   * (non-Javadoc)
   *
   * @see org.springframework.hateoas.ResourceSupport#toString()
   */
  @Override
  public String toString() {
    return "Resources { content: " + getContent() + "}";
  }

  /*
   * (non-Javadoc)
   *
   * @see org.springframework.hateoas.ResourceSupport#equals(java.lang.Object)
   */
  @Override
  public boolean equals(final Object obj) {

    if (obj == this) {
      return true;
    }

    if (obj == null || !obj.getClass().equals(getClass())) {
      return false;
    }

    final Resources<?> that = (Resources<?>) obj;

    return Objects.equals(this.content, that.content);
  }

  /*
   * (non-Javadoc)
   *
   * @see org.springframework.hateoas.ResourceSupport#hashCode()
   */
  @Override
  public int hashCode() {
    return Objects.hashCode(content);
  }
}

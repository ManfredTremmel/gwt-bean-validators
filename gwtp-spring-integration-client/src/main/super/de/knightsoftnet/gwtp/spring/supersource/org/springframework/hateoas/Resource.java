/*
 * Copyright 2012-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package org.springframework.hateoas;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonUnwrapped;

import org.springframework.util.Assert;

import java.util.Collection;
import java.util.Objects;

/**
 * A simple {@link Resource} wrapping a domain object and adding links to it.
 *
 * @author Oliver Gierke
 * @author Greg Turnquist
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Resource<T> {

  private final T content;

  /**
   * Creates an empty {@link Resource}.
   */
  protected Resource() {
    this.content = null;
  }

  /**
   * Creates a new {@link Resource} with the given content and {@link Link}s.
   *
   * @param content must not be {@literal null}.
   */
  public Resource(final T content) {

    Assert.notNull(content, "Content must not be null!");
    Assert.isTrue(!(content instanceof Collection),
        "Content must not be a collection! Use Resources instead!");
    this.content = content;
  }

  /**
   * Returns the underlying entity.
   *
   * @return the content
   */
  @JsonUnwrapped
  public T getContent() {
    return content;
  }

  /*
   * (non-Javadoc)
   *
   * @see org.springframework.hateoas.ResourceSupport#toString()
   */
  @Override
  public String toString() {
    return "Resource { content: " + getContent() + " }";
  }

  /*
   * (non-Javadoc)
   *
   * @see org.springframework.hateoas.ResourceSupport#equals(java.lang.Object)
   */
  @Override
  public boolean equals(final Object obj) {

    if (this == obj) {
      return true;
    }

    if (obj == null || !obj.getClass().equals(getClass())) {
      return false;
    }

    final Resource<?> that = (Resource<?>) obj;

    return Objects.equals(this.content, that.content);
  }

  /*
   * (non-Javadoc)
   *
   * @see org.springframework.hateoas.ResourceSupport#hashCode()
   */
  @Override
  public int hashCode() {
    return Objects.hashCode(content);
  }
}

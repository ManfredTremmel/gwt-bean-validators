/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.gwtp.spring.annotation.processor;

import de.knightsoftnet.gwtp.spring.shared.data.jpa.domain.AbstractPersistable;
import de.knightsoftnet.validators.shared.Email;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;

/**
 * email data.
 */
@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
public class EmailData extends AbstractPersistable<Long> {

  @Email
  @NotEmpty
  private String email;

  @NotNull
  @Enumerated(EnumType.STRING)
  private EmailTypeEnum type;

  @ManyToOne(optional = false, cascade = CascadeType.PERSIST)
  @JsonIgnore
  private Person person;

  public void setEmail(final String email) {
    this.email = email;
  }

  public String getEmail() {
    return email;
  }

  public final EmailTypeEnum getType() {
    return type;
  }

  public final void setType(final EmailTypeEnum type) {
    this.type = type;
  }

  public Person getPerson() {
    return person;
  }

  public void setPerson(final Person person) {
    this.person = person;
  }

  @Override
  public String toString() {
    return "EmailData [email=" + email + ", type=" + type + "]";
  }
}

package de.knightsoftnet.gwtp.spring.annotation.processor;

/**
 * possible salutations.
 *
 * @author Manfred Tremmel
 */
public enum SalutationEnum {
  // Mister
  MR,
  // Misses
  MRS,
  // Inter
  INTER
}

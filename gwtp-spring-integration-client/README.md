# gwtp-spring-integration-client
Basic functionality to integrate gwtp with spring, client part.

It includes

* authentication integration with spring security
* user session handling
* validation handling and report to client
* rest callback implementations handling errors from server
* expose some interfaces to client

Maven integration
-----------------

The dependency itself for GWT-Projects:

```xml
    <dependency>
      <groupId>de.knightsoft-net</groupId>
      <artifactId>gwtp-spring-integration-client</artifactId>
      <version>2.4.1</version>
    </dependency>
```

GWT Integration
---------------

You have to inherit GwtpSpringIntegration into your project .gwt.xml file:

```xml
<inherits name="de.knightsoftnet.gwtp.spring.GwtpSpringIntegration" />
```

New in version 2.4.0 or later
-----------------------------
gin and gwtp have been updated to own forks using the new `jakarta.inject` namespace instead of the outdated `javax.inject`. This makes it possible to share more code between server and client.

gwt-bean-validators-restservice-spring
======================================

This package contains the server side rest services based on spring controller for [gwt-bean-validators-rest-resolver](../gwt-bean-validators-rest-resolver).


Maven integraten
----------------

```xml
    <dependency>
      <groupId>de.knightsoft-net</groupId>
      <artifactId>gwt-bean-validators-restservice-spring</artifactId>
      <version>2.4.1</version>
    </dependency>
```

In the spring application don't forget to extend the component scan annotation:

```java
@ComponentScan(basePackages = {...,
    "de.knightsoftnet.validators.server.controller"})
```

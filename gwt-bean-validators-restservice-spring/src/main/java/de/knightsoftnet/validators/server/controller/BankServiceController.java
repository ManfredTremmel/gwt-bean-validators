/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.server.controller;

import de.knightsoftnet.validators.shared.ResourcePaths.BankData;
import de.knightsoftnet.validators.shared.data.BankAccountBicConstantsImpl;
import de.knightsoftnet.validators.shared.data.BankValidationConstantsData;
import de.knightsoftnet.validators.shared.data.BicMapConstantsImpl;
import de.knightsoftnet.validators.shared.data.IbanLengthMapConstantsImpl;
import de.knightsoftnet.validators.shared.util.BankConstantsProviderImpl;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jakarta.annotation.security.PermitAll;

/**
 * phone number web service brings phone number util functions to client.
 *
 * @author Manfred Tremmel
 */
@RestController
@RequestMapping(value = BankData.ROOT, produces = MediaType.APPLICATION_JSON_VALUE)
public class BankServiceController {

  private final BankValidationConstantsData constants;

  /**
   * default constructor.
   */
  public BankServiceController() {
    super();
    final BankConstantsProviderImpl provider = new BankConstantsProviderImpl();
    constants = new BankValidationConstantsData(
        (IbanLengthMapConstantsImpl) provider.getIbanLengthMapSharedConstants(),
        (BankAccountBicConstantsImpl) provider.getBankAccountBicSharedConstants(),
        (BicMapConstantsImpl) provider.getBicMapConstants());
  }

  @GetMapping
  @PermitAll
  public BankValidationConstantsData getBankValidationConstants() {
    return constants;
  }
}

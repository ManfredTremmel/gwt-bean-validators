/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.server.controller;

import de.knightsoftnet.validators.shared.Parameters;
import de.knightsoftnet.validators.shared.ResourcePaths.PhoneNumber;
import de.knightsoftnet.validators.shared.data.CreatePhoneCountryConstantsClass;
import de.knightsoftnet.validators.shared.data.PhoneCountryData;
import de.knightsoftnet.validators.shared.util.LocaleUtil;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

import jakarta.annotation.security.PermitAll;

/**
 * phone number web service brings phone number util functions to client.
 *
 * @author Manfred Tremmel
 */
@RestController
@RequestMapping(value = PhoneNumber.ROOT, produces = MediaType.APPLICATION_JSON_VALUE)
public class PhoneNumberServiceController {

  /**
   * return phone country constants.
   *
   * @param language language for which should language specific values be requested
   * @return PhoneCountrySharedConstants
   */
  @GetMapping(PhoneNumber.GET_PHONE_COUNTRY_CONSTANTS + "/{" + Parameters.LANGUAGE + "}")
  @PermitAll
  public Map<String, PhoneCountryData> getPhoneCountryConstants(
      @PathVariable(value = Parameters.LANGUAGE) final String language) {
    if (StringUtils.isEmpty(language)) {
      return CreatePhoneCountryConstantsClass.create().getCountriesMap();
    }
    return CreatePhoneCountryConstantsClass.create(LocaleUtil.convertLanguageToLocale(language))
        .getCountriesMap();
  }
}

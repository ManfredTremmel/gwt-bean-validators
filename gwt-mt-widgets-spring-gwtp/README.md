# gwt-mt-widgets-spring-gwtp
A set of widgets and handlers for gwt applications using gwt-bean-validators - REST calls based on gwtp rest dispatcher
Using this inherits instead of [gwt-mt-widgets](../gwt-mt-widgets) itself, means the needed data for PhoneNumber*SuggestBox, IbanTextBox and BicSuggestBox is not compiled into client side Java-Script code, it's read by rest call from the server.

Maven integraten
----------------

The dependency itself for GWT-Projects:

```xml
    <dependency>
      <groupId>de.knightsoft-net</groupId>
      <artifactId>gwt-mt-widgets-spring-gwtp</artifactId>
      <version>2.4.1</version>
    </dependency>
```

GWT Integration
---------------

What you still have to do, inherit GwtMtWidgets into your project .gwt.xml file:

```xml
<inherits name="de.knightsoftnet.mtwidgets.GwtMtWidgetsSpringGwtp" />
```

Included Widgets
----------------

|Widget | Usage
|-------|------
|AdminNavigationWidget | backoffice widget for navigate, search and handle entries, more about backoffice integration, you can find in my [wiki](https://gitlab.com/ManfredTremmel/gwt-bean-validators-example/-/wiki_pages/Backoffice)
|MultiLanguageTextBox | a text input widget for editing text in multiple languages (each one defined for the project)
|PageableList | can display spring pages including navigation between different pages and sorting entries by clicking on header

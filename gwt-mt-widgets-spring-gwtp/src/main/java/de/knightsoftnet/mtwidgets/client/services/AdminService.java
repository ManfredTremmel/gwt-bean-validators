/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.mtwidgets.client.services;

import de.knightsoftnet.gwtp.spring.shared.data.AdminResult;
import de.knightsoftnet.validators.shared.Parameters;

import org.springframework.data.domain.Persistable;
import org.springframework.data.web.PagedModel;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;

/**
 * Definition of administration service.
 *
 * @author Manfred Tremmel
 */
public interface AdminService<T extends Persistable<Long>> {

  @GET
  AdminResult<Long, T> getEntry();

  @GET
  @Path("/{" + Parameters.ID + "}")
  AdminResult<Long, T> getEntryById(@PathParam(Parameters.ID) final Long id);

  @GET
  @Path("/" + Parameters.SEARCH + "/{" + Parameters.LANGUAGE + "}")
  PagedModel<T> search(@PathParam(Parameters.LANGUAGE) final String language,
      @QueryParam(Parameters.SEARCH) final String search);

  @GET
  @Path("/" + Parameters.SEARCH + "/{" + Parameters.LANGUAGE + "}")
  PagedModel<T> searchPaging(@PathParam(Parameters.LANGUAGE) final String language,
      @QueryParam(Parameters.SEARCH) final String search,
      @QueryParam(Parameters.PAGE) final int page, @QueryParam(Parameters.SIZE) final int size);

  @GET
  @Path("/" + Parameters.SEARCH + "/{" + Parameters.LANGUAGE + "}")
  PagedModel<T> searchPagingAndSorting(@PathParam(Parameters.LANGUAGE) final String language,
      @QueryParam(Parameters.SEARCH) final String search,
      @QueryParam(Parameters.PAGE) final int page, @QueryParam(Parameters.SIZE) final int size,
      @QueryParam(Parameters.SORT) final String sort);

  @POST
  AdminResult<Long, T> createEntry(final T entry);

  @PUT
  @Path("/{" + Parameters.ID + "}")
  AdminResult<Long, T> changeEntry(@PathParam(Parameters.ID) final Long id, final T entry);

  @DELETE
  @Path("/{" + Parameters.ID + "}")
  AdminResult<Long, T> deleteEntry(@PathParam(Parameters.ID) final Long id);
}

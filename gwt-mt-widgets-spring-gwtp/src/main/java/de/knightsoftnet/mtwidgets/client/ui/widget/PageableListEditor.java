/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.mtwidgets.client.ui.widget;

import de.knightsoftnet.gwtp.spring.shared.search.TableFieldDefinition;
import de.knightsoftnet.mtwidgets.client.ui.widget.features.HandlesSelectedEntry;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.FlowPanel;

import org.gwtproject.editor.client.IsEditor;
import org.gwtproject.editor.client.SimpleBeanEditorDriver;
import org.gwtproject.editor.client.adapters.EditorSource;
import org.gwtproject.editor.client.adapters.ListEditor;
import org.gwtproject.editor.client.annotation.IsDriver;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import jakarta.inject.Inject;

/**
 * editor to show a list of search results.
 *
 * @author Manfred Tremmel
 */
public class PageableListEditor<T> extends FlowPanel
    implements IsEditor<ListEditor<Object, PageableListEntry<Object>>>, ClickHandler {

  /**
   * interface of the driver to combine ui and bean.
   */
  @IsDriver
  interface Driver
      extends SimpleBeanEditorDriver<List<Object>, ListEditor<Object, PageableListEntry<Object>>> {
  }

  private class AdminNavigationSearchResultEntryEditorSource
      extends EditorSource<PageableListEntry<Object>> {

    @SuppressWarnings({"unchecked", "rawtypes"})
    @Override
    public PageableListEntry<Object> create(final int index) {
      final PageableListEntry<Object> subEditor = new PageableListEntry(tableFieldDefinitions);
      subEditor.addClickHandler(PageableListEditor.this);
      insert(subEditor, index);
      return subEditor;
    }

    @Override
    public void dispose(final PageableListEntry<Object> subEditor) {
      subEditor.removeFromParent();
    }

    @Override
    public void setIndex(final PageableListEntry<Object> editor, final int index) {
      insert(editor, index);
    }
  }

  private final Driver driver;
  private final ListEditor<Object, //
      PageableListEntry<Object>> editor =
          ListEditor.of(new AdminNavigationSearchResultEntryEditorSource());

  private Collection<TableFieldDefinition<T>> tableFieldDefinitions;
  private HandlesSelectedEntry<T> parent;

  /**
   * constructor.
   *
   */
  @Inject
  public PageableListEditor(final PageableListEditor_Driver_Impl driver) {
    super();
    this.driver = driver;
    this.driver.initialize(editor);
  }

  @Override
  public ListEditor<Object, //
      PageableListEntry<Object>> asEditor() {
    return editor;
  }

  public void showList(final List<T> list) {
    driver.edit(list.stream().map(entry -> (Object) entry).collect(Collectors.toList()));
  }

  @SuppressWarnings("unchecked")
  @Override
  public void onClick(final ClickEvent event) {
    parent.handleSelectedEntry(((PageableListEntry<T>) event.getSource()).getValue());
  }

  public void setTableFieldDefinitions(
      final Collection<TableFieldDefinition<T>> tableFieldDefinitions) {
    this.tableFieldDefinitions = tableFieldDefinitions;
  }

  public void setParent(final HandlesSelectedEntry<T> parent) {
    this.parent = parent;
  }
}

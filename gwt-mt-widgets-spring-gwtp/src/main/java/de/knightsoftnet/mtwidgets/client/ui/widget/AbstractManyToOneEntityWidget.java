package de.knightsoftnet.mtwidgets.client.ui.widget;

import de.knightsoftnet.gwtp.spring.client.rest.helper.RestCallbackBuilder;
import de.knightsoftnet.mtwidgets.client.services.ManyToOneSuggestionRestService;
import de.knightsoftnet.mtwidgets.client.ui.widget.features.HasValidationMessageElement;
import de.knightsoftnet.mtwidgets.client.ui.widget.oracle.AbstractManyToOneSuggestion;
import de.knightsoftnet.mtwidgets.client.ui.widget.oracle.ManyToOneSuggestionWidgetOracle;
import de.knightsoftnet.validators.client.decorators.ExtendedValueBoxEditor;
import de.knightsoftnet.validators.client.editor.ValueBoxEditor;

import com.google.gwt.editor.client.LeafValueEditor;
import com.google.gwt.event.dom.client.DomEvent;
import com.google.gwt.event.dom.client.HasKeyPressHandlers;
import com.google.gwt.event.dom.client.HasKeyUpHandlers;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import com.gwtplatform.dispatch.rest.delegates.client.ResourceDelegate;

import org.gwtproject.editor.client.TakesValue;

/**
 * Input widget for database many to one entity references.
 *
 * @param <T> type of the entity
 * @param <S> suggestion for entity type
 */
public abstract class AbstractManyToOneEntityWidget<T, S extends AbstractManyToOneSuggestion<T>>
    extends Composite implements LeafValueEditor<T>, HasValue<T>, TakesValue<T>,
    HasValidationMessageElement<T, ValueBoxEditor<T>>, HasKeyUpHandlers, HasKeyPressHandlers {

  /**
   * ui binder interface.
   */
  public interface Binder extends UiBinder<Widget, AbstractManyToOneEntityWidget<?, ?>> {
  }

  private final ValueBoxEditor<T> editor;
  private final S suggestionPrototype;
  private S valueSuggestion;

  @UiField(provided = true)
  @org.gwtproject.editor.client.Editor.Ignore
  SuggestBoxWithEditorErrors valueKey;

  @UiField
  @org.gwtproject.editor.client.Editor.Ignore
  Label valueName;

  /**
   * default constructor.
   *
   * @param uiBinder ui binder
   * @param restDelegate rest delegate
   */
  public AbstractManyToOneEntityWidget(final Binder uiBinder,
      final ResourceDelegate<? extends ManyToOneSuggestionRestService<T>> restDelegate,
      final S suggestionPrototype) {
    super();
    this.suggestionPrototype = suggestionPrototype;
    valueKey = new SuggestBoxWithEditorErrors(
        new ManyToOneSuggestionWidgetOracle<>(restDelegate, suggestionPrototype));
    initWidget(uiBinder.createAndBindUi(this));
    editor = new ExtendedValueBoxEditor<>(this, null);
    valueKey.addValueChangeHandler(keyChanged -> {
      restDelegate.withCallback(RestCallbackBuilder.<T>build(

          success -> {
            setValue(success, false);
            ValueChangeEvent.fire(this, success);
          },

          failure -> setCustomValidity(failure.getMessage())

      )).getReferenceByKey(keyChanged.getValue());
    });
    valueKey.addKeyPressHandler(event -> DomEvent.fireNativeEvent(event.getNativeEvent(), this));
    valueKey.addKeyUpHandler(event -> DomEvent.fireNativeEvent(event.getNativeEvent(), this));
  }

  @Override
  public HandlerRegistration addValueChangeHandler(final ValueChangeHandler<T> handler) {
    return addHandler(handler, ValueChangeEvent.getType());
  }

  @Override
  public HandlerRegistration addKeyPressHandler(final KeyPressHandler handler) {
    return addHandler(handler, KeyPressEvent.getType());
  }

  @Override
  public HandlerRegistration addKeyUpHandler(final KeyUpHandler handler) {
    return addHandler(handler, KeyUpEvent.getType());
  }

  @Override
  public T getValue() {
    return valueSuggestion == null ? null : valueSuggestion.getEntity();
  }

  @Override
  public void setValue(final T value) {
    setValue(value, false);
  }

  @SuppressWarnings("unchecked")
  @Override
  public void setValue(final T value, final boolean fireEvents) {
    valueSuggestion = (S) suggestionPrototype.of(value, null);
    if (value == null) {
      valueKey.setValue(null, false);
      valueName.setText(null);
    } else {
      valueKey.setValue(valueSuggestion.getReplacementString(), false);
      valueName.setText(valueSuggestion.getNameString());
    }
    if (fireEvents) {
      ValueChangeEvent.fire(this, value);
    }
  }

  @Override
  public ValueBoxEditor<T> asEditor() {
    return editor;
  }

  @Override
  public void setValidationMessageElement(final HTMLPanel element) {
    valueKey.setValidationMessageElement(element);
  }

  @Override
  public HTMLPanel getValidationMessageElement() {
    return valueKey.getValidationMessageElement();
  }

  @Override
  public void setCustomValidity(final String message) {
    valueKey.setCustomValidity(message);
  }

  @Override
  protected void onEnsureDebugId(final String baseId) {
    valueKey.ensureDebugId(baseId);
  }
}

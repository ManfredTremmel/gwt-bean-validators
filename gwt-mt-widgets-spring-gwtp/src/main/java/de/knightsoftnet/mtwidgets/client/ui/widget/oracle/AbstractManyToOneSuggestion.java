/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.mtwidgets.client.ui.widget.oracle;

import com.google.gwt.user.client.ui.SuggestOracle.Request;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;

import org.apache.commons.lang3.StringUtils;

/**
 * abstract suggestion implementation for entities which implement HasKeyValueSuggestion interface.
 *
 * @param <T> type of entity
 */
public abstract class AbstractManyToOneSuggestion<T> implements Suggestion {

  protected final T entity;
  protected final Request request;

  /**
   * default constructor.
   *
   * @param entity the entity which was found
   * @param request the request which was fired
   */
  public AbstractManyToOneSuggestion(T entity, Request request) {
    super();
    this.entity = entity;
    this.request = request;
  }

  /**
   * generate new instance.
   *
   * @param entity the entity which was found
   * @param request the request which was fired
   * @return new entity instance
   */
  public abstract AbstractManyToOneSuggestion<T> of(T entity, Request request);

  @Override
  public String getDisplayString() {
    return request == null ? getReplacementString()
        : getReplacementString().replace(request.getQuery(),
            "<strong>" + request.getQuery() + "</strong>") + StringUtils.SPACE + getNameString();
  }

  /**
   * get name element of the entity.
   *
   * @return name string
   */
  public abstract String getNameString();

  /**
   * get entity base element.
   *
   * @return entity
   */
  public T getEntity() {
    return entity;
  }
}

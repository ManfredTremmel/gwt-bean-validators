package de.knightsoftnet.mtwidgets.client.ui.widget;

import de.knightsoftnet.gwtp.spring.shared.search.TableFieldDefinition;
import de.knightsoftnet.mtwidgets.client.ui.widget.features.HandlesSelectedEntry;
import de.knightsoftnet.mtwidgets.client.ui.widget.styling.WidgetResources;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiFactory;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.TakesValue;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Provider;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.gwtproject.editor.client.EditorDelegate;
import org.gwtproject.editor.client.ValueAwareEditor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Persistable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.data.web.PagedModel;
import org.springframework.data.web.PagedModel.PageMetadata;

import java.util.Collection;
import java.util.Map;
import java.util.stream.Collectors;

import jakarta.inject.Inject;

/**
 * list with pages.
 *
 * @author Manfred Tremmel
 *
 * @param <T> type of the list entries
 */
public class PageableList<T extends Persistable<Long>> extends Composite
    implements ValueAwareEditor<PagedModel<T>>, TakesValue<PagedModel<T>>,
    HasValueChangeHandlers<Pageable>, HandlesSelectedEntry<T> {

  interface Binder extends UiBinder<Widget, PageableList<?>> {
  }

  @UiField
  HTMLPanel header;

  @UiField
  PageableListEditor<T> content;

  @UiField
  PageNumberWithArrowsWidget pageable;

  private final Provider<PageableListEditor<T>> searchResultListProvider;

  /**
   * the default resources.
   */
  private final WidgetResources resources;

  private Map<String, HTMLPanel> headerMap;

  private PagedModel<T> value;

  private Sort sort;

  private HandlesSelectedEntry<T> parent;

  /**
   * Constructor for Pageable List.
   */
  @Inject
  public PageableList(final Binder binder, final WidgetResources resources,
      final Provider<PageableListEditor<T>> searchResultListProvider) {
    super();
    this.resources = resources;
    sort = Sort.unsorted();
    value = createEmptyValue();
    this.resources.pageableStyle().ensureInjected();
    this.searchResultListProvider = searchResultListProvider;
    initWidget(binder.createAndBindUi(this));
    content.setParent(this);
  }

  @Ignore
  @UiFactory
  public PageableListEditor<T> buildSearchResultList() {
    return searchResultListProvider.get();
  }

  @Override
  public void setDelegate(final EditorDelegate<PagedModel<T>> delegate) {
    delegate.subscribe();
  }

  @Override
  public PagedModel<T> getValue() {
    return value;
  }

  public boolean hasEntries() {
    return value.getMetadata().totalElements() > 0L;
  }

  @Override
  public void setValue(final PagedModel<T> value) {
    this.value = ObjectUtils.defaultIfNull(value, createEmptyValue());
    content.showList(this.value.getContent());
    pageable.setValue(this.value.getMetadata());
  }

  @Override
  public void flush() {
    // nothing to do
  }

  @Override
  public void onPropertyChange(final String... paths) {
    // nothing to do
  }

  @Override
  public HandlerRegistration addValueChangeHandler(final ValueChangeHandler<Pageable> handler) {
    return addHandler(handler, ValueChangeEvent.getType());
  }

  @Override
  public void handleSelectedEntry(final T entry) {
    parent.handleSelectedEntry(entry);
  }

  /**
   * pageable changed.
   *
   * @param event value change handler
   */
  @UiHandler("pageable")
  public void pageableChanged(final ValueChangeEvent<PageMetadata> event) {
    final Pageable newPageable =
        PageRequest.of((int) event.getValue().number(), (int) event.getValue().size(), sort);
    ValueChangeEvent.fire(this, newPageable);
  }

  /**
   * set table field definitions.
   *
   * @param tableFieldDefinitions collection of field definitions
   */
  public void setTableFieldDefinitions(
      final Collection<TableFieldDefinition<T>> tableFieldDefinitions) {
    content.setTableFieldDefinitions(tableFieldDefinitions);
    headerMap = tableFieldDefinitions.stream() //
        .collect(Collectors.toMap(

            TableFieldDefinition::getFieldName, //
            definition -> {
              final HTMLPanel headerCell = new HTMLPanel(definition.getFieldDisplayName());
              definition.getStyles().forEach(headerCell::addStyleName);
              headerCell.addDomHandler(event -> {
                final Direction direction;
                if (StringUtils.contains(headerCell.getStyleName(),
                    resources.pageableStyle().headerSortUp())) {
                  direction = Direction.DESC;
                } else {
                  direction = Direction.ASC;
                }
                fireSortChanged(Sort.by(direction, definition.getFieldName()));
              }, ClickEvent.getType());
              header.add(headerCell);
              return headerCell;
            }));
  }

  /**
   * clear/reset sorting (no header is selectd).
   */
  public void clearSort() {
    headerMap.forEach((key, headerCell) -> {
      headerCell.setStyleName(resources.pageableStyle().headerSortUp(), false);
      headerCell.setStyleName(resources.pageableStyle().headerSortDown(), false);
    });
  }

  private void fireSortChanged(final Sort sort) {
    this.sort = ObjectUtils.defaultIfNull(sort, Sort.unsorted());
    final PageMetadata currentPageable = pageable.getValue();
    final Pageable newPageable =
        PageRequest.of((int) currentPageable.number(), (int) currentPageable.size(), this.sort);
    headerMap.forEach((key, headerCell) -> {
      final Order order = this.sort.getOrderFor(key);
      headerCell.setStyleName(resources.pageableStyle().headerSortUp(),
          order != null && order.isAscending());
      headerCell.setStyleName(resources.pageableStyle().headerSortDown(),
          order != null && order.isDescending());
    });
    ValueChangeEvent.fire(this, newPageable);
  }

  public void setParent(final HandlesSelectedEntry<T> parent) {
    this.parent = parent;
  }

  private PagedModel<T> createEmptyValue() {
    return new PagedModel<>(Page.empty());
  }
}

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.mtwidgets.client.ui.request;

import de.knightsoftnet.mtwidgets.client.ui.request.DeleteRequestPresenter.MyView;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Widget;
import com.google.web.bindery.event.shared.EventBus;
import com.gwtplatform.mvp.client.PopupViewImpl;

import jakarta.inject.Inject;

/**
 * View Implementation for the machine delete confirmation.
 *
 * @author Manfred Tremmel
 *
 */
public class DeleteRequestViewGwtImpl extends PopupViewImpl implements MyView {

  interface Binder extends UiBinder<Widget, DeleteRequestViewGwtImpl> {
  }

  private DeleteRequestPresenter presenter;

  /**
   * constructor with injected parameters.
   *
   * @param uiBinder ui binder
   * @param eventBus event bus
   */
  @Inject
  public DeleteRequestViewGwtImpl(final Binder uiBinder, final EventBus eventBus) {
    super(eventBus);
    initWidget(uiBinder.createAndBindUi(this));
  }

  @Override
  public final void setPresenter(final DeleteRequestPresenter presenter) {
    this.presenter = presenter;
  }

  @UiHandler("deleteButton")
  public void delete(final ClickEvent event) {
    presenter.confirmDeletion();
  }

  @UiHandler("cancleButton")
  public void cancle(final ClickEvent event) {
    presenter.cancleDeletion();
  }
}

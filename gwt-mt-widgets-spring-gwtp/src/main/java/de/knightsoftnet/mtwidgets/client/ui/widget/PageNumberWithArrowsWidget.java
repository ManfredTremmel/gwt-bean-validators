/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.mtwidgets.client.ui.widget;

import de.knightsoftnet.validators.client.decorators.ExtendedValueBoxEditor;
import de.knightsoftnet.validators.client.editor.ValueBoxEditor;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.Widget;

import org.gwtproject.editor.client.Editor;
import org.gwtproject.editor.client.IsEditor;
import org.gwtproject.editor.client.TakesValue;
import org.springframework.data.web.PagedModel.PageMetadata;

/**
 * Input widget for page numbers used for pagination.
 *
 * @author Manfred Tremmel
 *
 */
public class PageNumberWithArrowsWidget extends Composite implements Editor<PageMetadata>,
    IsEditor<ValueBoxEditor<PageMetadata>>, HasValue<PageMetadata>, TakesValue<PageMetadata> {

  interface Binder extends UiBinder<Widget, PageNumberWithArrowsWidget> {
  }

  private static Binder uiBinder = GWT.create(Binder.class);

  @Ignore
  @UiField
  Anchor paginationPrev;

  @Ignore
  @UiField
  PageNumberListBox pageNumber;

  @Ignore
  @UiField
  Anchor paginationNext;

  private PageMetadata value;
  private boolean valueChangeHandlerInitialized;
  private final ValueBoxEditor<PageMetadata> editor;

  /**
   * constructor.
   */
  public PageNumberWithArrowsWidget() {
    super();
    editor = new ExtendedValueBoxEditor<>(this, null);
    initWidget(uiBinder.createAndBindUi(this));
    pageNumber.addValueChangeHandler(value -> setValue(newPageMetadata(value.getValue())));
  }

  private PageMetadata newPageMetadata(final Integer newPage) {
    if (value == null) {
      return value;
    }
    return new PageMetadata(value.size(), newPage == null ? 0L : newPage.longValue(),
        value.totalElements(), value.totalPages());
  }

  @Override
  public final void setValue(final PageMetadata newValue) {
    setValue(newValue, false);
  }

  @Override
  public final void setValue(final PageMetadata value, final boolean fireEvents) {
    this.value = value;
    pageNumber.setNumPages(Integer.valueOf((int) this.value.totalPages()));
    pageNumber.setValue(Integer.valueOf((int) this.value.number()), fireEvents);
    paginationPrev.setEnabled(this.value.number() > 0L);
    paginationPrev.getElement().getStyle().setOpacity(paginationPrev.isEnabled() ? 1.0 : 0.4);
    paginationNext.setEnabled(this.value.number() < this.value.totalPages() - 1L);
    paginationNext.getElement().getStyle().setOpacity(paginationNext.isEnabled() ? 1.0 : 0.4);
  }

  @Override
  public PageMetadata getValue() {
    return value;
  }

  @Override
  public HandlerRegistration addValueChangeHandler(final ValueChangeHandler<PageMetadata> handler) {
    if (!valueChangeHandlerInitialized) {
      ensureDomEventHandlers();
      valueChangeHandlerInitialized = true;
    }
    return this.addHandler(handler, ValueChangeEvent.getType());
  }

  protected void ensureDomEventHandlers() {
    final ValueChangeHandler<Integer> changeHandler =
        event -> ValueChangeEvent.fire(this, getValue());
    pageNumber.addValueChangeHandler(changeHandler);
  }

  /**
   * pressed minus button.
   *
   * @param event click event
   */
  @UiHandler("paginationPrev")
  public void pressedMinusButton(final ClickEvent event) {
    final Integer newValue;
    if (pageNumber.getValue() == null) {
      newValue = Integer.valueOf(0);
    } else {
      newValue = Integer.valueOf(pageNumber.getValue().intValue() - 1);
    }
    setValue(newPageMetadata(newValue), true);
  }

  /**
   * pressed plus button.
   *
   * @param event click event
   */
  @UiHandler("paginationNext")
  public void pressedPlusButton(final ClickEvent event) {
    final Integer newValue;
    if (pageNumber.getValue() == null) {
      newValue = Integer.valueOf((int) value.totalPages());
    } else {
      newValue = Integer.valueOf(pageNumber.getValue().intValue() + 1);
    }
    setValue(newPageMetadata(newValue), true);
  }

  @Override
  public ValueBoxEditor<PageMetadata> asEditor() {
    return editor;
  }
}

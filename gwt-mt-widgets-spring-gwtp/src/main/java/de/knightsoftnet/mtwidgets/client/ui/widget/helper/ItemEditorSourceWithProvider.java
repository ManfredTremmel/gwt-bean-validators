/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.mtwidgets.client.ui.widget.helper;

import jakarta.inject.Provider;

/**
 * Implementation of DataSource for a ListItem editor using provider.
 *
 * @author Manfred Tremmel
 *
 * @param <D> type of data to edit
 * @param <V> type of the view of a single item
 */
public class ItemEditorSourceWithProvider<D, V extends AbstractListItemView<D>>
    extends AbstractItemEditorSource<D, V> {

  private final Provider<V> itemViewProvider;

  /**
   * constructor.
   *
   * @param listEditor the list editor which uses this sourcer
   * @param itemViewProvider provider to create new view items
   */
  public ItemEditorSourceWithProvider(final AbstractListEditor<D, V> listEditor,
      final Provider<V> itemViewProvider) {
    super(listEditor);
    this.itemViewProvider = itemViewProvider;
  }

  /**
   * create new instance of a item view.
   *
   * @param index of the item
   * @return item view
   */
  @Override
  protected V createItemView(final int index) {
    return itemViewProvider.get();
  }
}

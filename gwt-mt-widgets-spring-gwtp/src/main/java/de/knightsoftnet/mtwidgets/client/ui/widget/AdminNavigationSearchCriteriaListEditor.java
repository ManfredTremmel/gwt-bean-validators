/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.mtwidgets.client.ui.widget;

import de.knightsoftnet.gwtp.spring.shared.search.SearchCriteriaSearch;
import de.knightsoftnet.gwtp.spring.shared.search.SearchFieldDefinition;
import de.knightsoftnet.mtwidgets.client.ui.widget.helper.AbstractItemEditorSource;
import de.knightsoftnet.mtwidgets.client.ui.widget.helper.AbstractListEditor;
import de.knightsoftnet.validators.client.editor.impl.ListValidationEditor;

import org.apache.commons.lang3.StringUtils;
import org.gwtproject.editor.client.SimpleBeanEditorDriver;
import org.gwtproject.editor.client.annotation.IsDriver;

import java.util.Collection;
import java.util.List;

/**
 * editor to show a list of usage field entries.
 *
 * @author Manfred Tremmel
 */
public class AdminNavigationSearchCriteriaListEditor
    extends AbstractListEditor<SearchCriteriaSearch, AdminNavigationSearchCriteria> {

  /**
   * interface of the driver to combine ui and bean.
   */
  @IsDriver
  interface Driver extends SimpleBeanEditorDriver<List<SearchCriteriaSearch>, //
      ListValidationEditor<SearchCriteriaSearch, AdminNavigationSearchCriteria>> {
  }

  private static Driver driver = new AdminNavigationSearchCriteriaListEditor_Driver_Impl();

  private Collection<SearchFieldDefinition> searchFieldDefinitions;

  private class AdminNavigationSearchCriteriaEditorSource
      extends AbstractItemEditorSource<SearchCriteriaSearch, AdminNavigationSearchCriteria> {

    public AdminNavigationSearchCriteriaEditorSource() {
      super(AdminNavigationSearchCriteriaListEditor.this);
    }

    @Override
    protected AdminNavigationSearchCriteria createItemView(final int index) {
      return new AdminNavigationSearchCriteria(index, searchFieldDefinitions);
    }
  }

  private final ListValidationEditor<SearchCriteriaSearch, //
      AdminNavigationSearchCriteria> editor =
          ListValidationEditor.of(new AdminNavigationSearchCriteriaEditorSource());

  /**
   * constructor.
   *
   */
  public AdminNavigationSearchCriteriaListEditor() {
    super();
    driver.initialize(editor);
  }

  @Override
  public ListValidationEditor<SearchCriteriaSearch, //
      AdminNavigationSearchCriteria> asEditor() {
    return editor;
  }

  @Override
  protected SearchCriteriaSearch createData() {
    final SearchFieldDefinition firstSearchField = searchFieldDefinitions.iterator().next();
    return new SearchCriteriaSearch(firstSearchField,
        firstSearchField.getAllowedSearchOperations().iterator().next(), StringUtils.EMPTY);
  }

  public void fillSearchFieldDefinition(
      final Collection<SearchFieldDefinition> searchFieldDefinitions) {
    this.searchFieldDefinitions = searchFieldDefinitions;
  }
}

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.mtwidgets.client.ui.page.admin;

import de.knightsoftnet.gwtp.spring.client.rest.helper.AbstractViewWithErrorHandling;
import de.knightsoftnet.gwtp.spring.shared.data.AdminNavigation;
import de.knightsoftnet.mtwidgets.client.ui.page.admin.AbstractAdminPresenter.MyViewDef;
import de.knightsoftnet.mtwidgets.client.ui.widget.AdminNavigationWidget;
import de.knightsoftnet.validators.client.editor.BeanValidationEditorDriver;
import de.knightsoftnet.validators.client.event.FormSubmitEvent;

import com.google.gwt.uibinder.client.UiFactory;
import com.google.gwt.uibinder.client.UiField;
import com.gwtplatform.mvp.client.proxy.Proxy;

import org.springframework.data.domain.Persistable;
import org.springframework.data.web.PagedModel;

import jakarta.inject.Provider;

/**
 * abstract view of a admin view.
 *
 * @param <T> Type of the entity to edit
 * @param <P> Proxy object type
 * @param <V> View interface
 * @author Manfred Tremmel
 *
 */
public abstract class AbstractAdminView<T extends Persistable<Long>, //
    P extends Proxy<?>, V extends MyViewDef<T, P, V>>
    extends AbstractViewWithErrorHandling<AbstractAdminPresenter<T, P, V>, T>
    implements MyViewDef<T, P, V> {

  @Ignore
  @UiField
  public AdminNavigationWidget<T> adminNavigation;

  private final Provider<AdminNavigationWidget<T>> adminNavigationProvider;

  /**
   * constructor with injected parameters.
   *
   * @param driver editor driver
   * @param adminNavigationProvider administration navigation widget provider
   */
  protected AbstractAdminView(final BeanValidationEditorDriver<T, //
      ? extends AbstractAdminView<T, P, V>> driver,
      final Provider<AdminNavigationWidget<T>> adminNavigationProvider) {
    super(driver);
    this.adminNavigationProvider = adminNavigationProvider;
  }

  @Ignore
  @UiFactory
  public AdminNavigationWidget<T> buildAdminNavigationWidget() {
    return adminNavigationProvider.get();
  }

  @Override
  public void setPresenter(final AbstractAdminPresenter<T, P, V> presenter) {
    super.setPresenter(presenter);
    adminNavigation.setPresenter(this.presenter);
  }

  @Override
  public final void showMessage(final String message) {
    adminNavigation.showMessage(message);
  }

  @Override
  public void onFormSubmit(final FormSubmitEvent<T> entry) {
    getPresenter().updateEntry(entry.getValue());
  }

  @Override
  public void displayNavigtion(final AdminNavigation<Long> navigation) {
    adminNavigation.setValue(navigation);
  }

  @Override
  public void showSearchResult(final PagedModel<T> result, final boolean resetSorting) {
    adminNavigation.displaySearchResult(result, resetSorting);
  }
}

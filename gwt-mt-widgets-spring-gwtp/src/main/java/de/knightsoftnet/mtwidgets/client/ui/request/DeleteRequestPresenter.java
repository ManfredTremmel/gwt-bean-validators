/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.mtwidgets.client.ui.request;

import de.knightsoftnet.mtwidgets.client.ui.request.DeleteRequestPresenter.MyView;
import de.knightsoftnet.mtwidgets.client.ui.widget.features.HasDelete;

import com.google.gwt.user.client.Window;
import com.google.web.bindery.event.shared.EventBus;
import com.gwtplatform.mvp.client.PopupView;
import com.gwtplatform.mvp.client.PresenterWidget;

import jakarta.inject.Inject;

/**
 * Presenter for machine deletion request confirmation.
 *
 * @author Manfred Tremmel
 *
 */
public class DeleteRequestPresenter extends PresenterWidget<MyView> {

  public interface MyView extends PopupView {
    void setPresenter(final DeleteRequestPresenter ppresenter);
  }

  private HasDelete myHasDeletePresenter;

  /**
   * constructor injecting parameters.
   *
   * @param eventBus event bus
   * @param view view of the page
   */
  @Inject
  public DeleteRequestPresenter(final EventBus eventBus, final MyView view) {
    super(eventBus, view);
    view.setPresenter(this);
  }

  @Override
  protected void onHide() {
    super.onHide();
    Window.enableScrolling(true);
  }

  public void setHasDeletePresenter(final HasDelete myHasDeletePresenter) {
    this.myHasDeletePresenter = myHasDeletePresenter;
  }

  public void confirmDeletion() {
    myHasDeletePresenter.deleteEntry();
    removeFromParentSlot();
  }

  public void cancleDeletion() {
    removeFromParentSlot();
  }
}

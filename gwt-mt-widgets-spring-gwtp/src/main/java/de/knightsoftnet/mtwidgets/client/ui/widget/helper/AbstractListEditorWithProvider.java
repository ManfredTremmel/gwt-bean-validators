/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.mtwidgets.client.ui.widget.helper;

import jakarta.inject.Provider;

/**
 * editor to edit a list of item entries using data provider.
 *
 * @author Manfred Tremmel
 *
 * @param <D> type of item data to edit
 * @param <V> type of view of the single items
 */
public abstract class AbstractListEditorWithProvider<D, V extends AbstractListItemView<D>>
    extends AbstractListEditor<D, V> {

  private final Provider<D> dataProvider;

  public AbstractListEditorWithProvider(final Provider<D> dataProvider) {
    super();
    this.dataProvider = dataProvider;
  }

  @Override
  protected D createData() {
    return dataProvider.get();
  }
}

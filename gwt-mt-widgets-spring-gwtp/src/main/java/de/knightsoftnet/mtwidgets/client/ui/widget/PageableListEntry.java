/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.mtwidgets.client.ui.widget;

import de.knightsoftnet.gwtp.spring.shared.search.TableFieldDefinition;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;

import org.gwtproject.editor.client.LeafValueEditor;

import java.util.Collection;

/**
 * pageable list entry.
 *
 * @author Manfred Tremmel
 *
 * @param <T> type of the entry
 */
public class PageableListEntry<T> extends Composite
    implements LeafValueEditor<T>, HasClickHandlers {

  @SuppressWarnings("rawtypes")
  interface Binder extends UiBinder<Widget, PageableListEntry> {
  }

  private final Binder uiBinder = GWT.create(Binder.class);

  @Ignore
  @UiField
  HTMLPanel row;

  private final Collection<TableFieldDefinition<T>> tableFieldDefinitions;
  private T value;

  /**
   * constructor.
   *
   * @param tableFieldDefinitions collection of table field definitions
   */
  public PageableListEntry(final Collection<TableFieldDefinition<T>> tableFieldDefinitions) {
    super();
    this.tableFieldDefinitions = tableFieldDefinitions;
    initWidget(uiBinder.createAndBindUi(this));
  }

  @Override
  public void setValue(final T value) {
    this.value = value;
    renderHtml(this.value);
  }

  private void renderHtml(final T value) {
    tableFieldDefinitions.forEach(entry -> {
      final HTMLPanel cell = new HTMLPanel(entry.getValueConverter().convert(value));
      entry.getStyles().forEach(style -> cell.addStyleName(style));
      row.add(cell);
    });
  }

  @Override
  public T getValue() {
    return value;
  }

  @Override
  public HandlerRegistration addClickHandler(final ClickHandler handler) {
    return addDomHandler(handler, ClickEvent.getType());
  }
}

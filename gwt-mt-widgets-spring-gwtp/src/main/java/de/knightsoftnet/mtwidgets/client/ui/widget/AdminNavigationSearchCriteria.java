/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.mtwidgets.client.ui.widget;

import de.knightsoftnet.gwtp.spring.shared.search.SearchCriteriaSearch;
import de.knightsoftnet.gwtp.spring.shared.search.SearchFieldDefinition;
import de.knightsoftnet.mtwidgets.client.ui.widget.helper.AbstractListItemView;
import de.knightsoftnet.mtwidgets.client.ui.widget.helper.IdAndNameBean;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.LabelElement;
import com.google.gwt.dom.client.Style.Display;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Widget;

import org.springframework.util.CollectionUtils;

import java.util.Collection;
import java.util.Collections;
import java.util.stream.Collectors;

public class AdminNavigationSearchCriteria extends AbstractListItemView<SearchCriteriaSearch> {

  interface Binder extends UiBinder<Widget, AdminNavigationSearchCriteria> {
  }

  /**
   * A ClientBundle that provides images and style sheets for the decorator.
   */
  public interface Resources extends ClientBundle {
  }

  private static Binder uiBinder = GWT.create(Binder.class);
  private final String keyId;
  private final String operationId;
  private final String valueId;
  private final String valueDataListId;

  @UiField(provided = true)
  SearchFieldListBox key;
  @UiField(provided = true)
  SearchOperationListBox operation;
  @UiField(provided = true)
  DynamicInputWidget value;
  @Ignore
  @UiField(provided = true)
  IdAndNameListBox<String> valueDataList;

  @Ignore
  @UiField
  Element valueLabel;

  @UiField
  Button removeEntry;

  /**
   * constructor.
   *
   * @param pos position number of the line
   * @param searchFieldDefinitions collection of field definitions
   */
  public AdminNavigationSearchCriteria(final int pos,
      final Collection<SearchFieldDefinition> searchFieldDefinitions) {
    super();

    keyId = "key_" + pos;
    operationId = "operation_" + pos;
    valueId = "value_" + pos;
    valueDataListId = "value_select_" + pos;

    key = new SearchFieldListBox();
    key.ensureDebugId(keyId);
    key.fillSearchFieldDefinition(searchFieldDefinitions);
    operation = new SearchOperationListBox();
    operation.ensureDebugId(operationId);
    value = new DynamicInputWidget();
    value.ensureDebugId(valueId);
    valueDataList = new IdAndNameListBox<String>();
    valueDataList.ensureDebugId(valueDataListId);
    valueDataList.getElement().getStyle().setDisplay(Display.NONE);

    initWidget(uiBinder.createAndBindUi(this));
    ((LabelElement) valueLabel).setHtmlFor(valueId);

    if (pos == 0) {
      removeEntry.getElement().getStyle().setDisplay(Display.NONE);
    }

    if (!CollectionUtils.isEmpty(searchFieldDefinitions)) {
      final SearchFieldDefinition fieldDefinition = searchFieldDefinitions.iterator().next();
      key.setValue(fieldDefinition);
      searchFieldChanged(fieldDefinition);
    }
  }

  /**
   * search field changed.
   *
   * @param event value change handler
   */
  @UiHandler("key")
  public void searchFieldChanged(final ValueChangeEvent<SearchFieldDefinition> event) {
    searchFieldChanged(event.getValue());
    if (CollectionUtils.isEmpty(event.getValue().getLocalizedValues())) {
      valueDataList.fillEntryCollections(Collections.emptyList());
      valueDataList.getElement().getStyle().setDisplay(Display.NONE);
      value.setValue(null);
      value.getElement().getStyle().setDisplay(Display.INITIAL);
    } else {
      valueDataList.fillEntryCollections(event.getValue().getLocalizedValues().stream()
          .map(pair -> new IdAndNameBean<String>(pair.getLeft(), pair.getRight()))
          .collect(Collectors.toList()));
      valueDataList.getElement().getStyle().setDisplay(Display.INITIAL);
      valueDataList.setValue(event.getValue().getLocalizedValues().iterator().next().getLeft());
      value.setValue(valueDataList.getValue());
      value.getElement().getStyle().setDisplay(Display.NONE);
    }
  }

  private void searchFieldChanged(final SearchFieldDefinition searchFieldDefinition) {
    operation.fillEntries(searchFieldDefinition.getAllowedSearchOperations());
    value.setFieldType(searchFieldDefinition.getFieldType(), true);
  }

  @UiHandler("removeEntry")
  public void removeEntryClicked(final ClickEvent event) {
    removeThisEntry();
  }

  @UiHandler("valueDataList")
  public void valueDataListChanged(final ValueChangeEvent<String> event) {
    value.setValue(event.getValue());
  }
}

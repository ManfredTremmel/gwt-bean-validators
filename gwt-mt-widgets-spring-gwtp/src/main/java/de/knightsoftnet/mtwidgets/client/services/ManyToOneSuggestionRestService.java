/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.mtwidgets.client.services;

import java.util.List;

/**
 * interface for many to one reference suggestions rest reads.
 *
 * @param <T> type of entity
 */
public interface ManyToOneSuggestionRestService<T> {

  /**
   * read on entry with given key.
   *
   * @param key to read with
   * @return entity
   */
  T getReferenceByKey(final String key);

  /**
   * read list of entities matching the key.
   *
   * @param key to read with
   * @return list of entities
   */
  List<T> findByKeyContaining(final String key);
}

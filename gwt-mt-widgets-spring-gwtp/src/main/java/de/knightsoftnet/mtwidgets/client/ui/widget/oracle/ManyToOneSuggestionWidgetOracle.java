/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.mtwidgets.client.ui.widget.oracle;

import de.knightsoftnet.gwtp.spring.client.rest.helper.RestCallbackBuilder;
import de.knightsoftnet.mtwidgets.client.services.ManyToOneSuggestionRestService;

import com.google.gwt.user.client.ui.SuggestOracle;
import com.gwtplatform.dispatch.rest.delegates.client.ResourceDelegate;

import org.gwtproject.core.shared.GWT;

import java.util.List;
import java.util.stream.Collectors;

/**
 * key value suggestion widget oracle.
 *
 * @param <E> entity type
 * @param <S> suggestion for entity
 * @param <T> rest service type
 */
public class ManyToOneSuggestionWidgetOracle<E, S extends AbstractManyToOneSuggestion<E>, //
    T extends ManyToOneSuggestionRestService<E>> extends SuggestOracle {

  private final ResourceDelegate<T> restDelegate;
  private final S suggestionPrototype;

  /**
   * constructor.
   *
   * @param restDelegate rest delegate
   * @param suggestionPrototype suggestion prototype
   */
  public ManyToOneSuggestionWidgetOracle(ResourceDelegate<T> restDelegate, S suggestionPrototype) {
    super();
    this.restDelegate = restDelegate;
    this.suggestionPrototype = suggestionPrototype;
  }

  @Override
  public final boolean isDisplayStringHTML() {
    return true;
  }

  @Override
  public final void requestSuggestions(final Request request, final Callback callback) {
    restDelegate.withCallback(RestCallbackBuilder.<List<E>>build(

        success -> {
          callback.onSuggestionsReady(request,
              new Response(success.stream().map(entity -> suggestionPrototype.of(entity, request))
                  .collect(Collectors.toList())));
        },

        failure -> {
          GWT.log(failure.getMessage());
        }

    )).findByKeyContaining(request.getQuery());
  }
}

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.mtwidgets.client.ui.widget;

import de.knightsoftnet.gwtp.spring.shared.search.SearchFieldDefinition;
import de.knightsoftnet.gwtp.spring.shared.search.SearchRequest;
import de.knightsoftnet.validators.client.editor.BeanValidationEditorDriver;
import de.knightsoftnet.validators.client.editor.annotation.IsValidationDriver;
import de.knightsoftnet.validators.client.event.FormSubmitEvent;
import de.knightsoftnet.validators.client.event.FormSubmitHandler;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;

import org.gwtproject.editor.client.Editor;

import java.util.Collection;

public class AdminNavigationSearch extends Composite
    implements Editor<SearchRequest>, FormSubmitHandler<SearchRequest>, HasValue<SearchRequest> {

  interface Binder extends UiBinder<Widget, AdminNavigationSearch> {
  }

  @IsValidationDriver(submitUnchanged = true, forceUsingGetter = true)
  interface Driver extends BeanValidationEditorDriver<SearchRequest, AdminNavigationSearch> {
  }

  @UiField
  AdminNavigationSearchCriteriaListEditor searchCriterias;

  @UiField
  Button searchButton;

  private final Driver driver;
  private SearchRequest formData;

  @Inject
  AdminNavigationSearch(final AdminNavigationSearch_Driver_Impl driver, final Binder uiBinder) {
    super();
    this.driver = driver;
    initWidget(uiBinder.createAndBindUi(this));
    driver.initialize(this);
    driver.setSubmitButton(searchButton);
    driver.addFormSubmitHandler(this);
    searchCriterias.setParentDriver(driver);
    formData = new SearchRequest();
  }

  @Override
  public void onFormSubmit(final FormSubmitEvent<SearchRequest> event) {
    ValueChangeEvent.fire(this, event.getValue());
  }

  @Override
  public HandlerRegistration addValueChangeHandler(
      final ValueChangeHandler<SearchRequest> handler) {
    return this.addHandler(handler, ValueChangeEvent.getType());
  }

  @Override
  public SearchRequest getValue() {
    return formData;
  }

  @Override
  public void setValue(final SearchRequest value) {
    setValue(value, false);
  }

  @Override
  public void setValue(final SearchRequest value, final boolean fireEvents) {
    final SearchRequest oldValue = getValue();
    formData = value;
    driver.edit(value);
    ValueChangeEvent.fireIfNotEqual(this, oldValue, value);
  }

  /**
   * fill search field definitions with possible search operations.
   */
  public void fillSearchFieldDefinition(
      final Collection<SearchFieldDefinition> searchFieldDefinitions) {
    searchCriterias.fillSearchFieldDefinition(searchFieldDefinitions);
    driver.edit(formData);
    searchCriterias.addNewEntry();
  }

  @UiHandler("addSearchCriteria")
  public void onClickAddSearchCriteria(final ClickEvent event) {
    searchCriterias.addNewEntry();
  }
}

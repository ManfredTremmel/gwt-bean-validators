/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.mtwidgets.client.ui.page.admin;

import de.knightsoftnet.gwtp.spring.client.rest.helper.EditorWithErrorHandling;
import de.knightsoftnet.gwtp.spring.client.rest.helper.HttpMessages;
import de.knightsoftnet.gwtp.spring.client.rest.helper.RestCallbackBuilder;
import de.knightsoftnet.gwtp.spring.client.session.Session;
import de.knightsoftnet.gwtp.spring.shared.data.AdminNavigation;
import de.knightsoftnet.gwtp.spring.shared.data.AdminResult;
import de.knightsoftnet.gwtp.spring.shared.search.SearchFieldDefinition;
import de.knightsoftnet.gwtp.spring.shared.search.TableFieldDefinition;
import de.knightsoftnet.mtwidgets.client.services.AdminService;
import de.knightsoftnet.mtwidgets.client.ui.page.admin.AbstractAdminPresenter.MyViewDef;
import de.knightsoftnet.mtwidgets.client.ui.request.DeleteRequestPresenter;
import de.knightsoftnet.mtwidgets.client.ui.widget.features.HasDelete;
import de.knightsoftnet.navigation.client.ui.basepage.AbstractBasePagePresenter;
import de.knightsoftnet.validators.client.event.FormSubmitHandler;
import de.knightsoftnet.validators.shared.Parameters;

import com.google.gwt.user.client.Window;
import com.google.web.bindery.event.shared.EventBus;
import com.gwtplatform.dispatch.rest.delegates.client.ResourceDelegate;
import com.gwtplatform.mvp.client.Presenter;
import com.gwtplatform.mvp.client.proxy.Proxy;
import com.gwtplatform.mvp.shared.proxy.PlaceRequest;

import org.apache.commons.lang3.ObjectUtils;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Persistable;
import org.springframework.data.web.PagedModel;

import java.util.Collection;

/**
 * Activity/Presenter of the administration page, implementation.
 *
 * @param <T> Type of the entity to edit
 * @param <P> Proxy object
 * @param <V> View interface
 * @author Manfred Tremmel
 *
 */
public abstract class AbstractAdminPresenter<T extends Persistable<Long>, //
    P extends Proxy<?>, V extends MyViewDef<T, P, V>> extends Presenter<V, P> implements HasDelete {

  public interface MyViewDef<T extends Persistable<Long>, P extends Proxy<?>, //
      V extends MyViewDef<T, P, V>>
      extends EditorWithErrorHandling<AbstractAdminPresenter<T, P, V>, T>, FormSubmitHandler<T> {
    void displayNavigtion(AdminNavigation<Long> navigation);

    void showSearchResult(PagedModel<T> result, boolean resetSort);
  }

  protected final Session session;
  protected final ResourceDelegate<? extends AdminService<T>> service;

  protected final AdminResult<Long, T> entry;
  private final DeleteRequestPresenter deleteRequestPresenter;

  private Long idToDelete;

  private final Collection<SearchFieldDefinition> searchFields;
  private final Collection<TableFieldDefinition<T>> searchListFields;

  /**
   * constructor injecting needed data.
   */
  protected AbstractAdminPresenter(final EventBus eventBus, final V view, final P proxy,
      final ResourceDelegate<? extends AdminService<T>> service, final Session session,
      final DeleteRequestPresenter deleteRequestPresenter,
      final Collection<SearchFieldDefinition> searchFields,
      final Collection<TableFieldDefinition<T>> searchListFields) {
    super(eventBus, view, proxy, AbstractBasePagePresenter.SLOT_MAIN_CONTENT);
    this.session = session;
    this.service = service;
    this.deleteRequestPresenter = deleteRequestPresenter;
    this.searchFields = searchFields;
    this.searchListFields = searchListFields;
    getView().setPresenter(this);
    this.deleteRequestPresenter.setHasDeletePresenter(this);
    entry = new AdminResult<>();
  }

  @Override
  public void prepareFromRequest(final PlaceRequest request) {
    super.prepareFromRequest(request);
    final String idString = request.getParameter(Parameters.ID, null);
    readEntry(idString == null ? null : Long.valueOf(idString));
  }

  /**
   * read one entry.
   */
  public void readEntry(final Long id) {
    if (id == null) {
      service
          .withCallback(RestCallbackBuilder
              .<AbstractAdminPresenter<T, P, V>, T, V, AdminResult<Long, T>, HttpMessages>build(
                  getView(), entry.getEntry(), session, result -> displayEntry(result, true)))
          .getEntry();
    } else {
      service
          .withCallback(RestCallbackBuilder
              .<AbstractAdminPresenter<T, P, V>, T, V, AdminResult<Long, T>, HttpMessages>build(
                  getView(), entry.getEntry(), session, result -> displayEntry(result, true)))
          .getEntryById(id);
    }
  }

  public void newEntry() {
    displayEntry(new AdminResult<>(entry.getNavigation().getFirstId(), null, null,
        entry.getNavigation().getLastId(), createNewEntry()), true);
  }

  protected abstract T createNewEntry();

  private void displayEntry(final AdminResult<Long, T> displayEntry, final boolean cleanMessage) {
    entry.setEntry(displayEntry.getEntry());
    entry.setNavigation(displayEntry.getNavigation());
    getView().fillForm(displayEntry.getEntry());
    getView().displayNavigtion(displayEntry.getNavigation());
    if (cleanMessage) {
      getView().showMessage(null);
    }
  }

  protected void displayChangedEntry(final AdminResult<Long, T> displayEntry) {
    displayEntry(displayEntry, false);
  }

  /**
   * update one entry.
   */
  public void updateEntry(final T updateEntry) {
    if (updateEntry.isNew()) {
      service
          .withCallback(RestCallbackBuilder
              .<AbstractAdminPresenter<T, P, V>, T, V, AdminResult<Long, T>, HttpMessages>build(
                  getView(), updateEntry, session, result -> displayChangedEntry(result)))
          .createEntry(updateEntry);
    } else {
      service
          .withCallback(RestCallbackBuilder
              .<AbstractAdminPresenter<T, P, V>, T, V, AdminResult<Long, T>, HttpMessages>build(
                  getView(), updateEntry, session, result -> displayChangedEntry(result)))
          .changeEntry(updateEntry.getId(), updateEntry);
    }
  }

  /**
   * delete one entry.
   */
  public void deleteEntry(final Long entryId) {
    idToDelete = entryId;
    Window.scrollTo(0, 0);
    this.addToPopupSlot(deleteRequestPresenter);
  }

  @Override
  public void deleteEntry() {
    service
        .withCallback(RestCallbackBuilder
            .<AbstractAdminPresenter<T, P, V>, T, V, AdminResult<Long, T>, HttpMessages>build(
                getView(), entry.getEntry(), session, result -> displayChangedEntry(result)))
        .deleteEntry(idToDelete);
  }

  /**
   * search for entries.
   *
   * @param search search values
   * @param pageable paging and sorting
   */
  public void search(final String search, final Pageable pageable) {
    final Pageable pageableInternal = ObjectUtils.defaultIfNull(pageable, Pageable.unpaged());
    if (pageableInternal.getSort() == null
        || "UNSORTED".equalsIgnoreCase(pageableInternal.getSort().toString())) {
      service.withCallback(RestCallbackBuilder.<PagedModel<T>>build(

          result -> displaySearchResult(result, false),
          caught -> getView().showMessage(caught.toString())))
          .searchPaging(session.getUsersLocale(), search, pageableInternal.getPageNumber(),
              pageableInternal.getPageSize());
    } else {
      service.withCallback(RestCallbackBuilder.<PagedModel<T>>build(

          result -> displaySearchResult(result, false),
          caught -> getView().showMessage(caught.toString())))
          .searchPagingAndSorting(session.getUsersLocale(), search,
              pageableInternal.getPageNumber(), pageableInternal.getPageSize(),
              pageableInternal.getSort().toString().replace(": ", ","));
    }
  }

  /**
   * search for entries.
   *
   * @param search search values
   */
  public void search(final String search) {
    service.withCallback(RestCallbackBuilder.<PagedModel<T>>build(

        result -> displaySearchResult(result, true),
        caught -> getView().showMessage(caught.toString())))
        .search(session.getUsersLocale(), search);
  }

  /**
   * display search results.
   *
   * @param result page object with search results
   * @param clearSort if true, sort is reseted
   */
  public void displaySearchResult(final PagedModel<T> result, final boolean clearSort) {
    getView().showSearchResult(result, clearSort);
    if (result != null && result.getMetadata() != null
        && result.getMetadata().totalElements() >= 1) {
      readEntry(result.getContent().get(0).getId());
    }
  }

  public Collection<SearchFieldDefinition> getSearchFieldDefinitions() {
    return searchFields;
  }

  public Collection<TableFieldDefinition<T>> getSearchResultDefinitions() {
    return searchListFields;
  }
}

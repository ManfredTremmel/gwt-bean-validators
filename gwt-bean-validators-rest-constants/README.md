gwt-bean-validators-rest-constants
==================================

Little helper package, contains some constants which are used for rest communication.
You don't need to include in your project it's included by other packages.

Maven integraten
----------------

The dependency itself for GWT-Projects:

```xml
    <dependency>
      <groupId>de.knightsoft-net</groupId>
      <artifactId>gwt-bean-validators-rest-constants</artifactId>
      <version>2.4.1</version>
    </dependency>
```

GWT Integration
---------------

```xml
<inherits name="de.knightsoftnet.validators.GwtBeanValidatorsRestConstants" />
```

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.mtwidgets.client.ui.widget;

import de.knightsoftnet.mtwidgets.client.ui.handler.HandlerFactory;
import de.knightsoftnet.mtwidgets.client.ui.widget.oracle.BicOracle;
import de.knightsoftnet.validators.shared.data.BicMapSharedConstants;
import de.knightsoftnet.validators.shared.impl.BicValidator;
import de.knightsoftnet.validators.shared.util.HasSetBicMapSharedConstants;
import de.knightsoftnet.validators.shared.util.IbanUtil;

import com.google.gwt.user.client.ui.HasText;

import org.apache.commons.lang3.StringUtils;

/**
 * BIC suggest widget.
 *
 * @author Manfred Tremmel
 *
 */
public class BicSuggestBox extends SuggestBoxWithEditorErrors
    implements HasSetBicMapSharedConstants {

  /**
   * bank name widget to fill.
   */
  @Ignore
  private HasText bankNameWidget;

  /**
   * provider for map of swift countries and the length of the ibans.
   */
  private BicMapSharedConstants bicMapSharedConstants;

  /**
   * default constructor.
   */
  public BicSuggestBox() {
    super(new BicOracle());
    setWidth("11em");
    getValueBox().addKeyPressHandler(HandlerFactory.getNumericAndUpperAsciiKeyPressHandler());
    final IbanUtil ibanUtil = new IbanUtil();
    ibanUtil.setBicMapSharedConstantsWhenAvailable(this);
    addValueChangeHandler(event -> fillBankName(event.getValue()));
  }

  @Override
  public void setBicMapSharedConstants(final BicMapSharedConstants bicMapSharedConstants) {
    this.bicMapSharedConstants = bicMapSharedConstants;
  }

  @Override
  public void setText(final String text) {
    super.setText(text);
    fillBankName(text);
  }

  @Override
  public void setValue(final String newValue) {
    this.setValue(newValue, false);
  }

  @Override
  public void setValue(final String value, final boolean fireEvents) {
    super.setValue(value, fireEvents);
    fillBankName(value);
  }


  protected final void fillBankName(final String value) {
    if (bankNameWidget != null && bicMapSharedConstants != null) {
      if (bicMapSharedConstants.bics().containsKey(value)) {
        bankNameWidget.setText(bicMapSharedConstants.bics().get(value));
      } else if (bicMapSharedConstants.bics()
          .containsKey(StringUtils.substring(value, 0, BicValidator.BIC_LENGTH_MIN))) {
        bankNameWidget.setText(bicMapSharedConstants.bics()
            .get(StringUtils.substring(value, 0, BicValidator.BIC_LENGTH_MIN)));
      } else {
        bankNameWidget.setText(StringUtils.EMPTY);
      }
    }
  }

  public HasText getBankNameWidget() {
    return bankNameWidget;
  }

  public void setBankNameWidget(final HasText bankNameWidget) {
    this.bankNameWidget = bankNameWidget;
  }
}

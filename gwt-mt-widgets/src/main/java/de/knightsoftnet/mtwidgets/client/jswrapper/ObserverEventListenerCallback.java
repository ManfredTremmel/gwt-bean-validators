package de.knightsoftnet.mtwidgets.client.jswrapper;

import jsinterop.annotations.JsFunction;

@JsFunction
public interface ObserverEventListenerCallback {

  void callEvent(IntersectionObserverEntry[] event);
}

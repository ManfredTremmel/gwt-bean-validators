/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.mtwidgets.client.ui.widget;

import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.uibinder.client.UiConstructor;
import com.google.gwt.user.client.TakesValue;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;

import org.apache.commons.lang3.StringUtils;

import java.util.Collection;

/**
 * data list to provide suggestions.
 *
 * @author Manfred Tremmel
 *
 */
public class DataListWidget extends Composite
    implements TakesValue<Collection<? extends Suggestion>> {

  private final HTMLPanel panel;
  private final HTMLPanel select;
  private Collection<? extends Suggestion> value;

  /**
   * default constructor.
   */
  @UiConstructor
  public DataListWidget() {
    super();
    panel = new HTMLPanel("datalist", StringUtils.EMPTY);
    select = new HTMLPanel("select", StringUtils.EMPTY);
    panel.add(select);
    initWidget(panel);
  }

  @Override
  public void setValue(final Collection<? extends Suggestion> collection) {
    value = collection;
    final SafeHtmlBuilder options = new SafeHtmlBuilder();
    for (final Suggestion entry : collection) {
      if (StringUtils.isEmpty(entry.getDisplayString())) {
        options.appendHtmlConstant("<option value=\"" + entry.getReplacementString() + "\"/>");

      } else {
        options.appendHtmlConstant("<option value=\"" + entry.getReplacementString() + "\" label=\""
            + entry.getDisplayString() + "\"/>");
      }
    }
    select.getElement().setInnerSafeHtml(options.toSafeHtml());
  }

  @Override
  public Collection<? extends Suggestion> getValue() {
    return value;
  }
}

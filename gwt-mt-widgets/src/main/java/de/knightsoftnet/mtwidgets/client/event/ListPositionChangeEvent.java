/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.mtwidgets.client.event;

import de.knightsoftnet.mtwidgets.client.event.ListPositionChangeEvent.ListPositionChangeHandler;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HasHandlers;
import com.google.web.bindery.event.shared.HandlerRegistration;

import org.gwtproject.editor.client.Editor;

/**
 * event which can be thrown by a list editor entry to inform about it's movement.
 *
 * @param <T> type of the Event
 *
 * @author Manfred Tremmel
 *
 */
public class ListPositionChangeEvent<T> extends GwtEvent<ListPositionChangeHandler<T>> {
  private static final Type<ListPositionChangeHandler<?>> TYPE = new Type<>();

  public enum ListPositionEnum {
    UP, // one position up
    DOWN, // one position down
    ABSOLUTE // move to the given position
  }

  public interface ListPositionChangeHandler<T> extends EventHandler {
    void onListPositionChange(ListPositionChangeEvent<T> event);
  }

  public interface ListPositionChangeHandlers<T> extends HasHandlers {
    HandlerRegistration addListPositionChangeHandler(ListPositionChangeHandler<T> handler);
  }

  private final Editor<T> editor;
  private final ListPositionEnum positionChange;
  private final int newPosition;

  public ListPositionChangeEvent(final Editor<T> editor, final ListPositionEnum positionChange) {
    this(editor, positionChange, -1);
  }

  /**
   * constructor.
   *
   * @param editor editor widget in the list
   * @param positionChange enum of change
   * @param newPosition new position for absolute changes
   */
  public ListPositionChangeEvent(final Editor<T> editor, final ListPositionEnum positionChange,
      final int newPosition) {
    super();
    this.editor = editor;
    this.positionChange = positionChange;
    this.newPosition = newPosition;
  }

  public static Type<ListPositionChangeHandler<?>> getType() {
    return TYPE;
  }

  @Override
  protected void dispatch(final ListPositionChangeHandler<T> handler) {
    handler.onListPositionChange(this);
  }

  @SuppressWarnings({"rawtypes", "unchecked"})
  @Override
  public Type<ListPositionChangeHandler<T>> getAssociatedType() {
    return (Type) TYPE;
  }

  public Editor<T> getEditor() {
    return editor;
  }

  public final ListPositionEnum getPositionChange() {
    return positionChange;
  }

  public final int getNewPosition() {
    return newPosition;
  }
}

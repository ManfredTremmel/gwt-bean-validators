package de.knightsoftnet.mtwidgets.client.ui.widget.features;

public interface HasAutocomplete {

  /**
   * get autocomplete attribute value.
   *
   * @return input field autocomplete attribute value
   */
  String getAutocomplete();

  /**
   * set autocomplete attribute value.
   *
   * @param arg input field autocomplete attribute value to set
   */
  void setAutocomplete(String arg);
}

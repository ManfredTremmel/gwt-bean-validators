package de.knightsoftnet.mtwidgets.client.ui.widget.features;

public interface HandlesSelectedEntry<T> {
  void handleSelectedEntry(T entry);
}

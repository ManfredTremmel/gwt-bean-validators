/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.mtwidgets.client.ui.widget;

import de.knightsoftnet.mtwidgets.client.ui.widget.features.HasValidationMessageElement;
import de.knightsoftnet.mtwidgets.client.ui.widget.helper.FeatureCheck;
import de.knightsoftnet.mtwidgets.client.ui.widget.helper.IdAndNamePlusIconBean;
import de.knightsoftnet.mtwidgets.client.ui.widget.styling.SelectBoxInputStyle;
import de.knightsoftnet.validators.client.decorators.ExtendedValueBoxEditor;
import de.knightsoftnet.validators.client.editor.ValueBoxEditor;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.uibinder.client.UiConstructor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.Focusable;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.HasValue;

import elemental2.dom.Element;
import elemental2.dom.HTMLElement;
import elemental2.dom.HTMLInputElement;
import elemental2.dom.NodeList;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.gwtproject.editor.client.TakesValue;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import jsinterop.base.Js;

/**
 * select box with icon field for star ratings.
 *
 * @author Manfred Tremmel
 *
 */
@SuppressWarnings("CPD-START")
public class SelectBoxWithIconInputWidget<T extends Comparable<T>> extends Composite implements
    HasValue<T>, TakesValue<T>, Focusable, HasValidationMessageElement<T, ValueBoxEditor<T>> {

  private boolean valueChangeHandlerInitialized;
  private final FocusPanel panel;
  private final FlowPanel options;
  private final ValueBoxEditor<T> editor;
  private final List<IdAndNamePlusIconBean<T>> entries;
  private final List<RadioButton> radioButtons;
  private final String idBase;
  private final Resources resource;
  private final ClickHandler clickHandler;
  private final ValueChangeHandler<Boolean> valueChangeHandler;

  private HTMLPanel validationMessageElement;

  /**
   * A ClientBundle that provides images and style sheets for the decorator.
   */
  public interface Resources extends ClientBundle {

    /**
     * The styles used in this widget.
     *
     * @return decorator style
     */
    @Source("styling/SelectBoxInputStyle.gss")
    SelectBoxInputStyle selectBoxInputStyle();
  }

  /**
   * the default resources.
   */
  private static volatile Resources defaultResource;

  /**
   * default constructor.
   *
   * @param idBase the id of the widget and base for the ids of the single radio buttons
   */
  @UiConstructor
  public SelectBoxWithIconInputWidget(final String idBase) {
    this(idBase, getDefaultResources());
  }

  /**
   * constructor with styling information.
   *
   * @param idBase the id of the widget and base for the ids of the single radio buttons
   * @param resource resources with styling information
   */
  public SelectBoxWithIconInputWidget(final String idBase, final Resources resource) {
    super();
    this.resource = resource;
    this.idBase = idBase;
    entries = new ArrayList<>();
    radioButtons = new ArrayList<>();
    resource.selectBoxInputStyle().ensureInjected();
    panel = new FocusPanel();
    panel.setStylePrimaryName(resource.selectBoxInputStyle().selectBox());
    panel.getElement().setId(idBase);
    options = new FlowPanel();
    options.setStylePrimaryName(resource.selectBoxInputStyle().options());
    panel.add(options);
    initWidget(panel);
    valueChangeHandler = event -> ValueChangeEvent.fire(this, this.getValue());
    clickHandler = event -> {
      if (StringUtils.contains(panel.getStyleName(),
          resource.selectBoxInputStyle().selectBoxSelected())) {
        panel.removeStyleName(resource.selectBoxInputStyle().selectBoxSelected());
      } else {
        panel.addStyleName(resource.selectBoxInputStyle().selectBoxSelected());
      }
    };
    panel.addClickHandler(clickHandler);
    editor = new ExtendedValueBoxEditor<>(this, null);
  }

  /**
   * get default resource, if not set, create one.
   *
   * @return default resource.
   */
  protected static Resources getDefaultResources() { // NOPMD it's thread save!
    if (defaultResource == null) {
      synchronized (Resources.class) {
        if (defaultResource == null) {
          defaultResource = GWT.create(Resources.class);
        }
      }
    }
    return defaultResource;
  }

  /**
   * fill entries for the select box.
   *
   * @param entries list of entries
   */
  @SuppressWarnings("unchecked")
  public void fillEntries(final IdAndNamePlusIconBean<T>... entries) {
    this.fillEntries(List.of(entries));
  }

  /**
   * fill entries for the select box.
   *
   * @param entries list of entries
   */
  public void fillEntries(final List<IdAndNamePlusIconBean<T>> entries) {
    this.entries.clear();
    radioButtons.clear();
    this.entries.addAll(entries);
    boolean selected = false;
    for (final IdAndNamePlusIconBean<T> entry : this.entries) {
      final FlowPanel option = new FlowPanel();
      option.setStylePrimaryName(resource.selectBoxInputStyle().option());
      final RadioButton radioButton = new RadioButton();
      radioButton.getElement().setId(idBase + entry.getId());
      radioButton.setFormValue(Objects.toString(entry.getId()));
      radioButton.setName(idBase);
      if (!selected) {
        radioButton.setValue(Boolean.TRUE, false);
        selected = true;
      }
      radioButton.addClickHandler(clickHandler);
      option.add(radioButton);
      radioButtons.add(radioButton);
      final SafeHtmlBuilder labelShb = new SafeHtmlBuilder();
      labelShb.appendHtmlConstant("<img src=\"" + SafeHtmlUtils.htmlEscape(entry.getIconUrl())
          + "\" alt=\"" + SafeHtmlUtils.htmlEscape(entry.getName()) + "\" />");
      labelShb.appendEscaped(entry.getName());
      final InputLabel label = new InputLabel();
      label.setFor(radioButton);
      label.setHtml(labelShb.toSafeHtml());
      option.add(label);
      options.add(option);
    }
  }

  @Override
  public void setValue(final T value) {
    this.setValue(value, false);
  }

  @Override
  public void setValue(final T value, final boolean fireEvents) {
    final T oldValue = this.getValue();
    if (value == null) {
      for (final RadioButton radioButton : radioButtons) {
        radioButton.setValue(Boolean.FALSE, false);
      }
    } else {
      for (final RadioButton radioButton : radioButtons) {
        if (StringUtils.equals(value.toString(), radioButton.getFormValue())) {
          radioButton.setValue(Boolean.TRUE, false);
          break;
        }
      }
    }
    if (fireEvents) {
      ValueChangeEvent.fireIfNotEqual(this, oldValue, value);
    }
  }

  @Override
  public final T getValue() {
    for (final RadioButton radioButton : radioButtons) {
      if (BooleanUtils.isTrue(radioButton.getValue())) {
        final String valueAsString = radioButton.getFormValue();
        for (final IdAndNamePlusIconBean<T> entry : entries) {
          if (StringUtils.equals(valueAsString, Objects.toString(entry.getId()))) {
            return entry.getId();
          }
        }
      }
    }
    return null;
  }

  @Override
  public HandlerRegistration addValueChangeHandler(final ValueChangeHandler<T> handler) {
    // Is this the first value change handler? If so, time to add handlers
    if (!valueChangeHandlerInitialized) {
      this.ensureDomEventHandlers();
      valueChangeHandlerInitialized = true;
    }
    return this.addHandler(handler, ValueChangeEvent.getType());
  }

  protected void ensureDomEventHandlers() {
    radioButtons.forEach(radioButton -> radioButton.addValueChangeHandler(valueChangeHandler));
  }

  @Override
  public void setTabIndex(final int index) {
    int indexCount = index;
    for (final RadioButton radioButton : radioButtons) {
      radioButton.setTabIndex(indexCount++);
    }
  }

  @Override
  public int getTabIndex() {
    return radioButtons.stream().map(radioButton -> radioButton.getTabIndex()).findFirst()
        .orElse(-1);
  }

  @Override
  public void setAccessKey(final char key) {
    final Optional<RadioButton> radioButton = radioButtons.stream().findFirst();
    if (radioButton.isPresent()) {
      radioButton.get().setAccessKey(key);
    }
  }

  @Override
  public void setFocus(final boolean focused) {
    final Optional<RadioButton> radioButton = radioButtons.stream().findFirst();
    if (radioButton.isPresent()) {
      radioButton.get().setFocus(focused);
    }
  }

  @Override
  public ValueBoxEditor<T> asEditor() {
    return editor;
  }

  @Override
  public void setCustomValidity(final String message) {
    final HTMLElement headElement = Js.<HTMLElement>uncheckedCast(getElement());
    final NodeList<Element> inputElements = headElement.getElementsByTagName("input");
    inputElements.asList().forEach(element -> {
      if (FeatureCheck.supportCustomValidity(element)) {
        ((HTMLInputElement) element).setCustomValidity(message);
      }
    });
  }

  @Override
  public void setTitle(final String title) {
    final HTMLElement headElement = Js.<HTMLElement>uncheckedCast(getElement());
    final NodeList<Element> inputElements = headElement.getElementsByTagName("input");
    inputElements.asList().forEach(element -> {
      ((HTMLInputElement) element).title = title;
    });
  }

  @Override
  public void setValidationMessageElement(final HTMLPanel element) {
    validationMessageElement = element;
  }

  @Override
  public HTMLPanel getValidationMessageElement() {
    return validationMessageElement;
  }
}

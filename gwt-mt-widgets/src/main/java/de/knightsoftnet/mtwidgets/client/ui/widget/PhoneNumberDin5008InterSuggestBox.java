/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.mtwidgets.client.ui.widget;

import de.knightsoftnet.mtwidgets.client.ui.widget.oracle.PhoneNumberDin5008Oracle;
import de.knightsoftnet.validators.shared.data.ValueWithPos;

/**
 * phone number DIN5008 international suggest widget.
 *
 * @author Manfred Tremmel
 *
 */
public class PhoneNumberDin5008InterSuggestBox extends AbstractPhoneNumberSuggestBox {

  /**
   * default constructor.
   */
  public PhoneNumberDin5008InterSuggestBox() {
    super(new PhoneNumberDin5008Oracle());
  }

  @Override
  public boolean isFormatingCharacter(final char character) {
    return character == '+' || character == '-' || character == ' ';
  }

  @Override
  public ValueWithPos<String> formatWithPos(final ValueWithPos<String> value,
      final String countryCode) {
    return phoneNumberUtil.formatDin5008InternationalWithPos(value, countryCode);
  }

  @Override
  public String format(final String value, final String countryCode) {
    return phoneNumberUtil.formatDin5008International(value, countryCode);
  }
}

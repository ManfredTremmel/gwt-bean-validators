/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.mtwidgets.client.ui.handler;

import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.TakesValue;

import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * Factory Class to get Handlers.
 *
 * @author Manfred Tremmel
 */
public class HandlerFactory {

  private static final Map<String, RegExKeyPressHandler> REG_EX_KEY_PRESS_HANDLER_MAP =
      new HashMap<>();

  private static volatile UpperAsciiKeyPressHandler upperAsciiKeyPressHandler = null;
  private static volatile NumericAndUpperAsciiKeyPressHandler numericAndUpperAsciiKeyPressHandler =
      null;
  private static volatile NumericKeyPressHandler numericKeyPressHandler = null;
  private static volatile NumericWithSeparatorsKeyPressHandler numericWsKeyPressHandler = null;

  private static volatile CurrencyKeyPressHandler currencyKeyPressHandler = null;
  private static volatile PercentKeyPressHandler percentKeyPressHandler = null;
  private static volatile PhoneNumberKeyPressHandler phoneNumberKeyPressHandler = null;
  private static volatile DecimalKeyPressHandler decimalKeyPressHandler = null;

  private static volatile FilterReplaceAndFormatKeyPressHandler<String> filReplFormStrKeyPrH = null;
  private static volatile FormatKeyUpHandler<String> formatStrKeyUpHandler = null;

  /**
   * get a upper case key press handler.
   *
   * @return UpperAsciiKeyPressHandler
   */
  public static final KeyPressHandler getUpperAsciiKeyPressHandler() { // NOPMD it's thread save!
    if (upperAsciiKeyPressHandler == null) {
      synchronized (UpperAsciiKeyPressHandler.class) {
        if (upperAsciiKeyPressHandler == null) {
          upperAsciiKeyPressHandler = new UpperAsciiKeyPressHandler();
        }
      }
    }
    return upperAsciiKeyPressHandler;
  }

  /**
   * get a numeric and upper case key press handler.
   *
   * @return NumericAndUpperAsciiKeyPressHandler
   */
  public static final KeyPressHandler getNumericAndUpperAsciiKeyPressHandler() { // NOPMD
    if (numericAndUpperAsciiKeyPressHandler == null) {
      synchronized (NumericAndUpperAsciiKeyPressHandler.class) {
        if (numericAndUpperAsciiKeyPressHandler == null) {
          numericAndUpperAsciiKeyPressHandler = new NumericAndUpperAsciiKeyPressHandler();
        }
      }
    }
    return numericAndUpperAsciiKeyPressHandler;
  }

  /**
   * get a numeric key press handler.
   *
   * @return NumericKeyPressHandler
   */
  public static final KeyPressHandler getNumericKeyPressHandler() { // NOPMD it's thread save!
    if (numericKeyPressHandler == null) {
      synchronized (NumericKeyPressHandler.class) {
        if (numericKeyPressHandler == null) {
          numericKeyPressHandler = new NumericKeyPressHandler();
        }
      }
    }
    return numericKeyPressHandler;
  }

  /**
   * get a numeric with separators key press handler.
   *
   * @return NumericWithSeparatorsKeyPressHandler
   */
  public static final KeyPressHandler getNumericWithSeparatorsKeyPressHandler() { // NOPMD
    if (numericWsKeyPressHandler == null) {
      synchronized (NumericWithSeparatorsKeyPressHandler.class) {
        if (numericWsKeyPressHandler == null) {
          numericWsKeyPressHandler = new NumericWithSeparatorsKeyPressHandler();
        }
      }
    }
    return numericWsKeyPressHandler;
  }

  /**
   * get a currency key press handler.
   *
   * @return CurrencyKeyPressHandler
   */
  public static final KeyPressHandler getCurrencyKeyPressHandler() { // NOPMD it's thread save!
    if (currencyKeyPressHandler == null) {
      synchronized (CurrencyKeyPressHandler.class) {
        if (currencyKeyPressHandler == null) {
          currencyKeyPressHandler = new CurrencyKeyPressHandler();
        }
      }
    }
    return currencyKeyPressHandler;
  }

  /**
   * get a percent key press handler.
   *
   * @return PercentKeyPressHandler
   */
  public static final KeyPressHandler getPercentKeyPressHandler() { // NOPMD it's thread save!
    if (percentKeyPressHandler == null) {
      synchronized (PercentKeyPressHandler.class) {
        if (percentKeyPressHandler == null) {
          percentKeyPressHandler = new PercentKeyPressHandler();
        }
      }
    }
    return percentKeyPressHandler;
  }

  /**
   * get a phone number key press handler.
   *
   * @return PhoneNumberKeyPressHandler
   */
  public static final KeyPressHandler getPhoneNumberKeyPressHandler() { // NOPMD it's thread save!
    if (phoneNumberKeyPressHandler == null) {
      synchronized (PhoneNumberKeyPressHandler.class) {
        if (phoneNumberKeyPressHandler == null) {
          phoneNumberKeyPressHandler = new PhoneNumberKeyPressHandler();
        }
      }
    }
    return phoneNumberKeyPressHandler;
  }

  /**
   * get a decimal key press handler.
   *
   * @return DecimalKeyPressHandler
   */
  public static final KeyPressHandler getDecimalKeyPressHandler() { // NOPMD it's thread save!
    if (decimalKeyPressHandler == null) {
      synchronized (DecimalKeyPressHandler.class) {
        if (decimalKeyPressHandler == null) {
          decimalKeyPressHandler = new DecimalKeyPressHandler();
        }
      }
    }
    return decimalKeyPressHandler;
  }

  /**
   * get a key press handler which allows all characters which could match a reg ex.
   *
   * @param regEx to check
   * @return key press handler
   */
  public static final KeyPressHandler getRegExKeyPressHandler(final String regEx) {
    if (StringUtils.isEmpty(regEx)) {
      return null;
    }
    RegExKeyPressHandler result = REG_EX_KEY_PRESS_HANDLER_MAP.get(regEx);
    if (result == null) {
      result = new RegExKeyPressHandler(regEx);
    }
    return result;
  }

  /**
   * get a key press handler which allows characters for postal codes of a referenced country.
   *
   * @param countryCodeField reference to country code field
   * @return key press handler
   */
  public static final KeyPressHandler getPostalCodeKeyPressHandler(
      final TakesValue<?> countryCodeField) {
    return new PostalCodeKeyPressHandler(countryCodeField);
  }


  /**
   * get a filter replace and format String key press handler.
   *
   * @return FilterReplaceAndFormatKeyPressHandler&lt;String&gt;
   */
  public static final KeyPressHandler getFilterReplAndFormatStrKeyPressHandler() { // NOPMD
    if (filReplFormStrKeyPrH == null) {
      synchronized (DecimalKeyPressHandler.class) {
        if (filReplFormStrKeyPrH == null) {
          filReplFormStrKeyPrH = new FilterReplaceAndFormatKeyPressHandler<>();
        }
      }
    }
    return filReplFormStrKeyPrH;
  }

  /**
   * get a format key up handler.
   *
   * @return FormatKeyUpHandler&lt;String&gt;
   */
  public static final KeyUpHandler getFormatStrKeyUpHandler() { // NOPMD it's thread save!
    if (formatStrKeyUpHandler == null) {
      synchronized (DecimalKeyPressHandler.class) {
        if (formatStrKeyUpHandler == null) {
          formatStrKeyUpHandler = new FormatKeyUpHandler<>();
        }
      }
    }
    return formatStrKeyUpHandler;
  }

  /**
   * get a key press handler which allows characters for tax number of a referenced country.
   *
   * @param countryCodeField reference to country code field
   * @return key press handler
   */
  public static final KeyPressHandler getTaxNumberKeyPressHandler(
      final TakesValue<?> countryCodeField) {
    return new TaxNumberKeyPressHandler(countryCodeField);
  }

  /**
   * get a key press handler which allows characters for tin of a referenced country.
   *
   * @param countryCodeField reference to country code field
   * @return key press handler
   */
  public static final KeyPressHandler getTinKeyPressHandler(final TakesValue<?> countryCodeField) {
    return new TinKeyPressHandler(countryCodeField);
  }

  /**
   * get a key press handler which allows characters for vat id of a referenced country.
   *
   * @param countryCodeField reference to country code field
   * @return key press handler
   */
  public static final KeyPressHandler getVatIdKeyPressHandler(
      final TakesValue<?> countryCodeField) {
    return new VatIdKeyPressHandler(countryCodeField);
  }
}

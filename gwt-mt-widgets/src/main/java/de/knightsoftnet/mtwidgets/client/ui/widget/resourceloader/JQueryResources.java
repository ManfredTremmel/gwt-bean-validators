package de.knightsoftnet.mtwidgets.client.ui.widget.resourceloader;

import com.google.gwt.core.client.GWT;

import elemental2.dom.DomGlobal;
import elemental2.dom.Element;
import elemental2.dom.Event;
import elemental2.dom.EventListener;
import elemental2.dom.HTMLScriptElement;
import elemental2.dom.NodeList;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class JQueryResources {

  private static final String JQUERY_URL =
      ((JsJQueryResourceDefinitionInterface) GWT.create(JsJQueryResourceDefinitionInterface.class))
          .getJQeryPath();
  private static final String JQUERY_MIGRATE_URL =
      ((JsJQueryResourceDefinitionInterface) GWT.create(JsJQueryResourceDefinitionInterface.class))
          .getJQeryMigratePath();

  private static boolean initializationStarted = false;

  private static Event rememberEvent;
  private static List<EventListener> eventLisenerQueue = new ArrayList<>();

  /**
   * async load of resources.
   *
   * @param function function to call on load
   */
  public static void whenReady(final EventListener function) {
    eventLisenerQueue.add(function);
    if (initializationStarted || isInHeader()) {
      if (isInitialized()) {
        eventLisenerQueue.forEach(action -> action.handleEvent(rememberEvent));
        eventLisenerQueue.clear();
      }
      return;
    }
    initializationStarted = true;

    final HTMLScriptElement jqueryScript =
        (HTMLScriptElement) DomGlobal.document.createElement(JsResources.TAG_TYPE);
    jqueryScript.src = JQUERY_URL;
    jqueryScript.type = JsResources.SCRIPT_TYPE;

    DomGlobal.document.head.appendChild(jqueryScript);

    jqueryScript.addEventListener("load", event -> {
      final HTMLScriptElement jqueryMigrateScript =
          (HTMLScriptElement) DomGlobal.document.createElement(JsResources.TAG_TYPE);
      jqueryMigrateScript.src = JQUERY_MIGRATE_URL;
      jqueryMigrateScript.type = JsResources.SCRIPT_TYPE;

      DomGlobal.document.head.appendChild(jqueryMigrateScript);
      jqueryMigrateScript.addEventListener("load", secondEvent -> {
        eventLisenerQueue.forEach(action -> action.handleEvent(secondEvent));
        eventLisenerQueue.clear();
        rememberEvent = secondEvent;
      });
    });
  }

  /**
   * check if script is available and initialized.
   *
   * @return true if it exists
   */
  public static boolean isInitialized() {
    final boolean loaded = rememberEvent != null || StringUtils.isEmpty(JQUERY_URL);
    return loaded || isInHeader();
  }

  /**
   * check if script is already in the header.
   *
   * @return true if it exists
   */
  public static boolean isInHeader() {
    boolean jqueryInHeader = false;
    boolean jqueryMigrateInHeader = false;
    final NodeList<Element> linkList =
        DomGlobal.document.head.getElementsByTagName(JsResources.TAG_TYPE);
    for (final Element element : linkList.asList()) {
      jqueryInHeader |= StringUtils.contains(((HTMLScriptElement) element).src, JQUERY_URL);
      jqueryMigrateInHeader |=
          StringUtils.contains(((HTMLScriptElement) element).src, JQUERY_MIGRATE_URL);
    }
    return jqueryInHeader && jqueryMigrateInHeader;
  }
}

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.mtwidgets.client.ui.widget.helper;

import de.knightsoftnet.mtwidgets.client.event.EditorDeleteEvent;
import de.knightsoftnet.mtwidgets.client.event.EditorDeleteEvent.EditorDeleteHandler;
import de.knightsoftnet.mtwidgets.client.event.ListPositionChangeEvent;
import de.knightsoftnet.mtwidgets.client.event.ListPositionChangeEvent.ListPositionChangeHandler;
import de.knightsoftnet.mtwidgets.client.event.ListPositionChangeEvent.ListPositionEnum;

import org.gwtproject.editor.client.EditorDelegate;
import org.gwtproject.editor.client.HasEditorDelegate;
import com.google.gwt.user.client.ui.Composite;
import com.google.web.bindery.event.shared.HandlerRegistration;

/**
 * Abstract View of an list item, gwt implementation.
 *
 * @author Manfred Tremmel
 * @version $Rev$, $Date$
 *
 * @param <D> type of data to edit
 */
public abstract class AbstractListItemView<D> extends Composite
    implements HasEditorDelegate<D>, EditorDeleteEvent.EditorDeleteHandlers<D>,
    ListPositionChangeEvent.ListPositionChangeHandlers<D> {

  @Override
  public void setDelegate(final EditorDelegate<D> delegate) {
    delegate.subscribe();
  }

  @Override
  public HandlerRegistration addEditorDeleteHandler(final EditorDeleteHandler<D> handler) {
    return this.addHandler(handler, EditorDeleteEvent.getType());
  }

  @Override
  public HandlerRegistration addListPositionChangeHandler(
      final ListPositionChangeHandler<D> handler) {
    return this.addHandler(handler, ListPositionChangeEvent.getType());
  }

  /**
   * removes current entry from list.
   */
  protected void removeThisEntry() {
    fireEvent(new EditorDeleteEvent<>(this));
  }

  /**
   * move position up.
   */
  protected void movePositionUp() {
    fireEvent(new ListPositionChangeEvent<>(this, ListPositionEnum.UP));
  }

  /**
   * move position down.
   */
  protected void movePositionDown() {
    fireEvent(new ListPositionChangeEvent<>(this, ListPositionEnum.DOWN));
  }

  /**
   * move to specified position.
   *
   * @param newPos position to move to
   */
  protected void moveToPosition(final int newPos) {
    fireEvent(new ListPositionChangeEvent<>(this, ListPositionEnum.ABSOLUTE, newPos));
  }
}

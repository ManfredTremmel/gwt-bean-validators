/*
 * Copyright 2009 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.mtwidgets.client.ui.widget;

import de.knightsoftnet.validators.client.editor.TakesValueEditor;

import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Element;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.user.client.ui.AbstractSuggestBoxNewEditor;
import com.google.gwt.user.client.ui.FocusWidget;
import com.google.gwt.user.client.ui.MultiWordSuggestOracle;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.SuggestOracle;
import com.google.gwt.user.client.ui.Widget;

import org.gwtproject.editor.client.Editor.Ignore;
import org.gwtproject.editor.client.IsEditor;
import org.gwtproject.editor.client.LeafValueEditor;
import org.gwtproject.editor.client.TakesValue;

/**
 * A {@link SuggestBoxNewEditor} is a text box or text area which displays a pre-configured set of
 * selections that match the user's input.
 *
 * <p>
 * Each {@link SuggestBoxNewEditor} is associated with a single {@link SuggestOracle}. The
 * {@link SuggestOracle} is used to provide a set of selections given a specific query string.
 * </p>
 *
 * <p>
 * By default, the {@link SuggestBoxNewEditor} uses a {@link MultiWordSuggestOracle} as its oracle.
 * Below we show how a {@link MultiWordSuggestOracle} can be configured:
 * </p>
 *
 * <pre>
 * MultiWordSuggestOracle oracle = new MultiWordSuggestOracle();
 * oracle.add("Cat");
 * oracle.add("Dog");
 * oracle.add("Horse");
 * oracle.add("Canary");
 *
 * SuggestBoxNewEditor box = new SuggestBoxNewEditor(oracle);
 * </pre>
 *
 * <p>
 * Using the example above, if the user types "C" into the text widget, the oracle will configure
 * the suggestions with the "Cat" and "Canary" suggestions. Specifically, whenever the user types a
 * key into the text widget, the value is submitted to the <code>MultiWordSuggestOracle</code>.
 * </p>
 *
 * <p>
 * Note that there is no method to retrieve the "currently selected suggestion" in a
 * SuggestBoxNewEditor, because there are points in time where the currently selected suggestion is
 * not defined. For example, if the user types in some text that does not match any of the
 * SuggestBoxNewEditor's suggestions, then the SuggestBoxNewEditor will not have a currently
 * selected suggestion. It is more useful to know when a suggestion has been chosen from the
 * SuggestBoxNewEditor's list of suggestions. A SuggestBoxNewEditor fires {@link SelectionEvent
 * SelectionEvents} whenever a suggestion is chosen, and handlers for these events can be added
 * using the {@link #addSelectionHandler(SelectionHandler)} method.
 * </p>
 *
 * <p>
 * <img class='gallery' src='doc-files/SuggestBox.png' alt='SuggestBox example image'/>
 * </p>
 *
 * <h2>CSS Style Rules</h2>
 * <dl>
 * <dt>.gwt-SuggestBox</dt>
 * <dd>the suggest box itself</dd>
 * </dl>
 *
 * @see SuggestOracle
 * @see MultiWordSuggestOracle
 * @see ValueBoxBaseWithEditorErrors
 */
@SuppressWarnings("deprecation")
public class SuggestBoxNewEditor extends AbstractSuggestBoxNewEditor
    implements IsEditor<LeafValueEditor<String>>, TakesValue<String> {

  /**
   * Creates a {@link SuggestBoxNewEditor} widget that wraps an existing &lt;input type='text'&gt;
   * element.
   *
   * <p>
   * This element must already be attached to the document. If the element is removed from the
   * document, you must call {@link RootPanel#detachNow(Widget)}.
   * </p>
   *
   * @param oracle the suggest box oracle to use
   * @param element the element to be wrapped
   */
  public static SuggestBoxNewEditor wrap(final SuggestOracle oracle, final Element element) {
    // Assert that the element is attached.
    assert Document.get().getBody().isOrHasChild(element);

    final TextBox textBox = new TextBox(element, null, null);
    final SuggestBoxNewEditor suggestBox = new SuggestBoxNewEditor(oracle, textBox);

    // Mark it attached and remember it for cleanup.
    suggestBox.onAttach();
    RootPanel.detachOnWindowClose(suggestBox);

    return suggestBox;
  }

  private LeafValueEditor<String> editor;
  private final ValueBoxBaseWithEditorErrors<String> box;


  @Override
  protected FocusWidget getBoxAsFoucsWidget() {
    return box;
  }


  /**
   * Constructor for {@link SuggestBoxNewEditor}. Creates a {@link MultiWordSuggestOracle} and
   * {@link TextBox} to use with this {@link SuggestBoxNewEditor}.
   */
  public SuggestBoxNewEditor() {
    this(new MultiWordSuggestOracle());
  }

  /**
   * Constructor for {@link SuggestBoxNewEditor}. Creates a {@link TextBox} to use with this
   * {@link SuggestBoxNewEditor}.
   *
   * @param oracle the oracle for this <code>SuggestBoxNewEditor</code>
   */
  public SuggestBoxNewEditor(final SuggestOracle oracle) {
    this(oracle, new TextBox());
  }

  /**
   * Constructor for {@link SuggestBoxNewEditor}. The text box will be removed from it's current
   * location and wrapped by the {@link SuggestBoxNewEditor}.
   *
   * @param oracle supplies suggestions based upon the current contents of the text widget
   * @param box the text widget
   */
  public SuggestBoxNewEditor(final SuggestOracle oracle,
      final ValueBoxBaseWithEditorErrors<String> box) {
    this(oracle, box, new DefaultSuggestionDisplay());
  }

  /**
   * Constructor for {@link SuggestBoxNewEditor}. The text box will be removed from it's current
   * location and wrapped by the {@link SuggestBoxNewEditor}.
   *
   * @param oracle supplies suggestions based upon the current contents of the text widget
   * @param box the text widget
   * @param suggestDisplay the class used to display suggestions
   */
  public SuggestBoxNewEditor(final SuggestOracle oracle,
      final ValueBoxBaseWithEditorErrors<String> box, final SuggestionDisplay suggestDisplay) {
    super(suggestDisplay);
    this.box = box;
    initWidget(box);

    addEventsToTextBox();

    setOracle(oracle);
    setStyleName(STYLENAME_DEFAULT);
  }

  /**
   * Returns a {@link TakesValueEditor} backed by the SuggestBoxNewEditor.
   */
  @Override
  public LeafValueEditor<String> asEditor() {
    if (editor == null) {
      editor = TakesValueEditor.of(this);
    }
    return editor;
  }

  @Override
  public int getTabIndex() {
    return box.getTabIndex();
  }

  @Override
  public String getText() {
    return box.getText();
  }

  @Override
  public String getValue() {
    return box.getValue();
  }

  /**
   * Get the ValueBoxBaseWithEditorErrors associated with this suggest box.
   *
   * @return this suggest box's value box
   */
  @Ignore
  public ValueBoxBaseWithEditorErrors<String> getValueBox() {
    return box;
  }

  /**
   * Gets whether this widget is enabled.
   *
   * @return <code>true</code> if the widget is enabled
   */
  @Override
  public boolean isEnabled() {
    return box.isEnabled();
  }

  @Override
  public void setAccessKey(final char key) {
    box.setAccessKey(key);
  }

  /**
   * Sets whether this widget is enabled.
   *
   * @param enabled <code>true</code> to enable the widget, <code>false</code> to disable it
   */
  @Override
  public void setEnabled(final boolean enabled) {
    box.setEnabled(enabled);
    super.setEnabled(enabled);
  }

  @Override
  public void setFocus(final boolean focused) {
    box.setFocus(focused);
  }

  @Override
  public void setTabIndex(final int index) {
    box.setTabIndex(index);
  }

  @Override
  public void setText(final String text) {
    box.setText(text);
  }

  @Override
  public void setValue(final String newValue) {
    box.setValue(newValue);
  }

  @Override
  public void setValue(final String value, final boolean fireEvents) {
    box.setValue(value, fireEvents);
  }

  @Override
  @SuppressWarnings("unchecked")
  protected void addEventsToTextBox(final EventHandler events) {
    box.addKeyDownHandler((KeyDownHandler) events);
    box.addKeyUpHandler((KeyUpHandler) events);
    box.addValueChangeHandler((ValueChangeHandler<String>) events);
  }
}

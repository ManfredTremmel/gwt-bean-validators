/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.mtwidgets.client.ui.widget;

import de.knightsoftnet.mtwidgets.client.ui.widget.features.SortableIdAndName;
import de.knightsoftnet.mtwidgets.client.ui.widget.helper.ListSortEnum;
import de.knightsoftnet.mtwidgets.client.ui.widget.helper.MessagesForValues;

import java.util.Collection;
import java.util.List;

/**
 * a list box with id and name which is sortable and returns id.
 *
 * @author Manfred Tremmel
 *
 * @param <T> type of the id
 */
public class SortableIdAndNameListBox<T extends Comparable<T>> extends IdAndNameListBox<T>
    implements SortableIdAndName<T> {
  private final ListSortEnum sortOrder;
  private final MessagesForValues<T> messages;

  /**
   * widget ui constructor.
   *
   * @param sort the sort order of the countries
   * @param messages message resolver
   * @param ids ids to add to listBox
   */
  @SafeVarargs
  public SortableIdAndNameListBox(final ListSortEnum sort, final MessagesForValues<T> messages,
      final T... ids) {
    this(sort, messages, List.of(ids));
  }

  /**
   * widget ui constructor.
   *
   * @param sort the sort order of the countries
   * @param messages message resolver
   * @param ids ids to add to listBox
   */
  public SortableIdAndNameListBox(final ListSortEnum sort, final MessagesForValues<T> messages,
      final Collection<T> ids) {
    super();
    setVisibleItemCount(1);
    sortOrder = sort;
    this.messages = messages;

    this.fillEntries(sort, messages, ids);
  }

  @Override
  public void fillEntries(final Collection<T> ids) {
    fillEntries(sortOrder, messages, ids);
  }
}

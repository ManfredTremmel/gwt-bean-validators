/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.mtwidgets.client.ui.widget;

import de.knightsoftnet.mtwidgets.client.ui.widget.features.HasCountryCodeReference;
import de.knightsoftnet.mtwidgets.client.ui.widget.oracle.AbstractPhoneNumberLocalOracle;
import de.knightsoftnet.validators.shared.data.ValueWithPos;
import de.knightsoftnet.validators.shared.util.CountryUtil;
import de.knightsoftnet.validators.shared.util.PhoneNumberUtil;

import com.google.gwt.i18n.client.LocaleInfo;
import com.google.gwt.user.client.TakesValue;

import elemental2.dom.DomGlobal;
import elemental2.dom.HTMLInputElement;

import org.apache.commons.lang3.StringUtils;

import java.util.Objects;

/**
 * phone number common international suggest widget.
 *
 * @author Manfred Tremmel
 *
 */
public abstract class AbstractPhoneNumberSuggestBox extends AbstractFormatingSuggestBox
    implements HasCountryCodeReference {

  protected TakesValue<?> countryCodeReference;
  protected final PhoneNumberUtil phoneNumberUtil;

  /**
   * default constructor.
   *
   * @param oracle suggest oracle to use
   */
  protected AbstractPhoneNumberSuggestBox(final AbstractPhoneNumberLocalOracle<?> oracle) {
    super(oracle, new TextBoxWithFormating(
        (HTMLInputElement) DomGlobal.document.createElement("input"), "tel"));
    phoneNumberUtil = oracle.getPhoneNumberUtil();
    ((TextBoxWithFormating) getValueBox()).setFormating(this);
  }

  /**
   * set reference to a field which contains the country code.
   *
   * @param countryCodeField field which contains the country code
   * @deprecated use {@link #setCountryCodeReference(TakesValue)} instead
   */
  @Deprecated
  public void setCountryCodeReferenceField(final TakesValue<?> countryCodeField) {
    setCountryCodeReference(countryCodeField);
  }

  @Override
  public void setCountryCodeReference(final TakesValue<?> countryCodeReference) {
    this.countryCodeReference = countryCodeReference;
  }

  /**
   * get country code from reference field or localization depending as fallback.
   *
   * @return string with country code
   */
  public String getCountryCode() {
    return StringUtils.upperCase(Objects.toString(countryCodeReference == null
        ? CountryUtil.convertLanguageToCountryEnum(LocaleInfo.getCurrentLocale().getLocaleName())
        : countryCodeReference.getValue(), null));
  }

  @Override
  public boolean isAllowedCharacter(final char character) {
    return character >= '0' && character <= '9' || isFormatingCharacter(character);
  }

  @Override
  public boolean isCharacterToReplace(final char character) {
    return false;
  }

  @Override
  public char replaceCharacter(final char character) {
    return character;
  }

  @Override
  public void formatValue(final ValueWithPos<String> value, final boolean fireEvents) {
    setTextWithPos(formatWithPos(value, getCountryCode()), fireEvents);
  }

  @Override
  public String formatValueSynchron(final String value) {
    final String formatedNumber = format(value, getCountryCode());
    if (StringUtils.startsWith(value, formatedNumber)) {
      return value;
    }
    return Objects.toString(formatedNumber, value);
  }

  /**
   * format phone number with position.
   *
   * @param value the value with position to format
   * @param countryCode code of default country
   * @return formated value with position
   */
  public abstract ValueWithPos<String> formatWithPos(final ValueWithPos<String> value,
      final String countryCode);

  /**
   * format phone number as string.
   *
   * @param value phone number to format
   * @param countryCode code of default country
   * @return formated phone number
   */
  public abstract String format(final String value, final String countryCode);
}

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.mtwidgets.client.ui.widget.oracle;

import de.knightsoftnet.validators.shared.data.BicMapSharedConstants;
import de.knightsoftnet.validators.shared.impl.BicValidator;
import de.knightsoftnet.validators.shared.util.HasSetBicMapSharedConstants;
import de.knightsoftnet.validators.shared.util.IbanUtil;

import com.google.gwt.user.client.ui.SuggestOracle;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

/**
 * suggest oracle of BIC suggest widget.
 *
 * @author Manfred Tremmel
 *
 */
public class BicOracle extends SuggestOracle implements HasSetBicMapSharedConstants {

  /**
   * default limit suggests.
   */
  private static final int LIMIT_DEFAULT = 20;

  /**
   * provider for map of swift countries and the length of the ibans.
   */
  private BicMapSharedConstants bicMapSharedConstants;

  /**
   * default constructor.
   */
  public BicOracle() {
    super();
    final IbanUtil ibanUtil = new IbanUtil();
    ibanUtil.setBicMapSharedConstantsWhenAvailable(this);
  }

  @Override
  public void setBicMapSharedConstants(final BicMapSharedConstants bicMapSharedConstants) {
    this.bicMapSharedConstants = bicMapSharedConstants;
  }

  @Override
  public final boolean isDisplayStringHTML() {
    return true;
  }

  @Override
  public final void requestSuggestions(final Request request, final Callback callback) {
    final SuggestOracle.Response response = new SuggestOracle.Response();
    if (request != null && StringUtils.isNotEmpty(request.getQuery())) {
      final int limit;
      if (request.getLimit() > 0) {
        limit = request.getLimit();
      } else {
        limit = LIMIT_DEFAULT;
      }
      final List<BicItemSuggest> suggestions = new ArrayList<>(limit);
      // first run, starts with exact test
      if (bicMapSharedConstants != null) {
        for (final Entry<String, String> entry : bicMapSharedConstants.bics().entrySet()) {
          if (entry.getKey().startsWith(request.getQuery())) {
            suggestions
                .add(new BicItemSuggest(entry.getKey(), entry.getKey().replace(request.getQuery(),
                    "<strong>" + request.getQuery() + "</strong>"), entry.getValue()));
            if (suggestions.size() >= limit) {
              break;
            }
          }
        }
      }
      // second run, contains
      if (suggestions.isEmpty() && bicMapSharedConstants != null) {
        for (final Entry<String, String> entry : bicMapSharedConstants.bics().entrySet()) {
          if (entry.getKey().contains(request.getQuery())) {
            suggestions
                .add(new BicItemSuggest(entry.getKey(), entry.getKey().replace(request.getQuery(),
                    "<strong>" + request.getQuery() + "</strong>"), entry.getValue()));
            if (suggestions.size() >= limit) {
              break;
            }
          }
        }
      }
      // third run, contains with limited length
      if (suggestions.isEmpty() && bicMapSharedConstants != null) {
        for (final Entry<String, String> entry : bicMapSharedConstants.bics().entrySet()) {
          if (entry.getKey().contains(
              StringUtils.substring(request.getQuery(), 0, BicValidator.BIC_LENGTH_MIN))) {
            if (request.getQuery().length() == BicValidator.BIC_LENGTH_MAX) {
              suggestions.add(new BicItemSuggest(request.getQuery(),
                  "<strong>" + request.getQuery() + "</strong>", entry.getValue()));
            }
            suggestions.add(new BicItemSuggest(entry.getKey(), //
                entry.getKey().replace(StringUtils.substring(request.getQuery(), 0, //
                    BicValidator.BIC_LENGTH_MIN), //
                    "<strong>" + StringUtils.substring(request.getQuery(), 0, //
                        BicValidator.BIC_LENGTH_MIN) + "</strong>"), //
                entry.getValue()));
            if (suggestions.size() >= limit) {
              break;
            }
          }
        }
      }
      response.setSuggestions(suggestions);
    }
    callback.onSuggestionsReady(request, response);
  }
}

package de.knightsoftnet.mtwidgets.client.ui.widget.helper;

import de.knightsoftnet.validators.client.decorators.ExtendedValueBoxEditor;
import de.knightsoftnet.validators.client.editor.ValueBoxBase;

import com.google.gwt.user.client.ui.SuggestBox;
import com.google.gwt.user.client.ui.TextBoxBase;

import org.gwtproject.editor.client.TakesValue;

public class SuggestBoxValueBoxEditor extends ExtendedValueBoxEditor<String> {

  public SuggestBoxValueBoxEditor(final TakesValue<String> takesValues) {
    super(takesValues, null);
  }

  /**
   * get text box.
   *
   * @return text box base of the widget
   */
  @Ignore
  public TextBoxBase getTextBox() {
    if (getTakesValues() instanceof final SuggestBox suggestBox) {
      return (TextBoxBase) suggestBox.getValueBox();
    }
    return null;
  }

  /**
   * get value box base.
   *
   * @return value box base of the widget
   */
  @Ignore
  public ValueBoxBase<String> getValueBox() {
    if (getTakesValues() instanceof final SuggestBox suggestBox) {
      return ValueBoxBase.oldToNew(suggestBox.getValueBox());
    }
    return null;
  }
}

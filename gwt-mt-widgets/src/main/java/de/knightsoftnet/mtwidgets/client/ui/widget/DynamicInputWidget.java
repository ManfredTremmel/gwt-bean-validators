/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.mtwidgets.client.ui.widget;

import de.knightsoftnet.mtwidgets.client.ui.handler.HandlerFactory;
import de.knightsoftnet.mtwidgets.client.ui.widget.helper.DateParser;
import de.knightsoftnet.mtwidgets.client.ui.widget.helper.DateRenderer;
import de.knightsoftnet.mtwidgets.client.ui.widget.helper.DateTimeLocalParser;
import de.knightsoftnet.mtwidgets.client.ui.widget.helper.DateTimeLocalRenderer;
import de.knightsoftnet.mtwidgets.client.ui.widget.helper.TimeParser;
import de.knightsoftnet.mtwidgets.client.ui.widget.helper.TimeRenderer;
import de.knightsoftnet.validators.shared.data.FieldTypeEnum;

import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.text.client.DoubleParser;
import com.google.gwt.text.client.DoubleRenderer;
import com.google.gwt.text.shared.testing.PassthroughParser;
import com.google.gwt.text.shared.testing.PassthroughRenderer;

import elemental2.dom.CSSProperties;
import elemental2.dom.DomGlobal;
import elemental2.dom.HTMLInputElement;
import elemental2.dom.ValidityState;

import org.apache.commons.lang3.StringUtils;

import java.text.ParseException;

/**
 * Dynamic input widget which can switch between the input types.
 *
 * @author Manfred Tremmel
 */
public class DynamicInputWidget extends ValueBox<String> {

  private static final String TYPE = "type";
  private FieldTypeEnum fieldType;
  private HandlerRegistration attachedHandler;

  /**
   * default constructor.
   */
  public DynamicInputWidget() {
    super((HTMLInputElement) DomGlobal.document.createElement("input"), "text",
        PassthroughRenderer.instance(), PassthroughParser.instance());
    fieldType = FieldTypeEnum.STRING;
  }

  public final FieldTypeEnum getFieldType() {
    return fieldType;
  }

  /**
   * set type of the field.
   *
   * @param fieldType FieldTypeEnum
   * @param required true if field is required
   */
  public final void setFieldType(final FieldTypeEnum fieldType, final boolean required) {
    if (attachedHandler != null) {
      attachedHandler.removeHandler();
      attachedHandler = null;
    }
    this.fieldType = fieldType;
    final HTMLInputElement inputElement = getInputElement();
    inputElement.required = required;
    inputElement.style.width = CSSProperties.WidthUnionType.of(null);
    switch (fieldType) {
      case NUMERIC:
        inputElement.setAttribute(TYPE, "text");
        attachedHandler = addKeyPressHandler(HandlerFactory.getDecimalKeyPressHandler());
        break;
      case BOOLEAN:
        inputElement.setAttribute(TYPE, "checkbox");
        inputElement.style.width = CSSProperties.WidthUnionType.of("auto");
        break;
      case DATE:
        inputElement.setAttribute(TYPE, "date");
        break;
      case TIME:
        inputElement.setAttribute(TYPE, "time");
        break;
      case DATETIME:
        inputElement.setAttribute(TYPE, "datetime-local");
        break;
      default:
        inputElement.setAttribute(TYPE, "text");
        break;
    }
  }

  @Override
  public String getValueOrThrow() throws ParseException {
    final HTMLInputElement inputElement = getInputElement();
    final ValidityState validityState = inputElement.validity;
    if (validityState.typeMismatch) {
      throw new ParseException("invalid input", 0);
    }
    final String valueAsString = inputElement.value;
    switch (fieldType) {
      case NUMERIC:
        if (StringUtils.isEmpty(valueAsString)) {
          return null;
        }
        return Double.toString(DoubleParser.instance().parse(valueAsString));
      case BOOLEAN:
        return Boolean.toString(inputElement.checked);
      case DATE:
        if (StringUtils.isEmpty(valueAsString)) {
          return null;
        }
        return DateRenderer.instance().render(DateParser.instance().parse(valueAsString));
      case TIME:
        if (StringUtils.isEmpty(valueAsString)) {
          return null;
        }
        return TimeRenderer.instance().render(TimeParser.instance().parse(valueAsString));
      case DATETIME:
        if (StringUtils.isEmpty(valueAsString)) {
          return null;
        }
        return DateTimeLocalRenderer.instance()
            .render(DateTimeLocalParser.instance().parse(valueAsString));
      default:
        return StringUtils.trimToNull(valueAsString);
    }
  }

  @Override
  public void setValue(final String value, final boolean fireEvents) {
    final String oldValue = getValue();
    final HTMLInputElement inputElement = getInputElement();
    switch (fieldType) {
      case NUMERIC:
        if (StringUtils.isEmpty(value) || !value.matches("^-{0,1}[0-9.]*$")) {
          inputElement.value = value;
        } else {
          inputElement.value = DoubleRenderer.instance().render(Double.parseDouble(value));
        }
        break;
      case BOOLEAN:
        inputElement.checked = StringUtils.equalsIgnoreCase(value, "true");
        break;
      default:
        inputElement.value = value;
        break;
    }
    if (fireEvents) {
      ValueChangeEvent.fireIfNotEqual(this, oldValue, value);
    }
  }
}

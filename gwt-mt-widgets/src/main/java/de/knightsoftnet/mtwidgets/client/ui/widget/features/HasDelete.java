package de.knightsoftnet.mtwidgets.client.ui.widget.features;

public interface HasDelete {
  void deleteEntry();
}

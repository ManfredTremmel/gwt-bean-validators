/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.mtwidgets.client.ui.widget;

import de.knightsoftnet.mtwidgets.client.ui.widget.features.HasAutocomplete;
import de.knightsoftnet.mtwidgets.client.ui.widget.features.HasAutofocus;
import de.knightsoftnet.mtwidgets.client.ui.widget.features.HasDataList;
import de.knightsoftnet.mtwidgets.client.ui.widget.features.HasFormNoValidate;
import de.knightsoftnet.mtwidgets.client.ui.widget.features.HasPlaceholder;
import de.knightsoftnet.mtwidgets.client.ui.widget.features.HasRequired;
import de.knightsoftnet.mtwidgets.client.ui.widget.features.HasValidationMessageElement;
import de.knightsoftnet.mtwidgets.client.ui.widget.features.HasValidationPattern;
import de.knightsoftnet.mtwidgets.client.ui.widget.features.HasValidity;
import de.knightsoftnet.mtwidgets.client.ui.widget.helper.FeatureCheck;
import de.knightsoftnet.validators.client.editor.ValueBoxBase;
import de.knightsoftnet.validators.client.editor.ValueBoxEditor;

import com.google.gwt.dom.client.Element;
import com.google.gwt.text.shared.Parser;
import com.google.gwt.text.shared.Renderer;
import com.google.gwt.user.client.ui.HTMLPanel;

import elemental2.dom.HTMLInputElement;
import elemental2.dom.ValidityState;

import jsinterop.base.Js;

/**
 * Text input box with editor error handling.
 */
public class ValueBoxBaseWithEditorErrors<T> extends ValueBoxBase<T> implements
    HasValidationMessageElement<T, ValueBoxEditor<T>>, HasAutofocus, HasRequired, HasValidity,
    HasPlaceholder, HasValidationPattern, HasFormNoValidate, HasDataList, HasAutocomplete {

  protected HTMLPanel validationMessageElement;

  protected ValueBoxBaseWithEditorErrors(final Element elem, final Renderer<T> renderer,
      final Parser<T> parser) {
    super(elem, renderer, parser);
  }

  public ValueBoxBaseWithEditorErrors(final HTMLInputElement element, final Renderer<T> renderer,
      final Parser<T> parser) {
    super(Js.<Element>uncheckedCast(element), renderer, parser);
  }

  /**
   * set custom validity if supported.
   *
   * @param message message to set
   */
  @Override
  public void setCustomValidity(final String message) {
    if (FeatureCheck.supportCustomValidity(getInputElement())) {
      getInputElement().setCustomValidity(message);
    }
  }

  public HTMLInputElement getInputElement() {
    return Js.<HTMLInputElement>uncheckedCast(getElement());
  }

  @Override
  public String getValidationMessage() {
    return getInputElement().validationMessage;
  }

  @Override
  public ValidityState getValidity() {
    return getInputElement().validity;
  }

  @Override
  public boolean checkValidity() {
    return getInputElement().checkValidity();
  }

  @Override
  public boolean isFormNoValidate() {
    return getInputElement().formNoValidate;
  }

  @Override
  public void setFormNoValidate(final boolean arg) {
    getInputElement().formNoValidate = arg;
  }

  @Override
  public boolean isRequired() {
    return getInputElement().required;
  }

  @Override
  public void setRequired(final boolean arg) {
    getInputElement().required = arg;
  }

  @Override
  public String getPattern() {
    return getInputElement().pattern;
  }

  @Override
  public void setPattern(final String arg) {
    getInputElement().pattern = arg;
  }


  @Override
  public String getPlaceholder() {
    return getInputElement().placeholder;
  }

  @Override
  public void setPlaceholder(final String arg) {
    getInputElement().placeholder = arg;
  }

  @Override
  public boolean isAutofocus() {
    return getInputElement().autofocus;
  }

  @Override
  public void setAutofocus(final boolean arg) {
    getInputElement().autofocus = arg;
  }

  @Override
  public void setValidationMessageElement(final HTMLPanel element) {
    validationMessageElement = element;
  }

  @Override
  public HTMLPanel getValidationMessageElement() {
    return validationMessageElement;
  }

  @Override
  public void setDataListWidget(final DataListWidget dataListWidget) {
    getInputElement().setAttribute("list", dataListWidget.getElement().getId());
  }

  @Override
  public String getAutocomplete() {
    return getInputElement().autocomplete;
  }

  @Override
  public void setAutocomplete(final String arg) {
    getInputElement().autocomplete = arg;
  }
}

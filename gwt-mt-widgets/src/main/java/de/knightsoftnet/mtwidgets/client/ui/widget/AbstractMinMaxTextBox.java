/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.mtwidgets.client.ui.widget;

import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.text.shared.Parser;
import com.google.gwt.text.shared.Renderer;

import elemental2.dom.DomGlobal;
import elemental2.dom.HTMLInputElement;

import org.apache.commons.lang3.StringUtils;

import java.text.ParseException;

/**
 * number box for input without separators.
 *
 * @author Manfred Tremmel
 *
 * @param <T> type of the value
 */
public abstract class AbstractMinMaxTextBox<T> extends ValueBox<T> {

  protected final Renderer<T> numberRenderer;
  protected final Parser<T> numberParser;

  /**
   * constructor.
   *
   * @param numberRenderer number renderer
   * @param numberParser number parser
   * @param keyPressHandler key press handler
   */
  protected AbstractMinMaxTextBox(final Renderer<T> numberRenderer, final Parser<T> numberParser,
      final KeyPressHandler keyPressHandler) {
    this((HTMLInputElement) DomGlobal.document.createElement("input"), "number", numberRenderer,
        numberParser, numberRenderer, numberParser, keyPressHandler);
  }

  /**
   * constructor.
   *
   * @param element input element
   * @param type type of the input element
   * @param numberRenderer number renderer
   * @param numberParser number parser
   * @param keyPressHandler key press handler
   */
  protected AbstractMinMaxTextBox(final HTMLInputElement element, final String type,
      final Renderer<T> numberRenderer, final Parser<T> numberParser,
      final KeyPressHandler keyPressHandler) {
    this(element, type, numberRenderer, numberParser, numberRenderer, numberParser,
        keyPressHandler);
  }

  /**
   * constructor.
   *
   * @param numberRenderer number renderer
   * @param numberParser number parser
   * @param plainNumberRenderer number renderer for internal usage
   * @param plainNumberParser number parser for internal usage
   * @param keyPressHandler key press handler
   */
  protected AbstractMinMaxTextBox(final Renderer<T> numberRenderer, final Parser<T> numberParser,
      final Renderer<T> plainNumberRenderer, final Parser<T> plainNumberParser,
      final KeyPressHandler keyPressHandler) {
    this((HTMLInputElement) DomGlobal.document.createElement("input"), "number", numberRenderer,
        numberParser, plainNumberRenderer, plainNumberParser, keyPressHandler);
  }

  /**
   * constructor.
   *
   * @param element input element
   * @param type type of the input element
   * @param numberRenderer number renderer
   * @param numberParser number parser
   * @param plainNumberRenderer number renderer for internal usage
   * @param plainNumberParser number parser for internal usage
   * @param keyPressHandler key press handler
   */
  protected AbstractMinMaxTextBox(final HTMLInputElement element, final String type,
      final Renderer<T> numberRenderer, final Parser<T> numberParser,
      final Renderer<T> plainNumberRenderer, final Parser<T> plainNumberParser,
      final KeyPressHandler keyPressHandler) {
    super(element, type, numberRenderer, numberParser);
    this.numberRenderer = plainNumberRenderer;
    this.numberParser = plainNumberParser;
    if (keyPressHandler != null) {
      addKeyPressHandler(keyPressHandler);
    }
  }

  /**
   * set minimum allowed value.
   *
   * @param min minimum value allowed
   */
  public void setMin(final T min) {
    if (min == null) {
      getInputElement().removeAttribute("min");
    } else {
      getInputElement().min = this.numberRenderer.render(min);
    }
  }

  /**
   * set minimum allowed value.
   *
   * @param min minimum value allowed
   */
  public void setMin(final String min) {
    try {
      this.setMin(this.numberParser.parse(min));
    } catch (final ParseException e) {
      this.setMin((T) null);
    }
  }

  /**
   * get minimum allowed value.
   *
   * @return minimum value allowed
   */
  public T getMin() {
    if (StringUtils.isEmpty(getInputElement().min)) {
      return null;
    }
    try {
      return this.numberParser.parse(getInputElement().min);
    } catch (final ParseException e) {
      return null;
    }
  }

  /**
   * set maximum allowed value.
   *
   * @param max maximum value allowed
   */
  public void setMax(final T max) {
    if (max == null) {
      getInputElement().removeAttribute("max");
    } else {
      getInputElement().max = this.numberRenderer.render(max);
    }
  }

  /**
   * set maximum allowed value.
   *
   * @param max maximum value allowed
   */
  public void setMax(final String max) {
    try {
      this.setMax(this.numberParser.parse(max));
    } catch (final ParseException e) {
      this.setMax((T) null);
    }
  }


  /**
   * get maximum allowed value.
   *
   * @return maximum value allowed
   */
  public T getMax() {
    if (StringUtils.isEmpty(getInputElement().max)) {
      return null;
    }
    try {
      return this.numberParser.parse(getInputElement().max);
    } catch (final ParseException e) {
      return null;
    }
  }

  /**
   * set distance value should be increased/decreased when using up/down buttons.
   *
   * @param step step distance
   */
  public void setStep(final Integer step) {
    if (step == null) {
      getInputElement().removeAttribute("step");
    } else {
      getInputElement().step = step.toString();
    }
  }

  /**
   * set distance value should be increased/decreased when using up/down buttons.
   *
   * @param step step distance
   */
  public void setStep(final String step) {
    try {
      this.setStep(Integer.valueOf(step));
    } catch (final NumberFormatException e) {
      this.setStep((Integer) null);
    }
  }


  /**
   * get distance value should be increased/decreased when using up/down buttons.
   *
   * @return maximum value allowed
   */
  public Integer getStep() {
    if (StringUtils.isEmpty(getInputElement().step)) {
      return null;
    }
    try {
      return Integer.valueOf(getInputElement().step);
    } catch (final NumberFormatException e) {
      return null;
    }
  }
}

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.mtwidgets.client.ui.widget;

import de.knightsoftnet.mtwidgets.client.ui.widget.features.HasAutofocus;
import de.knightsoftnet.mtwidgets.client.ui.widget.features.HasRequired;
import de.knightsoftnet.mtwidgets.client.ui.widget.features.HasValidationMessageElement;
import de.knightsoftnet.mtwidgets.client.ui.widget.helper.FeatureCheck;
import de.knightsoftnet.mtwidgets.client.ui.widget.helper.IdAndNameBean;
import de.knightsoftnet.validators.client.decorators.ExtendedValueBoxEditor;
import de.knightsoftnet.validators.client.editor.ValueBoxEditor;

import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.ListBox;

import elemental2.dom.HTMLSelectElement;

import org.gwtproject.editor.client.HasEditorErrors;
import org.gwtproject.editor.client.TakesValue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import jsinterop.base.Js;

/**
 * a list box with id and name.
 *
 * @author Manfred Tremmel
 *
 */
public class IdAndNameListBox<T> extends ListBox
    implements HasValue<T>, TakesValue<T>, HasEditorErrors<T>,
    HasValidationMessageElement<T, ValueBoxEditor<T>>, HasAutofocus, HasRequired {
  private boolean valueChangeHandlerInitialized;
  protected final List<IdAndNameBean<T>> entries;
  private final ValueBoxEditor<T> editor;

  private HTMLPanel validationMessageElement;
  private boolean required;
  private T defaultValue;

  /**
   * constructor.
   */
  public IdAndNameListBox() {
    this(Collections.emptyList());
  }

  /**
   * constructor.
   *
   * @param entries collection of id and name bean entries
   */
  public IdAndNameListBox(final Collection<? extends IdAndNameBean<T>> entries) {
    super();
    required = false;
    this.entries = new ArrayList<>();
    editor = new ExtendedValueBoxEditor<>(this, null);
    this.fillEntryCollections(entries);
  }

  /**
   * fill entries of the listbox.
   *
   * @param entries list of entries
   */
  public final void fillEntryCollections(final Collection<? extends IdAndNameBean<T>> entries) {
    final T rememberValue = getValue();
    this.entries.clear();
    if (entries != null && entries.size() > 0) {
      this.entries.addAll(entries);
      defaultValue = entries.iterator().next().getId();
    }

    clear();
    for (final IdAndNameBean<T> entry : this.entries) {
      this.addItem(entry.getName(), Objects.toString(entry.getId()));
    }
    setValue(rememberValue, false);
  }

  @Override
  public HandlerRegistration addValueChangeHandler(final ValueChangeHandler<T> handler) {
    // Is this the first value change handler? If so, time to add handlers
    if (!valueChangeHandlerInitialized) {
      this.ensureDomEventHandlers();
      valueChangeHandlerInitialized = true;
    }
    return this.addHandler(handler, ValueChangeEvent.getType());
  }

  protected void ensureDomEventHandlers() {
    addChangeHandler(event -> ValueChangeEvent.fire(this, this.getValue()));
  }

  @Override
  public T getValue() {
    if (getSelectedIndex() >= 0 && getSelectedIndex() < entries.size()) {
      return entries.get(getSelectedIndex()).getId();
    }
    return null;
  }

  @Override
  public void setValue(final T value) {
    this.setValue(value, false);
  }

  @Override
  public void setValue(final T value, final boolean fireEvents) {
    int pos = 0;
    final T oldValue = this.getValue();
    final T newValue = required && value == null ? defaultValue : value;
    for (final IdAndNameBean<T> entry : entries) {
      if (Objects.equals(newValue, entry.getId())) {
        setSelectedIndex(pos);
        if (fireEvents) {
          ValueChangeEvent.fireIfNotEqual(this, oldValue, newValue);
        }
        return;
      }
      pos++;
    }
    setSelectedIndex(-1);
    if (fireEvents) {
      ValueChangeEvent.fireIfNotEqual(this, oldValue, null);
    }
  }


  private HTMLSelectElement getInputElement() {
    return Js.<HTMLSelectElement>uncheckedCast(getElement());
  }

  @Override
  public ValueBoxEditor<T> asEditor() {
    return editor;
  }

  @Override
  public boolean isAutofocus() {
    return getInputElement().autofocus;
  }

  @Override
  public void setAutofocus(final boolean arg) {
    getInputElement().autofocus = arg;
  }

  @Override
  public void setValidationMessageElement(final HTMLPanel element) {
    validationMessageElement = element;
  }

  @Override
  public HTMLPanel getValidationMessageElement() {
    return validationMessageElement;
  }

  @Override
  public void setCustomValidity(final String message) {
    if (FeatureCheck.supportCustomValidity(getInputElement())) {
      getInputElement().setCustomValidity(message);
    }
  }

  @Override
  public boolean isRequired() {
    return required;
  }

  @Override
  public void setRequired(final boolean required) {
    this.required = required;
  }

  public T getDefaultValue() {
    return defaultValue;
  }

  public void setDefaultValue(final T defaultValue) {
    this.defaultValue = defaultValue;
  }
}

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.mtwidgets.client.ui.widget.helper;

import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;

import java.util.Objects;

/**
 * helper bean for sorting entries.
 *
 * @author Manfred Tremmel
 *
 */
public class IdAndNameBean<T> implements Suggestion {
  private final T id;
  private final String name;

  /**
   * constructor initializing fields.
   *
   * @param id id of the entry
   * @param name name of the entry
   */
  public IdAndNameBean(final T id, final String name) {
    super();
    this.id = id;
    this.name = name;
  }

  public final T getId() {
    return id;
  }

  public final String getName() {
    return name;
  }

  @Override
  public String getDisplayString() {
    return name;
  }

  @Override
  public String getReplacementString() {
    return Objects.toString(id, null);
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(id);
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (this.getClass() != obj.getClass()) {
      return false;
    }
    final IdAndNameBean<?> other = (IdAndNameBean<?>) obj;
    return Objects.equals(id, other.id);
  }
}

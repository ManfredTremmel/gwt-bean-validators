/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.mtwidgets.client.ui.widget.helper;

import de.knightsoftnet.mtwidgets.client.event.ListPositionChangeEvent;

import org.gwtproject.editor.client.adapters.EditorSource;

/**
 * Abstract implementation of DataSource for a ListItem editor.
 *
 * @author Manfred Tremmel
 *
 * @param <D> type of data to edit
 * @param <V> type of the view of a single item
 */
public abstract class AbstractItemEditorSource<D, V extends AbstractListItemView<D>>
    extends EditorSource<V> {

  private final AbstractListEditor<D, V> listEditor;

  protected AbstractItemEditorSource(final AbstractListEditor<D, V> listEditor) {
    super();
    this.listEditor = listEditor;
  }

  @Override
  public V create(final int index) {
    final V subEditor = this.createItemView(index);
    listEditor.insert(subEditor, index);
    subEditor.addEditorDeleteHandler(event -> listEditor.removeEntry(subEditor));
    subEditor.addListPositionChangeHandler(event -> moveEntry(subEditor, event));
    return subEditor;
  }

  @Override
  public void dispose(final V subEditor) {
    subEditor.removeFromParent();
  }

  @Override
  public void setIndex(final V editor, final int index) {
    listEditor.insert(editor, index);
  }

  private void moveEntry(final V subEditor, final ListPositionChangeEvent<D> event) {
    int newPos = switch (event.getPositionChange()) {
      case UP -> listEditor.getWidgetIndex(subEditor) - 1;
      case DOWN -> listEditor.getWidgetIndex(subEditor) + 1;
      default -> event.getNewPosition();
    };
    if (newPos < 0) {
      newPos = 0;
    } else if (newPos >= listEditor.getWidgetCount()) {
      newPos = listEditor.getWidgetCount() - 1;
    }
    listEditor.moveEntry(subEditor, newPos);
  }

  /**
   * create new instance of a item view.
   *
   * @param index of the item
   * @return item view
   */
  protected abstract V createItemView(final int index);
}

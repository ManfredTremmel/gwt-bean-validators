/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.mtwidgets.client.ui.widget;

import de.knightsoftnet.mtwidgets.client.ui.widget.helper.IdAndNameBean;
import de.knightsoftnet.mtwidgets.client.ui.widget.helper.PageNumberMessages;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiConstructor;

import java.util.ArrayList;
import java.util.List;

/**
 * list box to display and select page numbers.
 *
 * @author Manfred Tremmel
 */
public class PageNumberListBox extends IdAndNameListBox<Integer> {

  private static final PageNumberMessages MESSAGES = GWT.create(PageNumberMessages.class);

  /**
   * widget ui constructor.
   */
  @UiConstructor
  public PageNumberListBox() {
    super(List.of(new IdAndNameBean<>(Integer.valueOf(0), MESSAGES.name(1, 1))));
  }

  /**
   * set number pages, for each page a entry is created.
   *
   * @param numPages number of pages
   */
  public void setNumPages(final int numPages) {
    final List<IdAndNameBean<Integer>> newEntries = new ArrayList<>(numPages);
    for (int i = 0; i < numPages; i++) {
      newEntries.add(new IdAndNameBean<>(Integer.valueOf(i), MESSAGES.name(i + 1, numPages)));
    }
    fillEntryCollections(newEntries);
  }
}

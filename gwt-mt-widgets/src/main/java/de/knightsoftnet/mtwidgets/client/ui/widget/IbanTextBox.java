/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.mtwidgets.client.ui.widget;

import de.knightsoftnet.validators.shared.data.ValueWithPos;
import de.knightsoftnet.validators.shared.util.AbstractIbanUtil;
import de.knightsoftnet.validators.shared.util.IbanUtil;

import com.google.gwt.user.client.TakesValue;

import org.apache.commons.lang3.CharUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * input text box for iban.
 *
 * @author Manfred Tremmel
 *
 */
public class IbanTextBox extends AbstractFormatingTextBox {

  private TakesValue<String> bicInput;

  @Override
  public void formatValue(final ValueWithPos<String> value, final boolean fireEvents) {
    setTextWithPos(AbstractIbanUtil.ibanFormatWithPos(value), fireEvents);
  }

  @Override
  public boolean isAllowedCharacter(final char character) {
    return CharUtils.isAscii(character) || CharUtils.isAsciiNumeric(character);
  }

  @Override
  public boolean isCharacterToReplace(final char character) {
    return CharUtils.isAsciiAlphaLower(character);
  }

  @Override
  public boolean isFormatingCharacter(final char character) {
    return character == ' ';
  }

  @Override
  public char replaceCharacter(final char character) {
    return Character.toUpperCase(character);
  }

  @Override
  protected void setTextWithPos(final ValueWithPos<String> formatedEntry,
      final boolean fireEvents) {
    super.setTextWithPos(formatedEntry, fireEvents);
    if (bicInput != null && StringUtils.isEmpty(bicInput.getValue())) {
      final IbanUtil ibanUtil = new IbanUtil();
      final String bic = ibanUtil.getBicOfIban(formatedEntry.getValue());
      if (StringUtils.isNotEmpty(bic)) {
        bicInput.setValue(bic);
      }
    }
  }

  @Override
  public String formatValueSynchron(final String value) {
    return AbstractIbanUtil.ibanFormat(value);
  }

  public final TakesValue<String> getBicInput() {
    return bicInput;
  }

  public final void setBicInput(final TakesValue<String> bicInput) {
    this.bicInput = bicInput;
  }
}

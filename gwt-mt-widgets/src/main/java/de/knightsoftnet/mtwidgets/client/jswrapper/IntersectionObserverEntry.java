package de.knightsoftnet.mtwidgets.client.jswrapper;

import elemental2.dom.HTMLElement;

import jsinterop.annotations.JsProperty;
import jsinterop.annotations.JsType;

@JsType(isNative = true)
public class IntersectionObserverEntry {

  @JsProperty
  public HTMLElement target;

  @JsProperty
  public double intersectionRatio;
}

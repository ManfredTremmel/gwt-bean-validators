/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.mtwidgets.client.ui.widget;

import de.knightsoftnet.mtwidgets.client.ui.widget.features.HasValidationMessageElement;
import de.knightsoftnet.mtwidgets.client.ui.widget.helper.FeatureCheck;
import de.knightsoftnet.mtwidgets.client.ui.widget.styling.RatingInputStyle;
import de.knightsoftnet.validators.client.decorators.ExtendedValueBoxEditor;
import de.knightsoftnet.validators.client.editor.ValueBoxEditor;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.uibinder.client.UiConstructor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Focusable;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.HasValue;

import elemental2.dom.Element;
import elemental2.dom.HTMLElement;
import elemental2.dom.HTMLInputElement;
import elemental2.dom.NodeList;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.gwtproject.editor.client.TakesValue;

import jsinterop.base.Js;

/**
 * input field for star ratings.
 *
 * @author Manfred Tremmel
 *
 */
@SuppressWarnings("CPD-START")
public class RatingInputWidget extends Composite implements HasValue<Integer>, TakesValue<Integer>,
    Focusable, HasValidationMessageElement<Integer, ValueBoxEditor<Integer>> {

  private boolean valueChangeHandlerInitialized;
  private final FlowPanel panel;
  private final ValueBoxEditor<Integer> editor;

  private HTMLPanel validationMessageElement;

  /**
   * A ClientBundle that provides images and style sheets for the decorator.
   */
  public interface Resources extends ClientBundle {

    /**
     * The styles used in this widget.
     *
     * @return decorator style
     */
    @Source("styling/RatingInputStyle.gss")
    RatingInputStyle ratingInputStyle();
  }

  /**
   * the default resources.
   */
  private static volatile Resources defaultResource;

  /**
   * default constructor.
   *
   * @param max the number stars to display
   * @param idBase the id of the widget and base for the ids of the single radio buttons
   */
  @UiConstructor
  public RatingInputWidget(final int max, final String idBase) {
    this(max, idBase, getDefaultResources());
  }

  /**
   * constructor with styling information.
   *
   * @param max the number stars to display
   * @param idBase the id of the widget and base for the ids of the single radio buttons
   * @param resource resources with styling information
   */
  public RatingInputWidget(final int max, final String idBase, final Resources resource) {
    super();
    resource.ratingInputStyle().ensureInjected();
    panel = new FlowPanel();
    panel.setStylePrimaryName(resource.ratingInputStyle().ratingArea());
    panel.getElement().setId(idBase);
    initWidget(panel);
    editor = new ExtendedValueBoxEditor<>(this, null);
    for (int i = max; i > 0; i--) {
      final RadioButton radioButton = new RadioButton();
      radioButton.getElement().setId(idBase + Integer.toString(i));
      radioButton.setFormValue(Integer.toString(i));
      radioButton.setName(idBase);
      panel.add(radioButton);
      final InputLabel label = new InputLabel();
      label.setFor(radioButton);
      label.setText(StringUtils.SPACE);
      panel.add(label);
    }
  }

  /**
   * get default resource, if not set, create one.
   *
   * @return default resource.
   */
  protected static Resources getDefaultResources() { // NOPMD it's thread save!
    if (defaultResource == null) {
      synchronized (Resources.class) {
        if (defaultResource == null) {
          defaultResource = GWT.create(Resources.class);
        }
      }
    }
    return defaultResource;
  }

  @Override
  public void setValue(final Integer value) {
    this.setValue(value, false);
  }

  @Override
  public void setValue(final Integer value, final boolean fireEvents) {
    final Integer oldValue = getValue();
    if (value == null) {
      for (int i = 0; i < panel.getWidgetCount(); i++) {
        if (panel.getWidget(i) instanceof final RadioButton radioButton) {
          radioButton.setValue(Boolean.FALSE, false);
        }
      }
    } else {
      for (int i = 0; i < panel.getWidgetCount(); i++) {
        if (panel.getWidget(i) instanceof final RadioButton radioButton
            && StringUtils.equals(value.toString(), radioButton.getFormValue())) {
          radioButton.setValue(Boolean.TRUE, false);
          break;
        }
      }
    }
    if (fireEvents) {
      ValueChangeEvent.fireIfNotEqual(this, oldValue, value);
    }
  }

  @Override
  public Integer getValue() {
    for (int i = 0; i < panel.getWidgetCount(); i++) {
      if (panel.getWidget(i) instanceof final RadioButton radioButton
          && BooleanUtils.isTrue(radioButton.getValue())) {
        return Integer.valueOf(radioButton.getFormValue());
      }
    }
    return null;
  }

  @Override
  public HandlerRegistration addValueChangeHandler(final ValueChangeHandler<Integer> handler) {
    // Is this the first value change handler? If so, time to add handlers
    if (!valueChangeHandlerInitialized) {
      ensureDomEventHandlers();
      valueChangeHandlerInitialized = true;
    }
    return this.addHandler(handler, ValueChangeEvent.getType());
  }

  protected void ensureDomEventHandlers() {
    for (int i = 0; i < panel.getWidgetCount(); i++) {
      if (panel.getWidget(i) instanceof final RadioButton radioButton) {
        radioButton.addValueChangeHandler(event -> ValueChangeEvent.fire(this, getValue()));
      }
    }
  }

  @Override
  public void setTabIndex(final int index) {
    int indexCount = index;
    for (int i = 0; i < panel.getWidgetCount(); i++) {
      if (panel.getWidget(i) instanceof final RadioButton radioButton) {
        radioButton.setTabIndex(indexCount++);
      }
    }
  }

  @Override
  public int getTabIndex() {
    for (int i = 0; i < panel.getWidgetCount(); i++) {
      if (panel.getWidget(i) instanceof final RadioButton radioButton) {
        return radioButton.getTabIndex();
      }
    }
    return -1;
  }

  @Override
  public void setAccessKey(final char key) {
    for (int i = 0; i < panel.getWidgetCount(); i++) {
      if (panel.getWidget(i) instanceof final RadioButton radioButton) {
        radioButton.setAccessKey(key);
        return;
      }
    }
  }

  @Override
  public void setFocus(final boolean focused) {
    for (int i = 0; i < panel.getWidgetCount(); i++) {
      if (panel.getWidget(i) instanceof final RadioButton radioButton) {
        radioButton.setFocus(focused);
        return;
      }
    }
  }

  @Override
  public ValueBoxEditor<Integer> asEditor() {
    return editor;
  }

  @Override
  public void setCustomValidity(final String message) {
    final HTMLElement headElement = Js.<HTMLElement>uncheckedCast(getElement());
    final NodeList<Element> inputElements = headElement.getElementsByTagName("input");
    inputElements.asList().forEach(element -> {
      if (FeatureCheck.supportCustomValidity(element)) {
        ((HTMLInputElement) element).setCustomValidity(message);
      }
    });
  }

  @Override
  public void setTitle(final String title) {
    final HTMLElement headElement = Js.<HTMLElement>uncheckedCast(getElement());
    final NodeList<Element> inputElements = headElement.getElementsByTagName("input");
    inputElements.asList().forEach(element -> {
      ((HTMLInputElement) element).title = title;
    });
  }

  @Override
  public void setValidationMessageElement(final HTMLPanel element) {
    validationMessageElement = element;
  }

  @Override
  public HTMLPanel getValidationMessageElement() {
    return validationMessageElement;
  }
}

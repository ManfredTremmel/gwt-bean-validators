package de.knightsoftnet.mtwidgets.client.ui.widget.resourceloader;

import elemental2.dom.DomGlobal;
import elemental2.dom.Element;
import elemental2.dom.HTMLLinkElement;
import elemental2.dom.NodeList;

import org.apache.commons.lang3.StringUtils;

public class CssResources {

  private static final String TAG_TYPE = "link";
  private static final String SCRIPT_TYPE = "text/css";
  private static final String REL_TYPE = "stylesheet";

  /**
   * add css script to header.
   *
   * @param scriptname style sheet file to add to header
   */
  public static void addToHeader(final String scriptname) {
    if (!isInHeader(scriptname)) {
      final HTMLLinkElement styleLinkElement =
          (HTMLLinkElement) DomGlobal.document.createElement("link");
      styleLinkElement.rel = REL_TYPE;
      styleLinkElement.type = SCRIPT_TYPE;
      styleLinkElement.href = scriptname;
      DomGlobal.document.head.appendChild(styleLinkElement);
    }
  }

  /**
   * check if script is already in the header.
   *
   * @return true if it exists
   */
  public static boolean isInHeader(final String scriptname) {
    final NodeList<Element> linkList = DomGlobal.document.head.getElementsByTagName(TAG_TYPE);
    return linkList.asList().stream()
        .filter(element -> StringUtils.equals(((HTMLLinkElement) element).type, SCRIPT_TYPE)
            && StringUtils.equals(((HTMLLinkElement) element).rel, REL_TYPE)
            && StringUtils.contains(((HTMLLinkElement) element).href, scriptname))
        .findFirst().isPresent();
  }
}

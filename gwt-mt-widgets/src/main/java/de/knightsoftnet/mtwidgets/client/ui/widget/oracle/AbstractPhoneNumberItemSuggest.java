/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.mtwidgets.client.ui.widget.oracle;

import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;

/**
 * abstract suggest entry of phone number suggest widgets.
 *
 * @author Manfred Tremmel
 *
 */
public abstract class AbstractPhoneNumberItemSuggest implements Suggestion {

  private String countryCode;
  private String countryName;
  private String areaCode;
  private String areaName;

  /**
   * default constructor.
   */
  protected AbstractPhoneNumberItemSuggest() {
    this(null, null, null, null);
  }

  /**
   * constructor initializing fields.
   *
   *
   * @param countryCode country code to set
   * @param countryName country name to set
   * @param areaCode area code to set
   * @param areaName area name to set
   */
  protected AbstractPhoneNumberItemSuggest(final String countryCode, final String countryName,
      final String areaCode, final String areaName) {
    super();
    this.countryCode = countryCode;
    this.countryName = countryName;
    this.areaCode = areaCode;
    this.areaName = areaName;
  }

  @Override
  public abstract String getDisplayString();

  @Override
  public abstract String getReplacementString();

  public final String getCountryCode() {
    return countryCode;
  }

  public final void setCountryCode(final String countryCode) {
    this.countryCode = countryCode;
  }

  public final String getCountryName() {
    return countryName;
  }

  public final void setCountryName(final String countryName) {
    this.countryName = countryName;
  }

  public final String getAreaCode() {
    return areaCode;
  }

  public final void setAreaCode(final String areaCode) {
    this.areaCode = areaCode;
  }

  public final String getAreaName() {
    return areaName;
  }

  public final void setAreaName(final String areaName) {
    this.areaName = areaName;
  }
}

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.mtwidgets.client.ui.widget.features;

import de.knightsoftnet.mtwidgets.client.ui.widget.helper.IdAndNameBean;
import de.knightsoftnet.mtwidgets.client.ui.widget.helper.IdAndNameIdComperator;
import de.knightsoftnet.mtwidgets.client.ui.widget.helper.IdAndNameNameComperator;
import de.knightsoftnet.mtwidgets.client.ui.widget.helper.ListSortEnum;
import de.knightsoftnet.mtwidgets.client.ui.widget.helper.MessagesForValues;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * functional interface for sortable id and name widgets.
 *
 * @author Manfred Tremmel
 *
 * @param <T> type of the id
 */
public interface SortableIdAndName<T extends Comparable<T>> {

  /**
   * fill entry collections (possible values).
   *
   * @param entries collection of id and name beans
   */
  void fillEntryCollections(final Collection<? extends IdAndNameBean<T>> entries);

  /**
   * fill entries of the listbox.
   *
   * @param ids list of entries
   */
  void fillEntries(final Collection<T> ids);

  /**
   * fill entries of the listbox.
   *
   * @param sortOrder how to sort entries
   * @param messages corresponding to the id's
   * @param ids list of entries
   */
  default void fillEntries(final ListSortEnum sortOrder, final MessagesForValues<T> messages,
      final Collection<T> ids) {
    final List<IdAndNameBean<T>> entries = new ArrayList<>();
    for (final T proEnum : ids) {
      entries.add(new IdAndNameBean<>(proEnum, messages.name(proEnum)));
    }
    if (sortOrder != null) {
      switch (sortOrder) {
        case ID_ASC:
          Collections.sort(entries, new IdAndNameIdComperator<T>());
          break;
        case ID_DSC:
          Collections.sort(entries, Collections.reverseOrder(new IdAndNameIdComperator<T>()));
          break;
        case NAME_ASC:
          Collections.sort(entries, new IdAndNameNameComperator<T>());
          break;
        case NAME_DSC:
          Collections.sort(entries, Collections.reverseOrder(new IdAndNameNameComperator<T>()));
          break;
        default:
          break;
      }
    }
    fillEntryCollections(entries);
  }
}

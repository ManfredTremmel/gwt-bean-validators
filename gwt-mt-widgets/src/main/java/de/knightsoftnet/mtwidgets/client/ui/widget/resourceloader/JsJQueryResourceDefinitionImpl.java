package de.knightsoftnet.mtwidgets.client.ui.widget.resourceloader;

public class JsJQueryResourceDefinitionImpl implements JsJQueryResourceDefinitionInterface {
  @Override
  public String getJQeryPath() {
    return "//cdn.jsdelivr.net/npm/jquery@3/dist/jquery.min.js";
  }

  @Override
  public String getJQeryMigratePath() {
    return "//cdn.jsdelivr.net/npm/jquery-migrate@3/dist/jquery-migrate.min.js";
  }
}

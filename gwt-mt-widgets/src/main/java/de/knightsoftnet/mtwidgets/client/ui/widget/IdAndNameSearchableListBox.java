/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.mtwidgets.client.ui.widget;

import de.knightsoftnet.mtwidgets.client.ui.widget.features.HasAutocomplete;
import de.knightsoftnet.mtwidgets.client.ui.widget.features.HasAutofocus;
import de.knightsoftnet.mtwidgets.client.ui.widget.features.HasPlaceholder;
import de.knightsoftnet.mtwidgets.client.ui.widget.features.HasRequired;
import de.knightsoftnet.mtwidgets.client.ui.widget.features.HasValidationMessageElement;
import de.knightsoftnet.mtwidgets.client.ui.widget.features.HasValidity;
import de.knightsoftnet.mtwidgets.client.ui.widget.helper.IdAndNameBean;
import de.knightsoftnet.validators.client.decorators.ExtendedValueBoxEditor;
import de.knightsoftnet.validators.client.editor.ValueBoxEditor;

import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.Panel;

import elemental2.dom.ValidityState;

import org.apache.commons.lang3.StringUtils;
import org.gwtproject.editor.client.EditorError;
import org.gwtproject.editor.client.TakesValue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * a searchable list box with id and name, based on input text box and HTML5 data list.
 *
 * @author Manfred Tremmel
 *
 */
public class IdAndNameSearchableListBox<T> extends Composite
    implements HasValue<T>, TakesValue<T>, HasValidationMessageElement<T, ValueBoxEditor<T>>,
    HasAutofocus, HasRequired, HasValidity, HasPlaceholder, HasAutocomplete {
  private boolean valueChangeHandlerInitialized;
  protected final List<IdAndNameBean<T>> entries;
  private final ValueBoxEditor<T> editor;

  private final Panel panel;
  private final TextBox input;
  private final DataListWidget dataList;

  /**
   * constructor.
   */
  public IdAndNameSearchableListBox() {
    this(Collections.emptyList());
  }

  /**
   * constructor.
   *
   * @param entries collection of id and name bean entries
   */
  public IdAndNameSearchableListBox(final Collection<? extends IdAndNameBean<T>> entries) {
    super();
    panel = new FlowPanel();
    input = new TextBox();
    dataList = new DataListWidget();
    panel.add(input);
    panel.add(dataList);
    initWidget(panel);
    this.entries = new ArrayList<>();
    editor = new ExtendedValueBoxEditor<>(this, null);
    this.fillEntryCollections(entries);
  }

  /**
   * fill entries of the listbox.
   *
   * @param entries list of entries
   */
  public final void fillEntryCollections(final Collection<? extends IdAndNameBean<T>> entries) {
    final T rememberValue = getValue();
    this.entries.clear();
    if (entries != null) {
      this.entries.addAll(entries);
    }
    dataList.setValue(this.entries.stream().collect(Collectors.toList()));
    input.setPattern(
        "^(" + this.entries.stream().map(IdAndNameBean::getName).collect(Collectors.joining("|"))
            + ")$");
    setValue(rememberValue, false);
  }

  @Override
  protected void onEnsureDebugId(final String baseId) {
    input.ensureDebugId("input" + baseId);
    dataList.ensureDebugId("list" + baseId);
    input.getElement().setAttribute("list", "list" + baseId);
  }

  @Override
  public HandlerRegistration addValueChangeHandler(final ValueChangeHandler<T> handler) {
    // Is this the first value change handler? If so, time to add handlers
    if (!valueChangeHandlerInitialized) {
      this.ensureDomEventHandlers();
      valueChangeHandlerInitialized = true;
    }
    return this.addHandler(handler, ValueChangeEvent.getType());
  }

  protected void ensureDomEventHandlers() {
    input.addChangeHandler(event -> ValueChangeEvent.fire(this, this.getValue()));
  }

  @Override
  public T getValue() {
    final String text = input.getValue();
    return entries.stream().filter(entry -> StringUtils.equals(entry.getName(), text))
        .map(entry -> entry.getId()).findFirst().orElse(null);
  }

  @Override
  public void setValue(final T value) {
    this.setValue(value, false);
  }

  @Override
  public void setValue(final T value, final boolean fireEvents) {
    input.setValue(entries.stream().filter(entry -> Objects.equals(entry.getId(), value))
        .map(entry -> entry.getName()).findFirst().orElse(null), fireEvents);
  }

  @Override
  public void showErrors(final List<EditorError> errors) {
    input.showErrors(errors);
  }

  @Override
  public ValueBoxEditor<T> asEditor() {
    return editor;
  }

  @Override
  public boolean isAutofocus() {
    return input.isAutofocus();
  }

  @Override
  public void setAutofocus(final boolean arg) {
    input.setAutofocus(arg);
  }

  @Override
  public void setValidationMessageElement(final HTMLPanel element) {
    input.setValidationMessageElement(element);
  }

  @Override
  public HTMLPanel getValidationMessageElement() {
    return input.getValidationMessageElement();
  }

  @Override
  public String getAutocomplete() {
    return input.getAutocomplete();
  }

  @Override
  public void setAutocomplete(final String arg) {
    input.setAutocomplete(arg);
  }

  @Override
  public String getPlaceholder() {
    return input.getPlaceholder();
  }

  @Override
  public void setPlaceholder(final String arg) {
    input.setPlaceholder(arg);
  }

  @Override
  public String getValidationMessage() {
    return input.getValidationMessage();
  }

  @Override
  public ValidityState getValidity() {
    return input.getValidity();
  }

  @Override
  public boolean checkValidity() {
    return input.checkValidity();
  }

  @Override
  public boolean isRequired() {
    return input.isRequired();
  }

  @Override
  public void setRequired(final boolean arg) {
    input.setRequired(arg);
  }

  @Override
  public void setCustomValidity(final String message) {
    input.setCustomValidity(message);
  }
}

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.mtwidgets.client.ui.widget;

import de.knightsoftnet.mtwidgets.client.ui.widget.helper.FeatureCheck;

import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.TextAreaElement;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Widget;

import elemental2.dom.HTMLInputElement;
import elemental2.dom.HTMLTextAreaElement;
import elemental2.dom.ValidityState;

import org.apache.commons.lang3.StringUtils;

import jsinterop.base.Js;

/**
 * A text box that allows multiple lines of text to be entered.
 *
 * <p>
 * &lt;img class='gallery' src='doc-files/TextArea.png'/&gt;
 * </p>
 *
 * <h2>CSS Style Rules</h2>
 * <ul class="css">
 * <li>.gwt-TextArea { primary style }</li>
 * <li>.gwt-TextArea-readonly { dependent style set when the text area is read-only }</li>
 * </ul>
 *
 * <h2>Built-in Bidi Text Support</h2>
 * <p>
 * This widget is capable of automatically adjusting its direction according to the input text. This
 * feature is controlled by {@link #setDirectionEstimator}, and is available by default when at
 * least one of the application's locales is right-to-left.
 * </p>
 */
public class TextArea extends TextBoxBase {

  /**
   * Creates a TextArea widget that wraps an existing &lt;textarea&gt; element.
   *
   * <p>
   * This element must already be attached to the document. If the element is removed from the
   * document, you must call {@link RootPanel#detachNow(Widget)}.
   * </p>
   *
   * @param element the element to be wrapped
   * @return text area widget
   */
  public static TextArea wrap(final Element element) {
    // Assert that the element is attached.
    assert Document.get().getBody().isOrHasChild(element);

    final TextArea textArea = new TextArea(element);

    // Mark it attached and remember it for cleanup.
    textArea.onAttach();
    RootPanel.detachOnWindowClose(textArea);

    return textArea;
  }

  /**
   * Creates an empty text area.
   */
  public TextArea() {
    super(Document.get().createTextAreaElement());
    this.setStyleName("gwt-TextArea");
  }

  /**
   * This constructor may be used by subclasses to explicitly use an existing element. This element
   * must be a &lt;textarea&gt; element.
   *
   * @param element the element to be used
   */
  protected TextArea(final Element element) {
    super(element.<Element>cast());
    TextAreaElement.as(element);
  }

  /**
   * Gets the requested width of the text box (this is not an exact value, as not all characters are
   * created equal).
   *
   * @return the requested width, in characters
   */
  public int getCharacterWidth() {
    return getTextAreaElement().cols;
  }

  @Override
  public int getCursorPos() {
    return getImpl().getTextAreaCursorPos(getElement());
  }

  @Override
  public int getSelectionLength() {
    return getImpl().getTextAreaSelectionLength(getElement());
  }

  /**
   * Gets the number of text lines that are visible.
   *
   * @return the number of visible lines
   */
  public int getVisibleLines() {
    return getTextAreaElement().rows;
  }

  /**
   * Sets the requested width of the text box (this is not an exact value, as not all characters are
   * created equal).
   *
   * @param width the requested width, in characters
   */
  public void setCharacterWidth(final int width) {
    getTextAreaElement().cols = width;
  }

  /**
   * Sets the number of text lines that are visible.
   *
   * @param lines the number of visible lines
   */
  public void setVisibleLines(final int lines) {
    getTextAreaElement().rows = lines;
  }

  private HTMLTextAreaElement getTextAreaElement() {
    return Js.<HTMLTextAreaElement>uncheckedCast(getElement());
  }

  @Override
  public void setCustomValidity(final String message) {
    if (FeatureCheck.supportCustomValidity(getTextAreaElement())) {
      getTextAreaElement().setCustomValidity(message);
    }
  }

  @Override
  public HTMLInputElement getInputElement() {
    return null;
  }

  @Override
  public String getValidationMessage() {
    return getTextAreaElement().validationMessage;
  }

  @Override
  public ValidityState getValidity() {
    return getTextAreaElement().validity;
  }

  @Override
  public boolean checkValidity() {
    return getTextAreaElement().checkValidity();
  }

  @Override
  public boolean isFormNoValidate() {
    return getTextAreaElement().hasAttribute("formnovalidate");
  }

  @Override
  public void setFormNoValidate(final boolean arg) {
    if (arg) {
      getTextAreaElement().setAttribute("formnovalidate", arg);
    } else {
      getTextAreaElement().removeAttribute("formnovalidate");
    }
  }

  @Override
  public boolean isRequired() {
    return getTextAreaElement().hasAttribute("required");
  }

  @Override
  public void setRequired(final boolean arg) {
    if (arg) {
      getTextAreaElement().setAttribute("required", arg);
    } else {
      getTextAreaElement().removeAttribute("required");
    }
  }

  @Override
  public String getPattern() {
    return getTextAreaElement().getAttribute("pattern");
  }

  @Override
  public void setPattern(final String arg) {
    getTextAreaElement().setAttribute("pattern", arg);
  }

  @Override
  public String getPlaceholder() {
    return getTextAreaElement().getAttribute("placeholder");
  }

  @Override
  public void setPlaceholder(final String arg) {
    getTextAreaElement().setAttribute("placeholder", arg);
  }

  @Override
  public boolean isAutofocus() {
    return getTextAreaElement().autofocus;
  }

  @Override
  public void setAutofocus(final boolean arg) {
    getTextAreaElement().autofocus = arg;
  }

  @Override
  public String getAutocomplete() {
    return getTextAreaElement().getAttribute("autocomplete");
  }

  @Override
  public void setAutocomplete(final String arg) {
    getTextAreaElement().setAttribute("autocomplete", arg);
  }

  /**
   * Gets the maximum allowable length of the text box.
   *
   * @return the maximum length, in characters
   */
  public int getMaxLength() {
    final String maxLength = getTextAreaElement().getAttribute("maxlength");
    return StringUtils.isNumeric(maxLength) ? Integer.parseInt(maxLength) : Integer.MAX_VALUE;
  }

  /**
   * Sets the maximum allowable length of the text box.
   *
   * @param length the maximum length, in characters
   */
  public void setMaxLength(final int length) {
    getTextAreaElement().setAttribute("maxlength", length);
  }
}

package de.knightsoftnet.mtwidgets.client.ui.widget.resourceloader;

import elemental2.dom.DomGlobal;
import elemental2.dom.Element;
import elemental2.dom.Event;
import elemental2.dom.EventListener;
import elemental2.dom.HTMLScriptElement;
import elemental2.dom.NodeList;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JsResources {

  protected static final String TAG_TYPE = "script";
  protected static final String SCRIPT_TYPE = "text/javascript";

  protected static Map<String, Boolean> initializationStarted = new HashMap<>();

  protected static Map<String, Event> rememberEvent = new HashMap<>();
  protected static Map<String, List<EventListener>> eventLisenerQueue = new HashMap<>();

  /**
   * async load of resources.
   *
   * @param function function to call on load
   */
  public static void whenReady(final String scriptname, final EventListener function) {
    List<EventListener> eventList = eventLisenerQueue.get(scriptname);
    if (eventList == null) {
      eventList = new ArrayList<>();
      eventLisenerQueue.put(scriptname, eventList);
    }
    eventList.add(function);
    if (BooleanUtils.isTrue(initializationStarted.get(scriptname)) || isInHeader(scriptname)) {
      if (isInitialized(scriptname)) {
        eventLisenerQueue.get(scriptname)
            .forEach(action -> action.handleEvent(rememberEvent.get(scriptname)));
        eventLisenerQueue.get(scriptname).clear();
      }
      return;
    }
    initializationStarted.put(scriptname, Boolean.TRUE);

    final HTMLScriptElement jsScript =
        (HTMLScriptElement) DomGlobal.document.createElement(TAG_TYPE);
    if (StringUtils.endsWith(scriptname, ".js")) {
      jsScript.src = scriptname;
    } else {
      jsScript.innerHTML = scriptname;
    }
    jsScript.type = SCRIPT_TYPE;

    DomGlobal.document.head.appendChild(jsScript);

    jsScript.addEventListener("load", event -> {
      eventLisenerQueue.get(scriptname).forEach(action -> action.handleEvent(event));
      eventLisenerQueue.get(scriptname).clear();
      rememberEvent.put(scriptname, event);
    });
  }

  /**
   * check if script is available and initialized.
   *
   * @return true if it exists
   */
  public static boolean isInitialized(final String scriptname) {
    final boolean loaded = rememberEvent != null || StringUtils.isEmpty(scriptname);
    return loaded || isInHeader(scriptname);
  }

  /**
   * check if script is already in the header.
   *
   * @return true if it exists
   */
  public static boolean isInHeader(final String scriptname) {
    final NodeList<Element> linkList = DomGlobal.document.head.getElementsByTagName(TAG_TYPE);
    return linkList.asList().stream()
        .filter(element -> StringUtils.endsWith(scriptname, ".js")
            ? StringUtils.equals(((HTMLScriptElement) element).src, scriptname)
            : StringUtils.equals(((HTMLScriptElement) element).innerHTML, scriptname))
        .findFirst().isPresent();
  }
}

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.mtwidgets.client.ui.widget.features;

import de.knightsoftnet.mtwidgets.client.ui.widget.helper.ErrorMessageFormater;

import com.google.gwt.user.client.ui.HTMLPanel;

import org.apache.commons.lang3.StringUtils;
import org.gwtproject.editor.client.Editor;
import org.gwtproject.editor.client.EditorError;
import org.gwtproject.editor.client.HasEditorErrors;
import org.gwtproject.editor.client.IsEditor;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * interface for widgets which can delegate validation messages to another dom element.
 *
 * @param <T> the type of the values to edit
 * @param <E> the type of Editor the view object will provide
 *
 * @author Manfred Tremmel
 *
 */
public interface HasValidationMessageElement<T, E extends Editor<T>>
    extends HasEditorErrors<T>, IsEditor<E> {

  /**
   * set validation message element.
   *
   * @param element element to set
   */
  void setValidationMessageElement(final HTMLPanel element);

  /**
   * get validation message element.
   *
   * @return element or null if non is set
   */
  HTMLPanel getValidationMessageElement();

  /**
   * set custom validity value.
   *
   * @param message message to show
   */
  void setCustomValidity(final String message);

  /**
   * set title of element.
   *
   * @param title title to set
   */
  void setTitle(String title);

  @Override
  default void showErrors(final List<EditorError> errors) {
    final Set<String> messages = errors.stream().filter(error -> editorErrorMatches(error))
        .map(error -> error.getMessage()).collect(Collectors.toSet());
    showErrors(messages);
  }

  /**
   * show error messages.
   *
   * @param messages set of messages
   */
  default void showErrors(final Set<String> messages) {
    if (messages == null || messages.isEmpty()) {
      setCustomValidity(StringUtils.EMPTY);
      if (getValidationMessageElement() == null) {
        setTitle(StringUtils.EMPTY);
      } else {
        getValidationMessageElement().getElement().removeAllChildren();
      }
    } else {
      final String messagesAsString = ErrorMessageFormater.messagesToString(messages);
      setCustomValidity(messagesAsString);
      if (getValidationMessageElement() == null) {
        setTitle(messagesAsString);
      } else {
        getValidationMessageElement().getElement()
            .setInnerSafeHtml(ErrorMessageFormater.messagesToList(messages));
      }
    }
  }

  /**
   * Checks if a error belongs to this widget.
   *
   * @param error editor error to check
   * @return true if the error belongs to this widget
   */
  default boolean editorErrorMatches(final EditorError error) {
    return error != null && error.getEditor() != null
        && (equals(error.getEditor()) || error.getEditor().equals(asEditor()));
  }
}

package de.knightsoftnet.mtwidgets.client.ui.widget.features;

import com.google.gwt.user.client.TakesValue;

public interface HasCountryCodeReference {

  /**
   * set reference to a field which contains the country code.
   *
   * @param countryCodeReference field which contains the country code
   */
  void setCountryCodeReference(final TakesValue<?> countryCodeReference);
}

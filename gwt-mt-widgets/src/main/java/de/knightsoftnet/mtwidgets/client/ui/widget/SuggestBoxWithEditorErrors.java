/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.mtwidgets.client.ui.widget;

import de.knightsoftnet.mtwidgets.client.ui.widget.features.HasAutocomplete;
import de.knightsoftnet.mtwidgets.client.ui.widget.features.HasAutofocus;
import de.knightsoftnet.mtwidgets.client.ui.widget.features.HasPlaceholder;
import de.knightsoftnet.mtwidgets.client.ui.widget.features.HasRequired;
import de.knightsoftnet.mtwidgets.client.ui.widget.features.HasValidationMessageElement;
import de.knightsoftnet.mtwidgets.client.ui.widget.features.HasValidity;

import com.google.gwt.event.dom.client.BlurHandler;
import com.google.gwt.event.dom.client.FocusHandler;
import com.google.gwt.event.dom.client.HasBlurHandlers;
import com.google.gwt.event.dom.client.HasFocusHandlers;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.MultiWordSuggestOracle;
import com.google.gwt.user.client.ui.SuggestBox;
import com.google.gwt.user.client.ui.SuggestOracle;

import elemental2.dom.ValidityState;

import org.apache.commons.lang3.StringUtils;
import org.gwtproject.editor.client.LeafValueEditor;

/**
 * Text input box with suggestions and editor error handling.
 */
public class SuggestBoxWithEditorErrors extends SuggestBoxNewEditor implements HasFocusHandlers,
    HasBlurHandlers, HasValidationMessageElement<String, LeafValueEditor<String>>, HasRequired,
    HasValidity, HasAutofocus, HasPlaceholder, HasAutocomplete {

  /**
   * Constructor for {@link SuggestBox}. Creates a {@link MultiWordSuggestOracle} and
   * {@link TextBox} to use with this {@link SuggestBox}.
   */
  public SuggestBoxWithEditorErrors() {
    this(new MultiWordSuggestOracle(), new TextBox(), new DefaultSuggestionDisplay());
  }

  /**
   * Constructor for {@link SuggestBox}. Creates a {@link TextBox} to use with this
   * {@link SuggestBox}.
   *
   * @param oracle the oracle for this <code>SuggestBox</code>
   */
  public SuggestBoxWithEditorErrors(final SuggestOracle oracle) {
    this(oracle, new TextBox(), new DefaultSuggestionDisplay());
  }

  /**
   * Constructor for {@link SuggestBox}. The text box will be removed from it's current location and
   * wrapped by the {@link SuggestBox}.
   *
   * @param oracle supplies suggestions based upon the current contents of the text widget
   * @param box the text widget
   */
  public SuggestBoxWithEditorErrors(final SuggestOracle oracle,
      final ValueBoxBaseWithEditorErrors<String> box) {
    this(oracle, box, new DefaultSuggestionDisplay());
  }

  /**
   * Constructor for {@link SuggestBox}. The text box will be removed from it's current location and
   * wrapped by the {@link SuggestBox}.
   *
   * @param oracle supplies suggestions based upon the current contents of the text widget
   * @param box the text widget
   * @param suggestDisplay the class used to display suggestions
   */
  public SuggestBoxWithEditorErrors(final SuggestOracle oracle,
      final ValueBoxBaseWithEditorErrors<String> box, final SuggestionDisplay suggestDisplay) {
    super(oracle, box, suggestDisplay);
    addSelectionHandler(event -> {
      this.setValue(StringUtils.EMPTY, false);
      this.setValue(event.getSelectedItem().getReplacementString(), true);
    });
  }

  @Override
  public HandlerRegistration addBlurHandler(final BlurHandler handler) {
    return getValueBox().addBlurHandler(handler);
  }

  @Override
  public HandlerRegistration addFocusHandler(final FocusHandler handler) {
    return getValueBox().addFocusHandler(handler);
  }

  @Override
  public String getValidationMessage() {
    return getValueBox().getValidationMessage();
  }

  @Override
  public ValidityState getValidity() {
    return getValueBox().getValidity();
  }

  @Override
  public boolean checkValidity() {
    return getValueBox().checkValidity();
  }


  @Override
  public boolean isRequired() {
    return getValueBox().isRequired();
  }

  @Override
  public void setRequired(final boolean arg) {
    getValueBox().setRequired(arg);
  }

  @Override
  public boolean isAutofocus() {
    return getValueBox().isAutofocus();
  }

  @Override
  public void setAutofocus(final boolean arg) {
    getValueBox().setAutofocus(arg);
  }

  @Override
  public void setValidationMessageElement(final HTMLPanel element) {
    getValueBox().setValidationMessageElement(element);
  }

  @Override
  public HTMLPanel getValidationMessageElement() {
    return getValueBox().getValidationMessageElement();
  }

  @Override
  public String getPlaceholder() {
    return getValueBox().getPlaceholder();
  }

  @Override
  public void setPlaceholder(final String placeholder) {
    getValueBox().setPlaceholder(placeholder);
  }

  @Override
  public String getAutocomplete() {
    return getValueBox().getAutocomplete();
  }

  @Override
  public void setAutocomplete(final String arg) {
    getValueBox().setAutocomplete(arg);
  }

  @Override
  public void setCustomValidity(final String message) {
    getValueBox().setCustomValidity(message);
  }
}

package de.knightsoftnet.mtwidgets.client.ui.widget.helper;

import org.gwtproject.editor.client.LeafValueEditor;

public final class LeafValueEditorConverter {

  private LeafValueEditorConverter() {
    super();
  }

  /**
   * convert old com.google.gwt.editor.client.LeafValueEditor to new
   * org.gwtproject.editor.client.LeafValueEditor.
   *
   * @param <T> type to handle
   * @param oldEditor old editor to convert
   * @return new editor
   */
  public static <T> LeafValueEditor<T> convertOld(
      final com.google.gwt.editor.client.LeafValueEditor<T> oldEditor) {
    return new LeafValueEditor<T>() {

      @Override
      public void setValue(final T value) {
        oldEditor.setValue(value);
      }

      @Override
      public T getValue() {
        return oldEditor.getValue();
      }
    };
  }
}

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.mtwidgets.client.ui.widget.helper;

import de.knightsoftnet.mtwidgets.client.ui.widget.features.HasValidationMessageElement;
import de.knightsoftnet.validators.client.editor.BeanValidationEditorDriver;
import de.knightsoftnet.validators.client.editor.impl.ListValidationEditor;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTMLPanel;

import java.util.List;

/**
 * editor to show a list of items entries.
 *
 * @author Manfred Tremmel
 *
 * @param <D> type of item data to edit
 * @param <V> type of view of the single items
 */
public abstract class AbstractListEditor<D, V extends AbstractListItemView<D>> extends FlowPanel
    implements HasValueChangeHandlers<List<D>>,
    HasValidationMessageElement<List<D>, ListValidationEditor<D, V>> {

  private HTMLPanel validationMessageElement;


  /**
   * remove an existing entry.
   *
   * @param pos position of the entry to remove
   */
  public void removeEntry(final int pos) {
    asEditor().getList().remove(pos);
    ValueChangeEvent.fire(this, asEditor().getList());
  }

  /**
   * remove an existing entry.
   *
   * @param entry the entry to remove
   */
  public void removeEntry(final V entry) {
    removeEntry(this.getWidgetIndex(entry));
  }

  /**
   * add a new entry at the end of the list.
   */
  public void addNewEntry() {
    asEditor().getList().add(this.createData());
    ValueChangeEvent.fire(this, asEditor().getList());
  }

  /**
   * move one entry in the list.
   *
   * @param entry the entry we want to move
   * @param newPos new position in the list
   */
  public void moveEntry(final V entry, final int newPos) {
    final int oldPos = this.getWidgetIndex(entry);
    if (oldPos != newPos && newPos >= 0 && newPos < asEditor().getList().size()) {
      final D value = asEditor().getList().remove(oldPos);
      asEditor().getList().add(newPos, value);
      ValueChangeEvent.fire(this, asEditor().getList());
    }
  }

  /**
   * create new instance of a data element.
   *
   * @return data element
   */
  protected abstract D createData();

  /**
   * set parent editor driver.
   *
   * @param parentDriver BeanValidationEditorDriver to set
   */
  public final void setParentDriver(final BeanValidationEditorDriver<?, ?> parentDriver) {
    asEditor().setParentDriver(parentDriver);
  }

  @Override
  public void setCustomValidity(final String message) {
    GWT.log(message);
  }

  @Override
  public void setValidationMessageElement(final HTMLPanel element) {
    this.validationMessageElement = element;
  }

  @Override
  public HTMLPanel getValidationMessageElement() {
    return this.validationMessageElement;
  }

  @Override
  public HandlerRegistration addValueChangeHandler(final ValueChangeHandler<List<D>> handler) {
    return this.addHandler(handler, ValueChangeEvent.getType());
  }
}

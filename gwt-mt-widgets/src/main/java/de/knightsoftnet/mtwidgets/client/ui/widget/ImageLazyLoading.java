/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.mtwidgets.client.ui.widget;

import com.google.gwt.dom.client.Element;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.safehtml.shared.SafeUri;
import com.google.gwt.user.client.ui.Image;

/**
 * Image lazy loading is based on the GWT {@link Image} and provides all the functionality inside.
 * The difference is, image urls are put into the image tag when image is in the viewport so no
 * images are loaded by the browser, which are not visible.
 *
 * @author Manfred Tremmel
 *
 */
public class ImageLazyLoading extends Image {

  private static final String LOADING = "loading";
  private static final String LAZY = "lazy";

  public ImageLazyLoading() {
    super();
  }

  public ImageLazyLoading(final Element element) {
    super(element);
  }

  public ImageLazyLoading(final ImageResource resource) {
    super(resource);
  }

  public ImageLazyLoading(final SafeUri url, final int left, final int top, final int width,
      final int height) {
    super(url, left, top, width, height);
  }

  public ImageLazyLoading(final SafeUri url) {
    super(url);
  }

  public ImageLazyLoading(final String url, final int left, final int top, final int width,
      final int height) {
    super(url, left, top, width, height);
  }

  public ImageLazyLoading(final String url) {
    super(url);
  }

  @Override
  public void setUrl(final SafeUri url) {
    super.setUrl(url);
    getElement().setAttribute(LOADING, LAZY);
  }
}

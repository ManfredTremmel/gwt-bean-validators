/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.mtwidgets.client.ui.widget;

import de.knightsoftnet.mtwidgets.client.ui.widget.features.HasAutofocus;
import de.knightsoftnet.mtwidgets.client.ui.widget.features.HasValidationMessageElement;
import de.knightsoftnet.mtwidgets.client.ui.widget.helper.FeatureCheck;
import de.knightsoftnet.mtwidgets.client.ui.widget.helper.IdAndNameBean;
import de.knightsoftnet.mtwidgets.client.ui.widget.helper.IdAndNameIdComperator;
import de.knightsoftnet.mtwidgets.client.ui.widget.helper.IdAndNameNameComperator;
import de.knightsoftnet.mtwidgets.client.ui.widget.helper.ListSortEnum;
import de.knightsoftnet.mtwidgets.client.ui.widget.helper.MessagesForValues;
import de.knightsoftnet.validators.client.decorators.ExtendedValueBoxEditor;
import de.knightsoftnet.validators.client.editor.ValueBoxEditor;

import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Focusable;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.HasEnabled;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.impl.FocusImpl;

import elemental2.dom.Element;
import elemental2.dom.HTMLElement;
import elemental2.dom.HTMLInputElement;
import elemental2.dom.NodeList;

import org.apache.commons.lang3.BooleanUtils;
import org.gwtproject.editor.client.TakesValue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import jsinterop.base.Js;

/**
 * a radio box with id and name which is sortable and returns id.
 *
 * @author Manfred Tremmel
 *
 * @param <T> type of the id
 */
@SuppressWarnings("CPD-START")
public class SortableIdAndNameRadioButton<T extends Comparable<T>> extends Composite
    implements HasValue<T>, TakesValue<T>, Focusable,
    HasValidationMessageElement<T, ValueBoxEditor<T>>, HasAutofocus, HasEnabled {
  private boolean valueChangeHandlerInitialized;
  private final String widgetId;
  private final ListSortEnum sortOrder;
  private final MessagesForValues<T> messages;
  private final List<IdAndNameBean<T>> entries;
  private final FlowPanel flowPanel;
  private final Map<T, RadioButton> idToButtonMap;
  private final ValueBoxEditor<T> editor;

  private static final FocusImpl IMPL = FocusImpl.getFocusImplForWidget();

  private HTMLPanel validationMessageElement;
  private boolean enabled;

  /**
   * widget ui constructor.
   *
   * @param widgetId widget id which is the same for all created radio buttons
   * @param sort the sort order of the countries
   * @param messages message resolver
   * @param ids ids to add to listBox
   */
  @SafeVarargs
  public SortableIdAndNameRadioButton(final String widgetId, final ListSortEnum sort,
      final MessagesForValues<T> messages, final T... ids) {
    this(widgetId, sort, messages, List.of(ids));
  }

  /**
   * widget ui constructor.
   *
   * @param widgetId widget id which is the same for all created radio buttons
   * @param idAndNameList list of id's and names
   */
  public SortableIdAndNameRadioButton(final String widgetId,
      final Collection<IdAndNameBean<T>> idAndNameList) {
    this.widgetId = widgetId;
    sortOrder = null;
    messages = null;
    entries = new ArrayList<>(idAndNameList.size());
    flowPanel = new FlowPanel();
    idToButtonMap = new HashMap<>();
    enabled = true;

    fillEntries(idAndNameList);

    initWidget(flowPanel);
    editor = new ExtendedValueBoxEditor<>(this, null);
  }

  /**
   * widget ui constructor.
   *
   * @param widgetId widget id which is the same for all created radio buttons
   * @param sortOrder the sort order of the countries
   * @param messages message resolver
   * @param ids ids to add to listBox
   */
  public SortableIdAndNameRadioButton(final String widgetId, final ListSortEnum sortOrder,
      final MessagesForValues<T> messages, final Collection<T> ids) {
    super();
    this.widgetId = widgetId;
    this.sortOrder = sortOrder;
    this.messages = messages;
    entries = new ArrayList<>(ids.size());
    flowPanel = new FlowPanel();
    idToButtonMap = new HashMap<>();
    enabled = true;

    fillEntries(fillEntriesSorted(ids));

    initWidget(flowPanel);
    editor = new ExtendedValueBoxEditor<>(this, null);
  }

  /**
   * fill entries of the radio buttons.
   *
   * @param ids list of entries
   */
  private Collection<IdAndNameBean<T>> fillEntriesSorted(final Collection<T> ids) {
    final Stream<IdAndNameBean<T>> stream =
        ids.stream().map(proEnum -> new IdAndNameBean<>(proEnum, messages.name(proEnum)));
    final Stream<IdAndNameBean<T>> sortedStream;

    if (sortOrder == null) {
      sortedStream = stream;
    } else {
      switch (sortOrder == null ? null : sortOrder) {
        case ID_ASC:
          sortedStream = stream.sorted(new IdAndNameIdComperator<>());
          break;
        case ID_DSC:
          sortedStream = stream.sorted(Collections.reverseOrder(new IdAndNameIdComperator<>()));
          break;
        case NAME_ASC:
          sortedStream = stream.sorted(new IdAndNameNameComperator<>());
          break;
        case NAME_DSC:
          sortedStream = stream.sorted(Collections.reverseOrder(new IdAndNameNameComperator<>()));
          break;
        default:
          sortedStream = stream;
          break;
      }
    }
    return sortedStream.collect(Collectors.toList());
  }

  /**
   * fill entries of the radio buttons.
   *
   * @param pids list of entries
   */
  private void fillEntries(final Collection<IdAndNameBean<T>> idAndNameList) {
    entries.clear();
    entries.addAll(idAndNameList);

    flowPanel.clear();

    entries.forEach(entry -> {
      final RadioButton radioButton = new RadioButton(widgetId, entry.getName());
      radioButton.setFormValue(Objects.toString(entry.getId()));
      radioButton.setEnabled(enabled);
      flowPanel.add(radioButton);
      idToButtonMap.put(entry.getId(), radioButton);
    });
  }

  @Override
  public HandlerRegistration addValueChangeHandler(final ValueChangeHandler<T> handler) {
    // Is this the first value change handler? If so, time to add handlers
    if (!valueChangeHandlerInitialized) {
      this.ensureDomEventHandlers();
      valueChangeHandlerInitialized = true;
    }
    return this.addHandler(handler, ValueChangeEvent.getType());
  }

  protected void ensureDomEventHandlers() {
    idToButtonMap.values().forEach(radioButton -> radioButton
        .addValueChangeHandler(event -> ValueChangeEvent.fire(this, this.getValue())));
  }

  @Override
  public T getValue() {
    return idToButtonMap.entrySet().stream()
        .filter(entry -> BooleanUtils.isTrue(entry.getValue().getValue()))
        .map(entry -> entry.getKey()).findFirst().orElse(null);
  }

  @Override
  public void setValue(final T value) {
    this.setValue(value, false);
  }

  @Override
  public void setValue(final T value, final boolean fireEvents) {
    final T oldValue = this.getValue();
    final RadioButton radioButton = idToButtonMap.get(value);
    if (radioButton == null) {
      idToButtonMap.values().forEach(entry -> entry.setValue(Boolean.FALSE));
    } else {
      idToButtonMap.get(value).setValue(Boolean.TRUE);
    }
    if (fireEvents) {
      ValueChangeEvent.fireIfNotEqual(this, oldValue, value);
    }
  }

  @Override
  public int getTabIndex() {
    if (flowPanel.getWidgetCount() > 0
        && flowPanel.getWidget(0) instanceof final Focusable focusable) {
      return focusable.getTabIndex();
    }
    return -1;
  }

  @Override
  public void setAccessKey(final char key) {
    if (flowPanel.getWidgetCount() > 0
        && flowPanel.getWidget(0) instanceof final Focusable focusable) {
      focusable.setAccessKey(key);
    }
  }

  @Override
  public void setFocus(final boolean focused) {
    if (flowPanel.getWidgetCount() > 0
        && flowPanel.getWidget(0) instanceof final Focusable focusable) {
      focusable.setFocus(focused);
    }
  }

  @Override
  public void setTabIndex(final int index) {
    for (int i = 0; i < flowPanel.getWidgetCount(); i++) {
      IMPL.setTabIndex(flowPanel.getWidget(i).getElement(), index + i);
    }
  }

  @Override
  public void setCustomValidity(final String message) {
    final HTMLElement headElement = Js.<HTMLElement>uncheckedCast(getElement());
    final NodeList<Element> inputElements = headElement.getElementsByTagName("input");
    inputElements.asList().forEach(element -> {
      if (FeatureCheck.supportCustomValidity(element)) {
        ((HTMLInputElement) element).setCustomValidity(message);
      }
    });
  }

  @Override
  public void setTitle(final String title) {
    final HTMLElement headElement = Js.<HTMLElement>uncheckedCast(getElement());
    final NodeList<Element> inputElements = headElement.getElementsByTagName("input");
    inputElements.asList().forEach(element -> {
      ((HTMLInputElement) element).title = title;
    });
  }

  @Override
  public ValueBoxEditor<T> asEditor() {
    return editor;
  }

  @Override
  public boolean isAutofocus() {
    final HTMLElement headElement = Js.<HTMLElement>uncheckedCast(getElement());
    final NodeList<Element> inputElements = headElement.getElementsByTagName("input");
    final HTMLInputElement input = (HTMLInputElement) inputElements.getAt(0);
    return input.autofocus;
  }

  @Override
  public void setAutofocus(final boolean arg) {
    final HTMLElement headElement = Js.<HTMLElement>uncheckedCast(getElement());
    final NodeList<Element> inputElements = headElement.getElementsByTagName("input");
    final HTMLInputElement input = (HTMLInputElement) inputElements.getAt(0);
    input.autofocus = arg;
  }

  @Override
  public void setValidationMessageElement(final HTMLPanel element) {
    validationMessageElement = element;
  }

  @Override
  public HTMLPanel getValidationMessageElement() {
    return validationMessageElement;
  }

  @Override
  public boolean isEnabled() {
    return enabled;
  }

  @Override
  public void setEnabled(final boolean enabled) {
    this.enabled = enabled;
    idToButtonMap.values().forEach(entry -> entry.setEnabled(enabled));
  }
}

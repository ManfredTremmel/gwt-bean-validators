/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.mtwidgets.client.ui.widget.oracle;

import org.apache.commons.lang3.StringUtils;

/**
 * suggest entry of phone number URI suggest widget.
 *
 * @author Manfred Tremmel
 *
 */
public class PhoneNumberUriItemSuggest extends AbstractPhoneNumberItemSuggest {

  /**
   * constructor initializing fields.
   *
   *
   * @param countryCode country code to set
   * @param countryName country name to set
   * @param areaCode area code to set
   * @param areaName area name to set
   */
  public PhoneNumberUriItemSuggest(final String countryCode, final String countryName,
      final String areaCode, final String areaName) {
    super(countryCode, countryName, areaCode, areaName);
  }

  @Override
  public String getDisplayString() {
    if (StringUtils.isEmpty(getAreaCode())) {
      return "+" + getCountryCode() + " - " + getCountryName();
    }
    return "+" + getCountryCode() + "-" + getAreaCode() + " - " + getAreaName();
  }

  @Override
  public String getReplacementString() {
    if (StringUtils.isEmpty(getAreaCode())) {
      return "+" + getCountryCode();
    }
    return "+" + getCountryCode() + "-" + getAreaCode();
  }
}

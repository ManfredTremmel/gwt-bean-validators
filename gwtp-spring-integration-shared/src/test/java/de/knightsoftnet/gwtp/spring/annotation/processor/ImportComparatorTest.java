package de.knightsoftnet.gwtp.spring.annotation.processor;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

/**
 * test import comparator.
 */
public class ImportComparatorTest {

  /**
   * test sorting.
   */
  @Test
  public void testIbanFormat() {
    // given
    final List<String> imports = Arrays.asList(//
        "javax.test.something.Other", //
        "java.test.something.Other", //
        "org.test.something.Bbbbb", //
        "org.test.something.Aaaaa", //
        "de.test.something.Other", //
        "com.test.something.Other", //
        "this.project.thing.Other" //
    );
    final ImportComparator comparator = new ImportComparator("this.project");

    // when
    imports.sort(comparator);

    // then
    assertEquals("this.project.thing.Other", imports.get(0));
    assertEquals("com.test.something.Other", imports.get(1));
    assertEquals("de.test.something.Other", imports.get(2));
    assertEquals("org.test.something.Aaaaa", imports.get(3));
    assertEquals("org.test.something.Bbbbb", imports.get(4));
    assertEquals("java.test.something.Other", imports.get(5));
    assertEquals("javax.test.something.Other", imports.get(6));
  }
}

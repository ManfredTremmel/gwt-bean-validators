/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.gwtp.spring.shared.data;

import org.springframework.data.domain.Persistable;

import java.io.Serializable;

/**
 * administration result.
 *
 * @author Manfred Tremmel
 *
 * @param <I> type of the id
 * @param <T> database entity
 */
public class AdminResult<I, T extends Persistable<? extends I>> implements Serializable {

  private static final long serialVersionUID = -4149115334269218070L;

  /**
   * current navigation entries.
   */
  private AdminNavigation<I> navigation;
  /**
   * read entry.
   */
  private T entry;

  /**
   * default constructor.
   */
  public AdminResult() {
    super();
    navigation = new AdminNavigation<>();
  }

  /**
   * constructor initializing fields.
   *
   * @param firstId id of the first entry
   * @param previousId id of the previous entry
   * @param nextId id of the next entry
   * @param lastId id of the last entry
   * @param entry current entry
   */
  public AdminResult(final I firstId, final I previousId, final I nextId, final I lastId,
      final T entry) {
    super();
    this.navigation = new AdminNavigation<>(firstId, previousId, nextId, lastId,
        entry == null ? null : entry.getId());
    this.entry = entry;
  }

  public AdminNavigation<I> getNavigation() {
    return this.navigation;
  }

  public void setNavigation(final AdminNavigation<I> navigation) {
    this.navigation = navigation;
  }

  public T getEntry() {
    return this.entry;
  }

  public void setEntry(final T entry) {
    this.entry = entry;
  }
}

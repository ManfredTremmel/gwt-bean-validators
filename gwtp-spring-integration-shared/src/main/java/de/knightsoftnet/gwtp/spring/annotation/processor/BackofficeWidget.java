/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.gwtp.spring.annotation.processor;

import de.knightsoftnet.validators.shared.data.FieldTypeEnum;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Stream;

import javax.lang.model.element.Element;
import javax.lang.model.type.DeclaredType;

/**
 * Widget data used for generating ui binder and view.
 */
public class BackofficeWidget {

  private DeclaredType containing;
  private Element field;
  private String name;
  private List<String> imports;
  private String widgetName;
  private List<BackofficeWidgetParameter> parameters;
  private boolean provided;
  private String providedConstruction;
  private FieldTypeEnum fieldType;
  private List<BackofficeWidget> childWidgets;
  private BackofficeWidget parentWidget;
  private boolean ignore;
  private boolean writeDebugId;
  private boolean providerNeeded;
  private Map<String, Object> additional;

  /**
   * Default constructor.
   */
  public BackofficeWidget() {
    super();
    childWidgets = new ArrayList<>();
    additional = new HashMap<>();
  }

  /**
   * Constructor initializing fields.
   *
   * @param containing the declared type containing the element
   * @param field the field element
   * @param name field name
   * @param imports list of required imports
   * @param widgetName widget name
   * @param parameters list of parameters
   * @param provided create widget as provided
   * @param providedConstruction construction used when provided
   * @param fieldType type of the field, important for search implementation
   * @param childWidgets list of widgets which are child of this widget
   * @param parentWidget the parent widget or null
   * @param ignore flag for ignoring this widget
   * @param writeDebugId write debug id for widget
   * @param providerNeeded a provider is needed for the widget
   * @param additional a map of widget specific additional parameters
   */
  public BackofficeWidget(final DeclaredType containing, final Element field, final String name,
      final List<String> imports, final String widgetName,
      final List<BackofficeWidgetParameter> parameters, final boolean provided,
      final String providedConstruction, final FieldTypeEnum fieldType,
      final List<BackofficeWidget> childWidgets, final BackofficeWidget parentWidget,
      final boolean ignore, final boolean writeDebugId, final boolean providerNeeded,
      final Map<String, Object> additional) {
    super();
    this.containing = containing;
    this.field = field;
    this.name = name;
    this.imports = imports;
    this.widgetName = widgetName;
    this.parameters = parameters;
    this.provided = provided;
    this.providedConstruction = providedConstruction;
    this.fieldType = fieldType;
    this.childWidgets = childWidgets;
    this.parentWidget = parentWidget;
    this.ignore = ignore;
    this.writeDebugId = writeDebugId;
    this.providerNeeded = providerNeeded;
    this.additional = additional;
  }

  /**
   * create widget.
   *
   * @param containing the declared type containing the element
   * @param field the field element
   * @param name field name
   * @param imports list of required imports
   * @param widgetName widget name
   * @param parameters list of parameters
   * @param provided create widget as provided
   * @param providedConstruction construction used when provided
   * @param fieldType type of the field, important for search implementation
   * @param ignore flag for ignoring this widget
   * @param writeDebugId write debug id for widget
   * @param providerNeeded a provider is needed for the widget
   * @param additional a map of widget specific additional parameters
   */
  public static BackofficeWidget of(final DeclaredType containing, final Element field,
      final String name, final List<String> imports, final String widgetName,
      final List<BackofficeWidgetParameter> parameters, final boolean provided,
      final String providedConstruction, final FieldTypeEnum fieldType, final boolean ignore,
      final boolean writeDebugId, final boolean providerNeeded,
      final Map<String, Object> additional) {
    return new BackofficeWidget(containing, field, name, imports, widgetName, parameters, provided,
        providedConstruction, fieldType, new ArrayList<>(), null, ignore, writeDebugId,
        providerNeeded, additional);
  }

  /**
   * create widget.
   *
   * @param containing the declared type containing the element
   * @param field the field element
   * @param name field name
   * @param imports list of required imports
   * @param widgetName widget name
   * @param parameters list of parameters
   * @param provided create widget as provided
   * @param providedConstruction construction used when provided
   * @param fieldType type of the field, important for search implementation
   * @param parentWidget the parent widget or null
   * @param ignore flag for ignoring this widget
   * @param writeDebugId write debug id for widget
   * @param providerNeeded a provider is needed for the widget
   * @param additional a map of widget specific additional parameters
   */
  public static BackofficeWidget of(final DeclaredType containing, final Element field,
      final String name, final List<String> imports, final String widgetName,
      final List<BackofficeWidgetParameter> parameters, final boolean provided,
      final String providedConstruction, final FieldTypeEnum fieldType,
      final BackofficeWidget parentWidget, final boolean ignore, final boolean writeDebugId,
      final boolean providerNeeded, final Map<String, Object> additional) {
    return new BackofficeWidget(containing, field, name, imports, widgetName, parameters, provided,
        providedConstruction, fieldType, new ArrayList<>(), parentWidget, ignore, writeDebugId,
        providerNeeded, additional);
  }

  /**
   * create widget.
   *
   * @param containing the declared type containing the element
   * @param field the field element
   * @param name field name
   * @param imports list of required imports
   * @param widgetName widget name
   * @param parameters list of parameters
   * @param provided create widget as provided
   * @param providedConstruction construction used when provided
   * @param fieldType type of the field, important for search implementation
   * @param childWidgets list of widgets which are child of this widget
   * @param parentWidget the parent widget or null
   * @param ignore flag for ignoring this widget
   * @param writeDebugId write debug id for widget
   * @param providerNeeded a provider is needed for the widget
   * @param additional a map of widget specific additional parameters
   */
  public static BackofficeWidget of(final DeclaredType containing, final Element field,
      final String name, final List<String> imports, final String widgetName,
      final List<BackofficeWidgetParameter> parameters, final boolean provided,
      final String providedConstruction, final FieldTypeEnum fieldType,
      final List<BackofficeWidget> childWidgets, final BackofficeWidget parentWidget,
      final boolean ignore, final boolean writeDebugId, final boolean providerNeeded,
      final Map<String, Object> additional) {
    return new BackofficeWidget(containing, field, name, imports, widgetName, parameters, provided,
        providedConstruction, fieldType, childWidgets, parentWidget, ignore, writeDebugId,
        providerNeeded, additional);
  }

  public DeclaredType getContaining() {
    return containing;
  }

  public void setContaining(final DeclaredType containing) {
    this.containing = containing;
  }

  public Element getField() {
    return field;
  }

  public void setField(final Element field) {
    this.field = field;
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public List<String> getImports() {
    return imports;
  }

  public void setImports(final List<String> imports) {
    this.imports = imports;
  }

  public String getWidgetName() {
    return widgetName;
  }

  public void setWidgetName(final String widgetName) {
    this.widgetName = widgetName;
  }

  public List<BackofficeWidgetParameter> getParameters() {
    return parameters;
  }

  public void setParameters(final List<BackofficeWidgetParameter> parameters) {
    this.parameters = parameters;
  }

  public boolean isProvided() {
    return provided;
  }

  public void setProvided(final boolean provided) {
    this.provided = provided;
  }

  public String getProvidedConstruction() {
    return providedConstruction;
  }

  public void setProvidedConstruction(final String providedConstruction) {
    this.providedConstruction = providedConstruction;
  }

  public FieldTypeEnum getFieldType() {
    return fieldType;
  }

  public void setFieldType(final FieldTypeEnum fieldType) {
    this.fieldType = fieldType;
  }

  public List<BackofficeWidget> getChildWidgets() {
    return childWidgets;
  }

  public void setChildWidgets(final List<BackofficeWidget> childWidgets) {
    childWidgets.forEach(widget -> widget.setParentWidget(this));
    this.childWidgets = childWidgets;
  }

  public BackofficeWidget getParentWidget() {
    return parentWidget;
  }

  public void setParentWidget(final BackofficeWidget parentWidget) {
    this.parentWidget = parentWidget;
  }

  public boolean isIgnore() {
    return ignore;
  }

  public void setIgnore(final boolean ignore) {
    this.ignore = ignore;
  }

  public boolean isWriteDebugId() {
    return writeDebugId;
  }

  public void setWriteDebugId(final boolean writeDebugId) {
    this.writeDebugId = writeDebugId;
  }

  public Map<String, Object> getAdditional() {
    return additional;
  }

  public boolean isProviderNeeded() {
    return providerNeeded;
  }

  public void setProviderNeeded(final boolean providerNeeded) {
    this.providerNeeded = providerNeeded;
  }

  public void setAdditional(final Map<String, Object> additional) {
    this.additional = additional;
  }

  public Stream<BackofficeWidget> streamFlatBackofficeWidget() {
    return streamFlatBackofficeWidget(null);
  }

  /**
   * flat backoffice widgets and concatenate names.
   *
   * @param parent widget name of parent widget
   * @return flat backoffice widget stream
   */
  public Stream<BackofficeWidget> streamFlatBackofficeWidget(final String parent) {
    final String prefix = StringUtils.isEmpty(parent) ? StringUtils.EMPTY : parent + ".";
    return Stream.concat(
        Stream.of(of(getContaining(), getField(), prefix + getName(), getImports(), getWidgetName(),
            getParameters(), isProvided(), getProvidedConstruction(), getFieldType(),
            getChildWidgets(), getParentWidget(), isIgnore(), isWriteDebugId(), isProviderNeeded(),
            getAdditional())),
        childWidgets.stream()
            .flatMap(entry -> entry.streamFlatBackofficeWidget(prefix + getName())));
  }

  @Override
  public int hashCode() {
    return Objects.hash(name);
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final BackofficeWidget other = (BackofficeWidget) obj;
    return Objects.equals(name, other.name);
  }

  @Override
  public String toString() {
    return "BackofficeWidget [containing=" + containing + ", field=" + field + ", name=" + name
        + ", imports=" + imports + ", widgetName=" + widgetName + ", parameters=" + parameters
        + ", provided=" + provided + ", providedConstruction=" + providedConstruction
        + ", fieldType=" + fieldType + ", childWidgets=" + childWidgets + ", parentWidget="
        + parentWidget + ", ignore=" + ignore + ", writeDebugId=" + writeDebugId
        + ", providerNeeded=" + providerNeeded + ", additional=" + additional + "]";
  }
}

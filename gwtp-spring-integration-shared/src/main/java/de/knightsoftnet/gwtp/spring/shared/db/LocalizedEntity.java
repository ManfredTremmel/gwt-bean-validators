/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.gwtp.spring.shared.db;

import de.knightsoftnet.gwtp.spring.shared.data.jpa.domain.AbstractPersistable;
import de.knightsoftnet.validators.shared.interfaces.LocalizedValue;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotEmpty;

@Entity
@Table(name = "localized")
public class LocalizedEntity extends AbstractPersistable<Long>
    implements LocalizedValue<String, String>, Serializable {

  private static final long serialVersionUID = 5479449217468715495L;

  @ElementCollection(fetch = FetchType.EAGER)
  private Map<@NotEmpty String, String> localizedText;

  public LocalizedEntity() {
    this(null, new HashMap<>());
  }

  public LocalizedEntity(final Long id) {
    this(id, new HashMap<>());
  }

  public LocalizedEntity(final Map<String, String> map) {
    this(null, map);
  }

  /**
   * constructor initialize id and localized map.
   *
   * @param id entity id of the entry
   * @param map the map with the localized entries
   */
  public LocalizedEntity(final Long id, final Map<String, String> map) {
    super();
    setId(id);
    setLocalizedText(map); // NOPMD when final hybernate fails
  }

  @Override
  public void setLocalizedText(final Map<String, String> localizedText) {
    if (localizedText == null) {
      if (this.localizedText == null) {
        this.localizedText = new HashMap<>();
      } else {
        this.localizedText.clear();
      }
    } else {
      this.localizedText = localizedText;
    }
  }

  @Override
  public void putLocalizedText(final String locale, final String text) {
    localizedText.put(locale, text);
  }

  @Override
  public Map<String, String> getLocalizedText() {
    return localizedText;
  }

  @Override
  public String getLocalizedText(final String locale) {
    return localizedText.get(locale);
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(getId());
  }

  @Override
  public boolean equals(final Object obj) {

    if (null == obj) {
      return false;
    }

    if (this == obj) {
      return true;
    }

    if (getClass() != obj.getClass()) {
      return false;
    }

    final LocalizedEntity that = (LocalizedEntity) obj;

    return Objects.equals(getId(), that.getId())
        && Objects.equals(getLocalizedText(), that.getLocalizedText());
  }
}

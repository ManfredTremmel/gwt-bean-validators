/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.gwtp.spring.shared.search;

import java.util.Arrays;
import java.util.stream.Collectors;

public enum SearchOperation {
  EQUALITY(':'), // :
  NEGATION('!'), // !
  GREATER_THEN('>'), // >
  GREATER_OR_EQUAL_THEN('≥'), // ≥
  LESS_THEN('<'), // <
  LESS_OR_EQUAL_THEN('≤'), // <
  LIKE('~'), // ~
  STARTS_WITH('^'), // ^
  ENDS_WITH('$'), // $
  CONTAINS('∋'); // ∋

  private final char simpleOperation;

  SearchOperation(final char simpleOperation) {
    this.simpleOperation = simpleOperation;
  }

  public char getSimpleOperation() {
    return simpleOperation;
  }

  /**
   * get simple search operation from representing character.
   *
   * @param input character
   * @return SearchOperation enum or null if non matches
   */
  public static SearchOperation getSimpleOperation(final char input) {
    return Arrays.stream(SearchOperation.values()) // NOPMD
        .filter(entry -> entry.simpleOperation == input) // NOPMD
        .findFirst().orElse(null);
  }

  public static String getAllOperationsAsRegExString() {
    return Arrays.stream(SearchOperation.values()) // NOPMD
        .map(entry -> String.valueOf(entry.getSimpleOperation())).collect(Collectors.joining("|"));
  }
}

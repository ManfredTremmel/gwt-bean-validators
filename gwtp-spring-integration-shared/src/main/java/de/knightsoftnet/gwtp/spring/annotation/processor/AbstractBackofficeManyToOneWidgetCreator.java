package de.knightsoftnet.gwtp.spring.annotation.processor;

import de.knightsoftnet.validators.shared.data.FieldTypeEnum;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.annotation.processing.FilerException;
import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;
import javax.tools.Diagnostic;
import javax.tools.JavaFileObject;

/**
 * Create class or interface for many to one reference widgets.
 */
public abstract class AbstractBackofficeManyToOneWidgetCreator<T>
    extends AbstractBackofficeCreator<T> {

  public AbstractBackofficeManyToOneWidgetCreator(final String suffix) {
    super(suffix);
  }

  @Override
  public void writeClassOrInterface(final Element element, final T annotationInterface,
      final List<BackofficeWidget> widgets, final ProcessingEnvironment processingEnv) {
    final Map<String, BackofficeWidget> manyToOneWidgets =
        detectBackofficeWidgetsOfElementFlat(widgets).stream()
            .filter(widget -> widget.getFieldType() == FieldTypeEnum.MANY_TO_ONE)
            .collect(Collectors.toMap(widget -> widget.getWidgetName(), widget -> widget,
                (first, second) -> first));
    for (final Entry<String, BackofficeWidget> widgetEntry : manyToOneWidgets.entrySet()) {

      try {
        final String serverPackage = detectPackage(element, processingEnv);
        final String entityType = widgetEntry.getValue().getWidgetName();
        final String entityName = getEntityNameOfElement(widgetEntry.getValue().getField());

        final JavaFileObject builderFile =
            processingEnv.getFiler().createSourceFile(serverPackage + "." + entityType + suffix);
        try (PrintWriter out = new PrintWriter(builderFile.openWriter())) {
          writePackage(serverPackage, out);

          addAdditionalImports(serverPackage, widgetEntry.getValue().getField(),
              annotationInterface, widgets, processingEnv);

          writeImports(imports, out, serverPackage);

          writeBody(element, annotationInterface, widgets,
              Objects.toString(widgetEntry.getValue().getAdditional().get("keyfield")),
              Objects.toString(widgetEntry.getValue().getAdditional().get("valuefield")),
              entityType, entityName, out);
        }
      } catch (final FilerException e) {
        // happens when trying to recreate an existing interface
        processingEnv.getMessager().printMessage(Diagnostic.Kind.NOTE, e.getMessage());
      } catch (final IOException e) {
        processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR, e.getMessage());
        e.printStackTrace();
      }
    }
  }

  @Override
  protected void writeBody(final PrintWriter out, final String serverPackage, final Element element,
      final T annotationInterface, final List<BackofficeWidget> widgets,
      final ProcessingEnvironment processingEnv) {
    // ignore
  }

  protected abstract void writeBody(final Element element, final T annotationInterface,
      final List<BackofficeWidget> widgets, final String keyfield, final String valuefield,
      final String entityType, final String entityName, final PrintWriter out);
}

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.gwtp.spring.shared.data;

import java.io.Serializable;

/**
 * administration navigation.
 *
 * @author Manfred Tremmel
 *
 * @param <I> type of the id
 */
public class AdminNavigation<I> implements Serializable {

  private static final long serialVersionUID = 4760255340572531661L;

  /**
   * id of the first entry or null, if current entry is the first one.
   */
  private I firstId;
  /**
   * id of the previous entry or null, if current entry is the first one.
   */
  private I previousId;
  /**
   * id of the next entry or null, if current entry is the last one.
   */
  private I nextId;
  /**
   * id of the last entry or null, if current entry is the last one.
   */
  private I lastId;
  /**
   * id of the current entry, null if not persisted.
   */
  private I currentId;

  /**
   * default constructor.
   */
  public AdminNavigation() {
    super();
  }

  /**
   * constructor initializing fields.
   *
   * @param firstId id of the first entry
   * @param previousId id of the previous entry
   * @param nextId id of the next entry
   * @param lastId id of the last entry
   * @param currentId id of current entry
   */
  public AdminNavigation(final I firstId, final I previousId, final I nextId, final I lastId,
      final I currentId) {
    super();
    this.firstId = firstId;
    this.previousId = previousId;
    this.nextId = nextId;
    this.lastId = lastId;
    this.currentId = currentId;
  }

  public I getFirstId() {
    return this.firstId;
  }

  public void setFirstId(final I firstId) {
    this.firstId = firstId;
  }

  public I getPreviousId() {
    return this.previousId;
  }

  public void setPreviousId(final I previousId) {
    this.previousId = previousId;
  }

  public I getNextId() {
    return this.nextId;
  }

  public void setNextId(final I nextId) {
    this.nextId = nextId;
  }

  public I getLastId() {
    return this.lastId;
  }

  public void setLastId(final I lastId) {
    this.lastId = lastId;
  }

  public I getCurrentId() {
    return this.currentId;
  }

  public void setCurrentId(final I currentId) {
    this.currentId = currentId;
  }
}

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.gwtp.spring.annotation.processor;

import org.apache.commons.lang3.StringUtils;

import java.util.Comparator;

/**
 * comparator to sort imports.
 */
public class ImportComparator implements Comparator<String> {

  private static final String JAVA_PREFIX = "java.";
  private static final String JAVAX_PREFIX = "javax.";
  private static final String JAKARTA_PREFIX = "jakarta.";

  private final String projectPrefix;

  public ImportComparator(final String projectPrefix) {
    super();
    this.projectPrefix = projectPrefix;
  }

  @Override
  public int compare(final String first, final String second) {

    // empty entries to the top
    if (StringUtils.isEmpty(first)) {
      if (StringUtils.isEmpty(second)) {
        return 0;
      }
      return -1;
    }
    if (StringUtils.isEmpty(second)) {
      return 1;
    }

    // project prefix first
    if (StringUtils.startsWith(first, projectPrefix)) {
      if (StringUtils.startsWith(second, projectPrefix)) {
        return first.compareTo(second);
      }
      return -1;
    }
    if (StringUtils.startsWith(second, projectPrefix)) {
      return 1;
    }

    // jakarta last
    if (StringUtils.startsWith(first, JAKARTA_PREFIX)) {
      if (StringUtils.startsWith(second, JAKARTA_PREFIX)) {
        return first.compareTo(second);
      }
      return 1;
    }
    if (StringUtils.startsWith(second, JAKARTA_PREFIX)) {
      return -1;
    }

    // javax last
    if (StringUtils.startsWith(first, JAVAX_PREFIX)) {
      if (StringUtils.startsWith(second, JAVAX_PREFIX)) {
        return first.compareTo(second);
      }
      return 1;
    }
    if (StringUtils.startsWith(second, JAVAX_PREFIX)) {
      return -1;
    }

    // java last before javax/jakarta
    if (StringUtils.startsWith(first, JAVA_PREFIX)) {
      if (StringUtils.startsWith(second, JAVA_PREFIX)) {
        return first.compareTo(second);
      }
      return 1;
    }
    if (StringUtils.startsWith(second, JAVA_PREFIX)) {
      return -1;
    }

    // the rest is sorted by string
    return first.compareTo(second);
  }
}

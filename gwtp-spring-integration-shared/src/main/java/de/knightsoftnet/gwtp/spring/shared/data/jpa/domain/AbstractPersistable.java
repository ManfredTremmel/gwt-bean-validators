package de.knightsoftnet.gwtp.spring.shared.data.jpa.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import org.springframework.data.domain.Persistable;
import org.springframework.lang.Nullable;

import java.io.Serializable;
import java.util.Objects;

import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.MappedSuperclass;
import jakarta.persistence.Transient;

/**
 * Abstract base class for entities. Allows parameterization of id type, chooses auto-generation and
 * implements {@link #equals(Object)} and {@link #hashCode()} based on that id. Based on
 * {@link org.springframework.data.jpa.domain.AbstractPersistable} but with public setter for id,
 * it's required on editor and serializer on client side.
 *
 * @author Oliver Gierke
 * @author Thomas Darimont
 * @author Mark Paluch
 * @author Manfred Tremmel
 * @param <P> the type of the identifier.
 */
@MappedSuperclass
public abstract class AbstractPersistable<P extends Serializable> implements Persistable<P> {

  @Id
  @GeneratedValue
  private @Nullable P id;

  /*
   * (non-Javadoc)
   *
   * @see org.springframework.data.domain.Persistable#getId()
   */
  @Nullable
  @Override
  public P getId() {
    return id;
  }

  /**
   * Sets the id of the entity.
   *
   * @param id the id to set
   */
  public void setId(@Nullable final P id) {
    this.id = id;
  }

  /**
   * Must be {@link Transient} in order to ensure that no JPA provider complains because of a
   * missing setter.
   *
   * @see org.springframework.data.domain.Persistable#isNew()
   */
  @Transient // DATAJPA-622
  @Override
  @JsonIgnore
  public boolean isNew() {
    return null == getId();
  }

  /*
   * (non-Javadoc)
   *
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "Entity of type " + this.getClass().getName() + " with id: " + getId();
  }

  /*
   * (non-Javadoc)
   *
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(final Object obj) {
    if (null == obj) {
      return false;
    }

    if (this == obj) {
      return true;
    }

    if (getClass() != obj.getClass()) {
      return false;
    }

    final AbstractPersistable<?> that = (AbstractPersistable<?>) obj;

    return Objects.equals(this.getId(), that.getId());
  }

  /*
   * (non-Javadoc)
   *
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {
    return Objects.hashCode(getId());
  }
}

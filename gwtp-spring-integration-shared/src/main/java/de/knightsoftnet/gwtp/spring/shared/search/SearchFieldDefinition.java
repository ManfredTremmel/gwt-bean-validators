/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.gwtp.spring.shared.search;

import de.knightsoftnet.validators.shared.data.FieldTypeEnum;

import org.apache.commons.lang3.tuple.Pair;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class SearchFieldDefinition {
  private final String fieldName;
  private final String fieldDisplayName;
  private final FieldTypeEnum fieldType;
  private final Collection<Pair<String, String>> localizedValues;
  private final Collection<SearchOperation> allowedSearchOperations;

  /**
   * constructor initializing all fields.
   */
  public SearchFieldDefinition(final String fieldName, final String fieldDisplayName,
      final FieldTypeEnum fieldType) {
    this(fieldName, fieldDisplayName, fieldType, Collections.emptyList(),
        defaultSearchOptions(fieldType));
  }

  /**
   * constructor initializing all fields.
   */
  public SearchFieldDefinition(final String fieldName, final String fieldDisplayName,
      final FieldTypeEnum fieldType, final Collection<Pair<String, String>> localizedValues) {
    this(fieldName, fieldDisplayName, fieldType, localizedValues, defaultSearchOptions(fieldType));
  }

  /**
   * constructor initializing all fields.
   */
  public SearchFieldDefinition(final String fieldName, final String fieldDisplayName,
      final FieldTypeEnum fieldType, final Collection<Pair<String, String>> localizedValues,
      final SearchOperation... allowedSearchOperations) {
    super();
    this.fieldName = fieldName;
    this.fieldDisplayName = fieldDisplayName;
    this.fieldType = fieldType;
    this.localizedValues = localizedValues;
    this.allowedSearchOperations = List.of(allowedSearchOperations);
  }

  private static SearchOperation[] defaultSearchOptions(final FieldTypeEnum fieldType) {
    return switch (fieldType) {
      case BOOLEAN, ENUM_SQL, ENUM_FIXED -> new SearchOperation[] {SearchOperation.EQUALITY,
          SearchOperation.NEGATION}; // NOPMD
      case DATE, TIME, DATETIME -> new SearchOperation[] {SearchOperation.EQUALITY, // NOPMD
          SearchOperation.NEGATION, // NOPMD
          SearchOperation.GREATER_THEN, // NOPMD
          SearchOperation.GREATER_OR_EQUAL_THEN, // NOPMD
          SearchOperation.LESS_THEN, // NOPMD
          SearchOperation.LESS_OR_EQUAL_THEN}; // NOPMD
      default -> SearchOperation.values();
    };
  }

  public String getFieldName() {
    return fieldName;
  }

  public String getFieldDisplayName() {
    return fieldDisplayName;
  }

  public FieldTypeEnum getFieldType() {
    return fieldType;
  }

  public Collection<Pair<String, String>> getLocalizedValues() {
    return localizedValues;
  }

  public Collection<SearchOperation> getAllowedSearchOperations() {
    return allowedSearchOperations;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(fieldName);
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final SearchFieldDefinition other = (SearchFieldDefinition) obj;
    return Objects.equals(fieldName, other.fieldName);
  }

  @Override
  public String toString() {
    return fieldName;
  }
}

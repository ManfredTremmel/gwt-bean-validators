/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.gwtp.spring.shared.search;

import com.fasterxml.jackson.annotation.JsonIgnore;

import org.apache.commons.lang3.StringUtils;

import java.util.Objects;

public class SearchCriteria<K, T> {
  private K key;
  private SearchOperation operation;
  private T value;

  /**
   * default constructor.
   */
  public SearchCriteria() {
    super();
  }

  /**
   * constructor initializing fields.
   */
  public SearchCriteria(final K key, final SearchOperation operation, final T value) {
    super();
    this.key = key;
    this.operation = operation;
    this.value = value;
  }

  public final K getKey() {
    return key;
  }

  public final void setKey(final K key) {
    this.key = key;
  }

  public final SearchOperation getOperation() {
    return operation;
  }

  public final void setOperation(final SearchOperation operation) {
    this.operation = operation;
  }

  public final T getValue() {
    return value;
  }

  public final void setValue(final T value) {
    this.value = value;
  }

  @JsonIgnore
  public final boolean isLocalizedText() {
    return StringUtils.endsWith(Objects.toString(key, StringUtils.EMPTY), ".localizedText");
  }

  @Override
  public String toString() {
    return Objects.toString(key, StringUtils.EMPTY) + operation.getSimpleOperation()
        + StringUtils.replaceChars(Objects.toString(value, StringUtils.EMPTY), ',', '，');
  }
}

package de.knightsoftnet.gwtp.spring.shared.data;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Deserializeable {@code Page} implementation.
 *
 * @param <T> the type of which the page consists.
 */
public class DeserializeablePage<T> implements Page<T> {

  private int number;
  private int size;
  private int totalPages;
  private int numberOfElements;
  private long totalElements;
  private boolean empty;
  private boolean first;
  private boolean last;
  private List<T> content;
  private Sort sort;
  // private Pageable pageable;

  /**
   * constructor.
   *
   * @param content content list
   * @param pageable pageable element
   * @param total total elements
   */
  @SuppressWarnings("PMD.UnusedFormalParameter")
  public DeserializeablePage(final List<T> content, final Pageable pageable, final long total) {
    this.content = content;
    // this.pageable = pageable;
    totalElements = total;
  }

  public DeserializeablePage(final List<T> content) {
    this(content, Pageable.unpaged(), null == content ? 0 : content.size());
  }

  public DeserializeablePage() {
    this(new ArrayList<>());
  }

  public PageImpl<T> pageImpl() {
    return new PageImpl<>(getContent(), PageRequest.of(getNumber(), getSize(), getSort()),
        getTotalElements());
  }

  @Override
  public final int getNumber() {
    return number;
  }

  public final void setNumber(final int number) {
    this.number = number;
  }

  @Override
  public final int getSize() {
    return size;
  }

  public final void setSize(final int size) {
    this.size = size;
  }

  @Override
  public final int getTotalPages() {
    return totalPages;
  }

  public final void setTotalPages(final int totalPages) {
    this.totalPages = totalPages;
  }

  @Override
  public final int getNumberOfElements() {
    return numberOfElements;
  }

  public final void setNumberOfElements(final int numberOfElements) {
    this.numberOfElements = numberOfElements;
  }

  @Override
  public final long getTotalElements() {
    return totalElements;
  }

  public final void setTotalElements(final long totalElements) {
    this.totalElements = totalElements;
  }

  @Override
  public boolean isEmpty() {
    return empty;
  }

  public void setEmpty(final boolean empty) {
    this.empty = empty;
  }

  @Override
  public final boolean isFirst() {
    return first;
  }

  public final void setFirst(final boolean first) {
    this.first = first;
  }

  @Override
  public final boolean isLast() {
    return last;
  }

  public final void setLast(final boolean last) {
    this.last = last;
  }

  @Override
  public final List<T> getContent() {
    return content;
  }

  public final void setContent(final List<T> content) {
    this.content = content;
  }

  @Override
  public final Sort getSort() {
    return sort;
  }

  public final void setSort(final Sort sort) {
    this.sort = sort;
  }

  // public final Pageable getPageable() {
  // return this.pageable;
  // }
  //
  // public final void setPageable(final Pageable pageable) {
  // this.pageable = pageable;
  // }

  @Override
  public boolean hasContent() {
    return !CollectionUtils.isEmpty(content);
  }

  @Override
  public boolean hasNext() {
    return getNumber() + 1 < this.getTotalPages();
  }

  @Override
  public boolean hasPrevious() {
    return getNumber() > 0;
  }

  @Override
  public Pageable nextPageable() {
    // return hasNext() ? this.pageable.next() : Pageable.unpaged();
    return null;
  }

  @Override
  public Pageable previousPageable() {
    // return this.hasPrevious() ? this.pageable.previousOrFirst() : Pageable.unpaged();
    return null;
  }

  @Override
  public Iterator<T> iterator() {
    return content.iterator();
  }

  @Override
  public <U> Page<U> map(final Function<? super T, ? extends U> converter) {
    return new DeserializeablePage<>(this.getConvertedContent(converter), getPageable(),
        totalElements);
  }

  /**
   * Applies the given {@link Function} to the content of the Chunk.
   *
   * @param converter must not be {@literal null}.
   * @param <U> type of the entries
   * @return list of converted entries
   */
  protected <U> List<U> getConvertedContent(final Function<? super T, ? extends U> converter) {

    Assert.notNull(converter, "Function must not be null!");

    return stream().map(converter::apply).collect(Collectors.toList());
  }
}

package de.knightsoftnet.gwtp.spring.annotation.processor;

import java.util.Objects;

/**
 * Widget parameter used for generating ui binder and view.
 */
public class BackofficeWidgetParameter {

  private String name;
  private String value;
  private boolean uiBinder;

  /**
   * default constructor.
   */
  public BackofficeWidgetParameter() {
    super();
  }

  /**
   * constructor filling fields.
   *
   * @param name parameter name
   * @param value parameter value
   * @param uiBinder true when parameter is used in ui binder
   */
  public BackofficeWidgetParameter(final String name, final String value, final boolean uiBinder) {
    super();
    this.name = name;
    this.value = value;
    this.uiBinder = uiBinder;
  }

  public static BackofficeWidgetParameter of(final String name, final String value,
      final boolean uiBinder) {
    return new BackofficeWidgetParameter(name, value, uiBinder);
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public String getValue() {
    return value;
  }

  public void setValue(final String value) {
    this.value = value;
  }

  public boolean isUiBinder() {
    return uiBinder;
  }

  public void setUiBinder(final boolean uiBinder) {
    this.uiBinder = uiBinder;
  }

  @Override
  public int hashCode() {
    return Objects.hash(name);
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final BackofficeWidgetParameter other = (BackofficeWidgetParameter) obj;
    return Objects.equals(name, other.name);
  }

  @Override
  public String toString() {
    return "BackofficeWidgetParameter [name=" + name + ", value=" + value + ", uiBinder=" + uiBinder
        + "]";
  }
}

gwt-bean-validators-engine-jsr310
=================================

gwt-bean-validators-engine-jsr310 is a drop-in replacement for [gwt-bean-validators-engine](../gwt-bean-validators-engine). It includes [gwt-time](https://github.com/foal/gwt-time) to extend gwt with support for [JSR-310](https://jcp.org/en/jsr/detail?id=310) Date and Time API. The validation for this types is included into the validators engine.

Supported Validation Annotations
--------------------------------
This are the extended validators which work now with JSR-310 datatypes:

|Annotation | Support state 
|------|------------------
|Future | works
|FutureOrPresent | works
|Past | works
|PastOrPresent | works
|DurationMax | works
|DurationMin | works


Maven integration
----------------

Add the dependencies itself for GWT-Projects:

```xml
    <dependency>
      <groupId>de.knightsoft-net</groupId>
      <artifactId>gwt-bean-validators-engine-jsr310</artifactId>
      <version>2.4.1</version>
    </dependency>
```

And exclude **validation-api** from your **gwt-user** dependency (otherwise old 1.0.0.GA will be included):

```xml
    <dependency>
      <groupId>org.gwtproject</groupId>
      <artifactId>gwt-user</artifactId>
      <version>2.12.1</version>
      <exclusions>
        <exclusion>
          <groupId>javax.validation</groupId>
          <artifactId>validation-api</artifactId>
        </exclusion>
      </exclusions>
    </dependency>
```

GWT Integration
---------------

Add this inherits entry to your projects gwt.xml configuration file:

```xml
<inherits name="de.knightsoftnet.validators.GwtBeanValidatorsEngineJsr310" />
```

The rest is equal to [gwt-bean-validators-engine](../gwt-bean-validators-engine), so read stuff there.

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.client.factories;

import de.knightsoftnet.validators.client.AbstractGwtValidatorFactory;
import de.knightsoftnet.validators.client.GwtValidation;
import de.knightsoftnet.validators.client.impl.AbstractGwtValidator;
import de.knightsoftnet.validators.shared.beans.HibernateDurationMaxTestBean;
import de.knightsoftnet.validators.shared.beans.HibernateDurationMinTestBean;
import de.knightsoftnet.validators.shared.beans.HibernateFutureLocalDateTestBean;
import de.knightsoftnet.validators.shared.beans.HibernateFutureLocalDateTimeTestBean;
import de.knightsoftnet.validators.shared.beans.HibernateFutureOrPresentLocalDateTestBean;
import de.knightsoftnet.validators.shared.beans.HibernateFutureOrPresentLocalDateTimeTestBean;
import de.knightsoftnet.validators.shared.beans.HibernatePastLocalDateTestBean;
import de.knightsoftnet.validators.shared.beans.HibernatePastLocalDateTimeTestBean;
import de.knightsoftnet.validators.shared.beans.HibernatePastOrPresentLocalDateTestBean;
import de.knightsoftnet.validators.shared.beans.HibernatePastOrPresentLocalDateTimeTestBean;

import jakarta.validation.Validator;

/**
 * The <code>ValidatorFactory</code> class is used for client side validation by annotation. All
 * beans which should be checked by bean validators have to be added to @GwtValidation annotation.
 *
 * @author Manfred Tremmel
 *
 */
public class ValidatorFactoryJsr310 extends AbstractGwtValidatorFactory {

  /**
   * Validator marker for the Validation Sample project. Only the classes and groups listed in the
   * {@link GwtValidation} annotation can be validated.
   */
  @GwtValidation(value = {

      // test beans for hibernate validators
      HibernateDurationMaxTestBean.class, HibernateDurationMinTestBean.class,
      HibernateFutureLocalDateTestBean.class, HibernateFutureLocalDateTimeTestBean.class,
      HibernateFutureOrPresentLocalDateTestBean.class,
      HibernateFutureOrPresentLocalDateTimeTestBean.class, HibernatePastLocalDateTestBean.class,
      HibernatePastLocalDateTimeTestBean.class, HibernatePastOrPresentLocalDateTestBean.class,
      HibernatePastOrPresentLocalDateTimeTestBean.class

  }, forceUsingGetter = true, generateValidatorFactoryInterface = false)
  public interface GwtValidatorJsr310 extends Validator {
  }

  @Override
  public final AbstractGwtValidator createValidator() {
    return new GwtValidatorJsr310Impl();
  }
}

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.client;

import com.google.gwt.junit.tools.GWTTestSuite;

import junit.framework.Test;

/**
 * combine the single tests to speed up run.
 *
 * @author Manfred Tremmel
 *
 */
public class GwtTestBeanValidatorsJsr310Suite extends GWTTestSuite {
  /**
   * build a suite of all gwt unit tests.
   *
   * @return the test suite
   */
  public static Test suite() { // NOPMD
    final GWTTestSuite suite =
        new GWTTestSuite("validation engine client side integration tests for jsr-310");

    GwtTestBeanValidatorsJsr310Suite.appendHibernateBeanValidatorTests(suite);

    return suite;
  }

  private static void appendHibernateBeanValidatorTests(final GWTTestSuite suite) {
    suite.addTestSuite(GwtTstHibernateDurationMax.class);
    suite.addTestSuite(GwtTstHibernateDurationMin.class);
    suite.addTestSuite(GwtTstHibernateFutureLocalDate.class);
    suite.addTestSuite(GwtTstHibernateFutureLocalDateTime.class);
    suite.addTestSuite(GwtTstHibernateFutureOrPresentLocalDate.class);
    suite.addTestSuite(GwtTstHibernateFutureOrPresentLocalDateTime.class);
    suite.addTestSuite(GwtTstHibernatePastLocalDate.class);
    suite.addTestSuite(GwtTstHibernatePastLocalDateTime.class);
    suite.addTestSuite(GwtTstHibernatePastOrPresentLocalDate.class);
    suite.addTestSuite(GwtTstHibernatePastOrPresentLocalDateTime.class);
  }
}

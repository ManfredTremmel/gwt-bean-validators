/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.client;

import de.knightsoftnet.validators.shared.beans.HibernateDurationMinTestBean;
import de.knightsoftnet.validators.shared.testcases.HibernateDurationMinTestCases;

/**
 * test for hibernate DurationMin validator.
 *
 * @author Manfred Tremmel
 *
 */
public class GwtTstHibernateDurationMin
    extends AbstractValidationJsr310Tst<HibernateDurationMinTestBean> {

  /**
   * empty DurationMin is allowed.
   */
  public final void testEmptyDurationMinIsAllowed() {
    validationTest(HibernateDurationMinTestCases.getEmptyTestBean(), true, null);
  }

  /**
   * correct DurationMin is allowed.
   */
  public final void testCorrectDurationMinAreAllowed() {
    for (final HibernateDurationMinTestBean testBean : HibernateDurationMinTestCases
        .getCorrectTestBeans()) {
      validationTest(testBean, true, null);
    }
  }

  /**
   * wrong DurationMin is not allowed.
   */
  public final void testWrongDurationMinAreWrong() {
    for (final HibernateDurationMinTestBean testBean : HibernateDurationMinTestCases
        .getWrongtoSmallTestBeans()) {
      validationTest(testBean, false,
          "org.hibernate.validator.internal.constraintvalidators.hv.time.DurationMinValidator");
    }
  }
}

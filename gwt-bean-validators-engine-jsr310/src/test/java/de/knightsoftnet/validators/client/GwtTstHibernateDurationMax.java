/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.client;

import de.knightsoftnet.validators.shared.beans.HibernateDurationMaxTestBean;
import de.knightsoftnet.validators.shared.testcases.HibernateDurationMaxTestCases;

/**
 * test for hibernate DurationMax validator.
 *
 * @author Manfred Tremmel
 *
 */
public class GwtTstHibernateDurationMax
    extends AbstractValidationJsr310Tst<HibernateDurationMaxTestBean> {

  /**
   * empty DurationMax is allowed.
   */
  public final void testEmptyDurationMaxIsAllowed() {
    validationTest(HibernateDurationMaxTestCases.getEmptyTestBean(), true, null);
  }

  /**
   * correct DurationMax is allowed.
   */
  public final void testCorrectDurationMaxAreAllowed() {
    for (final HibernateDurationMaxTestBean testBean : HibernateDurationMaxTestCases
        .getCorrectTestBeans()) {
      validationTest(testBean, true, null);
    }
  }

  /**
   * wrong DurationMax is not allowed.
   */
  public final void testWrongDurationMaxAreWrong() {
    for (final HibernateDurationMaxTestBean testBean : HibernateDurationMaxTestCases
        .getWrongtoSmallTestBeans()) {
      validationTest(testBean, false,
          "org.hibernate.validator.internal.constraintvalidators.hv.time.DurationMaxValidator");
    }
  }
}

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.testcases;

import de.knightsoftnet.validators.shared.beans.HibernateFutureOrPresentLocalDateTimeTestBean;

import java.time.Clock;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * get test cases for hibernate future or present test.
 *
 * @author Manfred Tremmel
 *
 */
public class HibernateFutureOrPresentLocalDateTimeTestCases {
  /**
   * get empty test bean.
   *
   * @return empty test bean
   */
  public static final HibernateFutureOrPresentLocalDateTimeTestBean getEmptyTestBean() {
    return new HibernateFutureOrPresentLocalDateTimeTestBean(null);
  }

  /**
   * get correct test beans.
   *
   * @return correct test beans
   */
  public static final List<HibernateFutureOrPresentLocalDateTimeTestBean> getCorrectTestBeans() {
    final List<HibernateFutureOrPresentLocalDateTimeTestBean> correctCases = new ArrayList<>();
    correctCases.add(new HibernateFutureOrPresentLocalDateTimeTestBean(null));
    correctCases.add(new HibernateFutureOrPresentLocalDateTimeTestBean(getFutureDate()));

    return correctCases;
  }

  /**
   * get wrong test beans.
   *
   * @return wrong test beans
   */
  public static final List<HibernateFutureOrPresentLocalDateTimeTestBean> getWrongtoSmallTestBeans() {
    final List<HibernateFutureOrPresentLocalDateTimeTestBean> wrongCases = new ArrayList<>();
    wrongCases.add(new HibernateFutureOrPresentLocalDateTimeTestBean(getPastDate()));

    return wrongCases;
  }

  private static LocalDateTime getFutureDate() {
    return now().plusDays(1L);
  }

  private static LocalDateTime getPastDate() {
    return now().minusDays(1L);
  }

  private static LocalDateTime now() {
    try {
      return LocalDateTime.now();
    } catch (final Exception e) {
      return LocalDateTime.now(Clock.systemUTC());
    }
  }
}

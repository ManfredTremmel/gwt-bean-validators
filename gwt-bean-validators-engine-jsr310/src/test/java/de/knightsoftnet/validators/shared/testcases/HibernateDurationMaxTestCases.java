/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.testcases;

import de.knightsoftnet.validators.shared.beans.HibernateDurationMaxTestBean;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

/**
 * get test cases for hibernate DurationMax test.
 *
 * @author Manfred Tremmel
 *
 */
public class HibernateDurationMaxTestCases {
  /**
   * get empty test bean.
   *
   * @return empty test bean
   */
  public static final HibernateDurationMaxTestBean getEmptyTestBean() {
    return new HibernateDurationMaxTestBean(null);
  }

  /**
   * get correct test beans.
   *
   * @return correct test beans
   */
  public static final List<HibernateDurationMaxTestBean> getCorrectTestBeans() {
    final List<HibernateDurationMaxTestBean> correctCases =
        new ArrayList<>();
    correctCases.add(new HibernateDurationMaxTestBean(null));
    correctCases.add(new HibernateDurationMaxTestBean(Duration.ofMinutes(30L)));
    correctCases.add(new HibernateDurationMaxTestBean(Duration.ofHours(12L)));
    correctCases.add(new HibernateDurationMaxTestBean(Duration.ofHours(24L)));

    return correctCases;
  }

  /**
   * get wrong test beans.
   *
   * @return wrong test beans
   */
  public static final List<HibernateDurationMaxTestBean> getWrongtoSmallTestBeans() {
    final List<HibernateDurationMaxTestBean> wrongCases =
        new ArrayList<>();
    wrongCases.add(new HibernateDurationMaxTestBean(Duration.ofHours(25L)));
    wrongCases.add(new HibernateDurationMaxTestBean(Duration.ofDays(3L)));

    return wrongCases;
  }
}

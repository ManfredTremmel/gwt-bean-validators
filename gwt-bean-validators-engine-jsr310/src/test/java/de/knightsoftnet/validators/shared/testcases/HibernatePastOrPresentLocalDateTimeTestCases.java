/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.testcases;

import de.knightsoftnet.validators.shared.beans.HibernatePastOrPresentLocalDateTimeTestBean;

import java.time.Clock;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * get test cases for hibernate past or present test.
 *
 * @author Manfred Tremmel
 *
 */
public class HibernatePastOrPresentLocalDateTimeTestCases {
  /**
   * get empty test bean.
   *
   * @return empty test bean
   */
  public static final HibernatePastOrPresentLocalDateTimeTestBean getEmptyTestBean() {
    return new HibernatePastOrPresentLocalDateTimeTestBean(null);
  }

  /**
   * get correct test beans.
   *
   * @return correct test beans
   */
  public static final List<HibernatePastOrPresentLocalDateTimeTestBean> getCorrectTestBeans() {
    final List<HibernatePastOrPresentLocalDateTimeTestBean> correctCases = new ArrayList<>();
    correctCases.add(new HibernatePastOrPresentLocalDateTimeTestBean(null));
    correctCases.add(new HibernatePastOrPresentLocalDateTimeTestBean(getPastDate()));

    return correctCases;
  }

  /**
   * get wrong test beans.
   *
   * @return wrong test beans
   */
  public static final List<HibernatePastOrPresentLocalDateTimeTestBean> getWrongtoSmallTestBeans() {
    final List<HibernatePastOrPresentLocalDateTimeTestBean> wrongCases = new ArrayList<>();
    wrongCases.add(new HibernatePastOrPresentLocalDateTimeTestBean(getFutureDate()));

    return wrongCases;
  }

  private static LocalDateTime getFutureDate() {
    return now().plusDays(1L);
  }

  private static LocalDateTime getPastDate() {
    return now().minusDays(1L);
  }

  private static LocalDateTime now() {
    try {
      return LocalDateTime.now();
    } catch (final Exception e) {
      return LocalDateTime.now(Clock.systemUTC());
    }
  }
}
